param
(
    [string]$cmsSource = "localhost",
    [string]$cmsCatalog = "CCET1",
    [string]$cmsUserId = "sa",
    [string]$cmsUserPassword = "",
    [string]$identitySource = "localhost",
    [string]$identityCatalog = "ApplicationIdentity",
    [string]$identityUserId = "sa",
    [string]$identityUserPassword = "",

    [string]$sbcSource = "localhost",
    [string]$sbcCatalog = "Subscriptions",
    [string]$sbcUserId = "sa",
    [string]$sbcUserPassword = "",

    [string]$lgSource = "localhost",
    [string]$lgCatalog = "Logs",
    [string]$lgUserId = "sa",
    [string]$lgUserPassword = "",

    [string]$mode = "debug"
)
$xml = @'
<?xml version="1.0" encoding="utf-8" ?>
<connectionStrings>
  <add name="CMSContext" connectionString="data source={0};initial catalog={1};user id={2}; password={3}; MultipleActiveResultSets=True;" providerName="System.Data.SqlClient" />
  <add name="ApplicationIdentityConnectionString" connectionString="data source={4};initial catalog={5};user id={6}; password={7}; MultipleActiveResultSets=True;" providerName="System.Data.SqlClient" />
  <add name="SubscriptionsContext" connectionString="data source={8};initial catalog={9};user id={10}; password={11}; MultipleActiveResultSets=True;" providerName="System.Data.SqlClient" />
  <add name="LogsContext" connectionString="data source={12};initial catalog={13};user id={14}; password={15}; MultipleActiveResultSets=True;" providerName="System.Data.SqlClient" />
</connectionStrings>
'@ -f $cmsSource, $cmsCatalog, $cmsUserId, $cmsUserPassword, $identitySource, $identityCatalog, $identityUserId, $identityUserPassword,
$sbcSource, $sbcCatalog, $sbcUserId, $sbcUserPassword, $lgSource, $lgCatalog, $lgUserId, $lgUserPassword
$fileName = ".\Pixelario.CMS\src\4_Presentation\Pixelario.CMS.Web\App_Data\connectionStrings-{0}.xml" -f $mode
$xml | out-file $fileName -encoding utf8