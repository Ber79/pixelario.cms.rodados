# Login

Es la pantalla inicial del administrador, los usuarios con roles administrativos deben autenticarse con sus credenciales.


![Bloque de login](Images/Login-box.png)

Luego de **3 intentos** la cuenta del usuario quedar&aacute; bloqueada y deber&aacute; solicitar el desbloqueo.

### Campos obligatorios

|Campo|Descripci&oacute;n|
|---|---|
|Nombre de usuario| Texto sin espacio o correo electr&oacute;nico|
|Password|Contrase&ntilde;a del usuario. Se enmascara con astericos|
|Recordame| Permite continuar logueado|