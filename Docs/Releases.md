# Releases
## Version 2020.0605
* Add infinite scroll on topics, subtopics and contents
* Cache memory on page commands
* Fix several configuration and cache errors
## Version 2020.0604
* Add article plugins
* Add image gallery to special edition's articles
## Version 2020.0603
* Cache memory on articles commands
* Cache memory on advertisments commands
* Cache memory on Topic and subtopics commands
## Version 2020.0602
* Refactoring enumeration configurations
* Downgrade logs to Errors on Release
* Cache memory on configurations commands
* Prevent ghost borders on image service drawing
## Version 2020.0601
* Fix body error on articles create and update form
* Cache Memory on Contents
* Widget on article bottom view
## Version 2020.0512
* New Header
* New Script Widgets
## Version 2020.0511
* Improvements on software stability 
## Version 2.5.10
* Flyers now witch script code
## Version 2.5.9
* Improbe SEO
## Version 2.5.8
* Paging on home search result
## Version 2.5.7
* Change text editor
* Improbe special editions UI at Home
## Version 2.5.6
* Special content on menu
## Version 2.5.5
* Starting to deliver User Manual
## Version 2.5.4
* Constraint with maximum lengths on text inputs at administrator views
* Preview on pages and articles