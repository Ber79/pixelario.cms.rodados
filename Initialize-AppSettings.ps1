param
(
    [string]$appName = "",
    [string]$appDomain = "",
    [string]$mode = "Debug",
    [string]$enableSubscriptions = "false"
)
$appVersion = ""
try
{
    $gitRemote = Git ls-remote --tags | ForEach-object { $_.substring(51).TrimEnd("^{}") } |  sort -d | select -first 1
    if(![string]::IsNullOrWhiteSpace($gitRemote)) 
    { 
        $appVersion = $gitRemote
    }
}
catch{
    $appVersion = "Not found"
}
$xml = @'
<?xml version="1.0" encoding="utf-8" ?>
<appSettings>
  <add key="webpages:Version" value="3.0.0.0" />
  <add key="webpages:Enabled" value="false" />
  <add key="ClientValidationEnabled" value="true" />
  <add key="UnobtrusiveJavaScriptEnabled" value="true" />
  <add key="app:name" value="{0}"/>
  <add key="app:domain" value="{1}"/>
  <add key="app:version" value="{2}"/>
  <add key="enable:subscriptions" value="{3}"/>
</appSettings>
'@ -f $appName, $appDomain, $appVersion, $enableSubscriptions
$fileName = ".\Pixelario.CMS\src\4_Presentation\Pixelario.CMS.Web\App_Data\appSettings-{0}.xml" -f $mode
$xml | out-file $fileName -encoding utf8