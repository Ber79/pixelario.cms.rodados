$files = Get-ChildItem .\Docs\ -filter *.md |
 ForEach-Object {
    $folder = ".\Pixelario.CMS\src\4_Presentation\Pixelario.CMS.Web\Manual\" 
 $filename = $_.Basename
    markdown $_.FullName -t $_.Basename -s "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" > "$folder$filename.html"
}
$images = Get-ChildItem .\Docs\Images\ |
 ForEach-Object {
    $folder = ".\Pixelario.CMS\src\4_Presentation\Pixelario.CMS.Web\Manual\Images" 
    Copy-Item $_.FullName -Destination $folder
}