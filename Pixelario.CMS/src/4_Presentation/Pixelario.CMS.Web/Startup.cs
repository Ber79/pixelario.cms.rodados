﻿using Owin;
using Pixelario.CMS.Web.Infrastructure;
using System.Web.Http;

namespace Pixelario.CMS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AdminUsersInitializer.Seed();
        }
    }
}