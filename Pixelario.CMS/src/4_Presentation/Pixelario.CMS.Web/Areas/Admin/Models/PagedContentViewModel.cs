using System;
using System.Web.Mvc;
namespace Pixelario.CMS.Web.Areas.Admin.Models
{
    public class PagedContentViewModel : ContentViewModel
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        public int? ID { get; set; }

        public void SetPagingItems(int? iD,
           int totalCount, int? pageNumber,
           int? pageSize, string columns) 
        {
            SetPagingItems(totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            this.ID = iD;
        }
        public void SetPagingItems(
            int totalCount, int? pageNumber,
            int? pageSize, string columns)
        {
            this.PageCount = CalculatePageCount(
                totalRows: totalCount,
                pageSize: pageSize ?? 10);
            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
            this.Columns = columns;
        }        

        public int BackPage
        {
            get
            {
                if (PageNumber == 1)
                    return 1;
                return PageNumber - 1;
            }
        }
        public int NextPage
        {
            get
            {
                if (PageCount == 0)
                    return 1;
                if (PageNumber == PageCount)
                    return PageCount;
                return PageNumber + 1;
            }
        }

        public int StartPage { get; private set; }
        public int EndPage { get; private set; }



        private int CalculatePageCount(int totalRows, int pageSize)
        {
            if (totalRows == 0)
                return 1;
            return ((totalRows - 1) / pageSize) + 1;

        }
        public void CalculatePagerItems(int pagerItems = 5)
        {

            var pagerCenterIndex = 1;
            this.StartPage = 1;
            this.EndPage = PageCount;
            if (PageCount > pagerItems)
            {
                if (pagerItems % 2 != 0)
                {
                    pagerItems = pagerItems++;
                }
                pagerCenterIndex = pagerItems / 2;

                var currentPage = PageNumber;
                var startPage = currentPage - pagerCenterIndex;
                var endPage = currentPage + pagerCenterIndex;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }
                if (endPage > PageCount)
                {
                    endPage = PageCount;
                    if (endPage > pagerItems)
                    {
                        startPage = endPage - (pagerItems - 1);
                    }
                }
                StartPage = startPage;
                EndPage = endPage;
            }
        }
        public MvcHtmlString ColumnVisible(string index, bool defaultValue)
        {
            if (!string.IsNullOrEmpty(this.Columns))
                return ColumnVisible(index);
            if (defaultValue)
                return MvcHtmlString.Empty;
            else
                return new MvcHtmlString("data-visible=\"false\"");

        }
        public MvcHtmlString ColumnVisible(string index)
        {
            if (string.IsNullOrEmpty(this.Columns))
                return MvcHtmlString.Empty;
            var array = this.Columns.Split(',');
            if (Array.IndexOf(array, index) >= 0)
                return MvcHtmlString.Empty;
            return new MvcHtmlString("data-visible=\"false\"");
        }
        }
}