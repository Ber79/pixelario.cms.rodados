﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.SocialMediaSettings
{
    public class ComebackParametersViewModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        private ComebackParametersViewModel()
        {

        }

        public ComebackParametersViewModel(int pageNumber,
            int pageSize, string columns)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Columns = columns;
        }

        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.PageNumber, this.PageSize,
                    !string.IsNullOrEmpty(this.Columns) ?
                        string.Format("&columns={0}", this.Columns) :
                        "");
            }
        }
    }
}