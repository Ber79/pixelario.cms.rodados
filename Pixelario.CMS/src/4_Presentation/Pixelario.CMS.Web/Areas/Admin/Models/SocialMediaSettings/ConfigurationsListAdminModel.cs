﻿using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.SocialMediaSettings
{
    public class ConfigurationsListAdminModel
    {
        public string Section { get; set; }
        public List<ConfigurationFormAdminModel> Configurations { get; set; }
    }

}