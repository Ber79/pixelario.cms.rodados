﻿using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDEdition { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public string HomeImage { get; set; }
        public List<PlanAdminModel> PlanAdmins { get; set; }
        public List<int> Plans { get; set; }

        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public FormViewModel()
        {
        }

        public FormViewModel(string mode, EditionQueryModel editionQueryModel,
            int? pageNumber, int? pageSize, 
            string columns)
        {
            this.IDEdition = editionQueryModel.IDEdition;
            this.Title = editionQueryModel.Title;
            this.Summary = editionQueryModel.Summary;
            this.Keywords = editionQueryModel.Keywords;
            this.HomeImage = editionQueryModel.HomeImage;
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
        }

        public FormViewModel(string mode,
            List<PlanAdminModel> planAdmins,
            int? pageNumber,
            int? pageSize,
            string columns)
        {
            this.PlanAdmins = planAdmins;
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);

        }
        public FormViewModel(int iDEdition,
            string title,
            string summary, string keywords,
            string mode,
            List<PlanAdminModel> planAdmins,
            int? pageNumber,
            int? pageSize,
            string columns)
        {
            IDEdition = iDEdition;
            Title = title;
            Summary = summary;
            Keywords = keywords;
            this.PlanAdmins = planAdmins;
            this.SetMode(mode);
            this.SetPagingItems(
                iD: iDEdition,
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);

        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDEdition.HasValue)
                    return new ComebackParametersViewModel(
                        iDEdition: this.IDEdition.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);
            }
        }

        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Editions?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de contenidos";
            }
        }
        public ViewDataDictionary GetViewData(int pageNumber, int pageSize)
        {
            return new ViewDataDictionary {
                {"IDEdition", this.IDEdition },
                { "PageNumber", this.PageNumber},
                { "PageSize", this.PageSize}
            };
        }
        public AddCommand ToAddCommand(string createdBy)
        {
            var command = new AddCommand(
                title: Title,
                summary: Summary,
                keywords: Keywords);
            command.SetCreatedBy(createdBy: createdBy);
            return command;
        }
        public UpdateCommand ToUpdateCommand(string solicitedBy)
        {
            if (!IDEdition.HasValue)
                throw new Exception("No se envio Id de la edición");
            var command = new UpdateCommand(
                    idEdition: IDEdition.Value,
                    title: Title,
                    summary: Summary,
                    keywords: Keywords);
            command.SetSolicitedBy(solicitedBy: solicitedBy);
            return command;
        }

        public ChangeConstraintsCommand ToChangeConstraintsCommand()
        {
            if (IDEdition.HasValue)
                return new ChangeConstraintsCommand(
                    iDEdition: IDEdition.Value,
                    plansSelected: Plans);
            throw new Exception("No se envio Id de la edición");
        }

    }
}