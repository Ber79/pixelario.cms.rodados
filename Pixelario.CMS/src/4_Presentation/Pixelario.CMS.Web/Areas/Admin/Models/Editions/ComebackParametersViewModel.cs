﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class ComebackParametersViewModel
    {
        public int? IDEdition { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDEdition,
            int pageNumber,
            int pageSize, string columns) : this(pageNumber, pageSize, columns)
        {
            this.IDEdition = iDEdition;
        }
        public ComebackParametersViewModel( int pageNumber,
            int pageSize, string columns) : this(pageNumber, pageSize)
        {
            this.Columns = columns;
        }
        public ComebackParametersViewModel(int iDEdition,
            int pageNumber,
            int pageSize) : this(pageNumber, pageSize)
        {
            this.IDEdition = iDEdition;
        }
        public ComebackParametersViewModel(int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        public bool HasIDEdition
        {
            get
            {
                return this.IDEdition.HasValue;
            }
        }
        public bool HasColumns
        {
            get
            {
                return !string.IsNullOrEmpty(this.Columns);
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}{3}",
                    this.PageNumber, this.PageSize,
                    this.HasIDEdition ? 
                        string.Format("&edition={0}", this.IDEdition.Value) :
                        "",
                    this.HasColumns ?
                        string.Format("&columns={0}", this.Columns) :
                        "");
            }
        }
        public string URLEditionParameters
        {
            get
            {
                return string.Format("editionPageNumber={0}&editionPageSize={1}{2}",
                    this.PageNumber, this.PageSize,
                    this.HasColumns ?
                        string.Format("&editionColumns={0}", this.Columns) :
                        "");
            }
        }
    }
}