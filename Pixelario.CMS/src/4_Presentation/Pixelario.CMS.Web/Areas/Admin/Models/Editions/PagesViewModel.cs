﻿using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Pages;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class PagesViewModel : PagedContentViewModel
    {
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public List<PageAdminModel> Pages { get; set; }
        public EditionAdminModel Edition { get; set; }
        public PageComebackParametersViewModel EditionComebackParameters
        {
            get
            {
                return new PageComebackParametersViewModel(
                    iDEdition: this.Edition.IDEdition,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
            }
        }
        
        public PageComebackParametersViewModel GetComebackParameters(int id)
        {
            return new PageComebackParametersViewModel(
                    iDPage: id,
                    iDEdition: this.Edition.IDEdition,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
        }

        private PagesViewModel()
        {

        }

        public PagesViewModel(EditionQueryModel edition,
            List<PageQueryModel> pages,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns,
            int totalCount, int? pageNumber, int? pageSize)
        {
            this.Edition = new EditionAdminModel(
                queryModel: edition);
            this.Pages = new List<PageAdminModel>();
            foreach (var pageQueryModel in pages)
            {
               this.Pages.Add(new PageAdminModel(
                    queryModel: pageQueryModel));
            }            
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }

        #region CreatePageButton

        public string CreatePageButtonLink
        {
            get
            {
                return string.Format("/Admin/Pages/Create?edition={0}&editionPageNumber={1}&editionPageSize={2}&editionColumns={3}&pageNumber={4}&pageSize={5}",
                    this.Edition.IDEdition, this.EditionPageNumber, this.EditionPageSize,
                    this.EditionColumns,
                    this.PageNumber, this.PageSize);
            }
        }
        public string CreatePageButtonTitle
        {
            get
            {
                return "Nueva página";
            }
        }

        #endregion
        #region ConfigurationButton

        public string ConfigurationButtonLink
        {
            get
            {
                return string.Format("/Admin/Pages/Configurations?edition={0}&editionPageNumber={1}&editionPageSize={2}&editionColumns={3}&pageNumber={4}&pageSize={5}",
                    this.Edition.IDEdition, this.EditionPageNumber, this.EditionPageSize,
                    this.EditionColumns,
                    this.PageNumber, this.PageSize);
            }
        }
        public string ConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones";
            }
        }
        #endregion

        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Editions?{0}",
                    this.EditionComebackParameters.EditionURLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de contenidos";
            }
        }

        #endregion
    }
}