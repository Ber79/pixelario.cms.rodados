﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class PageAdminModel
    {
        public int IDPage { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }
        private PageAdminModel()
        {
        }
        public PageAdminModel(PageQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDPage = queryModel.IDPage;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.OnMenu = queryModel.OnMenu;
            this.OrderAtMenu = queryModel.OrderAtMenu;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar la página \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region Publish button
        public string OnMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada la página \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada la página \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string OnMenuButtonClass
        {
            get
            {
                return this.OnMenu ? "fa-check" : "fa-times";
            }
        }
        public string OnMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Edición en portada" : "Edición fuera de portada";
            }
        }
        #endregion
        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar la página \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar la página \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Edición habilitado" : "Edición deshabilitado";
            }
        }

        #endregion
        #region Preview Button

        public string PreviewURL
        {
            get
            {
                return string.Format("/preview/pages/Index/{0}",
                    this.IDPage);
            }
        }
        #endregion
        #region UpdateLink
        public string UpdateLinkURL(PageComebackParametersViewModel editionComebackParameters)
        {
            return string.Format("/Admin/Pages/Update/{0}?{1}",
                this.IDPage, editionComebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar página";
            }
        }
        #endregion
        #region MultimediaLink
        public string MultimediaLinkURL(PageComebackParametersViewModel editionComebackParameters)
        {
            return string.Format("/Admin/Pages/Images/{0}?{1}",
                this.IDPage, editionComebackParameters.URLParameters);
        }
        public string MultimediaLinkTitle
        {
            get
            {
                return "Multimedios de la página";
            }
        }
        #endregion
        #region DocumentsLink
        public string DocumentsLinkURL(PageComebackParametersViewModel editionComebackParameters)
        {
            return string.Format("/Admin/Pages/Documents/{0}?{1}",
                this.IDPage, editionComebackParameters.URLParameters);
        }
        public string DocumentsLinkTitle
        {
            get
            {
                return "Documentos de la página";
            }
        }
        #endregion
        #region WidgetsLink
        public string WidgetsLinkURL(PageComebackParametersViewModel editionComebackParameters)
        {
            return string.Format("/Admin/Pages/Widgets/{0}?{1}",
                this.IDPage, editionComebackParameters.URLParameters);
        }
        public string WidgetsLinkTitle
        {
            get
            {
                return "Widgets de la página";
            }
        }
        #endregion

    }
}