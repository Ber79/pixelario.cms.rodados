﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class PageComebackParametersViewModel
    {
        public int? IDPage { get; set; }
        public int IDEdition { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        private PageComebackParametersViewModel()
        {

        }
        public PageComebackParametersViewModel(int? iDPage, int iDEdition,
            int pageNumber, int pageSize,
            int editionPageNumber, int editionPageSize,
            string editionColumns) : this(iDEdition,
                pageNumber, pageSize,
                editionPageNumber, editionPageSize,
                editionColumns)
        {
            this.IDPage = iDPage;
        }
      
        public PageComebackParametersViewModel(int iDEdition,
            int pageNumber, int pageSize, 
            int editionPageNumber, int editionPageSize,
            string editionColumns) 
        {
            this.IDEdition = iDEdition;       
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.EditionPageNumber = editionPageNumber;
            this.EditionPageSize = editionPageSize;
            this.EditionColumns = editionColumns;
        }
       
        public bool HasEditionColumns
        {
            get
            {
                return !string.IsNullOrEmpty(this.EditionColumns);
            }
        }
        public bool HasIDPage
        {
            get
            {
                return this.IDPage.HasValue;
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}&edition={2}&editionPageNumber={3}&editionPageSize={4}{5}",
                    this.PageNumber, this.PageSize,
                    this.IDEdition, this.EditionPageNumber,
                    this.EditionPageSize,
                    this.HasEditionColumns ?
                        string.Format("&editionColumns={0}", this.EditionColumns) :
                        "");
            }
        }
        public string EditionURLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.PageNumber, this.PageSize,
                    this.HasEditionColumns ?
                        string.Format("&columns={0}", this.EditionColumns) :
                        "");
            }
        }
    }
}