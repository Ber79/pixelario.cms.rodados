﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<EditionAdminModel> Editions { get; set; }
        public IndexViewModel(List<EditionQueryModel> editionQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize,
            string columns)
        {
            this.Editions = new List<EditionAdminModel>();
            foreach (var queryModel in editionQueryModels)
            {
                Editions.Add(new EditionAdminModel(queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
        }

        public ComebackParametersViewModel ComebackParameters { 
            get {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns);
            } 
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDEdition: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns);
        }

        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Editions/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo contenido";
            }
        }

        #endregion
        #region ConfigurationButton
        public string ConfigurationButtonHref
        {
            get
            {
                return string.Format("/Admin/Editions/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string ConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones";
            }
        }

        #endregion
        #region WebsiteConfigurationButton
        public string WebsiteConfigurationButtonHref
        {
            get
            {
                return string.Format("/Admin/WebSiteSettings/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string WebsiteConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones de la web";
            }
        }

        #endregion
        #region HomeSettingsConfigurationButton
        public string HomeSettingsButtonHref
        {
            get
            {
                return string.Format("/Admin/HomeSettings/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string HomeSettingsButtonTitle
        {
            get
            {
                return "Configuraciones de la página de inicio";
            }
        }

        #endregion
        #region SocialMediaSettingsConfigurationButton
        public string SocialMediaSettingsButtonHref
        {
            get
            {
                return string.Format("/Admin/SocialMediaSettings/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string SocialMediaSettingsButtonTitle
        {
            get
            {
                return "Configuraciones de social media";
            }
        }

        #endregion    
        #region ContactSettingsConfigurationButton
        public string ContactSettingsButtonHref
        {
            get
            {
                return string.Format("/Admin/ContactSettings/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string ContactSettingsButtonTitle
        {
            get
            {
                return "Configuraciones de contacto";
            }
        }

        #endregion
        #region ArticleScriptButton
        public string ArticleScriptButtonHref
        {
            get
            {
                return string.Format("/Admin/ArticleScripts/Index?{0}", 
                    this.ComebackParameters.URLEditionParameters);
            }
        }
        public string ArticleScriptButtonTitle
        {
            get
            {
                return "Scripts de artículos";
            }
        }

        #endregion    

    }
}