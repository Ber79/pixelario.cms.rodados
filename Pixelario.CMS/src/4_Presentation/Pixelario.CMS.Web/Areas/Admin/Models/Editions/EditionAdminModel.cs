﻿using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Editions
{
    public class EditionAdminModel
    {
        public int IDEdition { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }

        public EditionType EditionType { get; set; }
        private EditionAdminModel()
        {
        }
        public EditionAdminModel(EditionQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDEdition = queryModel.IDEdition;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.AtHome = queryModel.AtHome;
            this.EditionType = queryModel.EditionType;
            this.Order = queryModel.Order;

            this.OnMenu = queryModel.OnMenu;
            this.OrderAtMenu = queryModel.OrderAtMenu;
        }

        public ViewDataDictionary GetViewData(int pageNumber, int pageSize)
        {
            return new ViewDataDictionary {
                {"IDEdition", this.IDEdition },
                { "PageNumber", pageNumber},
                { "PageSize", pageSize}
            };
        }
        
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar la edición \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region Publish button
        public string AtHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada la edición \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada la edición \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonClass
        {
            get
            {
                return this.AtHome ? "fa-check" : "fa-times";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Edición en portada" : "Edición fuera de portada";
            }
        }
        #endregion
        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar la edición \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar la edición \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Edición habilitado" : "Edición deshabilitado";
            }
        }

        #endregion
        #region OnMenu button
        public string OnMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar del menú la edición \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar al menú la edición \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string OnMenuButtonClass
        {
            get
            {
                return this.OnMenu ? "fa-check" : "fa-times";
            }
        }
        public string OnMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Edición en el menú" : "Edición fuera del menú";
            }
        }
        #endregion


        #region PagesLink
        public string PagesLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Editions/Pages/{0}?{1}",
                this.IDEdition, comebackParameters.URLEditionParameters);
        }
        public string PagesLinkTitle
        {
            get
            {
                return string.Format("Páginas de {0}", this.Title);
            }
        }
        #endregion


        #region MultimediaLink
        public string MultimediaLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Editions/Images/{0}?{1}",
                this.IDEdition, comebackParameters.URLParameters);
        }
        public string MultimediaLinkTitle
        {
            get
            {
                return string.Format("Multimedios de {0}", this.Title);
            }
        }
        #endregion

        #region ItemsLink
        public string ItemLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.IDEdition, comebackParameters.URLEditionParameters);
        }
        public string ItemLinkTitle
        {
            get
            {
                return string.Format("Ítems de {0}", this.Title);
            }
        }
        #endregion


        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Editions/Update/{0}?{1}",
                this.IDEdition, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar contenido";
            }
        }
        #endregion

    }
}