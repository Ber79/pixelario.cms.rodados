﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class ConfigurationsListAdminModel
    {
        public string Section { get; set; }
        public List<ConfigurationFormAdminModel> Configurations { get; set; }
    }

}