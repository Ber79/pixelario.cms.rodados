﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class EditionAdminModel
    {
        public int IDEdition { get; set; }
        public string Title { get; set; }
        public EditionAdminModel()
        {

        }
        public EditionAdminModel(EditionQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDEdition = queryModel.IDEdition;
            this.Title = queryModel.Title;
        }
    }
}