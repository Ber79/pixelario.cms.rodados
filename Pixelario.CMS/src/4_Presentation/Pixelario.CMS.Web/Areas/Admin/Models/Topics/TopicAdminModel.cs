﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class TopicAdminModel
    {
        public int IDTopic { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public bool OnMenu { get; set; }
        public int Order { get; set; }
        public int OrderAtMenu { get; set; }
        private TopicAdminModel()
        {
        }
        public TopicAdminModel(TopicQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDTopic = queryModel.IDTopic;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.AtHome = queryModel.AtHome;
            this.OnMenu = queryModel.OnMenu;
            this.Order = queryModel.Order;
            this.OrderAtMenu = queryModel.OrderAtMenu;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el tema \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion

        #region ToMenu button
        public string ToMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar del menú el tema \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar al menú el tema \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string ToMenuButtonClass
        {
            get
            {
                return this.OnMenu ? "fa-check" : "fa-times";
            }
        }
        public string ToMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Tema en menú" : "Tema fuera del menú";
            }
        }
        #endregion

        #region ToHome button
        public string ToHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada el tema \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada el tema \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string ToHomeButtonClass
        {
            get
            {
                return this.AtHome ? "fa-check" : "fa-times";
            }
        }
        public string ToHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Tema en portada" : "Tema fuera de portada";
            }
        }
        #endregion

        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar el tema \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar el tema \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Tema habilitado" : "Tema deshabilitado";
            }
        }

        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Topics/Update/{0}?{1}",
                this.IDTopic, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar tema";
            }
        }
        #endregion
        #region MultimediaLink
        public string MultimediaLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Topics/Images/{0}?{1}",
                this.IDTopic, comebackParameters.URLParameters);
        }
        public string MultimediaLinkTitle
        {
            get
            {
                return string.Format("Multimedios de {0}", this.Title);
            }
        }
        #endregion
        #region ItemsLink
        public string ItemLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/SubTopics/Index/{0}?{1}",
                this.IDTopic, comebackParameters.URLTopicParameters);
        }
        public string ItemLinkTitle
        {
            get
            {
                return string.Format("Ítems de {0}", this.Title);
            }
        }
        #endregion

    }
}