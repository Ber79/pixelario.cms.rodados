﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class ComebackParametersViewModel
    {
        public int? IDTopic { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDTopic,
            int pageNumber,
            int pageSize,
            string columns) : this(pageNumber, pageSize, columns)
        {
            this.IDTopic = iDTopic;
        }       
       
        public ComebackParametersViewModel(int pageNumber,
            int pageSize, string columns)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Columns = columns;
        }
        public bool HasIDTopic
        {
            get
            {
                return this.IDTopic.HasValue;
            }
        }
        public bool HasColumns
        {
            get
            {
                return !string.IsNullOrEmpty(this.Columns);
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.PageNumber, this.PageSize,
                    this.HasColumns ?
                        string.Format("&columns={0}", this.Columns) :
                        "");
            }
        }
        public string URLTopicParameters
        {
            get
            {
                return string.Format("topicPageNumber={0}&topicPageSize={1}{2}",
                    this.PageNumber, this.PageSize,
                    this.HasColumns ?
                        string.Format("&topicColumns={0}", this.Columns) :
                        "");
            }
        }
    }
}