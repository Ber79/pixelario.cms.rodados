﻿using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDTopic { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public List<EditionAdminModel> AvailableEditions { get; set; }
        public List<int> Editions { get; set; }
        public string HomeImage { get; set; }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }

        public void SetAvailableEditions(List<EditionQueryModel> availableEditions)
        {
            this.AvailableEditions = new List<EditionAdminModel>();
            foreach (var queryModel in availableEditions)
            {
                this.AvailableEditions.Add(new EditionAdminModel(
                    queryModel: queryModel));
            }
        }
        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
           int? pageNumber,
           int? pageSize,
           string columns)
        {
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
        }
        public FormViewModel(string mode,
            List<EditionQueryModel> availableEditions,
            List<int> editions,
            int? pageNumber,
            int? pageSize,
            string columns) : this(mode,
                pageNumber, pageSize,
                columns)
        {
            this.Editions = editions;
            this.SetAvailableEditions(availableEditions);
        }
        public FormViewModel(
            string mode,
            TopicQueryModel topicQueryModel,
            int? pageNumber,
            int? pageSize,
            string columns) : this(mode,
                pageNumber, pageSize, columns)
        {
            IDTopic = topicQueryModel.IDTopic;
            Title = topicQueryModel.Title;
            Summary = topicQueryModel.Summary;
            Keywords = topicQueryModel.Keywords;
            HomeImage = topicQueryModel.HomeImage;
        }
        public FormViewModel(
            string mode,
            List<EditionQueryModel> availableEditions,
            List<int> editions,
            int iDTopic,
            string title,
            string summary,
            string keywords,
            int? pageNumber,
            int? pageSize,
            string columns) : this(mode, availableEditions, editions,
                pageNumber, pageSize, columns)
        {
            IDTopic = iDTopic;
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDTopic.HasValue)
                    return new ComebackParametersViewModel(
                        iDTopic: this.IDTopic.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Topics?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de temas";
            }
        }
        #endregion
        public AddTopicCommand ToAddTopicCommand(string createdBy)
        {
            if (Editions == null)
                throw new Exception("El tema debe estar asignado al menos una edición.");
            var command = new AddTopicCommand(
                title: Title,
                summary: Summary,
                keywords: Keywords,
                editions: Editions);
            command.SetCreatedBy(createdBy: createdBy);
            return command;
        }
        public UpdateTopicCommand ToUpdateTopicCommand()
        {
            if (!IDTopic.HasValue)
                throw new Exception("No se envio el Id del tema.");
            if (Editions == null)
                throw new Exception("El tema debe estar asignado al menos una edición.");
            return new UpdateTopicCommand(
                    iDTopic: IDTopic.Value,
                    title: Title,
                    summary: Summary,
                    keywords: Keywords,
                    editions: Editions);
        }
    }
}