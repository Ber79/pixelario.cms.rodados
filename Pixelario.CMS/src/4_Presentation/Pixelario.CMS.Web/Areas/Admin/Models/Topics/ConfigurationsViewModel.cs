﻿using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class ConfigurationsViewModel : PagedContentViewModel
    {
        public string Module { get; set; }        
        public List<ConfigurationsListAdminModel> ConfigurationsLists { get; set; }
        private ConfigurationsViewModel()
        {

        }
        public ConfigurationsViewModel(
           string module, List<ConfigurationsListAdminModel> configurationsLists,
           int? pageNumber,
           int? pageSize,
           string columns)
        {
            this.Module = module;
            this.ConfigurationsLists = configurationsLists;
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);

        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns);
            }
        }

        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Topics?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de contenidos";
            }
        }

    }
}