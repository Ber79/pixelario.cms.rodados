﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Topics
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<TopicAdminModel> Topics { get; set; }
        public IndexViewModel(List<TopicQueryModel> topicQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize,
            string columns)
        {
            this.Topics = new List<TopicAdminModel>();
            foreach (var queryModel in topicQueryModels)
            {
                this.Topics.Add(new TopicAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDTopic: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Topics/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo tema";
            }
        }

        #endregion
        #region ConfigurationButton
        public string ConfigurationButtonHref
        {
            get
            {
                return string.Format("/Admin/Topics/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string ConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones";
            }
        }

        #endregion

    }
}