﻿using Pixelario.CMS.Web.Areas.Admin.Models.Configurations;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models
{
    public class ConfigurationsListViewModel : PagedContentViewModel
    {
        public string PageTitle { get; set; }
        public string ControllerName { get; set; }
        public string Module { get; set; }        
        public List<ConfigurationsListAdminModel> ConfigurationsLists { get; set; }
        public List<string> TypeOfEnumerations { get; set; }
    }
}