﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions
{
    public class ComebackParametersViewModel
    {
        public int? IDConfigurationOption { get; set; }
        public int IDConfiguration { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int ConfigurationPageNumber { get; set; }
        public int ConfigurationPageSize { get; set; }
        public string ConfigurationSearchText { get; set; }
        public int ConfigurationType { get; set; }
        public int ConfigurationOrderBy { get; set; }
        public int ConfigurationOrderDirection { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDConfigurationOption,
            int iDConfiguration,
            int pageNumber, int pageSize, string configurationSearchText,
            int configurationPageNumber, int configurationPageSize,
            int configurationType, int configurationOrderBy,
            int configurationOrderDirection) :
            this(iDConfiguration,
                pageNumber, pageSize, configurationSearchText,
                configurationPageNumber, configurationPageSize,
                configurationType, configurationOrderBy,
                configurationOrderDirection)
        {
            this.IDConfigurationOption = iDConfigurationOption;
        }
        public ComebackParametersViewModel(int iDConfiguration,
            int pageNumber, int pageSize, string configurationSearchText,
            int configurationPageNumber, int configurationPageSize,
            int configurationType, int configurationOrderBy,
            int configurationOrderDirection)
        {
            this.IDConfiguration = iDConfiguration;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.ConfigurationPageNumber = configurationPageNumber;
            this.ConfigurationPageSize = configurationPageSize;
            this.ConfigurationSearchText = configurationSearchText;
            this.ConfigurationType = configurationType;
            this.ConfigurationOrderBy = configurationOrderBy;
            this.ConfigurationOrderDirection = configurationOrderDirection;
        }       
       


        public bool HasIDConfigurationOption
        {
            get
            {
                return this.IDConfigurationOption.HasValue;
            }
        }       
        
        public string URLBackParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}&searchText={2}&configurationType={3}&orderBy={4}&orderDirection={5}",
                    this.ConfigurationPageNumber, this.ConfigurationPageSize,
                    this.ConfigurationSearchText, this.ConfigurationType,
                    this.ConfigurationOrderBy, this.ConfigurationOrderDirection
                    );
            }
        }
        public string URLConfigurationParameters
        {
            get
            {
                return string.Format("configurationPageNumber={0}&configurationPageSize={1}&configurationSearchText={2}&configurationType={3}&configurationOrderBy={4}&configurationOrderDirection={5}&pageNumber={6}&pageSize={7}",
                    this.ConfigurationPageNumber, this.ConfigurationPageSize,
                    this.ConfigurationSearchText, this.ConfigurationType,
                    this.ConfigurationOrderBy, this.ConfigurationOrderDirection,
                    this.PageNumber, this.PageSize);
            }
        }
    }
}