﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<ConfigurationOptionAdminModel> ConfigurationOptions { get; set; }
        public ConfigurationAdminModel Configuration { get; set; }
        public int? ConfigurationPageNumber { get; set; }
        public int? ConfigurationPageSize { get; set; }
        public string ConfigurationSearchText { get; set; }
        public int? ConfigurationType { get; set; }
        public int? ConfigurationOrderBy { get; set; }
        public int? ConfigurationOrderDirection { get; set; }

        public IndexViewModel(ConfigurationQueryModel configurationQueryModel,
            List<ConfigurationOptionQueryModel> configurationOptionQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize, 
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize, 
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection)
        {
            this.Configuration = new ConfigurationAdminModel(
               queryModel: configurationQueryModel);
            this.ConfigurationOptions = new List<ConfigurationOptionAdminModel>();
            if (configurationOptionQueryModels != null)
            {
                foreach (var queryModel in configurationOptionQueryModels)
                {
                    this.ConfigurationOptions.Add(new ConfigurationOptionAdminModel(
                        queryModel: queryModel));
                }
            }
            this.ConfigurationPageNumber = configurationPageNumber;
            this.ConfigurationPageSize = configurationPageSize;
            this.ConfigurationSearchText = configurationSearchText;
            this.ConfigurationType = configurationType;
            this.ConfigurationOrderBy = configurationOrderBy;
            this.ConfigurationOrderDirection = configurationOrderDirection;
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    iDConfiguration: this.Configuration.IDConfiguration,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    configurationPageNumber: this.ConfigurationPageNumber ?? 1,
                    configurationPageSize: this.ConfigurationPageSize ?? 10,
                    configurationSearchText: this.ConfigurationSearchText,
                    configurationType: this.ConfigurationType ?? 0,
                    configurationOrderBy: this.ConfigurationOrderBy ?? 0,
                    configurationOrderDirection: this.ConfigurationOrderDirection ?? 0
                    );
            }
        }

        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDConfigurationOption: id,
                    iDConfiguration: this.Configuration.IDConfiguration,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    configurationPageNumber: this.ConfigurationPageNumber ?? 1,
                    configurationPageSize: this.ConfigurationPageSize ?? 10,
                    configurationSearchText: this.ConfigurationSearchText,
                    configurationType: this.ConfigurationType ?? 0,
                    configurationOrderBy: this.ConfigurationOrderBy ?? 0,
                    configurationOrderDirection: this.ConfigurationOrderDirection ?? 0);
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Configurations?{0}",
                    this.ComebackParameters.URLBackParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de configuraciones";
            }
        }

        #endregion
        #region CreatePageButton

        public string CreatePageButtonLink
        {
            get
            {
                return string.Format("/Admin/ConfigurationOptions/Create/{0}?{1}",
                    this.Configuration.IDConfiguration,
                    this.ComebackParameters.URLConfigurationParameters);
            }
        }
        public string CreatePageButtonTitle
        {
            get
            {
                return string.Format("Nuevo opción de {0}",
                    this.Configuration.Title);
            }
        }

        #endregion

    }
}