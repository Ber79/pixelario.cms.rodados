﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions
{
    public class ConfigurationAdminModel
    {
        public int IDConfiguration { get; set; }
        public string Title { get; set; }

        private ConfigurationAdminModel()
        {
        }
        public ConfigurationAdminModel(ConfigurationQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDConfiguration = queryModel.IDConfiguration;
            this.Title = queryModel.Title;
        }
    }
}