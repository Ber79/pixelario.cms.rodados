﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions
{
    public class ConfigurationOptionAdminModel
    {
        public int IDConfigurationOption { get; set; }
        public string Value { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }
        private ConfigurationOptionAdminModel()
        {
        }
        public ConfigurationOptionAdminModel(ConfigurationOptionQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDConfigurationOption = queryModel.IDConfigurationOption;
            this.Value = queryModel.Value;
            this.Enabled = queryModel.Enabled;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar la opción \"{0}\"?') == false) return false;", this.Value);
            }
        }
        #endregion



        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar la opción \"{0}\"?') == false) return false;", this.Value) :
                    string.Format("if (confirm('¿Está seguro de habilitar la opción \"{0}\"?') == false) return false;", this.Value);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Opción habilitada" : "Opción deshabilitada";
            }
        }

        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/ConfigurationOptions/Update/{0}?{1}",
                this.IDConfigurationOption, comebackParameters.URLConfigurationParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar opción";
            }
        }
        #endregion

    }
}