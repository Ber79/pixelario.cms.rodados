﻿using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.QueryModels.Configurations;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDConfigurationOption { get; set; }
        public int IDConfiguration { get; set; }
        public ConfigurationAdminModel Configuration { get; set; }
        public string Value { get; set; }
        public int? ConfigurationPageNumber { get; set; }
        public int? ConfigurationPageSize { get; set; }
        public string ConfigurationSearchText { get; set; }
        public int? ConfigurationType { get; set; }
        public int? ConfigurationOrderBy { get; set; }
        public int? ConfigurationOrderDirection { get; set; }
        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection)
        {
            this.SetMode(mode);
            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
            this.ConfigurationPageNumber = configurationPageNumber;
            this.ConfigurationPageSize = configurationPageSize;
            this.ConfigurationSearchText = configurationSearchText;
            this.ConfigurationType = configurationType;
            this.ConfigurationOrderBy = configurationOrderBy;
            this.ConfigurationOrderDirection = configurationOrderDirection;
        }
        #region View model for create
        public FormViewModel(string mode,
            ConfigurationQueryModel configurationQueryModel,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection) :
                this(mode,
                    pageNumber, pageSize, configurationSearchText,
                    configurationPageNumber, configurationPageSize,
                    configurationType, configurationOrderBy,
                    configurationOrderDirection)
        {
            this.Configuration = new ConfigurationAdminModel(
                queryModel: configurationQueryModel);
            this.IDConfiguration = configurationQueryModel.IDConfiguration;           
        }
        public FormViewModel(string mode,
            ConfigurationQueryModel configurationQueryModel,
            AddConfigurationOptionCommand command,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection) :
            this(mode, configurationQueryModel,
                pageNumber, pageSize, configurationSearchText,
                configurationPageNumber, configurationPageSize,
                configurationType, configurationOrderBy,
                configurationOrderDirection)
        {
            this.Value = command.Value;
        }

        #endregion
        #region View model for update

        public FormViewModel(string mode,
            ConfigurationOptionQueryModel configurationOptionQueryModel,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection) :
            this(mode,
                pageNumber, pageSize, configurationSearchText,
                configurationPageNumber, configurationPageSize,
                configurationType, configurationOrderBy,
                configurationOrderDirection)
        {
            this.Configuration = new ConfigurationAdminModel(
                queryModel: configurationOptionQueryModel.Configuration);
            this.IDConfiguration = configurationOptionQueryModel.Configuration.IDConfiguration;
            this.IDConfigurationOption = configurationOptionQueryModel.IDConfigurationOption;
            this.Value = configurationOptionQueryModel.Value;
        }
        public FormViewModel(string mode,
            ConfigurationQueryModel configurationQueryModel,
            UpdateConfigurationOptionCommand command,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationType,
            int? configurationOrderBy,
            int? configurationOrderDirection) :
            this(mode, configurationQueryModel,
                pageNumber, pageSize, configurationSearchText,
                configurationPageNumber, configurationPageSize,
                configurationType, configurationOrderBy,
                configurationOrderDirection)
        {
            this.IDConfigurationOption = command.IDConfigurationOption;
            this.IDConfiguration = command.IDConfiguration;
            this.Value = command.Value;
        }
        #endregion
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDConfigurationOption != null)
                {
                    return new ComebackParametersViewModel(
                        iDConfigurationOption: this.IDConfigurationOption.Value,
                        iDConfiguration: this.IDConfiguration,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        configurationPageNumber: this.ConfigurationPageNumber ?? 1,
                        configurationPageSize: this.ConfigurationPageSize ?? 10,
                        configurationSearchText: this.ConfigurationSearchText,
                        configurationType: this.ConfigurationType ?? 0,
                        configurationOrderBy: this.ConfigurationOrderBy ?? 0,
                        configurationOrderDirection: this.ConfigurationOrderDirection ?? 0
                        );
                }
                else
                {
                    return new ComebackParametersViewModel(
                        iDConfiguration: this.IDConfiguration,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        configurationPageNumber: this.ConfigurationPageNumber ?? 1,
                        configurationPageSize: this.ConfigurationPageSize ?? 10,
                        configurationSearchText: this.ConfigurationSearchText,
                        configurationType: this.ConfigurationType ?? 0,
                        configurationOrderBy: this.ConfigurationOrderBy ?? 0,
                        configurationOrderDirection: this.ConfigurationOrderDirection ?? 0);
                }
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/ConfigurationOptions/Index/{0}?{1}",
                    this.Configuration.IDConfiguration,
                    this.ComebackParameters.URLConfigurationParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return string.Format(" Listado de opciones de {0}", this.Configuration.Title);
            }
        }
        #endregion
    }
}