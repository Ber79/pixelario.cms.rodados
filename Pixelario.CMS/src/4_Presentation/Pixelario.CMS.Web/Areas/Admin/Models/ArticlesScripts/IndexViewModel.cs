﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScripts
{
    public class IndexViewModel : PagedContentViewModel
    {
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public List<ArticleScriptAdminModel> ArticleScripts { get; set; }
        public IndexViewModel(List<ArticleScriptQueryModel> articleScriptQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns)
        {
            this.ArticleScripts = new List<ArticleScriptAdminModel>();
            foreach (var queryModel in articleScriptQueryModels)
            {
                this.ArticleScripts.Add(new ArticleScriptAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDArticleScript: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/ArticleScripts/Create?{0}", 
                    this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo script de artículo";
            }
        }

        #endregion
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Editions?{0}",
                    this.ComebackParameters.URLEditionParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de contenidos";
            }
        }

        #endregion

        #region ConfigurationButton
        public string ConfigurationButtonHref
        {
            get
            {
                return string.Format("/Admin/ArticleScripts/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string ConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones";
            }
        }

        #endregion

    }
}