﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScripts
{
    public class ComebackParametersViewModel
    {
        public int? IDArticleScript { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDArticleScript,
            int pageNumber,
            int pageSize, int editionPageNumber,
            int editionPageSize,
            string editionColumns) : this(pageNumber, pageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScript = iDArticleScript;
        }       
       
        public ComebackParametersViewModel(int pageNumber,
            int pageSize, int editionPageNumber,
            int editionPageSize,
            string editionColumns)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.EditionPageNumber = editionPageNumber;
            this.EditionPageSize = editionPageSize;
            this.EditionColumns = editionColumns;
        }
        public bool HasIDArticleScript
        {
            get
            {
                return this.IDArticleScript.HasValue;
            }
        }
        public string ColumnsParameter
        {
            get
            {
                if(string.IsNullOrEmpty(this.EditionColumns))
                    return "";
                return string.Format("&editionColumns={0}", this.EditionColumns);
            }
        }
        public string EditionColumnsParameter
        {
            get
            {
                if (string.IsNullOrEmpty(this.EditionColumns))
                    return "";
                return string.Format("&columns={0}", this.EditionColumns);
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}&editionPageNumber={2}&editionPageSize={3}{4}",
                    this.PageNumber, this.PageSize,
                    this.EditionPageNumber, this.EditionPageSize,
                    this.ColumnsParameter);
            }
        }
        public string URLEditionParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.EditionPageNumber, this.EditionPageSize,
                    this.EditionColumnsParameter);
            }
        }
    }
}