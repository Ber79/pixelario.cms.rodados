﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScripts
{
    public class ArticleScriptAdminModel
    {
        public int IDArticleScript { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }
        private ArticleScriptAdminModel()
        {
        }
        public ArticleScriptAdminModel(ArticleScriptQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDArticleScript = queryModel.IDArticleScript;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el script de artículo \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion

        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar el script de artículo \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar el script de artículo \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Script de artículo habilitado" : "Script de artículo deshabilitado";
            }
        }

        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/ArticleScripts/Update/{0}?{1}",
                this.IDArticleScript, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar script de artículo";
            }
        }
        #endregion

        #region ItemsLink
        public string ItemLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/ArticleScriptItems/Index/{0}?{1}",
                this.IDArticleScript, comebackParameters.URLParameters);
        }
        public string ItemLinkTitle
        {
            get
            {
                return string.Format("Ítems de {0}", this.Title);
            }
        }
        #endregion

    }
}