﻿using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScripts
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public int? IDArticleScript { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }

        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
            int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns)
        {
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;

        }
        public FormViewModel(
            string mode,
            ArticleScriptQueryModel articleScriptQueryModel,
            int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode,
                pageNumber, pageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScript = articleScriptQueryModel.IDArticleScript;
            Title = articleScriptQueryModel.Title;
            Summary = articleScriptQueryModel.Summary;
        }
        public FormViewModel(
            string mode,
            AddScriptCommand command,
            int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode,
                pageNumber, pageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            Title = command.Title;
            Summary = command.Summary;
        }
        public FormViewModel(
            string mode,
            UpdateScriptCommand command,
            int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode,
                pageNumber, pageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScript = command.IDArticleScript;
            this.Title = command.Title;
            this.Summary = command.Summary;
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDArticleScript.HasValue)
                    return new ComebackParametersViewModel(
                        iDArticleScript: this.IDArticleScript.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/ArticleScripts?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de scripts de artículos";
            }
        }
        #endregion
    }
}