﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Configurations
{
    public class ConfigurationAdminModel
    {
        public int IDConfiguration { get; set; }
        public string Title { get; set; }
        public string ConfigurationType { get; set; }
        public string Key { get; set; }
        private ConfigurationAdminModel()
        {
        }
        public ConfigurationAdminModel(ConfigurationQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDConfiguration = queryModel.IDConfiguration;
            this.Title = queryModel.Title;
            this.ConfigurationType = queryModel.ConfigurationType.ToString();
            this.Key = queryModel.Key;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el flyer \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region RolesLink
        public string RolesLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Configurations/Roles/{0}?{1}",
                this.IDConfiguration, comebackParameters.URLParameters);
        }
        public string RolesLinkTitle
        {
            get
            {
                return "Editar roles de la configuración";
            }
        }
        #endregion
        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Configurations/Update/{0}?{1}",
                this.IDConfiguration, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar configuración";
            }
        }
        #endregion
        #region ItemsLink
        public string ItemLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/ConfigurationOptions/Index/{0}?{1}",
                this.IDConfiguration, comebackParameters.URLConfigurationParameters);
        }
        public string ItemLinkTitle
        {
            get
            {
                return string.Format("Opciones de {0}", this.Title);
            }
        }
        #endregion

    }
}