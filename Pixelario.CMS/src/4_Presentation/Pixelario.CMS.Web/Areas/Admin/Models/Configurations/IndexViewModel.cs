﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Configurations
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<ConfigurationAdminModel> Configurations { get; set; }
        public string SearchText { get; set; }
        public int ConfigurationType { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public List<SelectListItem> ConfigurationTypesDropdownListOptions { get; private set; }
        public List<SelectListItem> PageSizesDropdownListOptions { get; private set; }
        public List<SelectListItem> OrderDirectionDropdownListOptions { get; private set; }
        public List<SelectListItem> OrderByDropdownListOptions { get; private set; }

        private IndexViewModel()
        {

        }
        public IndexViewModel(List<ConfigurationQueryModel> configurationQueryModels,
            string searchText, int? configurationType,
            int totalCount, int pageNumber,
            int pageSize, int orderBy,
            int orderDirection, int pagerItems)
        {
            this.Configurations = new List<ConfigurationAdminModel>();
            foreach (var queryModel in configurationQueryModels)
            {
                this.Configurations.Add(new ConfigurationAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.SearchText = searchText;
            this.ConfigurationType = configurationType ?? -1;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
            this.CalculatePagerItems(pagerItems);
            this.SetConfigurationTypesDropdownListOptions();
            this.SetPageSizesDropdownListOptions();
            this.SetOrderDirectionDropdownListOptions();
            this.SetOrderByDropdownListOptions();
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    searchText: this.SearchText,
                    configurationType: this.ConfigurationType,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDConfiguration: id,
                    searchText: this.SearchText,
                    configurationType: this.ConfigurationType,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
        }
        public ComebackParametersViewModel GetComebackParametersForPage(int pageNumber)
        {
            return new ComebackParametersViewModel(
                    searchText: this.SearchText,
                    configurationType: this.ConfigurationType,
                    pageNumber: pageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Configurations/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nueva configuración";
            }
        }

        #endregion

        #region FirstPageLink
        public string FirstPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(1);
            return string.Format("/Admin/Configurations/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string FirstPageTitle
        {
            get
            {
                return "Primera página";
            }
        }
        #endregion
        #region ToPageLink
        public string ToPageURL(int pageNumber)
        {
            var comebackParameters = this.GetComebackParametersForPage(pageNumber);
            return string.Format("/Admin/Configurations/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string ToPageTitle(int pageNumber)
        {

            return string.Format("Página {0}", pageNumber);

        }
        #endregion


        #region BackPageLink
        public string BackPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.BackPage);
            return string.Format("/Admin/Configurations/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string BackPageTitle
        {
            get
            {
                return "Anterior página";
            }
        }
        #endregion
        #region NextPageLink
        public string NextPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.NextPage);
            return string.Format("/Admin/Configurations/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string NextPageTitle
        {
            get
            {
                return "Siguiente página";
            }
        }
        #endregion
        #region LastPageLink
        public string LastPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.PageCount);
            return string.Format("/Admin/Configurations/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string LastPageTitle
        {
            get
            {
                return "Última página";
            }
        }
        #endregion

        public void SetConfigurationTypesDropdownListOptions()
        {
            this.ConfigurationTypesDropdownListOptions = new List<SelectListItem>(){
                new SelectListItem() {
                    Value = null,
                    Text = "Seleccione un tipo"
            }};
            foreach (var configurationType in Enum.GetValues(typeof(ConfigurationType)).Cast<ConfigurationType>())
            {
                this.ConfigurationTypesDropdownListOptions.Add(new SelectListItem()
                {
                    Value = ((int)configurationType).ToString(),
                    Text = Enum.GetName(typeof(ConfigurationType), configurationType)
                });
            }
        }
        public void SetPageSizesDropdownListOptions()
        {
            this.PageSizesDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="5",
                    Text = "5"
                },
                new SelectListItem() {
                    Value="10",
                    Text = "10"
                },
                new SelectListItem() {
                    Value="25",
                    Text = "25"
                },
                new SelectListItem() {
                    Value="100",
                    Text = "100"
                }
            };
        }
        public void SetOrderDirectionDropdownListOptions()
        {
            this.OrderDirectionDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="0",
                    Text = "Ascendente"
                },
                new SelectListItem() {
                    Value="1",
                    Text = "Descendente"
                }
            };
        }

        public void SetOrderByDropdownListOptions()
        {
            this.OrderByDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="0",
                    Text = "Fecha de creación"
                },
                new SelectListItem() {
                    Value="1",
                    Text = "Título"
            }};
        }
    }
}