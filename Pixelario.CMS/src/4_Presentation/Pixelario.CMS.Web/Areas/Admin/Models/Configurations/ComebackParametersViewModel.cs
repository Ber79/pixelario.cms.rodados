﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Configurations
{
    public class ComebackParametersViewModel
    {
        public int? IDConfiguration { get; set; }
        public string SearchText { get; set; }
        public int ConfigurationType { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDConfiguration,
            string searchText, int configurationType,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection
            ) : this(searchText, configurationType, 
                pageNumber, pageSize, orderBy,
                orderDirection)
        {
            this.IDConfiguration = iDConfiguration;
        }       
       
        public ComebackParametersViewModel(
            string searchText, int configurationType, 
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection)
        {
            this.SearchText = searchText;
            this.ConfigurationType = configurationType;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
        }
        public bool HasIDConfiguration
        {
            get
            {
                return this.IDConfiguration.HasValue;
            }
        }
       
        public string URLParameters
        {
            get
            {
                return string.Format("searchText={0}&configurationType={1}&pageNumber={2}&pageSize={3}&orderBy={4}&orderDirection={5}",
                    this.SearchText, this.ConfigurationType,
                    this.PageNumber, this.PageSize,
                    this.OrderBy, this.OrderDirection);
            }
        }
        public string URLConfigurationParameters
        {
            get
            {
                return string.Format("ConfigurationSearchText={0}&configurationType={1}&conFigurationPageNumber={2}&configurationPageSize={3}&configurationOrderBy={4}&configurationOrderDirection={5}",
                    this.SearchText, this.ConfigurationType,
                    this.PageNumber, this.PageSize,
                    this.OrderBy, this.OrderDirection);
            }
        }
    }
}