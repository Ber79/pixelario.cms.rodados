﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.Identity.Domain;
using System.Linq;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Configurations
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDConfiguration { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public string State { get; set; }
        public ConfigurationType ConfigurationType { get; set; }
        public List<ConfigurationType> ConfigurationTypes { get; set; }
        public IList<string> ConfigurationRoles { get; set; }
        public string[] Roles { get; set; }

        public string SearchText { get; set; }
        public int ConfigurationTypeParameter { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }


        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
            string searchText, int? configurationTypeParameter,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection)
        {
            this.SetMode(mode);
            this.SearchText = searchText;
            this.ConfigurationTypeParameter = configurationTypeParameter ?? -1;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
        }
        public FormViewModel(
            string mode,
            ConfigurationQueryModel configurationQueryModel,
            string searchText, int? configurationTypeParameter,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection
            ) : this(mode, searchText, configurationTypeParameter,
                pageNumber, pageSize, orderBy, orderDirection)
        {
            this.IDConfiguration = configurationQueryModel.IDConfiguration;
            this.Title = configurationQueryModel.Title;
            this.Key = configurationQueryModel.Key;
            this.State = configurationQueryModel.State;
            this.ConfigurationType = configurationQueryModel.ConfigurationType;
        }
        public FormViewModel(
            string mode,
            ConfigurationQueryModel configurationQueryModel,
            List<ApplicationRole> applicationRoles,
            string searchText, int? configurationTypeParameter,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection
            ) : this(mode, searchText, configurationTypeParameter,
                pageNumber, pageSize, orderBy, orderDirection)
        {
            this.IDConfiguration = configurationQueryModel.IDConfiguration;
            this.Title = configurationQueryModel.Title;
            this.ConfigurationRoles = configurationQueryModel.GetBindedRoles();
            this.SetRoles(
                applicationRoles: applicationRoles);
        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDConfiguration.HasValue)
                    return new ComebackParametersViewModel(
                        iDConfiguration: this.IDConfiguration.Value,
                        searchText: this.SearchText,
                        configurationType: this.ConfigurationTypeParameter,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection);
                else
                    return new ComebackParametersViewModel(
                        searchText: this.SearchText,
                        configurationType: this.ConfigurationTypeParameter,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Configurations?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de configuraciones";
            }
        }
        #endregion


        public AddCommand ToAddCommand(string createdBy)
        {
            var command = new AddCommand(
                title: Title,
                key: Key,
                configurationType: ConfigurationType);
            command.SetCreatedBy(
                createdBy: createdBy);
            return command;
        }
        public UpdateCommand ToUpdateCommand(string solicitedBy)
        {
            if (!IDConfiguration.HasValue)
                throw new Exception("No se envio el id de la configuración.");
            var command = new UpdateCommand(
                    iDConfiguration: IDConfiguration.Value,
                    title: Title,
                    key: Key,
                    configurationType: ConfigurationType);
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;
        }
        public BindRolesCommand ToBindRolesCommand(string solicitedBy)
        {
            if (!this.IDConfiguration.HasValue)
                throw new ArgumentException("No ingreso el identificador de la configuración.");

            var command = new BindRolesCommand(
                iDConfiguration: this.IDConfiguration.Value,
                roles: this.Roles ?? new string[] { });
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;

        }
        public void SetRoles(List<ApplicationRole> applicationRoles)
        {
            this.Roles = applicationRoles.Where(r => r.Name != "Super Administrador")
                .Select(r => r.Name).ToArray();
        }
    }
}