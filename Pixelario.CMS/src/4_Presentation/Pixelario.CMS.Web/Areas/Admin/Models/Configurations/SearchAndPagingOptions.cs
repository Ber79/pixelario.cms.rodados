﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Configurations
{
    public class SearchAndPagingOptions
    {
        public string SearchText { get; set; }
        public int? ConfigurationType { get; set; }        
        public string OrderBy { get; set; }
        public int OrderByValue
        {
            get
            {
                int orderByParsed = 0;
                if (!string.IsNullOrEmpty(this.OrderBy) && int.TryParse(this.OrderBy, out orderByParsed)
                    && orderByParsed > 0)
                {
                    return orderByParsed;
                }
                else
                    return 0;
            }
        }
        public string OrderDirection { get; set; }

        public override string ToString()
        {
            var segment = $"searchText={SearchText}&orderBy={OrderBy}&orderDirection={OrderDirection}";
            if (ConfigurationType.HasValue)
                segment += $"&configurationType={ConfigurationType.Value}";
            return segment;
        }
    }
}