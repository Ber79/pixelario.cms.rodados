﻿using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.CacheRegisters
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<CacheRegisterAdminModel> CacheRegisters { get; set; }
        private IndexViewModel()
        {

        }
        public IndexViewModel(List<KeyValuePair<string, object>> cacheRegisterKeyValues,
            int totalCount, int pageNumber,
            int pageSize, int pagerItems)
        {
            this.CacheRegisters = new List<CacheRegisterAdminModel>();
            foreach (var keyValuePair in cacheRegisterKeyValues)
            {
                this.CacheRegisters.Add(new CacheRegisterAdminModel(
                    keyValuePair: keyValuePair));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.CalculatePagerItems(pagerItems);
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDLogEvent: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
        }
        public ComebackParametersViewModel GetComebackParametersForPage(int pageNumber)
        {
            return new ComebackParametersViewModel(
                    pageNumber: pageNumber,
                    pageSize: this.PageSize);
        }

        #region FirstPageLink
        public string FirstPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(1);
            return string.Format("/Admin/CacheRegisters/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string FirstPageTitle
        {
            get
            {
                return "Primera página";
            }
        }
        #endregion
        #region ToPageLink
        public string ToPageURL(int pageNumber)
        {
            var comebackParameters = this.GetComebackParametersForPage(pageNumber);
            return string.Format("/Admin/CacheRegisters/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string ToPageTitle(int pageNumber)
        {

            return string.Format("Página {0}", pageNumber);

        }
        #endregion


        #region BackPageLink
        public string BackPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.BackPage);
            return string.Format("/Admin/CacheRegisters/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string BackPageTitle
        {
            get
            {
                return "Anterior página";
            }
        }
        #endregion
        #region NextPageLink
        public string NextPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.NextPage);
            return string.Format("/Admin/CacheRegisters/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string NextPageTitle
        {
            get
            {
                return "Siguiente página";
            }
        }
        #endregion
        #region LastPageLink
        public string LastPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.PageCount);
            return string.Format("/Admin/CacheRegisters/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string LastPageTitle
        {
            get
            {
                return "Última página";
            }
        }
        #endregion
    }
}