﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Pixelario.CMS.Web.Areas.Admin.Models.CacheRegisters
{
    public class CacheRegisterAdminModel
    {
        public string Key { get; set; }
        private CacheRegisterAdminModel()
        {
        }
        public CacheRegisterAdminModel(KeyValuePair<string, object> keyValuePair)
        {
            this.Key = keyValuePair.Key;
        }
    }
}