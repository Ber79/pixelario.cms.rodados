﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.CacheRegisters
{
    public class ComebackParametersViewModel
    {
        public int? IDLogEvent { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDLogEvent,
            int pageNumber,
            int pageSize
            ) : this(pageNumber, pageSize)
        {
            this.IDLogEvent = iDLogEvent;
        }       
       
        public ComebackParametersViewModel(
            int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        public bool HasIDLogEvent
        {
            get
            {
                return this.IDLogEvent.HasValue;
            }
        }
       
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}",
                    this.PageNumber, this.PageSize);
            }
        }
    }
}