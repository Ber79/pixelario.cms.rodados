﻿using Pixelario.CMS.Domain.AggregatesModel.Advertisments;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Advertisments
{
    public class AdvertismentPlaceAtWebAdminModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        private AdvertismentPlaceAtWebAdminModel()
        {

        }
        public AdvertismentPlaceAtWebAdminModel(AdvertismentPlaceAtWeb placeAtWeb)
        {
            this.Id = placeAtWeb.Id;
            this.Title = placeAtWeb.Title;
        }
    }
}