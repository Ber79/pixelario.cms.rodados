﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Advertisments
{
    public class ComebackParametersViewModel
    {
        public int? IDAdvertisment { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDAdvertisment,
            int pageNumber,
            int pageSize
            ) : this(pageNumber, pageSize)
        {
            this.IDAdvertisment = iDAdvertisment;
        }       
       
        public ComebackParametersViewModel(int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        public bool HasIDAdvertisment
        {
            get
            {
                return this.IDAdvertisment.HasValue;
            }
        }
       
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}",
                    this.PageNumber, this.PageSize);
            }
        }
    }
}