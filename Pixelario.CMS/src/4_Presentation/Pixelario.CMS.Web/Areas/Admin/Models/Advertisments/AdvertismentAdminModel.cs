﻿using Pixelario.CMS.Application.QueryModels.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Advertisments
{
    public class AdvertismentAdminModel
    {
        public int IDAdvertisment { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool Published { get; set; }
        public int Order { get; set; }
        public AdvertismentPlaceAtWeb PlaceAtWeb { get; set; }
        private AdvertismentAdminModel()
        {
        }
        public AdvertismentAdminModel(AdvertismentQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDAdvertisment = queryModel.IDAdvertisment;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.Published = queryModel.Published;
            this.PlaceAtWeb = queryModel.PlaceAtWeb;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el flyer \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region Publish button
        public string PublishButtonOnClick
        {
            get
            {
                return this.Published ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada el flyer \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada el flyer \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string PublishButtonClass
        {
            get
            {
                return this.Published ? "fa-check" : "fa-times";
            }
        }
        public string PublishButtonTitle
        {
            get
            {
                return this.Published ? "Flyer en portada" : "Flyer fuera de portada";
            }
        }
        
        
        #endregion
        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar el flyer \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar el flyer \"{0}\"?') == false) return false;", this.Title);
            }
        }
 
        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Flyer habilitado" : "Flyer deshabilitado";
            }
        }


        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Advertisments/Update/{0}?{1}",
                this.IDAdvertisment, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar flyer";
            }
        }
        #endregion
        #region MultimediaLink
        public string MultimediaLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Advertisments/Images/{0}?{1}",
                this.IDAdvertisment, comebackParameters.URLParameters);
        }
        public string MultimediaLinkTitle
        {
            get
            {
                return string.Format("Multimedios de {0}", this.Title);
            }
        }
        #endregion

    }
}