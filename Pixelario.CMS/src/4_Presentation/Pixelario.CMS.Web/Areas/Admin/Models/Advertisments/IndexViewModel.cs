﻿using Pixelario.CMS.Application.QueryModels.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Advertisments
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<AdvertismentAdminModel> Advertisments { get; set; }
        public List<AdvertismentPlaceAtWebAdminModel> PlacesAtWeb { get; set; }
        public IndexViewModel(List<AdvertismentQueryModel> advertismentQueryModel,
            int totalCount, int? pageNumber,
            int? pageSize)
        {
            this.Advertisments = new List<AdvertismentAdminModel>();
            foreach (var queryModel in advertismentQueryModel)
            {
                this.Advertisments.Add(new AdvertismentAdminModel(queryModel));
            }
            this.PlacesAtWeb = new List<AdvertismentPlaceAtWebAdminModel>();
            foreach (var placeAtWeb in AdvertismentPlaceAtWeb.List())
            {
                this.PlacesAtWeb.Add(
                    new AdvertismentPlaceAtWebAdminModel(placeAtWeb));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDAdvertisment: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Advertisments/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo flyer";
            }
        }

        #endregion

    }
}