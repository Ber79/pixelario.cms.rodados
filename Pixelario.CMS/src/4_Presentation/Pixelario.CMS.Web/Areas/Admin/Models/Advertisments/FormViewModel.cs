﻿using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.QueryModels.Advertisments;
using System;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Advertisments
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDAdvertisment { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string URL { get; set; }
        public string HomeImage { get; set; }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }

        public FormViewModel()
        {

        }
        public FormViewModel(string mode, int? pageNumber,
           int? pageSize)
        {
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }
        public FormViewModel(
            string mode, AdvertismentQueryModel advertismentQueryModel,
            int? pageNumber,
            int? pageSize) : this(mode, pageNumber, pageSize)
        {
            IDAdvertisment = advertismentQueryModel.IDAdvertisment;
            Title = advertismentQueryModel.Title;
            Code = advertismentQueryModel.Code;
            URL = advertismentQueryModel.URL;
            HomeImage = advertismentQueryModel.HomeImage;
        }
        public FormViewModel(
            string mode, UpdateCommand updateCommand,
            int? pageNumber,
            int? pageSize) : this(mode, pageNumber, pageSize)
        {
            IDAdvertisment = updateCommand.IDAdvertisment;
            Title = updateCommand.Title;
            Code = updateCommand.Code;
            URL = updateCommand.URL;
        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDAdvertisment.HasValue)
                    return new ComebackParametersViewModel(
                        iDAdvertisment: this.IDAdvertisment.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
            }
        }


        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Advertisments?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de flyers";
            }
        }
        #endregion


        public AddCommand ToAddCommand(string createdBy)
        {
            var command = new AddCommand(
                title: Title,
                code: Code, 
                url: URL);
            command.SetCreatedBy(
                createdBy: createdBy);
            return command;
        }
        public UpdateCommand ToUpdateCommand(string solicitedBy)
        {
            if (!IDAdvertisment.HasValue)
                throw new Exception("No se envio el id del flyer.");
            var command =  new UpdateCommand(
                    iDAdvertisment: IDAdvertisment.Value,
                    title: Title,
                    code: Code,
                    url: URL);
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;
        }
    }
}