﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System.Collections.Generic;
using System.Configuration;

namespace Pixelario.CMS.Web.Areas.Admin.Models
{
    public class BaseViewModel
    {
        public string AppName
        {
            get
            {
                return ConfigurationManager.AppSettings["app:name"];
            }
        }
        public string AppDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["app:domain"];
            }
        }
        public string AppVersion
        {
            get
            {
                return ConfigurationManager.AppSettings["app:version"];
            }
        }
        public bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }

    }
}