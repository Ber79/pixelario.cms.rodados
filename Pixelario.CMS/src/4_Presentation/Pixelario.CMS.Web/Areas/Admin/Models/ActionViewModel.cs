﻿namespace Pixelario.CMS.Web.Areas.Admin.Models
{
    public class ActionViewModel
    {
        public int? Id { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
    }
}