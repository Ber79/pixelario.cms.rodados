﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Roles
{
    public class ComebackParametersViewModel
    {
        public int? RoleId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int roleId,
            int pageNumber,
            int pageSize) : this(pageNumber, pageSize)
        {
            this.RoleId = roleId;
        }       
       
        public ComebackParametersViewModel(int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        public bool HasRoleId
        {
            get
            {
                return this.RoleId.HasValue;
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}",
                    this.PageNumber, this.PageSize);
            }
        }        
    }
}