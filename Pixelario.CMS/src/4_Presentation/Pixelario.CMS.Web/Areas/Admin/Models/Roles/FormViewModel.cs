﻿using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Domain;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Roles
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public string RoleName { get; set; }
        public int? RoleId { get; set; }
        public FormViewModel(string mode,
            int? pageNumber,
            int? pageSize)
        {
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);

        }
        public FormViewModel(AddCommand command, int? pageNumber,
            int? pageSize)
        {
            this.RoleName = command.RoleName;
            this.SetMode("create");
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }
        public FormViewModel(string mode,
            ApplicationRole applicationRole,
            int? pageNumber,
            int? pageSize) : this(mode, pageNumber, pageSize)
        {
            this.RoleId = applicationRole.Id;
            this.RoleName = applicationRole.Name;
        }
        public FormViewModel(UpdateCommand command, int? pageNumber,
           int? pageSize)
        {
            this.RoleId = command.RoleId;
            this.RoleName = command.RoleName;
            this.SetMode("update");
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.RoleId.HasValue)
                    return new ComebackParametersViewModel(
                        roleId: this.RoleId.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Roles?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de roles";
            }
        }
        #endregion

    }
}