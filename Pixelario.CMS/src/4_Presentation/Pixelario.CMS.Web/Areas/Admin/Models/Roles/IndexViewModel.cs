﻿using Pixelario.Identity.Domain;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Roles
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<RoleAdminModel> Roles { get; set; }
        public IndexViewModel(List<ApplicationRole> applicationRoles,
            int totalCount, int? pageNumber,
            int? pageSize)
        {
            this.Roles = new List<RoleAdminModel>();
            foreach (var applicationRole in applicationRoles)
            {
                this.Roles.Add(new RoleAdminModel(
                    queryModel: applicationRole));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);

        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    roleId: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Roles/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo role";
            }
        }

        #endregion

    }
}