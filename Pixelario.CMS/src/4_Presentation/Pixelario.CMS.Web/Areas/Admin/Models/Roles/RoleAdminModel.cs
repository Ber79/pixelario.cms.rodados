﻿using Pixelario.Identity.Domain;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Roles
{
    public class RoleAdminModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public RoleAdminModel()
        {

        }
        public RoleAdminModel(ApplicationRole queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.Id = queryModel.Id;
            this.Name = queryModel.Name;
        }

        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de borrar el role \"{0}\"?') == false) return false;", this.Name);
            }
        }
        #endregion
        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Roles/Update/{0}?{1}",
                this.Id, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar role";
            }
        }
        #endregion

    }
}