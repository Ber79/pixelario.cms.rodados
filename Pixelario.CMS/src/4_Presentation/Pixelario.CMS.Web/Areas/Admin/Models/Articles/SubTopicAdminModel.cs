﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class SubTopicAdminModel
    {
        public int IDSubTopic { get; set; }
        public string Title { get; set; }
        private SubTopicAdminModel()
        {
        }
        public SubTopicAdminModel(SubTopicQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDSubTopic = queryModel.IDSubTopic;
            this.Title = queryModel.Title;
        }
    }
}