﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class IndexViewModel : PagedContentViewModel
    {
        public string SearchText { get; set; }
        public string PublicationDate { get; set; }
        public string Enable { get; set; }
        public string AtHome { get; set; }
        public ArticlePlaceAtHome PlaceAtHome { get; set; }
        public string OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public List<ArticleAdminModel> Articles { get; set; }
        public EditionAdminModel Edition { get; set; }
        public List<ArticlePlaceAtHome> PlacesAtHome { get; set; }
        public List<SelectListItem> YesNoDropdownListOptions
        {
            get
            {
                var yesNoDropdownListOptions = new List<SelectListItem>() {
                    new SelectListItem() {
                        Value="-1",
                        Text = "Sin preferencia"
                    },
                    new SelectListItem() {
                        Value="1",
                        Text = "Si"
                    },
                    new SelectListItem(){
                        Value="0",
                        Text = "No"
                    }
                };
                return yesNoDropdownListOptions;
            }
        }
        public List<SelectListItem> OrderByDropdownListOptions
        {
            get
            {

                var orderByDropdownListOptions = new List<SelectListItem>() {
                    new SelectListItem()
                    {
                        Value="0",
                        Text = "Fecha de creación",
                        Selected = this.OrderBy == null || this.OrderBy == "0"
                    },
                    new SelectListItem()
                    {
                        Value="2",
                        Text = "Título",
                        Selected = this.OrderBy != null && this.OrderBy == "2"

                    },
                    new SelectListItem()
                    {
                        Value="3",
                        Text = "Orden",
                        Selected = this.OrderBy != null && this.OrderBy == "3"
                    }
                };
                if (this.Edition.EditionType == EditionType.News ||
                    this.Edition.EditionType == EditionType.Information)
                {
                    orderByDropdownListOptions.Add(new SelectListItem()
                    {
                        Value = "1",
                        Text = "Fecha de publicación",
                        Selected = this.OrderBy != null && this.OrderBy == "1"

                    });
                }
                return orderByDropdownListOptions;

            }
        }
        public List<SelectListItem> OrderDirectionDropdownListOptions
        {
            get
            {

                var orderDirectionDropdownListOptions = new List<SelectListItem>() {
                     new SelectListItem()
                     {
                         Value="0",
                         Text = "Ascendente",
                         Selected = this.OrderDirection.ToString() == "0"
                     },
                    new SelectListItem()
                    {
                        Value="1",
                        Text = "Descendente",
                        Selected = this.OrderDirection.ToString() == "1"
                    }
                };
                return orderDirectionDropdownListOptions;

            }
        }
        public List<SelectListItem> PageSizesDropdownListOptions
        {
            get
            {
                var pageSizesDropdownListOptions = new List<SelectListItem>() {
                 new SelectListItem()
                 {
                     Value="5",
                     Text = "5",
                     Selected = this.PageSize == 5
                 },
                new SelectListItem()
                {
                    Value="10",
                    Text = "10",
                    Selected = this.PageSize == 10

                },
                new SelectListItem()
                {
                    Value="25",
                    Text = "25",
                    Selected = this.PageSize == 25
                },
                new SelectListItem()
                {
                    Value="100",
                    Text = "100",
                    Selected = this.PageSize == 100
                }
                };

                return pageSizesDropdownListOptions;
            }
        }
        public List<SelectListItem> PlacesAtHomeListOptions
        {
            get
            {
                var placesAtHome = new List<SelectListItem>() {
                    new SelectListItem()
                    {
                        Value="0",
                        Text = "Sin preferencia",
                        Selected = this.PlaceAtHome == null
                    },
                };
                foreach (var articlePlaceAtHome in this.PlacesAtHome)
                {
                    var notZeroId = articlePlaceAtHome.Id + 1;
                    placesAtHome.Add(
                        item: new SelectListItem()
                        {
                            Value = notZeroId.ToString(),
                            Text = articlePlaceAtHome.Title,
                            Selected = this.PlaceAtHome != null && this.PlaceAtHome.Id == articlePlaceAtHome.Id
                        });
                }
                return placesAtHome;
            }
        }
        public IndexViewModel()
        {

        }
        public IndexViewModel(EditionQueryModel editionQueryModel,
            List<ArticleQueryModel> articleQueryModels,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int totalCount, int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            this.Edition = new EditionAdminModel(
                queryModel: editionQueryModel);
            this.Articles = new List<ArticleAdminModel>();
            foreach (var queryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleAdminModel(
                    queryModel: queryModel));
            }
            this.PlacesAtHome = ArticlePlaceAtHome.List(editionQueryModel.EditionType);
            this.SearchText = searchText;
            if (placeAtHome.HasValue && placeAtHome.Value > 0)
            {
                this.PlaceAtHome = ArticlePlaceAtHome.From(placeAtHome.Value);
            }
            this.PublicationDate = publicationDate;
            this.Enable = enable;
            this.AtHome = atHome;

            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection ?? 1;

            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            this.CalculatePagerItems();

        }




        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    searchText: this.SearchText,
                    publicationDate: this.PublicationDate,
                    enable: this.Enable,
                    atHome: this.AtHome,
                    placeAtHome: this.PlaceAtHome != null ? this.PlaceAtHome.Id : -1,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns,
                    iDEdition: this.Edition.IDEdition,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                iDArticle: id,
                searchText: this.SearchText,
                publicationDate: this.PublicationDate,
                enable: this.Enable,
                atHome: this.AtHome,
                placeAtHome: this.PlaceAtHome != null ? this.PlaceAtHome.Id : -1,
                orderBy: this.OrderBy,
                orderDirection: this.OrderDirection,
                pageNumber: this.PageNumber,
                pageSize: this.PageSize,
                columns: this.Columns,
                iDEdition: this.Edition.IDEdition,
                editionPageNumber: this.EditionPageNumber,
                editionPageSize: this.EditionPageSize,
                editionColumns: this.EditionColumns);
        }
        public ComebackParametersViewModel GetComebackParametersForPage(int pageNumber)
        {
            return new ComebackParametersViewModel(
                   searchText: this.SearchText,
                   publicationDate: this.PublicationDate,
                   enable: this.Enable,
                   atHome: this.AtHome,
                   placeAtHome: this.PlaceAtHome != null ? this.PlaceAtHome.Id : -1,
                   orderBy: this.OrderBy,
                   orderDirection: this.OrderDirection,
                   pageNumber: pageNumber,
                   pageSize: this.PageSize,
                   columns: this.Columns,
                   iDEdition: this.Edition.IDEdition,
                   editionPageNumber: this.EditionPageNumber,
                   editionPageSize: this.EditionPageSize,
                   editionColumns: this.EditionColumns);
        }

        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Editions?{0}",
                    this.ComebackParameters.URLBackParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de contenidos";
            }
        }

        #endregion
        #region CreatePageButton

        public string CreatePageButtonLink
        {
            get
            {
                return string.Format("/Admin/Articles/Create/{0}?{1}",
                    this.Edition.IDEdition,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string CreatePageButtonTitle
        {
            get
            {
                return string.Format("Nuevo artículo de {0}",
                    this.Edition.Title);
            }
        }

        #endregion
        #region FirstPageLink
        public string FirstPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(1);
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.Edition.IDEdition,
                comebackParameters.URLParameters);
        }
        public string FirstPageTitle
        {
            get
            {
                return "Primera página";
            }
        }
        #endregion
        #region ToPageLink
        public string ToPageURL(int pageNumber)
        {
            var comebackParameters = this.GetComebackParametersForPage(pageNumber);
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.Edition.IDEdition,
                comebackParameters.URLParameters);
        }
        public string ToPageTitle(int pageNumber)
        {

            return string.Format("Página {0}", pageNumber);

        }
        #endregion


        #region BackPageLink
        public string BackPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.BackPage);
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.Edition.IDEdition,
                comebackParameters.URLParameters);
        }
        public string BackPageTitle
        {
            get
            {
                return "Anterior página";
            }
        }
        #endregion
        #region NextPageLink
        public string NextPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.NextPage);
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.Edition.IDEdition,
                comebackParameters.URLParameters);
        }
        public string NextPageTitle
        {
            get
            {
                return "Siguiente página";
            }
        }
        #endregion
        #region LastPageLink
        public string LastPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.PageCount);
            return string.Format("/Admin/Articles/Index/{0}?{1}",
                this.Edition.IDEdition,
                comebackParameters.URLParameters);
        }
        public string LastPageTitle
        {
            get
            {
                return "Última página";
            }
        }
        #endregion

    }
}