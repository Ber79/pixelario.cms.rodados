﻿using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Web.Areas.Admin.Models.SubTopics;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class TopicAdminModel
    {
        public int IDTopic { get; set; }
        public string Title { get; set; }
        public List<SubTopicAdminModel>  SubTopics { get; set; }

        private TopicAdminModel()
        {
        }
        public TopicAdminModel(TopicQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDTopic = queryModel.IDTopic;
            this.Title = queryModel.Title;
            this.SubTopics = new List<SubTopicAdminModel>();
            foreach (var subTopicQueryModel in queryModel.SubTopics)
            {
                this.SubTopics.Add(new SubTopicAdminModel(
                    queryModel: subTopicQueryModel));
            }

        }
    }
}