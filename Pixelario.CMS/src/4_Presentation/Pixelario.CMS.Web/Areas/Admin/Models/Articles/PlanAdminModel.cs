﻿using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;
using System.Threading.Tasks;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class PlanAdminModel
    {
        public int IDPlan { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
        public async Task SetSelected(int iDArticles)
        {

            this.Selected = await _constraintsQueries.GetConstrainedValueAsyn(
                iDPlan: IDPlan,
                constraintType: ConstraintType.OnArticle,
                iDConstrained: iDArticles);
        }
        private IConstraintsQueries _constraintsQueries { get; set; }
        private PlanAdminModel()
        {
        }
        public PlanAdminModel(Plan plan,
            IConstraintsQueries constraintsQueries)
        {
            if (plan == null)
                throw new Exception("No se puede parsear el modelo.");
            if (constraintsQueries == null)
                throw new Exception("No se puede acceder a la base de datos.");
            this.IDPlan = plan.Id;
            this.Title = plan.Title;
            _constraintsQueries = constraintsQueries;
        }
    }
}