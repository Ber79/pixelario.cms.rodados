﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.Tools;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class ArticleAdminModel
    {
        public int IDArticle { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public ArticlePlaceAtHome PlaceAtHome { get; set; }
        private ArticleAdminModel()
        {
        }
        public ArticleAdminModel(ArticleQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDArticle = queryModel.IDArticle;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.AtHome = queryModel.AtHome;
            this.PlaceAtHome = queryModel.PlaceAtHome;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el articulo \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region Publish button
        public string AtHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada el articulo \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada el articulo \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonClass
        {
            get
            {
                return this.AtHome ? "fa-check" : "fa-times";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Articulo en portada" : "Articulo fuera de portada";
            }
        }
        #endregion
        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar el articulo \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar el articulo \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Articulo habilitado" : "Articulo deshabilitado";
            }
        }

        #endregion
        #region Preview Button
       
        public string PreviewURL
        {
            get
            {
                return string.Format("/preview/articles/Index/{0}",
                    this.IDArticle);
            }
        }
        #endregion
        #region MultimediaLink
        public string MultimediaLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Articles/Images/{0}?{1}",
                this.IDArticle, comebackParameters.URLParameters);
        }
        public string MultimediaLinkTitle
        {
            get
            {
                return string.Format("Multimedios de {0}", this.Title);
            }
        }
        #endregion

        #region DocumentsLink
        public string DocumentsLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Articles/Documents/{0}?{1}",
                this.IDArticle, comebackParameters.URLParameters);
        }
        public string DocumentsLinkTitle
        {
            get
            {
                return string.Format("Documentos de {0}", this.Title);
            }
        }
        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Articles/Update/{0}?{1}",
                this.IDArticle, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar artículo";
            }
        }
        #endregion
        #region WidgetsLink
        public string WidgetsLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Articles/Widgets/{0}?{1}",
                this.IDArticle, comebackParameters.URLParameters);
        }
        public string WidgetsLinkTitle
        {
            get
            {
                return string.Format("Widgets de {0}", this.Title);
            }
        }
        #endregion

    }
}