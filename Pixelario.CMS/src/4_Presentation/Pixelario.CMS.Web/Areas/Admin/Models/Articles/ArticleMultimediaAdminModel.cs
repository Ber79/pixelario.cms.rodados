﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Text.RegularExpressions;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class ArticleMultimediaAdminModel
    {
        public int IDMultimedia { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string GetCode(string width)
        {
            if (string.IsNullOrEmpty(this.Code))
                return null;
            var pattern = string.Format("width=\"{0}\"",
                width);
            var iframe = Regex.Replace(this.Code, @"width=""[^\s]*""", pattern);
            return Regex.Replace(iframe, @"height=""[^\s]*""", "height=\"auto\"");

        }
        public int IDMultimediaType { get; set; }
        public string Path1 { get; set; }
        public string GetImage(string width)
        {
            if (string.IsNullOrEmpty(this.Path1)) return null;
            return this.Path1.Replace("/80/",
                string.Format("/{0}/", width));

        }
        private ArticleMultimediaAdminModel()
        {
        }
        public ArticleMultimediaAdminModel(ArticleMultimediaQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDMultimedia = queryModel.IDMultimedia;
            this.Title = queryModel.Title;
            this.IDMultimediaType = queryModel.IDMultimediaType;
            this.Path1 = queryModel.Path1;
            this.Code = queryModel.Summary;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el multimedia \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion


    }
}