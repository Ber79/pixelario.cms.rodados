﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Articles;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class CropImagesViewModel : FormPagedContentViewModel
    {

        public int? IDArticle { get; set; }
        public int IDEdition { get; set; }
        public string Title { get; set; }
        public string HomeImage { get; set; }
        public string ActionName { get; set; }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string Ratio { get; set; }
        public string SearchText { get; set; }
        public string PublicationDateParameter { get; set; }
        public string Enable { get; set; }
        public string AtHome { get; set; }
        public ArticlePlaceAtHome PlaceAtHomeParameter { get; set; }
        public string OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public EditionAdminModel Edition { get; set; }

        public CropImagesViewModel()
        {

        }
        public CropImagesViewModel(string mode,
            ArticleQueryModel articleQueryModel,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            this.SetMode(mode);
            this.IDArticle = articleQueryModel.IDArticle;
            this.Title = articleQueryModel.Title;
            switch (mode)
            {
                case "cropHorizontalImage":
                    this.ActionName = "CropHorizontalImage";
                    this.Ratio = "911 / 380";
                    break;
                case "cropSlideImage":
                    this.ActionName = "CropSlideImage";
                    this.Ratio = "624 / 475";
                    break;
                case "cropVerticalImage":
                    this.ActionName = "CropVerticalImage";
                    this.Ratio = "480 / 600";
                    break;
            }
            this.Edition = new EditionAdminModel(
                queryModel: articleQueryModel.Edition);

            this.HomeImage = articleQueryModel.HomeImage;
            this.SearchText = searchText;
            if (placeAtHome.HasValue && placeAtHome.Value > 0)
            {
                this.PlaceAtHomeParameter = ArticlePlaceAtHome.From(placeAtHome.Value);
            }
            this.PublicationDateParameter = publicationDate;
            this.Enable = enable;
            this.AtHome = atHome;

            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection ?? 10;

            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;

            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
            this.Columns = columns;

        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDArticle.HasValue)
                {
                    return new ComebackParametersViewModel(
                        iDArticle: this.IDArticle.Value,
                        searchText: this.SearchText,
                        publicationDate: this.PublicationDateParameter,
                        enable: this.Enable,
                        atHome: this.AtHome,
                        placeAtHome: this.PlaceAtHomeParameter != null ? this.PlaceAtHomeParameter.Id : -1,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns,
                        iDEdition: this.Edition.IDEdition,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
                }
                else
                {
                    return new ComebackParametersViewModel(
                        searchText: this.SearchText,
                        publicationDate: this.PublicationDateParameter,
                        enable: this.Enable,
                        atHome: this.AtHome,
                        placeAtHome: this.PlaceAtHomeParameter != null ? this.PlaceAtHomeParameter.Id : -1,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns,
                        iDEdition: this.Edition.IDEdition,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);

                }
            }
        }

        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Articles/Images/{0}?{1}",
                    this.IDArticle,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return string.Format("Multimedios de {0}", this.Title);
            }
        }
        #endregion

    }
}