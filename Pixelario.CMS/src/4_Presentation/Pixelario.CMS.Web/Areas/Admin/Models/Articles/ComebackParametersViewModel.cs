﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class ComebackParametersViewModel
    {
        public int? IDArticle { get; set; }
        public string SearchText { get; set; }
        public string PublicationDate { get; set; }
        public string Enable { get; set; }
        public string AtHome { get; set; }
        public int PlaceAtHome { get; set; }
        public string OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        public int IDEdition { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDArticle,
            string searchText, string publicationDate,
            string enable, string atHome,
            int placeAtHome,
            string orderBy, int orderDirection,
            int pageNumber,
            int pageSize, string columns,
            int iDEdition,
            int editionPageNumber, int editionPageSize,
            string editionColumns) : this(searchText, publicationDate,
                enable, atHome, placeAtHome,
                orderBy, orderDirection,
                pageNumber, pageSize, columns,
                iDEdition, editionPageNumber, editionPageSize,
                editionColumns)
        {
            this.IDArticle = iDArticle;
        }
        public ComebackParametersViewModel(
            string searchText, string publicationDate,
            string enable, string atHome,
            int placeAtHome,
            string orderBy, int orderDirection,
            int pageNumber,
            int pageSize, string columns,
            int iDEdition,
            int editionPageNumber, int editionPageSize,
            string editionColumns)
        {
            this.SearchText = searchText;
            this.PublicationDate = publicationDate;
            this.Enable = enable;
            this.AtHome = atHome;
            this.PlaceAtHome = placeAtHome;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Columns = columns;
            this.IDEdition = iDEdition;
            this.EditionPageNumber = editionPageNumber;
            this.EditionPageSize = editionPageSize;
            this.EditionColumns = editionColumns;
        }
        public bool HasIDArticle
        {
            get
            {
                return this.IDArticle.HasValue;
            }
        }
        public string URLBackParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.EditionPageNumber, this.EditionPageSize,
                    !string.IsNullOrEmpty(this.EditionColumns) ?
                        string.Format("&columns={0}", this.EditionColumns) :
                        "");
            }
        }
        public string URLParameters
        {
            get
            {
                var searchTextParameter = string.Format("searchText={0}", this.SearchText);
                var publicationDateParameter = string.Format("publicationDate={0}", this.PublicationDate);
                var enableParameter = string.Format("enable={0}", this.Enable);
                var atHomeParameter = string.Format("atHome={0}", this.AtHome);
                var placeAtHomeParameter = string.Format("placeAtHome={0}", this.PlaceAtHome);
                var orderByParameter = string.Format("orderBy={0}", this.OrderBy);
                var orderDirectionParameter = string.Format("orderDirection={0}", this.OrderDirection);
                var editionPageNumberParameter = string.Format("editionPageNumber={0}", this.EditionPageNumber);
                var editionPageSizeParameter = string.Format("editionPageSize={0}", this.EditionPageSize);
                var editionColumns = string.Format("&editionColumns={0}", this.EditionColumns);
                var pageNumberParameter = string.Format("pageNumber={0}", this.PageNumber);
                var pageSizeParameter = string.Format("pageSize={0}", this.PageSize);
                var columnsParameter = string.Format("&columns={0}", this.Columns);
                return string.Format("{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&{9}&{10}&{11}&{12}",
                    searchTextParameter,
                    publicationDateParameter,
                    enableParameter, 
                    atHomeParameter,
                    orderByParameter, 
                    orderDirectionParameter,
                    placeAtHomeParameter,
                    editionPageNumberParameter, 
                    editionPageSizeParameter,
                    editionColumns, 
                    pageNumberParameter,
                    pageSizeParameter, 
                    columnsParameter);
            }
        }
    }
}