﻿using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class EditionAdminModel
    {
        public int IDEdition { get; set; }
        public string Title { get; set; }
        public EditionType EditionType { get; set; }
        public List<TopicAdminModel> Topics { get; set; }
        private EditionAdminModel()
        {
        }
        public EditionAdminModel(EditionQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDEdition = queryModel.IDEdition;
            this.Title = queryModel.Title;
            this.EditionType = queryModel.EditionType;

            this.Topics = new List<TopicAdminModel>();
            if (queryModel.Topics != null)
            {
                foreach (var topicQueryModel in queryModel.Topics)
                {
                    this.Topics.Add(new TopicAdminModel(
                        queryModel: topicQueryModel));
                }
            }
        }
    }
}