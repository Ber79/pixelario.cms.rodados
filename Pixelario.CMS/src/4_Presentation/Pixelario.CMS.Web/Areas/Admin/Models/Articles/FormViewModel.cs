﻿using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Articles
{
    public class FormViewModel : FormPagedContentViewModel
    {


        public int? IDArticle { get; set; }
        public int IDEdition { get; set; }
        public DateTime? PublicationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Source { get; set; }
        public string Keywords { get; set; }
        public string HomeImage { get; set; }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string SlideImage { get; set; }
        public string GetSlideImage(string width)
        {
            if (string.IsNullOrEmpty(this.SlideImage)) return null;
            return this.SlideImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string HorizontalImage { get; set; }
        public string GetHorizontalImage(string width)
        {
            if (string.IsNullOrEmpty(this.HorizontalImage)) return null;
            return this.HorizontalImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string VerticalImage { get; set; }
        public string GetVerticalImage(string width)
        {
            if (string.IsNullOrEmpty(this.VerticalImage)) return null;
            return this.VerticalImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public bool HasSlideImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.SlideImage);
            }
        }
        public bool HasHorizontalImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HorizontalImage);
            }
        }
        public bool HasVerticalImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.VerticalImage);
            }
        }
        public EditionAdminModel Edition { get; set; }
        public List<int> Topics { get; set; }
        public List<int> SubTopics { get; set; }
        public List<ArticleMultimediaAdminModel> Multimedia { get; set; }
        public bool SubscriptionsEnable { get; set; }
        public string MultimediaTitle { get; set; }
        public string MultimediaLink { get; set; }
        public List<PlanAdminModel> PlanAdmins { get; set; }
        public List<int> Plans { get; set; }
        public List<EditionAdminModel> Editions { get; set; }
        public List<SelectListItem> Scripts { get; set; }
        public List<SelectListItem> EditionsSelectList
        {
            get
            {
                return Editions.Select(e => new SelectListItem
                {
                    Text = e.Title,
                    Value = e.IDEdition.ToString(),
                    Selected = e.IDEdition == this.Edition.IDEdition
                }).ToList();
            }
        }
        public string SearchText { get; set; }
        public string PublicationDateParameter { get; set; }
        public string Enable { get; set; }
        public string AtHome { get; set; }
        public ArticlePlaceAtHome PlaceAtHomeParameter { get; set; }
        public string OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }

        public FormViewModel()
        {

        }
        // Base constructor for all page
        public FormViewModel(string mode,
           string searchText, string publicationDate,
           string enable, string atHome,
           int? placeAtHome, string orderBy,
           int? orderDirection,
           int? pageNumber,
           int? pageSize, string columns,
           int? editionPageNumber,
           int? editionPageSize, string editionColumns
           )
        {
            this.SetMode(mode);
            this.SearchText = searchText;
            if (placeAtHome.HasValue && placeAtHome.Value > 0)
            {
                this.PlaceAtHomeParameter = ArticlePlaceAtHome.From(placeAtHome.Value);
            }
            this.PublicationDateParameter = publicationDate;
            this.Enable = enable;
            this.AtHome = atHome;

            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection ?? 1;

            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;

            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
            this.Columns = columns;

        }

        // Base constructor for form views
        public FormViewModel(string mode,
            EditionQueryModel editionQueryModel,
            bool subscriptionsEnable,
            List<PlanAdminModel> planAdmins,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) : this(mode,
                    searchText, publicationDate, enable,
                    atHome, placeAtHome, orderBy,
                    orderDirection, pageNumber, pageSize,
                    columns,
                    editionPageNumber, editionPageSize,
                    editionColumns)
        {            
            this.PlanAdmins = planAdmins;
            this.SubscriptionsEnable = subscriptionsEnable;
            this.Edition = new EditionAdminModel(
                queryModel: editionQueryModel);
            this.IDEdition = editionQueryModel.IDEdition;
            this.Topics = new List<int>();
            this.SubTopics = new List<int>();
        }
        // Create constructorW
        public FormViewModel(string mode,
            EditionQueryModel editionQueryModel,
            List<ArticleScriptQueryModel> scripts,
            bool subscriptionsEnable,
            List<PlanAdminModel> planAdmins,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) : this(mode, editionQueryModel,
                    subscriptionsEnable, planAdmins,
                    searchText, publicationDate, enable,
                    atHome, placeAtHome, orderBy,
                    orderDirection, pageNumber, pageSize,
                    columns,
                    editionPageNumber, editionPageSize,
                    editionColumns)
        {
            this.Scripts = new List<SelectListItem>();
            foreach (var script in scripts)
            {
                this.Scripts.Add(new SelectListItem()
                {
                    Text = script.Title,
                    Value = script.IDArticleScript.ToString()
                });
            }
            
        }
        public FormViewModel(string mode,
            AddCommand command,
            EditionQueryModel editionQueryModel,
            List<ArticleScriptQueryModel> scripts,
            bool subscriptionsEnable,
            List<PlanAdminModel> planAdmins,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) : this(mode, editionQueryModel, subscriptionsEnable,
                    planAdmins,
                    searchText, publicationDate, enable,
                    atHome, placeAtHome, orderBy,
                    orderDirection, pageNumber, pageSize,
                    columns,
                    editionPageNumber, editionPageSize,
                    editionColumns)
        {
            this.PublicationDate = command.PublicationDate;
            this.Title = command.Title;
            this.Summary = command.Summary;
            this.Body = command.Body;
            this.Source = command.Source;
            this.Keywords = command.Keywords;
            this.Topics = new List<int>();
            if (command.Topics != null)
            {
                this.Topics = command.Topics;
            }
            this.SubTopics = new List<int>();
            if (command.SubTopics != null)
            {
                this.SubTopics = command.SubTopics;
            }
            this.Scripts = new List<SelectListItem>();
            foreach (var script in scripts)
            {
                this.Scripts.Add(new SelectListItem()
                {
                    Text = script.Title,
                    Value = script.IDArticleScript.ToString(),
                    Selected = command.Scripts.Count > 0 && command.Scripts.Contains(
                        item: script.IDArticleScript)
                });
            }

        }


        public FormViewModel(string mode,
            UpdateCommand command,
            EditionQueryModel editionQueryModel,
            List<ArticleScriptQueryModel> scripts,
            bool subscriptionsEnable,
            List<EditionQueryModel> editionQueryModels,
            List<PlanAdminModel> planAdmins,
            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) : this(mode, editionQueryModel, subscriptionsEnable,
                planAdmins,
                searchText, publicationDate, enable,
                atHome, placeAtHome, orderBy,
                orderDirection, pageNumber, pageSize,
                columns,
                editionPageNumber, editionPageSize,
                editionColumns)
        {
            this.IDArticle = command.IDArticle;
            this.PublicationDate = command.PublicationDate;
            this.Title = command.Title;
            this.Summary = command.Summary;
            this.Body = command.Body;
            this.Source = command.Source;
            this.Keywords = command.Keywords;
            this.Topics = new List<int>();
            if (command.Topics != null)
            {
                this.Topics = command.Topics;
            }
            this.SubTopics = new List<int>();
            if (command.SubTopics != null)
            {
                this.SubTopics = command.SubTopics;
            }
            this.Editions = new List<EditionAdminModel>();
            foreach (var queryModel in editionQueryModels)
            {
                this.Editions.Add(new EditionAdminModel(
                    queryModel: queryModel));
            }
            this.Scripts = new List<SelectListItem>();
            foreach (var script in scripts)
            {
                this.Scripts.Add(new SelectListItem()
                {
                    Text = script.Title,
                    Value = script.IDArticleScript.ToString(),
                    Selected = command.Scripts.Count > 0 && command.Scripts.Contains(
                        item: script.IDArticleScript)
                });
            }

        }


        public FormViewModel(
            string mode,
            EditionQueryModel editionQueryModel,
            List<ArticleScriptQueryModel> scripts,
            List<PlanAdminModel> planAdmins,
            bool subscriptionsEnable,
            ArticleQueryModel articleQueryModel,
            List<EditionQueryModel> editionQueryModels,

            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) :
                this(mode, editionQueryModel,
                    subscriptionsEnable, planAdmins,
                    searchText, publicationDate, enable,
                    atHome, placeAtHome, orderBy,
                    orderDirection, pageNumber, pageSize,
                    columns,
                    editionPageNumber, editionPageSize,
                    editionColumns)
        {
            IDArticle = articleQueryModel.IDArticle;
            PublicationDate = articleQueryModel.PublicationDate;
            Title = articleQueryModel.Title;
            Summary = articleQueryModel.Summary;
            Body = articleQueryModel.Body;
            Source = articleQueryModel.Source;
            Keywords = articleQueryModel.Keywords;

            this.Topics = articleQueryModel.Topics.Select(t => t.IDTopic).ToList();
            this.SubTopics = articleQueryModel.SubTopics.Select(s => s.IDSubTopic).ToList();

            this.Editions = new List<EditionAdminModel>();
            foreach (var queryModel in editionQueryModels)
            {
                this.Editions.Add(new EditionAdminModel(
                    queryModel: queryModel));
            }
            this.Scripts = new List<SelectListItem>();
            foreach (var script in scripts)
            {
                this.Scripts.Add(new SelectListItem()
                {
                    Text = script.Title,
                    Value = script.IDArticleScript.ToString(),
                    Selected = articleQueryModel.ArticleScripts.Count > 0 && 
                        articleQueryModel.ArticleScripts.Where(ars => ars.IDArticleScript == script.IDArticleScript ).Count() > 0
                });
            }

        }

        public FormViewModel(
            string mode,

            ArticleQueryModel articleQueryModel,
            List<ArticleMultimediaQueryModel> multimediaQueryModels,

            string searchText, string publicationDate,
            string enable, string atHome,
            int? placeAtHome, string orderBy,
            int? orderDirection,
            int? pageNumber,
            int? pageSize, string columns,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns
            ) :
                this(mode,
                    searchText, publicationDate, enable,
                    atHome, placeAtHome, orderBy,
                    orderDirection, pageNumber, pageSize,
                    columns,
                    editionPageNumber, editionPageSize,
                    editionColumns)
        {
            this.IDArticle = articleQueryModel.IDArticle;
            this.Edition = new EditionAdminModel(
                queryModel: articleQueryModel.Edition);
            this.IDEdition = articleQueryModel.Edition.IDEdition;
            this.Title = articleQueryModel.Title;
            this.HomeImage = articleQueryModel.HomeImage;
            this.HorizontalImage = articleQueryModel.HorizontalImage;
            this.VerticalImage = articleQueryModel.VerticalImage;
            this.SlideImage = articleQueryModel.SlideImage;

            this.Multimedia = new List<ArticleMultimediaAdminModel>();
            foreach (var queryModel in multimediaQueryModels)
            {
                this.Multimedia.Add(new ArticleMultimediaAdminModel(
                    queryModel: queryModel));
            }

        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDArticle.HasValue)
                {
                    return new ComebackParametersViewModel(
                        iDArticle: this.IDArticle.Value,
                        searchText: this.SearchText,
                        publicationDate: this.PublicationDateParameter,
                        enable: this.Enable,
                        atHome: this.AtHome,
                        placeAtHome: this.PlaceAtHomeParameter != null ? this.PlaceAtHomeParameter.Id : -1,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns,
                        iDEdition: this.Edition.IDEdition,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
                }
                else
                {
                    return new ComebackParametersViewModel(
                        searchText: this.SearchText,
                        publicationDate: this.PublicationDateParameter,
                        enable: this.Enable,
                        atHome: this.AtHome,
                        placeAtHome: this.PlaceAtHomeParameter != null ? this.PlaceAtHomeParameter.Id : -1,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns,
                        iDEdition: this.Edition.IDEdition,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);

                }
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Articles/Index/{0}?{1}",
                    this.Edition.IDEdition,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return string.Format("Listado de artículos de {0}", this.Edition.Title);
            }
        }
        #endregion
        #region CropImage
        public string CropSlideImageUrl
        {
            get
            {
                return string.Format("/Admin/Articles/CropSlideImage/{0}?{1}",
                    this.IDArticle,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string CropSlideImageTitle
        {
            get
            {
                return string.Format("Cortar Slide de {0}", this.Title);
            }
        }
        public string CropHorizontalImageUrl
        {
            get
            {
                return string.Format("/Admin/Articles/CropHorizontalImage/{0}?{1}",
                    this.IDArticle,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string CropHorizontalImageTitle
        {
            get
            {
                return string.Format("Cortar Horizontal de {0}", this.Title);
            }
        }


        public string CropVerticalImageUrl
        {
            get
            {
                return string.Format("/Admin/Articles/CropVerticalImage/{0}?{1}",
                    this.IDArticle,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string CropVerticalImageTitle
        {
            get
            {
                return string.Format("Cortar Vertical de {0}", this.Title);
            }
        }
        #endregion

        public ChangeConstraintsCommand ToChangeConstraintsCommand(string solicitedBy)
        {
            if (!IDArticle.HasValue)
                throw new Exception("No se envio Id de la edición");

            var command = new ChangeConstraintsCommand(
                    iDArticle: IDArticle.Value,
                    plansSelected: Plans);
            command.SetSolicitedBy(solicitedBy: solicitedBy);
            return command;
        }
    }
}