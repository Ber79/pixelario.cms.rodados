﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems
{
    public class ComebackParametersViewModel
    {
        public int? IDArticleScriptItem { get; set; }
        public int IDArticleScript { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int ScriptPageNumber { get; set; }
        public int ScriptPageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDArticleScriptItem,
            int iDArticleScript,
            int pageNumber,
            int pageSize, int scriptPageNumber,
            int scriptPageSize, int editionPageNumber,
            int editionPageSize,
            string editionColumns) : this(iDArticleScript, pageNumber, pageSize,
                scriptPageNumber, scriptPageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScriptItem = iDArticleScriptItem;
        }       
       
        public ComebackParametersViewModel(int iDArticleScript, int pageNumber,
            int pageSize, int scriptPageNumber,
            int scriptPageSize, int editionPageNumber,
            int editionPageSize,
            string editionColumns)
        {
            this.IDArticleScript = iDArticleScript;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.ScriptPageNumber = scriptPageNumber;
            this.ScriptPageSize = scriptPageSize;
            this.EditionPageNumber = editionPageNumber;
            this.EditionPageSize = editionPageSize;
            this.EditionColumns = editionColumns;
        }
        public bool HasIDArticleScriptItem
        {
            get
            {
                return this.IDArticleScriptItem.HasValue;
            }
        }
        public string ColumnsParameter
        {
            get
            {
                if(string.IsNullOrEmpty(this.EditionColumns))
                    return "";
                return string.Format("&editionColumns={0}", this.EditionColumns);
            }
        }
        
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}&scriptPageNumber={2}&scriptPageSize={3}&editionPageNumber={4}&editionPageSize={5}{6}",
                    this.PageNumber, this.PageSize,
                    this.ScriptPageNumber, this.ScriptPageSize, 
                    this.EditionPageNumber, this.EditionPageSize,
                    this.ColumnsParameter);
            }
        }
        public string URLScriptParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}&editionPageNumber={2}&editionPageSize={3}{4}",
                    this.ScriptPageNumber, this.ScriptPageSize,
                    this.EditionPageNumber, this.EditionPageSize,
                    this.ColumnsParameter);
            }
        }
    }
}