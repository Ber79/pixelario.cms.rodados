﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems
{
    public class ArticleScriptAdminModel
    {
        public int IDArticleScript { get; set; }
        public string Title { get; set; }
        private ArticleScriptAdminModel()
        {
        }
        public ArticleScriptAdminModel(ArticleScriptQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDArticleScript = queryModel.IDArticleScript;
            this.Title = queryModel.Title;
        }
    }
}