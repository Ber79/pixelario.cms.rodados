﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems
{
    public class IndexViewModel : PagedContentViewModel
    {
        public int ScriptPageNumber { get; set; }
        public int ScriptPageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public ArticleScriptAdminModel ArticleScript { get; set; }
        public List<ArticleScriptItemAdminModel> ArticleScriptItems { get; set; }
        public IndexViewModel(
            ArticleScriptQueryModel articleScriptQueryModel,
            List<ArticleScriptItemQueryModel> articleScriptItemQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns)
        {
            this.ArticleScript = new ArticleScriptAdminModel(
                queryModel: articleScriptQueryModel);
            this.ArticleScriptItems = new List<ArticleScriptItemAdminModel>();
            foreach (var queryModel in articleScriptItemQueryModels)
            {
                this.ArticleScriptItems.Add(new ArticleScriptItemAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.ScriptPageNumber = scriptPageNumber ?? 1;
            this.ScriptPageSize = scriptPageSize ?? 10;
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    iDArticleScript: this.ArticleScript.IDArticleScript,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    scriptPageNumber: this.ScriptPageNumber,
                    scriptPageSize: this.ScriptPageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDArticleScriptItem: id,
                    iDArticleScript: this.ArticleScript.IDArticleScript,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    scriptPageNumber: this.ScriptPageNumber,
                    scriptPageSize: this.ScriptPageSize,
                    editionPageNumber: this.EditionPageNumber,
                    editionPageSize: this.EditionPageSize,
                    editionColumns: this.EditionColumns);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/ArticleScriptItems/Create/{0}?{1}", 
                    this.ArticleScript.IDArticleScript,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo ítem";
            }
        }

        #endregion
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/ArticleScripts?{0}",
                    this.ComebackParameters.URLScriptParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de scripts de artículos";
            }
        }

        #endregion

        #region ConfigurationButton
        public string ConfigurationButtonHref
        {
            get
            {
                return string.Format("/Admin/ArticleScriptItems/Configurations?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string ConfigurationButtonTitle
        {
            get
            {
                return "Configuraciones";
            }
        }

        #endregion

        public ComebackParametersViewModel GetComebackParametersForPage(int pageNumber)
        {
            return new ComebackParametersViewModel(
                iDArticleScript: this.ArticleScript.IDArticleScript,
                pageNumber: pageNumber,
                pageSize: this.PageSize,
                scriptPageNumber: this.ScriptPageNumber,
                scriptPageSize: this.ScriptPageSize,
                editionPageNumber: this.EditionPageNumber,
                editionPageSize: this.EditionPageSize,
                editionColumns: this.EditionColumns);
        }


        #region FirstPageLink
        public string FirstPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(1);
            return string.Format("/Admin/ArticleScripts/Index/{0}?{1}",
                this.ArticleScript.IDArticleScript,
                comebackParameters.URLParameters);
        }
        public string FirstPageTitle
        {
            get
            {
                return "Primera página";
            }
        }
        #endregion
        #region ToPageLink
        public string ToPageURL(int pageNumber)
        {
            var comebackParameters = this.GetComebackParametersForPage(pageNumber);
            return string.Format("/Admin/ArticleScripts/Index/{0}?{1}",
                this.ArticleScript.IDArticleScript,
                comebackParameters.URLParameters);
        }
        public string ToPageTitle(int pageNumber)
        {

            return string.Format("Página {0}", pageNumber);

        }
        #endregion


        #region BackPageLink
        public string BackPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.BackPage);
            return string.Format("/Admin/ArticleScripts/Index/{0}?{1}",
                this.ArticleScript.IDArticleScript,
                comebackParameters.URLParameters);
        }
        public string BackPageTitle
        {
            get
            {
                return "Anterior página";
            }
        }
        #endregion
        #region NextPageLink
        public string NextPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.NextPage);
            return string.Format("/Admin/ArticleScripts/Index/{0}?{1}",
                this.ArticleScript.IDArticleScript,
                comebackParameters.URLParameters);
        }
        public string NextPageTitle
        {
            get
            {
                return "Siguiente página";
            }
        }
        #endregion
        #region LastPageLink
        public string LastPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.PageCount);
            return string.Format("/Admin/ArticleScripts/Index/{0}?{1}",
                this.ArticleScript.IDArticleScript,
                comebackParameters.URLParameters);
        }
        public string LastPageTitle
        {
            get
            {
                return "Última página";
            }
        }
        #endregion
    }
}