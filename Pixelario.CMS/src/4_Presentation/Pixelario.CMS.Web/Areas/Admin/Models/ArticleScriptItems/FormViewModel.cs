﻿using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.Commons;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDArticleScriptItem { get; set; }

        public int ScriptPageNumber { get; set; }
        public int ScriptPageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public int IDArticleScript { get; set; }
        public ArticleScriptAdminModel ArticleScript { get; set; }
        public List<SelectListItem> MultimediaTypeDropdownListOptions { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public int IDMultimediaType { get; set; }

        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
            ArticleScriptQueryModel articleScriptQueryModel,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns)
        {
            this.SetMode(mode);
            this.ArticleScript = new ArticleScriptAdminModel(
                queryModel: articleScriptQueryModel);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.ScriptPageNumber = scriptPageNumber ?? 1;
            this.ScriptPageSize = scriptPageSize ?? 10;
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;

        }
        public FormViewModel(string mode,
           ArticleScriptQueryModel articleScriptQueryModel,
           List<MultimediaType> multimediaTypes,
           int? pageNumber,
           int? pageSize, int? scriptPageNumber,
           int? scriptPageSize, int? editionPageNumber,
           int? editionPageSize,
           string editionColumns) : this(mode,
                articleScriptQueryModel,
                pageNumber, pageSize,
                scriptPageNumber, scriptPageSize,
                editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.MultimediaTypeDropdownListOptions = new List<SelectListItem>();
            foreach (var multimediaType in multimediaTypes)
            {
                this.MultimediaTypeDropdownListOptions.Add(
                    new SelectListItem()
                    {
                        Text = multimediaType.Title,
                        Value = multimediaType.Id.ToString(),
                    });
            }
        }
        public FormViewModel(
            string mode,
            ArticleScriptItemQueryModel articleScriptItemQueryModel,
            List<MultimediaType> multimediaTypes,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode,
                articleScriptItemQueryModel.ArticleScript,
                pageNumber, pageSize,
                scriptPageNumber, scriptPageSize,
                editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScriptItem = articleScriptItemQueryModel.IDArticleScriptItem;
            this.MultimediaTypeDropdownListOptions = new List<SelectListItem>();
            foreach (var multimediaType in multimediaTypes)
            {
                this.MultimediaTypeDropdownListOptions.Add(
                    new SelectListItem()
                    {
                        Text = multimediaType.Title,
                        Value = multimediaType.Id.ToString(),
                        Selected = multimediaType.Id == articleScriptItemQueryModel.MultimediaType.Id
                    });
            }
            this.IDMultimediaType = articleScriptItemQueryModel.MultimediaType.Id;
            this.Title = articleScriptItemQueryModel.Title;
            this.Code = articleScriptItemQueryModel.Code;
        }
        public FormViewModel(
            string mode,
            AddScriptItemCommand command,
            ArticleScriptQueryModel articleScriptQueryModel,
            List<MultimediaType> multimediaTypes,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode, articleScriptQueryModel,
                pageNumber, pageSize,
                scriptPageNumber, scriptPageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.MultimediaTypeDropdownListOptions = new List<SelectListItem>();
            foreach (var multimediaType in multimediaTypes)
            {
                this.MultimediaTypeDropdownListOptions.Add(
                    new SelectListItem()
                    {
                        Text = multimediaType.Title,
                        Value = multimediaType.Id.ToString(),
                        Selected = multimediaType.Id == command.IDMultimediaType
                    });
            }
            this.Title = command.Title;
            this.Code = command.Code;
        }
        public FormViewModel(
            string mode,
            UpdateScriptItemCommand command,
            ArticleScriptQueryModel articleScriptQueryModel,
            List<MultimediaType> multimediaTypes,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns) : this(mode, articleScriptQueryModel,
                pageNumber, pageSize,
                scriptPageNumber, scriptPageSize, editionPageNumber,
                editionPageSize, editionColumns)
        {
            this.IDArticleScriptItem = command.IDArticleScriptItem;
            this.MultimediaTypeDropdownListOptions = new List<SelectListItem>();
            foreach (var multimediaType in multimediaTypes)
            {
                this.MultimediaTypeDropdownListOptions.Add(
                    new SelectListItem()
                    {
                        Text = multimediaType.Title,
                        Value = multimediaType.Id.ToString(),
                        Selected = multimediaType.Id == command.IDMultimediaType
                    });
            }
            this.Title = command.Title;
            this.Code = command.Code;
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDArticleScriptItem.HasValue)
                    return new ComebackParametersViewModel(
                        iDArticleScriptItem: this.IDArticleScriptItem.Value,
                        iDArticleScript: this.ArticleScript.IDArticleScript,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        scriptPageNumber: this.ScriptPageNumber,
                        scriptPageSize: this.ScriptPageSize,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
                else
                    return new ComebackParametersViewModel(
                        iDArticleScript: this.ArticleScript.IDArticleScript,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        scriptPageNumber: this.ScriptPageNumber,
                        scriptPageSize: this.ScriptPageSize,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/ArticleScriptItems/Index/{0}?{1}",
                    this.ArticleScript.IDArticleScript,
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de ítems";
            }
        }
        #endregion
    }
}