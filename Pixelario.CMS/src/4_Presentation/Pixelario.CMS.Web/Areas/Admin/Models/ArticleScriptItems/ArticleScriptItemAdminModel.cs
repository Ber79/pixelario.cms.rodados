﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.Commons;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems
{
    public class ArticleScriptItemAdminModel
    {
        public int IDArticleScriptItem { get; set; }
        public string Title { get; set; }
        public MultimediaType MultimediaType { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }

        private ArticleScriptItemAdminModel()
        {
        }
        public ArticleScriptItemAdminModel(ArticleScriptItemQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDArticleScriptItem = queryModel.IDArticleScriptItem;
            this.Title = queryModel.Title;
            this.MultimediaType = queryModel.MultimediaType;
            this.Enabled = queryModel.Enabled;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el ítem \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion

        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar el ítem \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar el ítem \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Ítem habilitado" : "Ítem deshabilitado";
            }
        }

        #endregion

        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/ArticleScriptItems/Update/{0}?{1}",
                this.IDArticleScriptItem, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar ítem";
            }
        }
        #endregion

    }
}