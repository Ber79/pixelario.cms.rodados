﻿
namespace Pixelario.CMS.Web.Areas.Admin.Models.Home
{
    public class LoginViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}