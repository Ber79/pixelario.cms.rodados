﻿using Pixelario.CMS.Web.Areas.Admin.Models.Users;
using Pixelario.Identity.Domain;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Users
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<UserAdminModel> Users { get; private set; }
        public IndexViewModel()
        {

        }
        public IndexViewModel(List<ApplicationUser> applicationUsers,
            int totalCount, int? pageNumber,
            int? pageSize)
        {
            this.Users = new List<UserAdminModel>();
            foreach(var applicationUser in applicationUsers)
            {
                this.Users.Add(new UserAdminModel(
                    queryModel: applicationUser));            
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    userId: id,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize);
        }
        #region NewButton
        public string NewButtonHref
        {
            get
            {
                return string.Format("/Admin/Users/Create?{0}", this.ComebackParameters.URLParameters);
            }
        }
        public string NewButtonTitle
        {
            get
            {
                return "Nuevo tema";
            }
        }

        #endregion

    }
}