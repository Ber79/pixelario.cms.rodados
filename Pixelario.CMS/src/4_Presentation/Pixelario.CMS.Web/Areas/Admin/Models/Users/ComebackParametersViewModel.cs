﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Users
{
    public class ComebackParametersViewModel
    {
        public int? UserId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int userId,
            int pageNumber,
            int pageSize) : this(pageNumber, pageSize)
        {
            this.UserId = userId;
        }       
       
        public ComebackParametersViewModel(int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
        public bool HasUserId
        {
            get
            {
                return this.UserId.HasValue;
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}",
                    this.PageNumber, this.PageSize);
            }
        }        
    }
}