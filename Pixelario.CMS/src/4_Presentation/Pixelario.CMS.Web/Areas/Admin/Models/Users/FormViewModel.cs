﻿using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Users
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public bool IsMyProfile => "myprofile" == Mode;
        public bool IsMyPassword => "mypassword" == Mode;
        public bool IsPassword => "password" == Mode;

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public bool HasPicture
        {
            get
            {
                return !string.IsNullOrEmpty(this.Picture);
            }
        }
        public string GetPicture(string width)
        {
            if (string.IsNullOrEmpty(this.Picture)) return null;
            return this.Picture.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public List<int> RolesId { get; set; }
        public string[] Roles { get; set; }
        public int Id { get; set; }
        public int? UserId { get; set; }

        public IList<string> UserRoles { get; set; }

        public FormViewModel()
        {
            UserRoles = new List<string>();
            Roles = null;
        }
        public FormViewModel(string mode,
            int? pageNumber,
            int? pageSize)
        {
            this.SetMode(mode);
            this.SetPagingItems(
                totalCount: default(int),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);

        }
        public FormViewModel(
            string mode,
            ApplicationUser applicationUser,
            int? pageNumber,
            int? pageSize) : this(mode, pageNumber, pageSize)
        {
            this.UserId = applicationUser.Id;
            this.FirstName = applicationUser.FirstName;
            this.LastName = applicationUser.LastName;
            this.Picture = applicationUser.Picture;
        }
        public FormViewModel(
            string mode,
            List<ApplicationRole> applicatioRoles, List<string> userRoles,
            ApplicationUser applicationUser,
            int? pageNumber,
            int? pageSize) : this(mode, 
                applicationUser,
                pageNumber, pageSize)
        {
            this.Roles = applicatioRoles.Select(r=>r.Name).ToArray();
            this.UserRoles = userRoles;
        }
       
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.UserId.HasValue)
                    return new ComebackParametersViewModel(
                        userId: this.UserId.Value,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
                else
                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Users?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de usuarios";
            }
        }
        #endregion

    }
}