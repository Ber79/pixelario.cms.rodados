﻿using Pixelario.Identity.Domain;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Users
{
    public class UserAdminModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public UserAdminModel()
        {

        }
        public UserAdminModel(ApplicationUser queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.Id = queryModel.Id;
            this.UserName = queryModel.UserName;
            this.Email = queryModel.Email;
        }

        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de borrar el usuario \"{0}\"?') == false) return false;", this.UserName);
            }
        }
        #endregion
        #region UpdateLink
        public string UpdateLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Users/Update/{0}?{1}",
                this.Id, comebackParameters.URLParameters);
        }
        public string UpdateLinkTitle
        {
            get
            {
                return "Editar usuario";
            }
        }
        #endregion
        #region RolesLink
        public string RolesLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Users/Roles/{0}?{1}",
                this.Id, comebackParameters.URLParameters);
        }
        public string RolesLinkTitle
        {
            get
            {
                return "Editar roles del usuario";
            }
        }
        #endregion
        #region PasswordLink
        public string PasswordLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/Users/Password/{0}?{1}",
                this.Id, comebackParameters.URLParameters);
        }
        public string PasswordLinkTitle
        {
            get
            {
                return "Cambiar contraseña";
            }
        }
        #endregion
    }
}