﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.SubTopics
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<SubTopicAdminModel> SubTopics { get; set; }
        public TopicAdminModel Topic { get; set; }
        public int? TopicPageNumber { get; set; }
        public int? TopicPageSize { get; set; }
        public string TopicColumns { get; set; }
        public IndexViewModel(TopicQueryModel topicQueryModel,
            List<SubTopicQueryModel> subTopicQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize, string columns,
            int? topicPageNumber,
            int? topicPageSize, string topicColumns)
        {
            this.Topic = new TopicAdminModel(
               queryModel: topicQueryModel);
            this.SubTopics = new List<SubTopicAdminModel>();
            if (subTopicQueryModels != null)
            {
                foreach (var queryModel in subTopicQueryModels)
                {
                    this.SubTopics.Add(new SubTopicAdminModel(
                        queryModel: queryModel));
                }
            }
            this.TopicPageNumber = topicPageNumber;
            this.TopicPageSize = topicPageSize;
            this.TopicColumns = topicColumns;
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
        }

        public SubTopicComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new SubTopicComebackParametersViewModel(
                    iDTopic: this.Topic.IDTopic,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns,
                    topicPageNumber: this.TopicPageNumber ?? 1,
                    topicPageSize: this.TopicPageSize ?? 10,
                    topicColumns: this.TopicColumns);
            }
        }

        public SubTopicComebackParametersViewModel GetComebackParameters(int id)
        {
            return new SubTopicComebackParametersViewModel(
                    iDSubTopic: id,
                    iDTopic: this.Topic.IDTopic,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    columns: this.Columns,
                    topicPageNumber: this.TopicPageNumber ?? 1,
                    topicPageSize: this.TopicPageSize ?? 10,
                    topicColumns: this.TopicColumns);
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/Topics?{0}",
                    this.ComebackParameters.URLBackParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de temas";
            }
        }

        #endregion
        #region CreatePageButton

        public string CreatePageButtonLink
        {
            get
            {
                return string.Format("/Admin/SubTopics/Create/{0}?{1}",
                    this.Topic.IDTopic,
                    this.ComebackParameters.URLTopicParameters);
            }
        }
        public string CreatePageButtonTitle
        {
            get
            {
                return string.Format("Nuevo subtema de {0}",
                    this.Topic.Title);
            }
        }

        #endregion

    }
}