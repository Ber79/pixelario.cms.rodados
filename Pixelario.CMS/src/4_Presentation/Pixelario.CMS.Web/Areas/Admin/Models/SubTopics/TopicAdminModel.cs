﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.SubTopics
{
    public class TopicAdminModel
    {
        public int IDTopic { get; set; }
        public string Title { get; set; }

        private TopicAdminModel()
        {
        }
        public TopicAdminModel(TopicQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDTopic = queryModel.IDTopic;
            this.Title = queryModel.Title;
        }
    }
}