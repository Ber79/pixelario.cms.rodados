﻿using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.SubTopics
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDSubTopic { get; set; }
        public int IDTopic { get; set; }
        public TopicAdminModel Topic { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public string HomeImage { get; set; }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }
        public string TopicColumns { get; set; }
        public FormViewModel()
        {

        }
        public FormViewModel(string mode, TopicQueryModel topicQueryModel,
            int? topicPageNumber, int? topicPageSize,
            string topicColumns, int? pageSize,
            int? pageNumber, string columns)
        {
            this.SetMode(mode);
            this.Topic = new TopicAdminModel(
                queryModel: topicQueryModel);
            this.IDTopic = topicQueryModel.IDTopic;
            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
            this.Columns = columns;
            this.TopicPageNumber = topicPageNumber ?? 1;
            this.TopicPageSize = topicPageSize ?? 10;
            this.TopicColumns = topicColumns;
        }
        public FormViewModel(string mode,
            SubTopicQueryModel subTopicQueryModel,
            TopicQueryModel topicQueryModel,
            int? topicPageNumber, int? topicPageSize,
            string topicColumns, int? pageSize,
            int? pageNumber, string columns) :
            this(mode, topicQueryModel,
                topicPageNumber, topicPageSize,
                topicColumns, pageSize,
                pageNumber, columns)
        {
            this.IDSubTopic = subTopicQueryModel.IDSubTopic;
            this.Title = subTopicQueryModel.Title;
            this.Summary = subTopicQueryModel.Summary;
            this.Keywords = subTopicQueryModel.Keywords;
            this.HomeImage = subTopicQueryModel.HomeImage;
        }
        public SubTopicComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDSubTopic != null)
                {
                    return new SubTopicComebackParametersViewModel(
                        iDSubTopic: this.IDSubTopic.Value,
                        iDTopic: this.Topic.IDTopic,
                        topicPageNumber: this.TopicPageNumber,
                        topicPageSize: this.TopicPageSize,
                        topicColumns: this.TopicColumns,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);
                }
                else
                {
                    return new SubTopicComebackParametersViewModel(
                        iDTopic: this.Topic.IDTopic,
                        topicPageNumber: this.TopicPageNumber,
                        topicPageSize: this.TopicPageSize,
                        topicColumns: this.TopicColumns,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        columns: this.Columns);

                }
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/SubTopics/Index/{0}?{1}",
                    this.Topic.IDTopic,
                    this.ComebackParameters.URLTopicParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                    return string.Format(" Listado de subtemas de {0}", this.Topic.Title);
            }
        }
        #endregion


        public AddSubTopicCommand ToAddSubTopicCommand(string solicitedBy)
        {
            var command = new AddSubTopicCommand(
                iDTopic: IDTopic,
                title: Title,
                summary: Summary,
                keywords: Keywords);
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;
        }
        public UpdateSubTopicCommand ToUpdateSubTopicCommand(string solicitedBy)
        {
            if (!IDSubTopic.HasValue)
                throw new Exception("No se envio el Id del subtema.");
            var command = new UpdateSubTopicCommand(
                    iDTopic: IDTopic,
                    iDSubTopic: IDSubTopic.Value,
                    title: Title,
                    summary: Summary,
                    keywords: Keywords);
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;
        }
    }
}