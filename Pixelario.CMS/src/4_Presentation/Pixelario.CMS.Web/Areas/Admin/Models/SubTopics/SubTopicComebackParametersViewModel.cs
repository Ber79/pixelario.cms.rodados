﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.SubTopics
{
    public class SubTopicComebackParametersViewModel
    {
        public int? IDSubTopic { get; set; }
        public int IDTopic { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Columns { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }
        public string TopicColumns { get; set; }
        private SubTopicComebackParametersViewModel()
        {

        }
        public SubTopicComebackParametersViewModel(int? iDSubTopic, 
            int iDTopic,
            int pageNumber, int pageSize, string columns,
            int topicPageNumber, int topicPageSize, string topicColumns) :
            this(iDTopic, pageNumber, pageSize, columns,
                topicPageNumber, topicPageSize, topicColumns)
        {
            this.IDSubTopic = iDSubTopic;
        }
        public SubTopicComebackParametersViewModel(int iDTopic,
            int pageNumber, int pageSize, string columns,
            int topicPageNumber, int topicPageSize, string topicColumns)
        {
            this.IDTopic = iDTopic;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Columns = columns;
            this.TopicPageNumber = topicPageNumber;
            this.TopicPageSize = topicPageSize;
            this.TopicColumns = topicColumns;
        }       
       


        public bool HasIDSubTopic
        {
            get
            {
                return this.IDSubTopic.HasValue;
            }
        }
        public bool HasColumns
        {
            get
            {
                return !string.IsNullOrEmpty(this.Columns);
            }
        }
        public bool HasTopicColumns
        {
            get
            {
                return !string.IsNullOrEmpty(this.TopicColumns);
            }
        }
        public string URLBackParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}{2}",
                    this.TopicPageNumber, this.TopicPageSize,
                    this.HasTopicColumns ?
                        string.Format("&columns={0}", this.TopicColumns) :
                        "");
            }
        }
        public string URLTopicParameters
        {
            get
            {
                return string.Format("topicPageNumber={0}&topicPageSize={1}{2}&pageNumber={3}&pageSize={4}{5}",
                    this.TopicPageNumber, this.TopicPageSize,
                    this.HasTopicColumns ?
                        string.Format("&topicColumns={0}", 
                        this.TopicColumns) :
                        "",
                    this.PageNumber, this.PageSize,
                    this.HasColumns ?
                        string.Format("&columns={0}", 
                        this.Columns) :
                        "");
            }
        }
    }
}