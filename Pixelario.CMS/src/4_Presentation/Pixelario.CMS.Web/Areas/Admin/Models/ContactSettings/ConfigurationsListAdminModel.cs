﻿using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.ContactSettings
{
    public class ConfigurationsListAdminModel
    {
        public string Section { get; set; }
        public List<ConfigurationFormAdminModel> Configurations { get; set; }
    }

}