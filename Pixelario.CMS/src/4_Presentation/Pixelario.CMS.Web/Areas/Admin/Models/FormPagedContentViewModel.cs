﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.Admin.Models
{
    public class FormPagedContentViewModel : PagedContentViewModel
    {
        public string Mode { get; private set; }
        public void SetMode(string mode)
        {
            Mode = mode;
        }
        public bool IsCreate => "create" == Mode;
        public bool IsUpdate => "update" == Mode;
        public bool IsView => "view" == Mode;
    }
}