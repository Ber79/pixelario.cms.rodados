﻿using Pixelario.Subscriptions.Application.QueryModels.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Constraints
{
    public class ConstraintAdminModel
    {
        public int IDConstraint { get; set; }
        public ConstraintType ConstraintType { get; set; }
        public int IDConstrained { get; set; }
        public string ConstrainedTitle { get; private set; }
        public string EditUrl
        {
            get
            {
                var controller = "Editions";
                if (this.ConstraintType == ConstraintType.OnArticle)
                    controller = "Articles";
                return string.Format("/Admin/{0}/Update/{1}",
                    controller, IDConstrained);
            }
        }
        private ConstraintAdminModel()
        {

        }
        public ConstraintAdminModel(ConstraintQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDConstraint = queryModel.IDConstraint;
            this.ConstraintType = queryModel.ConstraintType;
            this.IDConstrained = queryModel.IDConstrained;
        }
        public void SetContrainedTitle(string title)
        {
            this.ConstrainedTitle = title;
        }
    }
}