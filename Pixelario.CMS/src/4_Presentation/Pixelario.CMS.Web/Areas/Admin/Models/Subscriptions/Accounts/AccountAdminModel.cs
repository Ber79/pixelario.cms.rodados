﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Application.QueryModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Accounts
{
    public class AccountAdminModel
    {
        public int IDAccount { get; set; }
        public DateTime CreationDate { get; set; }
        public int UserId { get; set; }
        public Plan Plan { get; set; }
        public string Email { get; private set; }
        private AccountAdminModel()
        {

        }
        public AccountAdminModel(AccountQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDAccount = queryModel.IDAccount;
            this.UserId = queryModel.UserId;
            this.Plan = queryModel.Plan;
        }
        public void SetUserEmail(string email)
        {
            this.Email = email;
        }
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar la cuenta \"{0}\"?') == false) return false;", this.IDAccount);
            }
        }

    }
}