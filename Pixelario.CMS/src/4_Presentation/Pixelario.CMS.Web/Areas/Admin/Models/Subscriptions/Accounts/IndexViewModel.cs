﻿using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Accounts
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<AccountAdminModel> Accounts { get; set; }
    }
}