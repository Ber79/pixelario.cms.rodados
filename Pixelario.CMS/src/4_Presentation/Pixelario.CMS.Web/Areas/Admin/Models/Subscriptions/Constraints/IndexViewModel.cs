﻿using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Constraints
{
    public class IndexViewModel : PagedContentViewModel
    {

        public List<ConstraintAdminModel> Constraints { get; set; }
        public PlanAdminModel Plan { get; set; }
    }
}