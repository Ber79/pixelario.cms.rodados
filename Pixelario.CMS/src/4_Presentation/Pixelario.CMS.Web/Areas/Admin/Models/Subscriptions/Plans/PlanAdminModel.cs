﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Plans
{
    public class PlanAdminModel
    {
        public int IDPlan { get; set; }
        public string Title { get; set; }
        private PlanAdminModel()
        {

        }
        public PlanAdminModel(Plan plan)
        {
            if (plan == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDPlan = plan.Id;
            this.Title = plan.Title;
        }
    }
}