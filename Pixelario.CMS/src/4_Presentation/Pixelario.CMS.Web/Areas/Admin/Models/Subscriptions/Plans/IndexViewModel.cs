﻿using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Plans
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<PlanAdminModel> Plans { get; set; }
    }
}