﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.Logs.Application.QueryModels.LogEvents;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Models.LogEvents
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<LogEventAdminModel> LogEvents { get; set; }
        public string SearchText { get; set; }
        public string Level { get; set; }
        public string LogDay { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }
        public List<SelectListItem> LevelsDropdownListOptions { get; private set; }
        public List<SelectListItem> PageSizesDropdownListOptions { get; private set; }
        public List<SelectListItem> OrderDirectionDropdownListOptions { get; private set; }
        public List<SelectListItem> OrderByDropdownListOptions { get; private set; }

        private IndexViewModel()
        {

        }
        public IndexViewModel(List<LogEventQueryModel> logEventQueryModels,
            string searchText, string level, string logDay,
            int totalCount, int pageNumber,
            int pageSize, int orderBy,
            int orderDirection, int pagerItems)
        {
            this.LogEvents = new List<LogEventAdminModel>();
            foreach (var queryModel in logEventQueryModels)
            {
                this.LogEvents.Add(new LogEventAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);
            this.SearchText = searchText;
            this.Level = level;
            this.LogDay = logDay;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
            this.CalculatePagerItems(pagerItems);
            this.SetLevelsDropdownListOptions();
            this.SetPageSizesDropdownListOptions();
            this.SetOrderDirectionDropdownListOptions();
            this.SetOrderByDropdownListOptions();
        }
        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    searchText: this.SearchText,
                    level: this.Level,
                    logDay:  this.LogDay,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
            }
        }
        public ComebackParametersViewModel GetComebackParameters(int id)
        {
            return new ComebackParametersViewModel(
                    iDLogEvent: id,
                    searchText: this.SearchText,
                    level: this.Level,
                    logDay: this.LogDay,
                    pageNumber: this.PageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
        }
        public ComebackParametersViewModel GetComebackParametersForPage(int pageNumber)
        {
            return new ComebackParametersViewModel(
                    searchText: this.SearchText,
                    level: this.Level,
                    logDay: this.LogDay,
                    pageNumber: pageNumber,
                    pageSize: this.PageSize,
                    orderBy: this.OrderBy,
                    orderDirection: this.OrderDirection);
        }

        #region FirstPageLink
        public string FirstPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(1);
            return string.Format("/Admin/LogEvents/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string FirstPageTitle
        {
            get
            {
                return "Primera página";
            }
        }
        #endregion
        #region ToPageLink
        public string ToPageURL(int pageNumber)
        {
            var comebackParameters = this.GetComebackParametersForPage(pageNumber);
            return string.Format("/Admin/LogEvents/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string ToPageTitle(int pageNumber)
        {

            return string.Format("Página {0}", pageNumber);

        }
        #endregion


        #region BackPageLink
        public string BackPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.BackPage);
            return string.Format("/Admin/LogEvents/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string BackPageTitle
        {
            get
            {
                return "Anterior página";
            }
        }
        #endregion
        #region NextPageLink
        public string NextPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.NextPage);
            return string.Format("/Admin/LogEvents/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string NextPageTitle
        {
            get
            {
                return "Siguiente página";
            }
        }
        #endregion
        #region LastPageLink
        public string LastPageURL()
        {
            var comebackParameters = this.GetComebackParametersForPage(this.PageCount);
            return string.Format("/Admin/LogEvents/Index?{0}",
                comebackParameters.URLParameters);
        }
        public string LastPageTitle
        {
            get
            {
                return "Última página";
            }
        }
        #endregion

        public void SetLevelsDropdownListOptions()
        {
            this.LevelsDropdownListOptions = new List<SelectListItem>(){
                new SelectListItem() {
                    Value = "-1",
                    Text = "Seleccione un nivel"
            }};
            foreach (var level in Enum.GetValues(typeof(LogEventLevel)).Cast<LogEventLevel>())
            {
                this.LevelsDropdownListOptions.Add(new SelectListItem()
                {
                    Value = Enum.GetName(typeof(LogEventLevel), level),
                    Text = Enum.GetName(typeof(LogEventLevel), level)
                });
            }
        }
        public void SetPageSizesDropdownListOptions()
        {
            this.PageSizesDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="5",
                    Text = "5"
                },
                new SelectListItem() {
                    Value="10",
                    Text = "10"
                },
                new SelectListItem() {
                    Value="25",
                    Text = "25"
                },
                new SelectListItem() {
                    Value="100",
                    Text = "100"
                }
            };
        }
        public void SetOrderDirectionDropdownListOptions()
        {
            this.OrderDirectionDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="0",
                    Text = "Ascendente"
                },
                new SelectListItem() {
                    Value="1",
                    Text = "Descendente"
                }
            };
        }

        public void SetOrderByDropdownListOptions()
        {
            this.OrderByDropdownListOptions = new List<SelectListItem>() {
                new SelectListItem() {
                    Value="0",
                    Text = "Fecha de creación"
                },
                new SelectListItem() {
                    Value="1",
                    Text = "Nivel"
            }};
        }
    }
}