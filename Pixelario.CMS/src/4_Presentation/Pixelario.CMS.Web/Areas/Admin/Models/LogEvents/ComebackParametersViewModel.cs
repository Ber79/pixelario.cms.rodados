﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.LogEvents
{
    public class ComebackParametersViewModel
    {
        public int? IDLogEvent { get; set; }
        public string SearchText { get; set; }
        public string Level { get; set; }
        public string LogDay { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }
        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDLogEvent,
            string searchText, string level, string logDay,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection
            ) : this(searchText, level, logDay,
                pageNumber, pageSize, orderBy,
                orderDirection)
        {
            this.IDLogEvent = iDLogEvent;
        }       
       
        public ComebackParametersViewModel(
            string searchText, string level, string logDay,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection)
        {
            this.SearchText = searchText;
            this.Level = level;
            this.LogDay = logDay;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
        }
        public bool HasIDLogEvent
        {
            get
            {
                return this.IDLogEvent.HasValue;
            }
        }
       
        public string URLParameters
        {
            get
            {
                return string.Format("searchText={0}&level={1}&logDay={2}&pageNumber={3}&pageSize={4}&orderBy={5}&orderDirection={6}",
                    this.SearchText, this.Level, this.LogDay,
                    this.PageNumber, this.PageSize,
                    this.OrderBy, this.OrderDirection);
            }
        }
    }
}