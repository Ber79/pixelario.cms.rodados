﻿using Pixelario.Logs.Application.Commands.LogEvents;
using System;
using System.Collections.Generic;
using Pixelario.Logs.Application.QueryModels.LogEvents;

namespace Pixelario.CMS.Web.Areas.Admin.Models.LogEvents
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDLogEvent { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Exception { get; set; }

        public string SearchText { get; set; }
        public string LevelParameter { get; set; }
        public string LogDay { get; set; }
        public int OrderBy { get; set; }
        public int OrderDirection { get; set; }


        public FormViewModel()
        {

        }
        public FormViewModel(string mode,
            string searchText, string level, string logDay,
            int pageNumber,
            int pageSize, int orderBy,
            int orderDirection)
        {
            this.SetMode(mode);
            this.SearchText = searchText;
            this.LevelParameter = level;
            this.LogDay = logDay;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.OrderBy = orderBy;
            this.OrderDirection = orderDirection;
        }
        public FormViewModel(
            string mode,
            LogEventQueryModel logEventQueryModel,
            string searchText,
            string level, string logDay,
            int pageNumber,
            int pageSize, 
            int orderBy,
            int orderDirection
            ) : this(mode, searchText, level, logDay,
                pageNumber, pageSize, orderBy, orderDirection)
        {
            this.IDLogEvent = logEventQueryModel.IDLogEvent;
            this.Level = logEventQueryModel.Level;
            this.TimeStamp = logEventQueryModel.TimeStamp;
            this.Message = logEventQueryModel.Message;
            this.Exception = logEventQueryModel.Exception;
        }
       

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.IDLogEvent.HasValue)
                    return new ComebackParametersViewModel(
                        iDLogEvent: this.IDLogEvent.Value,
                        searchText: this.SearchText,
                        level: this.Level,
                        logDay: this.LogDay,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection);
                else
                    return new ComebackParametersViewModel(
                        searchText: this.SearchText,
                        level: this.Level,
                        logDay: this.LogDay,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize,
                        orderBy: this.OrderBy,
                        orderDirection: this.OrderDirection);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                return string.Format("/Admin/LogEvents?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                return "Listado de logs";
            }
        }
        #endregion
    }
}