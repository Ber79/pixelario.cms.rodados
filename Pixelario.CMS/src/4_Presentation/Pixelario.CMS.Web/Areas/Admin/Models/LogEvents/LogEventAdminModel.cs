﻿using Pixelario.Logs.Application.QueryModels.LogEvents;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.LogEvents
{
    public class LogEventAdminModel
    {
        public int IDLogEvent { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
        private LogEventAdminModel()
        {
        }
        public LogEventAdminModel(LogEventQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDLogEvent = queryModel.IDLogEvent;
            this.Level = queryModel.Level;
            this.Message = queryModel.Message;
            this.TimeStamp = queryModel.TimeStamp;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el log \"{0}\"?') == false) return false;", this.IDLogEvent);
            }
        }
        #endregion
        #region ViewLink
        public string ViewLinkURL(ComebackParametersViewModel comebackParameters)
        {
            return string.Format("/Admin/LogEvents/View/{0}?{1}",
                this.IDLogEvent, comebackParameters.URLParameters);
        }
        public string ViewLinkTitle
        {
            get
            {
                return "Ver log";
            }
        }
        #endregion
    }
}