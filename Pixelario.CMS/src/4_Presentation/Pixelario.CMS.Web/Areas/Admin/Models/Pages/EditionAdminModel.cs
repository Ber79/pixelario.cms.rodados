﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class EditionAdminModel
    {
        public int IDEdition { get; set; }
        public string Title { get; set; }
        private EditionAdminModel()
        {

        }
        public EditionAdminModel(EditionQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDEdition = queryModel.IDEdition;
            this.Title = queryModel.Title;
        }
    }
}