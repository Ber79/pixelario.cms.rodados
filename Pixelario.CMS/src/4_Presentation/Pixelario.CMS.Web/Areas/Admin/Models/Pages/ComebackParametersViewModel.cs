﻿namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class ComebackParametersViewModel
    {
        public int? IDEdition { get; set; }
        public int? IDPage { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }

        private ComebackParametersViewModel()
        {

        }
        public ComebackParametersViewModel(int iDPage, int iDEdition, int editionPageNumber, int editionPageSize,
           string editionColumns, int pageNumber,
           int pageSize) : this(iDEdition, editionPageNumber, editionPageSize,
               editionColumns, pageNumber, pageSize)
        {
            this.IDPage = iDPage;
        }
        public ComebackParametersViewModel(int iDEdition, int editionPageNumber, int editionPageSize,
            string editionColumns, int pageNumber,
            int pageSize) : this(pageNumber, pageSize)
        {
            this.IDEdition = iDEdition;
            this.EditionPageNumber = editionPageNumber;
            this.EditionPageSize = editionPageSize;
            this.EditionColumns = editionColumns;
        }
        public ComebackParametersViewModel(int iDPage, int pageNumber,
            int pageSize) : this(pageNumber, pageSize)
        {
            this.IDPage = iDPage;
        }
        public ComebackParametersViewModel(int pageNumber,
            int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }


        public bool HasIDEdition
        {
            get
            {
                return this.IDEdition.HasValue;
            }
        }
        public bool HasIDPage
        {
            get
            {
                return this.IDPage.HasValue;
            }
        }
        public string URLParameters
        {
            get
            {
                return string.Format("pageNumber={0}&pageSize={1}",
                    this.PageNumber, this.PageSize);
            }
        }
        public string URLFullParameters
        {
            get
            {
                return string.Format("editionPageNumber={0}&editionPageSize={1}{2}&pageNumber={0}&pageSize={1}",
                    this.EditionPageNumber, this.EditionPageSize,
                    !string.IsNullOrEmpty(this.EditionColumns) ?
                        string.Format("&editionColumns={0}", this.EditionColumns) :
                        "",
                    this.PageNumber, this.PageSize);
            }
        }
    }
}