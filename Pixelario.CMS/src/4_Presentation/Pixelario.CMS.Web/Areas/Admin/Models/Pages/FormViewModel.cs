﻿using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Pages;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class FormViewModel : FormPagedContentViewModel
    {
        public int? IDPage { get; set; }
        public EditionAdminModel Edition { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Keywords { get; set; }
        public string HomeImage { get; set; }
        public bool HasHomeImage
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage);
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public List<EditionQueryModel> AvailableEditions { get; set; }
        public List<int> Editions { get; set; }

        public List<PageMultimediaAdminModel> Multimedia { get; set; }

        public string MultimediaTitle { get; set; }
        public string MultimediaLink { get; set; }

        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }
        public FormViewModel()
        {

        }

        private FormViewModel(string mode, int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber)
        {
            this.SetMode(mode);
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
        }
        public FormViewModel(
           string mode,
           EditionQueryModel editionQueryModel,
           PageQueryModel pageQueryModel,
           int? editionPageNumber, int? editionPageSize,
           string editionColumns, int? pageSize,
           int? pageNumber) : this(mode, editionPageNumber, editionPageSize,
               editionColumns, pageSize, pageNumber)
        {
            if (editionQueryModel != null)
                this.Edition = new EditionAdminModel(queryModel: editionQueryModel);         
            this.IDPage = pageQueryModel.IDPage;
            this.Title = pageQueryModel.Title;
            this.Summary = pageQueryModel.Summary;
            this.Body = pageQueryModel.Body;
            this.Keywords = pageQueryModel.Keywords;
        }
        public FormViewModel(string mode,
            EditionQueryModel editionQueryModel,
            List<EditionQueryModel> availableEditions,
            List<int> editions, int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber) : this(mode, editionPageNumber, editionPageSize,
                editionColumns, pageSize, pageNumber)
        {
            if (editionQueryModel != null)
                this.Edition = new EditionAdminModel(queryModel: editionQueryModel);
            this.AvailableEditions = availableEditions;
            this.Editions = editions;
        }
        public FormViewModel(
           string mode, 
           EditionQueryModel editionQueryModel,
           List<EditionQueryModel> availableEditions,
           List<int> editions,
           int iDPage, string title,
           string summary, string body,
           string keywords, int? editionPageNumber, int? editionPageSize,
           string editionColumns, int? pageSize,
           int? pageNumber) : this(mode, editionQueryModel,
               availableEditions, editions,
               editionPageNumber, editionPageSize,
               editionColumns, pageSize, pageNumber)
        {
            this.IDPage = iDPage;
            this.Title = title;
            this.Summary = summary;
            this.Body = body;
            this.Keywords = keywords;
        }

        public FormViewModel(
            string mode,
            EditionQueryModel editionQueryModel,
            PageQueryModel pageQueryModel, List<EditionQueryModel> availableEditions,
            List<int> editions,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber) : this(mode, editionQueryModel,
               availableEditions, editions,
               editionPageNumber, editionPageSize,
               editionColumns, pageSize, pageNumber)
        {
            this.IDPage = pageQueryModel.IDPage;
            this.Title = pageQueryModel.Title;
            this.Summary = pageQueryModel.Summary;
            this.Body = pageQueryModel.Body;
            this.Keywords = pageQueryModel.Keywords;
        }

        public FormViewModel(
            string mode,
            EditionQueryModel editionQueryModel,
            PageQueryModel pageQueryModel,
            List<PageMultimediaQueryModel> pageMultimediaQueryModels,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber) : this(mode, 
                editionQueryModel, pageQueryModel,
                editionPageNumber, editionPageSize,
                editionColumns, pageSize, pageNumber)
        {
            this.HomeImage = pageQueryModel.HomeImage;
            this.MultimediaTitle = pageQueryModel.Title;
            this.Multimedia = new List<PageMultimediaAdminModel>();
            foreach (var queryModel in pageMultimediaQueryModels)
            {
                this.Multimedia.Add(new PageMultimediaAdminModel(
                    queryModel: queryModel));
            }
        }
        public FormViewModel(string mode,
            AddCommand command,
            EditionQueryModel editionQueryModel,
            List<EditionQueryModel> availableEditions,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber) : this(mode,editionPageNumber, editionPageSize,
            editionColumns, pageSize, pageNumber)
        {
            if (editionQueryModel != null)
                this.Edition = new EditionAdminModel(
                    queryModel: editionQueryModel);
            this.Title = command.Title;
            this.Summary = command.Summary;
            this.Body = command.Body;
            this.Keywords = command.Keywords;
            this.Editions = command.Editions;
            this.AvailableEditions = availableEditions;
            if(command.Editions != null)
            {
                this.Editions = command.Editions;
            }
            else
            {
                this.Editions = new List<int>();
            }
        }

        public FormViewModel(
            string mode,
            EditionQueryModel editionQueryModel,
            UpdateCommand command, List<EditionQueryModel> availableEditions,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber) : this(mode, 
               editionPageNumber, editionPageSize,
               editionColumns, pageSize, pageNumber)
        {
            if (editionQueryModel != null)
                this.Edition = new EditionAdminModel(
                    queryModel: editionQueryModel);
            this.IDPage = command.IDPage;
            this.Title = command.Title;
            this.Summary = command.Summary;
            this.Body = command.Body;
            this.Keywords = command.Keywords;
            this.AvailableEditions = availableEditions;
            if (command.Editions != null)
            {
                this.Editions = command.Editions;
            }
            else
            {
                this.Editions = new List<int>();
            }
        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.Edition != null)
                {
                    if (this.IDPage.HasValue)
                        return new ComebackParametersViewModel(
                            iDPage: this.IDPage.Value,
                            iDEdition: this.Edition.IDEdition,
                            editionPageNumber: this.EditionPageNumber,
                            editionPageSize: this.EditionPageSize,
                            editionColumns: this.EditionColumns,
                            pageNumber: this.PageNumber,
                            pageSize: this.PageSize);
                    else
                        return new ComebackParametersViewModel(                        
                            iDEdition: this.Edition.IDEdition,
                            editionPageNumber: this.EditionPageNumber,
                            editionPageSize: this.EditionPageSize,
                            editionColumns: this.EditionColumns,
                            pageNumber: this.PageNumber,
                            pageSize: this.PageSize);
                }
                else
                {
                    if (this.IDPage.HasValue)
                        return new ComebackParametersViewModel(
                            iDPage: this.IDPage.Value,
                            pageNumber: this.PageNumber,
                            pageSize: this.PageSize);
                    else
                        return new ComebackParametersViewModel(
                            pageNumber: this.PageNumber,
                            pageSize: this.PageSize);
                }
            }
        }
        public ComebackParametersViewModel EditionComebackParameters
        {
            get
            {
                return new ComebackParametersViewModel(
                    pageNumber: this.EditionPageNumber,
                    pageSize: this.EditionPageSize);
            }
        }
        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                if (this.Edition != null)
                    return string.Format("/Admin/Editions/Pages/{0}?{1}",
                        this.Edition.IDEdition,
                        this.ComebackParameters.URLFullParameters);
                else
                    return string.Format("/Admin/Pages?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                if (this.Edition != null)
                    return string.Format(" Listado de páginas de {0}", this.Edition.Title);
                else
                    return "Listado de páginas";
            }
        }
        #endregion


        public AddCommand ToAddCommand(string createdBy)
        {
            if (Editions == null)
                throw new Exception("La página debe estar asignado al menos a una edición.");            
            var command = new AddCommand(
                title: Title,
                summary: Summary, 
                body: Body,
                keywords: Keywords,
                editions: Editions);
            command.SetCreatedBy(
                createdBy: createdBy);
            return command;
        }
        public UpdateCommand ToUpdateCommand(string solicitedBy)
        {
            if (!IDPage.HasValue)
                throw new Exception("No se envio el id de la página.");
            if (Editions == null)
                throw new Exception("La página debe estar asignado al menos a una edición.");
            var command = new UpdateCommand(
                    iDPage: IDPage.Value,
                    title: Title,
                    summary: Summary,
                    body: Body,
                    keywords: Keywords,
                    editions: Editions);
            command.SetSolicitedBy(
                solicitedBy: solicitedBy);
            return command;
        }
    }
}