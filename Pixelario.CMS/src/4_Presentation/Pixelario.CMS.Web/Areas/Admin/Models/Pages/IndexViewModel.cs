﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class IndexViewModel : PagedContentViewModel
    {
        public List<PageAdminModel> Pages { get; set; }
        
        public IndexViewModel(List<PageQueryModel> pageQueryModels,
            int totalCount, int? pageNumber,
            int? pageSize)
        {
            this.Pages = new List<PageAdminModel>();
            foreach (var queryModel in pageQueryModels)
            {
                this.Pages.Add(new PageAdminModel(
                    queryModel: queryModel));
            }
            this.SetPagingItems(
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: null);

        }
    }
}