﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class PageAdminModel
    {
        public int IDPage { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        private PageAdminModel()
        {
        }
        public PageAdminModel(PageQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDPage = queryModel.IDPage;
            this.Title = queryModel.Title;
            this.Enabled = queryModel.Enabled;
            this.AtHome = queryModel.AtHome;
            this.Order = queryModel.Order;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar la página \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion
        #region Publish button
        public string ToHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada la página \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada la página \"{0}\"?') == false) return false;", this.Title);
            }
        }
        public string ToHomeButtonClass
        {
            get
            {
                return this.AtHome ? "fa-check" : "fa-times";
            }
        }
        public string ToHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Página en portada" : "Página fuera de portada";
            }
        }
        #endregion
        #region Enable button
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar la página \"{0}\"?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar la página \"{0}\"?') == false) return false;", this.Title);
            }
        }

        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "fa-check" : "fa-times";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Página habilitado" : "Página deshabilitado";
            }
        }

        #endregion

        #region Preview Button

        public string PreviewURL
        {
            get
            {
                return string.Format("/preview/pages/Index/{0}",
                    this.IDPage);
            }
        }
        #endregion
    }
}