﻿using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Web.Areas.Admin.Models.Configurations;
using System.Collections.Generic;
namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class ConfigurationsViewModel : FormPagedContentViewModel
    {
        public string PageTitle { get; set; }
        public string ControllerName { get; set; }
        public string Module { get; set; }
        public EditionAdminModel Edition { get; set; }

        public List<ConfigurationsListAdminModel> ConfigurationsLists { get; set; }
        public List<string> TypeOfEnumerations { get; set; }
        public int EditionPageNumber { get; set; }
        public int EditionPageSize { get; set; }
        public string EditionColumns { get; set; }

        public ConfigurationsViewModel(
            string module,
            EditionQueryModel editionQueryModel,
            List<ConfigurationsListAdminModel> configurationsLists,
            int? editionPageNumber, int? editionPageSize,
            string editionColumns, int? pageSize,
            int? pageNumber)
        {
            this.Module = module;
            if (editionQueryModel != null)
                this.Edition = new EditionAdminModel(queryModel: editionQueryModel);
            this.ConfigurationsLists = configurationsLists;
            this.EditionPageNumber = editionPageNumber ?? 1;
            this.EditionPageSize = editionPageSize ?? 10;
            this.EditionColumns = editionColumns;
            this.PageNumber = pageNumber ?? 1;
            this.PageSize = pageSize ?? 10;
        }

        public ComebackParametersViewModel ComebackParameters
        {
            get
            {
                if (this.Edition != null)
                {

                    return new ComebackParametersViewModel(
                        iDEdition: this.Edition.IDEdition,
                        editionPageNumber: this.EditionPageNumber,
                        editionPageSize: this.EditionPageSize,
                        editionColumns: this.EditionColumns,
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
                }
                else
                {

                    return new ComebackParametersViewModel(
                        pageNumber: this.PageNumber,
                        pageSize: this.PageSize);
                }
            }
        }

        #region BackButton
        public string BackButtonUrl
        {
            get
            {
                if (this.Edition != null)
                    return string.Format("/Admin/Editions/Pages/{0}?{1}",
                        this.Edition.IDEdition,
                        this.ComebackParameters.URLFullParameters);
                else
                    return string.Format("/Admin/Pages?{0}",
                    this.ComebackParameters.URLParameters);
            }
        }
        public string BackButtonTitle
        {
            get
            {
                if (this.Edition != null)
                    return string.Format(" Listado de páginas de {0}", this.Edition.Title);
                else
                    return "Listado de páginas";
            }
        }
        #endregion
    }
}