﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System;

namespace Pixelario.CMS.Web.Areas.Admin.Models.Pages
{
    public class PageMultimediaAdminModel
    {
        public int IDMultimedia { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public int IDMultimediaType { get; set; }
        public string Path1 { get; set; }
        public string GetImage(string width)
        {
            if (string.IsNullOrEmpty(this.Path1)) return null;
            return this.Path1.Replace("/80/",
                string.Format("/{0}/", width));

        }
        private PageMultimediaAdminModel()
        {
        }
        public PageMultimediaAdminModel(PageMultimediaQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.IDMultimedia = queryModel.IDMultimedia;
            this.Title = queryModel.Title;
            this.IDMultimediaType = queryModel.IDMultimediaType;
            this.Path1 = queryModel.Path1;
            this.Code = queryModel.Summary;
        }
        #region Delete button
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar el multimedia \"{0}\"?') == false) return false;", this.Title);
            }
        }
        #endregion


    }
}