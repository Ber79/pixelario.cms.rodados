﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Pixelario.CMS.Web.Areas.Admin.Models.WebSiteSettings
{
    public class ConfigurationFormAdminModel
    {
        public int ID { get; set; }
        public string Setting { get; set; }
        public string State { get; set; }
        public string Title { get; set; }
        public string Section { get; set; }
        public ConfigurationType ConfigurationType { get; set; }
        public List<SelectListItem> Options { get; set; }
        private ConfigurationFormAdminModel()
        {
        }
        public ConfigurationFormAdminModel(ConfigurationQueryModel queryModel)
        {
            if (queryModel == null)
                throw new Exception("No se puede parsear el modelo.");
            this.ID = queryModel.IDConfiguration;
            this.Title = queryModel.Title;
            this.ConfigurationType = queryModel.ConfigurationType;
            if(queryModel.ConfigurationType == ConfigurationType.EnumerationClass)
            {
                this.Options = new List<SelectListItem>();
                foreach(var option in queryModel.ConfigurationOptions)
                {
                    this.Options.Add(new SelectListItem() { 
                        Value = option.Value,
                        Text = option.Value,
                        Selected = option.Value == queryModel.State
                    });
                }
            }
            this.Section = queryModel.Section;
            this.State = queryModel.State;
            this.Setting = queryModel.Setting;

        }

        public string InputTagID
        {
            get
            {
                return string.Format("{0}-{1}",
                    this.Section, this.ID);
            }
        }

    }
}