﻿using MediatR;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Pixelario.CMS.Web.Areas.Admin.Models.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class MenuController : BaseController
    {
        private readonly string[] ConfigurationModules = { "home/menu/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        public MenuController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));

            var configurationItems = new List<ConfigurationFormAdminModel>();
            foreach (var configurationItem in configurations.Where(
                c => (
                    userRoles.Contains("Super Administrador") ||
                    c.GetBindedRoles().Intersect(userRoles).Any()
                    )))
            {
                configurationItems.Add(new ConfigurationFormAdminModel(
                    queryModel: configurationItem));
            }
            return View("~/Areas/Admin/Views/Configurations/ConfigurationsForm.cshtml",
                           new ConfigurationsListViewModel()
                           {
                               PageTitle = "Configuraciones del menú",
                               ControllerName = "Menu",
                               Module = "pages",
                               ConfigurationsLists = new List<ConfigurationsListAdminModel>() {
                    new ConfigurationsListAdminModel()
                    {
                        Section = "menu",
                        Configurations = configurationItems
                    }
            }
                           });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys)
            {
                if (setting != "section")
                {
                    var key = string.Format("home/{0}/{1}",
                        section, setting);
                    var state = collection[setting];
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            return RedirectToAction("Configurations");
        }

        #endregion
    }
}