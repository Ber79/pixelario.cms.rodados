﻿using MediatR;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.Web.Areas.Admin.Models.WebSiteSettings;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class WebSiteSettingsController : ContentController
    {

        private readonly string[] ConfigurationModules = { "website/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IImageServices _imageServices;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        public WebSiteSettingsController(
            ILogger logger,
            IMediator mediator,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations(int? pageNumber,
            int? pageSize,
            string columnOrder,
            string orderDirection,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules,
                    configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));

            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) &&
                                c.Key.IndexOf(
                                    string.Format("/{0}/", section)) > -1))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }

                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }

            return View(new ConfigurationsViewModel(
                    module: "home",
                    configurationsLists: configurationsLists,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    columns: columns
                ));
        }
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize" &&
                    key != "Columns"))
            {
                if (setting != "section")
                {
                    var state = collection[setting];
                    var key = string.Format("website/{0}/{1}",
                        section, setting);
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadImage(FormCollection collection)
        {
            var section = collection["Section"];
            var setting = collection["Setting"];
            string[] configurationsToFind = {
                string.Format("{0}{1}/folder",
                    ConfigurationModules.Take(1).FirstOrDefault(), section),
                string.Format("{0}{1}/filename",
                    ConfigurationModules.Take(1).FirstOrDefault(), section),
            };
            var configurationsNeeded = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: configurationsToFind,
                    configurationType: null, rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["Image"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extention = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extention.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/configurations/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extention);
                    string graphicFile = Path.Combine(appDomainAppPath,
                        string.Format("uploads\\configurations\\raw\\{0}",
                            fileName));
                    string newFile = Path.Combine(appDomainAppPath,
                        string.Format("uploads\\{0}\\{1}{2}",
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[0]).State,
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[1]).State,
                            extention));
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    string pathDirectory = Server.MapPath(string.Format("~/uploads/{0}",
                        configurationsNeeded.Find(c => c.Key == configurationsToFind[0]).State));
                    if (!Directory.Exists(pathDirectory))
                        Directory.CreateDirectory(pathDirectory);

                    _imageServices.Salvar(graphicFile,
                                newFile, 260);
                    var commandFile = new ConfigurationCommands.SetStateCommand(
                        key: string.Format("{0}{1}/{2}",
                            ConfigurationModules.Take(1).FirstOrDefault(), section, setting),
                        state: string.Format("/uploads/configurations/raw/{0}", fileName));
                    commandFile.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(commandFile);
                    if (response.success)
                    {
                        var commandExtention = new ConfigurationCommands.SetStateCommand(
                            key: string.Format("{0}{1}/{2}-extention",
                                ConfigurationModules.Take(1).FirstOrDefault(), section, setting),
                            state: extention);
                        commandExtention.SetSolicitedBy(User.Identity.Name);
                        var responseCommandExtention = await _mediator.Send(commandExtention);
                        if (responseCommandExtention.success)
                        {
                            TempData["success"] = "El archivo fue subido exitosamente.";
                        }
                        else
                        {
                            TempData["errors"] = response.errors;
                        }
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];

            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadFile(FormCollection collection)
        {
            var section = collection["Section"];
            var setting = collection["Setting"];
            string[] configurationsToFind = {
                string.Format("{0}{1}/folder",
                    ConfigurationModules.Take(1).FirstOrDefault(), section),
                string.Format("{0}{1}/filename",
                    ConfigurationModules.Take(1).FirstOrDefault(), section),
            };
            var configurationsNeeded = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: configurationsToFind, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["File"];
            if (file != null)
            {
                try
                {
                    string rawName = file.FileName;
                    byte[] buf = new byte[file.ContentLength];
                    file.InputStream.Read(buf, 0, file.ContentLength);
                    string extention = Path.GetExtension(rawName);
                    string rawPath = Server.MapPath(string.Format("~/uploads/{0}/",
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[0]).State));
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    string path = Path.Combine(appDomainAppPath, string.Format("uploads\\{0}\\{1}{2}",
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[0]).State,
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[1]).State,
                            extention));
                    file.SaveAs(path);
                    var command = new ConfigurationCommands.SetStateCommand(
                        key: string.Format("{0}{1}/{2}",
                            ConfigurationModules.Take(1).FirstOrDefault(), section, setting),
                        state: string.Format("/uploads/{0}/{1}{2}",
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[0]).State,
                            configurationsNeeded.Find(c => c.Key == configurationsToFind[1]).State,
                            extention));
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        var commandExtention = new ConfigurationCommands.SetStateCommand(
                            key: string.Format("{0}{1}/{2}-extention",
                                ConfigurationModules.Take(1).FirstOrDefault(),
                                section, setting),
                            state: extention);
                        commandExtention.SetSolicitedBy(User.Identity.Name);
                        var responseCommandExtention = await _mediator.Send(commandExtention);
                        if (responseCommandExtention.success)
                        {
                            TempData["success"] = "El archivo fue subido exitosamente.";
                        }
                        else
                        {
                            TempData["errors"] = response.errors;
                        }
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                catch (Exception ex)
                {
                    var errors = new List<string>() { };
                    errors.Add(ex.Message);
                    if (ex.InnerException != null)
                        errors.Add(ex.InnerException.Message);
                    TempData["errors"] = errors;
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Enumeration(FormCollection collection)
        {
            var section = collection["Section"];
            var setting = collection["Setting"];
            var enumerationCommand = new ConfigurationCommands.SetStateCommand(
                key: string.Format("website/{0}/{1}", section, setting),
                state: collection["Value"]);
            enumerationCommand.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var enumerationCommandResponse = await _mediator.Send(enumerationCommand);
            if (enumerationCommandResponse.success)
            {                
                TempData["success"] = "La opción se eligió exitosamente.";                   
            }
            else
            {
                TempData["errors"] = enumerationCommandResponse.errors;
            }
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        #endregion
    }
}