﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.CacheRegisters;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using CacheRegisterCommands = Pixelario.CMS.Application.Commands.CacheRegisters;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers.CacheRegisters
{
    [Authorize(Roles = "Super Administrador")]
    public class CacheRegistersController : ContentController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        public CacheRegistersController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/CacheRegister
        public async Task<ActionResult> Index(
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                var totalCacheRegisters = await _mediator.Send(
                    new CacheRegisterCommands.CountCommand());
                var _pageNumber = pageNumber ?? 1;
                var _pageSize = pageSize ?? 10;
                var cacheRegisterKeyValues = await _mediator.Send(new CacheRegisterCommands.ListCommand(
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize));

                var model = new IndexViewModel(
                    cacheRegisterKeyValues: cacheRegisterKeyValues,
                    totalCount: totalCacheRegisters,
                    pageSize: _pageSize,
                    pageNumber: _pageNumber,
                    pagerItems: 5
                );
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                TempData["errors"] = new List<string>() { ex.Message };
                return RedirectToAction("Start", "Home");
            }
        }
        #endregion

    }
}