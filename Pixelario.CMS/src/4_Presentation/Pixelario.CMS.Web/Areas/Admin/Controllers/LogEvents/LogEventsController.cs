﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.LogEvents;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LogEventCommands = Pixelario.Logs.Application.Commands.LogEvents;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers.LogEvents
{
    [Authorize(Roles = "Super Administrador")]
    public class LogEventsController : ContentController
    {


        private ILogger _logger;
        private readonly IMediator _mediator;

        public LogEventsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/LogEvent
        public async Task<ActionResult> Index(
            string searchText, string level,
            string logDay,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                DateTime? timeStampFrom = null;
                DateTime? timeStampTo = null;
                    DateTime LogDay = DateTime.Now;
                if(!string.IsNullOrEmpty(logDay) &&
                        DateTime.TryParseExact(logDay, "yyyy-MM-dd",
                        System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None,
                        out LogDay))
                {
                    timeStampFrom = LogDay.Date;
                    timeStampTo = LogDay.AddDays(1).Date;
                }
                if (level == "-1")
                    level = null;
                var totalLogEvents = await _mediator.Send(new LogEventCommands.CountCommand(
                    filteredBy: HttpUtility.UrlDecode(searchText),
                    level: level,
                    timeStampFrom: timeStampFrom,
                    timeStampTo: timeStampTo));
                var _pageNumber = pageNumber ?? 1;
                var _pageSize = pageSize ?? 10;
                var logEventQueryModels = await _mediator.Send(new LogEventCommands.ListCommand(
                    filteredBy: HttpUtility.UrlDecode(searchText),
                    level: level,
                    timeStampFrom: timeStampFrom,
                    timeStampTo: timeStampTo,
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize,
                    columnOrder: orderBy,
                    orderDirection: orderDirection));

                var model = new IndexViewModel(
                    logEventQueryModels: logEventQueryModels,
                    totalCount: totalLogEvents,
                    searchText: HttpUtility.UrlDecode(searchText),
                    level: level,
                    logDay: logDay,
                    pageSize: _pageSize,
                    pageNumber: _pageNumber,
                    orderBy: orderBy ?? 0,
                    orderDirection: orderDirection ?? 1,
                    pagerItems: 5
                );
                model.CalculatePagerItems();
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                TempData["errors"] = new List<string>() { ex.Message };
                return RedirectToAction("Start", "Home");
            }
        }

        // POST: Admin/Configutationd
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            return RedirectToAction("Index", new
            {
                pageNumber = 1,
                pageSize = collection["PageSize"],
                searchText = HttpUtility.UrlEncode(collection["SearchText"]),
                level = collection["Level"],
                logDay = collection["LogDay"],
                orderBy = collection["OrderBy"],
                orderDirection = collection["OrderDirection"]
            });
        }


        //[HttpPost]
        //public async Task<ActionResult> Delete(LogEventCommands.DeleteCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (response.success)
        //    {
        //        TempData["success"] = "La configuración fue borrada exitosamente.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = response.errors;
        //    }
        //    var pageNumber = 1;
        //    if (this.Request.Params["PageNumber"] != null)
        //        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
        //    var pageSize = 10;
        //    if (this.Request.Params["PageSize"] != null)
        //        int.TryParse(this.Request.Params["PageSize"], out pageSize);
        //    var searchText = this.Request.Params["SearchText"];
        //    var logEventType = -1;
        //    if (this.Request.Params["LogEventTypeParameter"] != null)
        //        int.TryParse(this.Request.Params["LogEventTypeParameter"], out logEventType);
        //    var orderBy = 0;
        //    if (this.Request.Params["OrderBy"] != null)
        //        int.TryParse(this.Request.Params["OrderBy"], out orderBy);
        //    var orderDirection = 1;
        //    if (this.Request.Params["OrderDirection"] != null)
        //        int.TryParse(this.Request.Params["OrderDirection"], out orderDirection);
        //    return RedirectToAction("Index", new
        //    {
        //        pageNumber,
        //        pageSize,
        //        searchText,
        //        logEventType,
        //        orderBy,
        //        orderDirection
        //    });
        //}
        #endregion
        //Get Admin/LogEvents/Update
        public async Task<ActionResult> View(int id,
            string searchText, string level, string logDay,
            DateTime? timeStampFrom, DateTime? timeStampTo,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var logEventQueryModel = await _mediator.Send(new LogEventCommands.GetCommand(
                iDLogEvent: id));
            if (logEventQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el log." };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    searchText,
                    level,
                    logDay,
                    timeStampFrom,
                    timeStampTo,
                    orderBy,
                    orderDirection
                });
            }
            var model = new FormViewModel(
                mode: "view",
                logEventQueryModel: logEventQueryModel,
                searchText: HttpUtility.UrlDecode(searchText),
                level: level,
                logDay: logDay,
                pageSize: pageSize ?? 1,
                pageNumber: pageNumber ?? 10,
                orderBy: orderBy ?? 0,
                orderDirection: orderDirection ?? 1
               );
            return View("Form", model);

        }

    }
}