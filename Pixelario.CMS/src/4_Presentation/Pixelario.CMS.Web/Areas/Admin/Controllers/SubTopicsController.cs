﻿using MediatR;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.SubTopics;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class SubTopicsController : ContentController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IImageServices _imageServices;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };

        public SubTopicsController(
            ILogger logger,
            IMediator mediator,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        #region Index
        // GET: Admin/SubTopics
        public async Task<ActionResult> Index(int id,
            int? pageNumber,
            int? pageSize, string columns,
            int? topicPageNumber,
            int? topicPageSize, string topicColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var totalSubTopics = await _mediator.Send(
                new TopicCommands.CountSubTopicsCommand(
                    iDTopic: id));

            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;

            var subTopicQueryModels = await _mediator.Send(
                new TopicCommands.ListSubTopicsCommand(
                    iDTopic: id,
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize));
            var topicQueryModel = await _mediator.Send(
                new TopicCommands.GetCommand(
                    iDTopic: id));
            var model = new IndexViewModel(
                topicQueryModel: topicQueryModel,
                subTopicQueryModels: subTopicQueryModels,
                totalCount: totalSubTopics,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                topicPageNumber: topicPageNumber,
                topicPageSize: topicPageSize,
                topicColumns: topicColumns);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Enable(TopicCommands.EnableSubTopicCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
               solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }

            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToHome(TopicCommands.SubTopicToHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> OrderAtHome(TopicCommands.ChangeSubTopicOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToMenu(TopicCommands.SubTopicToMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> OrderAtMenu(TopicCommands.SubTopicOrderAtMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> Delete(TopicCommands.DeleteSubTopicCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        #endregion
        #region Nuevo
        //Get Admin/SubTopics/Nuevo
        public async Task<ActionResult> Create(int id,
            int? pageNumber,
            int? pageSize, string columns,
            int? topicPageNumber,
            int? topicPageSize, string topicColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var topicQueryModel = await _mediator.Send(new TopicCommands.GetCommand(
                iDTopic: id
                ));
            if (topicQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el tema buscado." };
                return RedirectToAction("Index", "Topics", new
                {
                    pageNumber = topicPageNumber,
                    pageSize = topicPageSize,
                    columns = topicColumns
                });
            }
            var model = new FormViewModel(
                mode: "create",
                topicQueryModel: topicQueryModel,
                topicPageNumber: topicPageNumber,
                topicPageSize: topicPageSize,
                topicColumns: topicColumns,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View("Form", model);
        }
        //Get Admin/SubTopics/Nuevo
        [HttpPost]
        public async Task<ActionResult> Create(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                try
                {
                    var command = model.ToAddSubTopicCommand(
                        solicitedBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El subtema fue creado exitosamente.";
                        var pageNumber = 1;
                        if (this.Request.Params["PageNumber"] != null)
                            int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                        var pageSize = 10;
                        if (this.Request.Params["PageSize"] != null)
                            int.TryParse(this.Request.Params["PageSize"], out pageSize);
                        var columns = this.Request.Params["Columns"];
                        var topicPageNumber = 1;
                        if (this.Request.Params["TopicPageNumber"] != null)
                            int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
                        var topicPageSize = 10;
                        if (this.Request.Params["TopicPageSize"] != null)
                            int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
                        var topicColumns = this.Request.Params["TopicColumns"];
                        return RedirectToAction("Index", new
                        {
                            id = command.IDTopic,
                            pageNumber,
                            pageSize,
                            columns,
                            topicPageNumber,
                            topicPageSize,
                            topicColumns
                        });
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                catch (Exception ex)
                {
                    TempData["errors"] = new List<string>() { ex.Message };
                }
            }
            model.SetMode("create");
            return View("Form", model);

        }
        #endregion
        #region Edicion
        //Get Admin/SubTopics/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize, string columns,
            int? topicPageNumber,
            int? topicPageSize, string topicColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var subTopicQueryModel = await _mediator.Send(new TopicCommands.GetSubTopicCommand(
                iDSubTopic: id));
            if (subTopicQueryModel == null)
            {
                TempData["errors"] = "no se encontró el subtema buscado.";
                return RedirectToAction("Index", "Topics", new
                {
                    pageNumber = topicPageNumber,
                    pageSize = topicPageSize,
                    columns = topicColumns
                });
            }
            var topicQueryModel = await _mediator.Send(new TopicCommands.GetCommand(
                iDTopic: subTopicQueryModel.IDTopic
                ));
            if (topicQueryModel == null)
            {
                TempData["errors"] = "no se encontró el tema buscado.";
                return RedirectToAction("Index", "Topics", new
                {
                    pageNumber = topicPageNumber,
                    pageSize = topicPageSize,
                    columns = topicColumns
                });
            }

            var model = new FormViewModel(
                mode: "update",
                subTopicQueryModel: subTopicQueryModel,
                topicQueryModel: topicQueryModel,
                topicPageNumber: topicPageNumber,
                topicPageSize: topicPageSize,
                topicColumns: topicColumns,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View("Form", model);
        }
        ////Get Admin/SubTopics/Update
        [HttpPost]
        public async Task<ActionResult> Update(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                var command = model.ToUpdateSubTopicCommand(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El subtema fue editado exitosamente.";
                    var pageNumber = 1;
                    if (this.Request.Params["PageNumber"] != null)
                        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                    var pageSize = 10;
                    if (this.Request.Params["PageSize"] != null)
                        int.TryParse(this.Request.Params["PageSize"], out pageSize);
                    var columns = this.Request.Params["Columns"];
                    var topicPageNumber = 1;
                    if (this.Request.Params["TopicPageNumber"] != null)
                        int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
                    var topicPageSize = 10;
                    if (this.Request.Params["TopicPageSize"] != null)
                        int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
                    var topicColumns = this.Request.Params["TopicColumns"];

                    return RedirectToAction("Index", new
                    {
                        id = command.IDTopic,
                        pageNumber,
                        pageSize,
                        columns,
                        topicPageNumber,
                        topicPageSize,
                        topicColumns
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }
            model.SetMode("update");
            return View("Form", model);
        }
        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            int? pageNumber,
            int? pageSize, string columns,
            int? topicPageNumber,
            int? topicPageSize, string topicColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var subTopicQueryModel = await _mediator.Send(new TopicCommands.GetSubTopicCommand(
                iDSubTopic: id));
            if (subTopicQueryModel == null)
            {
                TempData["errors"] = "no se encontró el subtema buscado.";
                return RedirectToAction("Index", "Topics", new
                {
                    pageNumber = topicPageNumber,
                    pageSize = topicPageSize,
                    columns = topicColumns
                });
            }
            var topicQueryModel = await _mediator.Send(new TopicCommands.GetCommand(
                iDTopic: subTopicQueryModel.IDTopic
            ));
            if (topicQueryModel == null)
            {
                TempData["errors"] = "no se encontró el tema buscado.";
                return RedirectToAction("Index", "Topics", new
                {
                    pageNumber = topicPageNumber,
                    pageSize = topicPageSize,
                    columns = topicColumns
                });
            }
            return View(new FormViewModel(
                mode: "images",
                subTopicQueryModel: subTopicQueryModel,
                topicQueryModel: topicQueryModel,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                topicPageNumber: topicPageNumber,
                topicPageSize: topicPageSize,
                topicColumns: topicColumns
               ));
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/subtopics/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new TopicCommands.ChangeSubTopicImageCommand(
                        iDTopic: model.IDTopic,
                        iDSubTopic: model.IDSubTopic.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El subtema fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            TempData["success"] = "El subtema fue editado exitosamente.";
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Images", new
            {
                id = model.IDSubTopic.Value,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(TopicCommands.RemoveSubTopicImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El subtema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            var topicPageNumber = 1;
            if (this.Request.Params["TopicPageNumber"] != null)
                int.TryParse(this.Request.Params["TopicPageNumber"], out topicPageNumber);
            var topicPageSize = 10;
            if (this.Request.Params["TopicPageSize"] != null)
                int.TryParse(this.Request.Params["TopicPageSize"], out topicPageSize);
            var topicColumns = this.Request.Params["TopicColumns"];

            return RedirectToAction("Images", new
            {
                id = command.IDSubTopic,
                pageNumber,
                pageSize,
                columns,
                topicPageNumber,
                topicPageSize,
                topicColumns
            });
        }
        #endregion

    }
}