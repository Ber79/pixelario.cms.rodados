﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.ArticleScripts;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ArticleScriptCommands = Pixelario.CMS.Application.Commands.Articles;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class ArticleScriptsController : ContentController
    {
        private readonly string[] ConfigurationModules = { "articleScripts/" };
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        private ILogger _logger;
        private readonly IMediator _mediator;

        public ArticleScriptsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/ArticleScript
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var totalCount = await _mediator.Send(new ArticleScriptCommands.CountScriptCommand());
            var articleScriptQueryModels = await _mediator.Send(new ArticleScriptCommands.ListScriptCommand(
                rowIndex: (_pageNumber - 1) * _pageSize,
                rowCount: _pageSize
                ));
            var model = new IndexViewModel(
                articleScriptQueryModels: articleScriptQueryModels,
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Enable(ArticleScriptCommands.EnableScriptCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El script de artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"]; return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                editionPageNumber, editionPageSize, editionColumns
            });
        }


        [HttpPost]
        public async Task<ActionResult> Delete(ArticleScriptCommands.DeleteScriptCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El script de artículo fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"]; return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                editionPageNumber,
                editionPageSize,
                editionColumns
            });
        }
        #endregion
        #region Create
        //Get Admin/ArticleScripts/Create
        public async Task<ActionResult> Create(int? pageNumber,
            int? pageSize, int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var model = new FormViewModel(
                mode: "create",
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("Form", model);
        }
        //Get Admin/ArticleScripts/Create
        [HttpPost]
        public async Task<ActionResult> Create(ArticleScriptCommands.AddScriptCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];
            if (ModelState.IsValid)
            {
                try
                {
                    command.SetCreatedBy(
                        createdBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {

                        TempData["success"] = "El script de artículo fue creado exitosamente.";
                        return RedirectToAction("Index", new
                        {
                            pageNumber = 1,
                            pageSize,
                            editionPageNumber,
                            editionPageSize,
                            editionColumns
                        });
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                catch (Exception ex)
                {
                    TempData["errors"] = new List<string>() { ex.Message };
                }
            }
            var model = new FormViewModel(
                mode: "create",
                command: command,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("Form", model);

        }
        #endregion
        #region Edicion
        //Get Admin/Temas/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleScript = await _mediator.Send(new ArticleScriptCommands.GetScriptCommand(
                iDArticleScript: id));

            if (articleScript != null)
            {
                var model = new FormViewModel(
                    mode: "update",
                    articleScriptQueryModel: articleScript,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
                return View("Form", model);
            }
            else
            {
                TempData["errors"] = new List<string>() { "No se encontró el script del artículo" };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,                    
                    editionColumns
                });
            }
        }
        //Get Admin/ArticleScript/Update
        [HttpPost]
        public async Task<ActionResult> Update(ArticleScriptCommands.UpdateScriptCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];
            try
            {
                command.SetSolicitedBy(User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El script de artículo fue editado exitosamente.";

                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize,
                        editionPageSize,
                        editionPageNumber,
                        editionColumns
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                {
                    var errors = (List<string>)TempData["errors"];
                    errors.Add(ex.InnerException.Message);
                    if (ex.InnerException.InnerException != null)
                    {
                        errors.Add(ex.InnerException.InnerException.Message);
                    }
                    TempData["errors"] = errors;
                }
            }
            var model = new FormViewModel(
                mode: "update",
                command: command,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("Form", model);
        }
        #endregion
    }
}