﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.ArticleScriptItems;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ArticleScriptCommands = Pixelario.CMS.Application.Commands.Articles;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class ArticleScriptItemsController : ContentController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly List<MultimediaType> articleScriptMultimediaTypes = new List<MultimediaType>()
        {
            MultimediaType.ScriptOnHeader, MultimediaType.ScriptOnBottom,
            MultimediaType.ScriptOnArticleBottom
        };
        public ArticleScriptItemsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/ArticleScriptItems
        public async Task<ActionResult> Index(int id,
            int? pageNumber,
            int? pageSize,
            int? scriptPageNumber,
            int? scriptPageSize,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var totalArticleScriptItems = await _mediator.Send(
                new ArticleScriptCommands.CountScriptItemsCommand(
                    iDArticleScript: id));

            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;

            var articleScriptItemQueryModels = await _mediator.Send(
                new ArticleScriptCommands.ListScriptItemsCommand(
                    iDArticleScript: id,
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize));
            var articleScriptQueryModel = await _mediator.Send(
                new ArticleScriptCommands.GetScriptCommand(
                    iDArticleScript: id));
            var model = new IndexViewModel(
                articleScriptQueryModel: articleScriptQueryModel,
                articleScriptItemQueryModels: articleScriptItemQueryModels,
                totalCount: totalArticleScriptItems,
                pageNumber: pageNumber,
                pageSize: pageSize,
                scriptPageNumber: scriptPageNumber,
                scriptPageSize: scriptPageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Enable(ArticleScriptCommands.EnableScriptItemCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
               solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El ítem fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }

            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var scriptPageNumber = 1;
            if (this.Request.Params["ScriptPageNumber"] != null)
                int.TryParse(this.Request.Params["ScriptPageNumber"], out scriptPageNumber);
            var scriptPageSize = 10;
            if (this.Request.Params["ScriptPageSize"] != null)
                int.TryParse(this.Request.Params["ScriptPageSize"], out scriptPageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];

            return RedirectToAction("Index", new
            {
                id = command.IDArticleScript,
                pageNumber,
                pageSize,
                scriptPageNumber,
                scriptPageSize,
                editionPageNumber,
                editionPageSize,
                editionColumns
            });
        }
        [HttpPost]
        public async Task<ActionResult> Delete(ArticleScriptCommands.DeleteScriptItemCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El ítem fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var scriptPageNumber = 1;
            if (this.Request.Params["ScriptPageNumber"] != null)
                int.TryParse(this.Request.Params["ScriptPageNumber"], out scriptPageNumber);
            var scriptPageSize = 10;
            if (this.Request.Params["ScriptPageSize"] != null)
                int.TryParse(this.Request.Params["ScriptPageSize"], out scriptPageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];
            return RedirectToAction("Index", new
            {
                id = command.IDArticleScript,
                pageNumber,
                pageSize,
                scriptPageNumber,
                scriptPageSize,
                editionPageNumber,
                editionPageSize,
                editionColumns
            });
        }
        #endregion
        #region Create
        //Get Admin/ArticleScriptItems/Create        
        public async Task<ActionResult> Create(int id,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleScriptQueryModel = await _mediator.Send(new ArticleScriptCommands.GetScriptCommand(
                iDArticleScript: id
                ));
            if (articleScriptQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el script de artículo buscado." };
                return RedirectToAction("Index", "ArticleScripts", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var model = new FormViewModel(
                mode: "create",
                articleScriptQueryModel: articleScriptQueryModel,
                multimediaTypes: articleScriptMultimediaTypes,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                scriptPageNumber: scriptPageNumber,
                scriptPageSize: scriptPageSize);
            return View("Form", model);
        }
        //Get Admin/ArticleScriptItems/Nuevo
        [HttpPost]
        [ValidateInput(false)]

        public async Task<ActionResult> Create(ArticleScriptCommands.AddScriptItemCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var scriptPageNumber = 1;
            if (this.Request.Params["ScriptPageNumber"] != null)
                int.TryParse(this.Request.Params["ScriptPageNumber"], out scriptPageNumber);
            var scriptPageSize = 10;
            if (this.Request.Params["ScriptPageSize"] != null)
                int.TryParse(this.Request.Params["ScriptPageSize"], out scriptPageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];
            try
            {
                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El ítem fue creado exitosamente.";

                    return RedirectToAction("Index", new
                    {
                        id = command.IDArticleScript,
                        pageNumber,
                        pageSize,
                        editionPageNumber,
                        editionPageSize,
                        editionColumns,
                        scriptPageNumber,
                        scriptPageSize
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }
            var articleScriptQueryModel = await _mediator.Send(new ArticleScriptCommands.GetScriptCommand(
                iDArticleScript: command.IDArticleScript
                ));
            if (articleScriptQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el script de artículo buscado." };
                return RedirectToAction("Index", "ArticleScripts", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var model = new FormViewModel(
                mode: "create",
                command: command,
                articleScriptQueryModel: articleScriptQueryModel,
                multimediaTypes: articleScriptMultimediaTypes,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                scriptPageNumber: scriptPageNumber,
                scriptPageSize: scriptPageSize);
            return View("Form", model);
        }
        #endregion
        #region Edicion
        //Get Admin/ArticleScriptItems/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize, int? scriptPageNumber,
            int? scriptPageSize,
            int? editionPageNumber,
            int? editionPageSize, string editionColumns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleScriptItemQueryModel = await _mediator.Send(new ArticleScriptCommands.GetScriptItemCommand(
                iDArticleScriptItem: id));
            if (articleScriptItemQueryModel == null)
            {
                TempData["errors"] = "no se encontró el ítem buscado.";
                return RedirectToAction("Index", "ArticleScripts", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var articleScriptQueryModel = await _mediator.Send(new ArticleScriptCommands.GetScriptCommand(
                iDArticleScript: articleScriptItemQueryModel.ArticleScript.IDArticleScript
                ));
            if (articleScriptQueryModel == null)
            {
                TempData["errors"] = "no se encontró el script de artículo buscado.";
                return RedirectToAction("Index", "ArticleScripts", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }

            var model = new FormViewModel(
                mode: "update",
                articleScriptItemQueryModel: articleScriptItemQueryModel,
                multimediaTypes: articleScriptMultimediaTypes,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                scriptPageNumber: scriptPageNumber,
                scriptPageSize: scriptPageSize);
            return View("Form", model);
        }
        ////Get Admin/ArticleScriptItems/Update
        [HttpPost]
        [ValidateInput(false)]

        public async Task<ActionResult> Update(ArticleScriptCommands.UpdateScriptItemCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var scriptPageNumber = 1;
            if (this.Request.Params["ScriptPageNumber"] != null)
                int.TryParse(this.Request.Params["ScriptPageNumber"], out scriptPageNumber);
            var scriptPageSize = 10;
            if (this.Request.Params["ScriptPageSize"] != null)
                int.TryParse(this.Request.Params["ScriptPageSize"], out scriptPageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];
            var iDArticleScript = 0;
            if (this.Request.Params["IDArticleScript"] != null)
                int.TryParse(this.Request.Params["IDArticleScript"], out iDArticleScript);

            try
            {
                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El ítem fue editado exitosamente.";
                    return RedirectToAction("Index", new
                    {
                        id = iDArticleScript,
                        pageNumber,
                        pageSize,
                        editionPageNumber,
                        editionPageSize,
                        editionColumns,
                        scriptPageNumber,
                        scriptPageSize
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }
            var articleScriptQueryModel = await _mediator.Send(new ArticleScriptCommands.GetScriptCommand(
               iDArticleScript: iDArticleScript
               ));
            if (articleScriptQueryModel == null)
            {
                TempData["errors"] = "no se encontró el script de artículo buscado.";
                return RedirectToAction("Index", "ArticleScripts", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var model = new FormViewModel(
                mode: "update",
                command: command,
                articleScriptQueryModel: articleScriptQueryModel,
                multimediaTypes: articleScriptMultimediaTypes,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                scriptPageNumber: scriptPageNumber,
                scriptPageSize: scriptPageSize);
            return View("Form", model);
        }
        #endregion
    }
}