﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Articles;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class ArticlesController : ContentController
    {
        public static readonly string[] ColumnsOrder = { "a.IDArticle", "a.PublicationDate", "a.Title", "a.[Order]" };
        public static readonly string[] OrderDirections = { "ASC", "DESC" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IConstraintsQueries _constraintsQueries;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        public bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }
        public ArticlesController(
            ILogger logger,
            IMediator mediator,
            IConstraintsQueries constraintsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            if (SubscriptionsEnable)
            {
                _constraintsQueries = constraintsQueries ?? throw new ArgumentNullException(nameof(constraintsQueries));
            }
            else
            {
                _constraintsQueries = null;
            }
        }
        #region Index
        // GET: Admin/Articulos
        public async Task<ActionResult> Index(
            int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var editionQueryModel = await _mediator.Send(new EditionCommands.GetCommand(
                iDEdition: id));
            if (editionQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró la edición" };
                return RedirectToAction("Index", "Editions", new
                {
                    id,
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }



            var microFilterStatments = "";
            var publicationDateSearched = DateTime.Now;
            if (!string.IsNullOrEmpty(publicationDate) &&
                    DateTime.TryParseExact(publicationDate, "yyyy-MM-dd",
                        System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None,
                        out publicationDateSearched))
            {
                microFilterStatments +=
                    $"publishDateLessThan={publicationDateSearched.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
                microFilterStatments +=
                    $"publishDateGreatherThan={publicationDateSearched.Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
            }
            if (!string.IsNullOrEmpty(searchText) && searchText.Trim().Length >= 4)
            {
                microFilterStatments +=
                    $"query={searchText.Trim()}";
            }
            bool? enabled = null;
            if (!string.IsNullOrEmpty(enable) && enable != "-1")
            {
                enabled = enable == "1";
            }
            bool? atHomeValue = null;
            if (!string.IsNullOrEmpty(atHome) && atHome != "-1")
            {
                atHomeValue = atHome == "1";
            }
            ArticlePlaceAtHome placeAtHomeValue = null;
            if (placeAtHome.HasValue && placeAtHome.Value > 0)
            {
                placeAtHomeValue = ArticlePlaceAtHome.From(placeAtHome.Value - 1);
            }
            int orderByValue = 0;
            if (orderBy.HasValue)
            {
                orderByValue = orderBy.Value;
            }
            int orderDirectionValue = 1;
            if (orderDirection.HasValue)
            {
                orderDirectionValue = orderDirection.Value;
            }
            if (pageSize <= 0 || pageSize > 100)
            {
                pageSize = 10;
            }
            var totalCount = await _mediator.Send(new ArticleCommands.CountCommand(
                iDEdition: id,
                iDTopic: null,
                iDSubTopic: null,
                filteredBy: microFilterStatments,
                enabled: enabled,
                placesAtHome: placeAtHomeValue,
                atHome: atHomeValue));

            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var rowIndex = (_pageNumber - 1) * _pageSize;
            var articleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                filteredBy: microFilterStatments,
                iDEdition: id,
                iDTopic: null,
                iDSubTopic: null,
                rowIndex: rowIndex,
                rowCount: _pageSize,
                columnOrder: ColumnsOrder[orderByValue],
                orderDirection: OrderDirections[orderDirectionValue],
                enabled: enabled,
                atHome: atHomeValue,
                placesAtHome: placeAtHomeValue));

            var model = new IndexViewModel(
                editionQueryModel: editionQueryModel,
                articleQueryModels: articleQueryModels,
                searchText: searchText,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy.HasValue ? orderBy.ToString() : "0",
                orderDirection: orderDirection,
                publicationDate: publicationDate,
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns
                );
            return View(model);
        }
        // POST: Admin/Articulos
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            return RedirectToAction("Index", new
            {
                id = collection["IDEdition"],
                searchText = collection["SearchText"],
                publicationDate = collection["PublicationDateParameter"],
                enable = collection["Enable"],
                atHome = collection["AtHome"],
                editionPageNumber = collection["EditionPageNumber"],
                editionPageSize = collection["EditionPageSize"],
                editionColumns = collection["EditionColumns"],
                pageNumber = 1,
                pageSize = collection["PageSize"],
                placeAtHome = collection["PlaceAtHome"],
                orderBy = collection["OrderBy"],
                orderDirection = collection["OrderDirection"]
            });
        }
        [HttpPost]
        public async Task<ActionResult> Enable(ArticleCommands.EnableCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                id = this.Request.Params["IDEdition"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"],
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"]
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToHome(ArticleCommands.ToHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                id = this.Request.Params["IDEdition"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"],
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"]
            });
        }
        [HttpPost]
        public async Task<ActionResult> ChangeHomeOrder(ArticleCommands.ChangeHomeOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                id = this.Request.Params["IDEdition"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"],
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"]
            });
        }
        [HttpPost]
        public async Task<ActionResult> ChangePlaceAtHome(ArticleCommands.ChangePlaceAtHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                id = this.Request.Params["IDEdition"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"],
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"]
            });
        }
        [HttpPost]
        public async Task<ActionResult> Delete(ArticleCommands.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                id = this.Request.Params["IDEdition"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"],
                pageNumber = 1,
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"]
            });
        }
        #endregion
        #region Create
        //Get Admin/Articulos/Create
        public async Task<ActionResult> Create(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var editionQueryModel = await _mediator.Send(new EditionCommands.GetCommand(
                iDEdition: id));
            if (editionQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el contenido buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    plans.Add(new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries));
                }
            }
            var scripts = await _mediator.Send(new ArticleCommands.ListScriptCommand(
                filteredBy: null,
                enabled: true,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "ars.Title",
                orderDirection: "ASC"
                ));
            var model = new FormViewModel(
                mode: "create",
                editionQueryModel: editionQueryModel,
                subscriptionsEnable: this.SubscriptionsEnable,
                planAdmins: plans,
                scripts: scripts,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("Form", model);
        }
        //Post Admin/Articulos/Create
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(ArticleCommands.AddCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetCreatedBy(
                createdBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue creado exitosamente.";
                return RedirectToAction("Index", new
                {
                    id = command.IDEdition
                });
            }
            else
            {
                TempData["errors"] = response.errors; ;
            }
            var editionQueryModel = await _mediator.Send(new EditionCommands.GetCommand(
                iDEdition: command.IDEdition));
            var scripts = await _mediator.Send(new ArticleCommands.ListScriptCommand(
                filteredBy: null,
                enabled: true,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "ars.Title",
                orderDirection: "ASC"
            ));
            int? _editionPageNumber = null;
            if (int.TryParse(this.Request.Params["EditionPageNumber"], out int value1))
            {
                _editionPageNumber = value1;
            }
            int? _editionPageSize = null;
            if (int.TryParse(this.Request.Params["EditionPageSize"], out int value2))
            {
                _editionPageSize = value2;
            }
            var _editionColumns = this.Request.Params["EditionColumns"];
            if (editionQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el contenido buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = _editionPageNumber,
                    pageSize = _editionPageSize,
                    columns = _editionColumns
                });
            }
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    plans.Add(new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries));
                }
            }
            int? _pageNumber = null;
            if (int.TryParse(this.Request.Params["PageNumber"], out int value3))
            {
                _pageNumber = value3;
            }
            int? _pageSize = null;
            if (int.TryParse(this.Request.Params["PageSize"], out int value4))
            {
                _pageSize = value4;
            }
            var _publicationDate = this.Request.Params["PublicationDateParameter"];
            var _searchText = this.Request.Params["SearchText"];
            var _atHome = this.Request.Params["AtHome"];
            var _enable = this.Request.Params["Enable"];

            int? _orderDirection = null;
            if (int.TryParse(this.Request.Params["OrderDirection"], out int value5))
            {
                _orderDirection = value5;
            }
            var _orderBy = this.Request.Params["OrderBy"];

            int? _placeAtHome = null;
            if (int.TryParse(this.Request.Params["PlaceAtHomeParameter"], out int value6))
            {
                _placeAtHome = value6;
            }
            var _columns = this.Request.Params["Columns"];

            var model = new FormViewModel(
                mode: "create",
                command: command,
                editionQueryModel: editionQueryModel,
                scripts: scripts,
                subscriptionsEnable: this.SubscriptionsEnable,
                planAdmins: plans,
                searchText: _searchText,
                publicationDate: _publicationDate,
                enable: _enable,
                atHome: _atHome,
                placeAtHome: _placeAtHome,
                orderBy: _orderBy,
                orderDirection: _orderDirection,
                pageNumber: _pageNumber,
                pageSize: _pageSize,
                columns: _columns,
                editionPageNumber: _editionPageNumber,
                editionPageSize: _editionPageSize,
                editionColumns: _editionColumns);
            return View("Form", model);

        }
        #endregion
        #region Editar
        //Get Admin/Articulos/Nuevo
        public async Task<ActionResult> Update(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            else
            {
                var editionQueryModel = await _mediator.Send(new EditionCommands.GetCommand(
                        iDEdition: articleQueryModel.Edition.IDEdition));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró el contenido buscado." };
                    return RedirectToAction("Index", "Editions", new
                    {
                        pageNumber = editionPageNumber,
                        pageSize = editionPageSize,
                        columns = editionColumns
                    });
                }
                var editionQueryModels = await _mediator.Send(
                    new EditionCommands.ListCommand(
                          filteredBy: null,
                          enabled: true,
                          atHome: null,
                          onMenu: null,
                          editionTypes: null,
                          pagesOnEditionFilter: null,
                          rowIndex: 0,
                          rowCount: int.MaxValue,
                          columnOrder: "e.Title",
                          orderDirection: "ASC"));
                var scripts = await _mediator.Send(new ArticleCommands.ListScriptCommand(
                    filteredBy: null,
                    enabled: true,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "ars.Title",
                    orderDirection: "ASC"
                ));


                var plans = new List<PlanAdminModel>();
                if (this.SubscriptionsEnable)
                {
                    foreach (var plan in Plan.List())
                    {
                        var planAdmimModel = new PlanAdminModel(plan: plan,
                            constraintsQueries: _constraintsQueries);
                        await planAdmimModel.SetSelected(id);
                        plans.Add(planAdmimModel);
                    }
                }
                var model = new FormViewModel(
                    mode: "update",
                    editionQueryModel: editionQueryModel,
                    scripts:scripts,
                    subscriptionsEnable: this.SubscriptionsEnable,
                    planAdmins: plans,

                    articleQueryModel: articleQueryModel,
                    editionQueryModels: editionQueryModels,

                    searchText: searchText,
                    publicationDate: publicationDate,
                    enable: enable,
                    atHome: atHome,
                    placeAtHome: placeAtHome,
                    orderBy: orderBy,
                    orderDirection: orderDirection,
                    pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                    editionPageNumber: editionPageNumber,
                    editionPageSize: editionPageSize,
                    editionColumns: editionColumns);
                return View("Form", model);
            }

        }
        //Get Admin/Articles/Update
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(
            ArticleCommands.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
                return RedirectToAction("Index", new
                {
                    id = this.Request.Params["IDEdition"],
                    searchText = this.Request.Params["SearchText"],
                    publicationDate = this.Request.Params["PublicationDateParameter"],
                    enable = this.Request.Params["Enable"],
                    atHome = this.Request.Params["AtHome"],
                    placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                    orderBy = this.Request.Params["OrderBy"],
                    orderDirection = this.Request.Params["OrderDirection"],
                    pageNumber = this.Request.Params["PageNumber"],
                    pageSize = this.Request.Params["PageSize"],
                    columns = this.Request.Params["Columns"],
                    editionPageNumer = this.Request.Params["EditionPageNumber"],
                    editionPageSize = this.Request.Params["EditionPageSize"],
                    editionColumns = this.Request.Params["EditionColumns"]
                });
            }

            int? _editionPageNumber = null;
            if (int.TryParse(this.Request.Params["EditionPageNumber"], out int value1))
            {
                _editionPageNumber = value1;
            }
            int? _editionPageSize = null;
            if (int.TryParse(this.Request.Params["EditionPageSize"], out int value2))
            {
                _editionPageSize = value2;
            }
            var _editionColumns = this.Request.Params["EditionColumns"];

            int _iDEdition;
            if (!int.TryParse(this.Request.Params["EditionPageNumber"], out _iDEdition) || 
                _iDEdition <= 0)
            {
                TempData["errors"] = new List<string>() { "No se encontró el contenido buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = _editionPageNumber,
                    pageSize = _editionPageSize,
                    columns = _editionColumns
                });

            }
            var editionQueryModel = await _mediator.Send(new EditionCommands.GetCommand(
                iDEdition: _iDEdition));
            if (editionQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró el contenido buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = _editionPageNumber,
                    pageSize = _editionPageSize,
                    columns = _editionColumns
                });
            }
            var editionQueryModels = await _mediator.Send(
                new EditionCommands.ListCommand(
                    filteredBy: null,
                    enabled: true,
                    atHome: null,
                    onMenu: null,
                    editionTypes: null,
                    pagesOnEditionFilter: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "e.Title",
                    orderDirection: "ASC"));
            var scripts = await _mediator.Send(new ArticleCommands.ListScriptCommand(
                   filteredBy: null,
                   enabled: true,
                   rowIndex: 0,
                   rowCount: int.MaxValue,
                   columnOrder: "ars.Title",
                   orderDirection: "ASC"
               ));
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    plans.Add(new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries));
                }
            }
            int? _pageNumber = null;
            if (int.TryParse(this.Request.Params["PageNumber"], out int value3))
            {
                _pageNumber = value3;
            }
            int? _pageSize = null;
            if (int.TryParse(this.Request.Params["PageSize"], out int value4))
            {
                _pageSize = value4;
            }
            var _publicationDate = this.Request.Params["PublicationDateParameter"];
            var _searchText = this.Request.Params["SearchText"];
            var _atHome = this.Request.Params["AtHome"];
            var _enable = this.Request.Params["Enable"];

            int? _orderDirection = null;
            if (int.TryParse(this.Request.Params["OrderDirection"], out int value5))
            {
                _orderDirection = value5;
            }
            var _orderBy = this.Request.Params["OrderBy"];

            int? _placeAtHome = null;
            if (int.TryParse(this.Request.Params["PlaceAtHomeParameter"], out int value6))
            {
                _placeAtHome = value6;
            }
            var _columns = this.Request.Params["Columns"];

            TempData["errors"] = response.errors;

            var model = new FormViewModel(
                mode: "update",
                command: command,
                editionQueryModel: editionQueryModel,
                scripts: scripts,
                subscriptionsEnable: this.SubscriptionsEnable,
                planAdmins: plans,
                editionQueryModels: editionQueryModels,
                searchText: _searchText,
                publicationDate: _publicationDate,
                enable: _enable,
                atHome: _atHome,
                placeAtHome: _placeAtHome,
                orderBy: _orderBy,
                orderDirection: _orderDirection,
                pageNumber: _pageNumber,
                pageSize: _pageSize,
                columns: _columns,
                editionPageNumber: _editionPageNumber,
                editionPageSize: _editionPageSize,
                editionColumns: _editionColumns);
            return View("Form", model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> TransferToEdition(ArticleCommands.TransferToEditionCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                command.SetSolicitedBy(User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El artículo fue editado exitosamente.";
                    return RedirectToAction("Index", new
                    {
                        id = command.IDEdition,
                        pageNumber = 1,
                        pageSize = this.Request.Params["EditionPageSize"],
                        columns = this.Request.Params["EditionColumns"]
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                var errors = new List<string>();
                errors.Add(ex.Message);
                if (ex.InnerException != null)
                    errors.Add(ex.InnerException.Message);
                TempData["errors"] = errors;
            }
            return RedirectToAction("Update", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });

        }



        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });

            }

            var multimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                iDArticle: articleQueryModel.IDArticle,
                multimediaType: MultimediaType.Imagen,
                enabled: null,
                atHome: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "m.IDMultimedia",
                orderDirection: "DESC"
            ));

            var model = new FormViewModel(
                mode: "images",
                articleQueryModel: articleQueryModel,
                multimediaQueryModels: multimediaQueryModels,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadHomeImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.ChangeImageCommand(
                        iDArticle: model.IDArticle.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El artículo fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("Images", new
            {
                id = model.IDArticle,
                searchText = model.SearchText,
                publicationDate = model.PublicationDateParameter,
                enable = model.Enable,
                atHome = model.AtHome,
                placeAtHome = model.PlaceAtHomeParameter,
                orderBy = model.OrderBy,
                orderDirection = model.OrderDirection,
                pageNumber = model.PageNumber,
                pageSize = model.PageSize,
                columns = model.Columns,
                editionPageNumer = model.EditionPageNumber,
                editionPageSize = model.EditionPageSize,
                editionColumns = model.EditionColumns
            });
        }



        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(ArticleCommands.RemoveImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Images", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }

        [Authorize]
        public async Task<ActionResult> CropHorizontalImage(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });

            }
            var model = new CropImagesViewModel(
                mode: "cropHorizontalImage",
                articleQueryModel: articleQueryModel,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("CropImages", model);

        }
        [Authorize]
        public async Task<ActionResult> CropSlideImage(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var model = new CropImagesViewModel(
                mode: "cropSlideImage",
                articleQueryModel: articleQueryModel,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View("CropImages", model);
        }

        [Authorize]
        public async Task<ActionResult> CropVerticalImage(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var model = new CropImagesViewModel(
               mode: "cropVerticalImage",
               articleQueryModel: articleQueryModel,
               searchText: searchText,
               publicationDate: publicationDate,
               enable: enable,
               atHome: atHome,
               placeAtHome: placeAtHome,
               orderBy: orderBy,
               orderDirection: orderDirection,
               pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
               editionPageNumber: editionPageNumber,
               editionPageSize: editionPageSize,
               editionColumns: editionColumns);
            return View("CropImages", model);

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CropImage(ArticleCommands.CropImageCommand model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            model.AppDomainAppPath = HttpRuntime.AppDomainAppPath;
            Guid guid = Guid.NewGuid();
            model.FileName = guid.ToString("N").Substring(0, 13);
            model.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(model);
            if (response.success)
            {
                TempData["success"] = "El artículo fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }


            return RedirectToAction("Images", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadHorizontalImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HorizontalImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.ChangeImageCommand(
                        iDArticle: model.IDArticle.Value,
                        imageType: ImageType.Horizontal,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El artículo fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("Images", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadVerticalImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["VerticalImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.ChangeImageCommand(
                        iDArticle: model.IDArticle.Value,
                        imageType: ImageType.Vertical,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El artículo fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("Images", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadGaleryImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["GalleryImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Imagen.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.AddMultimediaCommand(
                        iDArticle: model.IDArticle.Value,
                        multimediaType: MultimediaType.Imagen,
                        title: model.MultimediaTitle,
                        summary: null,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: null);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "La foto fue subida exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["error1"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("Images", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveGaleryImage(ArticleCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La foto fue borrada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Images", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }


        [Authorize]
        public async Task<ActionResult> Documents(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var multimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                iDArticle: articleQueryModel.IDArticle,
                multimediaType: MultimediaType.Documento,
                enabled: null,
                atHome: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "m.IDMultimedia",
                orderDirection: "DESC"
                ));

            var model = new FormViewModel(
                mode: "documents",
                articleQueryModel: articleQueryModel,
                multimediaQueryModels: multimediaQueryModels,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadDocument(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["DocuemtFile"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Documento.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath =
                        Server.MapPath("~/uploads/articles/documents");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.AddMultimediaCommand(
                        iDArticle: model.IDArticle.Value,
                        multimediaType: MultimediaType.Documento,
                        title: model.MultimediaTitle,
                        summary: null,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: extension);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El documento fue subido exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato del documento no aceptado, debe ser { string.Join(", ", MultimediaType.Documento.Extensions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("Documents", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveDocument(ArticleCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El documento fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Documents", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }


        [Authorize]
        public async Task<ActionResult> Widgets(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var multimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                iDArticle: articleQueryModel.IDArticle,
                multimediaType: MultimediaType.Widget,
                enabled: null,
                atHome: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "m.IDMultimedia",
                orderDirection: "DESC"));
            var model = new FormViewModel(
                mode: "widgets",
                articleQueryModel: articleQueryModel,
                multimediaQueryModels: multimediaQueryModels,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> UploadWidget(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["CoverImage"];
            if (file != null)
            {

                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Imagen.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.AddMultimediaCommand(
                        iDArticle: model.IDArticle.Value,
                        multimediaType: MultimediaType.Widget,
                        title: model.Title,
                        summary: model.Summary,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: null);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El widget fue subido exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["success"] = "No se seleccionó una imagen de tapa para el widget.";
            }
            return RedirectToAction("Widgets", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveWidget(ArticleCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El widget fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Widgets", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }
        #endregion

        [Authorize]
        public async Task<ActionResult> Links(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var multimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                iDArticle: articleQueryModel.IDArticle,
                multimediaType: MultimediaType.Link,
                enabled: null,
                atHome: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "m.IDMultimedia",
                orderDirection: "DESC"
                ));
            var model = new FormViewModel(
                mode: "links",
                articleQueryModel: articleQueryModel,
                multimediaQueryModels: multimediaQueryModels,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> UploadLink(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["LinkyImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Link.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/articles/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ArticleCommands.AddMultimediaCommand(
                        iDArticle: model.IDArticle.Value,
                        multimediaType: MultimediaType.Link,
                        title: model.MultimediaTitle,
                        summary: null,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: model.MultimediaLink);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El flyer fue subido exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["error1"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["error1"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("links", new
            {
                id = model.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveLink(ArticleCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El link fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Links", new
            {
                id = command.IDArticle,
                pageNumber = this.Request.Params["PageNumber"],
                pageSize = this.Request.Params["PageSize"],
                columns = this.Request.Params["Columns"],
                searchText = this.Request.Params["SearchText"],
                publicationDate = this.Request.Params["PublicationDateParameter"],
                enable = this.Request.Params["Enable"],
                atHome = this.Request.Params["AtHome"],
                placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                orderBy = this.Request.Params["OrderBy"],
                orderDirection = this.Request.Params["OrderDirection"],
                editionPageNumber = this.Request.Params["EditionPageNumber"],
                editionPageSize = this.Request.Params["EditionPageSize"],
                editionColumns = this.Request.Params["EditionColumns"]
            });
        }

        #region Scripts 
        [Authorize]
        public async Task<ActionResult> Scripts(int id,
            string searchText,
            string publicationDate,
            string enable, string atHome,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? placeAtHome,
            int? pageNumber,
            int? pageSize,
            string columns,
            string orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var articleQueryModel = await _mediator.Send(new ArticleCommands.GetCommand(
                iDArticle: id));
            if (articleQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el artículo buscado." };
                return RedirectToAction("Index", "Editions", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var multimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                iDArticle: articleQueryModel.IDArticle,
                multimediaTypes: new List<MultimediaType> { 
                    MultimediaType.ScriptOnHeader, 
                    MultimediaType.ScriptOnBottom 
                }));
            var model = new FormViewModel(
                mode: "scripts",
                articleQueryModel: articleQueryModel,
                multimediaQueryModels: multimediaQueryModels,
                searchText: searchText,
                publicationDate: publicationDate,
                enable: enable,
                atHome: atHome,
                placeAtHome: placeAtHome,
                orderBy: orderBy,
                orderDirection: orderDirection,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns);
            return View(model);
        }

        #endregion

        #region Constraints
        [HttpPost]
        public async Task<ActionResult> Constraints(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var command = model.ToChangeConstraintsCommand(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "Las restricciones fueron cargadas exitosamente.";
                return RedirectToAction("Index", new
                {
                    id = model.IDEdition,
                    pageNumber = this.Request.Params["PageNumber"],
                    pageSize = this.Request.Params["PageSize"],
                    columns = this.Request.Params["Columns"],
                    searchText = this.Request.Params["SearchText"],
                    publicationDate = this.Request.Params["PublicationDateParameter"],
                    enable = this.Request.Params["Enable"],
                    atHome = this.Request.Params["AtHome"],
                    placeAtHome = this.Request.Params["PlaceAtHomeParameter"],
                    orderBy = this.Request.Params["OrderBy"],
                    orderDirection = this.Request.Params["OrderDirection"],
                    editionPageNumber = this.Request.Params["EditionPageNumber"],
                    editionPageSize = this.Request.Params["EditionPageSize"],
                    editionColumns = this.Request.Params["EditionColumns"]

                });
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            model.SetMode("update");
            return View("Form", model);

        }

        #endregion

    }
}
