﻿using MediatR;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Editions;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using PageCommands = Pixelario.CMS.Application.Commands.Pages;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class EditionsController : ContentController
    {

        private readonly string[] ConfigurationModules = { "editions/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IConstraintsQueries _constraintsQueries;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        public bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }

        public EditionsController(
            ILogger logger,
            IMediator mediator,
            IConstraintsQueries constraintsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            if (SubscriptionsEnable)
            {
                _constraintsQueries = constraintsQueries ?? throw new ArgumentNullException(nameof(constraintsQueries));
            }
            else
            {
                _constraintsQueries = null;
            }
        }



        #region Index
        // GET: Admin/Ediciones
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize,
            string columnOrder,
            string orderDirection,
            string columns
            )
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var editionQueryModels = await _mediator.Send(
                    request: new ListCommand());
            var totalCount = await _mediator.Send(
                    request: new CountCommand());

            var model = new IndexViewModel(
                editionQueryModels: editionQueryModels,
                totalCount: totalCount,
                pageSize: pageSize,
                pageNumber: pageNumber,
                columns: columns);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Enable(EnableCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToHome(ToHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [HttpPost]
        public async Task<ActionResult> ToMenu(ToMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }


        [HttpPost]
        public async Task<ActionResult> ChangeHomeOrder(ChangeHomeOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [HttpPost]
        public async Task<ActionResult> ChangeOnMenuOrder(ChangeMenuOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [HttpPost]
        public async Task<ActionResult> ChangeEditionType(ChangeEditionTypeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue borrada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }
        #endregion
        #region Nuevo
        //Get Admin/Ediciones/Nuevo
        public async Task<ActionResult> Create(int? pageNumber,
            int? pageSize,
            string columnOrder,
            string orderDirection,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    plans.Add(new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries));
                }
            }
            var model = new FormViewModel(
                mode: "create",
                planAdmins: plans,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View("Form", model);
        }
        //Get Admin/Ediciones/Nuevo
        [HttpPost]
        public async Task<ActionResult> Create(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                var command = model.ToAddCommand(
                    createdBy: this.User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La edición fue creada exitosamente.";
                    var pageNumber = 1;
                    if (this.Request.Params["PageNumber"] != null)
                        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                    var pageSize = 10;
                    if (this.Request.Params["PageSize"] != null)
                        int.TryParse(this.Request.Params["PageSize"], out pageSize);
                    var columns = this.Request.Params["Columns"];
                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize,
                        columns
                    });
                }
                else
                {
                    foreach (var error in response.errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    plans.Add(new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries));
                }
            }
            model.SetMode("create");
            model.PlanAdmins = plans;
            return View("Form", model);

        }
        #endregion
        #region Edicion
        //Get Admin/Ediciones/Nuevo
        public async Task<ActionResult> Update(int id, int? pageNumber,
            int? pageSize, string columnOrder,
            string orderDirection, string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var edition = await _mediator.Send(new GetCommand(
                iDEdition: id));

            if (edition == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró la edición" };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    columnOrder,
                    orderDirection,
                    columns
                });
            }
            var plans = new List<PlanAdminModel>();
            if (this.SubscriptionsEnable)
            {
                foreach (var plan in Plan.List())
                {
                    var planAdmimModel = new PlanAdminModel(plan: plan,
                        constraintsQueries: _constraintsQueries);
                    await planAdmimModel.SetSelected(id);
                    plans.Add(planAdmimModel);
                }
            }
            var model = new FormViewModel(
                mode: "update",
                iDEdition: edition.IDEdition,
                title: edition.Title,
                summary: edition.Summary,
                keywords: edition.Keywords,
                planAdmins: plans,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View("Form", model);
        }
        //Get Admin/Ediciones/Nuevo
        [HttpPost]
        public async Task<ActionResult> Update(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var command = model.ToUpdateCommand(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (!response.success)
            {
                TempData["errors"] = response.errors;
                var plans = new List<PlanAdminModel>();
                if (this.SubscriptionsEnable)
                {
                    foreach (var plan in Plan.List())
                    {
                        var planAdmimModel = new PlanAdminModel(plan: plan,
                            constraintsQueries: _constraintsQueries);
                        await planAdmimModel.SetSelected(model.IDEdition.Value);
                        plans.Add(planAdmimModel);
                    }
                }
                model.SetMode("update");
                model.PlanAdmins = plans;
                return View("Form", model);
            }
            TempData["success"] = "La edición fue editada exitosamente.";
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }
        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            int? pageNumber,
            int? pageSize,
            string columns,
            string columnOrder,
            string orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var editionQueryModel = await _mediator.Send(new GetCommand(
                iDEdition: id));
            if (editionQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el contenido buscado" };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    columns
                });
            }


            var model = new FormViewModel(
                mode: "view",
                editionQueryModel: editionQueryModel,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View(model);

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadHomeImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/editions/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new ChangeImageCommand(
                        iDEdition: model.IDEdition.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "La edición fue editada exitosamente.";
                    }
                    else
                    {
                        foreach (var error in response.errors)
                        {
                            TempData["error"] += error;
                        }
                    }
                }
                else
                {
                    TempData["error"] = $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}.";
                }
            }
            else
            {
                TempData["error"] = "No seleccionó ningun archivo para subir.";
            }
            return RedirectToAction("Images", new
            {
                id = model.IDEdition.Value,
                pageNumber = model.PageNumber,
                pageSize = model.PageSize,
                columns = model.Columns
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(RemoveImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La edición fue editada exitosamente.";
            }
            else
            {
                foreach (var error in response.errors)
                {
                    TempData["error"] += $"{error}<br/>";
                }
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Images", new
            {
                id = command.IDEdition,
                pageNumber = command.PageNumber,
                pageSize = command.PageSize,
                columns = columns
            });
        }
        #endregion
        #region Pages
        public async Task<ActionResult> Pages(int id,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var edition = await _mediator.Send(new GetCommand(
                iDEdition: id));
            if (edition == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró la edición" };
                return RedirectToAction("Index", new
                {
                    pageNumber = editionPageNumber,
                    pageSize = editionPageSize,
                    columns = editionColumns
                });
            }
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var pages = await _mediator.Send(new PageCommands.ListCommand(
                iDEdition: id,
                rowIndex: (_pageNumber - 1) * _pageSize,
                rowCount: _pageSize
            ));
            var totalCount = await _mediator.Send(new PageCommands.CountCommand(
                iDEdition: id));
            var model = new PagesViewModel(
                edition: edition,
                pages: pages,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber,
                totalCount: totalCount);
            return View(model);
        }
        #endregion
        #region Constraints
        [HttpPost]
        public async Task<ActionResult> Constraints(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                var command = model.ToChangeConstraintsCommand();
                command.SetSolicitedBy(User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "Las restricciones fueron cargadas exitosamente.";
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in response.errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            model.SetMode("update");
            return View("Form", model);

        }

        #endregion
        [Authorize]
        public async Task<ActionResult> Configurations(int? pageNumber,
            int? pageSize,
            string columnOrder,
            string orderDirection,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));
            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) && c.Key.StartsWith(
                                    string.Format("{0}{1}/",
                                        ConfigurationModules[0], section
                                ))))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }
                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }
            return View(new ConfigurationsViewModel(
                module: "editions",
                configurationsLists: configurationsLists,
                pageSize,
                pageNumber,
                columns));
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];

            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize"
                && key != "Edition" && key != "Columns"
                ))
            {
                if (setting != "section")
                {
                    var key = string.Format("editions/{0}/{1}",
                        section, setting);
                    var state = collection[setting];
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }

            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

    }
}