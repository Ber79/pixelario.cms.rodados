﻿using Pixelario.CMS.Application.Queries.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        internal int CalculatePageCount(int totalRows, int pageSize)
        {
            if (totalRows == 0)
                return 1;
            return ((totalRows - 1) / pageSize) + 1;
        }       
    }
}