﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Pages;
using Pixelario.CMS.Web.Models.Subscriptions;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
using PageCommands = Pixelario.CMS.Application.Commands.Pages;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class PagesController : ContentController
    {
        private readonly string[] ConfigurationModules = { "pages/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        public PagesController(
            ILogger logger,
            IMediator mediator
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/Paginas
        public async Task<ActionResult> Index(int pageNumber = 1,
            int pageSize = 10)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);

            var totalCount = await _mediator.Send(new PageCommands.CountCommand());
            var pageQueryModels = await _mediator.Send(new PageCommands.ListCommand(
                rowIndex: (pageNumber - 1) * pageSize,
                rowCount: pageSize));
            var model = new IndexViewModel(
                pageQueryModels: pageQueryModels,
                pageSize: pageSize,
                pageNumber: pageNumber,
                totalCount: totalCount);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Enable(PageCommands.EnableCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            if (command.IDEdition.HasValue)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["PageNumber"] != null)
                    int.TryParse(this.Request.Params["PageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["PageSize"] != null)
                    int.TryParse(this.Request.Params["PageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Pages", "Editions", new
                {
                    id = command.IDEdition.Value,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
        }
        [HttpPost]
        public async Task<ActionResult> ToHome(PageCommands.ToHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToMenu(PageCommands.ToMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            if (command.IDEdition.HasValue)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["PageNumber"] != null)
                    int.TryParse(this.Request.Params["PageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["PageSize"] != null)
                    int.TryParse(this.Request.Params["PageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];

                return RedirectToAction("Pages", "Editions", new
                {
                    id = command.IDEdition.Value,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
        }
        [HttpPost]
        public async Task<ActionResult> OrderAtHome(PageCommands.OrderAtHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        [HttpPost]
        public async Task<ActionResult> OrderAtMenu(PageCommands.OrderAtMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            if (command.IDEdition.HasValue)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["PageNumber"] != null)
                    int.TryParse(this.Request.Params["PageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["PageSize"] != null)
                    int.TryParse(this.Request.Params["PageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];

                return RedirectToAction("Pages", "Editions", new
                {
                    id = command.IDEdition.Value,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }

        }
        [HttpPost]
        public async Task<ActionResult> Delete(PageCommands.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue borrada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            if (command.IDEdition.HasValue)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["PageNumber"] != null)
                    int.TryParse(this.Request.Params["PageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["PageSize"] != null)
                    int.TryParse(this.Request.Params["PageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];

                return RedirectToAction("Pages", "Editions", new
                {
                    id = command.IDEdition.Value,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
        }
        #endregion
        #region Nuevo
        //Get Admin/Pages/Create
        public async Task<ActionResult> Create(int? edition,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            EditionQueryModel editionQueryModel = null;
            if (edition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: edition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró la edición." };
                    if (edition.HasValue)
                    {
                        return RedirectToAction("Start", "Home");
                    }
                }
            }
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.News, EditionType.Information }));
            var model = new FormViewModel(
                mode: "create",
                editionQueryModel: editionQueryModel,
                availableEditions: availableEditions,
                editions: new List<int>(),
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber);
            return View("Form", model);
        }
        //Get Admin/Pages/Nuevo
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(PageCommands.AddCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];

            try
            {
                command.SetCreatedBy(
                    createdBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La página fue creada exitosamente.";
                    int iDEdition = -1;
                    if (this.Request.Params["IDEdition"] != null &&
                        int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                        iDEdition > 0)
                    {
                        return RedirectToAction("Pages", "Editions", new
                        {
                            id = iDEdition,
                            pageNumber,
                            pageSize,
                            editionPageNumber,
                            editionPageSize,
                            editionColumns
                        });
                    }
                    else
                    {
                        return RedirectToAction("Index", new
                        {
                            pageNumber,
                            pageSize,
                        });
                    }
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }


            EditionQueryModel editionQueryModel = null;
            if (command.IDEdition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: command.IDEdition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró la edición." };
                    return RedirectToAction("Start", "Home");
                }
            }

            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.News, EditionType.Information }));

            var model = new FormViewModel(
                mode: "create",
                command: command,
                editionQueryModel: editionQueryModel,
                availableEditions: availableEditions,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber
                );
            return View("Form", model);
        }
        #endregion
        #region Edicion
        //Get Admin/Pages/Update
        public async Task<ActionResult> Update(int id,
            int? edition, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var page = await _mediator.Send(
                new PageCommands.GetCommand(
                    iDPage: id));
            if (page == null)
            {
                TempData["errors"] = new List<string> { "No se encontró la página." };
                if (edition.HasValue)
                {
                    return RedirectToAction("Pages", "Editions",
                        new
                        {
                            edition,
                            pageNumber,
                            pageSize,
                            editionPageNumber,
                            editionPageSize,
                            editionColumns
                        });
                }
                else
                {
                    return RedirectToAction("Index",
                        new
                        {
                            pageNumber,
                            pageSize
                        });
                }

            }
            EditionQueryModel editionQueryModel = null;
            if (edition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: edition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró la edición." };
                    if (edition.HasValue)
                    {
                        return RedirectToAction("Start", "Home");
                    }
                }
            }
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.News, EditionType.Information }));

            var model = new FormViewModel(
                mode: "update",
                editionQueryModel: editionQueryModel,
                pageQueryModel: page,
                availableEditions: availableEditions,
                editions: page.Editions.Select(e => e.IDEdition)
                    .ToList(),
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber);
            return View("Form", model);
        }
        //Get Admin/Pages/Update
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(PageCommands.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var editionPageNumber = 1;
            if (this.Request.Params["EditionPageNumber"] != null)
                int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
            var editionPageSize = 10;
            if (this.Request.Params["EditionPageSize"] != null)
                int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
            var editionColumns = this.Request.Params["EditionColumns"];


            command.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El página fue editada exitosamente.";
                int iDEdition = -1;
                if (this.Request.Params["IDEdition"] != null &&
                    int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                    iDEdition > 0)
                {
                    return RedirectToAction("Pages", "Editions", new
                    {
                        id = iDEdition,
                        pageNumber,
                        pageSize,
                        editionPageNumber,
                        editionPageSize,
                        editionColumns
                    });
                }
                else
                {
                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize
                    });
                }
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            EditionQueryModel editionQueryModel = null;
            if (command.IDEdition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: command.IDEdition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró la edición." };
                    return RedirectToAction("Start", "Home");
                }
            }
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.News, EditionType.Information })
                );
            var model = new FormViewModel(
                mode: "update",
                editionQueryModel: editionQueryModel,
                command: command,
                availableEditions: availableEditions,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber);              

            return View("Form", model);
        }
        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            int? edition, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageQueryModel = await _mediator.Send(new PageCommands.GetCommand(
                iDPage: id));
            if (pageQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró la página." };
                return RedirectToAction("Start", "Home");
            }
            EditionQueryModel editionQueryModel = null;
            if (edition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: edition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string> { "No se encontró la edición." };
                    return RedirectToAction("Start", "Home");
                }
            }
            var multimediaQueryModel = await _mediator.Send(new PageCommands.ListMultimediaCommand(
                iDPage: pageQueryModel.IDPage,
                multimediaType: MultimediaType.Imagen,
                rowIndex: 0,
                rowCount: int.MaxValue));

            return View(new FormViewModel(
                mode: "images",
                editionQueryModel: editionQueryModel,
                pageQueryModel: pageQueryModel,
                pageMultimediaQueryModels: multimediaQueryModel,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber
                ));

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/pages/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new PageCommands.ChangeImageCommand(
                        iDPage: model.IDPage.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "La página fue editada exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Images", new
                {
                    id = model.IDPage.Value,
                    edition = iDEdition,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns,
                    pageNumber,
                    pageSize
                });
            }
            else
            {
                return RedirectToAction("Images", new
                {
                    id = model.IDPage.Value,
                    pageNumber,
                    pageSize
                });
            }

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(PageCommands.RemoveImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La página fue editada exitosamente.";
            }
            else
            {
                TempData["error"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Images", new
                {
                    id = command.IDPage,
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Images", new
                {
                    id = command.IDPage,
                    pageNumber,
                    pageSize
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadGaleryImage(
            FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["GalleryImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Imagen.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/pages/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new PageCommands.AddMultimediaCommand(
                        iDPage: model.IDPage.Value,
                        multimediaType: MultimediaType.Imagen,
                        title: model.MultimediaTitle,
                        summary: null,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: null);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success1"] = "La foto fue subida exitosamente.";
                    }
                    else
                    {
                        TempData["error1"] = response.errors;
                    }
                }
                else
                {
                    TempData["error1"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["error1"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Images", new
                {
                    id = model.IDPage.Value,
                    edition = iDEdition,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns,
                    pageNumber,
                    pageSize
                });
            }
            else
            {
                return RedirectToAction("Images", new
                {
                    id = model.IDPage.Value,
                    pageNumber,
                    pageSize
                });
            }


        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveGaleryImage(PageCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success1"] = "La foto fue borrada exitosamente.";
            }
            else
            {
                TempData["error1"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Images", new
                {
                    id = command.IDPage,
                    edition = iDEdition,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns,
                    pageNumber,
                    pageSize
                });
            }
            else
            {
                return RedirectToAction("Images", new
                {
                    id = command.IDPage,
                    pageNumber,
                    pageSize
                });
            }

        }
        [Authorize]
        public async Task<ActionResult> Documents(int id,
            int? edition, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageQueryModel = await _mediator.Send(new PageCommands.GetCommand(
                iDPage: id));
            if (pageQueryModel == null)
            {
                TempData["errors"] = "No se encontro la página buscada.";
                return RedirectToAction("Start", "Home");
            }
            else
            {
                EditionQueryModel editionQueryModel = null;
                if (edition.HasValue)
                {
                    editionQueryModel = await _mediator.Send(
                       new EditionCommands.GetCommand(
                           iDEdition: edition.Value));
                    if (editionQueryModel == null)
                    {
                        TempData["errors"] = "No se encontro la edición buscada.";
                        return RedirectToAction("Start", "Home");
                    }
                }
                var multimediaQueryModels = await _mediator.Send(new PageCommands.ListMultimediaCommand(
                    iDPage: pageQueryModel.IDPage,
                    multimediaType: MultimediaType.Documento,
                    rowIndex: 0,
                    rowCount: int.MaxValue));
                return View(new FormViewModel(
                    mode: "docuemnt",
                    editionQueryModel: editionQueryModel,
                    pageQueryModel: pageQueryModel,
                    pageMultimediaQueryModels: multimediaQueryModels,
                    editionPageNumber: editionPageNumber,
                    editionPageSize: editionPageSize,
                    editionColumns: editionColumns,
                    pageSize: pageSize,
                    pageNumber: pageNumber
                ));
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadDocument(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["DocuemtFile"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Documento.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath =
                        Server.MapPath("~/uploads/pages/documents");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new PageCommands.AddMultimediaCommand(
                        iDPage: model.IDPage.Value,
                        multimediaType: MultimediaType.Documento,
                        title: model.MultimediaTitle,
                        summary: null,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: extension);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El documento fue subido exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato del documento no aceptado, debe ser { string.Join(", ", MultimediaType.Documento.Extensions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Documents", new
                {
                    id = model.IDPage,
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Documents", new
                {
                    id = model.IDPage,
                    pageNumber,
                    pageSize,
                });
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveDocument(PageCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El documento fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Documents", new
                {
                    id = command.IDPage,
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Documents", new
                {
                    id = command.IDPage,
                    pageNumber,
                    pageSize
                });
            }
        }

        [Authorize]
        public async Task<ActionResult> Widgets(int id,
            int? edition, int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageQueryModel = await _mediator.Send(new PageCommands.GetCommand(
                iDPage: id));
            if (pageQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontro la página buscada." };
                return RedirectToAction("Start", "Home");
            }
            EditionQueryModel editionQueryModel = null;
            if (edition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: edition.Value));
                if (editionQueryModel == null)
                {
                    TempData["errors"] = new List<string>() { "No se encontro la edición buscada." };
                    return RedirectToAction("Start", "Home");
                }
            }
            var pageMultimediaQueryModels = await _mediator.Send(new PageCommands.ListMultimediaCommand(
                iDPage: pageQueryModel.IDPage,
                multimediaType: MultimediaType.Widget,
                rowIndex: 0,
                rowCount: int.MaxValue));
            return View(new FormViewModel(
                mode: "docuemnt",
                editionQueryModel: editionQueryModel,
                pageQueryModel: pageQueryModel,
                pageMultimediaQueryModels: pageMultimediaQueryModels,
                editionPageNumber: editionPageNumber,
                editionPageSize: editionPageSize,
                editionColumns: editionColumns,
                pageSize: pageSize,
                pageNumber: pageNumber));
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> UploadWidget(
            FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["CoverImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (MultimediaType.Imagen.Extensions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/pages/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new PageCommands.AddMultimediaCommand(
                        iDPage: model.IDPage.Value,
                        multimediaType: MultimediaType.Widget,
                        title: model.Title,
                        summary: model.Summary,
                        appDomainAppPath: appDomainAppPath,
                        path1: fileName,
                        path2: null);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El widget fue subido exitosamente.";
                    }
                    else
                    {
                        TempData["error"] = response.errors;
                    }
                }
                else
                {
                    TempData["error"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["success"] = "No se seleccionó una imagen de tapa para el widget.";
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];

                return RedirectToAction("Widgets", new
                {
                    id = model.IDPage,
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Widgets", new
                {
                    id = model.IDPage,
                    pageNumber,
                    pageSize
                });
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveWidget(PageCommands.RemoveMultimediaCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El widget fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];

                return RedirectToAction("Widgets", new
                {
                    id = command.IDPage,
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Widgets", new
                {
                    id = command.IDPage,
                    pageNumber,
                    pageSize
                });
            }
        }


        #endregion
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations(int? edition,
            int? editionPageNumber,
            int? editionPageSize,
            string editionColumns,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));
            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) && c.Key.StartsWith(
                                    string.Format("{0}{1}/",
                                        ConfigurationModules[0], section
                                ))))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }
                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }
            EditionQueryModel editionQueryModel = null;
            if (edition.HasValue)
            {
                editionQueryModel = await _mediator.Send(
                    new EditionCommands.GetCommand(
                        iDEdition: edition.Value));
                if (editionQueryModel == null)
                {
                    _logger.Warning("Edition with id {0} not found", edition.Value);
                    throw new HttpException(404, "Edition not found");
                }
            }

            return View(new ConfigurationsViewModel(
                module: "pages",
                configurationsLists: configurationsLists,
                editionQueryModel: editionQueryModel,
                pageNumber: pageNumber,
                pageSize: pageSize,
                editionPageSize: editionPageSize,
                editionPageNumber: editionPageNumber,
                editionColumns: editionColumns
            ));
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize"
                && key != "EditionPageNumber" && key != "EditionPageSize" &&
                key != "EditionColumns" && key != "IDEdition"
                ))
            {
                if (setting != "section")
                {
                    var key = string.Format("pages/{0}/{1}",
                        section, setting);
                    var state = collection[setting];
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }

            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            int iDEdition = -1;
            if (this.Request.Params["IDEdition"] != null &&
                int.TryParse(this.Request.Params["IDEdition"], out iDEdition) &&
                iDEdition > 0)
            {
                var editionPageNumber = 1;
                if (this.Request.Params["EditionPageNumber"] != null)
                    int.TryParse(this.Request.Params["EditionPageNumber"], out editionPageNumber);
                var editionPageSize = 10;
                if (this.Request.Params["EditionPageSize"] != null)
                    int.TryParse(this.Request.Params["EditionPageSize"], out editionPageSize);
                var editionColumns = this.Request.Params["EditionColumns"];
                return RedirectToAction("Configurations", new
                {
                    edition = iDEdition,
                    pageNumber,
                    pageSize,
                    editionPageNumber,
                    editionPageSize,
                    editionColumns
                });
            }
            else
            {
                return RedirectToAction("Configurations", new
                {
                    pageNumber,
                    pageSize
                });
            }
        }

        #endregion
    }
}