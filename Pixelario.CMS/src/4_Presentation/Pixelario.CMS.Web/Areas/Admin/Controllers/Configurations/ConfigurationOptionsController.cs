﻿using MediatR;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.ConfigurationOptions;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador")]
    public class ConfigurationOptionsController : ContentController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;

        public ConfigurationOptionsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/ConfigurationOptions
        public async Task<ActionResult> Index(int id,
            int? pageNumber,
            int? pageSize,
            string configurationSearchText,
            int? configurationType,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationOrderBy,
            int? configurationOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var totalConfigurationOptions = await _mediator.Send(
                new ConfigurationCommands.CountConfigurationOptionsCommand(
                    iDConfiguration: id));

            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;

            var configurationOptionQueryModels = await _mediator.Send(
                new ConfigurationCommands.ListConfigurationOptionsCommand(
                    iDConfiguration: id,
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize));
            var configurationQueryModel = await _mediator.Send(
                new ConfigurationCommands.GetCommand(
                    iDConfiguration: id));
            var model = new IndexViewModel(
                configurationQueryModel: configurationQueryModel,
                configurationOptionQueryModels: configurationOptionQueryModels,
                totalCount: totalConfigurationOptions,
                pageNumber: pageNumber,
                pageSize: pageSize,
                configurationSearchText: configurationSearchText,
                configurationType: configurationType,
                configurationOrderBy: configurationOrderBy,
                configurationOrderDirection: configurationOrderDirection,
                configurationPageNumber: configurationPageNumber,
                configurationPageSize: configurationPageSize);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Enable(ConfigurationCommands.EnableConfigurationOptionCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
               solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La opción fue editada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }

            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            var configurationPageNumber = 1;
            if (this.Request.Params["ConfigurationPageNumber"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageNumber"], out configurationPageNumber);
            var configurationPageSize = 10;
            if (this.Request.Params["ConfigurationPageSize"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageSize"], out configurationPageSize);

            var configurationSearchText = this.Request.Params["ConfigurationSearchText"];
            var configurationType = 0;
            if (this.Request.Params["ConfigurationType"] != null)
                int.TryParse(this.Request.Params["ConfigurationType"], out configurationType);
            var configurationOrderBy = 0;
            if (this.Request.Params["ConfigurationOrderBy"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderBy"], out configurationOrderBy);
            var configurationOrderDirection = 0;
            if (this.Request.Params["ConfigurationOrderDirection"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderDirection"], out configurationOrderDirection);


            return RedirectToAction("Index", new
            {
                id = command.IDConfiguration,
                pageNumber,
                pageSize,
                configurationPageNumber,
                configurationPageSize,
                configurationSearchText,
                configurationType,
                configurationOrderBy,
                configurationOrderDirection
            });
        }
        //[HttpPost]
        //public async Task<ActionResult> OrderAtHome(ConfigurationCommands.ChangeConfigurationOptionOrderCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (response.success)
        //    {
        //        TempData["success"] = "La opción fue editado exitosamente.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = response.errors;
        //    }
        //    var pageNumber = 1;
        //    if (this.Request.Params["PageNumber"] != null)
        //        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
        //    var pageSize = 10;
        //    if (this.Request.Params["PageSize"] != null)
        //        int.TryParse(this.Request.Params["PageSize"], out pageSize);
        //    var columns = this.Request.Params["Columns"];
        //    var configurationPageNumber = 1;
        //    if (this.Request.Params["ConfigurationPageNumber"] != null)
        //        int.TryParse(this.Request.Params["ConfigurationPageNumber"], out configurationPageNumber);
        //    var configurationPageSize = 10;
        //    if (this.Request.Params["ConfigurationPageSize"] != null)
        //        int.TryParse(this.Request.Params["ConfigurationPageSize"], out configurationPageSize);
        //    var configurationColumns = this.Request.Params["ConfigurationColumns"];

        //    return RedirectToAction("Index", new
        //    {
        //        id = command.IDConfiguration,
        //        pageNumber,
        //        pageSize,
        //        columns,
        //        configurationPageNumber,
        //        configurationPageSize,
        //        configurationColumns
        //    });
        //}
        [HttpPost]
        public async Task<ActionResult> Delete(ConfigurationCommands.DeleteConfigurationOptionCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La opción fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            var configurationPageNumber = 1;
            if (this.Request.Params["ConfigurationPageNumber"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageNumber"], out configurationPageNumber);
            var configurationPageSize = 10;
            if (this.Request.Params["ConfigurationPageSize"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageSize"], out configurationPageSize);

            var configurationSearchText = this.Request.Params["ConfigurationSearchText"];
            var configurationType = 0;
            if (this.Request.Params["ConfigurationType"] != null)
                int.TryParse(this.Request.Params["ConfigurationType"], out configurationType);
            var configurationOrderBy = 0;
            if (this.Request.Params["ConfigurationOrderBy"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderBy"], out configurationOrderBy);
            var configurationOrderDirection = 0;
            if (this.Request.Params["ConfigurationOrderDirection"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderDirection"], out configurationOrderDirection);


            return RedirectToAction("Index", new
            {
                id = command.IDConfiguration,
                pageNumber,
                pageSize,
                configurationPageNumber,
                configurationPageSize,
                configurationSearchText,
                configurationType,
                configurationOrderBy,
                configurationOrderDirection
            });
        }
        #endregion
        #region Create
        //Get Admin/ConfigurationOptions/Create
        public async Task<ActionResult> Create(int id,
            int? pageNumber,
            int? pageSize, string configurationSearchText,
            int? configurationType,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationOrderBy,
            int? configurationOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationQueryModel = await _mediator.Send(new ConfigurationCommands.GetCommand(
                iDConfiguration: id
                ));
            if (configurationQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró la configuración buscada." };
                return RedirectToAction("Index", "Configurations", new
                {
                    pageNumber = configurationPageNumber,
                    pageSize = configurationPageSize
                });
            }
            var model = new FormViewModel(
                mode: "create",
                pageNumber: pageNumber,
                pageSize: pageSize,
                configurationQueryModel: configurationQueryModel,
                configurationPageNumber: configurationPageNumber,
                configurationPageSize: configurationPageSize,
                configurationSearchText: configurationSearchText,
                configurationType: configurationType,
                configurationOrderBy: configurationOrderBy,
                configurationOrderDirection: configurationOrderDirection);
            return View("Form", model);
        }
        //Get Admin/ConfigurationOptions/Create
        [HttpPost]
        public async Task<ActionResult> Create(ConfigurationCommands.AddConfigurationOptionCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);

            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            var configurationPageNumber = 1;
            if (this.Request.Params["ConfigurationPageNumber"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageNumber"], out configurationPageNumber);
            var configurationPageSize = 10;
            if (this.Request.Params["ConfigurationPageSize"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageSize"], out configurationPageSize);

            var configurationSearchText = this.Request.Params["ConfigurationSearchText"];
            var configurationType = 0;
            if (this.Request.Params["ConfigurationType"] != null)
                int.TryParse(this.Request.Params["ConfigurationType"], out configurationType);
            var configurationOrderBy = 0;
            if (this.Request.Params["ConfigurationOrderBy"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderBy"], out configurationOrderBy);
            var configurationOrderDirection = 0;
            if (this.Request.Params["ConfigurationOrderDirection"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderDirection"], out configurationOrderDirection);

            try
            {

                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La opción fue creada exitosamente.";

                    return RedirectToAction("Index", new
                    {
                        id = command.IDConfiguration,
                        configurationPageNumber,
                        configurationPageSize,
                        configurationSearchText,
                        configurationType,
                        configurationOrderBy,
                        configurationOrderDirection
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }
            var iDConfiguration = -1;
            if (this.Request.Params["IDConfiguration"] != null &&
                int.TryParse(this.Request.Params["IDConfiguration"], out configurationType) &&
                iDConfiguration > 0)
            {
                var configurationQueryModel = await _mediator.Send(new ConfigurationCommands.GetCommand(
                    iDConfiguration: iDConfiguration
                    ));
                if (configurationQueryModel != null)
                {
                    var model = new FormViewModel(
                        mode: "create",
                        configurationQueryModel: configurationQueryModel,
                        command: command,
                        pageNumber: pageNumber,
                        pageSize: pageSize,
                        configurationPageNumber: configurationPageNumber,
                        configurationPageSize: configurationPageSize,
                        configurationSearchText: configurationSearchText,
                        configurationType: configurationType,
                        configurationOrderBy: configurationOrderBy,
                        configurationOrderDirection: configurationOrderDirection);
                    return View("Form", model);
                }
            }
            TempData["errors"] = new List<string>() { "No se encontró la configuración buscada." };
            return RedirectToAction("Index", "Configurations", new
            {
                pageNumber = configurationPageNumber,
                pageSize = configurationPageSize
            });
        }
        #endregion
        #region Edicion
        //Get Admin/ConfigurationOptions/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize, string configurationSearchText,
            int? configurationType,
            int? configurationPageNumber,
            int? configurationPageSize,
            int? configurationOrderBy,
            int? configurationOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationOptionQueryModel = await _mediator.Send(new ConfigurationCommands.GetConfigurationOptionCommand(
                iDConfigurationOption: id));
            if (configurationOptionQueryModel == null)
            {
                TempData["errors"] = "no se encontró el subtema buscado.";
                return RedirectToAction("Index", "Configurations", new
                {
                    id,
                    pageNumber,
                    pageSize,
                    configurationPageNumber,
                    configurationPageSize,
                    configurationSearchText,
                    configurationType,
                    configurationOrderBy,
                    configurationOrderDirection
                });
            }
            var model = new FormViewModel(
                mode: "update",
                configurationOptionQueryModel: configurationOptionQueryModel,
                pageNumber: pageNumber,
                pageSize: pageSize,
                configurationPageNumber: configurationPageNumber,
                configurationPageSize: configurationPageSize,
                configurationSearchText: configurationSearchText,
                configurationType: configurationType,
                configurationOrderBy: configurationOrderBy,
                configurationOrderDirection: configurationOrderDirection);
            return View("Form", model);
        }

        ////Get Admin/ConfigurationOptions/Update
        [HttpPost]
        public async Task<ActionResult> Update(ConfigurationCommands.UpdateConfigurationOptionCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            var configurationPageNumber = 1;
            if (this.Request.Params["ConfigurationPageNumber"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageNumber"], out configurationPageNumber);
            var configurationPageSize = 10;
            if (this.Request.Params["ConfigurationPageSize"] != null)
                int.TryParse(this.Request.Params["ConfigurationPageSize"], out configurationPageSize);

            var configurationSearchText = this.Request.Params["ConfigurationSearchText"];
            var configurationType = 0;
            if (this.Request.Params["ConfigurationType"] != null)
                int.TryParse(this.Request.Params["ConfigurationType"], out configurationType);
            var configurationOrderBy = 0;
            if (this.Request.Params["ConfigurationOrderBy"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderBy"], out configurationOrderBy);
            var configurationOrderDirection = 0;
            if (this.Request.Params["ConfigurationOrderDirection"] != null)
                int.TryParse(this.Request.Params["ConfigurationOrderDirection"], out configurationOrderDirection);

            try
            {
                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La opción fue editado exitosamente.";
                    return RedirectToAction("Index", new
                    {
                        id = command.IDConfiguration,
                        pageNumber,
                        pageSize,
                        configurationPageNumber,
                        configurationPageSize,
                        configurationSearchText,
                        configurationType,
                        configurationOrderBy,
                        configurationOrderDirection
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }

            var iDConfiguration = -1;
            if (this.Request.Params["IDConfiguration"] != null &&
                int.TryParse(this.Request.Params["IDConfiguration"], out configurationType) &&
                iDConfiguration > 0)
            {
                var configurationQueryModel = await _mediator.Send(new ConfigurationCommands.GetCommand(
                    iDConfiguration: iDConfiguration
                    ));

                var model = new FormViewModel(
                    mode: "update",
                    configurationQueryModel: configurationQueryModel,
                    command: command,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    configurationPageNumber: configurationPageNumber,
                    configurationPageSize: configurationPageSize,
                    configurationSearchText: configurationSearchText,
                    configurationType: configurationType,
                    configurationOrderBy: configurationOrderBy,
                    configurationOrderDirection: configurationOrderDirection);
                return View("Form", model);
            }
            TempData["errors"] = new List<string>() { "No se encontró la configuración buscada." };
            return RedirectToAction("Index", "Configurations", new
            {
                pageNumber = configurationPageNumber,
                pageSize = configurationPageSize
            });
        }
        #endregion
    }
}