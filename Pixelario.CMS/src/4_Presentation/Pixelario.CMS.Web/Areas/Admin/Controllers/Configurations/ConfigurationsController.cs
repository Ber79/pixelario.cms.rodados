﻿using MediatR;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.Web.Areas.Admin.Models.Configurations;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using RoleCommands = Pixelario.CMS.Application.Commands.Roles;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador")]
    public class ConfigurationsController : ContentController
    {


        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IImageServices _imageServices;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };

        public ConfigurationsController(
            ILogger logger,
            IMediator mediator,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));

        }
        #region Index
        // GET: Admin/Configuration
        public async Task<ActionResult> Index(
            string searchText, int? configurationType,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                var _configurationType = configurationType;
                if (configurationType == -1)
                    _configurationType = null;
                var totalConfigurations = await _mediator.Send(new ConfigurationCommands.CountCommand(
                    filteredBy: HttpUtility.UrlDecode(searchText),
                    configurationType: _configurationType,
                    keys: null));
                var _orderBy = orderBy ?? 0;
                var _orderDirection = orderDirection ?? 1;
                var _pageNumber = pageNumber ?? 1;
                var _pageSize = pageSize ?? 10;
                var configurationQueryModels = await _mediator.Send(new ConfigurationCommands.ListCommand(
                        filteredBy: HttpUtility.UrlDecode(searchText),
                        keys: null,
                        configurationType: _configurationType,
                        rowIndex: (_pageNumber - 1) * _pageSize,
                        rowCount: _pageSize,
                        orderBy: _orderBy,
                        orderDirection: _orderDirection));

                var model = new IndexViewModel(
                    configurationQueryModels: configurationQueryModels,
                    totalCount: totalConfigurations,
                    searchText: HttpUtility.UrlDecode(searchText),
                    configurationType: _configurationType,
                    pageSize: _pageSize,
                    pageNumber: _pageNumber,
                    orderBy: _orderBy,
                    orderDirection: _orderDirection,
                    pagerItems: 5
                );
                model.CalculatePagerItems();
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                TempData["errors"] = new List<string>() { ex.Message };
                return RedirectToAction("Start", "Home");
            }
        }

        // POST: Admin/Configutationd
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            return RedirectToAction("Index", new
            {
                id = collection["IDEdition"],
                pageNumber = 1,
                pageSize = collection["PageSize"],
                searchText = HttpUtility.UrlEncode(collection["SearchText"]),
                configurationType = collection["ConfigurationType"],
                orderBy = collection["OrderBy"],
                orderDirection = collection["OrderDirection"]
            });
        }


        [HttpPost]
        public async Task<ActionResult> Delete(ConfigurationCommands.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "La configuración fue borrada exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var searchText = this.Request.Params["SearchText"];
            var configurationType = -1;
            if (this.Request.Params["ConfigurationTypeParameter"] != null)
                int.TryParse(this.Request.Params["ConfigurationTypeParameter"], out configurationType);
            var orderBy = 0;
            if (this.Request.Params["OrderBy"] != null)
                int.TryParse(this.Request.Params["OrderBy"], out orderBy);
            var orderDirection = 1;
            if (this.Request.Params["OrderDirection"] != null)
                int.TryParse(this.Request.Params["OrderDirection"], out orderDirection);
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                searchText,
                configurationType,
                orderBy,
                orderDirection
            });
        }
        #endregion
        #region Nuevo
        //Get Admin/Configuration/Create
        public ActionResult Create(string searchText, int? configurationType,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var model = new FormViewModel(
                mode: "create",
                searchText: HttpUtility.UrlDecode(searchText),
                configurationTypeParameter: configurationType,
                pageSize: pageSize ?? 1,
                pageNumber: pageNumber ?? 10,
                orderBy: orderBy ?? 0,
                orderDirection: orderDirection ?? 0
                );
            return View("Form", model);
        }
        //Get Admin/Configuration/Nuevo
        [HttpPost]
        public async Task<ActionResult> Create(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                try
                {
                    var command = model.ToAddCommand(
                        createdBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "La configuración fue creada exitosamente.";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in response.errors)
                        {
                            TempData["errors"] = response.errors;
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["errors"] = new List<string>() { ex.Message };
                }
            }
            model.SetMode("create");
            return View("Form", model);

        }
        #endregion
        #region Update
        //Get Admin/Configuration/Update
        public async Task<ActionResult> Update(int id,
            string searchText, int? configurationType,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationQueryModel = await _mediator.Send(new ConfigurationCommands.GetCommand(
                iDConfiguration: id));
            if (configurationQueryModel == null)
            {
                TempData["errors"] = new List<string>() { "No se encontró la configuración." };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    searchText,
                    configurationType,
                    orderBy,
                    orderDirection
                });
            }
            var model = new FormViewModel(
                mode: "update",
                configurationQueryModel: configurationQueryModel,
                searchText: HttpUtility.UrlDecode(searchText),
                configurationTypeParameter: configurationType,
                pageSize: pageSize ?? 1,
                pageNumber: pageNumber ?? 10,
                orderBy: orderBy ?? 0,
                orderDirection: orderDirection ?? 0
               );
            return View("Form", model);

        }
        //Get Admin/Configuration/Update
        [HttpPost]
        public async Task<ActionResult> Update(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                var command = model.ToUpdateCommand(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La configuración fue editada exitosamente.";
                    var pageNumber = 1;
                    if (this.Request.Params["PageNumber"] != null)
                        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                    var pageSize = 10;
                    if (this.Request.Params["PageSize"] != null)
                        int.TryParse(this.Request.Params["PageSize"], out pageSize);
                    var searchText = this.Request.Params["SearchText"];
                    var configurationType = -1;
                    if (this.Request.Params["ConfigurationTypeParameter"] != null)
                        int.TryParse(this.Request.Params["ConfigurationTypeParameter"], out configurationType);
                    var orderBy = 0;
                    if (this.Request.Params["OrderBy"] != null)
                        int.TryParse(this.Request.Params["OrderBy"], out orderBy);
                    var orderDirection = 1;
                    if (this.Request.Params["OrderDirection"] != null)
                        int.TryParse(this.Request.Params["OrderDirection"], out orderDirection);
                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize,
                        searchText,
                        configurationType,
                        orderBy,
                        orderDirection
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }
            model.SetMode("update");
            return View("Form", model);
        }
        #endregion

        public async Task<ActionResult> Roles(int id,
            string searchText, int? configurationType,
            int? pageNumber, int? pageSize,
            int? orderBy,
            int? orderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationQueryModel = await _mediator.Send(new ConfigurationCommands.GetCommand(
                iDConfiguration: id));
            if (configurationQueryModel == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar la configuración."
                };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    searchText,
                    configurationType,
                    orderBy,
                    orderDirection
                });
            }
            var applicationRoles = await _mediator.Send(new RoleCommands.ListCommand(
                orderBy: "Name"));

            var model = new FormViewModel(
                mode: "roles",
                configurationQueryModel: configurationQueryModel,
                applicationRoles: applicationRoles,
                searchText: HttpUtility.UrlDecode(searchText),
                configurationTypeParameter: configurationType,
                pageSize: pageSize ?? 1,
                pageNumber: pageNumber ?? 10,
                orderBy: orderBy ?? 0,
                orderDirection: orderDirection ?? 0
                );
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BindRoles(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                var command = model.ToBindRolesCommand(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "La configuración fue editada exitosamente.";
                    var pageNumber = 1;
                    if (this.Request.Params["PageNumber"] != null)
                        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                    var pageSize = 10;
                    if (this.Request.Params["PageSize"] != null)
                        int.TryParse(this.Request.Params["PageSize"], out pageSize);
                    var searchText = this.Request.Params["SearchText"];
                    var configurationType = -1;
                    if (this.Request.Params["ConfigurationTypeParameter"] != null)
                        int.TryParse(this.Request.Params["ConfigurationTypeParameter"], out configurationType);
                    var orderBy = 0;
                    if (this.Request.Params["OrderBy"] != null)
                        int.TryParse(this.Request.Params["OrderBy"], out orderBy);
                    var orderDirection = 1;
                    if (this.Request.Params["OrderDirection"] != null)
                        int.TryParse(this.Request.Params["OrderDirection"], out orderDirection);
                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize,
                        searchText,
                        configurationType,
                        orderBy,
                        orderDirection
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            model.SetMode("update");
            var applicationRoles = await _mediator.Send(new RoleCommands.ListCommand(
                orderBy: "Name"));

            model.SetRoles(
                applicationRoles: applicationRoles);
            return View(model);
        }
    }
}