﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.SocialMediaSettings;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class SocialMediaSettingsController : ContentController
    {

        private readonly string[] ConfigurationModules = { "socialmedia/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        public SocialMediaSettingsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations(int? pageNumber,
            int? pageSize,
            string columnOrder,
            string orderDirection,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));
            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) &&
                                c.Key.IndexOf(
                                    string.Format("/{0}/", section)) > -1))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }
                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }

            return View(new ConfigurationsViewModel(
                module: "home",
                configurationsLists: configurationsLists,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns
            ));
        }
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize" &&
                    key != "Columns"))
            {
                if (setting != "section")
                {
                    var state = collection[setting];
                    var key = string.Format("socialmedia/{0}/{1}",
                        section, setting);
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns
            });
        }

        #endregion
    }
}