﻿using MediatR;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Topics;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class TopicsController : ContentController
    {
        private readonly string[] ConfigurationModules = { "topics/" };
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };
        private ILogger _logger;
        private readonly IMediator _mediator;

        public TopicsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/Temas
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize, string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var totalCount = await _mediator.Send(new TopicCommands.CountCommand());
            var topicQueryModels = await _mediator.Send(new TopicCommands.ListCommand(
                rowIndex: (_pageNumber - 1) * _pageSize,
                rowCount: _pageSize
                ));
            var model = new IndexViewModel(
                topicQueryModels: topicQueryModels,
                totalCount: totalCount,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Enable(TopicCommands.EnableTopicCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }
        [HttpPost]
        public async Task<ActionResult> ToHome(TopicCommands.TopicToHomeCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }

        [HttpPost]
        public async Task<ActionResult> ToMenu(TopicCommands.ToMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }
        [HttpPost]
        public async Task<ActionResult> OrderAtHome(TopicCommands.ChangeTopicHomeOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }

        [HttpPost]
        public async Task<ActionResult> OrderAtMenu(TopicCommands.OrderAtMenuCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }

        [HttpPost]
        public async Task<ActionResult> Delete(TopicCommands.DeleteTopicCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }
        #endregion
        #region Nuevo
        //Get Admin/Temas/Nuevo
        public async Task<ActionResult> Create(int? pageNumber,
            int? pageSize, string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var editionTypes = new List<EditionType>() { EditionType.News, EditionType.Information };
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: editionTypes));
            var model = new FormViewModel(
                mode: "create",
                availableEditions: availableEditions,
                editions: new List<int>(),
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns);
            return View("Form", model);
        }
        //Get Admin/Temas/Nuevo
        [HttpPost]
        public async Task<ActionResult> Create(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                try
                {
                    var command = model.ToAddTopicCommand(
                        createdBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        var pageNumber = 1;
                        if (this.Request.Params["PageNumber"] != null)
                            int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                        var pageSize = 10;
                        if (this.Request.Params["PageSize"] != null)
                            int.TryParse(this.Request.Params["PageSize"], out pageSize);
                        var columns = this.Request.Params["Columns"];

                        TempData["success"] = "El tema fue creado exitosamente.";
                        return RedirectToAction("Index", new
                        {
                            pageNumber,
                            pageSize,
                            columns
                        });
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                catch (Exception ex)
                {
                    TempData["errors"] = new List<string>() { ex.Message };
                }
            }
            model.SetMode("create");
            var editionTypes = new List<EditionType>() { EditionType.News, EditionType.Information };
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.Title",
                orderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: editionTypes));
            model.SetAvailableEditions(
                availableEditions: availableEditions);
            if (model.Editions == null)
                model.Editions = new List<int>();
            return View("Form", model);

        }
        #endregion
        #region Edicion
        //Get Admin/Temas/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var topic = await _mediator.Send(new TopicCommands.GetCommand(
                iDTopic: id));

            if (topic != null)
            {
                var editionTypes = new List<EditionType>() { EditionType.News, EditionType.Information };
                var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                    filteredBy: null,
                    pagesOnEditionFilter: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "e.Title",
                    orderDirection: "ASC",
                    enabled: true,
                    atHome: null,
                    onMenu: null,
                    editionTypes: editionTypes));

                var model = new FormViewModel(
                    mode: "update",
                    availableEditions: availableEditions,
                    editions: topic.Editions.Select(e => e.IDEdition)
                        .ToList(),
                    iDTopic: topic.IDTopic,
                    title: topic.Title,
                    summary: topic.Summary,
                    keywords: topic.Keywords,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    columns: columns);
                return View("Form", model);
            }
            else
            {
                TempData["errors"] = new List<string>() { "No se encontró el tema" };
                return RedirectToAction("Index");
            }

        }
        //Get Admin/Temas/Update
        [HttpPost]
        public async Task<ActionResult> Update(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            try
            {
                var command = model.ToUpdateTopicCommand();
                command.SetSolicitedBy(User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El tema fue editado exitosamente.";
                    var pageNumber = 1;
                    if (this.Request.Params["PageNumber"] != null)
                        int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                    var pageSize = 10;
                    if (this.Request.Params["PageSize"] != null)
                        int.TryParse(this.Request.Params["PageSize"], out pageSize);
                    var columns = this.Request.Params["Columns"];
                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize,
                        columns
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                {
                    var errors = (List<string>)TempData["errors"];
                    errors.Add(ex.InnerException.Message);
                    if (ex.InnerException.InnerException != null)
                    {
                        errors.Add(ex.InnerException.InnerException.Message);
                    }
                    TempData["errors"] = errors;
                }
            }
            model.SetMode("update");
            var editionTypes = new List<EditionType>() { EditionType.News, EditionType.Information };
            var availableEditions = await _mediator.Send(new EditionCommands.ListCommand(
                    filteredBy: null,
                    pagesOnEditionFilter: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "e.Title",
                    orderDirection: "ASC",
                    enabled: true,
                    atHome: null,
                    onMenu: null,
                    editionTypes: editionTypes));
            model.SetAvailableEditions(
                availableEditions: availableEditions);
            if (model.Editions == null)
                model.Editions = new List<int>();
            return View("Form", model);
        }
        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            int? pageNumber,
            int? pageSize,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var topicQueryModel = await _mediator.Send(new TopicCommands.GetCommand(
                iDTopic: id));
            if (topicQueryModel != null)
            {
                return View(new FormViewModel(
                    mode: "images",
                    topicQueryModel: topicQueryModel,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    columns: columns));
            }
            else
            {
                TempData["errors"] = new List<string>() { "No se encontró el tema." };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize,
                    columns
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadHomeImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/topics/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new TopicCommands.ChangeTopicImageCommand(
                        iDTopic: model.IDTopic.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El tema fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Images", new
            {
                id = model.IDTopic.Value,
                pageNumber,
                pageSize,
                columns
            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(TopicCommands.RemoveTopicImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El tema fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Images", new
            {
                id = command.IDTopic,
                pageNumber,
                pageSize,
                columns
            });
        }
        #endregion
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations(int? pageNumber,
            int? pageSize,
            string columns)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));
            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) &&
                            c.Key.StartsWith(
                                string.Format("{0}{1}/",
                                ConfigurationModules[0], section
                            ))))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }

                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }
            return View(new ConfigurationsViewModel(
                module: "Topics",
                configurationsLists: configurationsLists,
                pageNumber: pageNumber,
                pageSize: pageSize,
                columns: columns));
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            TempData["success"] = "";
            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize" &&
                    key != "Columns"))
            {
                if (setting != "section")
                {
                    var key = string.Format("topics/{0}/{1}",
                        section, setting);
                    var state = collection[setting];
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        foreach (var error in response.errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);
            var columns = this.Request.Params["Columns"];
            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
                columns,
            });
        }

        #endregion

    }
}