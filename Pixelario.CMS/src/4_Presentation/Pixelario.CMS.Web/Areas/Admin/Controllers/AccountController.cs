﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Pixelario.CMS.Web.Areas.Admin.Models.Home;
using Pixelario.Identity.Infrastructure.Managers;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Serilog;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;       
        private ILogger _logger;
        public AccountController(
            ILogger logger,
            ApplicationSignInManager signInManager,
            IAuthenticationManager authenticationManager
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _authenticationManager = authenticationManager ?? throw new ArgumentNullException(nameof(authenticationManager));
        }
        // GET: Admin/Account
        [AllowAnonymous]
        public ActionResult Index()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _signInManager.PasswordSignInAsync(
                userName: model.Email,
                password: model.Password,
                isPersistent: model.RememberMe == "on",
                shouldLockout: true);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    TempData["errors"] = new List<string>() { "Cuenta bloqueada." };
                    return Redirect("/Admin/");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    TempData["errors"] = new List<string>() { "Intento de login invalido." };
                    return Redirect("/Admin/");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            _authenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}