﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Advertisments;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdvertismentCommands = Pixelario.CMS.Application.Commands.Advertisments;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class AdvertismentsController : ContentController
    {
        private readonly string[] ConfigurationModules = { "advertisments/" };
        private ILogger _logger;
        private readonly IMediator _mediator;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };

        public AdvertismentsController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/Advertisments
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var totalCount = await _mediator.Send(new AdvertismentCommands.CountCommand());
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var advertismentQueryModels = await _mediator.Send(new AdvertismentCommands.ListCommand(
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize));
            var advertismentPlaceAtWebAdminModels =
                new List<AdvertismentPlaceAtWebAdminModel>();
            foreach (var placeAtWeb in AdvertismentPlaceAtWeb.List())
            {
                advertismentPlaceAtWebAdminModels.Add(
                    new AdvertismentPlaceAtWebAdminModel(placeAtWeb));
            }
            var model = new IndexViewModel(
                advertismentQueryModel: advertismentQueryModels,
                totalCount: totalCount,
                pageSize = _pageNumber,
                pageSize = _pageSize);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Enable(AdvertismentCommands.EnableCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        [HttpPost]
        public async Task<ActionResult> Publish(AdvertismentCommands.PublishCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        [HttpPost]
        public async Task<ActionResult> ChangeOrder(AdvertismentCommands.ChangeOrderCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        [HttpPost]
        public async Task<ActionResult> ChangePlaceAtWeb(AdvertismentCommands.ChangePlaceAtWebCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }

        [HttpPost]
        public async Task<ActionResult> Delete(AdvertismentCommands.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }
        #endregion
        #region Nuevo
        //Get Admin/Advertisments/Create
        public ActionResult Create(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var model = new FormViewModel(
                mode: "create",
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }
        //Get Admin/Advertisments/Nuevo
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (ModelState.IsValid)
            {
                try
                {
                    var command = model.ToAddCommand(
                        createdBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El flyer fue creado exitosamente.";
                        var pageNumber = 1;
                        if (this.Request.Params["PageNumber"] != null)
                            int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
                        var pageSize = 10;
                        if (this.Request.Params["PageSize"] != null)
                            int.TryParse(this.Request.Params["PageSize"], out pageSize);

                        return RedirectToAction("Index", new
                        {
                            pageNumber,
                            pageSize
                        });
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                catch (Exception ex)
                {
                    TempData["errors"] = new List<string>() { ex.Message };
                }
            }
            model.SetMode("create");
            return View("Form", model);
        }
        #endregion
        #region Edicion
        //Get Admin/Advertisments/Update
        public async Task<ActionResult> Update(int id,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var advertismentQueryModel = await _mediator.Send(new AdvertismentCommands.GetCommand(
                iDAdvertisment: id));
            if (advertismentQueryModel == null)
            {
                TempData["errors"] = new List<string> { "No se encontró el flyer." };
                return RedirectToAction("Index",
                    new
                    {
                        pageNumber,
                        pageSize
                    });
            }
            var model = new FormViewModel(
                mode: "update",
                advertismentQueryModel: advertismentQueryModel,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }
        //Get Admin/Advertisments/Update
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(AdvertismentCommands.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            int? pageNumber = null;
            if (this.Request.Params["PageNumber"] != null &&
                int.TryParse(this.Request.Params["PageNumber"], out int _pageNumber))
            {
                pageNumber = _pageNumber;
            }
            int? pageSize = null;
            if (this.Request.Params["PageSize"] != null &&
                int.TryParse(this.Request.Params["PageSize"], out int _pageSize))
            {
                pageSize = _pageSize;
            }
            try
            {
                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El flyer fue editada exitosamente.";

                    return RedirectToAction("Index", new
                    {
                        pageNumber,
                        pageSize
                    });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            catch (Exception ex)
            {
                TempData["errors"] = new List<string>() { ex.Message };
            }            
            var model = new FormViewModel(
                mode: "update",
                updateCommand: command,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }
        #endregion
        #region Multimedios

        [Authorize]
        public async Task<ActionResult> Images(int id,
            int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var advertismentQueryModel = await _mediator.Send(new AdvertismentCommands.GetCommand(
                iDAdvertisment: id));
            if (advertismentQueryModel == null)

            {
                TempData["errors"] = new List<string> { "No se encontró el flyer." };

                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
            return View(new FormViewModel(
                mode: "images",
                advertismentQueryModel: advertismentQueryModel,
                pageNumber,
                pageSize
            ));
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UploadImage(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["HomeImage"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/advertisments/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new AdvertismentCommands.ChangeImageCommand(
                        iDAdvertisment: model.IDAdvertisment.Value,
                        imageType: ImageType.Home,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El flyer fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Images", new
            {
                id = model.IDAdvertisment.Value,
                pageNumber,
                pageSize
            });

        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveImage(AdvertismentCommands.RemoveImageCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El flyer fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = 1;
            if (this.Request.Params["PageNumber"] != null)
                int.TryParse(this.Request.Params["PageNumber"], out pageNumber);
            var pageSize = 10;
            if (this.Request.Params["PageSize"] != null)
                int.TryParse(this.Request.Params["PageSize"], out pageSize);

            return RedirectToAction("Images", new
            {
                id = command.IDAdvertisment,
                pageNumber,
                pageSize
            });
        }
        #endregion
        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));
            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) && c.Key.StartsWith(
                                    string.Format("{0}{1}/",
                                        ConfigurationModules[0], section
                                ))))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }
                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }
            return View(new ConfigurationsListViewModel()
            {
                Module = "advertisments",
                ConfigurationsLists = configurationsLists,

            });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys)
            {
                if (setting != "section")
                {
                    var key = string.Format("advertisments/{0}/{1}",
                        section, setting);
                    var state = collection[setting];
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            return RedirectToAction("Configurations");
        }

        #endregion
    }
}