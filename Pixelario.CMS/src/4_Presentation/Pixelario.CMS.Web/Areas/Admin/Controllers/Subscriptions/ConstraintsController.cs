﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Constraints;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers.Subscriptions
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class ConstraintsController : ContentController
    {
        private const string DefaultColumnOrder = "c.IDConstraint";
        private const string DefaultOrderDirection = "DESC";

        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IConstraintsQueries _constraintsQueries;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };

        public ConstraintsController(
            ILogger logger,
            IMediator mediator,
            IConstraintsQueries constraintsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _constraintsQueries = constraintsQueries ?? throw new ArgumentNullException(nameof(constraintsQueries));
        }
        #region Index
        // GET: Admin/Ediciones
        public async Task<ActionResult> Index(int id,
            int pageNumber = 1,
            int pageSize = 10,
            string columnOrder = DefaultColumnOrder,
            string orderDirection = DefaultOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var constraintAdminModels = new List<ConstraintAdminModel>();
            foreach (var queryModel in await _constraintsQueries.ListAsync(
                plan: Plan.From(id),
                entities: null,
                rowIndex: (pageNumber - 1) * pageSize,
                rowCount: pageSize,
                columnOrder: columnOrder,
                orderDirection: orderDirection))
            {
                var constraint = new ConstraintAdminModel(queryModel);
                if (constraint.ConstraintType == ConstraintType.OnEdition)
                {
                    var entity = await _mediator.Send(new EditionCommands.GetCommand(
                        constraint.IDConstrained));
                    if (entity != null)
                    {
                        constraint.SetContrainedTitle(entity.Title);
                    }
                }
                else if (constraint.ConstraintType == ConstraintType.OnArticle)
                {
                    var entity = await _mediator.Send(new ArticleCommands.GetCommand(
                       constraint.IDConstrained));
                    if (entity != null)
                    {
                        constraint.SetContrainedTitle(entity.Title);
                    }
                }
                constraintAdminModels.Add(constraint);
            }
            var model = new IndexViewModel()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Plan = new PlanAdminModel(Plan.From(id)),
                Constraints = constraintAdminModels,
                PageCount = CalculatePageCount(
                totalRows: await _constraintsQueries.CountAsync(
                    plan: Plan.From(id),
                    entities: null), pageSize)
            };
            return View(model);
        }
        #endregion
    }
}