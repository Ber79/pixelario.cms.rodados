﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Accounts;
using Pixelario.Subscriptions.Application.Queries.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers.Subscriptions
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class AccountsController : ContentController
    {
        private const string DefaultColumnOrder = "a.IDAccount";
        private const string DefaultOrderDirection = "DESC";

        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IAccountsQueries _accountsQueries;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };


        public AccountsController(
            ILogger logger,
            IMediator mediator,
            IAccountsQueries accountsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _accountsQueries = accountsQueries ?? throw new ArgumentNullException(nameof(accountsQueries));
        }
        #region Index
        // GET: Admin/Ediciones
        public async Task<ActionResult> Index(int pageNumber = 1,
            int pageSize = 10,
            string columnOrder = DefaultColumnOrder,
            string orderDirection = DefaultOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var accountAdminModels = new List<AccountAdminModel>();
            foreach (var queryModel in await _accountsQueries.ListAsync(
                plans: Plan.List(),
                rowIndex: (pageNumber - 1) * pageSize,
                rowCount: pageSize,
                columnOrder: columnOrder,
                orderDirection: orderDirection))
            {
                var accountAdminModel = new AccountAdminModel(queryModel);
                var user = await _mediator.Send(new UserCommands.GetCommand(
                    userId: queryModel.UserId));
                if (user != null)
                {
                    accountAdminModel.SetUserEmail(
                        email: user.Email);
                }
                accountAdminModels.Add(accountAdminModel);

            }
            var model = new IndexViewModel()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Accounts = accountAdminModels,
                PageCount = CalculatePageCount(
                totalRows: await _accountsQueries.CountAsync(
                    plans: Plan.List()), pageSize)
            };
            return View(model);
        }

        //[HttpPost]
        //public async Task<ActionResult> Enable(EnableCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}
        //[HttpPost]
        //public async Task<ActionResult> ToHome(ToHomeCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public async Task<ActionResult> ChangeAccountType(ChangeAccountTypeCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public async Task<ActionResult> Delete(DeleteCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue borrada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}
        #endregion
    }
}