﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.Commons;
using Pixelario.CMS.Web.Areas.Admin.Models.Subscriptions.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers.Subscriptions
{
    [Authorize(Roles = "Super Administrador, Administrador")]
    public class PlansController : ContentController
    {
        private const string DefaultColumnOrder = "a.IDAccount";
        private const string DefaultOrderDirection = "DESC";

        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly string[] ConfigurationModules = { "subscriptions/plans/" };

        public PlansController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        #region Index
        // GET: Admin/Ediciones
        public async Task<ActionResult> Index(int pageNumber = 1,
            int pageSize = 10,
            string columnOrder = DefaultColumnOrder,
            string orderDirection = DefaultOrderDirection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var planAdminModels = new List<PlanAdminModel>();
            int rowIndex = pageSize * (pageNumber - 1);
            foreach (var plan in Plan.List().Skip(rowIndex).Take(pageSize))
            {
                planAdminModels.Add(new PlanAdminModel(plan));
            }
            var model = new IndexViewModel()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Plans = planAdminModels,
                PageCount = CalculatePageCount(
                    totalRows: Plan.List().Count,
                    pageSize: pageSize)
            };
            return View(model);
        }

        //[HttpPost]
        //public async Task<ActionResult> Enable(EnableCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}
        //[HttpPost]
        //public async Task<ActionResult> ToHome(ToHomeCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public async Task<ActionResult> ChangeAccountType(ChangeAccountTypeCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue editada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public async Task<ActionResult> Delete(DeleteCommand command)
        //{
        //    _logger.Information("Request started {0} {1}",
        //        Request.HttpMethod,
        //        Request.RawUrl);
        //    command.SetSolicitedBy(User.Identity.Name);
        //    var response = await _mediator.Send(command);
        //    if (responsa.success)
        //    {
        //        TempData["success"] = "La edición fue borrada exitosamenta.";
        //    }
        //    else
        //    {
        //        TempData["errors"] = responsa.errors;
        //    }
        //    return RedirectToAction("Index");
        //}
        #endregion


        #region Configurations
        [Authorize]
        public async Task<ActionResult> Configurations(
            int pageNumber = 1,
            int pageSize = 10)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: ConfigurationModules,
                    configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0));
            var sections = configurations
                .GroupBy(c => c.Section)
                .Select(c => c.First().Section).ToList();
            var configurationsLists = new List<ConfigurationsListAdminModel>();
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: user.Id));

            foreach (var section in sections)
            {
                var configurationItems = new List<ConfigurationFormAdminModel>();
                foreach (var configurationItem in configurations.Where(
                            c => (
                                    userRoles.Contains("Super Administrador") ||
                                    c.GetBindedRoles().Intersect(userRoles).Any()
                                ) &&
                                c.Key.IndexOf(
                                    string.Format("/{0}/", section)) > -1))
                {
                    configurationItems.Add(new ConfigurationFormAdminModel(
                        queryModel: configurationItem));
                }
                configurationsLists.Add(
                    new ConfigurationsListAdminModel()
                    {
                        Section = section,
                        Configurations = configurationItems
                    });
            }

            return View(new ConfigurationsViewModel()
            {
                PageTitle = "Configuraciones de subscripciones",
                ControllerName = "Plans",
                Module = "subscriptions",
                ConfigurationsLists = configurationsLists,
                PageNumber = pageNumber,
                PageSize = pageSize
            });
        }
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Configurations(FormCollection collection)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var section = collection["section"];
            var hadErrors = false;
            foreach (var setting in collection.AllKeys.Where(
                key => key != "PageNumber" && key != "PageSize"))
            {
                if (setting != "section")
                {
                    var state = collection[setting];
                    var key = string.Format("subscriptions/{0}/{1}",
                        section, setting);
                    if (state.StartsWith("!checkbox"))
                    {
                        if (state == "!checkbox")
                            state = "";
                        else
                            state = "on";
                    }

                    var command = new ConfigurationCommands.SetStateCommand(
                        key: key,
                        state: state
                        );
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (!response.success)
                    {
                        hadErrors = true;
                        TempData["errors"] = response.errors;
                    }
                }
            }
            if (!hadErrors)
                TempData["success"] = "Las configuraciones fueron guardadas correctamente.";
            var pageNumber = 1;
            if (collection["PageNumber"] != null &&
                int.TryParse(collection["PageNumber"], out pageNumber))
            {
                if (pageNumber <= 0)
                    pageNumber = 1;
            }
            var pageSize = 10;
            if (collection["PageSize"] != null &&
                int.TryParse(collection["PageSize"], out pageSize))
            {
                if (pageSize <= 0)
                    pageSize = 10;
            }

            return RedirectToAction("Configurations", new
            {
                pageNumber,
                pageSize,
            });
        }

        #endregion



    }
}