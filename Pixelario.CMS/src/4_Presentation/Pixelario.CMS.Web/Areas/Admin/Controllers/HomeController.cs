﻿using Pixelario.CMS.Application.Queries.Editions;
using Pixelario.CMS.Web.Areas.Admin.Models;
using Pixelario.CMS.Web.Areas.Admin.Models.Home;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    public class HomeController : ContentController
    {
        private ILogger _logger;
        public HomeController(
            ILogger logger,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        // GET: Admin/Home
        public ActionResult Index(string ReturnUrl)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Start");
            var model = new LoginViewModel();
            if (!string.IsNullOrEmpty(ReturnUrl))
                model.ReturnUrl = ReturnUrl;
            else
                model.ReturnUrl = "~/Admin/Editions/Index";
            return View(model);
        }
        [Authorize(Roles = "Super Administrador, Administrador")]
        public async Task<ActionResult> Start()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            return View(new ContentViewModel()
            {
            });
        }
    }
}