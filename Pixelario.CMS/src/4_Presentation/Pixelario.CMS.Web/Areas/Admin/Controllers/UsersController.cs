﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.Users;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RoleCommands = Pixelario.CMS.Application.Commands.Roles;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador")]
    public class UsersController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private string[] imageExtentions = { ".jpg", ".png", ".jpeg" };

        public UsersController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        // GET: Admin/Usuarios
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;
            var applicationUsers = await _mediator.Send(new UserCommands.ListCommand(
                rowIndex: (_pageNumber - 1) * _pageSize,
                rowCount: _pageSize
                ));
            var applicationUserCount = await _mediator.Send(new UserCommands.CountCommand(
                ));
            var model = new IndexViewModel(
                applicationUsers: applicationUsers,
                totalCount: applicationUserCount,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View(model);
        }

        public ActionResult Create(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var model = new FormViewModel(
                mode: "create",
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserCommands.AddCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetCreatedBy(
               createdBy: this.User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El usuario fue creado exitosamente.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            int? _pageNumber = null;
            if (int.TryParse(this.Request.Params["PageNumber"], out int value1))
            {
                _pageNumber = value1;
            }
            int? _pageSize = null;
            if (int.TryParse(this.Request.Params["PageSize"], out int value2))
            {
                _pageSize = value2;
            }
            var model = new FormViewModel(
                mode: "create",
                pageNumber: _pageNumber,
                pageSize: _pageSize);
            return View("Form", model);
        }
        [HttpPost]
        public async Task<ActionResult> Delete(UserCommands.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
                solicitedBy: this.User.Identity.Name);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];

            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El usuario fue borrado exitosamente.";
                pageNumber = "1";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index", new
            {
                pageNumber,
                pageSize
            });
        }


        #region update
        public async Task<ActionResult> Update(int id,
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
                userId: id));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
            var model = new FormViewModel(
                applicationUser: applicationUser,
                mode: "update",
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(UserCommands.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
                solicitedBy: this.User.Identity.Name);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El usuario fue editado exitosamente.";
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            int? _pageNumber = null;
            if (int.TryParse(pageNumber, out int value1))
            {
                _pageNumber = value1;
            }
            int? _pageSize = null;
            if (int.TryParse(pageSize, out int value2))
            {
                _pageSize = value2;
            }
            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
               userId: command.UserId));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
            var model = new FormViewModel(
                applicationUser: applicationUser,
                mode: "update",
                pageNumber: _pageNumber,
                pageSize: _pageSize);
            return View("Form", model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> MyProfile()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!User.Identity.IsAuthenticated)
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para editar el perfil de usuario."
                };
                return RedirectToAction("Start", "Home");
            }
            var applicationUser = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: User.Identity.Name));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            var model = new FormViewModel(
                applicationUser: applicationUser,
                mode: "myprofile",
                pageNumber: null,
                pageSize: null);
            return View("Form", model);
        }


        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> MyProfile(UserCommands.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!User.Identity.IsAuthenticated)
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para editar el perfil de usuario."
                };
                return RedirectToAction("Start", "Home");
            }
            var applicationUser = await _mediator.Send(new UserCommands.GetByNameCommand(
                    userName: User.Identity.Name));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");

            }
            command.SetUserId(
                userId: applicationUser.Id);
            command.SetSolicitedBy(
                solicitedBy: this.User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "Su perfil de usuario fue editado exitosamente.";
                return RedirectToAction("MyProfile");
            }
            else
            {
                TempData["errors"] = response.errors;

            }
            var model = new FormViewModel(
                applicationUser: applicationUser,
                mode: "myprofile",
                pageNumber: null,
                pageSize: null);
            return View("Form", model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeMyPicture(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!User.Identity.IsAuthenticated)
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para editar el perfil de usuario."
                };
                return RedirectToAction("Start", "Home");
            }
            var applicationUser = await _mediator.Send(new UserCommands.GetByNameCommand(
               userName: User.Identity.Name));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];

            var appDomainAppPath = HttpRuntime.AppDomainAppPath;
            var file = Request.Files["Image"];
            if (file != null)
            {
                string rawName = file.FileName;
                byte[] buf = new byte[file.ContentLength];
                file.InputStream.Read(buf, 0, file.ContentLength);
                string extension = Path.GetExtension(rawName);
                if (imageExtentions.Contains(extension.ToLower()))
                {
                    string rawPath = Server.MapPath("~/uploads/users/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var command = new UserCommands.ChangePictureCommand(
                        userId: applicationUser.Id,
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El usuario fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
                else
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
            }
            else
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            return RedirectToAction("MyProfile", new
            {
                pageNumber,
                pageSize
            });

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePicture(UserCommands.ChangePictureCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];
            var file = Request.Files["Image"];
            if (file == null)
            {
                TempData["errors"] = new List<string>() { "No seleccionó ningun archivo para subir." };
            }
            else
            {
                string rawName = file.FileName;
                string extension = Path.GetExtension(rawName);
                if (!imageExtentions.Contains(extension.ToLower()))
                {
                    TempData["errors"] = new List<string>() { $"Formato de ímagen no aceptado, debe ser { string.Join(", ", imageExtentions)}." };
                }
                else
                {
                    byte[] buf = new byte[file.ContentLength];
                    file.InputStream.Read(buf, 0, file.ContentLength);
                    string rawPath = Server.MapPath("~/uploads/users/raw");
                    if (!Directory.Exists(rawPath))
                        Directory.CreateDirectory(rawPath);
                    Guid guid = Guid.NewGuid();
                    string fileName = string.Format("{0}{1}",
                         guid.ToString("N").Substring(0, 13),
                        extension);
                    file.SaveAs(Path.Combine(rawPath, fileName));
                    var appDomainAppPath = HttpRuntime.AppDomainAppPath;
                    command.SetAppDomainAppPath(
                        appDomainAppPath: appDomainAppPath,
                        fileName: fileName);
                    command.SetSolicitedBy(
                        solicitedBy: User.Identity.Name);
                    var response = await _mediator.Send(command);
                    if (response.success)
                    {
                        TempData["success"] = "El usuario fue editado exitosamente.";
                    }
                    else
                    {
                        TempData["errors"] = response.errors;
                    }
                }
            }
            return RedirectToAction("Update", new
            {
                id = command.UserId,
                pageNumber,
                pageSize
            });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePicture(UserCommands.RemovePictureCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El usuario fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];
            return RedirectToAction("Update", new
            {
                id = command.UserId,
                pageNumber,
                pageSize
            });
        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveMyPicture(FormViewModel model)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!User.Identity.IsAuthenticated)
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para editar el perfil de usuario."
                };
                return RedirectToAction("Start", "Home");
            }
            var applicationUser = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: User.Identity.Name));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];


            var command = new UserCommands.RemovePictureCommand(
                userId: applicationUser.Id);
            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El usuario fue editado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }

            return RedirectToAction("MyProfile", new
            {
                pageNumber,
                pageSize
            });

        }
        #endregion


        public async Task<ActionResult> Roles(int id,
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
                userId: id));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            var userRoles = await _mediator.Send(new UserCommands.ListUserRolesCommand(
                userId: id));
            var applicationRoles = await _mediator.Send(new RoleCommands.ListCommand(
                orderBy: "Name"));
            var model = new FormViewModel(
                mode: "roles",
                applicationUser: applicationUser,
                applicatioRoles: applicationRoles,
                userRoles: userRoles,
                pageNumber: pageNumber,
                pageSize: pageSize);

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetRoles(UserCommands.SetRolesCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];
            if (response.success)
            {

                TempData["success"] = "El usuario fue editado exitosamente.";
                return RedirectToAction("Index",
                    new
                    {
                        pageNumber,
                        pageSize
                    });
            }
            else
            {
                TempData["errors"] = response.errors;
                return RedirectToAction("Roles",
                new
                {
                    id = command.UserId,
                    pageNumber,
                    pageSize
                });
            }
        }
        [AllowAnonymous]
        public async Task<ActionResult> Password(int id,
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
                userId: id));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            if (!(User.Identity.IsAuthenticated && (
                User.IsInRole("Super Administrador") ||
                User.Identity.Name == applicationUser.UserName)))
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para cambiar el password de este usuario."
                };
                return RedirectToAction("Start", "Home");
            }
            var model = new FormViewModel(
                mode: "password",
                applicationUser: applicationUser,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("FormPassword", model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> MyPassword()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!User.Identity.IsAuthenticated)
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para editar el perfil de usuario."
                };
                return RedirectToAction("Start", "Home");
            }

            var applicationUser = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: User.Identity.Name));
            var model = new FormViewModel(
                mode: "mypassword",
                applicationUser: applicationUser,
                pageNumber: null,
                pageSize: null
                );
            return View("FormPassword", model);

        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(UserCommands.ChangePasswordCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];

            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
                    userId: command.UserId));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }
            if (!(User.Identity.IsAuthenticated && (
                User.IsInRole("Super Administrador") ||
                User.Identity.Name == applicationUser.UserName)))
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para cambiar el password de este usuario."
                };
                return RedirectToAction("Start", "Home");
            }


            var confirmPassword = this.Request.Params["ConfirmPassword"];
            if (confirmPassword != command.Password)
            {
                TempData["errors"] = new List<string>() {
                    "El password con fue confirmado correctamente."
                };
            }
            else
            {

                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {

                    TempData["success"] = "El password del usuario fue editado exitosamente.";
                    return RedirectToAction("Index",
                        new
                        {
                            pageNumber,
                            pageSize
                        });
                }
                else
                {
                    TempData["errors"] = response.errors;
                }
            }
            int? _pageNumber = null;
            if (int.TryParse(pageNumber, out int value1))
            {
                _pageNumber = value1;
            }
            int? _pageSize = null;
            if (int.TryParse(pageSize, out int value2))
            {
                _pageSize = value2;
            }
            var model = new FormViewModel(
                mode: "password",
                applicationUser: applicationUser,
                pageNumber: _pageNumber,
                pageSize: _pageSize);
            return View("FormPassword", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetMyPassword(UserCommands.ChangePasswordCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var applicationUser = await _mediator.Send(new UserCommands.GetCommand(
                userId: command.UserId));
            if (applicationUser == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el usuario."
                    };
                return RedirectToAction("Start", "Home");
            }

            if (!(User.Identity.IsAuthenticated &&
                User.Identity.Name == applicationUser.UserName))
            {
                TempData["errors"] = new List<string>() {
                    "No tiene permisos para cambiar el password de este usuario."
                };
                return RedirectToAction("Start", "Home");

            }
            var confirmPassword = this.Request.Params["ConfirmPassword"];
            if (confirmPassword == command.Password)
            {
                command.SetSolicitedBy(
                    solicitedBy: User.Identity.Name);
                var response = await _mediator.Send(command);
                if (response.success)
                {
                    TempData["success"] = "El password del usuario fue editado exitosamente.";
                    return RedirectToAction("Start", "Home");
                }
                else
                    TempData["errors"] = response.errors;
            }
            else
            {
                TempData["errors"] = new List<string>() {
                                "El password con fue confirmado correctamente."
                            };
            }
            var model = new FormViewModel(
                mode: "mypassword",
                applicationUser: applicationUser,
                pageNumber: null,
                pageSize: null);
            return View("FormPassword", model);
        }
    }
}