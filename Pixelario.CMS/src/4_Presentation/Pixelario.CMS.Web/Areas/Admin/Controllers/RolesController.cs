﻿using MediatR;
using Pixelario.CMS.Web.Areas.Admin.Models.Roles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using RolesCommand = Pixelario.CMS.Application.Commands.Roles;
namespace Pixelario.CMS.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super Administrador")]
    public class RolesController : BaseController
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public RolesController(
            ILogger logger,
            IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        // GET: Admin/Usuarios
        public async Task<ActionResult> Index(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var _pageNumber = pageNumber ?? 1;
            var _pageSize = pageSize ?? 10;

            var applicationRoles = await _mediator.Send(new RolesCommand.ListCommand(
                rowIndex: (_pageNumber - 1) * _pageSize,
                rowCount: _pageSize));
            var applicationRoleCount = await _mediator.Send(new RolesCommand.CountCommand(
                ));
            var model = new IndexViewModel(
                applicationRoles: applicationRoles,
                totalCount: applicationRoleCount,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View(model);
        }

        public ActionResult Create(int? pageNumber,
            int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var model = new FormViewModel(
                mode: "create",
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RolesCommand.AddCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            command.SetCreatedBy(
                createdBy: this.User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El role fue creado exitosamente.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            int? _pageNumber = null;
            if (int.TryParse(this.Request.Params["PageNumber"], out int value1))
            {
                _pageNumber = value1;
            }
            int? _pageSize = null;
            if (int.TryParse(this.Request.Params["PageSize"], out int value2))
            {
                _pageSize = value2;
            }
            var model = new FormViewModel(
                command: command,
                pageNumber: _pageNumber,
                pageSize: _pageSize);
            return View("Form", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(RolesCommand.DeleteCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var pageNumber = this.Request.Params["PageNumber"];
            var pageSize = this.Request.Params["PageSize"];

            command.SetSolicitedBy(
                solicitedBy: User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El role fue borrado exitosamente.";
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            return RedirectToAction("Index",
                new
                {
                    pageNumber,
                    pageSize
                });
        }

        public async Task<ActionResult> Update(int id,
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var applicationRole = await _mediator.Send(new RolesCommand.GetCommand(
                roleId: id));
            if (applicationRole == null)
            {
                TempData["errors"] = new List<string>() {
                    "No se puede encontrar el role."
                };
                return RedirectToAction("Index", new
                {
                    pageNumber,
                    pageSize
                });
            }
            var model = new FormViewModel(
                mode: "update",
                applicationRole: applicationRole,
                pageNumber: pageNumber,
                pageSize: pageSize);
            return View("Form", model);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(RolesCommand.UpdateCommand command)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            int? _pageNumber = null;
            if (int.TryParse(this.Request.Params["PageNumber"], out int value1))
            {
                _pageNumber = value1;
            }
            int? _pageSize = null;
            if (int.TryParse(this.Request.Params["PageSize"], out int value2))
            {
                _pageSize = value2;
            }

            command.SetSolicitedBy(User.Identity.Name);
            var response = await _mediator.Send(command);
            if (response.success)
            {
                TempData["success"] = "El role fue editado exitosamente.";
                return RedirectToAction("Index",
                    new
                    {
                        pageNumber = _pageNumber,
                        pageSize = _pageSize,
                    });
            }
            else
            {
                TempData["errors"] = response.errors;
            }
            var model = new FormViewModel(
              command: command,
              pageNumber: _pageNumber,
              pageSize: _pageSize);
            return View("Form", model);
        }
    }
}