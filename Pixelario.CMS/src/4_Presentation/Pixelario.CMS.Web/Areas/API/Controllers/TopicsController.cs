﻿using MediatR;
using Pixelario.CMS.Web.Areas.API.Model.Topics;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;

namespace Pixelario.CMS.Web.Areas.API.Controllers
{
    public class TopicsController : ApiController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly int PAGESIZE = 6;
        public TopicsController(ILogger logger, IMediator mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet]
        public async Task<IHttpActionResult> Index(int id, int pageNumber)
        {
            _logger.Information("Request started {0} {1}",
                Request.Method.Method,
                Request.RequestUri.AbsoluteUri);
            var topic = await _mediator.Send(new TopicCommands.GetCommand(
                    iDTopic: id));
            if (topic == null || !topic.Enabled)
            {
                _logger.Warning("Topic with id {0} not found or not enable", id);
                return NotFound();
            }
            if (pageNumber == 0)
                pageNumber = 1;
            var pageCount = 1;
            var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
            var totalRows = await _mediator.Send(new ArticleCommands.CountCommand(
                    iDEdition: null,
                    iDTopic: id,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments
                    ));
            if (totalRows > 0)
                pageCount = ((totalRows - 1) / PAGESIZE) + 1;
            var nextPageNumber = pageNumber + 1;
            if (nextPageNumber > pageCount)
                nextPageNumber = -1;
            var rowIndex = PAGESIZE * (pageNumber - 1);
            var articles = await _mediator.Send(new ArticleCommands.ListCommand(
                    iDEdition: null,
                    iDTopic: id,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments,
                    rowIndex: rowIndex,
                    rowCount: PAGESIZE,
                    columnOrder: "a.PublicationDate",
                    orderDirection: "DESC"
                    ));
            var model = new IndexAPIModel(
                nextPageNumber: nextPageNumber,
                topicQueryModel: topic,
                articleQueryModels: articles); ;
            return Ok(model);
        }
    }

}
