﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Web.Areas.API.Model.Contents;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
namespace Pixelario.CMS.Web.Areas.API.Controllers
{
    public class ContentsController : ApiController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly int PAGESIZE = 6;

        public ContentsController(
            ILogger logger, IMediator mediator
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet]
        public async Task<IHttpActionResult> Index(int id, int pageNumber)
        {
            _logger.Information("Request started {0} {1}",
                Request.Method.Method,
                Request.RequestUri.AbsoluteUri);
            var edition = await _mediator.Send(new EditionCommands.GetCommand(
                    iDEdition: id));
            if (edition == null || !edition.Enabled ||
                edition.EditionType != EditionType.Especial_Content)
            {
                _logger.Warning("Edition with id {0} not found", id);
                throw new HttpException(404, "Edition not found");
            }
            if (pageNumber == 0)
                pageNumber = 1;
            var pageCount = 1;
            var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
            var totalRows = await _mediator.Send(new ArticleCommands.CountCommand(
                iDEdition: id,
                iDTopic: null,
                iDSubTopic: null,
                enabled: true,
                atHome: null,
                placesAtHome: null,
                filteredBy: microFilterStatments
                ));
            if (totalRows > 0)
                pageCount = ((totalRows - 1) / PAGESIZE) + 1;
            var nextPageNumber = pageNumber + 1;
            if (nextPageNumber > pageCount)
                nextPageNumber = -1;
            var rowIndex = PAGESIZE * (pageNumber - 1);
            var articleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                iDEdition: id,
                iDTopic: null,
                iDSubTopic: null,
                enabled: true,
                atHome: null,
                placesAtHome: null,
                filteredBy: microFilterStatments,
                rowIndex: rowIndex,
                rowCount: PAGESIZE,
                columnOrder: "a.PublicationDate",
                orderDirection: "DESC"
                ));
            var model = new IndexAPIModel(
                nextPageNumber: nextPageNumber,
                editionQueryModel: edition,
                articleQueryModels: articleQueryModels);
            return Ok(model);
        }
    }
}