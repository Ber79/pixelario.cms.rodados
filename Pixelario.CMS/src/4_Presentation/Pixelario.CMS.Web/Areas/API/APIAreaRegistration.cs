﻿using System.Web.Http;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.API
{
    public class APIAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "API";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            GlobalConfiguration.Configuration.MapHttpAttributeRoutes();

            context.Routes.MapHttpRoute(
                name:"API_default",
                routeTemplate: "API/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}