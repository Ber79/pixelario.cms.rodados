﻿using Newtonsoft.Json;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.API.Model.Contents
{
    [JsonObject(MemberSerialization.OptIn)]
    public class IndexAPIModel
    {
        [JsonProperty]
        public List<ArticleAPIModel> Articles { get; private set; }
        [JsonProperty]
        public string NextURL { get; set; }
        public IndexAPIModel(int nextPageNumber,
            EditionQueryModel editionQueryModel,
            List<ArticleQueryModel> articleQueryModels)
        {
            this.Articles = new List<ArticleAPIModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleAPIModel(
                    queryModel: articleQueryModel));
            }
            if (nextPageNumber >= 0)
            {
                this.NextURL = string.Format("/API/Contents/Index/{0}?pageNumber={1}",
                    editionQueryModel.IDEdition,
                    nextPageNumber);
            }
            else
            {
                this.NextURL = "";
            }    
        }
    }
}