﻿using Newtonsoft.Json;
using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.API.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ArticleAPIModel
    {
        [JsonProperty]
        public string Title { get; private set; }
        [JsonProperty]
        public string URL { get; private set; }
        [JsonProperty]
        public string HomeImage { get; private set; }
        [JsonProperty]
        public string CssClass { get; private set; }
        [JsonProperty]
        public List<TopicAPIModel> Topics { get; private set; }
        public ArticleAPIModel(ArticleQueryModel queryModel)
        {
            this.Title = queryModel.Title;
            this.URL = queryModel.URL;
            this.HomeImage = !string.IsNullOrEmpty(queryModel.HomeImage) ? 
                queryModel.HomeImage.Replace("/80/", "/500/") : "";
            this.CssClass = queryModel.Title.Length >= 40 ? "e-article-long-title" : "";
            var totalTopics = 0;
            this.Topics = new List<TopicAPIModel>();
            foreach(var topic in queryModel.Topics)
            {
                if(totalTopics < 3)
                {
                    this.Topics.Add(new TopicAPIModel(
                        queryModel: topic));
                    totalTopics++;
                }
            }
            foreach (var subTopic in queryModel.SubTopics)
            {
                if (totalTopics < 3)
                {
                    this.Topics.Add(new TopicAPIModel(
                        queryModel: subTopic));
                    totalTopics++;
                }
            }
        }
    }
}