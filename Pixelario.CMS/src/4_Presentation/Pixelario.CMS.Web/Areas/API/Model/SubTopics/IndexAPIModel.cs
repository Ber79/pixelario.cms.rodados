﻿using Newtonsoft.Json;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Areas.API.Model.SubTopics
{
    [JsonObject(MemberSerialization.OptIn)]
    public class IndexAPIModel
    {
        [JsonProperty]
        public TopicAPIModel Topic { get; set; }
        [JsonProperty]
        public List<ArticleAPIModel> Articles { get; private set; }
        [JsonProperty]
        public string NextURL { get; set; }
        public IndexAPIModel(int nextPageNumber,
            SubTopicQueryModel subTopicQueryModel,
            List<ArticleQueryModel> articleQueryModels)
        {
            this.Topic = new TopicAPIModel(
                queryModel: subTopicQueryModel);
            this.Articles = new List<ArticleAPIModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleAPIModel(
                    queryModel: articleQueryModel));
            }
            if (nextPageNumber >= 0)
            {
                this.NextURL = string.Format("/API/SubTopics/Index/{0}?pageNumber={1}",
                    subTopicQueryModel.IDSubTopic,
                    nextPageNumber);
            }
            else
            {
                this.NextURL = "";
            }
        }
    }
}