﻿using Newtonsoft.Json;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.API.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class TopicAPIModel
    {
        [JsonProperty]
        public string Title { get; private set; }
        [JsonProperty]
        public string URL { get; private set; }
        public TopicAPIModel(TopicQueryModel queryModel)
        {
            this.Title = queryModel.Title;
            this.URL = queryModel.URL;
        }
        public TopicAPIModel(SubTopicQueryModel queryModel)
        {
            this.Title = queryModel.Title;
            this.URL = queryModel.URL;
        }
    }
}