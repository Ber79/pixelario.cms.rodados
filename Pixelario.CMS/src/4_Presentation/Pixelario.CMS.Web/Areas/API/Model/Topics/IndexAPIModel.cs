﻿using Newtonsoft.Json;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Areas.API.Model.Topics
{
    [JsonObject(MemberSerialization.OptIn)]
    public class IndexAPIModel
    {
        [JsonProperty]
        public TopicAPIModel Topic { get; set; }
        [JsonProperty]
        public List<ArticleAPIModel> Articles { get; private set; }
        [JsonProperty]
        public string NextURL { get; set; }
        public IndexAPIModel(int nextPageNumber,
            TopicQueryModel topicQueryModel,
            List<ArticleQueryModel> articleQueryModels)
        {
            this.Topic = new TopicAPIModel(
                queryModel: topicQueryModel);
            this.Articles = new List<ArticleAPIModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleAPIModel(
                    queryModel: articleQueryModel));
            }
            if (nextPageNumber >= 0)
            {
                this.NextURL = string.Format("/API/Topics/Index/{0}?pageNumber={1}",
                    topicQueryModel.IDTopic,
                    nextPageNumber);
            }
            else
            {
                this.NextURL = "";
            }    
        }
    }
}