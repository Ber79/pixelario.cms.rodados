﻿using System.Web.Mvc;

namespace Pixelario.CMS.Web.Areas.Preview
{
    public class PreviewAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Preview";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Preview_default",
                "Preview/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "Pixelario.CMS.Web.Preview.Controllers" }
            );
        }
    }
}