﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Controllers;
using Pixelario.CMS.Web.Models.Articles;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.Subscriptions.Application.Queries.Accounts;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using UserCommands = Pixelario.CMS.Application.Commands.Users;
namespace Pixelario.CMS.Web.Preview.Controllers
{
    [Authorize(Roles = "Super Administrador,Administrador")]
    public class ArticlesController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IConstraintsQueries _constraintsQueries;
        private readonly IAccountsQueries _accountQueries;

        private static string CONSTRAINT_REDIRECT_URL = "subscriptions/plans/redirecturl";
        public ArticlesController(
            ILogger logger, IMediator mediator,
            IConstraintsQueries constraintsQueries,
            IAccountsQueries accountQueries) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _constraintsQueries = constraintsQueries ?? throw new ArgumentNullException(nameof(constraintsQueries));
            _accountQueries = accountQueries ?? throw new ArgumentNullException(nameof(accountQueries));
        }


        public async Task<ActionResult> Index(int id)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var article = await _mediator.Send(new ArticleCommands.GetCommand(
                    iDArticle: id));
            if (article != null)
            {
                if (this.SubscriptionsEnable)
                {
                    var entities = new Dictionary<int, int>();
                    entities.Add(ConstraintType.OnEdition.Id, article.Edition.IDEdition);
                    entities.Add(ConstraintType.OnArticle.Id, article.IDArticle);
                    var constraints = await _constraintsQueries.ListAsync(
                        plan: null,
                        entities: entities,
                        rowIndex: 0,
                        rowCount: int.MaxValue,
                        columnOrder: "c.IDPlan",
                        orderDirection: "ASC");
                    if (constraints != null && constraints.Count > 0)
                    {
                        if (!this.User.Identity.IsAuthenticated)
                        {
                            string redirectUrl = await getConstraintRedirectUrl();
                            return Redirect(redirectUrl);
                        }
                        if (!this.User.IsInRole("Administrador") &&
                            !this.User.IsInRole("Super Administrador"))
                        {
                            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                                userName: this.User.Identity.Name));
                            if (user == null)
                            {
                                string redirectUrl = await getConstraintRedirectUrl();
                                return Redirect(redirectUrl);
                            }
                            var plans = constraints.Select(c => c.Plan);
                            var account = await _accountQueries.GetByUserIdAsync(
                                user.Id);
                            if (!plans.Contains(account.Plan))
                            {
                                string redirectUrl = await getConstraintRedirectUrl();
                                return Redirect(redirectUrl);
                            }
                        }
                    }
                }
                var configurationsList = await GetCommonFrontConfigurations();
                #region Flyers items
                var asideFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                var asideTopFlyers = await GetFlyer(
                   placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                var footerLeftFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                var footerRightFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                #endregion
                var articleMultimediaQueryModels = await _mediator.Send(new ArticleCommands.ListMultimediaCommand(
                    iDArticle: article.IDArticle,
                    multimediaType: null,
                    enabled: true,
                    atHome: null,
                    filteredBy: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "m.IDMultimediaType",
                    orderDirection: "ASC"));
                string articleKeywords = $"excludeArticle={article.IDArticle};publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
                foreach (var keyword in article.Keywords.Split(';'))
                {
                    if (!string.IsNullOrEmpty(keyword))
                        articleKeywords += $"keyword={keyword.Trim()};";
                }

                #region Header items           
                var headerViewModel = await GetHeaderViewModel();
                #endregion

                //TODO: Enhance the code
                var relatedArticleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                    iDEdition: article.Edition.IDEdition,
                    iDTopic: null,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: articleKeywords,
                    rowIndex: 0,
                    rowCount: 3,
                    columnOrder: "a.PublicationDate",
                    orderDirection: "DESC"
                    ));

                var model = new IndexViewModel()
                {
                    // TODO: Hacer un controlo del indice del menú
                    MenuIndex = 2,
                    ConfigurationsList = configurationsList,
                    HeaderViewModel = headerViewModel,
                    AsideFlyers = asideFlyers,
                    AsideTopFlyers = asideTopFlyers,
                    FooterLeftFlyers = footerLeftFlyers,
                    FooterRightFlyers = footerRightFlyers,
                    Article = new ArticleViewModel(
                        mappingMode: MappingMode.Complete,
                        articleQueryModel: article,
                        multimediaQueryModels: articleMultimediaQueryModels,
                        relatedArticleQueryModels: relatedArticleQueryModels,
                        headerArticleScriptCodes: null,
                        bottomArticleScriptCodes: null,
                        scriptOnArticleBottomCodes: null)
                };
                return View(model);
            }
            else
            {
                _logger.Warning("Article with id {0} not found", id);
                throw new HttpException(404, "Article not found");
            }
        }

        private async Task<string> getConstraintRedirectUrl()
        {
            var redirectUrlState = await _mediator.Send(new ConfigurationCommands.GetStateCommand(
                key: CONSTRAINT_REDIRECT_URL));
            var redirectUrl = "/";
            if (!string.IsNullOrEmpty(redirectUrlState))
                redirectUrl = redirectUrlState;
            return redirectUrl;
        }

        [Route("Widget/{iDArticleMultimedia}")]
        public async Task<ActionResult> Widget(int iDArticleMultimedia)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var widget = await _mediator.Send(new ArticleCommands.GetMultimediaCommand(
                iDMultimedia: iDArticleMultimedia));
            if (widget != null && widget.Enabled &&
                widget.IDMultimediaType == MultimediaType.Widget.Id)
            {
                var model = new WidgetMultimediaViewModel(
                    multimediaQueryModel: widget);
                return View(model);
            }
            else
            {
                _logger.Warning("Article multimedia with id {0} not found", iDArticleMultimedia);
                throw new HttpException(404, "Article multimedia not found");
            }
        }
    }
}