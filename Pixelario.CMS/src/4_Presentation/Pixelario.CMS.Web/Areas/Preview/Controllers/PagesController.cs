﻿using MediatR;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Web.Controllers;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.CMS.Web.Models.Pages;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PageCommands = Pixelario.CMS.Application.Commands.Pages;

namespace Pixelario.CMS.Web.Preview.Controllers
{
    [Authorize(Roles = "Super Administrador,Administrador")]
    public class PagesController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;

        public PagesController(
            ILogger logger, IMediator mediator,
            IArticleMultimediasQueries articleMultimediasQueries) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        // GET: Page
        public async Task<ActionResult> Index(int id)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var page = await _mediator.Send(new PageCommands.GetCommand(
                    iDPage: id));
            if (page != null)
            {

                var configurationsList = await GetCommonFrontConfigurations();
                #region Flyers items
                var asideFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                var asideTopFlyers = await GetFlyer(
                   placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                var footerLeftFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                var footerRightFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                #endregion

                var pageMultimediaQueryModels = await _mediator.Send(new PageCommands.ListMultimediaCommand(
                    iDPage: page.IDPage,
                    multimediaType: null,
                    enabled: true,
                    atHome: null,
                    filteredBy: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "pm.IDMultimediaType",
                    orderDirection: "ASC"
                    ));
                #region Header items           
                var headerViewModel = await GetHeaderViewModel();
                #endregion
                var model = new IndexViewModel()
                {
                    // TODO: Hacer un controlo del indice del menú
                    MenuIndex = 2,
                    ConfigurationsList = configurationsList,
                    HeaderViewModel = headerViewModel,
                    AsideFlyers = asideFlyers,
                    AsideTopFlyers = asideTopFlyers,
                    FooterRightFlyers = footerRightFlyers,
                    FooterLeftFlyers = footerLeftFlyers,
                    Page = new PageViewModel(page, pageMultimediaQueryModels)
                };
                return View(model);
            }
            else
            {
                _logger.Warning("Page with id {0} not found", id);
                throw new HttpException(404, "Page not found");
            }
        }

        [Route("Widget/{iDPageMultimedia}")]
        public async Task<ActionResult> Widget(int iDPageMultimedia)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var widget = await _mediator.Send(new PageCommands.GetMultimediaCommand(
                iDMultimedia: iDPageMultimedia));
            if (widget != null && widget.Enabled &&
                widget.IDMultimediaType == MultimediaType.Widget.Id)
            {
                var model = new WidgetMultimediaViewModel(
                    multimediaQueryModel: widget);
                return View(model);
            }
            else
            {
                _logger.Warning("Page multimedia with id {0} not found", iDPageMultimedia);
                throw new HttpException(404, "Page multimedia not found");
            }
        }
    }
}