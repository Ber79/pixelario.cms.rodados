﻿using Pixelario.CMS.Web.Infrastructure.GlobalFilters;
using System.Web.Mvc;

namespace Pixelario.CMS.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LogErrorsAtribute());
        }
    }
}
