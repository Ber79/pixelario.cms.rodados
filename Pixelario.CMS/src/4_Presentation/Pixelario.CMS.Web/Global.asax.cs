﻿using Pixelario.CMS.Infrastructure;
using Pixelario.CMS.Web.Infrastructure.AutofacModules;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Pixelario.CMS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            IoCConfigurator.ConfigureDependencyInjection();
            Database.SetInitializer<CMSContext>(
                new CreateDatabaseIfNotExists<CMSContext>()
                );
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
