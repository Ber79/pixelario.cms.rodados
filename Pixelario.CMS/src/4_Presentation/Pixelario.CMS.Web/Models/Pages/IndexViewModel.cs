﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;


namespace Pixelario.CMS.Web.Models.Pages
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public PageViewModel Page { get; set; }
        public List<EditionViewModel> InformativeEditions { get;  set; }
    }
}