﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace Pixelario.CMS.Web.Models
{
    public class FrontViewModel
    {
        public string AppName
        {
            get
            {
                return ConfigurationManager.AppSettings["app:name"];
            }
        }
        public string AppDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["app:domain"];
            }
        }
        public bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }
        public bool IsDebug { get; set; }
        public List<ConfigurationQueryModel> ConfigurationsList { get; set; }
        public string GetConfiguration(string key)
        {
            if (ConfigurationsList != null)
            {
                var configuration = ConfigurationsList.FirstOrDefault(c => c.Key == key);
                return configuration != null ? configuration.State : "";
            }
            else
                return "";
        }

        public HeaderViewModel HeaderViewModel { get; set; }


        public List<AdvertismentFrontModel> AsideFlyers { get; set; }
        public List<AdvertismentFrontModel> AsideTopFlyers { get; set; }
        public List<AdvertismentFrontModel> FooterRightFlyers { get; set; }
        public List<AdvertismentFrontModel> FooterLeftFlyers { get; set; }
        public List<AdvertismentFrontModel> ScriptFlyers { get; set; }

        public string SearchQuery { get; set; }


        public FrontViewModel()
        {
            this.IsDebug = SetIsDebug();
        }
        public FrontViewModel(List<ConfigurationQueryModel> configurationsList,
            HeaderViewModel headerViewModel,
            List<AdvertismentFrontModel> asideFlyers,
            List<AdvertismentFrontModel> asideTopFlyers,
            List<AdvertismentFrontModel> footerLeftFlyers,
            List<AdvertismentFrontModel> footerRightFlyers,
            List<AdvertismentFrontModel> scritpFlyers) : this()
        {
            this.ConfigurationsList = configurationsList;
            this.HeaderViewModel = headerViewModel;
            this.AsideFlyers = asideFlyers;
            this.AsideTopFlyers = asideTopFlyers;
            this.FooterLeftFlyers = footerLeftFlyers;
            this.FooterRightFlyers = footerRightFlyers;
            this.ScriptFlyers = scritpFlyers;

        }

        /// <summary>
        /// Set the IsDebug property if is in mode Debug
        /// </summary>
        /// <returns></returns>
        private bool SetIsDebug()
        {
#if DEBUG
            return true;
#else
            return false;
#endif

        }
    }
}