﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Models.Articles
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public ArticleViewModel Article { get; set; }
        public List<AdvertismentFrontModel> BottomFlyers { get; set; }
        public IndexViewModel()
        {

        }
        public IndexViewModel(
            ArticleQueryModel article,
            List<ArticleMultimediaQueryModel> articleMultimediaQueryModels,
            List<ArticleQueryModel> relatedArticleQueryModels,
            List<AdvertismentFrontModel> bottomFlyers,
            List<ArticleScriptItemQueryModel> scriptOnHeader,
            List<ArticleScriptItemQueryModel> scriptOnBottom,
            List<ArticleScriptItemQueryModel> scriptOnArticleBottom,

            List<ConfigurationQueryModel> configurationsList,
            HeaderViewModel headerViewModel,
            List<AdvertismentFrontModel> asideFlyers,
            List<AdvertismentFrontModel> asideTopFlyers,
            List<AdvertismentFrontModel> footerLeftFlyers,
            List<AdvertismentFrontModel> footerRightFlyers,
            List<AdvertismentFrontModel> scritpFlyers
            ) :base(
                configurationsList, headerViewModel, asideFlyers,
                asideTopFlyers,
                footerLeftFlyers, footerRightFlyers, scritpFlyers
                )
        {                       
            this.BottomFlyers = bottomFlyers;
            this.Article = new ArticleViewModel(
                mappingMode: MappingMode.Complete,
                articleQueryModel: article,
                multimediaQueryModels: articleMultimediaQueryModels,
                relatedArticleQueryModels: relatedArticleQueryModels,
                headerArticleScriptCodes: scriptOnHeader.Select(i => i.Code).ToList(),
                bottomArticleScriptCodes: scriptOnBottom.Select(i => i.Code).ToList(),
                scriptOnArticleBottomCodes: scriptOnArticleBottom.Select(i => i.Code).ToList()
                );
        }
    }
}