﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.SubTopics
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public SubTopicViewModel SubTopic { get; set; }
        public int PageCount { get; set; }
        public TopicViewModel Topic { get; set; }
        public IList<ArticleViewModel> Articles { get; set; }
        public IndexViewModel(
            SubTopicQueryModel subTopicQueryModel,
            TopicQueryModel topicQueryModel,
            List<SubTopicQueryModel> subTopicQueryModels,
            List<ArticleQueryModel> articleQueryModels,
            int pageCount,
            List<ConfigurationQueryModel> configurationsList,
            HeaderViewModel headerViewModel,
            List<AdvertismentFrontModel> asideFlyers,
            List<AdvertismentFrontModel> asideTopFlyers,
            List<AdvertismentFrontModel> footerLeftFlyers,
            List<AdvertismentFrontModel> footerRightFlyers,
            List<AdvertismentFrontModel> scritpFlyers
        ) : base(
                configurationsList, headerViewModel, asideFlyers,
                asideTopFlyers,
                footerLeftFlyers, footerRightFlyers, scritpFlyers
                )
        {
            this.SubTopic = new SubTopicViewModel(
                mappingMode: MappingMode.Summary,
                queryModel: subTopicQueryModel
                );
            this.Topic = new TopicViewModel(
                queryModel: topicQueryModel,
                subTopicQueryModels: subTopicQueryModels);
            if (!subTopicQueryModel.AtHome)
            {
                this.Topic.SubTopics.Insert(
                    index: 0,
                    item: new SubTopicViewModel(
                        mappingMode: MappingMode.Atomic,
                        queryModel: subTopicQueryModel));
            }
            this.Articles = new List<ArticleViewModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleViewModel(
                    mappingMode: MappingMode.Atomic,
                    articleQueryModel: articleQueryModel,
                    urlRef: "subtopic"));
            }
            this.PageCount = pageCount;
        }
    }
}