﻿using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Web.Models.Frontend;

namespace Pixelario.CMS.Web.Models.SubTopics
{
    public class SubTopicViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string URL { get; private set; }
        public string HomeImage { get; private set; }
        public string Summary { get; private set; }
        public string Keywords { get; private set; }

        public SubTopicViewModel(
            MappingMode mappingMode,
            SubTopicQueryModel queryModel)
        {
            this.Title = queryModel.Title;
            this.URL = queryModel.URL + "?ref=subtopic";
            if (mappingMode == MappingMode.Summary)
            {
                this.ID = queryModel.IDSubTopic;
                this.HomeImage = queryModel.HomeImage;
                this.Keywords = queryModel.Keywords;
            }
        }

    }
}