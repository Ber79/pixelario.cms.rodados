﻿using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.SubTopics
{
    public class TopicViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string URL { get; private set; }
        public string Summary { get; private set; }
        public string Keywords { get; private set; }
        public string HomeImage { get; private set; }
        public IList<SubTopicViewModel> SubTopics { get; set; }
        public TopicViewModel(TopicQueryModel queryModel,
            List<SubTopicQueryModel> subTopicQueryModels)
        {
            this.ID = queryModel.IDTopic;
            this.Title = queryModel.Title;
            this.URL = queryModel.URL + "?ref=subtopic";
            this.Summary = queryModel.Summary;
            this.Keywords = queryModel.Keywords;
            this.HomeImage = queryModel.HomeImage;
            this.SubTopics = new List<SubTopicViewModel>();
            foreach(var subTopicQueryModel in subTopicQueryModels)
            {
                this.SubTopics.Add(new SubTopicViewModel(
                    mappingMode: MappingMode.Atomic,
                    queryModel: subTopicQueryModel));
            }
        }

    }
}