﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Topics
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public TopicViewModel Topic { get; set; }
        public int PageCount { get; set; }
        public IList<ArticleViewModel> Articles { get; set; }
        public IndexViewModel(
            TopicQueryModel topicQueryModel,
            List<SubTopicQueryModel> subTopicQueryModels,
            List<ArticleQueryModel> articleQueryModels,
            int pageCount,
            List<ConfigurationQueryModel> configurationsList,
            HeaderViewModel headerViewModel,
            List<AdvertismentFrontModel> asideFlyers,
            List<AdvertismentFrontModel> asideTopFlyers,
            List<AdvertismentFrontModel> footerLeftFlyers,
            List<AdvertismentFrontModel> footerRightFlyers,
            List<AdvertismentFrontModel> scritpFlyers
        ) : base(
                configurationsList, headerViewModel, asideFlyers,
                asideTopFlyers,
                footerLeftFlyers, footerRightFlyers, scritpFlyers
                )
        {
            this.Topic = new TopicViewModel(
                queryModel: topicQueryModel,
                subTopicQueryModels: subTopicQueryModels);
            this.Articles = new List<ArticleViewModel>();
            foreach(var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleViewModel(
                    mappingMode: MappingMode.Atomic,
                    articleQueryModel: articleQueryModel,
                    urlRef: "topic"));
            }
            this.PageCount = pageCount;
        }
    }
}