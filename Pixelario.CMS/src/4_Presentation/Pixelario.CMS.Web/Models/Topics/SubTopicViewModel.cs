﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Topics
{
    public class SubTopicViewModel
    {
        public string Title { get; private set; }
        public string URL { get; private set; }
        public SubTopicViewModel(SubTopicQueryModel queryModel)
        {
            this.Title = queryModel.Title;
            this.URL = queryModel.URL + "?ref=topic";
        }

    }
}