﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Subscriptions
{
    public class CreateViewModel : FrontViewModel
    {

        public int MenuIndex { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int PlanID { get; set; }
        public List<Plan> Plans { get; set; }
        public bool ShowTechnicalErrors { get; set; }
    }
}