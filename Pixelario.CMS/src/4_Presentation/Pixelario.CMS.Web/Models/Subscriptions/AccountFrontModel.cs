﻿using Pixelario.Identity.Domain;
using Pixelario.Subscriptions.Application.QueryModels.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Subscriptions
{
    public class AccountFrontModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PlanTitle { get; set; }
        private AccountFrontModel()
        {

        }
        public AccountFrontModel(ApplicationUser applicationUser,
            AccountQueryModel accountQueryModel)
        {
            this.Email = applicationUser.Email;
            this.FirstName = applicationUser.FirstName;
            this.LastName = applicationUser.LastName;
            this.PlanTitle = accountQueryModel.Plan.Title;
        }
    }
}