﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Subscriptions
{
    public class SignInFrontModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string RememberMe { get; set; }
        public bool ShowTechnicalErrors { get; set; }
    }
}