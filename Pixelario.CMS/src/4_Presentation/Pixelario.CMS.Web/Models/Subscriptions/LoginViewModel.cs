﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Subscriptions
{
    public class LoginViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public string ReturnUrl { get; set; }
        public bool ShowTechnicalErrors { get; set; }
    }
}