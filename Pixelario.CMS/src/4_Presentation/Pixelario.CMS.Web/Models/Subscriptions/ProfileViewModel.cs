﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Subscriptions
{
    public class ProfileViewModel : FrontViewModel
    {

        public int MenuIndex { get; set; }
        public string Title { get; set; }
        public AccountFrontModel Account { get; set; }
    }
}