﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class DocumentMultimediaViewModel
    {
        public string Title { get; private set; }
        public string HRef { get; private set; }
        public string Extension { get; private set; }
        public DocumentMultimediaViewModel(PageMultimediaQueryModel multimediaQueryModel)
        {
            this.Title = multimediaQueryModel.Title;
            this.HRef = multimediaQueryModel.Path1;
            this.Extension = multimediaQueryModel.Path2;
        }
        public DocumentMultimediaViewModel(ArticleMultimediaQueryModel multimediaQueryModel)
        {
            this.Title = multimediaQueryModel.Title;
            this.HRef = multimediaQueryModel.Path1;
            this.Extension = multimediaQueryModel.Path2;
        }
        public string CssClass
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Extension) && this.Extension.Length > 3)
                    return string.Format("{0}-file",this.Extension.Substring(1));
                else
                    return null;
            }
            
        }
    }
}