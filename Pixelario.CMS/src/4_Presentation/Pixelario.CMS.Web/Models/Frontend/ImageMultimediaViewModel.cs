﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class ImageMultimediaViewModel
    {
        public string Title { get; private set; }
        public string SRC { get; private set; }
        public ImageMultimediaViewModel(PageMultimediaQueryModel multimediaQueryModel)
        {
            this.Title = multimediaQueryModel.Title;
            this.SRC = multimediaQueryModel.Path1;
        }
        public ImageMultimediaViewModel(ArticleMultimediaQueryModel multimediaQueryModel)
        {
            this.Title = multimediaQueryModel.Title;
            this.SRC = multimediaQueryModel.Path1;
        }
        public string GetSRC(string width)
        {
            if (string.IsNullOrEmpty(this.SRC)) return null;
            return this.SRC.Replace("/80/",
                string.Format("/{0}/", width));
        }
    }
}