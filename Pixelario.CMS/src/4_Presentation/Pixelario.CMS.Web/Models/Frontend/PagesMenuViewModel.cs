﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class PagesMenuViewModel
    {
        public List<EditionViewModel> Editions { get; set; }
        private PagesMenuViewModel()
        {

        }
        public PagesMenuViewModel(List<EditionQueryModel> editionsQueryModels)
        {
            this.Editions = new List<EditionViewModel>();
            foreach (var editionQueryModel in editionsQueryModels)
            {
                this.Editions.Add(new EditionViewModel(mappingMode: MappingMode.Atomic,
                    editionsQueryModel: editionQueryModel));
            }           
        }
    }
}