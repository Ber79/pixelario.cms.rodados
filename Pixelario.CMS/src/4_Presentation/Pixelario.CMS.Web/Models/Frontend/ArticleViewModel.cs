﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class ArticleViewModel
    {
        public int ID { get; private set; }
        public string PublicationDate { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public string Body { get; private set; }
        public string Source { get; private set; }
        public string HomeImage { get; private set; }
        public string SlideImage { get; private set; }
        public string VerticalImage { get; private set; }
        public string HorizontalImage { get; private set; }

        public string URL { get; private set; }
        public string Keywords { get; private set; }
        public ArticlePlaceAtHome PlaceAtHome { get; set; }
        public IList<SubTopicViewModel> SubTopics { get; set; }
        public IList<TopicViewModel> Topics { get; set; }
        public IList<ImageMultimediaViewModel> ImageMultimedia { get; set; }
        public IList<DocumentMultimediaViewModel> Documents { get; set; }
        public IList<WidgetMultimediaViewModel> Widgets { get; set; }
        public IList<ArticleViewModel> RelatedArticles { get; set; }

        public IList<string> HeaderScriptCodes { get; set; }
        public IList<string> BottomScriptCodes { get; set; }
        public IList<string> ArticleBottomScriptCodes { get; set; }

        /// <summary>
        /// Base ArticlViewModel Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="summary"></param>
        /// <param name="homeImage"></param>
        public ArticleViewModel(int id, string title, 
            string summary, string homeImage)
        {
            this.ID = id;
            this.Title = title;
            this.Summary = summary;
            this.HomeImage = homeImage;
            
        }

        public ArticleViewModel(
            EditionType editionType,
            ArticleQueryModel articleQueryModel)
        {
            this.ID = articleQueryModel.IDArticle;
            this.Title = articleQueryModel.Title;
            this.Summary = articleQueryModel.Summary;
            if (editionType == EditionType.News||
                editionType == EditionType.Information ||
                editionType == EditionType.Especial_Content)
            {
                this.HomeImage = articleQueryModel.HomeImage;
                this.HorizontalImage = articleQueryModel.HorizontalImage;
                this.URL = articleQueryModel.URL;
                if (articleQueryModel.Topics != null && articleQueryModel.Topics.Count> 0)
                {
                    this.Topics = new List<TopicViewModel>();
                    foreach(var topicQueryModel in articleQueryModel.Topics)
                    {
                        this.Topics.Add(new TopicViewModel(
                            mappingMode: MappingMode.Atomic,
                            topicQueryModel: topicQueryModel));
                    }
                }
                if (articleQueryModel.SubTopics != null && articleQueryModel.SubTopics.Count > 0)
                {
                    this.SubTopics = new List<SubTopicViewModel>();
                    foreach (var subTopicQueryModel in articleQueryModel.SubTopics)
                    {
                        this.SubTopics.Add(new SubTopicViewModel(
                            mappingMode: MappingMode.Atomic,
                            subTopicQueryModel: subTopicQueryModel));
                    }
                }
            }
            else if (editionType == EditionType.Flyer)
            {
                this.PlaceAtHome = articleQueryModel.PlaceAtHome;
                var totalMultimedia = articleQueryModel.Multimedia.Count;
                if (totalMultimedia == 0)
                {
                    this.HomeImage = "";
                    this.URL = "";
                }
                else
                {
                    var index = 0;
                    if (totalMultimedia > 1)
                    {
                        var random = new Random();
                        index = random.Next(0, totalMultimedia - 1);
                    }
                    this.HomeImage = articleQueryModel.Multimedia[index].Path1;
                    this.URL = articleQueryModel.Multimedia[index].Path2;
                }
            }
        }


        public ArticleViewModel(MappingMode mappingMode,
            ArticleQueryModel articleQueryModel)
        {
            this.ID = articleQueryModel.IDArticle;
            this.Title = articleQueryModel.Title;
            this.Summary = articleQueryModel.Summary;
            this.URL = articleQueryModel.URL;
            this.HomeImage = articleQueryModel.HomeImage;
            this.VerticalImage = articleQueryModel.VerticalImage;
            this.HorizontalImage = articleQueryModel.HorizontalImage;
            this.SlideImage = articleQueryModel.SlideImage;
            this.Topics = new List<TopicViewModel>();
            foreach(var topicQueryModel in articleQueryModel.Topics)
            {
                Topics.Add(new TopicViewModel(
                    mappingMode: MappingMode.Atomic,
                    topicQueryModel: topicQueryModel));
            }
            this.SubTopics = new List<SubTopicViewModel>();
            foreach (var subTopicQueryModel in articleQueryModel.SubTopics)
            {
                SubTopics.Add(new SubTopicViewModel(
                    mappingMode: MappingMode.Atomic,
                    subTopicQueryModel: subTopicQueryModel));
            }
            if(mappingMode == MappingMode.Atomic)
            {
                this.PublicationDate = articleQueryModel.PublicationDate.HasValue ?
                    articleQueryModel.PublicationDate.Value.ToString(@"dd\/MM\/yyyy") : "";
            }
            if (mappingMode == MappingMode.Complete)
            {
                this.PublicationDate = articleQueryModel.PublicationDate.HasValue ? 
                    articleQueryModel.PublicationDate.Value.ToString(@"dd \d\e MMMM \d\e yyyy") : "";
                this.Body = articleQueryModel.Body;
                this.Source = articleQueryModel.Source;
                this.Keywords = articleQueryModel.Keywords;
            }
        }
        public ArticleViewModel(MappingMode mappingMode,
            ArticleQueryModel articleQueryModel,
            string urlRef)
        {
            this.ID = articleQueryModel.IDArticle;
            this.Title = articleQueryModel.Title;
            this.Summary = articleQueryModel.Summary;
            this.URL = articleQueryModel.URL + "?ref=" +urlRef;
            this.HomeImage = articleQueryModel.HomeImage;
            this.VerticalImage = articleQueryModel.VerticalImage;
            this.HorizontalImage = articleQueryModel.HorizontalImage;
            this.SlideImage = articleQueryModel.SlideImage;
            this.Topics = new List<TopicViewModel>();
            foreach (var topicQueryModel in articleQueryModel.Topics)
            {
                Topics.Add(new TopicViewModel(
                    mappingMode: MappingMode.Atomic,
                    topicQueryModel: topicQueryModel,
                    urlRef: urlRef));
            }
            this.SubTopics = new List<SubTopicViewModel>();
            foreach (var subTopicQueryModel in articleQueryModel.SubTopics)
            {
                SubTopics.Add(new SubTopicViewModel(
                    mappingMode: MappingMode.Atomic,
                    subTopicQueryModel: subTopicQueryModel,
                    urlRef: urlRef));
            }
            if (mappingMode == MappingMode.Atomic)
            {
                this.PublicationDate = articleQueryModel.PublicationDate.HasValue ?
                    articleQueryModel.PublicationDate.Value.ToString(@"dd\/MM\/yyyy") : "";
            }
            if (mappingMode == MappingMode.Complete)
            {
                this.PublicationDate = articleQueryModel.PublicationDate.HasValue ?
                    articleQueryModel.PublicationDate.Value.ToString(@"dd \d\e MMMM \d\e yyyy") : "";
                this.Body = articleQueryModel.Body;
                this.Source = articleQueryModel.Source;
                this.Keywords = articleQueryModel.Keywords;
            }
        }

        public ArticleViewModel(MappingMode mappingMode,
            ArticleQueryModel articleQueryModel, 
            List<ArticleMultimediaQueryModel> multimediaQueryModels) :
        this(mappingMode: mappingMode, articleQueryModel: articleQueryModel)
        {
            if (mappingMode == MappingMode.Complete)
            {
                this.ImageMultimedia = new List<ImageMultimediaViewModel>();
                foreach (var multimediaQueryModel in multimediaQueryModels
                    .Where(am => am.IDMultimediaType == MultimediaType.Imagen.Id).OrderBy(am => am.IDMultimedia).ToList())
                {
                    this.ImageMultimedia.Add(new ImageMultimediaViewModel(
                        multimediaQueryModel: multimediaQueryModel));
                }
                this.Documents = new List<DocumentMultimediaViewModel>();
                foreach (var multimediaQueryModel in multimediaQueryModels
                    .Where(am => am.IDMultimediaType == MultimediaType.Documento.Id).OrderBy(am => am.IDMultimedia).ToList())
                {
                    this.Documents.Add(new DocumentMultimediaViewModel(
                        multimediaQueryModel: multimediaQueryModel));
                }
                this.Widgets = new List<WidgetMultimediaViewModel>();
                foreach (var multimediaQueryModel in multimediaQueryModels
                    .Where(am => am.IDMultimediaType == MultimediaType.Widget.Id).OrderBy(am => am.IDMultimedia).ToList())
                {
                    this.Widgets.Add(new WidgetMultimediaViewModel(
                        multimediaQueryModel: multimediaQueryModel));
                }
            }
        }
        public ArticleViewModel(MappingMode mappingMode,
            ArticleQueryModel articleQueryModel,
            List<ArticleMultimediaQueryModel> multimediaQueryModels,
            List<ArticleQueryModel> relatedArticleQueryModels,
            List<string> headerArticleScriptCodes,
            List<string> bottomArticleScriptCodes,
            List<string> scriptOnArticleBottomCodes) :
        this(mappingMode: mappingMode, articleQueryModel: articleQueryModel, multimediaQueryModels: multimediaQueryModels)
        {
            if (mappingMode == MappingMode.Complete)
            {
                this.RelatedArticles = new List<ArticleViewModel>();
                foreach (var relatedArticleQueryModel in relatedArticleQueryModels)
                {
                    this.RelatedArticles.Add(new ArticleViewModel(
                        mappingMode: MappingMode.Atomic,
                        articleQueryModel: relatedArticleQueryModel));
                }
                this.HeaderScriptCodes = new List<string>();
                foreach(var code in headerArticleScriptCodes)
                {
                    this.HeaderScriptCodes.Add(code);
                }
                this.BottomScriptCodes = new List<string>();
                foreach (var code in bottomArticleScriptCodes)
                {
                    this.BottomScriptCodes.Add(code);
                }
                this.ArticleBottomScriptCodes = new List<string>();
                foreach (var code in scriptOnArticleBottomCodes)
                {
                    this.ArticleBottomScriptCodes.Add(code);
                }
            }
        }


        public string GetSlideImage(string width)
        {
            if (string.IsNullOrEmpty(this.SlideImage)) return null;
            return this.SlideImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string GetVerticalImage(string width)
        {
            if (string.IsNullOrEmpty(this.VerticalImage))
                return null;
            return this.VerticalImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string GetHorizontalImage(string width)
        {
            if (string.IsNullOrEmpty(this.HorizontalImage))
                return null;
            return this.HorizontalImage.Replace("/80/",
                string.Format("/{0}/", width));
        }

        public bool HasLongTitle
        {
            get
            {
                return this.Title.Length >= 40;
            }
        }

    }
}