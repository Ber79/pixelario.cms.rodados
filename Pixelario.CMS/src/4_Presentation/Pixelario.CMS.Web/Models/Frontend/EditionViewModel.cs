﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class EditionViewModel
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public string URL
        {
            get
            {
                return string.Format("/Contents/{0}/{1}",
                    this.ID, ToolsForURL.GenerateSlug(this.Title));
            }
        }
        public List<ArticleViewModel> Articles { get; set; }
        public IList<PageViewModel> Pages { get; set; }
        public EditionViewModel()
        {

        }
        public EditionViewModel(EditionQueryModel editionQueryModel,
            List<ArticleQueryModel> articleQueryModelList) : base()
        {
            this.Articles = new List<ArticleViewModel>();
            this.ID = editionQueryModel.IDEdition;
            this.Title = editionQueryModel.Title;
            this.Pages = new List<PageViewModel>();
            foreach (var articleQueryModel in articleQueryModelList)
            {
                if (articleQueryModel != null)
                {
                    this.Articles.Add(new ArticleViewModel(
                        editionType: editionQueryModel.EditionType,
                        articleQueryModel: articleQueryModel));
                }
            }
        }

        public EditionViewModel(MappingMode mappingMode,
            EditionQueryModel editionsQueryModel)
        {
            this.ID = editionsQueryModel.IDEdition;
            this.Title = editionsQueryModel.Title;
            this.Summary = editionsQueryModel.Summary;
            this.Keywords = editionsQueryModel.Keywords;
            if (mappingMode == MappingMode.Atomic)
            {
                this.Pages = new List<PageViewModel>();
                foreach (var page in editionsQueryModel.Pages)
                {
                    this.Pages.Add(new PageViewModel(mappingMode: MappingMode.Atomic,
                        pageQueryModel: page));
                }
            }
        }
    }
}