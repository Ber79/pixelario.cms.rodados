﻿using Pixelario.CMS.Application.QueryModels.Advertisments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class AdvertismentFrontModel
    {
        public string Title { get; private set; }
        public string HomeImage { get; private set; }
        public string VerticalImage { get; private set; }
        public string URL { get; private set; }
        public bool HasCode { get; set; }
        public string Code { get; set; }
        public string Href { get; set; }
        private AdvertismentFrontModel()
        {

        }
        public AdvertismentFrontModel(MappingMode mappingMode,
            AdvertismentQueryModel advertismentQueryModel)
        {
            this.Title = advertismentQueryModel.Title;
            this.HomeImage = advertismentQueryModel.HomeImage;
            this.HasCode = false;
            if (!string.IsNullOrEmpty(advertismentQueryModel.Code))
            {
                this.HasCode = true;
                this.Href = string.Format("/Advertisments/Widget/{0}", advertismentQueryModel.IDAdvertisment);
                this.Code = advertismentQueryModel.Code;
            }
            else
            {
                this.URL = advertismentQueryModel.URL;
            }
        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
    }
}