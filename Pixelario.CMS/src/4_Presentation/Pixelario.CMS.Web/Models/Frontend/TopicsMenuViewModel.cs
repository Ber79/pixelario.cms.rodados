﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class TopicsMenuViewModel
    {
        public List<TopicViewModel> Topics { get; set; }

        public bool ShowSubTopics { get; private set; }

        private TopicsMenuViewModel()
        {

        }
        public TopicsMenuViewModel(List<TopicQueryModel> topicsQueryModels, 
            List<ConfigurationQueryModel> configurationQueryModels)
        {
            this.Topics = new List<TopicViewModel>();
            foreach (var topicQueryModel in topicsQueryModels)
            {
                this.Topics.Add(new TopicViewModel(mappingMode: MappingMode.Atomic,
                    topicQueryModel: topicQueryModel));
            }
            var showSubTopics = configurationQueryModels.Where(
                c => c.Key == "topics/menu/showsubtopics").FirstOrDefault();
            if (showSubTopics != null)
                this.ShowSubTopics = showSubTopics.State == "on";
        }
    }
}