﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class TopicViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string URL { get; private set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public string HomeImage { get; set; }
        public IList<SubTopicViewModel> SubTopics { get; set; }
        public TopicViewModel(MappingMode mappingMode,
            TopicQueryModel topicQueryModel)
        {
            this.ID = topicQueryModel.IDTopic;
            this.Title = topicQueryModel.Title;
            this.URL = topicQueryModel.URL;
            if (mappingMode == MappingMode.Summary ||
                mappingMode == MappingMode.Complete)
            {
                this.Summary = topicQueryModel.Summary;
                this.HomeImage = topicQueryModel.HomeImage;

            }
            if (mappingMode == MappingMode.Complete)
            {
                this.Keywords = topicQueryModel.Keywords;
            }
        }
        public TopicViewModel(MappingMode mappingMode,
            TopicQueryModel topicQueryModel,
            string urlRef)
        {
            this.ID = topicQueryModel.IDTopic;
            this.Title = topicQueryModel.Title;
            this.URL = topicQueryModel.URL + "?ref=" + urlRef;
            if (mappingMode == MappingMode.Summary ||
                mappingMode == MappingMode.Complete)
            {
                this.Summary = topicQueryModel.Summary;
                this.HomeImage = topicQueryModel.HomeImage;

            }
            if (mappingMode == MappingMode.Complete)
            {
                this.Keywords = topicQueryModel.Keywords;
            }
        }

        public TopicViewModel(MappingMode mappingMode, TopicQueryModel topicQueryModel,  List<SubTopicQueryModel> subTopicQueryModels) :
            this( mappingMode: mappingMode, topicQueryModel: topicQueryModel)
        {
            if (subTopicQueryModels != null && subTopicQueryModels.Count > 0)
            {
                this.SubTopics = new List<SubTopicViewModel>();
                foreach (var subTopicQueryModel in subTopicQueryModels)
                {
                    this.SubTopics.Add(new SubTopicViewModel(
                        mappingMode: MappingMode.Atomic,
                        subTopicQueryModel: subTopicQueryModel));
                }
            }

        }
    }
}