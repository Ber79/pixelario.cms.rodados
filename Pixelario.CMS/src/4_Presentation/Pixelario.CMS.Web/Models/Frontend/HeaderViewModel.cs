﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class HeaderViewModel
    {
        public bool SubscriptionsEnable { get; set; }
        public MenuViewModel MenuViewModel { get; private set; }

        public string LogoClass { get; private set; }
        public string Title { get; private set; }
        public bool ShowSearch { get; private set; }

        public bool ShowTwitterLink { get; private set; }
        public string TwitterLink { get; private set; }
        public bool ShowFacebookLink { get; private set; }
        public string FacebookLink { get; private set; }
        public bool ShowInstagramLink { get; private set; }
        public string InstagramLink { get; private set; }
        public bool ShowLinkedinLink { get; private set; }
        public string LinkedinLink { get; private set; }

        public bool ShowYouTubeLink { get; private set; }
        public string YoutubeLink { get; private set; }


        public HeaderViewModel(bool subscriptionEnable,
            MenuViewModel menuViewModel,
            List<ConfigurationQueryModel> websiteConfigurationQueryModels,
            List<ConfigurationQueryModel> socialMediaConfigurationQueryModels)
        {
            this.SubscriptionsEnable = subscriptionEnable;
            this.MenuViewModel = menuViewModel;

            var logoFileExtention = websiteConfigurationQueryModels.Where(
                c => c.Key == "website/logo/file-extention").FirstOrDefault();
            if (logoFileExtention != null && !string.IsNullOrEmpty(logoFileExtention.State))
            {
                this.LogoClass = string.Format("e-logo-{0}",
                    logoFileExtention.State.Substring(1));
            }
            var title = websiteConfigurationQueryModels.Where(
                c => c.Key == "website/info/name").FirstOrDefault();
            if (title != null && !string.IsNullOrEmpty(title.State))
            {
                this.Title = title.State;
            }
            var showSearch = websiteConfigurationQueryModels.Where(
               c => c.Key == "website/topheader/showsearch").FirstOrDefault();
            if (title != null && !string.IsNullOrEmpty(title.State))
            {
                this.ShowSearch = showSearch.State == "on";
            }
            else
            {
                this.ShowSearch = false;
            }

            var showTwitterLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/twitter/show").FirstOrDefault();
            var twitterLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/twitter/url").FirstOrDefault();
            if (showTwitterLink != null && twitterLink != null &&
                !string.IsNullOrEmpty(showTwitterLink.State) && !string.IsNullOrEmpty(twitterLink.State) &&
                showTwitterLink.State == "on")
            {
                this.ShowTwitterLink = true;
                this.TwitterLink = string.Format("https://twitter.com/{0}", twitterLink.State);
            }
            else
            {
                this.ShowTwitterLink = false;
            }

            var showFacebookLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/facebook/show").FirstOrDefault();
            var facebookLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/facebook/url").FirstOrDefault();
            if (showFacebookLink != null && facebookLink != null &&
                !string.IsNullOrEmpty(showFacebookLink.State) && !string.IsNullOrEmpty(facebookLink.State) &&
                showFacebookLink.State == "on")
            {
                this.ShowFacebookLink = true;
                this.FacebookLink = string.Format("https://www.facebook.com/{0}", facebookLink.State);
            }
            else
            {
                this.ShowFacebookLink = false;
            }

            var showInstagramLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/instagram/show").FirstOrDefault();
            var instragramLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/instagram/url").FirstOrDefault();
            if (showInstagramLink != null && instragramLink != null &&
                !string.IsNullOrEmpty(showInstagramLink.State) && !string.IsNullOrEmpty(instragramLink.State) &&
                showInstagramLink.State == "on")
            {
                this.ShowInstagramLink = true;
                this.InstagramLink = string.Format("https://www.instagram.com/{0}", instragramLink.State);
            }
            else
            {
                this.ShowInstagramLink = false;
            }

            var showLinkedinLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/linkedin/show").FirstOrDefault();
            var linkedinLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/linkedin/url").FirstOrDefault();
            if (showLinkedinLink != null && instragramLink != null &&
                !string.IsNullOrEmpty(showLinkedinLink.State) && !string.IsNullOrEmpty(linkedinLink.State) &&
                showLinkedinLink.State == "on")
            {
                this.ShowLinkedinLink = true;
                this.LinkedinLink = string.Format("https://www.linkedin.com/{0}", linkedinLink.State);
            }
            else
            {
                this.ShowLinkedinLink = false;
            }

            var showYouTubeLink = socialMediaConfigurationQueryModels.Where(
               c => c.Key == "socialmedia/youtube/show").FirstOrDefault();
            var youtubeLink = socialMediaConfigurationQueryModels.Where(
                c => c.Key == "socialmedia/youtube/url").FirstOrDefault();
            if (showYouTubeLink != null && youtubeLink != null &&
                !string.IsNullOrEmpty(showYouTubeLink.State) && !string.IsNullOrEmpty(youtubeLink.State) &&
                showYouTubeLink.State == "on")
            {
                this.ShowYouTubeLink = true;
                this.YoutubeLink = string.Format("https://youtube.com/channel/{0}", youtubeLink.State);
            }
            else
            {
                this.ShowYouTubeLink = false;
            }
        }
    }
}