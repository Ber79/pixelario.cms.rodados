﻿namespace Pixelario.CMS.Web.Models.Frontend
{
    public enum  MappingMode
    {
        Atomic,
        Summary,
        Complete
    }
}