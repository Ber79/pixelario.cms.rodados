﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.QueryModels.Pages;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class PageViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public string Body { get; private set; }

        public string HomeImage { get; set; }
        public string URL { get; private set; }
        public string Keywords { get; private set; }
        public List<ImageMultimediaViewModel> ImageMultimedia { get; set; }
        public List<DocumentMultimediaViewModel> Documents { get; set; }
        public List<WidgetMultimediaViewModel> Widgets { get; set; }

        public PageViewModel(
            PageQueryModel pageQueryModel)
        {
            this.ID = pageQueryModel.IDPage;
            this.Title = pageQueryModel.Title;
            this.Summary = pageQueryModel.Summary;
            this.Body = pageQueryModel.Body;
            this.HomeImage = pageQueryModel.HomeImage;
            this.URL = pageQueryModel.URL;
            this.Keywords = pageQueryModel.Keywords;
        }
        public PageViewModel(
            PageQueryModel pageQueryModel, List<PageMultimediaQueryModel> multimediaQueryModels) :
            this(pageQueryModel: pageQueryModel)
        {
            this.ImageMultimedia = new List<ImageMultimediaViewModel>();
            foreach(var multimediaQueryModel in multimediaQueryModels
                .Where(pm=> pm.IDMultimediaType == MultimediaType.Imagen.Id).OrderBy(pm=>pm.IDMultimedia).ToList())
            {
                this.ImageMultimedia.Add(new ImageMultimediaViewModel(
                    multimediaQueryModel: multimediaQueryModel));
            }
            this.Documents = new List<DocumentMultimediaViewModel>();
            foreach (var multimediaQueryModel in multimediaQueryModels
                .Where(pm => pm.IDMultimediaType == MultimediaType.Documento.Id).OrderBy(pm => pm.IDMultimedia).ToList())
            {
                this.Documents.Add(new DocumentMultimediaViewModel(
                    multimediaQueryModel: multimediaQueryModel));
            }
            this.Widgets = new List<WidgetMultimediaViewModel>();
            foreach (var multimediaQueryModel in multimediaQueryModels
                .Where(pm => pm.IDMultimediaType == MultimediaType.Widget.Id).OrderBy(pm => pm.IDMultimedia).ToList())
            {
                this.Widgets.Add(new WidgetMultimediaViewModel(
                    multimediaQueryModel: multimediaQueryModel));
            }
        }

        public PageViewModel(MappingMode mappingMode, 
            PageQueryModel pageQueryModel)
        {
            this.ID = pageQueryModel.IDPage;
            this.Title = pageQueryModel.Title;
            this.URL = pageQueryModel.URL;
        }

        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
    }
}