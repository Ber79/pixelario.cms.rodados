﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class SubTopicViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string HomeImage { get; private set; }
        public string URL { get; private set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public SubTopicViewModel(
            MappingMode mappingMode,
            SubTopicQueryModel subTopicQueryModel,
            string urlRef)
        {
            this.ID = subTopicQueryModel.IDSubTopic;
            this.Title = subTopicQueryModel.Title;
            this.URL = subTopicQueryModel.URL + "?ref=" +urlRef;
            if (mappingMode == MappingMode.Summary ||
                mappingMode == MappingMode.Complete)
            {
                this.Summary = subTopicQueryModel.Summary;
                this.HomeImage = subTopicQueryModel.HomeImage;

            }
            if (mappingMode == MappingMode.Complete)
            {
                this.Keywords = subTopicQueryModel.Keywords;
            }
        }

        public SubTopicViewModel(
            MappingMode mappingMode,
            SubTopicQueryModel subTopicQueryModel)
        {
            this.ID = subTopicQueryModel.IDSubTopic;
            this.Title = subTopicQueryModel.Title;            
            this.URL = subTopicQueryModel.URL;
            if (mappingMode == MappingMode.Summary ||
                mappingMode == MappingMode.Complete)
            {
                this.Summary = subTopicQueryModel.Summary;
                this.HomeImage = subTopicQueryModel.HomeImage;

            }
            if (mappingMode == MappingMode.Complete)
            {
                this.Keywords = subTopicQueryModel.Keywords;
            }
        }
    }
}