﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class EspecialEditionsMenuViewModel
    {
        public List<EditionViewModel> Editions { get; set; }

        public string Title { get; private set; }
        public bool Grouped { get; private set; }

        private EspecialEditionsMenuViewModel()
        {

        }
        public EspecialEditionsMenuViewModel(List<EditionQueryModel> editionsQueryModels, List<ConfigurationQueryModel> configurationQueryModels)
        {
            this.Editions = new List<EditionViewModel>();
            foreach (var editionQueryModel in editionsQueryModels)
            {
                this.Editions.Add(new EditionViewModel(mappingMode: MappingMode.Atomic,
                    editionsQueryModel: editionQueryModel));
            }
            var title = configurationQueryModels.Where(
                c => c.Key == "editions/especial_menu/title").FirstOrDefault();
            if (title != null)
                this.Title = title.State;
            var grouped = configurationQueryModels.Where(
                c => c.Key == "editions/especial_menu/group").FirstOrDefault();
            if (grouped != null)
                this.Grouped = grouped.State == "on";
        }
    }
}