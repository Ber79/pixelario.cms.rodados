﻿using Pixelario.CMS.Application.QueryModels.Advertisments;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Pages;
using System.Text.RegularExpressions;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class WidgetMultimediaViewModel
    {
        
        public static readonly Regex YoutubeVideoRegex = 
            new Regex("src\\s*=\\s*\"([^\"]+)\"");
        public string Title { get; private set; }
        public string SRC { get; private set; }
        public string Code { get; private set; }
        public int ID { get; set; }
        public WidgetMultimediaViewModel(PageMultimediaQueryModel multimediaQueryModel)
        {
            this.ID = multimediaQueryModel.IDMultimedia;
            this.Title = multimediaQueryModel.Title;
            this.SRC = multimediaQueryModel.Path1;
            this.Code = multimediaQueryModel.Summary;
        }
        public WidgetMultimediaViewModel(ArticleMultimediaQueryModel multimediaQueryModel)
        {
            this.ID = multimediaQueryModel.IDMultimedia;
            this.Title = multimediaQueryModel.Title;
            this.SRC = multimediaQueryModel.Path1;
            this.Code = multimediaQueryModel.Summary;
        }

        public WidgetMultimediaViewModel(AdvertismentQueryModel queryModel)
        {
            this.ID = queryModel.IDAdvertisment;
            this.Title = queryModel.Title;
            this.SRC = queryModel.HomeImage;
            this.Code = queryModel.Code;
        }

        public string GetSRC(string width)
        {
            if (string.IsNullOrEmpty(this.SRC)) return null;
            return this.SRC.Replace("/80/",
                string.Format("/{0}/", width));
        }
        public string DataType
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Code))
                {
                    if (this.Code.IndexOf("https://www.youtube.com/") > -1)
                    {
                        return "youtube";
                    }

                    else
                    {
                        return "url";
                    }
                }                
                return null;

            }
        }
        public string Href
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Code))
                {
                    if (this.Code.IndexOf("https://www.youtube.com/") > -1)
                    {
                        var youtubeMatch = YoutubeVideoRegex.Match(
                            this.Code);
                        if (youtubeMatch.Success)
                        {
                            var value = youtubeMatch.Groups[0].Value.Substring(5);
                            var url = value.Substring(0, value.Length - 1);
                            return url.Replace("embed/", "watch?v=");
                        }
                            
                    }
                    else
                    {
                        return string.Format("http://localhost:56952/pages/widget/{0}", this.ID);
                    }
                }
                return null;
            }
        }
    }
}