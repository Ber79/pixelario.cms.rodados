﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Web.Models.Frontend
{
    public class MenuViewModel
    {
        public string NavbarClass { get; private set; }
        public string StartTitle { get; set; }
        public string ContactTitle { get; set; }
        private MenuViewModel()
        {

        }
        public MenuViewModel(EspecialEditionsMenuViewModel especialEditionsMenu,
                PagesMenuViewModel pagesMenu,
                TopicsMenuViewModel topicsMenu,
                List<ConfigurationQueryModel> configurationQueryModels)
        {
            this.EspecialEditionsMenu = especialEditionsMenu;
            this.PagesMenu = pagesMenu;
            this.TopicsMenu = topicsMenu;
            var navbarOption = configurationQueryModels.Where(
               c => c.Key == "website/navbar/enumeration").FirstOrDefault();
            this.NavbarClass = "";
            if (navbarOption != null && !string.IsNullOrEmpty(navbarOption.State))
            {
                if (navbarOption.State != "automatic")
                {
                    this.NavbarClass = navbarOption.State;
                }
                else
                {
                    var totalMenuItems = 2 + this.TopicsMenu.Topics.Count + this.PagesMenu.Editions.Count;
                    if (this.EspecialEditionsMenu.Grouped)
                        totalMenuItems += 1;
                    else
                        totalMenuItems += this.EspecialEditionsMenu.Editions.Count;
                    if (totalMenuItems < 5)
                    {
                        this.NavbarClass = "e-short-navbar";
                    }
                    else if (totalMenuItems < 8)
                    {
                        this.NavbarClass = "e-middle-navbar";
                    }
                }
            }


            var startTitle = configurationQueryModels.Where(
                c => c.Key == "website/navbar/startTitle").FirstOrDefault();
            if (startTitle != null && !string.IsNullOrEmpty(startTitle.State))
            {
                this.StartTitle = startTitle.State;
            }
            else
            {
                this.StartTitle = "Inicio";
            }

            var contactTitle = configurationQueryModels.Where(
                c => c.Key == "website/navbar/contactTitle").FirstOrDefault();
            if (contactTitle != null && !string.IsNullOrEmpty(contactTitle.State))
            {
                this.ContactTitle = contactTitle.State;
            }
            else
            {
                this.ContactTitle = "Contacto";
            }
        }

        public EspecialEditionsMenuViewModel EspecialEditionsMenu { get; private set; }
        public PagesMenuViewModel PagesMenu { get; private set; }
        public TopicsMenuViewModel TopicsMenu { get; private set; }

    }
}