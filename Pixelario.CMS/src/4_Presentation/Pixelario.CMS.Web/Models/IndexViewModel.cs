﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;


namespace Pixelario.CMS.Web.Models
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public bool VerticalImagesAllowed { get; set; }
        public IList<ArticleViewModel> News { get; set; }   
        public IList<ArticleViewModel> ArticlesAtSlider { get; set; }
        public List<EditionViewModel> InformativeEditions { get;  set; }
    }
}