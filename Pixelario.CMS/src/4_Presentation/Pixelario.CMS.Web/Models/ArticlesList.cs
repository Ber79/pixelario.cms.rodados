﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models
{
    public class ArticlesList
    {
        public List<ArticleViewModel> Articles { get; private set; }
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public int PageCount { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public int BackPage
        {
            get
            {
                if (this.PageNumber == 1)
                    return 1;
                return this.PageNumber - 1;
            }
        }
        public int NextPage
        {
            get
            {
                if (this.PageCount == 0)
                    return 1;
                if (this.PageNumber == this.PageCount)
                    return PageCount;
                return this.PageNumber + 1;
            }
        }
        public string URL { get; private set; }
        public int TotalCount { get; set; }
        private ArticlesList()
        {

        }
        public ArticlesList(MappingMode mappingMode,
            List<ArticleQueryModel> articleQueryModels,
            int pageNumber, int pageSize,
            int totalCount, int pagerItems,
            string url)
        {
            this.Articles = new List<ArticleViewModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleViewModel(
                    mappingMode: mappingMode,
                    articleQueryModel: articleQueryModel));
            }
            this.PageCount = 1;
            if (totalCount > 0)
                this.PageCount = ((totalCount - 1) / pageSize) + 1;
            var pagerCenterIndex = 1;
            this.StartPage = 1;
            this.EndPage = this.PageCount;
            if (this.PageCount > pagerItems)
            {
                if (pagerItems % 2 != 0)
                {
                    pagerItems = pagerItems++;
                }
                pagerCenterIndex = pagerItems / 2;

                var currentPage = pageNumber;
                var startPage = currentPage - pagerCenterIndex;
                var endPage = currentPage + pagerCenterIndex;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }
                if (endPage > this.PageCount)
                {
                    endPage = this.PageCount;
                    if (endPage > pagerItems)
                    {
                        startPage = endPage - (pagerItems - 1);
                    }
                }
                this.StartPage = startPage;
                this.EndPage = endPage;
            }
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.PageCount = this.PageCount;
            if (url.IndexOf("?") >= 0)
                this.URL = url + "&";
            else
                this.URL = url + "?";
            
            this.TotalCount = totalCount;
        }
    }
}