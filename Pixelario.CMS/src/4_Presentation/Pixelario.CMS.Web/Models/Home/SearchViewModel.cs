﻿using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Home
{
    public class SearchViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public string Title { get; set; }
        public string Q { get; set; }
        public ArticlesList ArticlesList { get; set; }
        public string ToDate { get; set; }
        public string FromDate { get; set; }
    }
}