﻿using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Home
{
    public class ContactViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public bool ShowTechnicalErrors { get; set; }
    }
}