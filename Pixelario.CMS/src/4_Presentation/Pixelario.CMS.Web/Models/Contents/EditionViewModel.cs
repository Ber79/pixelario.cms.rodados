﻿using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.Tools;

namespace Pixelario.CMS.Web.Models.Contents
{
    public class EditionViewModel
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string URL { get; private set; }
        public string Summary { get; private set; }
        public string Keywords { get; private set; }
        public string HomeImage { get; private set; }
        public EditionViewModel(EditionQueryModel queryModel)
        {
            this.ID = queryModel.IDEdition;
            this.Title = queryModel.Title;
            this.URL = string.Format("/Contents/{0}/{1}?ref=content",
                    this.ID, ToolsForURL.GenerateSlug(this.Title));
            this.Summary = queryModel.Summary;
            this.Keywords = queryModel.Keywords;
            this.HomeImage = queryModel.HomeImage;
        }
    }
}