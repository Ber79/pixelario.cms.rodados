﻿using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;

namespace Pixelario.CMS.Web.Models.Contents
{
    public class IndexViewModel : FrontViewModel
    {
        public int MenuIndex { get; set; }
        public EditionViewModel Edition { get; set; }
        public IList<ArticleViewModel> Articles { get; set; }
        public int PageCount { get; set; }

        public IndexViewModel()
        {

        }
        public IndexViewModel(    
            EditionQueryModel editionQueryModel,
            List<ArticleQueryModel> articleQueryModels,
            int pageCount,
            List<ConfigurationQueryModel> configurationsList,
            HeaderViewModel headerViewModel,
            List<AdvertismentFrontModel> asideFlyers,
            List<AdvertismentFrontModel> asideTopFlyers,
            List<AdvertismentFrontModel> footerLeftFlyers,
            List<AdvertismentFrontModel> footerRightFlyers,
            List<AdvertismentFrontModel> scritpFlyers
        ) : base(
            configurationsList, headerViewModel, asideFlyers,
            asideTopFlyers,
            footerLeftFlyers, footerRightFlyers, scritpFlyers
            )
        {
            this.Edition = new EditionViewModel(
                queryModel: editionQueryModel);
            this.Articles = new List<ArticleViewModel>();
            foreach (var articleQueryModel in articleQueryModels)
            {
                this.Articles.Add(new ArticleViewModel(
                    mappingMode: MappingMode.Atomic,
                    articleQueryModel: articleQueryModel,
                    urlRef: "content"));
            }
            this.PageCount = pageCount;
        }

    }
}