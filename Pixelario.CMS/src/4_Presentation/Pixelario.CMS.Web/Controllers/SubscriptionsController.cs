﻿using MediatR;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Web.Models.Subscriptions;
using Pixelario.Identity.Infrastructure.Managers;
using Pixelario.Subscriptions.Application.Queries.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AccountCommnads = Pixelario.Subscriptions.Application.Commands.Accounts;
using UserCommands = Pixelario.CMS.Application.Commands.Users;

namespace Pixelario.CMS.Web.Controllers
{
    public class SubscriptionsController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IAccountsQueries _accountsQueries;
        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;

        public SubscriptionsController(
            ILogger logger, IMediator mediator,
            IAccountsQueries accountsQueries,
            ApplicationSignInManager signInManager,
            IAuthenticationManager authenticationManager) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _accountsQueries = accountsQueries ?? throw new ArgumentNullException(nameof(accountsQueries));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _authenticationManager = authenticationManager ?? throw new ArgumentNullException(nameof(authenticationManager));
        }

        // GET: Subscriptions
        public async Task<ActionResult> Create(bool? showTechnicalErrors)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();

            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion

            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new CreateViewModel()
            {
                MenuIndex = 1,
                ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                    showTechnicalErrors.Value : false,
                Title = "Suscribirme",
                Email = "",
                PlanID = Plan.None.Id,
                Plans = Plan.List(),
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideTopFlyers = asideTopFlyers,
                AsideFlyers = asideFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }

        [HttpPost]
        //[Route("Create")]
        public async Task<ActionResult> Create(FormCollection collection,
            bool? showTechnicalErrors)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            bool userCreated = false;
            bool subscriptorRoleSetted = false;
            bool subscriptorAccountCreated = false;
            var errors = new List<string>();
            int planId = 0;
            if (collection["PlanId"] != null)
            {
                int.TryParse(collection["PlanId"], out planId);
            }
            var addUserCommand = new UserCommands.AddCommand(
                userName: collection["Email"],
                email: collection["Email"],
                password: collection["Password"],
                firstName: collection["FirstName"],
                lastName: collection["LastName"]);
            addUserCommand.SetCreatedBy(
                createdBy: "SubscriptionCreateForm");
            var responseAddUserCommand = await _mediator.Send(addUserCommand);
            if (responseAddUserCommand.success)
            {
                userCreated = true;
            }
            else
            {
                errors.AddRange(responseAddUserCommand.errors);
            }
            if (userCreated)
            {
                var setRoleCommand = new UserCommands.SetRolesCommand(
                       userId: responseAddUserCommand.ID.Value,
                       roles: new string[] { "Subscriptor" });
                setRoleCommand.SetSolicitedBy(
                    solicitedBy: "SubscriptionCreateForm");
                var responseSetRolesCommand = await _mediator.Send(setRoleCommand);
                if (responseSetRolesCommand.success)
                {
                    subscriptorRoleSetted = true;
                }
                else
                {
                    userCreated = false;
                    errors.AddRange(responseSetRolesCommand.errors);

                }
            }
            if (subscriptorRoleSetted)
            {
                var addAccountCommand = new AccountCommnads.AddCommand(
                    userId: responseAddUserCommand.ID.Value,
                    plan: Plan.From(planId));
                var responseAddAccountCommand = await _mediator.Send(addAccountCommand);
                if (responseAddAccountCommand.success)
                {
                    subscriptorAccountCreated = true;
                }
                else
                {
                    userCreated = false;
                    subscriptorRoleSetted = false;
                    errors.AddRange(responseAddAccountCommand.errors);
                }
            }
            if (subscriptorAccountCreated)
            {
                TempData["success"] = "El usuario fue creado exitosamente.";
                return RedirectToAction("Success");
            }
            else
            {
                if (!userCreated)
                {
                    var deleteUserCommand = new UserCommands.DeleteCommand(
                    userId: responseAddUserCommand.ID.Value);
                    deleteUserCommand.SetSolicitedBy(
                        solicitedBy: "SubscriptionCreateForm");
                    var responseDeleteUserCommand = await _mediator.Send(deleteUserCommand);
                    if (!responseDeleteUserCommand.success)
                    {
                        errors.AddRange(responseDeleteUserCommand.errors);
                    }
                }
                if (!subscriptorRoleSetted)
                {
                    var removeRoleCommand = new UserCommands.SetRolesCommand(
                       userId: responseAddUserCommand.ID.Value,
                       roles: new string[] { });
                    removeRoleCommand.SetSolicitedBy(
                        solicitedBy: "SubscriptionCreateForm");
                    var responseRemoveRolesCommand = await _mediator.Send(removeRoleCommand);
                    if (!responseRemoveRolesCommand.success)
                    {
                        errors.AddRange(responseRemoveRolesCommand.errors);
                    }
                }
            }
            TempData["errors"] = errors;
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion
            if (showTechnicalErrors.HasValue && showTechnicalErrors.Value)
            {
                TempData["errors"] = errors;
            }
            else
            {
                TempData["errors"] = new List<string>() { "Ocurrió un error, intente nuevamente." };
            }
            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new CreateViewModel()
            {
                MenuIndex = 1,
                Title = "Suscribirme",
                ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                    showTechnicalErrors.Value : false,
                Email = collection["Email"],
                PlanID = planId,
                Plans = Plan.List(),
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }


        //[Route("Contact/Success")]
        public async Task<ActionResult> Success()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion

            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new CreateViewModel()
            {
                MenuIndex = 1,
                Title = "Subcripción exitosa",
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }

        public async Task<ActionResult> Login(bool? showTechnicalErrors,
            string returnUrl)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();

            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion

            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new LoginViewModel()
            {
                MenuIndex = 1,
                ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                    showTechnicalErrors.Value : false,
                Title = "Ingresar",
                Description = "Ingrese con sus credenciales",
                Keywords = "ingresar",
                ReturnUrl = returnUrl,
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideTopFlyers = asideTopFlyers,
                AsideFlyers = asideFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(SignInFrontModel model,
            string returnUrl)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _signInManager.PasswordSignInAsync(
                userName: model.Email,
                password: model.Password,
                isPersistent: model.RememberMe == "on",
                shouldLockout: true);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    TempData["errors"] = new List<string>() { "Cuenta bloqueada." };
                    return Redirect(string.Format("/Subscriptions/Login?returnUrl={0}{1}",
                        returnUrl, model.ShowTechnicalErrors ? "&showTechnicalErrors=true" : ""));
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    TempData["errors"] = new List<string>() { "Intento de login invalido." };
                    return Redirect(string.Format("/Subscriptions/Login?returnUrl={0}{1}",
                        returnUrl, model.ShowTechnicalErrors ? "&showTechnicalErrors=true" : ""));
            }
        }
        public ActionResult LogOff()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            _authenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect("/");
        }
        public async Task<ActionResult> Profile()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            if (!this.User.Identity.IsAuthenticated)
                return Redirect("/");
            if (!this.User.IsInRole("Subscriptor"))
                return Redirect("/");
            var user = await _mediator.Send(new UserCommands.GetByNameCommand(
                userName: this.User.Identity.Name));
            if (user == null || user.Id <= 0)
                return Redirect("/");
            var accountQueryModel = await _accountsQueries.GetByUserIdAsync(
                userId: user.Id);
            if (accountQueryModel == null || accountQueryModel.IDAccount <= 0)
                return Redirect("/");
            var account = new AccountFrontModel(
                applicationUser: user,
                accountQueryModel: accountQueryModel);
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion

            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion

            var model = new ProfileViewModel()
            {
                MenuIndex = 1,
                Title = "Perfil del usuario",
                Account = account,
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideTopFlyers = asideTopFlyers,
                AsideFlyers = asideFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }


    }
}