﻿using System.Web.Mvc;

namespace Pixelario.CMS.Web.Controllers
{
    [RoutePrefix("Errors")]
    public class ErrorPagesController : Controller
    {
        // GET: Errors
        [Route("500")]
        public ActionResult Error500()
        {
            return View();
        }
        [Route("404")]
        public ActionResult Error404()
        {
            return View();
        }
        [Route("403")]
        public ActionResult Error403()
        {
            return View();
        }
    }
}