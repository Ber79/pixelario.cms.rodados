﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Web.Models.Contents;
using Pixelario.CMS.Web.Models.Frontend;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
namespace Pixelario.CMS.Web.Controllers
{
    [RoutePrefix("Contents")]
    public class ContentsController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly int PAGESIZE = 6;

        public ContentsController(
            ILogger logger, IMediator mediator
            ) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        // GET: Topic
        [Route("{iDEdition:int}/{title}")]
        public async Task<ActionResult> Index(int iDEdition)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var edition = await _mediator.Send(new EditionCommands.GetCommand(
                    iDEdition: iDEdition));
            if (edition != null && edition.Enabled &&
                (edition.EditionType == EditionType.Especial_Content))
            {
                #region Header items           
                var headerViewModel = await GetHeaderViewModel();
                #endregion
                var configurationsList = await GetCommonFrontConfigurations();
                #region Flyers items
                var asideFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                var asideTopFlyers = await GetFlyer(
                   placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                var footerLeftFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                var footerRightFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                var scritpFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Script);
                #endregion
                var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
                var pageCount = 1;
                var totalRows = await _mediator.Send(new ArticleCommands.CountCommand(
                    iDEdition: iDEdition,
                    iDTopic: null,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments
                ));
                if (totalRows > 0)
                    pageCount = ((totalRows - 1) / PAGESIZE) + 1;
                var articleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                    iDEdition: iDEdition,
                    iDTopic: null,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments,
                    rowIndex: 0,
                    rowCount: PAGESIZE, //int.MaxValue,
                    columnOrder: "a.PublicationDate",
                    orderDirection: "DESC"
                    ));
                var model = new IndexViewModel(
                    editionQueryModel: edition,
                    articleQueryModels: articleQueryModels,
                    pageCount: pageCount,
                    configurationsList: configurationsList,
                    headerViewModel: headerViewModel,
                    asideFlyers: asideFlyers,
                    asideTopFlyers: asideTopFlyers,
                    footerLeftFlyers: footerLeftFlyers,
                    footerRightFlyers: footerRightFlyers,
                    scritpFlyers: scritpFlyers
                    );
                return View(model);
            }
            else
            {
                _logger.Warning("Edition with id {0} not found", iDEdition);
                throw new HttpException(404, "Edition not found");
            }
        }
    }
}