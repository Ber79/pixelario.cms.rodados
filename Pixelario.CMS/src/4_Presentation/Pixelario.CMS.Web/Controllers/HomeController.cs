﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Web.Models;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.CMS.Web.Models.Home;
using Pixelario.CMS.Web.Services.Sitemap;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;

namespace Pixelario.CMS.Web.Controllers
{
    [RoutePrefix("")]
    public class HomeController : FrontController
    {
        private readonly ISitemapService _sitemapServices;
        private readonly IMediator _mediator;

        private ILogger _logger;
        public HomeController(
            ILogger logger,
            IMediator mediator,
            ISitemapService sitemapServices
            ) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _sitemapServices = sitemapServices ?? throw new ArgumentNullException(nameof(sitemapServices));
        }
        public async Task<ActionResult> Index()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion
            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new IndexViewModel()
            {
                MenuIndex = 1,
                News = await GetArticlesOnColumns(),
                ArticlesAtSlider = await GetArticlesOnSlide(),
                VerticalImagesAllowed = true,
                InformativeEditions = await GetInformativeEditions(),
                HeaderViewModel = headerViewModel,
                ConfigurationsList = configurationsList,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }

        private async Task<List<ArticleViewModel>> GetArticlesOnColumns()
        {
            var news = new List<ArticleViewModel>();
            var newsEditions = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                orderDirection: "ASC",
                enabled: true,
                atHome: true,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.News },
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.[Order]"));
            var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
            if (newsEditions != null && newsEditions.Count() > 0)
            {
                foreach (var articleQueryModel in await _mediator.Send(new ArticleCommands.ListCommand(
                    iDEdition: newsEditions.First().IDEdition,
                    iDTopic: null,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: true,
                    placesAtHome: ArticlePlaceAtHome.Columns,
                    filteredBy: microFilterStatments,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "a.[Order]",
                    orderDirection: "ASC"
                    )))
                {
                    news.Add(new ArticleViewModel(mappingMode: MappingMode.Atomic,
                        articleQueryModel: articleQueryModel));
                }
            }
            return news;
        }
        private async Task<List<ArticleViewModel>> GetArticlesOnSlide()
        {
            var articlesAtSlider = new List<ArticleViewModel>();
            var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
            foreach (var articleQueryModel in await _mediator.Send(new ArticleCommands.ListCommand(
                iDEdition: 1,
                iDTopic: null,
                iDSubTopic: null,
                enabled: true,
                atHome: true,
                placesAtHome: ArticlePlaceAtHome.Slider,
                filteredBy: microFilterStatments,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "a.[Order]",
                orderDirection: "ASC"
                )))
            {
                articlesAtSlider.Add(new ArticleViewModel(
                    mappingMode: MappingMode.Atomic,
                    articleQueryModel: articleQueryModel));
            }
            return articlesAtSlider;
        }

        [Route("Search")]
        public async Task<ActionResult> Search(string q, string toDate, string fromDate,
            int? pageNumber, int? pageSize)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion
            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var _pageSize = pageSize ?? 10;
            var _pageNumber = pageNumber ?? 1;
            ArticlesList articlesList = null;
            if (!string.IsNullOrEmpty(q) && q.Trim().Length >= 4)
            {
                var toDateTime = DateTime.Now;
                if (!string.IsNullOrEmpty(toDate))
                {
                    DateTime.TryParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None,
                        out toDateTime);
                }
                var fromDateMicroFilterStatments = "";
                var fromDateTime = new DateTime(1900, 1, 1);
                if (!string.IsNullOrEmpty(fromDate) &&
                    DateTime.TryParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None,
                        out fromDateTime))
                {
                    fromDateMicroFilterStatments = $"publishDateGreatherThan={fromDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";

                }
                var microFilterStatments = $"query={q.Trim()};publishDateLessThan={toDateTime.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};{fromDateMicroFilterStatments}";
                var articlesCount = await _mediator.Send(new ArticleCommands.CountCommand(
                    iDEdition: null,
                     iDTopic: null,
                     iDSubTopic: null,
                     enabled: true,
                     atHome: null,
                     placesAtHome: null,
                     filteredBy: microFilterStatments));
                var articleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                    iDEdition: null,
                    iDTopic: null,
                    iDSubTopic: null,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments,
                    rowIndex: (_pageNumber - 1) * _pageSize,
                    rowCount: _pageSize,
                    columnOrder: "a.PublicationDate",
                orderDirection: "DESC"));
                articlesList = new ArticlesList(
                    mappingMode: MappingMode.Atomic,
                    articleQueryModels: articleQueryModels,
                    pageNumber: _pageNumber,
                    pageSize: _pageSize,
                    totalCount: articlesCount,
                    pagerItems: 5,
                    url: string.Format("/Search?q={0}{1}{2}",
                        q,
                        !string.IsNullOrEmpty(toDate) ? string.Format("&toDate={0}", toDate) : "",
                        !string.IsNullOrEmpty(fromDate) ? string.Format("&fromDate={0}", fromDate) : ""
                    ));
            }
            var model = new SearchViewModel()
            {
                MenuIndex = 1,
                Title = $"Resultado de la búsqueda de \"{q}\"",
                Q = q,
                ToDate = toDate,
                FromDate = fromDate,
                ArticlesList = articlesList,
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }


        private async Task<List<EditionViewModel>> GetInformativeEditions()
        {
            var informativeEditions = new List<EditionViewModel>();
            foreach (var editionQuery in await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                pagesOnEditionFilter: null,
                enabled: true,
                atHome: true,
                onMenu: null,
                editionTypes: new List<EditionType>() { EditionType.Especial_Content },
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.[Order]",
                orderDirection: "ASC"
                )))
            {
                if (editionQuery != null)
                {

                    var articleList = await _mediator.Send(new ArticleCommands.ListCommand(
                        iDEdition: editionQuery.IDEdition,
                        iDTopic: null,
                        iDSubTopic: null,
                        enabled: true,
                        atHome: true,
                        placesAtHome: null,
                        filteredBy: null,
                        rowIndex: 0,
                        rowCount: 3,
                        columnOrder: "a.[Order]",
                        orderDirection: "ASC"));
                    if (articleList != null && articleList.Count > 0)
                    {
                        informativeEditions.Add(new EditionViewModel(
                            editionQueryModel: editionQuery,
                            articleQueryModelList: articleList));
                    }
                }
            }
            return informativeEditions;
        }

        [Route("sitemap.xml")]
        public async Task<ActionResult> SitemapXML()
        {
            var xml = await _sitemapServices.GetSitemapDocument();
            return this.Content(xml, "text/xml", Encoding.UTF8);
        }
        [Route("Contact")]
        public async Task<ActionResult> Contact(
            bool? showTechnicalErrors)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();

            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion

            #region Me            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion

            var model = new ContactViewModel()
            {
                MenuIndex = 1,
                ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                    showTechnicalErrors.Value : false,
                Title = "Contacto",
                Name = "",
                Phone = "",
                Email = "",
                Comment = "",
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }

        [HttpPost]
        [Route("Contact")]
        public async Task<ActionResult> Contact(FormCollection collection,
            bool? showTechnicalErrors)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var contactConfigurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                filteredBy: null,
                keys: new string[] { "contact/" },
                configurationType: null,
                rowIndex: 0, rowCount: int.MaxValue,
                orderBy: 2,
                orderDirection: 0));
            var toAddresses = this.GetConfiguration(contactConfigurations, "contact/to/addresses");
            var fromAddress = this.GetConfiguration(contactConfigurations, "contact/from/address");
            if (!string.IsNullOrEmpty(toAddresses) && !string.IsNullOrEmpty(fromAddress))
            {
                var from = new MailAddress(fromAddress);
                SmtpClient smtpCliente = new SmtpClient()
                {
                    Host = this.GetConfiguration(contactConfigurations, "contact/smtp/host"),
                    Port = int.Parse(this.GetConfiguration(contactConfigurations, "contact/smtp/port")),
                    EnableSsl = this.GetConfiguration(contactConfigurations, "contact/smtp/enablessl") == "on",
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(this.GetConfiguration(contactConfigurations, "contact/from/address"),
                        this.GetConfiguration(contactConfigurations, "contact/from/password"))
                };
                try
                {
                    using (var message = new MailMessage()
                    {
                        From = from,
                        Subject = "Consulta desde el sitio web",
                        Body = string.Format("Nombre: {0} <br/>Email: {1} <br/>Teléfono: {2} <br/>Mensaje: {3}",
                            collection["Name"], collection["Email"],
                            collection["Phone"], collection["Comment"]),
                        IsBodyHtml = true
                    })
                    {
                        foreach (var toAddress in toAddresses.Split(';'))
                        {
                            if (!string.IsNullOrEmpty(toAddress))
                            {
                                message.To.Add(toAddress);
                            }
                        }
                        smtpCliente.Send(message);
                    }
                    return Redirect("/contact/success");
                }
                catch (Exception ex)
                {
                    var configurationsList = await GetCommonFrontConfigurations();
                    #region Flyers items
                    var asideFlyers = await GetFlyer(
                        placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                    var asideTopFlyers = await GetFlyer(
                       placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                    var footerLeftFlyers = await GetFlyer(
                        placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                    var footerRightFlyers = await GetFlyer(
                        placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                    var scritpFlyers = await GetFlyer(
                        placeAtWeb: AdvertismentPlaceAtWeb.Script);
                    #endregion


                    #region Header items           
                    var headerViewModel = await GetHeaderViewModel();
                    #endregion


                    if (showTechnicalErrors.HasValue && showTechnicalErrors.Value)
                    {
                        var errors = new List<string>();
                        errors.Add(ex.Message);
                        if (ex.InnerException != null)
                            errors.Add(ex.InnerException.Message);
                        TempData["errors"] = errors;
                    }
                    else
                        TempData["errors"] = new List<string>() { "Ocurrió un error, intente nuevamente." };
                    var model = new ContactViewModel()
                    {
                        MenuIndex = 1,
                        Title = "Contacto",
                        ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                            showTechnicalErrors.Value : false,
                        Name = collection["Name"],
                        Phone = collection["Phone"],
                        Email = collection["Email"],
                        Comment = collection["Comment"],
                        ConfigurationsList = configurationsList,
                        HeaderViewModel = headerViewModel,
                        AsideFlyers = asideFlyers,
                        AsideTopFlyers = asideTopFlyers,
                        FooterLeftFlyers = footerLeftFlyers,
                        FooterRightFlyers = footerRightFlyers,
                        ScriptFlyers = scritpFlyers
                    };
                    return View(model);
                }
            }
            else
            {
                var configurationsList = await GetCommonFrontConfigurations();
                #region Flyers items
                var asideFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                var asideTopFlyers = await GetFlyer(
                   placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                var footerLeftFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                var footerRightFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                var scritpFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Script);
                #endregion
                #region Header items           
                var headerViewModel = await GetHeaderViewModel();
                #endregion


                if (showTechnicalErrors.HasValue && showTechnicalErrors.Value)
                {
                    TempData["errors"] = new List<string>() { "La configuración de contacto no fue enviada correctamente." };
                }
                else
                    TempData["errors"] = new List<string>() { "Ocurrió un error, intente nuevamente." };
                var model = new ContactViewModel()
                {
                    MenuIndex = 1,
                    Title = "Contacto",
                    ShowTechnicalErrors = showTechnicalErrors.HasValue ?
                        showTechnicalErrors.Value : false,
                    Name = collection["Name"],
                    Phone = collection["Phone"],
                    Email = collection["Email"],
                    Comment = collection["Comment"],
                    ConfigurationsList = configurationsList,
                    HeaderViewModel = headerViewModel,
                    AsideFlyers = asideFlyers,
                    AsideTopFlyers = asideTopFlyers,
                    FooterLeftFlyers = footerLeftFlyers,
                    FooterRightFlyers = footerRightFlyers,
                    ScriptFlyers = scritpFlyers
                };
                return View(model);
            }
        }

        [Route("Contact/Success")]
        public async Task<ActionResult> Success()
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var configurationsList = await GetCommonFrontConfigurations();
            #region Flyers items
            var asideFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            var asideTopFlyers = await GetFlyer(
               placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
            var footerLeftFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
            var footerRightFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
            var scritpFlyers = await GetFlyer(
                placeAtWeb: AdvertismentPlaceAtWeb.Script);
            #endregion
            #region Header items           
            var headerViewModel = await GetHeaderViewModel();
            #endregion
            var model = new ContactViewModel()
            {
                MenuIndex = 1,
                Title = "Contacto - Mensaje enviado",
                ConfigurationsList = configurationsList,
                HeaderViewModel = headerViewModel,
                AsideFlyers = asideFlyers,
                AsideTopFlyers = asideTopFlyers,
                FooterLeftFlyers = footerLeftFlyers,
                FooterRightFlyers = footerRightFlyers,
                ScriptFlyers = scritpFlyers
            };
            return View(model);
        }
        private string GetConfiguration(List<ConfigurationQueryModel> configurationsList, string key)
        {
            var configuration = configurationsList.FirstOrDefault(c => c.Key == key);
            return configuration != null ? configuration.State : "";
        }
    }
}