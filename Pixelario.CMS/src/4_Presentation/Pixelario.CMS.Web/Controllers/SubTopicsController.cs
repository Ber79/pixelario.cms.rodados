﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.CMS.Web.Models.SubTopics;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;

namespace Pixelario.CMS.Web.Controllers
{
    [RoutePrefix("SubTopics")]
    public class SubTopicsController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        private readonly int PAGESIZE = 6;
        public SubTopicsController(
            ILogger logger, IMediator mediator
            ) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        // GET: SubTopic
        [Route("{iDSubTopic:int}/{title}")]
        public async Task<ActionResult> Index(int iDSubTopic)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var subTopic = await _mediator.Send(new TopicCommands.GetSubTopicCommand(
                    iDSubTopic: iDSubTopic));
            if (subTopic != null && subTopic.Enabled)
            {
                var configurationsList = await GetCommonFrontConfigurations();
                #region Flyers items
                var asideFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Aside);
                var asideTopFlyers = await GetFlyer(
                   placeAtWeb: AdvertismentPlaceAtWeb.AsideTop);
                var footerLeftFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Left);
                var footerRightFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Footer_Right);
                var scritpFlyers = await GetFlyer(
                    placeAtWeb: AdvertismentPlaceAtWeb.Script);
                #endregion
                #region Header items           
                var headerViewModel = await GetHeaderViewModel();
                #endregion

                var topicQueryModel = await _mediator.Send(new TopicCommands.GetCommand(
                        iDTopic: subTopic.IDTopic));
                var subTopicQueryModels = await _mediator.Send(new TopicCommands.ListSubTopicsCommand(
                        iDTopic: subTopic.IDTopic,
                        filteredBy: null,
                        enabled: true,
                        atHome: true,
                        onMenu: null,
                        rowIndex: 0,
                        rowCount: int.MaxValue,
                        columnOrder: "s.[Order]",
                        orderDirection: "ASC"));
                var microFilterStatments = $"publishDateLessThan={DateTime.Now.AddDays(1).Date.ToString("yyyy-MM-dd HH:mm:ss.fff")};";
                var articleQueryModels = await _mediator.Send(new ArticleCommands.ListCommand(
                     iDEdition: null,
                     iDTopic: null,
                     iDSubTopic: iDSubTopic,
                     enabled: true,
                     atHome: null,
                     placesAtHome: null,
                     filteredBy: microFilterStatments,
                     rowIndex: 0,
                     rowCount: PAGESIZE, //int.MaxValue,
                     columnOrder: "a.PublicationDate",
                     orderDirection: "DESC"
                     ));
                var pageCount = 1;
                var totalRows = await _mediator.Send(new ArticleCommands.CountCommand(
                    iDEdition: null,
                    iDTopic: null,
                    iDSubTopic: iDSubTopic,
                    enabled: true,
                    atHome: null,
                    placesAtHome: null,
                    filteredBy: microFilterStatments
                ));
                if (totalRows > 0)
                    pageCount = ((totalRows - 1) / PAGESIZE) + 1;

                var model = new IndexViewModel(
                    subTopicQueryModel: subTopic,
                    topicQueryModel: topicQueryModel,
                    subTopicQueryModels: subTopicQueryModels,
                    articleQueryModels: articleQueryModels,
                    pageCount: pageCount,
                    configurationsList: configurationsList,
                    headerViewModel: headerViewModel,
                    asideFlyers: asideFlyers,
                    asideTopFlyers: asideTopFlyers,
                    footerLeftFlyers: footerLeftFlyers,
                    footerRightFlyers:footerRightFlyers,
                    scritpFlyers: scritpFlyers
                    );
                return View(model);
            }
            else
            {
                _logger.Warning("Subtopic with id {0} not found", iDSubTopic);
                throw new HttpException(404, "Subtopic not found");
            }
        }
    }
}