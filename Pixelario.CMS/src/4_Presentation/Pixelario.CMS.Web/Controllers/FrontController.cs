﻿using MediatR;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Web.Models.Frontend;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AdvertismentCommands = Pixelario.CMS.Application.Commands.Advertisments;
using ConfigurationCommands = Pixelario.CMS.Application.Commands.Configurations;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;

namespace Pixelario.CMS.Web.Controllers
{
    public class FrontController : Controller
    {
        private readonly IMediator _mediator;

        public FrontController(IMediator mediator)
        {
            _mediator = mediator ?? throw new System.ArgumentNullException(nameof(mediator));
        }
        internal async Task<List<ConfigurationQueryModel>> GetCommonFrontConfigurations()
        {
            var keys = new string[] { "pages/menu/", "home/", "topics/menu/", "website/", "socialmedia/" };
            var configurations = await _mediator.Send(new ConfigurationCommands.ListCommand(
                filteredBy: null,
                keys: keys, configurationType: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                orderBy: 2,
                orderDirection: 0));
            return configurations;
        }

        internal async Task<HeaderViewModel> GetHeaderViewModel()
        {
            var websiteKeys = new string[] { "website/" };
            var websiteConfigurationQueryModels = await _mediator.Send(new ConfigurationCommands.ListCommand(
                filteredBy: null,
                keys: websiteKeys, configurationType: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                orderBy: 2,
                orderDirection: 0));
            var socialMediaKeys = new string[] { "socialmedia/" };
            var socialMediaConfigurationQueryModels = await _mediator.Send(new ConfigurationCommands.ListCommand(
                filteredBy: null,
                keys: socialMediaKeys, configurationType: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                orderBy: 2,
                orderDirection: 0));
            var menuViewModel = await this.GetMenuViewModel();
            return new HeaderViewModel(
                subscriptionEnable: this.SubscriptionsEnable,
                menuViewModel: menuViewModel,
                websiteConfigurationQueryModels: websiteConfigurationQueryModels,
                socialMediaConfigurationQueryModels: socialMediaConfigurationQueryModels
                );
        }

        internal async Task<MenuViewModel> GetMenuViewModel()
        {
            var informationEditionsOnMenu = await GetInformationEditionsOnMenu();
            var topicsOnMenu = await GetTopicsOnMenu();
            var especialEditionsOnMenu = await GeEspecialEditionsOnMenu();
            var keys = new string[] { "website/navbar/" };
            return new MenuViewModel(
                especialEditionsMenu: especialEditionsOnMenu,
                pagesMenu: informationEditionsOnMenu,
                topicsMenu: topicsOnMenu,
                configurationQueryModels: await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: keys, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0))
                );
        }

        private async Task<PagesMenuViewModel> GetInformationEditionsOnMenu()
        {
            return new PagesMenuViewModel(
                editionsQueryModels: await _mediator.Send(new EditionCommands.ListCommand(
                    filteredBy: null,
                    enabled: null,
                    atHome: null,
                    onMenu: true,
                    editionTypes: new List<EditionType> { EditionType.Information },
                    pagesOnEditionFilter: new PagesOnEditionQueryFilter(
                        pagesAtHome: null, pagesOnMenu: true),
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "e.[Order]",
                    orderDirection: "ASC"))
                );
        }

        private async Task<TopicsMenuViewModel> GetTopicsOnMenu()
        {
            var keys = new string[] { "topics/menu/" };
            return new TopicsMenuViewModel(
                topicsQueryModels: await _mediator.Send(new TopicCommands.ListCommand(
                    iDEdition: null,
                    filteredBy: null,
                    enabled: true,
                    atHome: null,
                    onMenu: true,
                    includeEnableSubTopics: null,
                    includeHomePageSubTopics: null,
                    includeOnMenuSubTopics: true,
                    subTopicColumnOrder: "OrderAtMenu",
                    subTopicOrderDirection: "ASC",
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "t.[Order]",
                    orderDirection: "ASC")),
                configurationQueryModels: await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: keys, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0))
                );
        }
        internal async Task<List<AdvertismentFrontModel>> GetFlyer(AdvertismentPlaceAtWeb placeAtWeb)
        {
            var advertismentList = new List<AdvertismentFrontModel>();
            foreach (var advertismentQueryModel in await _mediator.Send(new AdvertismentCommands.ListCommand(
                placeAtWeb: placeAtWeb.Id,
                enabled: true,
                published: true,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "a.[Order]",
                orderDirection: "ASC")))
            {
                advertismentList.Add(new AdvertismentFrontModel(
                    mappingMode: MappingMode.Atomic,
                    advertismentQueryModel: advertismentQueryModel));
            }
            return advertismentList;
        }

        private async Task<EspecialEditionsMenuViewModel> GeEspecialEditionsOnMenu()
        {
            var keys = new string[] { "editions/especial_menu/" };
            return new EspecialEditionsMenuViewModel(
                editionsQueryModels: await _mediator.Send(new EditionCommands.ListCommand(
                    filteredBy: null,
                    enabled: null,
                    atHome: null,
                    onMenu: true,
                    editionTypes: new List<EditionType> { EditionType.Especial_Content },
                    pagesOnEditionFilter: new PagesOnEditionQueryFilter(
                        pagesAtHome: null, pagesOnMenu: true),
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    columnOrder: "e.[Order]",
                    orderDirection: "ASC")),
                configurationQueryModels: await _mediator.Send(new ConfigurationCommands.ListCommand(
                    filteredBy: null,
                    keys: keys, configurationType: null,
                    rowIndex: 0,
                    rowCount: int.MaxValue,
                    orderBy: 2,
                    orderDirection: 0))
                );
        }

        internal bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }
    }
}