﻿using MediatR;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.Subscriptions.Application.Queries.Accounts;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdvertismentCommands = Pixelario.CMS.Application.Commands.Advertisments;
namespace Pixelario.CMS.Web.Controllers
{
    [RoutePrefix("Advertisments")]
    public class AdvertismentsController : FrontController
    {
        private ILogger _logger;
        private readonly IMediator _mediator;
        public AdvertismentsController(
            ILogger logger, IMediator mediator,
            IConstraintsQueries constraintsQueries,
            IAccountsQueries accountQueries) : base(mediator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }



        [Route("Widget/{iDAdvertisment}")]
        public async Task<ActionResult> Widget(int iDAdvertisment)
        {
            _logger.Information("Request started {0} {1}",
                Request.HttpMethod,
                Request.RawUrl);
            var advertismentQuery = await _mediator.Send(new AdvertismentCommands.GetCommand(
                iDAdvertisment: iDAdvertisment));
            if (advertismentQuery != null && advertismentQuery.Enabled)
            {
                var model = new WidgetMultimediaViewModel(
                    queryModel: advertismentQuery);
                return View(model);
            }
            else
            {
                _logger.Warning("Advertisment with id {0} not found", iDAdvertisment);
                throw new HttpException(404, "Advertisment multimedia not found");
            }
        }
    }
}