﻿using Autofac;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Cache.Advertisments;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Cache.Pages;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Queries.Advertisments;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.Queries.CacheRegisters;
using Pixelario.CMS.Application.Queries.Configurations;
using Pixelario.CMS.Application.Queries.Editions;
using Pixelario.CMS.Application.Queries.Pages;
using Pixelario.CMS.Application.Queries.Topics;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Infrastructure;
using Pixelario.CMS.Infrastructure.Repositories;
using Pixelario.Logs.Application.Queries.LogEvents;
using Pixelario.Subscriptions.Application.Queries.Accounts;
using Pixelario.Subscriptions.Application.Queries.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Pixelario.Subscriptions.Infrastructure;
using Pixelario.Subscriptions.Infrastructure.Repositories;
using Serilog;
using System.Runtime.Caching;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        public string ConnectionString { get; private set; }
        public string SubscriptionsConnectionString { get; private set; }
        public string LogsQueryString { get; private set; }
        public MemoryCache Cache { get; private set; }

        public ApplicationModule(string connectionString,
            string subscriptionsConnectionString,
            string logsQueryString)
        {
            this.ConnectionString = connectionString;
            this.SubscriptionsConnectionString = subscriptionsConnectionString;
            this.LogsQueryString = logsQueryString;
            this.Cache = new MemoryCache("CMS");

        }
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<CMSContext>().InstancePerRequest();
            #region Cache services
            builder.Register(c =>
            {
                var editionCacheService = new EditionCache(
                    cache: this.Cache);
                return editionCacheService;
            }).As<IEditionCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var configurationCacheService = new ConfigurationCache(
                    cache: this.Cache);
                return configurationCacheService;
            }).As<IConfigurationCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var configurationOptionCacheService = new ConfigurationOptionCache(
                    cache: this.Cache);
                return configurationOptionCacheService;
            }).As<IConfigurationOptionCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var topicCacheService = new TopicCache(
                    cache: this.Cache);
                return topicCacheService;
            }).As<ITopicCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var subTopicCacheService = new SubTopicCache(
                    cache: this.Cache);
                return subTopicCacheService;
            }).As<ISubTopicCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var advertismentCacheService = new AdvertismentCache(
                    cache: this.Cache);
                return advertismentCacheService;
            }).As<IAdvertismentCache>()
                .InstancePerRequest();

            builder.Register(c =>
            {
                var articleCacheService = new ArticleCache(
                    cache: this.Cache);
                return articleCacheService;
            }).As<IArticleCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var articleMultimediaCacheService = new ArticleMultimediaCache(
                    cache: this.Cache);
                return articleMultimediaCacheService;
            }).As<IArticleMultimediaCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var articleScriptCacheService = new ArticleScriptCache(
                    cache: this.Cache);
                return articleScriptCacheService;
            }).As<IArticleScriptCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var articleScriptItemCacheService = new ArticleScriptItemCache(
                    cache: this.Cache);
                return articleScriptItemCacheService;
            }).As<IArticleScriptItemCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var pageCacheService = new PageCache(
                    cache: this.Cache);
                return pageCacheService;
            }).As<IPageCache>()
                .InstancePerRequest();
            builder.Register(c =>
            {
                var pageMultimediaCacheService = new PageMultimediaCache(
                    cache: this.Cache);
                return pageMultimediaCacheService;
            }).As<IPageMultimediaCache>()
                .InstancePerRequest();
            #endregion
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var configurationsQueries = new ConfigurationsQueries(
                    logger: logger,
                    this.ConnectionString);
                return configurationsQueries;
            }).As<IConfigurationsQueries>()
                .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var configurationOptionQueries = new ConfigurationOptionQueries(
                    logger: logger,
                    this.ConnectionString);
                return configurationOptionQueries;
            }).As<IConfigurationOptionQueries>()
                .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var editionsQueries = new EditionsQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return editionsQueries;
            }).As<IEditionsQueries>()
                .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var articlesQueries = new ArticlesQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return articlesQueries;
            }).As<IArticlesQueries>()
                .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var articleMultimediasQueries = new ArticleMultimediasQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return articleMultimediasQueries;
            }).As<IArticleMultimediasQueries>()
               .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var articleScriptsQueries = new ArticleScriptsQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return articleScriptsQueries;
            }).As<IArticleScriptsQueries>()
                .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var articleScriptItemsQueries = new ArticleScriptItemsQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return articleScriptItemsQueries;
            }).As<IArticleScriptItemsQueries>()
                .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var topicQueries = new TopicsQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return topicQueries;
            }).As<ITopicsQueries>()
                .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var subTopicQueries = new SubTopicsQueries(logger: logger,
                        connectionString: ConnectionString);
                return subTopicQueries;
            }).As<ISubTopicsQueries>()
                    .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var pageQueries = new PagesQueries(
                    logger: logger,
                    connectionString: ConnectionString);
                return pageQueries;
            }).As<IPagesQueries>()
                    .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var pageMultimediaQueries = new PageMultimediaQueries(
                     logger: logger,
                    connectionString: ConnectionString);
                return pageMultimediaQueries;
            }).As<IPageMultimediaQueries>()
                       .InstancePerLifetimeScope();

            builder.Register(c =>
                    {
                        var logger = c.Resolve<ILogger>();
                        var advertismentQueries = new AdvertismentsQueries(
                             logger: logger,
                            connectionString: ConnectionString);
                        return advertismentQueries;
                    }).As<IAdvertismentsQueries>()
                   .InstancePerLifetimeScope();

            builder.RegisterType<EditionRepository>()
                            .As<IEditionRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<ArticleRepository>()
                            .As<IArticleRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<ArticleScriptRepository>()
                            .As<IArticleScriptRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<TopicRepository>()
                            .As<ITopicRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<PageRepository>()
                            .As<IPageRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<ConfigurationRepository>()
                            .As<IConfigurationRepository>()
                            .InstancePerLifetimeScope();
            builder.RegisterType<AdvertismentRepository>()
                             .As<IAdvertismentRepository>()
                             .InstancePerLifetimeScope();
            builder.RegisterType<ImageServices>()
                            .As<IImageServices>()
                            .SingleInstance();
            builder.RegisterType<SubscriptionsContext>().InstancePerRequest();
            builder.RegisterType<AccountRepository>()
                .As<IAccountRepository>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ConstraintRepository>()
               .As<IConstraintRepository>()
               .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var cacheRegisterQueries = new CacheRegisterQueries(
                    logger: logger,
                    cache: this.Cache);
                return cacheRegisterQueries;
            }).As<ICacheRegisterQueries>()
                .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var accountsQueries = new AccountsQueries(
                    logger: logger,
                    connectionString: SubscriptionsConnectionString);
                return accountsQueries;
            }).As<IAccountsQueries>()
                .InstancePerLifetimeScope();
            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var constraintsQueries = new ConstraintsQueries(
                     logger: logger,
                    connectionString: SubscriptionsConnectionString);
                return constraintsQueries;
            }).As<IConstraintsQueries>()
               .InstancePerLifetimeScope();

            builder.Register(c =>
            {
                var logger = c.Resolve<ILogger>();
                var logEventsQueries = new LogEventsQueries(
                    logger: logger,
                    connectionString: this.LogsQueryString);
                return logEventsQueries;
            }).As<ILogEventsQueries>()
              .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}