﻿using Autofac;
using Pixelario.CMS.Web.Services.Sitemap;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public class ServicesModule : Autofac.Module
    {
        public ServicesModule()
        {
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SitemapService>()
                .As<ISitemapService>()
                .InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}