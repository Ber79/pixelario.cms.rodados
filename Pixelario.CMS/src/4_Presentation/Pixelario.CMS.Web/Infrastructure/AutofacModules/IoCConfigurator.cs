﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public static class IoCConfigurator
    {
        /// <summary>
        /// Set the IsDebug property if is in mode Debug
        /// </summary>
        /// <returns></returns>
        private static bool SetIsDebug()
        {
#if DEBUG
            return true;
#else
            return false;
#endif

        }
        private static bool SubscriptionsEnable
        {
            get
            {
                var enable = false;
                var appSettingValue = ConfigurationManager.AppSettings["enable:subscriptions"];
                if (appSettingValue != null && !string.IsNullOrEmpty(appSettingValue))
                {
                    bool.TryParse(appSettingValue, out enable);
                }
                return enable;
            }
        }

        public static void ConfigureDependencyInjection()
        {
            var connectionStringSetting = ConfigurationManager.ConnectionStrings["CMSContext"];
            if (connectionStringSetting == null || string.IsNullOrEmpty(connectionStringSetting.ConnectionString))
                throw new Exception("Fatal error: missing connecting string in web.config file");
            var queryString = connectionStringSetting.ConnectionString;

            #region Get Subscriptions Query String
            var subscriptionsQueryString = "";
            var subscriptionsConnectionStringSetting = 
                ConfigurationManager.ConnectionStrings["SubscriptionsContext"];
            if (SubscriptionsEnable && (subscriptionsConnectionStringSetting == null ||
                string.IsNullOrEmpty(subscriptionsConnectionStringSetting.ConnectionString)))
            {
                throw new Exception("Fatal error: missing connecting string in web.config file");
            }
            subscriptionsQueryString = subscriptionsConnectionStringSetting.ConnectionString;
            #endregion
            #region Get Logs Query String
            var logsQueryString = "";
            var logsConnectionStringSetting =
                ConfigurationManager.ConnectionStrings["LogsContext"];
            if (logsConnectionStringSetting == null ||
                string.IsNullOrEmpty(logsConnectionStringSetting.ConnectionString))
            {
                throw new Exception("Fatal error: missing connecting string in web.config file");
            }
            logsQueryString = logsConnectionStringSetting.ConnectionString;
            #endregion
            var builder = new ContainerBuilder();
            builder.RegisterModule(new LoggingModule(
                isDebug: SetIsDebug(),
                logsQueryString: logsQueryString));
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new MediatorModule(
                subscriptionsEnable: SubscriptionsEnable));
            builder.RegisterModule(new ApplicationModule(
                connectionString: queryString,
                subscriptionsConnectionString: subscriptionsQueryString,
                logsQueryString: logsQueryString
                ));
            builder.RegisterModule(new IdentityModule());
            builder.RegisterModule(new ServicesModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(
                new AutofacDependencyResolver(
                container));
            GlobalConfiguration.Configuration.DependencyResolver = 
                new AutofacWebApiDependencyResolver((IContainer)container); //Set the WebApi DependencyResolver
        }
    }
}