﻿using Autofac;
using MediatR;
using Pixelario.CMS.Application.DomainEventHandlers.Editions;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
using AccountCommands = Pixelario.Subscriptions.Application.Commands.Accounts;
using LogEventCommands = Pixelario.Logs.Application.Commands.LogEvents;
using System.Reflection;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public class MediatorModule : Autofac.Module
    {
        public bool SubscriptionsEnable { get; set; }
        public MediatorModule(bool subscriptionsEnable)
        {
         
            SubscriptionsEnable = subscriptionsEnable;
        }
        protected override void Load(ContainerBuilder builder)
        {
            // Registro IMediator
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
                .AsImplementedInterfaces();
            //Registro los commandos (tomo como tipo el commando de creacion de cuenta)
            builder.RegisterAssemblyTypes(typeof(EditionCommands.AddCommand).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>));
            //Registro los eventos (tomo como tipo el evento de cambio de orden en ediciones)
            builder.RegisterAssemblyTypes(typeof(OrderChangedDomainEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>));
            if(SubscriptionsEnable)
            {
                //Registro los commandos (tomo como tipo el commando de creacion de cuenta)
                builder.RegisterAssemblyTypes(typeof(AccountCommands.AddCommand).GetTypeInfo().Assembly)
                    .AsClosedTypesOf(typeof(IRequestHandler<,>));
            }

            //Registro los commandos del Proyecto Pixelario.logs.Application
            builder.RegisterAssemblyTypes(typeof(LogEventCommands.CountCommand).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>));

            // Registro ServiceFactory
            builder.Register<ServiceFactory>(context =>
            {
                var componentContext = context.Resolve<IComponentContext>();
                return t =>
                {
                    object o;
                    return componentContext.TryResolve(t, out o) ? o : null;
                };
            });

        }
    }

}