﻿using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure;
using Pixelario.Identity.Infrastructure.Managers;
using Pixelario.Identity.Infrastructure.Stores;
using System.Web;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public class IdentityModule : Autofac.Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<IdentityDbContext>().InstancePerRequest();
            builder.RegisterType<ApplicationIdentityDbContext>();
            builder.RegisterType<ApplicationUserStore>()
                .As<IUserStore<ApplicationUser, int>>();
            builder.RegisterType<ApplicationRoleStore>()
                .As<IRoleStore<ApplicationRole, int>>();
            builder.RegisterType<ApplicationRoleStore>()
                .As<IRoleStore<ApplicationRole, int>>();

            builder.RegisterType<ApplicationUserManager>();
            builder.RegisterType<ApplicationRoleManager>();
            builder.RegisterType<ApplicationSignInManager>();
            builder.Register<IAuthenticationManager>(
                c =>HttpContext.Current.GetOwinContext().Authentication
            ).InstancePerRequest();
           
            
            
            builder.Register<IDataProtectionProvider>(
                c => HttpContext.Current.GetOwinContext().Get<AppBuilderProvider>().Get().GetDataProtectionProvider()
            ).InstancePerRequest();

            base.Load(builder);
        }

    }
}