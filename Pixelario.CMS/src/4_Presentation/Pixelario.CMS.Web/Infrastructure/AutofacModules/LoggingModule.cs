﻿using Autofac;
using Serilog;

using System.Configuration;
using System.IO;
using System.Web.Hosting;

namespace Pixelario.CMS.Web.Infrastructure.AutofacModules
{
    public class LoggingModule : Autofac.Module
    {
        private bool IsDebug { get; set; }
        private string LogsConnectionString { get; set; }
        public LoggingModule(bool isDebug, string logsQueryString)
        {
            this.IsDebug = isDebug;
            this.LogsConnectionString = logsQueryString;
        }
        protected override void Load(ContainerBuilder builder)
        {
            string logsPath = HostingEnvironment.MapPath("~/logs");
            if (!Directory.Exists(logsPath))
                Directory.CreateDirectory(logsPath);
            var logFilename = string.Format("~/logs/log-{0}-.txt",
                IsDebug ? "debug" : "release");
            var appName = ConfigurationManager.AppSettings["app:name"];
            if (appName != null && appName.Length >= 3)
            {
                logFilename = string.Format("~/logs/{0}-log-{1}-.txt",
                    appName,
                    IsDebug ? "debug" : "release");
            }

            if (IsDebug)
            {
                builder.Register<ILogger>((c, p) =>
                {
                    var logger = new LoggerConfiguration()
                        .Enrich.WithHttpRequestId()
                        .MinimumLevel.Debug()
                        .WriteTo.Debug()
                        .WriteTo.File(HostingEnvironment.MapPath(logFilename),
                            rollingInterval: RollingInterval.Day,
                            outputTemplate: "{Timestamp:HH:mm} [{Level}] ({HttpRequestId}) {Message}{NewLine}{Exception}",
                            shared: true)
                        .WriteTo.MSSqlServer(
                            connectionString: this.LogsConnectionString,
                            tableName: "Logs"
                        )
                        .CreateLogger();
                    logger.Information("Start logging");
                    return logger;
                }).SingleInstance();
            }
            else
            {
                builder.Register<ILogger>((c, p) =>
                {
                    var logger = new LoggerConfiguration()
                        .Enrich.WithHttpRequestId()
                        .MinimumLevel.Error()
                        .WriteTo.File(HostingEnvironment.MapPath(logFilename),
                            rollingInterval: RollingInterval.Day,
                            outputTemplate: "{Timestamp:HH:mm} [{Level}] ({HttpRequestId}) {Message}{NewLine}{Exception}",
                            shared: true)
                        .WriteTo.MSSqlServer(
                            connectionString: this.LogsConnectionString,
                            tableName: "Logs"
                        )
                        .CreateLogger();
                    logger.Information("Start logging");
                    return logger;
                }).SingleInstance();
            }
            base.Load(builder);
        }
    }
}