﻿using Serilog;
using System.Web.Mvc;

namespace Pixelario.CMS.Web.Infrastructure.GlobalFilters
{
    public class LogErrorsAtribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext != null && filterContext.Exception != null)
            {
                var logger = DependencyResolver.Current.GetService<ILogger>();
                if (logger != null)
                {
                    string controller = filterContext.RouteData.Values["controller"].ToString();
                    string action = filterContext.RouteData.Values["action"].ToString();
                    string logMessage = string.Format("Exception throwed on {0}Controller.{1}", controller, action);
                    logger.Error(filterContext.Exception, logMessage);
                }
            }
        }
    }
}