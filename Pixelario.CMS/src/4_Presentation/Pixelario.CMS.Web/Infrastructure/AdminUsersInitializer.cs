﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure;
using Pixelario.Identity.Infrastructure.Managers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Pixelario.Identity.Infrastructure.Stores;

namespace Pixelario.CMS.Web.Infrastructure
{
    public class AdminUsersInitializer 
    {
        public static void Seed()
        {
            // Initialize default identity roles            
            var dbContext = new ApplicationIdentityDbContext();
            var roleManager = new ApplicationRoleManager(
                new ApplicationRoleStore(dbContext));
            
            List<ApplicationRole> identityRoles = new List<ApplicationRole>();
            identityRoles.Add(new ApplicationRole() { Name = "Administrador" });
            identityRoles.Add(new ApplicationRole() { Name = "Super Administrador" });
            identityRoles.Add(new ApplicationRole() { Name = "Subscriptor" });
            foreach (ApplicationRole role in identityRoles)
            {
                if (!roleManager.RoleExists(role.Name))
                {
                    roleManager.Create(role);
                }
            }

            // Initialize default user
            var userManager = new ApplicationUserManager(
                new ApplicationUserStore(dbContext));
            var userFinded = userManager.FindByName("admin");
            if (userFinded != null)
            {
                if(userFinded.LockoutEnabled)
                {
                    userFinded.LockoutEnabled = false;
                }
                if(!userManager.IsInRole(userFinded.Id, "Super Administrador"))
                {
                    userManager.AddToRole(userFinded.Id, "Super Administrador");
                }
                userManager.Update(userFinded);
            }
            else
            {
                var adminUser = new ApplicationUser()
                {
                    Email = "admin@pixelario.com.ar",
                    UserName = "admin"
                };
                userManager.Create(adminUser, "control");
                userManager.AddToRole(adminUser.Id, "Super Administrador");
            }
        }
    }
}