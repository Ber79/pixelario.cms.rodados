﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using System.Xml.Linq;
using PageCommands = Pixelario.CMS.Application.Commands.Pages;
using TopicCommands = Pixelario.CMS.Application.Commands.Topics;
using EditionCommands = Pixelario.CMS.Application.Commands.Editions;
using ArticleCommands = Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Web.Models.Frontend;
using Pixelario.CMS.Application.QueryFilters.Editions;

namespace Pixelario.CMS.Web.Services.Sitemap
{
    public class SitemapService : ISitemapService
    {
        private string AppDomain = ConfigurationManager.AppSettings["app:domain"];
        private readonly IMediator _mediator;
        public SitemapService(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public async Task<string> GetSitemapDocument()
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");
            var siteMapNodes = this.HomeNodes(
                xmlns: xmlns);
            siteMapNodes.AddRange(await this.ArticleNodes(
                xmlns: xmlns));
            siteMapNodes.AddRange(await this.PageNodes(
                xmlns: xmlns));
            siteMapNodes.AddRange(await this.TopicNodes(
                xmlns: xmlns));
            siteMapNodes.AddRange(await this.EspecialEditionsNodes(
                xmlns: xmlns));
            foreach (XElement element in siteMapNodes)
            {
                root.Add(element);
            }
            XDocument document = new XDocument(root);
            document.Declaration = new XDeclaration("1.0", "UTF-8", null);
            return document.Declaration.ToString() + document.ToString();
        }

        private List<XElement> HomeNodes(XNamespace xmlns)
        {
            var homeXElements = new List<XElement>();
            var homeNodes = new List<SitemapNode>() {
                new SitemapNode()
                {
                    Url= string.Format("http://{0}/", this.AppDomain),
                    Priority=1,
                    Frequency = SitemapFrequency.Daily
                },
                new SitemapNode()
                {
                    Url= string.Format("http://{0}/Contact", this.AppDomain),
                    Priority=1,
                    Frequency = SitemapFrequency.Yearly
                },
                new SitemapNode()
                {
                    Url= string.Format("http://{0}/Search", this.AppDomain),
                    Priority=1,
                    Frequency = SitemapFrequency.Yearly
                }
            };
            foreach (SitemapNode sitemapNode in homeNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc",
                        Uri.EscapeUriString(sitemapNode.Url)),
                        sitemapNode.LastModified == null ? null : new XElement(
                            xmlns + "lastmod",
                            sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                        sitemapNode.Frequency == null ? null : new XElement(
                            xmlns + "changefreq",
                            sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                        sitemapNode.Priority == null ? null : new XElement(
                            xmlns + "priority",
                            sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                homeXElements.Add(urlElement);
            }
            return homeXElements;

        }
        private async Task<List<XElement>> ArticleNodes(XNamespace xmlns)
        {
            var articleNodes = new List<SitemapNode>();
            var articleXElements = new List<XElement>();
            foreach (var article in await _mediator.Send(new ArticleCommands.ListCommand(
                iDEdition: null,
                iDTopic: null,
                iDSubTopic: null,
                enabled: true,
                atHome: null,
                placesAtHome: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "a.IDArticle",
                orderDirection: "ASC")))
            {
                articleNodes.Add(new SitemapNode()
                {
                    Url = string.Format("http://{0}{1}",
                        this.AppDomain, article.URL),
                    Priority = 1,
                    Frequency = SitemapFrequency.Monthly
                });
            };
            foreach (SitemapNode sitemapNode in articleNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc",
                        Uri.EscapeUriString(sitemapNode.Url)),
                        sitemapNode.LastModified == null ? null : new XElement(
                            xmlns + "lastmod",
                            sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                        sitemapNode.Frequency == null ? null : new XElement(
                            xmlns + "changefreq",
                            sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                        sitemapNode.Priority == null ? null : new XElement(
                            xmlns + "priority",
                            sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                articleXElements.Add(urlElement);
            }
            return articleXElements;
        }
        private async Task<List<XElement>> PageNodes(XNamespace xmlns)
        {
            var pageNodes = new List<SitemapNode>();
            var pageXElements = new List<XElement>();
            var pageQueryModels = await _mediator.Send(new PageCommands.ListCommand(
                iDEdition: null,
                enabled: true,
                atHome: null,
                onMenu: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "p.IDPage",
                orderDirection: "ASC"));
            foreach (var page in pageQueryModels)
            {
                pageNodes.Add(new SitemapNode()
                {
                    Url = string.Format("http://{0}{1}",
                        this.AppDomain, page.URL),
                    Priority = 1,
                    Frequency = SitemapFrequency.Monthly
                });
            };
            foreach (SitemapNode sitemapNode in pageNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc",
                        Uri.EscapeUriString(sitemapNode.Url)),
                        sitemapNode.LastModified == null ? null : new XElement(
                            xmlns + "lastmod",
                            sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                        sitemapNode.Frequency == null ? null : new XElement(
                            xmlns + "changefreq",
                            sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                        sitemapNode.Priority == null ? null : new XElement(
                            xmlns + "priority",
                            sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                pageXElements.Add(urlElement);
            }
            return pageXElements;
        }
        private async Task<List<XElement>> TopicNodes(XNamespace xmlns)
        {
            var topicNodes = new List<SitemapNode>();
            var topicXElements = new List<XElement>();
            foreach (var topic in await _mediator.Send(new TopicCommands.ListCommand(
                iDEdition: null,
                includeEnableSubTopics: true,
                includeHomePageSubTopics: null,
                includeOnMenuSubTopics: null,
                subTopicColumnOrder: "IDSubTopic",
                subTopicOrderDirection: "ASC",
                enabled: true,
                atHome: null,
                onMenu: null,
                filteredBy: null,
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "t.IDTopic",
                orderDirection: "ASC")))
            {
                topicNodes.Add(new SitemapNode()
                {
                    Url = string.Format("http://{0}{1}",
                        this.AppDomain, topic.URL),
                    Priority = 1,
                    Frequency = SitemapFrequency.Daily
                });
                foreach (var subtopic in topic.SubTopics)
                {
                    topicNodes.Add(new SitemapNode()
                    {
                        Url = string.Format("http://{0}{1}",
                        this.AppDomain, subtopic.URL),
                        Priority = 1,
                        Frequency = SitemapFrequency.Daily
                    });
                }
            };
            foreach (SitemapNode sitemapNode in topicNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc",
                        Uri.EscapeUriString(sitemapNode.Url)),
                        sitemapNode.LastModified == null ? null : new XElement(
                            xmlns + "lastmod",
                            sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                        sitemapNode.Frequency == null ? null : new XElement(
                            xmlns + "changefreq",
                            sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                        sitemapNode.Priority == null ? null : new XElement(
                            xmlns + "priority",
                            sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                topicXElements.Add(urlElement);
            }
            return topicXElements;
        }
        private async Task<List<XElement>> EspecialEditionsNodes(XNamespace xmlns)
        {
            var especialEditionNodes = new List<SitemapNode>();
            var topicXElements = new List<XElement>();
            
            var editionQueryModels = await _mediator.Send(new EditionCommands.ListCommand(
                filteredBy: null,
                enabled: true,
                atHome: null,
                onMenu: null,
                editionTypes: new List<EditionType> { EditionType.Especial_Content },
                pagesOnEditionFilter: new PagesOnEditionQueryFilter(
                    pagesAtHome: null, pagesOnMenu: null),
                rowIndex: 0,
                rowCount: int.MaxValue,
                columnOrder: "e.IDEdition",
                orderDirection: "ASC"));
            if (editionQueryModels != null)
            {
                foreach (var editionsQueryModel in editionQueryModels)
                {
                    var edition = new EditionViewModel(
                        mappingMode: MappingMode.Summary,
                        editionsQueryModel: editionsQueryModel
                        );
                    especialEditionNodes.Add(new SitemapNode()
                    {
                        Url = string.Format("http://{0}{1}",
                            this.AppDomain, edition.URL),
                        Priority = 1,
                        Frequency = SitemapFrequency.Daily
                    });

                };
                foreach (SitemapNode sitemapNode in especialEditionNodes)
                {
                    XElement urlElement = new XElement(
                        xmlns + "url",
                        new XElement(xmlns + "loc",
                            Uri.EscapeUriString(sitemapNode.Url)),
                            sitemapNode.LastModified == null ? null : new XElement(
                                xmlns + "lastmod",
                                sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                            sitemapNode.Frequency == null ? null : new XElement(
                                xmlns + "changefreq",
                                sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                            sitemapNode.Priority == null ? null : new XElement(
                                xmlns + "priority",
                                sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                    topicXElements.Add(urlElement);
                }
            }
            return topicXElements;
        }

    }
}