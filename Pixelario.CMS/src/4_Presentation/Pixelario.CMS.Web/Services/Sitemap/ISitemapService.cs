﻿using System.Threading.Tasks;
namespace Pixelario.CMS.Web.Services.Sitemap
{
    public interface ISitemapService
    {
        Task<string> GetSitemapDocument();
    }
}
