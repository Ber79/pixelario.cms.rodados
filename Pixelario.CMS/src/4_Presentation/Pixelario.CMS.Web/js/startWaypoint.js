﻿var startWaypoint = function (ref) {
    var waypoint = new Waypoint({
        element: $('#infinite-more-link'),
        offset: '100%',
        handler: function (direction) {
            var $this = this;
            if (direction == 'down') {
                var moreLink = $('#infinite-more-link');
                $('#infinite-more-link').html('<img src="/img/Load.gif" style="height:30px;"/>');
                $.ajax({
                    url: moreLink.attr('href'),
                }).done(function (data) {
                    $.each(data.Articles, function (i, article) {
                        var $header = $('<header></header');
                        if (article.Topics && article.Topics.length > 0) {
                            $.each(article.Topics, function (i, topic) {
                                $header.append('<a href="' + topic.URL + '?ref=' + ref + '" title="' + topic.Title + '">' + topic.Title + '</a>')
                            });
                        }
                        var $figure = $('<figure><a href="' + article.URL + '?ref=' + ref + '" title="' + article.Title + '"><img src="' + article.HomeImage + '" alt="' + article.Title + '" title="' + article.Title + '" /></a></figure>');
                        var $aside = $('<aside><h1><a href="' + article.URL + '?ref=' + ref + '" title="' + article.Title + '">' + article.Title + '</a></h1></aside>');
                        var $article = $('<article class="' + article.CssClass + '"></article>')
                        $article.append($header).append($figure).append($aside);
                        var $li = $('<li class="col-sm-6"></li>');
                        $li.append($article);
                        $('#central-columns').append($li);
                    });
                    $this.destroy();
                    if (data.NextURL && data.NextURL.length > 0) {
                        $('#infinite-more-link').attr('href', data.NextURL);
                        $('#infinite-more-link').html('Siguiente');
                        startWaypoint(ref);
                    } else {
                        $('#infinite-more-link').attr('href', "#");
                        $('#infinite-more-link').html('');
                    }
                });
            }
        }
    });
};
