﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure;
using Pixelario.Identity.Infrastructure.Managers;
using Pixelario.Identity.Infrastructure.Stores;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Web;

namespace Pixelario.Identity.Infrastructure
{
    public class ApplicationIdentityDbContextInitializer :
        DropCreateDatabaseIfModelChanges<ApplicationIdentityDbContext>
    {
        protected override void Seed(ApplicationIdentityDbContext context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }
        public static void InitializeIdentityForEF(ApplicationIdentityDbContext db)
        {
            var roleStore = new ApplicationRoleStore(db);
            var roleManager = new ApplicationRoleManager(roleStore);

            var userStore = new ApplicationUserStore(db);
            var userManager = new ApplicationUserManager(userStore);

            const string name = "admin";
            const string email = "admin@pixelario.com.ar";
            const string password = "control";
            const string roleName = "Super Administrador";

            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new ApplicationRole(roleName);
                var roleResult = roleManager.Create(role);
            }
            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser {
                    UserName=name,
                    Email = email
                };
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {                
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }
}
