﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pixelario.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace Pixelario.Identity.Infrastructure.Stores
{
    public class ApplicationUserStore :
        UserStore<ApplicationUser, ApplicationRole, int,
            ApplicationUserLogin, ApplicationUserRole,
            ApplicationUserClaim>,
        IUserStore<ApplicationUser, int>, IDisposable
    {
        public ApplicationUserStore()
            : this(new ApplicationIdentityDbContext())
                  //IdentityDbContext("IdentityDbContext"))
        {
            base.DisposeContext = true;
        }
        public ApplicationUserStore(DbContext context)
            : base(context)
        {

        }
    }
}
