﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pixelario.Identity.Domain;
using System;
using System.Data.Entity;

namespace Pixelario.Identity.Infrastructure.Stores
{
    public class ApplicationRoleStore :
        RoleStore<ApplicationRole, int,
            ApplicationUserRole>,
        IQueryableRoleStore<ApplicationRole, int>,
        IRoleStore<ApplicationRole, int>, IDisposable
    {
        public ApplicationRoleStore()
            : base(new ApplicationIdentityDbContext())
        {
            base.DisposeContext = true;
        }
        public ApplicationRoleStore(DbContext context)
            : base(context)
        {
        }
    }
}
