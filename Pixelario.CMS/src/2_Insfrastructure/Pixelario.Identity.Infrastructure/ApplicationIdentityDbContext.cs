﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Pixelario.Identity.Domain;
using System;

namespace Pixelario.Identity.Infrastructure
{
    public class ApplicationIdentityDbContext : 
        IdentityDbContext<ApplicationUser, ApplicationRole, int,
            ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationIdentityDbContext() : 
            base("name=ApplicationIdentityConnectionString")
        {
            Database.Initialize(force: true);
        }
        public static ApplicationIdentityDbContext Create()
        {
            var context = new ApplicationIdentityDbContext();
            return context;
        }
        static ApplicationIdentityDbContext()
        {
            var debug = true;
            if (debug)
                Database.SetInitializer<ApplicationIdentityDbContext>(
                    strategy: new ApplicationIdentityDbContextInitializer());
            else
                Database.SetInitializer<ApplicationIdentityDbContext>(
                    strategy: null);

        }
    }
}
