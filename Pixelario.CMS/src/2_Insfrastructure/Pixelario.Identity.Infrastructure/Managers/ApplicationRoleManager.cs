﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure;
using Pixelario.Identity.Infrastructure.Stores;
using System;

namespace Pixelario.Identity.Infrastructure.Managers
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, int>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, int> roleStore) 
            : base(roleStore)
        {
        }       
    }
}
