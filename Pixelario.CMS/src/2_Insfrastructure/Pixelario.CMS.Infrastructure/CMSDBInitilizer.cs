﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using System.Data.Entity;
using System.Linq;

namespace Pixelario.CMS.Infrastructure
{
    public class CMSDBInitilizer : CreateDatabaseIfNotExists<CMSContext>
    {
        protected override void Seed(CMSContext context)
        {
            //if (!context.Configurations.Any(c=> c.Key=="PAGE-MENU-TITLE"))
            //{
            //    context.Configurations.Add(new Configuration(
            //        createdBy: "seed",
            //        title: "Texto del menú de paginas.",
            //        key: "PAGE-MENU-TITLE",
            //        state: "Páginas"
            //        ));
            //}                
            base.Seed(context);
        }
    }
}