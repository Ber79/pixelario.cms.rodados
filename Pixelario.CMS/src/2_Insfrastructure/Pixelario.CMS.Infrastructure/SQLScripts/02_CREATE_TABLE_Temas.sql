USE [CCET1]
GO

/****** Object:  Table [dbo].[Temas]    Script Date: 4/4/2019 12:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Temas](
	[IDTema] [int] IDENTITY(1,1) NOT NULL,
	[CreadoPor] [varchar](256) NOT NULL,
	[FechaDeCreacion] [datetime] NOT NULL,
	[Titulo] [varchar](128) NOT NULL,
	[Sumario] [varchar](4000) NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Habilitado] [bit] NOT NULL,
	[Portada] [bit] NOT NULL,
	[Orden] [int] NOT NULL,
	[ImagenDePortada] [varchar](256) NULL,
 CONSTRAINT [PK_Temas] PRIMARY KEY CLUSTERED 
(
	[IDTema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Temas] ADD  CONSTRAINT [DF_Temas_Habilitado]  DEFAULT ((0)) FOR [Habilitado]
GO

ALTER TABLE [dbo].[Temas] ADD  CONSTRAINT [DF_Temas_Portada]  DEFAULT ((0)) FOR [Portada]
GO

ALTER TABLE [dbo].[Temas] ADD  CONSTRAINT [DF_Temas_Orden]  DEFAULT ((0)) FOR [Orden]
GO


