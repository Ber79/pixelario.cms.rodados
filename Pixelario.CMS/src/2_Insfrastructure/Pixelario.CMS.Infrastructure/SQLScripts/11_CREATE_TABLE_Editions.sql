USE [CCET1]
GO

/****** Object:  Table [dbo].[Editions]    Script Date: 28/6/2019 15:54:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Editions](
	[IDEdition] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Title] [varchar](128) NOT NULL,
	[Summary] [varchar](4000) NOT NULL,
	[EditionType] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[HomeImage] [varchar](256) NULL,
	[Keywords] [varchar](256) NULL,
 CONSTRAINT [PK_Editions] PRIMARY KEY CLUSTERED 
(
	[IDEdition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Editions] ADD  CONSTRAINT [DF_Editions_EditionType]  DEFAULT ((0)) FOR [EditionType]
GO

ALTER TABLE [dbo].[Editions] ADD  CONSTRAINT [DF_Editions_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Editions] ADD  CONSTRAINT [DF_Editions_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[Editions] ADD  CONSTRAINT [DF_Editions_Order]  DEFAULT ((0)) FOR [Order]
GO


ALTER TABLE Editions
ADD [OnMenu] [bit] NOT NULL DEFAULT(0),
	[OrderAtMenu] [int] NOT NULL DEFAULT(0)