USE [CCET1]
GO

/****** Object:  Table [dbo].[Configurations]    Script Date: 10/5/2019 11:58:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Configurations](
	[IDConfiguration] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Key] [varchar](255) NOT NULL,
	[State] [varchar](255) NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_Configurations] PRIMARY KEY CLUSTERED 
(
	[IDConfiguration] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Configurations]
ADD Roles [varchar](255) NULL
GO
