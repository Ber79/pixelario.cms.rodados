USE [CCET1]
GO

/****** Object:  Table [dbo].[PageEditions]    Script Date: 9/7/2019 11:02:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PageEditions](
	[IDPage] [int] NOT NULL,
	[IDEdition] [int] NOT NULL,
 CONSTRAINT [PK_PageEditions] PRIMARY KEY CLUSTERED 
(
	[IDPage] ASC,
	[IDEdition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PageEditions]  WITH CHECK ADD  CONSTRAINT [FK_PageEditions_Editions] FOREIGN KEY([IDEdition])
REFERENCES [dbo].[Editions] ([IDEdition])
GO

ALTER TABLE [dbo].[PageEditions] CHECK CONSTRAINT [FK_PageEditions_Editions]
GO

ALTER TABLE [dbo].[PageEditions]  WITH CHECK ADD  CONSTRAINT [FK_PageEditions_Pages] FOREIGN KEY([IDPage])
REFERENCES [dbo].[Pages] ([IDPage])
GO

ALTER TABLE [dbo].[PageEditions] CHECK CONSTRAINT [FK_PageEditions_Pages]
GO


