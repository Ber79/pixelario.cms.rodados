USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleScript]    Script Date: 9/6/2020 19:31:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleScriptItems](
	[IDArticleScriptItem] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[IDMultimediaType] [int] NOT NULL,
	[IDArticleScript] [int] NOT NULL,
	[Title] [varchar](256) NOT NULL,
	[Code] [ntext] NULL,
	[Enabled] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_CTL_ArticleScriptItems] PRIMARY KEY CLUSTERED 
(
	[IDArticleScriptItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleScriptItems] ADD  CONSTRAINT [DF_ArticleScriptItems_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[ArticleScriptItems]  WITH CHECK ADD  CONSTRAINT [FK_ArticleScriptItems_ArticleScripts] FOREIGN KEY([IDArticleScript])
REFERENCES [dbo].[ArticleScripts] ([IDArticleScript])
GO

ALTER TABLE [dbo].[ArticleScriptItems] CHECK CONSTRAINT [FK_ArticleScriptITems_ArticleScripts]
GO

