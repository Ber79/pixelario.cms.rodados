USE [CCET1]
GO

/****** Object:  Table [dbo].[BLG_Articulos]    Script Date: 16/3/2019 19:56:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Articulos](
	[IDArticulo] [int] IDENTITY(1,1) NOT NULL,
	[FechaDeCreacion] [datetime] NOT NULL,
	[CreadoPor] [nvarchar](256) NOT NULL,
	[IDEdicion] [int] NULL,
	[Titulo] [varchar](128) NOT NULL,
	[Sumario] [varchar](4000) NULL,
	[Cuerpo] [ntext] NOT NULL,
	[Habilitado] [bit] NOT NULL,
	[Portada] [bit] NOT NULL,
	[ImagenDePortada] [varchar](256) NULL,
	[ImagenVertical] [varchar](256) NULL,
	[ImagenHorizontal] [varchar](256) NULL,
	[Orden] [int] NOT NULL,
	[Posicion] [int] NOT NULL,
	[Keywords] [varchar](256) NULL,
	[OrdenEnEdicion] [int] NOT NULL,
 CONSTRAINT [PK_Articulos] PRIMARY KEY CLUSTERED 
(
	[IDArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_Habilitado]  DEFAULT ((0)) FOR [Habilitado]
GO

ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_Portada]  DEFAULT ((0)) FOR [Portada]
GO

ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_Orden]  DEFAULT ((0)) FOR [Orden]
GO

ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_Posicion]  DEFAULT ((0)) FOR [Posicion]
GO

ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [DF_Articulos_OrdenEnEdicion]  DEFAULT ((0)) FOR [OrdenEnEdicion]
GO

ALTER TABLE [dbo].[Articulos]  WITH CHECK ADD  CONSTRAINT [FK_Articulos_Ediciones] FOREIGN KEY([IDEdicion])
REFERENCES [dbo].[Ediciones] ([IDEdicion])
GO

ALTER TABLE [dbo].[Articulos] CHECK CONSTRAINT [FK_Articulos_Ediciones]
GO


