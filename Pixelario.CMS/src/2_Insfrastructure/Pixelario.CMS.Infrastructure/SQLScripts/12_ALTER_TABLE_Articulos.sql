USE [CCET1]
GO

ALTER TABLE [dbo].[Articulos] DROP CONSTRAINT [FK_Articulos_Ediciones]
GO

ALTER TABLE [dbo].[Articulos]  WITH CHECK ADD  CONSTRAINT [FK_Articulos_Editions] FOREIGN KEY([IDEdicion])
REFERENCES [dbo].[Editions] ([IDEdition])
GO

ALTER TABLE [dbo].[Articulos] CHECK CONSTRAINT [FK_Articulos_Editions]
GO


