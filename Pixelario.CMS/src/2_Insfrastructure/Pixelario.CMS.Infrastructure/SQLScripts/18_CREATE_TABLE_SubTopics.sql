USE [CCET1]
GO

/****** Object:  Table [dbo].[SubTopics]    Script Date: 20/7/2019 18:14:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SubTopics](
	[IDSubTopic] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[IDTopic] [int] NOT NULL,
	[Title] [varchar](128) NOT NULL,
	[Summary] [varchar](4000) NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[OnMenu] [bit] NOT NULL,
	[OrderAtMenu] [int] NOT NULL,
	[HomeImage] [varchar](256) NULL,
 CONSTRAINT [PK_SubTopics] PRIMARY KEY CLUSTERED 
(
	[IDSubTopic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SubTopics] ADD  CONSTRAINT [DF_SubTopics_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[SubTopics] ADD  CONSTRAINT [DF_SubTopics_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[SubTopics] ADD  CONSTRAINT [DF_SubTopics_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[SubTopics] ADD  CONSTRAINT [DF_SubTopics_OnMenu]  DEFAULT ((0)) FOR [OnMenu]
GO

ALTER TABLE [dbo].[SubTopics] ADD  CONSTRAINT [DF_SubTopics_OrderAtMenu]  DEFAULT ((0)) FOR [OrderAtMenu]
GO

ALTER TABLE [dbo].[SubTopics]  WITH CHECK ADD  CONSTRAINT [FK_SubTopics_Topics] FOREIGN KEY([IDTopic])
REFERENCES [dbo].[Topics] ([IDTopic])
GO

ALTER TABLE [dbo].[SubTopics] CHECK CONSTRAINT [FK_SubTopics_Topics]
GO


