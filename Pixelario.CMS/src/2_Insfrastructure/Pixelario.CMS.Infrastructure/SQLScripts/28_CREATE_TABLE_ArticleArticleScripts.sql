USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleArticleScripts]    Script Date: 21/6/2020 18:56:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleArticleScripts](
	[IDArticle] [int] NOT NULL,
	[IDArticleScript] [int] NOT NULL,
 CONSTRAINT [PK_ArticleArticleScripts] PRIMARY KEY CLUSTERED 
(
	[IDArticle] ASC,
	[IDArticleScript] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleArticleScripts]  WITH CHECK ADD  CONSTRAINT [FK_ArticleArticleScripts_Articles] FOREIGN KEY([IDArticle])
REFERENCES [dbo].[Articles] ([IDArticle])
GO

ALTER TABLE [dbo].[ArticleArticleScripts] CHECK CONSTRAINT [FK_ArticleArticleScripts_Articles]
GO

ALTER TABLE [dbo].[ArticleArticleScripts]  WITH CHECK ADD  CONSTRAINT [FK_ArticleArticleScripts_ArticleScripts] FOREIGN KEY([IDArticleScript])
REFERENCES [dbo].[ArticleScripts] ([IDArticleScript])
GO

ALTER TABLE [dbo].[ArticleArticleScripts] CHECK CONSTRAINT [FK_ArticleArticleScripts_ArticleScripts]
GO


