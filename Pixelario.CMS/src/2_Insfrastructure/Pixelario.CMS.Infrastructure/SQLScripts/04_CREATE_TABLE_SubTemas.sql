USE [CCET1]
GO

/****** Object:  Table [dbo].[SubTemas]    Script Date: 9/4/2019 12:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SubTemas](
	[IDSubTema] [int] IDENTITY(1,1) NOT NULL,
	[CreadoPor] [varchar](256) NOT NULL,
	[FechaDeCreacion] [datetime] NOT NULL,
	[IDTema] [int] NOT NULL,
	[Titulo] [varchar](128) NOT NULL,
	[Sumario] [varchar](4000) NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Habilitado] [bit] NOT NULL,
	[Portada] [bit] NOT NULL,
	[Orden] [int] NOT NULL,
	[ImagenDePortada] [varchar](256) NULL,
 CONSTRAINT [PK_SubTemas] PRIMARY KEY CLUSTERED 
(
	[IDSubTema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SubTemas] ADD  CONSTRAINT [DF_SubTemas_Habilitado]  DEFAULT ((0)) FOR [Habilitado]
GO

ALTER TABLE [dbo].[SubTemas] ADD  CONSTRAINT [DF_SubTemas_Portada]  DEFAULT ((0)) FOR [Portada]
GO

ALTER TABLE [dbo].[SubTemas] ADD  CONSTRAINT [DF_SubTemas_Orden]  DEFAULT ((0)) FOR [Orden]
GO

ALTER TABLE [dbo].[SubTemas]  WITH CHECK ADD  CONSTRAINT [FK_SubTemas_Temas] FOREIGN KEY([IDTema])
REFERENCES [dbo].[Temas] ([IDTema])
GO

ALTER TABLE [dbo].[SubTemas] CHECK CONSTRAINT [FK_SubTemas_Temas]
GO


