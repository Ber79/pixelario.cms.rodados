USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticuloSubTemas]    Script Date: 19/4/2019 17:37:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticuloSubTemas](
	[IDArticulo] [int] NOT NULL,
	[IDSubTema] [int] NOT NULL,
 CONSTRAINT [PK_ArticuloSubTemas] PRIMARY KEY CLUSTERED 
(
	[IDArticulo] ASC,
	[IDSubTema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticuloSubTemas]  WITH CHECK ADD  CONSTRAINT [FK_ArticuloSubTemas_Articulos] FOREIGN KEY([IDArticulo])
REFERENCES [dbo].[Articulos] ([IDArticulo])
GO

ALTER TABLE [dbo].[ArticuloSubTemas] CHECK CONSTRAINT [FK_ArticuloSubTemas_Articulos]
GO

ALTER TABLE [dbo].[ArticuloSubTemas]  WITH CHECK ADD  CONSTRAINT [FK_ArticuloSubTemas_SubTemas] FOREIGN KEY([IDSubTema])
REFERENCES [dbo].[SubTemas] ([IDSubTema])
GO

ALTER TABLE [dbo].[ArticuloSubTemas] CHECK CONSTRAINT [FK_ArticuloSubTemas_SubTemas]
GO


