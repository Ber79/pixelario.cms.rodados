USE [CCET1]
GO

/****** Object:  Table [dbo].[Pages]    Script Date: 9/7/2019 10:59:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Pages](
	[IDPage] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Title] [varchar](128) NOT NULL,
	[Summary] [varchar](4000) NOT NULL,
	[Body] [ntext] NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[HomeImage] [varchar](256) NULL,
	[OnMenu] [bit] NOT NULL,
	[OrderAtMenu] [int] NOT NULL,
 CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED 
(
	[IDPage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_OnMenu]  DEFAULT ((0)) FOR [OnMenu]
GO

ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_OrderAtMenu]  DEFAULT ((0)) FOR [OrderAtMenu]
GO


