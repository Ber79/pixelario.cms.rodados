USE [CCET1]
GO

/****** Object:  Table [dbo].[Articles]    Script Date: 2/8/2019 10:55:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Articles](
	[IDArticle] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NOT NULL,
	[PublicationDate] [datetime] NULL,
	[IDEdition] [int] NULL,
	[Title] [varchar](128) NOT NULL,
	[Summary] [ntext] NULL,
	[Body] [ntext] NOT NULL,
	[Source] [varchar](128) NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[HomeImage] [varchar](256) NULL,
	[VerticalImage] [varchar](256) NULL,
	[HorizontalImage] [varchar](256) NULL,
	[Order] [int] NOT NULL,
	[IDPlaceAtHome] [int] NOT NULL,
	[Keywords] [varchar](256) NULL,
	[OrderAtEdition] [int] NOT NULL,
 CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED 
(
	[IDArticle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Articles] ADD  CONSTRAINT [DF_Articles_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Articles] ADD  CONSTRAINT [DF_Articles_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[Articles] ADD  CONSTRAINT [DF_Articles_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[Articles] ADD  CONSTRAINT [DF_Articles_IDPlaceAtHome]  DEFAULT ((0)) FOR [IDPlaceAtHome]
GO

ALTER TABLE [dbo].[Articles] ADD  CONSTRAINT [DF_Articles_OrderAtEdition]  DEFAULT ((0)) FOR [OrderAtEdition]
GO


