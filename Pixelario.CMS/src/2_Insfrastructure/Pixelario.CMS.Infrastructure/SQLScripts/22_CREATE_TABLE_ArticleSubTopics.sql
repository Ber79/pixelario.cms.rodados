USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleSubTopics]    Script Date: 27/7/2019 18:57:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleSubTopics](
	[IDArticle] [int] NOT NULL,
	[IDSubTopic] [int] NOT NULL,
 CONSTRAINT [PK_ArticleSubTopics] PRIMARY KEY CLUSTERED 
(
	[IDArticle] ASC,
	[IDSubTopic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleSubTopics]  WITH CHECK ADD  CONSTRAINT [FK_ArticleSubTopics_Articles] FOREIGN KEY([IDArticle])
REFERENCES [dbo].[Articles] ([IDArticle])
GO

ALTER TABLE [dbo].[ArticleSubTopics] CHECK CONSTRAINT [FK_ArticleSubTopics_Articles]
GO

ALTER TABLE [dbo].[ArticleSubTopics]  WITH CHECK ADD  CONSTRAINT [FK_ArticleSubTopics_SubTopics] FOREIGN KEY([IDSubTopic])
REFERENCES [dbo].[SubTopics] ([IDSubTopic])
GO

ALTER TABLE [dbo].[ArticleSubTopics] CHECK CONSTRAINT [FK_ArticleSubTopics_SubTopics]
GO


