USE [CCET1]
GO

/****** Object:  Table [dbo].[TopicEditions]    Script Date: 18/7/2019 11:31:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TopicEditions](
	[IDTopic] [int] NOT NULL,
	[IDEdition] [int] NOT NULL,
 CONSTRAINT [PK_TopicEditions] PRIMARY KEY CLUSTERED 
(
	[IDTopic] ASC,
	[IDEdition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TopicEditions]  WITH CHECK ADD  CONSTRAINT [FK_TopicEditions_Editions] FOREIGN KEY([IDEdition])
REFERENCES [dbo].[Editions] ([IDEdition])
GO

ALTER TABLE [dbo].[TopicEditions] CHECK CONSTRAINT [FK_TopicEditions_Editions]
GO

ALTER TABLE [dbo].[TopicEditions]  WITH CHECK ADD  CONSTRAINT [FK_TopicEditions_Topics] FOREIGN KEY([IDTopic])
REFERENCES [dbo].[Topics] ([IDTopic])
GO

ALTER TABLE [dbo].[TopicEditions] CHECK CONSTRAINT [FK_TopicEditions_Topics]
GO


