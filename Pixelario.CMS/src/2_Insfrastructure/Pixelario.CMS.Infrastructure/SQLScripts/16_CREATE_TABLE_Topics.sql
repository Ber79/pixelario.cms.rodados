USE [CCET1]
GO

/****** Object:  Table [dbo].[Topics]    Script Date: 18/7/2019 11:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Topics](
	[IDTopic] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Title] [varchar](128) NOT NULL,
	[Summary] [varchar](4000) NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[HomeImage] [varchar](256) NULL,
	[OnMenu] [bit] NOT NULL,
	[OrderAtMenu] [int] NOT NULL,
 CONSTRAINT [PK_Topics] PRIMARY KEY CLUSTERED 
(
	[IDTopic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Topics] ADD  CONSTRAINT [DF_Topics_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Topics] ADD  CONSTRAINT [DF_Topics_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[Topics] ADD  CONSTRAINT [DF_Topics_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[Topics] ADD  CONSTRAINT [DF_Topics_OnMenu]  DEFAULT ((0)) FOR [OnMenu]
GO

ALTER TABLE [dbo].[Topics] ADD  CONSTRAINT [DF_Topics_OrderAtMenu]  DEFAULT ((0)) FOR [OrderAtMenu]
GO


