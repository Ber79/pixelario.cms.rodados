USE [CCET1]
GO

/****** Object:  Table [dbo].[PageMultimedia]    Script Date: 9/7/2019 11:03:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PageMultimedia](
	[IDMultimedia] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[IDMultimediaType] [int] NOT NULL,
	[IDPage] [int] NOT NULL,
	[Title] [varchar](256) NOT NULL,
	[Summary] [varchar](4000) NULL,
	[Path1] [varchar](256) NULL,
	[Path2] [varchar](256) NULL,
	[Cover] [bit] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_CTL_PageMultimedia] PRIMARY KEY CLUSTERED 
(
	[IDMultimedia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PageMultimedia] ADD  CONSTRAINT [DF_PageMultimedia_Cover]  DEFAULT ((0)) FOR [Cover]
GO

ALTER TABLE [dbo].[PageMultimedia] ADD  CONSTRAINT [DF_PageMultimedia_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO

ALTER TABLE [dbo].[PageMultimedia] ADD  CONSTRAINT [DF_PageMultimedia_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[PageMultimedia]  WITH CHECK ADD  CONSTRAINT [FK_PageMultimedia_Pages] FOREIGN KEY([IDPage])
REFERENCES [dbo].[Pages] ([IDPage])
GO

ALTER TABLE [dbo].[PageMultimedia] CHECK CONSTRAINT [FK_PageMultimedia_Pages]
GO


