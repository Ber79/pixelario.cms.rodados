USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticuloTemas]    Script Date: 18/4/2019 20:35:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticuloTemas](
	[IDArticulo] [int] NOT NULL,
	[IDTema] [int] NOT NULL,
 CONSTRAINT [PK_ArticuloTemas] PRIMARY KEY CLUSTERED 
(
	[IDArticulo] ASC,
	[IDTema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticuloTemas]  WITH CHECK ADD  CONSTRAINT [FK_ArticuloTemas_Articulos] FOREIGN KEY([IDArticulo])
REFERENCES [dbo].[Articulos] ([IDArticulo])
GO

ALTER TABLE [dbo].[ArticuloTemas] CHECK CONSTRAINT [FK_ArticuloTemas_Articulos]
GO

ALTER TABLE [dbo].[ArticuloTemas]  WITH CHECK ADD  CONSTRAINT [FK_ArticuloTemas_Temas] FOREIGN KEY([IDTema])
REFERENCES [dbo].[Temas] ([IDTema])
GO

ALTER TABLE [dbo].[ArticuloTemas] CHECK CONSTRAINT [FK_ArticuloTemas_Temas]
