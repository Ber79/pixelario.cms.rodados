USE [CCET1]
GO

/****** Object:  Table [dbo].[PaginaEdiciones]    Script Date: 20/4/2019 19:23:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PaginaEdiciones](
	[IDPagina] [int] NOT NULL,
	[IDEdicion] [int] NOT NULL,
 CONSTRAINT [PK_PaginaEdiciones] PRIMARY KEY CLUSTERED 
(
	[IDPagina] ASC,
	[IDEdicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PaginaEdiciones]  WITH CHECK ADD  CONSTRAINT [FK_PaginaEdiciones_Ediciones] FOREIGN KEY([IDEdicion])
REFERENCES [dbo].[Ediciones] ([IDEdicion])
GO

ALTER TABLE [dbo].[PaginaEdiciones] CHECK CONSTRAINT [FK_PaginaEdiciones_Ediciones]
GO

ALTER TABLE [dbo].[PaginaEdiciones]  WITH CHECK ADD  CONSTRAINT [FK_PaginaEdiciones_Paginas] FOREIGN KEY([IDPagina])
REFERENCES [dbo].[Paginas] ([IDPagina])
GO

ALTER TABLE [dbo].[PaginaEdiciones] CHECK CONSTRAINT [FK_PaginaEdiciones_Paginas]
GO


