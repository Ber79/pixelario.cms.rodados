USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleMultimedia]    Script Date: 27/7/2019 18:46:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleMultimedia](
	[IDMultimedia] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[IDMultimediaType] [int] NOT NULL,
	[IDArticle] [int] NOT NULL,
	[Title] [varchar](256) NOT NULL,
	[Summary] [varchar](4000) NULL,
	[Path1] [varchar](256) NULL,
	[Path2] [varchar](256) NULL,
	[Cover] [bit] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[AtHome] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_CTL_ArticleMultimedia] PRIMARY KEY CLUSTERED 
(
	[IDMultimedia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleMultimedia] ADD  CONSTRAINT [DF_ArticleMultimedia_Cover]  DEFAULT ((0)) FOR [Cover]
GO

ALTER TABLE [dbo].[ArticleMultimedia] ADD  CONSTRAINT [DF_ArticleMultimedia_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO

ALTER TABLE [dbo].[ArticleMultimedia] ADD  CONSTRAINT [DF_ArticleMultimedia_AtHome]  DEFAULT ((0)) FOR [AtHome]
GO

ALTER TABLE [dbo].[ArticleMultimedia]  WITH CHECK ADD  CONSTRAINT [FK_ArticleMultimedia_Articles] FOREIGN KEY([IDArticle])
REFERENCES [dbo].[Articles] ([IDArticle])
GO

ALTER TABLE [dbo].[ArticleMultimedia] CHECK CONSTRAINT [FK_ArticleMultimedia_Articles]
GO


