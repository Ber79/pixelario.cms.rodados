USE [CCET1]
GO

/****** Object:  Table [dbo].[Paginas]    Script Date: 20/4/2019 19:21:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Paginas](
	[IDPagina] [int] IDENTITY(1,1) NOT NULL,
	[CreadoPor] [varchar](256) NOT NULL,
	[FechaDeCreacion] [datetime] NOT NULL,
	[Titulo] [varchar](128) NOT NULL,
	[Sumario] [varchar](4000) NOT NULL,
	[Cuerpo] [ntext] NOT NULL,
	[Keywords] [varchar](256) NULL,
	[Habilitado] [bit] NOT NULL,
	[Portada] [bit] NOT NULL,
	[Orden] [int] NOT NULL,
	[ImagenDePortada] [varchar](256) NULL,
 CONSTRAINT [PK_Paginas] PRIMARY KEY CLUSTERED 
(
	[IDPagina] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Paginas] ADD  CONSTRAINT [DF_Paginas_Habilitado]  DEFAULT ((0)) FOR [Habilitado]
GO

ALTER TABLE [dbo].[Paginas] ADD  CONSTRAINT [DF_Paginas_Portada]  DEFAULT ((0)) FOR [Portada]
GO

ALTER TABLE [dbo].[Paginas] ADD  CONSTRAINT [DF_Paginas_Orden]  DEFAULT ((0)) FOR [Orden]
GO


