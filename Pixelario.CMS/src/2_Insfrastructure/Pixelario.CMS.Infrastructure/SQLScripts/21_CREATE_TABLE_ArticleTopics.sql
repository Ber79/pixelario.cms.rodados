USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleTopics]    Script Date: 27/7/2019 18:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleTopics](
	[IDArticle] [int] NOT NULL,
	[IDTopic] [int] NOT NULL,
 CONSTRAINT [PK_ArticleTopics] PRIMARY KEY CLUSTERED 
(
	[IDArticle] ASC,
	[IDTopic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleTopics]  WITH CHECK ADD  CONSTRAINT [FK_ArticleTopics_Articles] FOREIGN KEY([IDArticle])
REFERENCES [dbo].[Articles] ([IDArticle])
GO

ALTER TABLE [dbo].[ArticleTopics] CHECK CONSTRAINT [FK_ArticleTopics_Articles]
GO

ALTER TABLE [dbo].[ArticleTopics]  WITH CHECK ADD  CONSTRAINT [FK_ArticleTopics_Topics] FOREIGN KEY([IDTopic])
REFERENCES [dbo].[Topics] ([IDTopic])
GO

ALTER TABLE [dbo].[ArticleTopics] CHECK CONSTRAINT [FK_ArticleTopics_Topics]
GO


