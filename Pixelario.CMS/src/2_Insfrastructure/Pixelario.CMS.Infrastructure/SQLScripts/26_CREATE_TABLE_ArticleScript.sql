USE [CCET1]
GO

/****** Object:  Table [dbo].[ArticleScript]    Script Date: 9/6/2020 19:26:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ArticleScripts](
	[IDArticleScript] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[Title] [varchar](256) NOT NULL,
	[Summary] [varchar](4000) NULL,
	[Enabled] [bit] NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_CTL_ArticleScripts] PRIMARY KEY CLUSTERED 
(
	[IDArticleScript] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ArticleScripts] ADD  CONSTRAINT [DF_ArticleScripts_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

