USE [CCET1]
GO

/****** Object:  Table [dbo].[TemaEdiciones]    Script Date: 5/4/2019 18:17:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TemaEdiciones](
	[IDTema] [int] NOT NULL,
	[IDEdicion] [int] NOT NULL,
 CONSTRAINT [PK_TemaEdiciones] PRIMARY KEY CLUSTERED 
(
	[IDTema] ASC,
	[IDEdicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TemaEdiciones]  WITH CHECK ADD  CONSTRAINT [FK_TemaEdiciones_Ediciones] FOREIGN KEY([IDEdicion])
REFERENCES [dbo].[Ediciones] ([IDEdicion])
GO

ALTER TABLE [dbo].[TemaEdiciones] CHECK CONSTRAINT [FK_TemaEdiciones_Ediciones]
GO

ALTER TABLE [dbo].[TemaEdiciones]  WITH CHECK ADD  CONSTRAINT [FK_TemaEdiciones_Temas] FOREIGN KEY([IDTema])
REFERENCES [dbo].[Temas] ([IDTema])
GO

ALTER TABLE [dbo].[TemaEdiciones] CHECK CONSTRAINT [FK_TemaEdiciones_Temas]
GO


