﻿USE [CCET1]
GO

/****** Object:  Table [dbo].[ConfigurationOptions]    Script Date: 27/5/2020 18:25:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ConfigurationOptions](
	[IDConfigurationOption] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[IDConfiguration] [int] NOT NULL,
	[Value] [varchar](128) NOT NULL,
	[Enabled] [bit] NOT NULL,	
	[Order] [int] NOT NULL,	
 CONSTRAINT [PK_ConfigurationOptions] PRIMARY KEY CLUSTERED 
(
	[IDConfigurationOption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ConfigurationOptions] ADD  CONSTRAINT [DF_ConfigurationOptions_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[ConfigurationOptions] ADD  CONSTRAINT [DF_ConfigurationOptions_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[ConfigurationOptions]  WITH CHECK ADD  CONSTRAINT [FK_ConfigurationOptions_Configurations] FOREIGN KEY([IDConfiguration])
REFERENCES [dbo].[Configurations] ([IDConfiguration])
GO

ALTER TABLE [dbo].[ConfigurationOptions] CHECK CONSTRAINT [FK_ConfigurationOptions_Configurations]
GO


