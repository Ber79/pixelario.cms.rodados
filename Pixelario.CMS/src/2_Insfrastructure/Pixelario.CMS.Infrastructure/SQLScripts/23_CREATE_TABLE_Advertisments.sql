USE [CCET1]
GO

/****** Object:  Table [dbo].[Advertisments]    Script Date: 5/11/2019 09:51:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Advertisments](
	[IDAdvertisment] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](256) NOT NULL,
	[PublicationStartDate] [datetime] NULL,
	[PublicationEndDate] [datetime] NULL,
	[Title] [varchar](128) NOT NULL,
	[Code] [ntext] NULL,
	[URL] [varchar](255) NULL,
	[Enabled] [bit] NOT NULL,
	[Published] [bit] NOT NULL,
	[HomeImage] [varchar](256) NULL,
	[VerticalImage] [varchar](256) NULL,
	[HorizontalImage] [varchar](256) NULL,
	[Order] [int] NOT NULL,
	[IDPlaceAtWeb] [int] NOT NULL,
 CONSTRAINT [PK_Advertisments] PRIMARY KEY CLUSTERED 
(
	[IDAdvertisment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Advertisments] ADD  CONSTRAINT [DF_Advertisments_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Advertisments] ADD  CONSTRAINT [DF_Advertisments_Published]  DEFAULT ((0)) FOR [Published]
GO

ALTER TABLE [dbo].[Advertisments] ADD  CONSTRAINT [DF_Advertisments_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[Advertisments] ADD  CONSTRAINT [DF_Advertisments_IDPlaceAtWeb]  DEFAULT ((0)) FOR [IDPlaceAtWeb]
GO


