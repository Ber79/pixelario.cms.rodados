USE [CCET1]
GO

/****** Object:  Table [dbo].[MultimediosDeArticulo]    Script Date: 1/5/2019 14:40:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MultimediosDeArticulo](
	[IDMultimedio] [int] IDENTITY(1,1) NOT NULL,
	[FechaDeCreacion] [datetime] NOT NULL,
	[CreadoPor] [varchar](256) NOT NULL,
	[IDTipoDeMultimedio] [int] NOT NULL,
	[IDArticulo] [int] NOT NULL,
	[Titulo] [varchar](256) NOT NULL,
	[Sumario] [varchar](4000) NULL,
	[Ruta1] [varchar](256) NULL,
	[Ruta2] [varchar](256) NULL,
	[Destacado] [bit] NOT NULL,
	[Habilitado] [bit] NOT NULL,
	[Portada] [bit] NOT NULL,
	[Orden] [int] NOT NULL,
 CONSTRAINT [PK_CTL_Multimedios] PRIMARY KEY CLUSTERED 
(
	[IDMultimedio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MultimediosDeArticulo] ADD  CONSTRAINT [DF_MultimediosDeArticulo_Destacado]  DEFAULT ((0)) FOR [Destacado]
GO

ALTER TABLE [dbo].[MultimediosDeArticulo] ADD  CONSTRAINT [DF_MultimediosDeArticulo_Habilitado]  DEFAULT ((1)) FOR [Habilitado]
GO

ALTER TABLE [dbo].[MultimediosDeArticulo] ADD  CONSTRAINT [DF_MultimediosDeArticulo_Portada]  DEFAULT ((0)) FOR [Portada]
GO

ALTER TABLE [dbo].[MultimediosDeArticulo]  WITH CHECK ADD  CONSTRAINT [FK_MultimediosDeArticulo_Articulos] FOREIGN KEY([IDArticulo])
REFERENCES [dbo].[Articulos] ([IDArticulo])
GO

ALTER TABLE [dbo].[MultimediosDeArticulo] CHECK CONSTRAINT [FK_MultimediosDeArticulo_Articulos]
GO


