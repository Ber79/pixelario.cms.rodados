﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Advertisments;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Articles;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Configurations;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Editions;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Pages;
using Pixelario.CMS.Infrastructure.EntityConfigurations.Topics;
using Pixelario.CMS.Seedwork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;
namespace Pixelario.CMS.Infrastructure
{
    public class CMSContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "cms";

        public DbSet<Edition> Editions { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleMultimedia> ArticleMultimedia { get; set; }

        public DbSet<ArticleScript> ArticleScripts { get; set; }
        public DbSet<ArticleScriptItem> ArticleScriptItems { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<SubTopic> SubTopics { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<PageMultimedia> PageMultimedia { get; set; }

        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<ConfigurationOption> ConfigurationOptions { get; set; }

        public DbSet<Advertisment> Advertisments { get; set; }
        private CMSContext() 
            : base("CMSContext")
        {
            //Database.SetInitializer(
            //    new CMSDBInitilizer());            
        }
       
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        public CMSContext(ILogger logger,
            IMediator mediator) : base("CMSContext")
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EditionsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new TopicsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SubTopicsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ArticlesEntityTypeConfiguration());            
            modelBuilder.Configurations.Add(new ArticleMultimediaEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new PagesEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new PageMultimediaEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ConfigurationsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ConfigurationOptionsEntityTypeConfiguration());

            modelBuilder.Configurations.Add(new ArticleScriptEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ArticleScriptItemEntityTypeConfiguration());



            modelBuilder.Configurations.Add(new AdvertismentsEntityTypeConfiguration());



            base.OnModelCreating(modelBuilder);
        }             

        public async Task<SaveEntitiesResponse> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var saveEntitiesResponse = new SaveEntitiesResponse();
            try
            {
                // Dispara los eventos de dominio antes de salvar los cambios
                // await _mediator.DispatchBeforeDomainEventsAsync(this);
                _logger.Debug("Saving change on CMSContext");
                saveEntitiesResponse.Success = await base.SaveChangesAsync() > 0;
                _logger.Debug("Dispaching after DomainEvents");
                await _mediator.DispatchAfterDomainEventsAsync(this);
                return saveEntitiesResponse;
            }            
            catch(DbEntityValidationException ex)
            {
                _logger.Error(ex, "DbEntityValidationException catched");
                saveEntitiesResponse.Success = false;
                saveEntitiesResponse.Errors = new List<string>() { ex.Message };
                if (ex.EntityValidationErrors != null)
                {
                    foreach(var entityValidationError in ex.EntityValidationErrors)
                    {
                        var entryMessage = string.Format("Entry: {0}", entityValidationError.Entry);
                        foreach(var validationError in entityValidationError.ValidationErrors)
                        {
                            saveEntitiesResponse.Errors.Add(string.Format("{0} - Property: {1} - Error: {2}",
                                entryMessage, validationError.PropertyName,
                                validationError.ErrorMessage));
                        }
                    }
                }                
                return saveEntitiesResponse;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                saveEntitiesResponse.Success = false;
                saveEntitiesResponse.Errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    saveEntitiesResponse.Errors.Add(ex.InnerException.Message);                
                return saveEntitiesResponse;
            }
        }
    }

}
