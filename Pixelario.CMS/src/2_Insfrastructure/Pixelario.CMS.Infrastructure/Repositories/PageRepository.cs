﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class PageRepository : IPageRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public PageRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Page Add(Page page)
        {
           return _context.Pages.Add(page);
        }
        public async Task<Page> GetAsync(int iDPage)
        {
            var page = await _context.Pages                
                .FindAsync(iDPage);
            if (page != null)
            {
                await _context.Entry(page)
                .Collection(a => a.PageMultimedia).LoadAsync();
                await _context.Entry(page)
                    .Collection(p => p.Editions).LoadAsync();
            }
            return page;
        }
        public void Update(Page page)
        {
            _context.Entry(page).State = 
                EntityState.Modified;
        }
        public void Delete(Page page)
        {
            _context.Pages.Remove(page);
        }
        public void Delete(PageMultimedia multimedia)
        {
            _context.PageMultimedia.Remove(multimedia);
        }

        public void OrderPagesAtHome()
        {
            var newOrder = 1;
            foreach (var page in _context.Pages.Where(
                p => p.AtHome)
                .OrderBy(p => p.Order)
                .ToList())
            {
                page.Order = newOrder;
                _context.Entry(page).State =
                    EntityState.Modified;
                newOrder++;
            }
        }
        public void OrderPagesAtMenu(Edition edition)
        {
            var newOrder = 1;
            foreach (var page in _context.Pages
                .Include(
                    p => p.Editions)
                .Where(
                    p => p.OnMenu && p.Editions.Any()
                        && p.Editions.Any(
                            e=> e.ID == edition.ID))
                .OrderBy(p => p.OrderAtMenu)
                .ToList())
            {
                page.OrderAtMenu = newOrder;
                _context.Entry(page).State =
                    EntityState.Modified;
                newOrder++;
            }
        }
        public void ReorderPagesAtHome(Page page, int lastOrder)
        {
            List<Page> pages = null;
            int newOrder = 1;
            if (page.Order < lastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = page.Order + 1;
                pages = _context.Pages.Where(t => t.AtHome &&
                    t.Order <= lastOrder && t.Order >= page.Order)
                    .OrderBy(t => t.Order)
                    .ToList();
                foreach (var item in pages)
                {
                    if (item.ID != page.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = lastOrder;
                pages = _context.Pages.Where(t => t.AtHome &&
                    t.Order >= lastOrder && t.Order <= page.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in pages)
                {
                    if (item.ID != page.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void ReorderPagesAtMenu(Page page, int lastOrder)
        {
            List<Page> pages = null;
            int newOrder = 1;
            try
            {


                if (page.OrderAtMenu < lastOrder)
                {
                    // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                    newOrder = page.OrderAtMenu + 1;
                    var iDEdition = page.Editions.First().ID;
                    pages = _context.Pages
                        .Include(
                            p => p.Editions)
                        .Where(
                            p => p.OnMenu && p.Editions.Any() &&
                            p.Editions.Any(
                                e => e.ID == iDEdition) &&
                            p.OrderAtMenu <= lastOrder && p.OrderAtMenu >= page.OrderAtMenu)
                        .OrderBy(p => p.OrderAtMenu)
                        .ToList();
                    foreach (var item in pages)
                    {
                        if (item.ID != page.ID)
                        {
                            item.OrderAtMenu = newOrder;
                            _context.Entry(item).State =
                                EntityState.Modified;
                            newOrder++;
                        }
                    }
                }
                else
                {
                    // ej: 2 pasa a 4(3 y 4 deben restar 1)
                    newOrder = lastOrder;
                    var iDEdition = page.Editions.First().ID;
                    pages = _context.Pages
                        .Include(
                            p => p.Editions)
                        .Where(
                            p => p.OnMenu &&
                            p.Editions.Any(
                                e => e.ID == iDEdition) &&
                            p.OrderAtMenu >= lastOrder && p.OrderAtMenu <= page.OrderAtMenu)
                        .OrderBy(a => a.OrderAtMenu)
                        .ToList();
                    foreach (var item in pages)
                    {
                        if (item.ID != page.ID)
                        {
                            item.OrderAtMenu = newOrder;
                            _context.Entry(item).State =
                                EntityState.Modified;
                            newOrder++;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                string m = ex.Message;
            }
        }
    }
}
