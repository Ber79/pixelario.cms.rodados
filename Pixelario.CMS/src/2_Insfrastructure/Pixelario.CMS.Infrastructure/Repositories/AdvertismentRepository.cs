﻿using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class AdvertismentRepository : IAdvertismentRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public AdvertismentRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Advertisment Add(Advertisment advertisment)
        {
            return _context.Advertisments.Add(advertisment);
        }
        public async Task<Advertisment> GetAsync(int iDAdvertisment)
        {
            var advertisment = await _context.Advertisments
                .FindAsync(iDAdvertisment);           
            return advertisment;
        }
        public void Update(Advertisment advertisment)
        {
            _context.Entry(advertisment).State =
                EntityState.Modified;
        }
        public void Delete(Advertisment advertisment)
        {
            _context.Advertisments.Remove(advertisment);
        }
        public void OrderAdvertismentsAtPlace(int placeAtWeb)
        {
            if (placeAtWeb != AdvertismentPlaceAtWeb.None.Id)
            {
                var newOrder = 1;
                foreach (var advertisment in _context.Advertisments.Where(
                    a => a.IDPlaceAtWeb == (int)placeAtWeb)
                    .OrderBy(a => a.Order)
                    .ToList())
                {
                    advertisment.Order = newOrder;
                    _context.Entry(advertisment).State =
                        EntityState.Modified;
                    newOrder++;
                }
            }
        }
        public void ReorderAdvertismentsAtPlace(Advertisment advertisment, 
            int LastOrder)
        {
            List<Advertisment> advertisments = null;
            int newOrder = 1;
            if (advertisment.Order < LastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = advertisment.Order + 1;
                advertisments = _context.Advertisments
                    .Where(a => a.IDPlaceAtWeb == advertisment.IDPlaceAtWeb &&
                        a.Order <= LastOrder && a.Order >= advertisment.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in advertisments)
                {
                    if (item.ID != advertisment.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = LastOrder;
                advertisments = _context.Advertisments
                    .Where(a => a.Enabled && a.Published && 
                        a.IDPlaceAtWeb == advertisment.IDPlaceAtWeb &&
                        a.Order >= LastOrder && a.Order <= advertisment.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in advertisments)
                {
                    if (item.ID != advertisment.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
    }
}
