﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public ArticleRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Article Add(Article article)
        {
            return _context.Articles.Add(article);
        }
        public async Task<Article> GetAsync(int idArticle)
        {
            var article = await _context.Articles
                .FindAsync(idArticle);
            if (article != null)
            {
                await _context.Entry(article)
                    .Reference(a => a.Edition).LoadAsync();
                await _context.Entry(article)
                    .Collection(a => a.ArticleMultimedia).LoadAsync();
                await _context.Entry(article)
                    .Collection(a => a.Topics).LoadAsync();
                await _context.Entry(article)
                    .Collection(t => t.SubTopics).LoadAsync();
                await _context.Entry(article)
                    .Collection(a => a.Scripts).LoadAsync();

            }
            return article;
        }
        public void Update(Article article)
        {
            _context.Entry(article).State =
                EntityState.Modified;
        }
        public void Delete(Article article)
        {
            _context.Articles.Remove(article);
        }
        public void OrderArticlesAtPlace(int placeAtHome)
        {
            if (placeAtHome != ArticlePlaceAtHome.None.Id ||
                placeAtHome != ArticlePlaceAtHome.Boxes.Id)
            {
                var newOrder = 1;
                foreach (var article in _context.Articles.Where(
                    a => a.IDPlaceAtHome == (int)placeAtHome)
                    .OrderBy(a => a.Order)
                    .ToList())
                {
                    article.Order = newOrder;
                    _context.Entry(article).State =
                        EntityState.Modified;
                    newOrder++;
                }
            }
        }
        public void OrderArticlesAtHomeByEdition(int iDEdition)
        {
            var newOrder = 1;
            foreach (var article in _context.Articles.Where(
                a => a.IDEdition == iDEdition &&
                a.AtHome)
                .OrderBy(a => a.Order)
                .ToList())
            {
                article.Order = newOrder;
                _context.Entry(article).State =
                    EntityState.Modified;
                newOrder++;
            }

        }
        public void ReorderArticlesAtPlace(Article article, int LastOrder)
        {
            List<Article> articles = null;
            int newOrder = 1;
            if (article.Order < LastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = article.Order + 1;
                articles = _context.Articles.Where(a => a.IDPlaceAtHome == article.IDPlaceAtHome &&
                    a.Order <= LastOrder && a.Order >= article.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in articles)
                {
                    if (item.ID != article.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = LastOrder;
                articles = _context.Articles.Where(a => a.Enabled && a.AtHome && a.IDPlaceAtHome == article.IDPlaceAtHome &&
                    a.Order >= LastOrder && a.Order <= article.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in articles)
                {
                    if (item.ID != article.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void ReorderArticlesAtHomeByEdition(Article article, 
            int LastOrder)
        {
            List<Article> articles = null;
            int newOrder = 1;
            if (article.Order < LastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = article.Order + 1;
                articles = _context.Articles.Where(a => 
                    a.IDEdition == article.IDEdition && a.AtHome &&
                    a.Order <= LastOrder && a.Order >= article.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in articles)
                {
                    if (item.ID != article.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = LastOrder;
                articles = _context.Articles.Where(
                    a => a.Enabled && a.AtHome &&
                    a.IDEdition == article.IDEdition &&
                    a.Order >= LastOrder && a.Order <= article.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in articles)
                {
                    if (item.ID != article.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }


        public void Delete(ArticleMultimedia multimedia)
        {
            _context.ArticleMultimedia.Remove(multimedia);
        }
    }
}
