﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Seedwork;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class ArticleScriptRepository : IArticleScriptRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public ArticleScriptRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public ArticleScript Add(ArticleScript articleScript)
        {
            return _context.ArticleScripts.Add(articleScript);
        }
        public async Task<ArticleScript> GetAsync(int iDArticleScript)
        {
            var articleScript = await _context.ArticleScripts
                .FindAsync(iDArticleScript);
            if (articleScript != null)
            {
                await _context.Entry(articleScript)
                    .Collection(ars => ars.Items).LoadAsync();
            }
            return articleScript;
        }
        public void Update(ArticleScript articleScript)
        {
            _context.Entry(articleScript).State =
                EntityState.Modified;
        }
        public void Delete(ArticleScript articleScript)
        {
            _context.ArticleScripts.Remove(articleScript);
        }
        public void Delete(ArticleScriptItem articleScriptItem)
        {
            _context.ArticleScriptItems.Remove(articleScriptItem);
        }
    }
}
