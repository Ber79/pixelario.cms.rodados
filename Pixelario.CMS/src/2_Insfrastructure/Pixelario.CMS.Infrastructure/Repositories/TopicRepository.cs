﻿using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class TopicRepository : ITopicRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public TopicRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Topic Add(Topic tema)
        {
           return _context.Topics.Add(tema);
        }
        public async Task<Topic> GetAsync(int iDTopic)
        {
            var tema = await _context.Topics                
                .FindAsync(iDTopic);
            if (tema != null)
            {
                await _context.Entry(tema)
                    .Collection(t => t.Editions).LoadAsync();
                await _context.Entry(tema)
                    .Collection(t => t.SubTopics).LoadAsync();
            }
            return tema;
        }
        public void Update(Topic tema)
        {
            _context.Entry(tema).State = 
                EntityState.Modified;
        }
        public void Delete(Topic tema)
        {
            _context.Topics.Remove(tema);
        }
        public void Delete(SubTopic subTopic)
        {
            _context.SubTopics.Remove(subTopic);
        }

        public async Task<SubTopic> GetSubTopicAsync(int iDSubTopic)
        {
            var subTopic = await _context.SubTopics
                .FindAsync(iDSubTopic);
            return subTopic;
        }
        public void OrderTopicsAtHome()
        {
            var newOrder = 1;
            foreach (var topic in _context.Topics.Where(t => t.AtHome)
                .OrderBy(t => t.Order)
                .ToList())
            {
                topic.Order = newOrder;
                _context.Entry(topic).State =
                    EntityState.Modified;
                newOrder++;
            }
        }
        public void OrderTopicsAtMenu()
        {
            var newOrder = 1;
            foreach (var topic in _context.Topics.Where(t => t.OnMenu)
                .OrderBy(t => t.OrderAtMenu)
                .ToList())
            {
                topic.OrderAtMenu = newOrder;
                _context.Entry(topic).State =
                    EntityState.Modified;
                newOrder++;
            }
        }
        public void ReorderTopicsAtHome(Topic topic, int lastOrder)
        {
            List<Topic> topics = null;
            int newOrder = 1;
            if (topic.Order < lastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = topic.Order + 1;
                topics = _context.Topics.Where(t => t.AtHome &&
                    t.Order <= lastOrder && t.Order >= topic.Order)
                    .OrderBy(t => t.Order)
                    .ToList();
                foreach (var item in topics)
                {
                    if (item.ID != topic.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = lastOrder;
                topics = _context.Topics.Where(t => t.AtHome &&
                    t.Order >= lastOrder && t.Order <= topic.Order)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in topics)
                {
                    if (item.ID != topic.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void ReorderTopicsAtMenu(Topic topic, int lastOrder)
        {
            List<Topic> topics = null;
            int newOrder = 1;
            if (topic.OrderAtMenu < lastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = topic.OrderAtMenu + 1;
                topics = _context.Topics.Where(p => p.OnMenu &&
                    p.OrderAtMenu <= lastOrder && p.OrderAtMenu >= topic.OrderAtMenu)
                    .OrderBy(p => p.OrderAtMenu)
                    .ToList();
                foreach (var item in topics)
                {
                    if (item.ID != topic.ID)
                    {
                        item.OrderAtMenu = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = lastOrder;
                topics = _context.Topics.Where(p => p.OnMenu &&
                    p.OrderAtMenu >= lastOrder && p.OrderAtMenu <= topic.OrderAtMenu)
                    .OrderBy(a => a.OrderAtMenu)
                    .ToList();
                foreach (var item in topics)
                {
                    if (item.ID != topic.ID)
                    {
                        item.OrderAtMenu = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }

        public async Task OrderSubTopicsAtMenuAsync(int iDTopic)
        {
            var topic = await GetAsync(
                iDTopic: iDTopic);
            if (topic != null && topic.SubTopics != null &&
                topic.SubTopics.Where(s => s.OnMenu).Count() > 0)
            {
                var newOrder = 1;
                foreach (var subTopic in topic.SubTopics.Where(s=> s.OnMenu)
                    .OrderBy(s => s.OrderAtMenu)
                    .ToList())
                {
                    subTopic.OrderAtMenu = newOrder;
                    _context.Entry(subTopic).State =
                        EntityState.Modified;
                    newOrder++;
                }
            }
        }
        public async Task OrderSubTopicsAtHomeAsync(int iDTopic)
        {
            var topic = await GetAsync(
                iDTopic: iDTopic);
            if (topic != null && topic.SubTopics != null &&
                topic.SubTopics.Where(s => s.AtHome).Count() > 0)
            {
                var newOrder = 1;
                foreach (var subTopic in topic.SubTopics.Where(s => s.AtHome)
                    .OrderBy(s => s.Order)
                    .ToList())
                {
                    subTopic.Order = newOrder;
                    _context.Entry(subTopic).State =
                        EntityState.Modified;
                    newOrder++;
                }
            }
        }
        public async Task ReorderSubTopicsAtHome(SubTopic subTopic, int lastOrder)
        {
            if (subTopic != null)
            {
                var topic = await this.GetAsync(
                    iDTopic: subTopic.IDTopic);
                int newOrder = 1;
                if (subTopic.Order < lastOrder)
                {
                    // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                    newOrder = subTopic.Order + 1;
                    var subTopics = topic.SubTopics.Where(s => s.AtHome &&
                        s.Order <= lastOrder && s.Order >= subTopic.Order)
                        .OrderBy(s => s.Order)
                        .ToList();
                    foreach (var item in subTopics)
                    {
                        if (item.ID != subTopic.ID)
                        {
                            item.Order = newOrder;
                            newOrder++;
                        }
                    }
                }
                else
                {
                    // ej: 2 pasa a 4(3 y 4 deben restar 1)
                    newOrder = lastOrder;
                    var subTopics = topic.SubTopics.Where(s => s.AtHome &&
                        s.Order >= lastOrder && s.Order <= subTopic.Order)
                        .OrderBy(s => s.Order)
                        .ToList();
                    foreach (var item in subTopics)
                    {
                        if (item.ID != subTopic.ID)
                        {
                            item.Order = newOrder;
                            newOrder++;
                        }
                    }
                }
            }

        }
        public async Task ReorderSubTopicsAtMenu(SubTopic subTopic, int lastOrder)
        {
            if (subTopic != null)
            {
                var topic = await this.GetAsync(
                    iDTopic: subTopic.IDTopic);
                int newOrder = 1;
                if (subTopic.OrderAtMenu < lastOrder)
                {
                    // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                    newOrder = subTopic.OrderAtMenu + 1;
                    var subTopics = topic.SubTopics.Where(s => s.OnMenu &&
                        s.OrderAtMenu <= lastOrder && s.OrderAtMenu >= subTopic.OrderAtMenu)
                        .OrderBy(s => s.OrderAtMenu)
                        .ToList();
                    foreach (var item in subTopics)
                    {
                        if (item.ID != subTopic.ID)
                        {
                            item.OrderAtMenu = newOrder;
                            newOrder++;
                        }
                    }
                }
                else
                {
                    // ej: 2 pasa a 4(3 y 4 deben restar 1)
                    newOrder = lastOrder;
                    var subTopics = topic.SubTopics.Where(s => s.OnMenu &&
                        s.OrderAtMenu >= lastOrder && s.OrderAtMenu <= subTopic.OrderAtMenu)
                        .OrderBy(s => s.OrderAtMenu)
                        .ToList();
                    foreach (var item in subTopics)
                    {
                        if (item.ID != subTopic.ID)
                        {
                            item.OrderAtMenu = newOrder;
                            newOrder++;
                        }
                    }
                }
            }
        }
    }
}
