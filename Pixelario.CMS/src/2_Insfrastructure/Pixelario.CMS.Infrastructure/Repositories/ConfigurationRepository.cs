﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Seedwork;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public ConfigurationRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Configuration Add(Configuration configuration)
        {
           return _context.Configurations.Add(configuration);
        }
        public async Task<Configuration> GetAsync(int iDConfiguration)
        {
            var configuration = await _context.Configurations.FindAsync(iDConfiguration);
            if (configuration != null)
            {
                await _context.Entry(configuration)
                    .Collection(t => t.ConfigurationOptions).LoadAsync();
            }
            return configuration;
        }
        public void Update(Configuration configuration)
        {
            _context.Entry(configuration).State = EntityState.Modified;
        }
        public void Delete(Configuration configuration)
        {
            _context.Configurations.Remove(configuration);
        }

        public async Task<Configuration> GetAsync(string key)
        {
            var configuration =
                await _context.Configurations.Where(c => c.Key == key)
                .FirstOrDefaultAsync();
            return configuration;
        }

        public async Task<ConfigurationOption> GetConfigurationOptionAsync(int iDConfigurationOption)
        {
            var configurationOption = await _context.ConfigurationOptions
                .FindAsync(iDConfigurationOption);
            return configurationOption;
        }

        public void DeleteConfigurationOption(ConfigurationOption configurationOption)
        {
            _context.ConfigurationOptions.Remove(configurationOption);
        }
    }
}
