﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure.Repositories
{
    public class EditionRepository : IEditionRepository
    {
        private readonly CMSContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public EditionRepository(CMSContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Edition Add(Edition edition)
        {
            return _context.Editions.Add(edition);
        }
        public async Task<Edition> GetAsync(int iDEdition)
        {
            return await _context.Editions.FindAsync(iDEdition);
        }
        public void Update(Edition edition)
        {
            _context.Entry(edition).State = EntityState.Modified;
        }
        public void Delete(Edition edition)
        {
            _context.Editions.Remove(edition);
        }
        public void OrderOnTypes(int? iDEditionException)
        {
            foreach (var editionType in EditionType.List())
            {
                var newOrder = 1;
                foreach (var edition in _context.Editions.Where(
                    e => e.AtHome && e.EditionType == editionType.Id)
                    .OrderBy(e => e.Order)
                    .ToList())
                {
                    if (!iDEditionException.HasValue || iDEditionException.Value != edition.ID)
                    {
                        edition.Order = newOrder;
                        _context.Entry(edition).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void OrderMenuOnTypes(int? iDEditionException)
        {
            foreach (var editionType in EditionType.List())
            {
                var newOrder = 1;
                foreach (var edition in _context.Editions.Where(
                    e => e.OnMenu && e.EditionType == editionType.Id)
                    .OrderBy(e => e.OrderAtMenu)
                    .ToList())
                {
                    if (!iDEditionException.HasValue || iDEditionException.Value != edition.ID)
                    {
                        edition.OrderAtMenu = newOrder;
                        _context.Entry(edition).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void OrderOnType(int IDEditionType)
        {
            if (IDEditionType != EditionType.None.Id)
            {
                var newOrder = 1;
                foreach (var edition in _context.Editions.Where(
                    e => e.AtHome && e.EditionType == IDEditionType)
                    .OrderBy(e => e.Order)
                    .ToList())
                {
                    edition.Order = newOrder;
                    _context.Entry(edition).State =
                        EntityState.Modified;
                    newOrder++;
                }
            }
        }
        public void ReorderOnType(Edition edition, int lastOrder)
        {
            List<Edition> editions = null;
            int newOrder = 1;
            if (edition.Order < lastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = edition.Order + 1;
                editions = _context.Editions
                    .Where(e => e.AtHome &&
                        e.Order <= lastOrder && e.Order >= edition.Order &&
                        e.EditionType == edition.EditionType)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in editions)
                {
                    if (item.ID != edition.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = lastOrder;
                editions = _context.Editions
                    .Where(e => e.AtHome &&
                        e.Order >= lastOrder && e.Order <= edition.Order &&
                        e.EditionType == edition.EditionType)
                    .OrderBy(a => a.Order)
                    .ToList();
                foreach (var item in editions)
                {
                    if (item.ID != edition.ID)
                    {
                        item.Order = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
        }
        public void ReOrderMenuOnType(Edition edition, int lastOrder)
        {
            List<Edition> editions = null;
            int newOrder = 1;
            if (edition.OrderAtMenu < lastOrder)
            {
                // ej: 4 pasa a 2 (2 y 3 deben sumar 1)                
                newOrder = edition.OrderAtMenu + 1;
                editions = _context.Editions
                    .Where(e => e.OnMenu &&
                        e.OrderAtMenu <= lastOrder && e.OrderAtMenu >= edition.OrderAtMenu &&
                        e.EditionType == edition.EditionType)
                    .OrderBy(a => a.OrderAtMenu)
                    .ToList();
                foreach (var item in editions)
                {
                    if (item.ID != edition.ID)
                    {
                        item.OrderAtMenu = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }
            else
            {
                // ej: 2 pasa a 4(3 y 4 deben restar 1)
                newOrder = lastOrder;
                editions = _context.Editions
                    .Where(e => e.OnMenu &&
                        e.OrderAtMenu >= lastOrder && e.OrderAtMenu <= edition.OrderAtMenu &&
                        e.EditionType == edition.EditionType)
                    .OrderBy(a => a.OrderAtMenu)
                    .ToList();
                foreach (var item in editions)
                {
                    if (item.ID != edition.ID)
                    {
                        item.OrderAtMenu = newOrder;
                        _context.Entry(item).State =
                            EntityState.Modified;
                        newOrder++;
                    }
                }
            }

        }
    }
}
