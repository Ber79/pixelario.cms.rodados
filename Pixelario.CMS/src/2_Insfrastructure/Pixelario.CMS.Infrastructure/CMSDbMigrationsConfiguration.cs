﻿using System.Data.Entity.Migrations;

namespace Pixelario.CMS.Infrastructure
{
    public class CMSDbMigrationsConfiguration :
        DbMigrationsConfiguration<CMSContext>
    {
        public CMSDbMigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "CMSContextKey";
        }
        protected override void Seed(CMSContext context)
        {
            //base.Seed(context);
        }
    }
}
