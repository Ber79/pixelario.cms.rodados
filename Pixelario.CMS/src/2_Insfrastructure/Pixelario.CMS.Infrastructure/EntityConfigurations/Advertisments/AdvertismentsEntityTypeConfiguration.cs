﻿using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Advertisments
{
    public class AdvertismentsEntityTypeConfiguration : 
        EntityTypeConfiguration<Advertisment>
    {
        public AdvertismentsEntityTypeConfiguration() : base()
        {
            ToTable("Advertisments");
            HasKey<int>(s => s.ID);
            Property(a => a.ID)
                .HasColumnName("IDAdvertisment");
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(a => a.CreationDate)
                .IsRequired();           
            this.Property(a => a.PublicationStartDate);
            this.Property(a => a.PublicationEndDate);
            this.Property(a => a.Title)                
                .IsRequired();
            this.Property(a => a.Code);
            this.Property(a => a.URL);
            this.Property(a => a.Enabled)                
                .IsRequired();
            this.Property(a => a.Published)
               .IsRequired();
            this.Property(a => a.IDPlaceAtWeb)
               .IsRequired();
            this.Property(a => a.Order)
               .IsRequired();
            this.Property(a => a.HomeImage);
            this.Property(a => a.HorizontalImage);
            this.Property(a => a.VerticalImage);

            #region relaciones
            #endregion
        }
    }
}