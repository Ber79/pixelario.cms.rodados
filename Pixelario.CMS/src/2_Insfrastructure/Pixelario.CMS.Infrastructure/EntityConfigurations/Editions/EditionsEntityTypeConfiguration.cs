﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Editions
{
    public class EditionsEntityTypeConfiguration : 
        EntityTypeConfiguration<Edition>
    {
        public EditionsEntityTypeConfiguration() : base()
        {
            ToTable("Editions");
            HasKey<int>(s => s.ID);
            Property(e => e.ID)
                .HasColumnName("IDEdition");
            Property(e => e.Title)
                .HasColumnName("Title")
                .IsRequired();
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(e => e.CreationDate)
                .IsRequired();           
            this.Property(e => e.Summary)                
                .IsRequired();
            this.Property(e => e.Keywords);
            this.Property(e => e.EditionType)
                .IsRequired();
            this.Property(e => e.Enabled)                
                .IsRequired();
            this.Property(e => e.AtHome)
               .IsRequired();
            this.Property(e => e.Order)
               .IsRequired();

            this.Property(e => e.OnMenu)
                .IsRequired();
            this.Property(e => e.OrderAtMenu)
               .IsRequired();


            this.Property(e => e.HomeImage);
        }
    }
}