﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Configurations
{
    public class ConfigurationsEntityTypeConfiguration : 
        EntityTypeConfiguration<Configuration>
    {
        public ConfigurationsEntityTypeConfiguration() : base()
        {
            ToTable("Configurations");
            HasKey<int>(s => s.ID);
            Property(c => c.ID)
                .HasColumnName("IDConfiguration");
            Property(c => c.Title)
                .IsRequired();
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(c => c.CreationDate)
                .IsRequired();           
            this.Property(c => c.Title)                
                .IsRequired();
            this.Property(c => c.Key)
                .IsRequired();
            this.Property(c => c.State);
            this.Property(c => c.ConfigurationType)
                .HasColumnName("Type")
                .HasColumnType("int")
                .IsRequired();
            this.Property(c => c.Roles);
            #region relaciones
            this.HasMany<ConfigurationOption>(t => t.ConfigurationOptions)
                .WithRequired(s => s.Configuration)
                .HasForeignKey<int>(s => s.IDConfiguration);
            #endregion
        }
    }
}