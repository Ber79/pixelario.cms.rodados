﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Configurations
{
    public class ConfigurationOptionsEntityTypeConfiguration :
        EntityTypeConfiguration<ConfigurationOption>
    {
        public ConfigurationOptionsEntityTypeConfiguration() : base()
        {
            ToTable("ConfigurationOptions");
            HasKey<int>(s => s.ID);
            Property(s => s.ID)
                .HasColumnName("IDConfigurationOption");
            this.Property(s => s.CreatedBy)
                 .IsRequired();
            this.Property(s => s.CreationDate)
                .IsRequired();
            this.Property(s => s.Value)
                .IsRequired();
            this.Property(s => s.Enabled)
                .IsRequired();
            this.Property(s => s.Order)
               .IsRequired();
        }
    }
}