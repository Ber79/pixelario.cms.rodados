﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Articles
{
    public class ArticleScriptItemEntityTypeConfiguration : 
        EntityTypeConfiguration<ArticleScriptItem>
    {
        public ArticleScriptItemEntityTypeConfiguration() : base()
        {
            ToTable("ArticleScriptItems");
            HasKey<int>(m => m.ID);
            Property(m => m.ID)
                .HasColumnName("IDArticleScriptItem");
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(m => m.CreationDate)
                .IsRequired();           
            this.Property(m => m.IDArticleScript)
                .IsRequired();
            this.Property(m => m.IDMultimediaType)
                .IsRequired();
            this.Property(m => m.Title)                
                .IsRequired();
            this.Property(m => m.Code);
            this.Property(m => m.Enabled)                
                .IsRequired();
            this.Property(m => m.Order)
               .IsRequired();           
        }
    }
}