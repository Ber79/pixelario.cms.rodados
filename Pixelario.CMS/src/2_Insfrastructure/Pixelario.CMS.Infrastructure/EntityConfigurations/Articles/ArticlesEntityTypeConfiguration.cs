﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Articles
{
    public class ArticlesEntityTypeConfiguration : 
        EntityTypeConfiguration<Article>
    {
        public ArticlesEntityTypeConfiguration() : base()
        {
            ToTable("Articles");
            HasKey<int>(s => s.ID);
            Property(a => a.ID)
                .HasColumnName("IDArticle");
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(a => a.CreationDate)
                .IsRequired();           
            this.Property(a => a.IDEdition)
                .IsRequired();
            this.Property(a => a.PublicationDate);
            this.Property(a => a.Title)                
                .IsRequired();
            this.Property(a => a.Summary)
                .IsRequired();
            this.Property(a => a.Body)
                .IsRequired();
            this.Property(a => a.Source);
            this.Property(a => a.Keywords);
            this.Property(a => a.Enabled)                
                .IsRequired();
            this.Property(a => a.AtHome)
               .IsRequired();
            this.Property(a => a.IDPlaceAtHome)
               .IsRequired();
            this.Property(a => a.Order)
               .IsRequired();
            this.Property(a => a.OrderAtEdition)
               .IsRequired();
            this.Property(a => a.HomeImage);
            this.Property(a => a.SlideImage);
            this.Property(a => a.HorizontalImage);
            this.Property(a => a.VerticalImage);

            #region relaciones
            this.HasRequired<Edition>(a => a.Edition)
                .WithMany(e=>e.Articles)
                .HasForeignKey(a=>a.IDEdition);
            this.HasMany<Topic>(a => a.Topics)
                .WithMany(t => t.Articles)
                .Map(at =>
                {
                    at.MapLeftKey("IDArticle");
                    at.MapRightKey("IDTopic");
                    at.ToTable("ArticleTopics");
                });
            this.HasMany<SubTopic>(a => a.SubTopics)
                .WithMany(s => s.Articles)
                .Map(ast =>
                {
                    ast.MapLeftKey("IDArticle");
                    ast.MapRightKey("IDSubTopic");
                    ast.ToTable("ArticleSubTopics");
                });
            this.HasMany<ArticleMultimedia>(a => a.ArticleMultimedia)
                .WithRequired(m => m.Article)
                .HasForeignKey<int>(m => m.IDArticle);
            this.HasMany<ArticleScript>(a => a.Scripts)
                .WithMany(s => s.Articles)
                .Map(aas =>
                {
                    aas.MapLeftKey("IDArticle");
                    aas.MapRightKey("IDArticleScript");
                    aas.ToTable("ArticleArticleScripts");
                });

            #endregion



        }
    }
}