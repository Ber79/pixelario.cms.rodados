﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Articles
{
    public class ArticleScriptEntityTypeConfiguration : 
        EntityTypeConfiguration<ArticleScript>
    {
        public ArticleScriptEntityTypeConfiguration() : base()
        {
            ToTable("ArticleScripts");
            HasKey<int>(m => m.ID);
            Property(m => m.ID)
                .HasColumnName("IDArticleScript");
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(m => m.CreationDate)
                .IsRequired();           
            this.Property(m => m.Title)                
                .IsRequired();
            this.Property(m => m.Summary);
            this.Property(m => m.Enabled)                
                .IsRequired();
            this.Property(m => m.Order)
               .IsRequired();
            #region relaciones            
            this.HasMany<ArticleScriptItem>(ars => ars.Items)
                .WithRequired(asi => asi.ArticleScript)
                .HasForeignKey<int>(asi => asi.IDArticleScript);
            #endregion
        }
    }
}