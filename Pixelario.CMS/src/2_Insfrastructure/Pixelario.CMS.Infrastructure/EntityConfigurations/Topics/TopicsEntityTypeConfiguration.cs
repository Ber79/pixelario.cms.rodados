﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Topics
{
    public class TopicsEntityTypeConfiguration : 
        EntityTypeConfiguration<Topic>
    {
        public TopicsEntityTypeConfiguration() : base()
        {
            ToTable("Topics");
            HasKey<int>(s => s.ID);
            Property(t => t.ID)
                .HasColumnName("IDTopic");
            Property(t => t.Title)                
                .IsRequired();
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(t => t.CreationDate)
                .IsRequired();           
            this.Property(t => t.Summary)
                .IsRequired();
            this.Property(t => t.Keywords);
            this.Property(t => t.Enabled)
                .IsRequired();
            this.Property(t => t.AtHome)
               .IsRequired();
            this.Property(t => t.Order)
               .IsRequired();
            this.Property(t => t.HomeImage);
            this.Property(t => t.OnMenu)
                .IsRequired();
            this.Property(t => t.OrderAtMenu)
               .IsRequired();
            #region relaciones
            this.HasMany<Edition>(t => t.Editions)
                .WithMany(e => e.Topics)
                .Map(te =>
                {
                    te.MapLeftKey("IDTopic");
                    te.MapRightKey("IDEdition");
                    te.ToTable("TopicEditions");
                });
            this.HasMany<SubTopic>(t => t.SubTopics)
                .WithRequired(s => s.Topic)
                .HasForeignKey<int>(s => s.IDTopic);
            #endregion
        }
    }
}