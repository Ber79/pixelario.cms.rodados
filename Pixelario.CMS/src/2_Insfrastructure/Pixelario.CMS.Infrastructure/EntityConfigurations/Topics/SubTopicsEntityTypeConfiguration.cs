﻿using Pixelario.CMS.Domain.AggregatesModel.Topics;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Topics
{
    public class SubTopicsEntityTypeConfiguration :
        EntityTypeConfiguration<SubTopic>
    {
        public SubTopicsEntityTypeConfiguration() : base()
        {
            ToTable("SubTopics");
            HasKey<int>(s => s.ID);
            Property(s => s.ID)
                .HasColumnName("IDSubTopic");
            this.Property(s => s.CreatedBy)
                 .IsRequired();
            this.Property(s => s.CreationDate)
                .IsRequired();
            this.Property(s => s.Title)
                .IsRequired();
            this.Property(s => s.IDTopic)
                .IsRequired();
            this.Property(s => s.Summary)
                .IsRequired();
            this.Property(s => s.Keywords);
            this.Property(s => s.Enabled)
                .IsRequired();
            this.Property(s => s.AtHome)
               .IsRequired();
            this.Property(s => s.Order)
               .IsRequired();
            this.Property(s => s.HomeImage);
            this.Property(t => t.OnMenu)
                .IsRequired();
            this.Property(t => t.OrderAtMenu)
               .IsRequired();
        }
    }
}