﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Pages
{
    public class PagesEntityTypeConfiguration :
        EntityTypeConfiguration<Page>
    {
        public PagesEntityTypeConfiguration() : base()
        {
            ToTable("Pages");
            HasKey<int>(s => s.ID);
            Property(p => p.ID)
                .HasColumnName("IDPage");
            Property(p => p.Title)
                .IsRequired();
            this.Property(e => e.CreatedBy)
                .IsRequired();
            this.Property(p => p.CreationDate)
                .IsRequired();
            this.Property(p => p.Summary)
                .IsRequired();
            this.Property(p => p.Body)
                .IsRequired();
            this.Property(p => p.Keywords);
            this.Property(p => p.Enabled)
                .IsRequired();
            this.Property(p => p.AtHome)
               .IsRequired();
            this.Property(p => p.Order)
               .IsRequired();
            this.Property(p => p.HomeImage);
            this.Property(p => p.OnMenu)
                .IsRequired();
            this.Property(p => p.OrderAtMenu)
               .IsRequired();

            #region relaciones
            this.HasMany<Edition>(p => p.Editions)
                .WithMany(e => e.Pages)
                .Map(te =>
                {
                    te.MapLeftKey("IDPage");
                    te.MapRightKey("IDEdition");
                    te.ToTable("PageEditions");
                });
            this.HasMany<PageMultimedia>(a => a.PageMultimedia)
                .WithRequired(m => m.Page)
                .HasForeignKey<int>(m => m.IDPage);

            #endregion
        }
    }
}