﻿using Pixelario.CMS.Domain.AggregatesModel.Pages;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.CMS.Infrastructure.EntityConfigurations.Pages
{
    public class PageMultimediaEntityTypeConfiguration : 
        EntityTypeConfiguration<PageMultimedia>
    {
        public PageMultimediaEntityTypeConfiguration() : base()
        {
            ToTable("PageMultimedia");
            HasKey<int>(m => m.ID);
            Property(m => m.ID)
                .HasColumnName("IDMultimedia");
            this.Property(e=>e.CreatedBy)
                .IsRequired();
            this.Property(m => m.CreationDate)
                .IsRequired();           
            this.Property(m => m.IDPage)
                .IsRequired();
            this.Property(m => m.IDMultimediaType)
                .IsRequired();
            this.Property(m => m.Title)                
                .IsRequired();
            this.Property(m => m.Summary);
            this.Property(m => m.Path1);
            this.Property(m => m.Path2);
            this.Property(m => m.Enabled)                
                .IsRequired();
            this.Property(m => m.AtHome)
               .IsRequired();
            this.Property(m => m.Order)
               .IsRequired();
            this.Property(m => m.Cover)
               .IsRequired();

            #region relaciones
            #endregion



        }
    }
}