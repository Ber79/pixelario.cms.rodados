﻿using MediatR;
using Pixelario.CMS.Seedwork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Infrastructure
{
    static class MediatorExtension
    {
        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky        
        /// Función: Acciona asincronica que dispara los eventos de dominio
        /// antes del commit del modelo aggregate
        /// </summary>
        /// <param name="mediator">Mediador de eventos de dominio</param>
        /// <param name="context">Contexto de cuentas</param>
        /// <returns>Ejecución asincrona</returns>
        public static async Task DispatchBeforeDomainEventsAsync(this IMediator mediator,
            CMSContext context)
        {
            ///Busco todos las entidades del contexto que tienen eventos de dominio
            var domainEntites = context.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.BeforeCommitDomainEvents != null 
                    && x.Entity.BeforeCommitDomainEvents.Any());
            //Recolecto de las entidades con eventos todos su eventos
            var domainEvents = domainEntites.SelectMany(x => x.Entity.BeforeCommitDomainEvents)
                .ToList();
            //Limpio los eventos de las entidades del contexto
            domainEntites.ToList().ForEach(entity => entity.Entity.ClearBeforeDomainEvents());
            // IEnumerable de tareas de publicacion asincronicas
            var tasks = domainEvents.Select(
                async (domainEvent) =>
                {
                    await mediator.Publish(domainEvent);
                });
            //Cuando terminan todos los eventos de dominio termina el evento
            await Task.WhenAll(tasks);
        }


        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky        
        /// Función: Acciona asincronica que dispara los eventos de dominio
        /// despues del commit del modelo aggregate
        /// </summary>
        /// <param name="mediator">Mediador de eventos de dominio</param>
        /// <param name="context">Contexto de cuentas</param>
        /// <returns>Ejecución asincrona</returns>
        public static async Task DispatchAfterDomainEventsAsync(this IMediator mediator,
            CMSContext context)
        {
            ///Busco todos las entidades del contexto que tienen eventos de dominio
            var domainEntites = context.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.AfterCommitDomainEvents != null 
                    && x.Entity.AfterCommitDomainEvents.Any());
            //Recolecto de las entidades con eventos todos su eventos
            var domainEvents = domainEntites.SelectMany(x => x.Entity.AfterCommitDomainEvents)
                .ToList();
            //Limpio los eventos de las entidades del contexto
            domainEntites.ToList().ForEach(entity => entity.Entity.ClearAfterDomainEvents());
            // IEnumerable de tareas de publicacion asincronicas
            var tasks = domainEvents.Select(
                async (domainEvent) =>
                {
                    try
                    {
                        await mediator.Publish(domainEvent);
                    }
                    catch(Exception ex)
                    {
                        var m = ex.Message;
                        var i = ex.InnerException != null ? ex.InnerException.Message : "";
                    }
                    
                });
            //Cuando terminan todos los eventos de dominio termina el evento
            await Task.WhenAll(tasks);
        }
    }
}
