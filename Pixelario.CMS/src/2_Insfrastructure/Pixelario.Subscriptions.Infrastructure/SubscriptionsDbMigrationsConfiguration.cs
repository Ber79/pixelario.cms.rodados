﻿using System.Data.Entity.Migrations;

namespace Pixelario.Subscriptions.Infrastructure
{
    public class SubscriptionsDbMigrationsConfiguration :
        DbMigrationsConfiguration<SubscriptionsContext>
    {
        public SubscriptionsDbMigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "SubscriptionsContextKey";
        }
        protected override void Seed(SubscriptionsContext context)
        {
            //base.Seed(context);
        }
    }
}
