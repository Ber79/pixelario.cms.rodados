﻿using MediatR;
using Pixelario.CMS.Seedwork;
using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Pixelario.Subscriptions.Infrastructure.EntityConfigurations.Accounts;
using Pixelario.Subscriptions.Infrastructure.EntityConfigurations.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Infrastructure
{
    public class SubscriptionsContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "sbc";
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Constraint> Constraints { get; set; }
        private SubscriptionsContext() :
            base("SubscriptionsContext")
        {

        }
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        public SubscriptionsContext(ILogger logger,
            IMediator mediator) : base("SubscriptionsContext")
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountsEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ConstraintEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public async Task<SaveEntitiesResponse> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            var saveEntitiesResponse = new SaveEntitiesResponse();
            try
            {
                // Dispara los eventos de dominio antes de salvar los cambios
                // await _mediator.DispatchBeforeDomainEventsAsync(this);
                _logger.Debug("Saving change on CMSContext");
                saveEntitiesResponse.Success = await base.SaveChangesAsync() > 0;
                _logger.Debug("Dispaching after DomainEvents");
                await _mediator.DispatchAfterDomainEventsAsync(this);
                return saveEntitiesResponse;
            }
            catch (DbEntityValidationException ex)
            {
                _logger.Error(ex, "DbEntityValidationException catched");
                saveEntitiesResponse.Success = false;
                saveEntitiesResponse.Errors = new List<string>() { ex.Message };
                if (ex.EntityValidationErrors != null)
                {
                    foreach (var entityValidationError in ex.EntityValidationErrors)
                    {
                        var entryMessage = string.Format("Entry: {0}", entityValidationError.Entry);
                        foreach (var validationError in entityValidationError.ValidationErrors)
                        {
                            saveEntitiesResponse.Errors.Add(string.Format("{0} - Property: {1} - Error: {2}",
                                entryMessage, validationError.PropertyName,
                                validationError.ErrorMessage));
                        }
                    }
                }
                return saveEntitiesResponse;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                saveEntitiesResponse.Success = false;
                saveEntitiesResponse.Errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    saveEntitiesResponse.Errors.Add(ex.InnerException.Message);
                return saveEntitiesResponse;
            }
        }
    }
}
