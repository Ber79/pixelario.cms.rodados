USE [Subscriptions]
GO

/****** Object:  Table [dbo].[Constraints]    Script Date: 25/1/2020 10:52:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Constraints](
	[IDConstraint] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](256) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[IDPlan] [int] NOT NULL,
	[IDConstraintType] [int] NOT NULL,
	[IDConstrained] [int] NOT NULL,
 CONSTRAINT [PK_Constraints] PRIMARY KEY CLUSTERED 
(
	[IDConstraint] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


