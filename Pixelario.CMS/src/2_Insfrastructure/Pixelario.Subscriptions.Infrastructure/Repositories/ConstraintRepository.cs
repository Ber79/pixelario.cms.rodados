﻿using Pixelario.CMS.Seedwork;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Infrastructure.Repositories
{
    public class ConstraintRepository : IConstraintRepository
    {
        private readonly SubscriptionsContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public ConstraintRepository(SubscriptionsContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Constraint Add(Constraint constraint)
        {
            return _context.Constraints.Add(constraint);
        }
        public async Task<List<Constraint>> ListByConstrainedAsync(int iDConstraintType,
            int iDConstrained)
        {
            return await _context.Constraints
                .Where(c=> c.IDConstraintType == iDConstraintType && c.IDConstrained == iDConstrained)
                .ToListAsync();
        }
        public void Update(Constraint constraint)
        {
            _context.Entry(constraint).State = EntityState.Modified;
        }
        public void Delete(Constraint constraint)
        {
            _context.Constraints.Remove(constraint);
        }
    }
}
