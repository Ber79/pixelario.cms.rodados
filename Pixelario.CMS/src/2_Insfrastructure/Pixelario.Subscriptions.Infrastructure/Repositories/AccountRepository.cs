﻿using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Infrastructure.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly SubscriptionsContext _context;
        public IUnitOfWork UnitOfWork => _context;
        public AccountRepository(SubscriptionsContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            _context = context;
        }
        public Account Add(Account account)
        {
            return _context.Accounts.Add(account);
        }
        public async Task<Account> GetAsync(int iDAccount)
        {
            return await _context.Accounts.FindAsync(iDAccount);
        }
        public void Update(Account account)
        {
            _context.Entry(account).State = EntityState.Modified;
        }
        public void Delete(Account account)
        {
            _context.Accounts.Remove(account);
        }
    }
}
