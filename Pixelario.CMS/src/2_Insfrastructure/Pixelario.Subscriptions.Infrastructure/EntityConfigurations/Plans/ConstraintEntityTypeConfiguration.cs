﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.Subscriptions.Infrastructure.EntityConfigurations.Plans
{
    public class ConstraintEntityTypeConfiguration :
        EntityTypeConfiguration<Constraint>
    {
        public ConstraintEntityTypeConfiguration() : base()
        {
            ToTable("Constraints");
            HasKey<int>(s => s.ID);
            Property(e => e.ID)
                .HasColumnName("IDConstraint");
            this.Property(e => e.CreatedBy)
                .IsRequired();
            this.Property(e => e.CreationDate)
                .IsRequired();
            this.Property(e => e.IDPlan)
                .IsRequired();
            this.Property(e => e.IDConstraintType)
                .IsRequired();
            this.Property(e => e.IDConstrained)
                .IsRequired();
        }
    }
}