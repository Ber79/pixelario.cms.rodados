﻿using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using System.Data.Entity.ModelConfiguration;

namespace Pixelario.Subscriptions.Infrastructure.EntityConfigurations.Accounts
{
    public class AccountsEntityTypeConfiguration : 
        EntityTypeConfiguration<Account>
    {
        public AccountsEntityTypeConfiguration() : base()
        {
            ToTable("Accounts");
            HasKey<int>(s => s.ID);
            Property(e => e.ID)
                .HasColumnName("IDAccount");           
            this.Property(e => e.CreationDate)
                .IsRequired();
            this.Property(e => e.UserId)
                .IsRequired();
            this.Property(e => e.IDPlan)
                .IsRequired();
        }
    }
}