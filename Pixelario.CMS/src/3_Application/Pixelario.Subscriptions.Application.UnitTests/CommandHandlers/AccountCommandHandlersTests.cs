﻿using MediatR;
using Moq;
using Pixelario.CMS.Seedwork;
using Pixelario.Subscriptions.Application.CommandHandlers.Accounts;
using Pixelario.Subscriptions.Application.Commands.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Pixelario.Subscriptions.Application.UnitTest.Commands
{
    public class AccountCommandHandlersTests
    {
        private Mock<IAccountRepository> _accountRepositoryMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDAccount = 1;
        private int fakeUserId = 1;
        private Plan freePlan = Plan.Free;

        private Account fakeAccount()
        {
            var account = new Account(
                userId: fakeUserId,
                plan: freePlan);
            return account;
        }

        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                userId: fakeUserId,
                plan: freePlan);
            return command;
        }

        public AccountCommandHandlersTests()
        {
            _accountRepositoryMock = new Mock<IAccountRepository>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };

            return response;
        }
        [Fact]
        public async Task T001_AddCommandHandler_Return_False_When_DB_Not_Saves()
        {
            // Preparo el commando
            var command = fakeAddCommand();
            // Preparo el repositorio
            _accountRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddCommandHandler(
                _logger.Object,
                _mediator.Object,
                _accountRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
    }
}
