﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Seedwork;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Pixelario.Subscriptions.Application.UnitTest.Commands
{
    public class ConstraintCommandHandlersTests
    {
        private Mock<IConstraintRepository> _constraintRepositoryMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDConstraint = 1;
        private int fakeUserId = 1;
        private Plan freePlan = Plan.Free;
        private ConstraintType onEdition = ConstraintType.OnEdition;
        private string fakeCreatedBy = "ComandTestProyect";
        private string fakeEditionTitle = "Título de la edición";
        private string fakeEditionSummary = "Sumario de la edición";
        private string fakeEditionKeyword = "Keywords de la edición";
        private int fakeID = 1;
        
        private Constraint fakeConstraint()
        {
            var constraint = new Constraint(
                createdBy: fakeCreatedBy,
                plan: freePlan,
                constraintType: onEdition,
                iDConstrained: fakeID);            
            return constraint;
        }


        public ConstraintCommandHandlersTests()
        {
            _constraintRepositoryMock = new Mock<IConstraintRepository>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };

            return response;
        }
    }
}
