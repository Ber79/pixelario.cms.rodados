﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;

namespace Pixelario.Subscriptions.Application.Commands.Accounts
{
    public class AddCommand : IRequest<CommandResponse>
    {
        public int UserId { get; set; }
        public Plan Plan { get; set; }
        public AddCommand(int userId,
            Plan plan)
        {
            UserId = userId;
            Plan = plan ?? throw new ArgumentNullException(nameof(plan));
        }
    }
}