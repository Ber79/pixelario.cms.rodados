﻿using MediatR;
using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Application.Commands.Accounts;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.SeedWork;

namespace Pixelario.Subscriptions.Application.CommandHandlers.Accounts
{
    public class AddCommandHandler : IRequestHandler<AddCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IAccountRepository _accountRepository;
        public AddCommandHandler(
            ILogger logger,
            IMediator mediator,
            IAccountRepository accountRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _accountRepository = accountRepository ?? throw new ArgumentNullException(nameof(accountRepository));
        }
        public async Task<CommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("Starting to add an account with UserId \"{1}\"",
                    command.UserId);
                var account = new Account(
                    userId: command.UserId,
                    plan: command.Plan);
                _accountRepository.Add(account);
                var saved = await _accountRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Account with UserId \"{0}\" saved with id {1}",
                        command.UserId, account.ID);
                    response.success = true;
                    response.errors = null;
                }
                else
                {
                    _logger.Warning("Account with UserId {0} not created",
                            command.UserId);
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}