﻿using Dapper;
using Pixelario.Subscriptions.Application.QueryModels.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Application.Queries.Plans
{
    public class ConstraintsQueries : IConstraintsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public ConstraintsQueries(ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<int> CountAsync(Plan plan,
            Dictionary<int, int> entities)
        {
            _logger.Debug("Starting to query");
            try
            {
                var where = this.WhereForConstraintsBuilder(
                    plan: plan,
                    entities: entities);
                IEnumerable<int> result;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Constraints AS c "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        public Task<ConstraintQueryModel> GetAsync(int iDConstraint)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> GetConstrainedValueAsyn(int iDPlan, ConstraintType constraintType, int iDConstrained)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query =
                        @"SELECT COUNT(*)
                        FROM Constraints AS c 
                        WHERE c.IDConstraintType = @IDConstraintType 
                        AND c.IDPlan = @IDPlan
                        and c.IDConstrained = @IDConstrained";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query,
                        new {
                            IDPlan = iDPlan,
                            IDConstraintType = constraintType.Id,
                            IDConstrained  = iDConstrained
                        });
                    return result.FirstOrDefault() > 0;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return false;
            }

        }

        public async Task<List<ConstraintQueryModel>> ListAsync(Plan plan, 
            Dictionary<int,int> entities,
            int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereForConstraintsBuilder(
                    plan: plan,
                    entities: entities);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query =
                        @"SELECT c.IDConstraint AS IDConstraint, 
                        c.CreationDate AS CreationDate, 
                        c.IDConstraintType AS IDConstraintType, 
                        c.IDPlan AS IDPlan,
                        c.IDConstrained AS IDConstrained
                        FROM Constraints AS c " +
                        where +
                        " ORDER BY " + columnOrder + " " + orderDirection +
                        @" OFFSET @rowIndex ROWS       
                        FETCH NEXT @rowCount ROWS ONLY";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                            new
                            {
                                rowIndex,
                                rowCount
                            });
                    var constraintsList = new List<ConstraintQueryModel>();
                    foreach (var item in result)
                    {
                        constraintsList.Add(new ConstraintQueryModel()
                        {
                            IDConstraint = item.IDConstraint,
                            CreationDate = item.CreationDate,
                            ConstraintType = ConstraintType.From(item.IDConstraintType),
                            Plan = Plan.From(item.IDPlan),
                            IDConstrained = item.IDConstrained
                        });
                    }
                    return constraintsList;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private string WhereForConstraintsBuilder(Plan plan, Dictionary<int, int> entities)
        {
            List<string> whereStatments = new List<string>();
            if (plan != null)
            {
                whereStatments.Add($"c.IDPlan = {plan.Id}");
            }
            if (entities != null)
            {
                List<string> whereEntityStatments = new List<string>();
                foreach (var entity in entities)
                {
                    whereEntityStatments.Add($"(c.IDConstraintType = {entity.Key} AND c.IDConstrained = {entity.Value})");
                }
                whereStatments.Add(String.Join(" OR ", whereEntityStatments));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }
            return where;
        }
    }
}
