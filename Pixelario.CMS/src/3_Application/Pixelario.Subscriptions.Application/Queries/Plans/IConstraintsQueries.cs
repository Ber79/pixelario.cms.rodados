﻿using Pixelario.Subscriptions.Application.QueryModels.Plans;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Application.Queries.Plans
{
    public interface IConstraintsQueries
    {
        Task<ConstraintQueryModel> GetAsync(int iDConstraint);
        Task<List<ConstraintQueryModel>> ListAsync(
            Plan plan, Dictionary<int, int> entities,
            int rowIndex, int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(
            Plan plan, Dictionary<int, int> entities);
        Task<bool> GetConstrainedValueAsyn(
            int iDPlan,
            ConstraintType constraintType,
            int iDConstrained);

    }
}
