﻿using Pixelario.Subscriptions.Application.QueryModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Application.Queries.Accounts
{
    public interface IAccountsQueries
    {
        Task<AccountQueryModel> GetAsync(int iDAccount);
        Task<AccountQueryModel> GetByUserIdAsync(int userId);
        Task<List<AccountQueryModel>> ListAsync(
            List<Plan> plans,
            int rowIndex, int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(
            List<Plan> plans);
    }
}
