﻿using Dapper;
using Pixelario.Subscriptions.Application.QueryModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Application.Queries.Accounts
{
    public class AccountsQueries : IAccountsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public AccountsQueries(ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<int> CountAsync(List<Plan> plans)
        {
            _logger.Debug("Starting to query");
            try
            {
                var where = this.WhereForAccountsBuilder(
                    plans: plans);
                IEnumerable<int> result;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Accounts AS a "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        public Task<AccountQueryModel> GetAsync(int iDAccount)
        {
            throw new NotImplementedException();
        }
        public async Task<AccountQueryModel> GetByUserIdAsync(int userId)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query =
                        @"SELECT a.IDAccount AS IDAccount, 
                        a.CreationDate AS CreationDate, 
                        a.UserId AS UserId, 
                        a.IDPlan AS IDPlan
                        FROM Accounts AS a 
                        WHERE a.UserId = @userId";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                            new
                            {
                                userId
                            });
                    var list = result.AsList();
                    if (list != null && list.Count() > 0)
                    {
                        var accountQueryModel = new AccountQueryModel()
                        {
                            IDAccount = list[0].IDAccount,
                            CreationDate = list[0].CreationDate,
                            UserId = list[0].UserId,
                            Plan = Plan.From(list[0].IDPlan)
                        };
                        return accountQueryModel;
                    }                   
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
            }
            return null;

        }

        public async Task<List<AccountQueryModel>> ListAsync(List<Plan> plans, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereForAccountsBuilder(
                    plans: plans);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query =
                        @"SELECT a.IDAccount AS IDAccount, 
                        a.CreationDate AS CreationDate, 
                        a.UserId AS UserId, 
                        a.IDPlan AS IDPlan
                        FROM Accounts AS a " +
                        where +
                        " ORDER BY " + columnOrder + " " + orderDirection +
                        @" OFFSET @rowIndex ROWS       
                        FETCH NEXT @rowCount ROWS ONLY";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                            new
                            {
                                rowIndex,
                                rowCount
                            });
                    var accountsList = new List<AccountQueryModel>();
                    foreach (var item in result)
                    {
                        accountsList.Add(new AccountQueryModel()
                        {
                            IDAccount = item.IDAccount,
                            CreationDate = item.CreationDate,
                            UserId = item.UserId,
                            Plan = Plan.From(item.IDPlan)
                        });
                    }
                    return accountsList;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private string WhereForAccountsBuilder(List<Plan> plans)
        {
            List<string> whereStatments = new List<string>();
            if (plans != null && plans.Count > 0)
            {
                var plansWhere = new List<string>();
                foreach (var plan in plans)
                {
                    plansWhere.Add($"a.IDPlan = {plan.Id}");
                }
                whereStatments.Add(string.Format("({0})",
                    string.Join(" OR ", plansWhere)));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }
            return where;
        }
    }
}
