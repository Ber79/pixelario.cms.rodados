﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;

namespace Pixelario.Subscriptions.Application.QueryModels.Plans
{
    public class ConstraintQueryModel
    {
        public int IDConstraint { get; set; }
        public DateTime CreationDate { get; set; }
        public Plan Plan { get; set; }
        public ConstraintType ConstraintType { get; set; }
        public int IDConstrained { get; set; }
    }
}
