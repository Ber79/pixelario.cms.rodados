﻿using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;

namespace Pixelario.Subscriptions.Application.QueryModels.Accounts
{
    public class AccountQueryModel
    {
        public int IDAccount { get; set; }
        public DateTime CreationDate { get; set; }
        public int UserId { get; set; }
        public Plan Plan { get; set; }

    }
}
