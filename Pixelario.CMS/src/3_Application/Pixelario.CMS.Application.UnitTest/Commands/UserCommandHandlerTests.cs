﻿using MediatR;
using Moq;
using Microsoft.AspNet.Identity;
using Pixelario.CMS.Application.CommandHandlers.Users;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class UserCommandHandlerTests
    {
        private readonly Mock<IUserStore<ApplicationUser, int>> _applicationUserStore;

        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private Mock<ApplicationUserManager> _userManager;

        private int fakeUserId = 2;
        private int fakeAdminUserId = 1;
        private string fakeCreatedBy = "admin";
        private string fakeUserName = "UserName";
        private string fakeEmail = "username@pixelario.com.ar";
        private string fakePassword = "password";
        private string fakeFirstName = "name";
        private string fakeLastName = "last name";
        private string fakePicture = "/picture.jpg";

        private string[] fakeRolesSeleccionados = {
            "Super Administrador", "admin"};
        private string[] fakeRolesSeleccionadosSinAdmin
            = { "admin"};
        
        private AddCommand fakeAddCommand()
        {
            return new AddCommand(
                userName: fakeUserName,
                email: fakeEmail,
                password: fakePassword,
                firstName: fakeFirstName,
                lastName: fakeLastName
                );
        }
        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                userId: fakeUserId,
                firstName: fakeFirstName,
                lastName: fakeLastName);
            command.SetSolicitedBy(
                solicitedBy: fakeCreatedBy);
            return command;
        }
        private SetRolesCommand fakeSetRolesCommand()
        {
            return new SetRolesCommand(
                userId: fakeUserId,
                roles: fakeRolesSeleccionados
                );
        }
        private SetRolesCommand fakeRolesSinSuperAdminCommand()
        {
            return new SetRolesCommand(
                userId: fakeAdminUserId,
                roles: fakeRolesSeleccionadosSinAdmin
                );
        }
        private ChangePasswordCommand fakeChangePasswordCommand()
        {
            return new ChangePasswordCommand(
                userId: fakeAdminUserId,
                password: fakePassword
                );
        }
        //private ChangePictureCommand fakeChangePictureCommand()
        //{
        //    return new ChangePictureCommand(
        //        userId: fakeUserId,
        //        picture: fakePicture);
        //}
        public UserCommandHandlerTests()
        {
            _applicationUserStore = new Mock<IUserStore<ApplicationUser, int>>(
                MockBehavior.Strict);
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
            _userManager = new Mock<ApplicationUserManager>(
                _applicationUserStore.Object);
        }

        [Fact]
        public async Task T001_SetRolesCommandHandler_Return_False_When_Not_Find_User()
        {
            // Preparo el commando
            var command = fakeRolesSinSuperAdminCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el user store
            //_userManager.Setup(manager =>
            //    manager.(
            //        It.IsAny<ApplicationUser>()))
            //    .Returns(Task.FromResult(new IdentityResult(
            //        new List<string>() { "Ocurrio un error"})));
            var userManager = new ApplicationUserManager(
                _applicationUserStore.Object);
            // preparo el handler
            var handler = new SetRolesCommandHandler(
                _logger.Object,
                _mediator.Object,
                _userManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }



        [Fact]
        public async Task T002_Handler_De_Roles_Retorna_False_Cuando_No_Encuentra_Un_Usuario()
        {
            // Preparo el commando
            var command = fakeSetRolesCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el store
            _userManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationUser>(null));
            var userManager = new ApplicationUserManager(
                _applicationUserStore.Object);
            // preparo el handler
            var handler = new SetRolesCommandHandler(
                _logger.Object,
                _mediator.Object,
                _userManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T003_Handler_De_Roles_Retorna_False_Cuando_Intenta_Quitar_El_Role_Super_Administrado_A_Admin()
        {
            // Preparo el commando
            var command = fakeSetRolesCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el store
            _userManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationUser>(null));
            var userManager = new ApplicationUserManager(
                _applicationUserStore.Object);
            // preparo el handler
            var handler = new SetRolesCommandHandler(
                _logger.Object,
                _mediator.Object,
                _userManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_Handler_De_Password_Retorna_False_Cuando_No_Encuentra_Un_Usuario()
        {
            // Preparo el commando
            var command = fakeChangePasswordCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el store
            _userManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationUser>(null));
            // preparo el handler
            var handler = new ChangePasswordCommandHandler(_logger.Object,
                _mediator.Object,
                _userManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T005_UpdateCommandHandler_Return_False_When_Not_Find_User()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            // preparo el resultado en el store
            _userManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationUser>(null));
            // preparo el handler
            var handler = new UpdateCommandHandler(
                _logger.Object,
                _mediator.Object,
                _userManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        //[Fact]
        //public async Task T006_ChangePictureCommandHandler_Return_False_When_Not_Find_User()
        //{
        //    // Preparo el commando
        //    var command = fakeChangePictureCommand();
        //    // preparo el resultado en el store
        //    _applicationUserStore
        //        .Setup(store=> store.FindByIdAsync(It.IsAny<int>()))
        //        .Returns(Task.FromResult<ApplicationUser>(null));
        //    var userManager = new ApplicationUserManager(_applicationUserStore.Object);
        //    //_userManager.Setup(manager =>
        //    //    manager.FindByIdAsync(It.IsAny<int>()))
        //    //    .Returns(Task.FromResult<ApplicationUser>(null));
        //    // preparo el handler
        //    var handler = new ChangePictureCommandHandler(
        //        _logger.Object,
        //        _mediator.Object,
        //        userManager);
        //    var cltToken = new System.Threading.CancellationToken();
        //    var result = await handler.Handle(command, cltToken);
        //    // pruebo que la respuesta contiene el success en falso y errores
        //    Assert.False(result.success);
        //    Assert.NotEmpty(result.errors);
        //}

    }
}
