﻿using MediatR;
using Moq;
using Microsoft.AspNet.Identity;
using Pixelario.CMS.Application.CommandHandlers.Roles;
using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Domain;
using System.Threading.Tasks;
using Xunit;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class RoleCommandHandlerTests
    {
        private readonly Mock<IRoleStore<ApplicationRole,int>> _applicationRoleStore;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private Mock<ApplicationRoleManager> _roleManager;

        private string fakeCreatedBy = "admin";
        private string fakeRoleName = "RoleName";
        private int fakeRoleId = 1;

        private ApplicationRole fakeSuperAdministradorRole()
        {
            return new ApplicationRole()
            {
                Name= "Super Administrador"
            };
        }
        private ApplicationRole fakeRole()
        {
            return new ApplicationRole()
            {
                Name = "fake role"
            };
        }
        private AddCommand fakeAddCommand()
        {
            return new AddCommand(
                roleName: fakeRoleName
                );
        }
        private DeleteCommand fakeDeleteCommand()
        {
            return new DeleteCommand(
               roleId: fakeRoleId
           );
        }
        private UpdateCommand fakeUpdateCommand()
        {
            return new UpdateCommand(
                roleId: fakeRoleId,
                roleName: fakeRoleName
            );
        }
        
        public RoleCommandHandlerTests()
        {
            
            _applicationRoleStore = new Mock<IRoleStore<ApplicationRole, int>>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
            _roleManager = new Mock<ApplicationRoleManager>();
        }

        [Fact]
        public async Task T001_Handler_De_Borrar_Retorna_False_Cuando_No_Encuentra_Un_Role()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el store
            _roleManager.Setup(store =>
                store.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationRole>(null));           
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _mediator.Object,
                _roleManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }


        [Fact]
        public async Task T002_Handler_De_Borrar_Retorna_False_Cuando_Intenta_Borrar_Super_Admin_Role()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el manager
            var role = fakeSuperAdministradorRole();
            _roleManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationRole>(role));
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _mediator.Object,
                _roleManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T003_Handler_De_Editar_Retorna_False_Cuando_No_Encuentra_Un_Role()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el store
            _roleManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationRole>(null));
            // preparo el handler
            var handler = new UpdateCommandHandler(
                _logger.Object,
                _mediator.Object,
                _roleManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        public async Task T004_Handler_De_Editar_Retorna_False_Cuando_Intenta_Borrar_Super_Admin_Role()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // preparo el resultado en el manager
            var role = fakeSuperAdministradorRole();
            _roleManager.Setup(manager =>
                manager.FindByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<ApplicationRole>(role));
            // preparo el handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _mediator.Object,
                _roleManager.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

    }

}
