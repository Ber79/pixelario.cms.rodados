﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class ConfigurationCommandHandlerTests
    {
        private Mock<IConfigurationRepository> _configurationRepositoryMock;
        private Mock<IConfigurationCache> _configurationCacheMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDConfiguration = 1;
        private string fakeCreatedBy = "test";
        private string fakeSolicitedBy = "test";
        private string fakeTitle = "Fake title";
        private string fakeNewTitle = "Fake new title";
        private string fakeKey = "Fake-key";
        private string fakeNewKey = "Fake-New-key";
        private string fakeState = "1";
        private string[] fakeRoles = new  string[] { "fakeRole1", "fake Role 2" };

        private Configuration fakeConfiguration()
        {
            var configuration = new Configuration(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText);
            return configuration;
        }

        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText
                );
            command.SetCreatedBy(fakeCreatedBy);
            return command;
        }
        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                iDConfiguration: default(int),
                title: fakeNewTitle,
                key: fakeNewKey,
                configurationType: ConfigurationType.AreaText
                );
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }

        private SetStateCommand fakeSetStateCommand()
        {
            var command = new SetStateCommand(
                key: default(string),
                state: fakeState);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private BindRolesCommand fakeBindRolesCommand()
        {
            var command = new BindRolesCommand(
                iDConfiguration: default(int),
                roles: fakeRoles);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private DeleteCommand fakeDeleteCommand()
        {
            return new DeleteCommand(
                iDConfiguration: fakeIDConfiguration);
        }


        public ConfigurationCommandHandlerTests()
        {
            _configurationRepositoryMock = new Mock<IConfigurationRepository>();
            _configurationCacheMock = new Mock<IConfigurationCache>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };
            return response;
        }
        [Fact]
        public async Task T001_Handler_De_Creacion_Retorna_False_Cuando_No_Agrega_Una_Configuration()
        {
            // Preparo el commando
            var command = fakeAddCommand();
            command.SetCreatedBy(fakeCreatedBy);
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddCommandHandler(
                _logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T002_Handler_De_Editar_Retorna_False_Cuando_No_Exista_Una_Configuration()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();            
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Configuration>(null));
            // preparo el handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T003_Handler_De_Edita_Una_Configuration()
        {
            // Preparo la configuración
            var configuration = fakeConfiguration();
            // Preparo el commando
            var command = fakeUpdateCommand();
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Configuration>(configuration));
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errore y con los cambios
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeNewTitle, configuration.Title);
            Assert.Equal(fakeNewKey, configuration.Key);
            Assert.Equal(ConfigurationType.AreaText, configuration.ConfigurationType);
        }
        [Fact]
        public async Task T004_Handler_Establecer_Estado_Retorna_False_Cuando_No_Exista_Una_Configuration()
        {
            // Preparo el commando
            var command = fakeSetStateCommand();
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Configuration>(null));
            // preparo el handler
            var handler = new SetStateCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T005_Handler_Establece_El_Estado_Una_Configuration()
        {
            // Preparo la configuración
            var configuration = fakeConfiguration();
            // Preparo el commando
            var command = fakeSetStateCommand();
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<Configuration>(configuration));
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new SetStateCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errore y con los cambios
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeState, configuration.State);            
        }



        [Fact]
        public async Task T006_Handler_De_Borrar_Retorna_False_Cuando_No_Borra_Una_Configuration()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T007_Handler_De_Borrar_Retorna_False_Cuando_No_Exista_Una_Configuration()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Configuration>(null));
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T008_Handler_Bind_Roles_To_Configuration()
        {
            // Preparo la configuración
            var configuration = fakeConfiguration();
            // Preparo el commando
            var command = fakeBindRolesCommand();
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Configuration>(configuration));
            // Preparo el repositorio
            _configurationRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new BindRolesCommandHandler(_logger.Object,
                _configurationCacheMock.Object, _configurationRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errore y con los cambios
            Assert.True(result.success);
            Assert.Null(result.errors);
            foreach (var fakeRole in fakeRoles)
            {
                Assert.Contains(fakeRole, configuration.GetBindedRoles());
            }
        }

    }
}
