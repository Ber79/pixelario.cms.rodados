﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.Queries.Advertisments;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class AdvertismentCommandHandlerTests
    {
        private Mock<IAdvertismentRepository> _advertismentRepositoryMock;
        private Mock<IAdvertismentsQueries> _advertismentsQueriesMock;
        private Mock<IAdvertismentCache> _advertismentsCacheMock;

        private Mock<IImageServices> _imageServicesMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDAdvertisment = 1;
        private int fakeIDEdition = 1;
        private string fakeCreatedBy = "admin";
        private string fakeSolicitedBy = "admin";
        private DateTime? fakePublicationDate = DateTime.Now.AddDays(1);
        private string fakeTitle = "Titulo del artículo";
        private string fakeCode = "Summary del artículo";
        private string fakeKeywords = "keyword1; keyword2; keyword3";
        private string fakeUrl = "Cuerpo del artículo";
        private string fakeSource = "Fake source";
        private string fakeFileName = "test.jpg";
        private string fakeExternalURL = "http://www.myfakeurl.com";
        private string fakeDocumentFileName = "test.docx";
        private string fakeAppDomainAppPath = "c:\\dev\\app\\";
        private string fakeWidgetCode = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/oM1c1MH0q7E\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";
        private int fakeOrder = 1;
        private string fakeURI = "/fake/uri.jpg";
        private List<int> fakeTemasSeleccionados =
            new List<int> { 1, 2 };
        private List<int> fakeSubTemasSeleccionados =
            new List<int> { 1, 2 };
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };
            return response;          
        }


        private Advertisment fakeAdvertisment()
        {
            return new Advertisment(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                code: fakeCode,
                url: fakeUrl);
        }
        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                title: fakeTitle,
                code: fakeCode,
                url: fakeUrl);
            command.SetCreatedBy(fakeCreatedBy);
            return command;
        }
        private EnableCommand fakeEnableCommand()
        {
            var command = new EnableCommand(
                iDAdvertisment: fakeIDAdvertisment);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private PublishCommand fakePublishCommand()
        {
            var command = new PublishCommand(
                iDAdvertisment: fakeIDAdvertisment);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private ChangeOrderCommand fakeChangeOrderCommand()
        {
            var command = new ChangeOrderCommand(
                iDAdvertisment: fakeIDAdvertisment,
                order: fakeOrder);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private ChangePlaceAtWebCommand fakeChangePlaceAtWebCommand(AdvertismentPlaceAtWeb placeAtWeb)
        {
            var command = new ChangePlaceAtWebCommand(
                iDAdvertisment: default(int),
                placeAtWeb: placeAtWeb.Id);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                iDAdvertisment: fakeIDAdvertisment,
                title: fakeTitle,
                code: fakeCode,
                url: fakeUrl);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }

        private DeleteCommand fakeDeleteCommand()
        {
            var command = new DeleteCommand(
                iDAdvertisment: fakeIDAdvertisment);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }

        private ChangeImageCommand fakeChangeImageCommand(ImageType imageType)
        {
            var command = new ChangeImageCommand(
                iDAdvertisment: fakeIDAdvertisment,
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private RemoveImageCommand fakeRemoveImageCommand(ImageType imageType)
        {
            var command = new RemoveImageCommand(
                iDAdvertisment: fakeIDAdvertisment,
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        public AdvertismentCommandHandlerTests()
        {
            _advertismentRepositoryMock = new Mock<IAdvertismentRepository>();
            _advertismentsQueriesMock = new Mock<IAdvertismentsQueries>();
            _advertismentsCacheMock = new Mock<IAdvertismentCache>();
            _imageServicesMock = new Mock<IImageServices>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }

        [Fact]
        public async Task T001_Handler_Return_False_When_Cannot_Save_Advertisment()
        {
            // Preparo el commando
            var command = fakeAddCommand();           
            // Preparo el repositorio
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
                
        [Fact]
        public async Task T002_EnableCommandHandler_Return_False_When_Cant_Save()
        {
            // Making the command
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Making and executing the handler
            var handler = new EnableCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T003_PublishCommandHandler_Return_False_When_Cant_Save()
        {
            // Making the command
            var command = fakePublishCommand();
            // Mocking repository
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Moking queries
            _advertismentsQueriesMock.Setup(service =>
                service.CountAsync(It.IsAny<AdvertismentPlaceAtWeb>(),                            
                            It.IsAny<bool?>(),
                            It.IsAny<bool?>(),
                            It.IsAny<string>())).Returns(Task.FromResult(0));
                
            // Making and executing the handler
            var handler = new PublishCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object,
                _advertismentsQueriesMock.Object
                );
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // Testing first case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            // Moking again
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // Testing second case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_ChangeImageCommand_Change_Correctly_Image_On_Advertisment()
        {
            // Making the advertise
            var asvertisment = fakeAdvertisment();
            // Making the command
            var command = fakeChangeImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Mocking Services and Repositories
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _advertismentRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(asvertisment));
            _advertismentRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object,
                _advertismentRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/advertisments/80/{0}", fakeFileName),
                asvertisment.HomeImage);
        }

        [Fact]
        public async Task T005_RemoveImageCommand_Remove_Correctly_Image_On_Advertisment()
        {
            // Making the advertisment
            var asvertisment = fakeAdvertisment();
            asvertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: string.Format("/uploads/advertisments/{0}",
                    fakeFileName)
                );
            // Making the command
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Moking repository
            _advertismentRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(asvertisment));
            _advertismentRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object,
                _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(asvertisment.HomeImage);
        }

        [Fact]
        public async Task T006_ChangeOrderCommand_Return_False_When_Cant_Save()
        {
            // Making the command
            var command = fakeChangeOrderCommand();
            // Mocking repository
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Making and executing the handler
            var handler = new ChangeOrderCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // Testing first case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            // Moking again
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // Testing second case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);            
        }
        [Fact]
        public async Task T007_UpdateCommand_Return_False_When_Cant_Save()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            // Mocking repository
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Making and executing the handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // Testing first case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            // Moking again
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // Testing second case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T008_ChangePlaceAtWebCommand_Return_False_When_Cant_Save()
        {
            // Making the command
            var command = fakeChangePlaceAtWebCommand(
                AdvertismentPlaceAtWeb.Aside);
            // Mocking repository
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Making and executing the handler
            var handler = new ChangePlaceAtWebCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // Testing first case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            // Moking again
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // Testing second case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);

        }

        [Fact]
        public async Task T009_DeleteCommand_Return_False_When_Cant_Save()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            // Mocking repository
            _advertismentRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Making and executing the handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _advertismentsCacheMock.Object, _advertismentRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // Testing first case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
            // Moking again
            _advertismentRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Advertisment>(null));
            result = await handler.Handle(command, cltToken);
            // Testing second case
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);

        }
    }
}
