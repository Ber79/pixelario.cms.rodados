﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class PageCommandHandlerTests
    {
        private Mock<IPageRepository> _pageRepositoryMock;
        private Mock<IPageCache> _pageCacheMock;
        private Mock<IPageMultimediaCache> _pageMultimediaCacheMock;
        private Mock<IEditionRepository> _editionRepositoryMock;
        private Mock<IImageServices> _imageServicesMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDPage = 1;
        private int fakeIDSubPage = 1;

        private string fakeCreatedBy = "unitest";
        private string fakeSolicitedBy = "test";

        private string fakeTitle = "Titulo falso";
        private string fakeSummary = "Sumario falso";
        private string fakeBody = "Cuerpo falso";
        private string fakeKeywords = "keyword1; keyword2; keyword3";

        private string fakeEditedTitle = "Titulo falso editado";
        private string fakeEditedSummary = "Sumario falso editado";
        private string fakeEditedBody = "Cuerpo falso editado";
        private string fakeEditedKeywords = "keyword4; keyword5; keyword6";
        private int fakeOrder = 1;
        private string fakeFileName = "test.jpg";
        private string fakeDocumentFileName = "test.docx";
        private string fakeExtention = ".docx";
        private string fakeAppDomainAppPath = "c:\\dev\\app\\";
        private string fakeWidgetCode = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/oM1c1MH0q7E\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";


        private List<int> fakeSelectedEditions =
            new List<int> { 1, 2 };
        private Edition fakeEdition()
        {
            return new Edition(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeSummary);
        }
        private Page fakePage()
        {
            return new Page(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                keywords: fakeSummary);
        }
        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                keywords: fakeKeywords,
                editions: fakeSelectedEditions);
            command.SetCreatedBy(
                createdBy: fakeCreatedBy);
            return command;
        }
        private EnableCommand fakeEnableCommand()
        {
            var command = new EnableCommand(
                iDPage: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ToHomeCommand fakeToHomeCommand()
        {
            var command = new ToHomeCommand(
                iDPage: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ToMenuCommand fakeToMenuCommand()
        {
            var command = new ToMenuCommand(
                iDPage: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private OrderAtHomeCommand fakeOrderAtHomeCommand()
        {
            var command = new OrderAtHomeCommand(
                iDPage: default(int),
                order: fakeOrder);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }
        private OrderAtMenuCommand fakeOrderAtMenuCommand()
        {
            var command = new OrderAtMenuCommand(
                iDPage: default(int),
                order: fakeOrder);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }
        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                iDPage: fakeIDPage,
                title: fakeEditedTitle,
                summary: fakeEditedSummary,
                body: fakeEditedBody,
                keywords: fakeEditedKeywords,
                editions: fakeSelectedEditions);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private DeleteCommand fakeDeleteCommand()
        {
            var command= new DeleteCommand(
                iDPage: fakeIDPage);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private ChangeImageCommand fakeChangeImageCommand(ImageType imageType)
        {
            var command = new ChangeImageCommand(
                iDPage: fakeIDPage,
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private RemoveImageCommand fakeRemoveImageCommand(ImageType imageType)
        {
            var command = new RemoveImageCommand(
                iDPage: fakeIDPage,
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private AddMultimediaCommand fakeAddMultimediaCommand(
          string sumario, string fileName1, string fileName2,
          MultimediaType multimediaType)
        {
            var command = new AddMultimediaCommand(
                iDPage: fakeIDPage,
                title: fakeTitle,
                summary: sumario,
                appDomainAppPath: fakeAppDomainAppPath,
                path1: fileName1,
                path2: fileName2,
                multimediaType: multimediaType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private RemoveMultimediaCommand fakeRemoveMultimediaCommand()
        {
            var command = new RemoveMultimediaCommand(
                iDPage: fakeIDPage,
                iDMultimedia: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        public PageCommandHandlerTests()
        {
            _pageRepositoryMock = new Mock<IPageRepository>();
            _pageCacheMock = new Mock<IPageCache>();
            _pageMultimediaCacheMock = new Mock<IPageMultimediaCache>();
            _editionRepositoryMock = new Mock<IEditionRepository>();
            _imageServicesMock = new Mock<IImageServices>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };

            return response;
        }
        [Fact]
        public async Task T001_Handler_De_Creacion_Retorna_False_Cuando_No_Agrega_Una_Pagina()
        {
            // Preparo el commando
            var command = fakeAddCommand();
            // Preparo los repositorio
            _pageRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            var edicion = fakeEdition();
            _editionRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(edicion));
            // preparo el handler
            var handler = new AddCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T002_Handler_Habilita_La_Pagina()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new EnableCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errores y con habilitado en true
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(page.Enabled);
        }
        [Fact]
        public async Task T003_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Pagina()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new EnableCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_Handler_De_Habilitacion_Retorna_False_Cuando_No_Exista_Una_Pagina()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(null));
            // preparo el handler
            var handler = new EnableCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T005_Handler_Cambia_En_Portada_La_Pagina()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeToHomeCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new ToHomeCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errores y con portada en true
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(page.AtHome);
        }
        [Fact]
        public async Task T006_Handler_Cambia_De_Orden_La_Pagina()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeOrderAtHomeCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new OrderAtHomeCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeOrder, page.Order);
        }
        [Fact]
        public async Task T007_Handler_Borrar_Retorna_False_Cuando_No_Borra_Una_Pagina()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T008_Handler_De_Borrar_Retorna_False_Cuando_No_Exista_Una_Pagina()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(null));
            // preparo el handler
            var handler = new DeleteCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T009_Handler_Edita_La_Tema()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeUpdateCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            _editionRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(fakeEdition()));
            // preparo el handler
            var handler = new UpdateCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);

            Assert.Equal(fakeEditedTitle, page.Title);
            Assert.Equal(fakeEditedSummary, page.Summary);
            Assert.Equal(fakeEditedBody, page.Body);
            Assert.Equal(fakeEditedKeywords, page.Keywords);
        }
        [Fact]
        public async Task T010_Handler_Cambia_Imagen_De_Portada_De_Una_Pagina()
        {
            // Preparo el articulo
            var page = fakePage();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Home
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(
                _logger.Object,
                _pageCacheMock.Object,
                _pageRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // sin errores y la imagen en la propiedad.
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/pages/80/{0}", fakeFileName),
                    page.HomeImage);
        }
        [Fact]
        public async Task T011_Handler_Cambia_Imagen_De_Portada_De_Una_Edition_Da_Error_En_Tipo_No_Disponible()
        {
            // Preparo el articulo
            var page = fakePage();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Vertical // Tipo no disponible
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(
                _logger.Object,
                _pageCacheMock.Object,
                _pageRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T012_Handler_Quita_Imagen_Vertical_De_Un_Page()
        {
            // Preparo el articulo
            var page = fakePage();
            page.ChangeImage(
                imageType: ImageType.Home,
                uri: string.Format("/uploads/pages/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeSolicitedBy);
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(
                _logger.Object,
                _pageCacheMock.Object,
                _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(page.HomeImage);
        }
        [Fact]
        public async Task T013_Handler_Cambia_En_Menu_La_Pagina()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeToMenuCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new ToMenuCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errores y con portada en true
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(page.OnMenu);
        }
        [Fact]
        public async Task T014_Handler_Cambia_De_Orden_La_Pagina_En_El_Menu()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeOrderAtMenuCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(page));
            _pageRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new OrderAtMenuCommandHandler(
                _logger.Object,
                _pageCacheMock.Object, _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeOrder, page.OrderAtMenu);
        }
        [Fact]
        public async Task T015_Handler_Agrega_Multimedio_Imagen_A_Una_Pagina()
        {
            // Preparo el articulo
            var page = fakePage();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: null,
                fileName1: fakeFileName,
                fileName2: null,
                multimediaType: MultimediaType.Imagen
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(
                _logger.Object,
                _pageMultimediaCacheMock.Object,
                _pageRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(page.PageMultimedia);
            Assert.Equal(
                string.Format("/uploads/pages/80/{0}", fakeFileName),
                page.PageMultimedia.First().Path1);
        }

        [Fact]
        public async Task T019_Handler_Quita_Multimedio_Imagen_De_Una_Pagina()
        {
            // Preparo el articulo
            var page = fakePage();
            page.AddMultimedia(
                createdBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: null,
                path1: string.Format("/uploads/pages/{0}",
                    fakeFileName),
                path2: null,
                multimediaType: MultimediaType.Imagen
                );
            // Preparo el commando
            var command = fakeRemoveMultimediaCommand();
            // Preparo el repositorio
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveMultimediaCommandHandler(
                _logger.Object,
                _pageMultimediaCacheMock.Object,
                _pageRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Empty(page.PageMultimedia);
        }

        [Fact]
        public async Task T020_Handler_Agrega_Documento_A_Una_Pagina()
        {
            // Preparo el articulo
            var page = fakePage();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: null,
                fileName1: fakeDocumentFileName,
                fileName2: fakeExtention,
                multimediaType: MultimediaType.Documento
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(
                _logger.Object,
                _pageMultimediaCacheMock.Object,
                _pageRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(page.PageMultimedia);
            Assert.Equal(
                string.Format("/uploads/pages/documents/{0}", fakeDocumentFileName),
                page.PageMultimedia.First().Path1);
            Assert.Equal(
                fakeExtention, page.PageMultimedia.First().Path2);
        }

        [Fact]
        public async Task T021_Handler_Agrega_Multimedio_Widget_A_Una_Pagina()
        {
            // Preparo la página
            var page = fakePage();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: fakeWidgetCode,
                fileName1: fakeFileName,
                fileName2: null,
                multimediaType: MultimediaType.Widget
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _pageRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Page>(page));
            _pageRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(
                _logger.Object,
                _pageMultimediaCacheMock.Object,
                _pageRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(page.PageMultimedia);
            Assert.Equal(
                fakeWidgetCode,
                page.PageMultimedia.First().Summary);
            Assert.Equal(string.Format("/uploads/pages/80/{0}", fakeFileName), page.PageMultimedia.First().Path1);
        }

    }
}
