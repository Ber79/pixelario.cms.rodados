﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Editions;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Application.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Pixelario.CMS.Application.Cache;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class EditionCommandHandlerTests
    {
        private Mock<IEditionRepository> _editionRepositoryMock;
        private Mock<IEditionCache> _editionCacheMock;
        private Mock<IConstraintRepository> _constraintsRepositoryMock;
        private Mock<IImageServices> _imageServicesMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDEdition = 1;
        private string fakeCreatedBy = "test";
        private string fakeSolicitedBy = "test";
        private string fakeTitle = "Fake título";
        private string fakeSummary = "Fake sumario";
        private string fakeKeywords = "keyword1; keyword2; keyword3";
        private int fakeOrden = 1;
        private string fakeFileName = "test.jpg";
        private string fakeAppDomainAppPath = "c:\\dev\\app\\";
        private List<int> fakePlansSelected =
            new List<int> { 1 };

        private Edition fakeEdition()
        {
            var edition = new Edition(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            return edition;
        }

        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords
                );
            command.SetCreatedBy(createdBy: fakeCreatedBy);
            return command;
        }
        private EnableCommand fakeEnableCommand()
        {
            var command = new EnableCommand(
                idEdition: fakeIDEdition);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ToHomeCommand fakeToHomeCommand()
        {
            var command = new ToHomeCommand(
                idEdition: fakeIDEdition);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ToMenuCommand fakeToMenuCommand()
        {
            var command = new ToMenuCommand(
                idEdition: fakeIDEdition);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ChangeHomeOrderCommand fakeChangeHomeOrderCommand()
        {
            var command = new ChangeHomeOrderCommand(
                idEdition: fakeIDEdition,
                order: fakeOrden);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private ChangeMenuOrderCommand fakeChangeMenuOrderCommand()
        {
            var command = new ChangeMenuOrderCommand(
                idEdition: fakeIDEdition,
                order: fakeOrden);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }


        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                idEdition: fakeIDEdition,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords
                );
            command.SetSolicitedBy(solicitedBy: fakeSolicitedBy);
            return command;
        }

        private DeleteCommand fakeDeleteCommand()
        {
            var command = new DeleteCommand(
                idEdition: fakeIDEdition);
            command.SetSolicitedBy(
             solicitedBy: fakeSolicitedBy);
            return command;
        }

        private ChangeImageCommand fakeChangeImageCommand(ImageType imageType)
        {
            var command = new ChangeImageCommand(
                iDEdition: fakeIDEdition,
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private ChangeEditionTypeCommand fakeChangeEditionTypeCommand(EditionType editionType)
        {
            var command = new ChangeEditionTypeCommand(
                iDEdition: fakeIDEdition,
                editionType: editionType.Id);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private RemoveImageCommand fakeRemoveImageCommand(ImageType imageType)
        {
            var command = new RemoveImageCommand(
                iDEdition: fakeIDEdition,
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ChangeConstraintsCommand fakeAddConstraintsCommand()
        {
            var command = new ChangeConstraintsCommand(
                iDEdition: fakeIDEdition,
                plansSelected: fakePlansSelected);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }

        public EditionCommandHandlerTests()
        {
            _editionRepositoryMock = new Mock<IEditionRepository>();
            _editionCacheMock = new Mock<IEditionCache>();
            _constraintsRepositoryMock = new Mock<IConstraintRepository>();
            _imageServicesMock = new Mock<IImageServices>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };

            return response;
        }
        [Fact]
        public async Task T001_Handler_De_Creacion_Retorna_False_Cuando_No_Agrega_Una_Edition()
        {
            // Preparo el commando
            var command = fakeAddCommand();
            command.SetCreatedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, 
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T002_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Edition()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new EnableCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T003_Handler_De_Habilitacion_Retorna_False_Cuando_No_Exista_Una_Edition()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new EnableCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_Handler_De_En_Portada_Retorna_False_Cuando_No_Edita_Una_Edition()
        {
            // Preparo el commando
            var command = fakeToHomeCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ToHomeCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T005_Handler_De_En_Portada_Retorna_False_Cuando_No_Exista_Una_Edition()
        {
            // Preparo el commando
            var command = fakeToHomeCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new ToHomeCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T006_Handler_De_Cambio_De_Orden_Retorna_False_Cuando_No_Edita_Una_Edition()
        {
            // Preparo el commando
            var command = fakeChangeHomeOrderCommand();
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeHomeOrderCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, 
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T007_Handler_De_De_Cambio_De_Orden_Retorna_False_Cuando_No_Exista_Una_Edition()
        {
            // Preparo el commando
            var command = fakeChangeHomeOrderCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new ChangeHomeOrderCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, 
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T008_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Edition()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new UpdateCommandHandler( _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T009_Handler_De_Editar_Retorna_False_Cuando_No_Exista_Una_Edition()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new UpdateCommandHandler( _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T010_Handler_De_Borrar_Retorna_False_Cuando_No_Borra_Una_Edition()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, 
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T011_Handler_De_Borrar_Retorna_False_Cuando_No_Exista_Una_Edition()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new DeleteCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T012_Handler_Cambia_Imagen_De_Portada_De_Una_Edition()
        {
            // Preparo el articulo
            var edition = fakeEdition();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Home
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _editionRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(edition));
            _editionRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(
                _logger.Object, 
                _editionCacheMock.Object, _editionRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/editions/80/{0}", fakeFileName),
                edition.HomeImage);
        }
        [Fact]
        public async Task T013_Handler_Cambia_Imagen_De_Portada_De_Una_Edition_Da_Error_En_Tipo_No_Disponible()
        {
            // Preparo el articulo
            var edicion = fakeEdition();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Vertical // Tipo no disponible
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _editionRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(edicion));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(
                _logger.Object,
                _editionCacheMock.Object,
                _editionRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T014_Handler_Quita_Imagen_Vertical_De_Un_Articulo()
        {
            // Preparo el articulo
            var edition = fakeEdition();
            edition.ChangeImage(
                imageType: ImageType.Home,
                uri: string.Format("/uploads/editions/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Home
                );
            // Preparo el repositorio
            _editionRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(edition));
            _editionRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(
                _logger.Object,
                _editionCacheMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(edition.HomeImage);
        }
        [Fact]
        public async Task T015_Handler_Cambia_Tipo_De_Edicion_De_Una_Edicion()
        {
            // Preparo la edición
            var edition = fakeEdition();
            // Preparo el commando
            var command = fakeChangeEditionTypeCommand(
                editionType: EditionType.News
                );
            // Preparo el repositorio
            _editionRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(edition));
            _editionRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeEditionTypeCommandHandler(
                _logger.Object,
                _editionCacheMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                EditionType.News.Id,
                edition.EditionType);
        }
        [Fact]
        public async Task T016_AddConstraintsHandler_Return_False_When_Not_Update()
        {
            // Preparo el commando
            var command = fakeAddConstraintsCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeConstraintsCommandHandler(
                _logger.Object,
                _mediator.Object, _constraintsRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T017_OnMenuHandler_Return_False_Not_Edit()
        {
            // Preparo el commando
            var command = fakeToMenuCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ToMenuCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T018_OnMenuHandler_Return_False_When_Not_Find_Edition()
        {
            // Preparo el commando
            var command = fakeToMenuCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new ToMenuCommandHandler(
                _logger.Object,
                _editionCacheMock.Object, _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T019_ChangeMenuOrderCommandHandler_Return_False_When_Cant_Update()
        {
            // Preparo el commando
            var command = fakeChangeMenuOrderCommand();
            // Preparo el repositorio
            _editionRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeMenuOrderCommandHandler(
                _logger.Object,
                _editionCacheMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T020_ChangeMenuOrderCommandHandler_Return_False_When_Edition_Not_Exist()
        {
            // Preparo el commando
            var command = fakeChangeMenuOrderCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _editionRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Edition>(null));
            // preparo el handler
            var handler = new ChangeMenuOrderCommandHandler(
                _logger.Object,
                _editionCacheMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }



    }
}
