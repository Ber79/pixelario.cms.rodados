﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class TopicCommandHandlerTests
    {
        private Mock<ITopicRepository> _topicRepositoryMock;
        private Mock<ITopicCache> _topicCacheMock;
        private Mock<ISubTopicCache> _subTopicCacheMock;
        private Mock<IEditionRepository> _editionRepositoryMock;
        private Mock<IImageServices> _imageServicesMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDTopic = 1;
        private int fakeIDSubTopic = 1;

        private string fakeCreatedBy = "unitest";
        private string fakeSolicitedBy = "test";
        private string fakeTitle = "Title falso";
        private string fakeSummary = "Sumario falso";
        private string fakeKeywords = "keyword1; keyword2; keyword3";

        private string fakeTitleEditado = "Title falso editado";
        private string fakeSummaryEditado = "Sumario falso editado";
        private string fakeKeywordsEditado = "keyword4; keyword5; keyword6";
        private int fakeOrder = 1;
        private string fakeFileName = "test.jpg";
        private string fakeAppDomainAppPath = "c:\\dev\\app\\";
        private List<int> fakedicionesSeleccionadas =
            new List<int> { 1, 2 };
        private Edition fakeEdition()
        {
            return new Edition(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeSummary);
        }
        private Topic fakeTopic()
        {
            return new Topic(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeSummary);
        }
        private SubTopic fakeSubTopic()
        {
            var subtopic = new SubTopic(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeSummary);
            return subtopic;
        }
        private AddTopicCommand fakeAddTopicCommand()
        {
            var command = new AddTopicCommand(
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords,
                editions: fakedicionesSeleccionadas);
            command.SetCreatedBy(
                createdBy: fakeCreatedBy);
            return command;
        }
        private EnableTopicCommand fakeEnableTopicCommand()
        {
            var command = new EnableTopicCommand(
                iDTopic: fakeIDTopic);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private EnableSubTopicCommand fakeEnableSubTopicCommand()
        {
            var command = new EnableSubTopicCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(Int32));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }

        private TopicToHomeCommand fakeTopicToHomeCommand()
        {
            var command = new TopicToHomeCommand(
                iDTopic: fakeIDTopic);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private SubTopicToHomeCommand fakeSubTopicToHomeCommand()
        {
            var command = new SubTopicToHomeCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(Int32));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ChangeTopicHomeOrderCommand fakeChangeTopicHomeOrderCommand()
        {
            return new ChangeTopicHomeOrderCommand(
                iDTopic: fakeIDTopic,
                order: fakeOrder);
        }
        private ChangeSubTopicOrderCommand fakeSubTopicOrderToHomeCommand()
        {
            var command = new ChangeSubTopicOrderCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(Int32),
                order: fakeOrder);
            command.SetSolicitedBy(
               solicitedBy: fakeSolicitedBy);
            return command;
        }
        private UpdateTopicCommand fakeUpdateTopicCommand()
        {
            var command = new UpdateTopicCommand(
                iDTopic: fakeIDTopic,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords,
                editions: fakedicionesSeleccionadas);
            command.SetSolicitedBy(
               solicitedBy: fakeSolicitedBy);
            return command;
        }
        private UpdateSubTopicCommand fakeUpdateSubTopicCommand()
        {
            var command = new UpdateSubTopicCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(Int32),
                title: fakeTitleEditado,
                summary: fakeSummaryEditado,
                keywords: fakeKeywordsEditado);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private DeleteTopicCommand fakeDeleteTopicCommand()
        {
            var command = new DeleteTopicCommand(
                iDTopic: fakeIDTopic);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private DeleteSubTopicCommand fakeDeleteSubTopicCommand()
        {
            var command = new DeleteSubTopicCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(Int32));
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private AddSubTopicCommand fakeAddSubTopicCommand()
        {
            var command = new AddSubTopicCommand(
                iDTopic: fakeIDTopic,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private ChangeTopicImageCommand fakeChangeTopicImageCommand(ImageType imageType)
        {
            var command = new ChangeTopicImageCommand(
                iDTopic: fakeIDTopic,
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ChangeSubTopicImageCommand fakeChangeSubTopicImageCommand(ImageType imageType)
        {
            var command = new ChangeSubTopicImageCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(int),
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private RemoveTopicImageCommand fakeRemoveTopicImageCommand(ImageType imageType)
        {
            var command = new RemoveTopicImageCommand(
                iDTopic: fakeIDTopic,
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private RemoveSubTopicImageCommand fakeRemoveSubTopicImageCommand(ImageType imageType)
        {
            var command = new RemoveSubTopicImageCommand(
                iDTopic: fakeIDTopic,
                iDSubTopic: default(int),
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ToMenuCommand fakeToMenuCommand()
        {
            var command = new ToMenuCommand(
                iDTopic: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private SubTopicToMenuCommand fakeSubTopicToMenuCommand()
        {
            var command = new SubTopicToMenuCommand(
                iDSubTopic: default(int),
                iDTopic: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private OrderAtMenuCommand fakeOrderAtMenuCommand()
        {
            var command = new OrderAtMenuCommand(
                iDTopic: default(int),
                order: fakeOrder);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }
        private SubTopicOrderAtMenuCommand fakeSubTopicOrderAtMenuCommand()
        {
            var command = new SubTopicOrderAtMenuCommand(
                iDSubTopic: default(int),
                iDTopic: default(int),
                order: fakeOrder);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;

        }
        public TopicCommandHandlerTests()
        {
            _topicRepositoryMock = new Mock<ITopicRepository>();
            _topicCacheMock = new Mock<ITopicCache>();
            _subTopicCacheMock = new Mock<ISubTopicCache>();
            _editionRepositoryMock = new Mock<IEditionRepository>();
            _imageServicesMock = new Mock<IImageServices>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }
        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };

            return response;
        }
        [Fact]
        public async Task T001_Handler_De_Creacion_Retorna_False_Cuando_No_Agrega_Una_Topic()
        {
            // Preparo el commando
            var command = fakeAddTopicCommand();
            // Preparo los repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            var edicion = fakeEdition();
            _editionRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(edicion));
            // preparo el handler
            var handler = new AddTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T002_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Topic()
        {
            // Preparo el commando
            var command = fakeEnableTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new EnableTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T003_Handler_De_Habilitacion_Retorna_False_Cuando_No_Exista_Una_Topic()
        {
            // Preparo el commando
            var command = fakeEnableTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new EnableTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_Handler_De_En_Portada_Retorna_False_Cuando_No_Edita_Una_Topic()
        {
            // Preparo el commando
            var command = fakeTopicToHomeCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new TopicToHomeCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T005_Handler_De_En_Portada_Retorna_False_Cuando_No_Exista_Una_Topic()
        {
            // Preparo el commando
            var command = fakeTopicToHomeCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new TopicToHomeCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T006_Handler_De_Cambio_De_Orden_Retorna_False_Cuando_No_Edita_Una_Topic()
        {
            // Preparo el commando
            var command = fakeChangeTopicHomeOrderCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeTopicHomeOrderCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T007_Handler_De_De_Cambio_De_Orden_Retorna_False_Cuando_No_Exista_Una_Topic()
        {
            // Preparo el commando
            var command = fakeChangeTopicHomeOrderCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new ChangeTopicHomeOrderCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T008_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Topic()
        {
            // Preparo el commando
            var command = fakeUpdateTopicCommand();            
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            _editionRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(fakeEdition()));

            // preparo el handler
            var handler = new UpdateTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T009_Handler_De_Editar_Retorna_False_Cuando_No_Exista_Una_Topic()
        {
            // Preparo el commando
            var command = fakeUpdateTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            _editionRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(fakeEdition()));

            // preparo el handler
            var handler = new UpdateTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object,
                _editionRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T010_Handler_De_Borrar_Retorna_False_Cuando_No_Borra_Una_Topic()
        {
            // Preparo el commando
            var command = fakeDeleteTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T011_Handler_De_Borrar_Retorna_False_Cuando_No_Exista_Una_Topic()
        {
            // Preparo el commando
            var command = fakeDeleteTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new DeleteTopicCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T012_Handler_De_Creacion_De_SubTopic_Retorna_False_Cuando_No_Agrega_Una_Topic()
        {
            // Preparo el commando
            var command = fakeAddSubTopicCommand();
            // Preparo los repositorio
            var topic = fakeTopic();
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }


        [Fact]
        public async Task T013_Handler_De_Habilitar_SubTopic_Retorna_False_Cuando_No_Habilita()
        {
            // Preparo el commando
            var command = fakeEnableSubTopicCommand();
            // Preparo el repositorio
            var topic = fakeTopic();
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new EnableSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T014_Handler_De_Habilitar_SubTopic_Retorna_False_Cuando_No_Exista_Un_Topic()
        {
            // Preparo el commando
            var command = fakeEnableSubTopicCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new EnableSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T015_Handler_De_Habilitar_SubTopic_Retorna_False_Cuando_No_Exista_Un_SubTopic()
        {
            // Preparo el commando
            var command = fakeEnableSubTopicCommand();
            // Preparo el repositorio
            // El tema no contiene al subtopic
            var topic = fakeTopic();
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(null));
            // preparo el handler
            var handler = new EnableSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T016_Handler_De_Habilitar_SubTopic_Correctamente()
        {
            // Preparo el commando
            var command = fakeEnableSubTopicCommand();
            // Preparo el repositorio
            // El tema no contiene al subtopic
            var topic = fakeTopic();
            topic.AddSubTopic (
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new EnableSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            var subtopic = topic.SubTopics.Where(
                s => s.ID == default(Int32)).FirstOrDefault();
            Assert.True(subtopic.Enabled);
        }
        [Fact]
        public async Task T017_Handler_De_Portada_SubTopic_Correctamente()
        {
            // Preparo el commando
            var command = fakeSubTopicToHomeCommand();
            // Preparo el repositorio
            // El tema no contiene al subtopic
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new SubTopicToHomeCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true
            Assert.True(result.success);
            var subtopic = topic.SubTopics.Where(
                s => s.ID == default(Int32)).FirstOrDefault();
            Assert.True(subtopic.AtHome);
        }

        [Fact]
        public async Task T018_Handler_De_OrdenEnPortada_SubTopic_Correctamente()
        {
            // El tema no contiene al subtopic
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            // Preparo el commando
            var command = fakeSubTopicOrderToHomeCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new ChangeSubTopicOrderCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true
            Assert.True(result.success);
            var subtopic = topic.SubTopics.Where(
                s => s.ID == default(Int32)).FirstOrDefault();
            Assert.Equal(fakeOrder, subtopic.Order);
        }
        [Fact]
        public async Task T019_Handler_De_Borrar_SubTopic_Correctamente()
        {
            // Preparo el commando
            var command = fakeDeleteSubTopicCommand();
            // Preparo el repositorio
            // El tema no contiene al subtopic
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            _topicRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new DeleteSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true
            Assert.True(result.success);
            Assert.Empty(topic.SubTopics);
        }
        [Fact]
        public async Task T020_Handler_De_Editar_SubTopic_Correctamente()
        {
            // Preparo el commando
            var command = fakeUpdateSubTopicCommand();
            // Preparo el tema y el subtopic
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);

            // Preparo el repositorio
            _topicRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new UpdateSubTopicCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true y los nuevos valores
            Assert.True(result.success);
            var subTopic = topic.SubTopics.Where(s => s.ID == default(Int32)).FirstOrDefault();
            Assert.Equal(fakeTitleEditado, subTopic.Title);
            Assert.Equal(fakeSummaryEditado, subTopic.Summary);
            Assert.Equal(fakeKeywordsEditado, subTopic.Keywords);
        }
        [Fact]
        public async Task T021_Handler_Cambia_Imagen_De_Portada_De_Un_Topic()
        {
            // Preparo el tema
            var topic = fakeTopic();
            // Preparo el commando
            var command = fakeChangeTopicImageCommand(
                imageType: ImageType.Home
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeTopicImageCommandHandler(
                _logger.Object,
                _topicCacheMock.Object,
                _topicRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // errores en null y el nuevo valor de la imagen de portada
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/topics/80/{0}", fakeFileName),
                topic.HomeImage);
        }
        [Fact]
        public async Task T022_Handler_Cambia_Imagen_De_Portada_De_Un_Topic_Da_Error_En_Tipo_No_Disponible()
        {
            // Preparo el articulo
            var topic = fakeTopic();
            // Preparo el commando
            var command = fakeChangeTopicImageCommand(
                imageType: ImageType.Vertical // Tipo no disponible
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            // preparo el handlerd
            var handler = new ChangeTopicImageCommandHandler(
                _logger.Object, _topicCacheMock.Object,
                _topicRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y
            // con errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T023_Handler_Quita_Imagen_Portada_De_Un_Topic()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.ChangeImage(
                imageType: ImageType.Home,
                uri: string.Format("/uploads/topics/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveTopicImageCommand(
                imageType: ImageType.Home
                );
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveTopicImageCommandHandler(
                _logger.Object, _topicCacheMock.Object,
                _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // sin errores y con null en imagen de portada
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(topic.HomeImage);
        }

        [Fact]
        public async Task T024_Handler_Cambia_Imagen_De_Portada_De_Un_SubTopic()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            // Preparo el commando
            var command = fakeChangeSubTopicImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeSubTopicImageCommandHandler(
                _logger.Object, _subTopicCacheMock.Object,
                _topicRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // errores en null y el nuevo valor de la imagen de portada
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/subtopics/80/{0}", fakeFileName),
                topic.SubTopics.Where(s => s.ID == default(int)).First().HomeImage);
        }

        [Fact]
        public async Task T025_Handler_Cambia_Imagen_De_Portada_De_Un_SubTopic_Da_Error_En_Tipo_No_Disponible()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords
                );
            // Preparo el commando
            var command = fakeChangeSubTopicImageCommand(
                imageType: ImageType.Vertical
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));

            // preparo el handlerd
            var handler = new ChangeSubTopicImageCommandHandler(
                _logger.Object, _subTopicCacheMock.Object,
                _topicRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // errores en null y el nuevo valor de la imagen de portada
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T026_Handler_Quita_Imagen_Portada_De_Un_SubTopic()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords
                );
            topic.ChangeSubTopicImage(
                iDSubTopic: default(int),
                imageType: ImageType.Home,
                uri: string.Format("/uploads/subtopics/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveSubTopicImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Topic>(topic));
            _topicRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveSubTopicImageCommandHandler(
                _logger.Object, _subTopicCacheMock.Object,
                _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true,
            // sin errores y con null en imagen de portada
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(topic.HomeImage);
        }

        [Fact]
        public async Task T027_Handler_Cambia_En_Menu_El_Tema()
        {
            // Preparo el tema
            var topic = fakeTopic();
            // Preparo el commando
            var command = fakeToMenuCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new ToMenuCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errores y con portada en true
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(topic.OnMenu);
        }
        [Fact]
        public async Task T028_Handler_Cambia_De_Orden_El_Tema_En_El_Menu()
        {
            // Preparo el tema
            var topic = fakeTopic();
            // Preparo el commando
            var command = fakeOrderAtMenuCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new OrderAtMenuCommandHandler(
                _logger.Object,
                _topicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeOrder, topic.OrderAtMenu);
        }
        [Fact]
        public async Task T029_Handler_Cambia_En_Menu_El_SubTema()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
            // Preparo el commando
            var command = fakeSubTopicToMenuCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new SubTopicToMenuCommandHandler(
                _logger.Object,
                _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en true, sin errores y con portada en true
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(topic.SubTopics.First().OnMenu);
        }
        [Fact]
        public async Task T030_Handler_Cambia_De_Orden_El_SubTema_En_El_Menu()
        {
            // Preparo el tema
            var topic = fakeTopic();
            topic.AddSubTopic(
                solicitedBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);

            // Preparo el commando
            var command = fakeSubTopicOrderAtMenuCommand();
            // Preparo el repositorio
            _topicRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(topic));
            _topicRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new SubTopicOrderAtMenuCommandHandler(
                _logger.Object, _subTopicCacheMock.Object, _topicRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeOrder, topic.SubTopics.First().OrderAtMenu);
        }

    }
}
