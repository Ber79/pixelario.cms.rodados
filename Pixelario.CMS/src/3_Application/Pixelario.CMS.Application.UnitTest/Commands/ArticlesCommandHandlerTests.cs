﻿using MediatR;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using Pixelario.CMS.Application.CommandHandlers.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Serilog;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Infrastructure;
using System.Data.Entity;
using Pixelario.CMS.Infrastructure.Repositories;

namespace Pixelario.CMS.Application.UnitTest.Commands
{
    public class ArticleCommandHandlerTests
    {
        private Mock<IArticleRepository> _articleRepositoryMock;
        private Mock<IArticleScriptRepository> _articleScriptRepositoryMock;

        private Mock<IArticlesQueries> _articlesQueriesMock;
        private Mock<IArticleCache> _articleCacheMock;
        private Mock<IArticleScriptCache> _articleScriptCacheMock;
        private Mock<IArticleScriptItemCache> _articleScriptItemCacheMock;
        private Mock<IArticleMultimediaCache> _articleMultimediaCacheMock;
        private Mock<ITopicRepository> _topicRepositoryMock;
        private Mock<IConstraintRepository> _constraintsRepositoryMock;
        private Mock<IImageServices> _imageServicesMock;
        private Mock<IMediator> _mediator;
        private Mock<ILogger> _logger;
        private int fakeIDArticle = 1;
        private int fakeIDEdition = 1;
        private string fakeCreatedBy = "admin";
        private string fakeSolicitedBy = "admin";
        private DateTime? fakePublicationDate = DateTime.Now.AddDays(1);
        private string fakeTitle = "Titulo del artículo";
        private string fakeSummary = "Summary del artículo";
        private string fakeKeywords = "keyword1; keyword2; keyword3";
        private string fakeBody = "Cuerpo del artículo";
        private string fakeSource = "Fake source";
        private string fakeFileName = "test.jpg";
        private string fakeExternalURL = "http://www.myfakeurl.com";
        private string fakeDocumentFileName = "test.docx";
        private string fakeAppDomainAppPath = "c:\\dev\\app\\";
        private string fakeWidgetCode = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/oM1c1MH0q7E\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";
        private int fakeOrder = 1;
        private string fakeURI = "/fake/uri.jpg";
        private List<int> fakeTemasSeleccionados =
            new List<int> { 1 };
        private List<int> fakeSubTemasSeleccionados =
            new List<int> { 1 };
        private List<int> fakeSelectedScripts =
            new List<int> { 1 };
        private List<int> fakePlansSelected =
            new List<int> { 1 };
        private string fakeCode = "<iframe></iframe>";

        private SaveEntitiesResponse fakeSaveEntitiesResponse(bool success)
        {
            var response = new SaveEntitiesResponse();
            response.Success = success;
            if (!success)
                response.Errors = new List<string>() { "Error" };
            return response;          
        }


        private Article fakeArticle()
        {
            return new Article(
                createdBy: fakeCreatedBy,
                iDEdition: fakeIDEdition,
                publicationDate: fakePublicationDate,
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                source: fakeSource,
                keywords: fakeKeywords
                );
        }
        private ArticleScript fakeArticleScript()
        {
            return new ArticleScript(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary);
        }
        private Topic fakeTopic()
        {
            return new Topic(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
        }
        private SubTopic fakeSubTopic()
        {
            return new SubTopic(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeywords);
        }

        private Edition fakeNewEdition(EditionType editionType)
        {
            var editionMoq = new Mock<Edition>(fakeCreatedBy, fakeTitle,
                fakeSummary, fakeKeywords);
            editionMoq.SetupGet(p => p.ID).Returns(2);
            var edition = editionMoq.Object;
            edition.ChangeEditionType(editionType);
            return edition;
        }
        private AddCommand fakeAddCommand()
        {
            var command = new AddCommand(
                iDEdition: fakeIDEdition,
                publicationDate: null,
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                source: fakeSource,
                keywords: fakeKeywords,
                topics: fakeTemasSeleccionados,
                subTopics: fakeSubTemasSeleccionados,
                scripts: fakeSelectedScripts
                );
            command.SetCreatedBy(fakeCreatedBy);
            return command;
        }
        private EnableCommand fakeEnableCommand()
        {
            var command = new EnableCommand(
                iDArticle: fakeIDArticle);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }

        private EnableScriptCommand fakeEnableScriptCommand(ArticleScript articleScript)
        {
            var command = new EnableScriptCommand(
                iDArticleScript: articleScript.ID);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private EnableScriptItemCommand fakeEnableScriptItemCommand()
        {
            var command = new EnableScriptItemCommand(
                iDArticleScript: default(int),
                iDArticleScriptItem: default(int));
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }

        private ToHomeCommand fakeToHomeCommand()
        {
            var command = new ToHomeCommand(
                iDArticle: fakeIDArticle);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private ChangeHomeOrderCommand fakeChangeHomeOrderCommand()
        {
            var command = new ChangeHomeOrderCommand(
                iDArticle: fakeIDArticle,
                order: fakeOrder);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private ChangePlaceAtHomeCommand fakeChangePlaceAtHomeCommand(ArticlePlaceAtHome placeAtHome)
        {
            var command = new ChangePlaceAtHomeCommand(
                iDArticle: default(int),
                placeAtHome: placeAtHome.Id);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private UpdateCommand fakeUpdateCommand()
        {
            var command = new UpdateCommand(
                iDArticle: fakeIDArticle,
                publicationDate: fakePublicationDate.Value.AddDays(1),
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                source: fakeSource,
                keywords: fakeKeywords,
                topics: fakeTemasSeleccionados,
                subTopics: fakeSubTemasSeleccionados,
                scripts: fakeSelectedScripts
                );
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }

        private DeleteCommand fakeDeleteCommand()
        {
            var command = new DeleteCommand(
                iDArticle: fakeIDArticle);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }

        private ChangeImageCommand fakeChangeImageCommand(ImageType imageType)
        {
            var command = new ChangeImageCommand(
                iDArticle: fakeIDArticle,
                imageType: imageType,
                appDomainAppPath: fakeAppDomainAppPath,
                fileName: fakeFileName);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private RemoveImageCommand fakeRemoveImageCommand(ImageType imageType)
        {
            var command = new RemoveImageCommand(
                iDArticle: fakeIDArticle,
                imageType: imageType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private AddMultimediaCommand fakeAddMultimediaCommand(
            string sumario, string fileName1, string fileName2,
            MultimediaType multimediaType)
        {
            var command = new AddMultimediaCommand(
                iDArticle: fakeIDArticle,
                title: fakeTitle,
                summary: sumario,
                appDomainAppPath: fakeAppDomainAppPath,
                path1: fileName1,
                path2: fileName2,
                multimediaType: multimediaType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private RemoveMultimediaCommand fakeRemoveMultimediaCommand()
        {
            var command = new RemoveMultimediaCommand(
                iDArticle: fakeIDArticle,
                iDMultimedia: default(int));
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private ChangeConstraintsCommand fakeChangeConstraintsCommand()
        {
            var command = new ChangeConstraintsCommand(
                iDArticle: fakeIDArticle,
                plansSelected: fakePlansSelected);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }

        private TransferToEditionCommand fakeTransferToEditionCommand(int iDEdition)
        {
            var command = new TransferToEditionCommand(
                iDArticle: default(int),
                iDEdition: iDEdition);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }


        private AddScriptCommand fakeAddScriptCommand()
        {
            var command = new AddScriptCommand(
                title: fakeTitle,
                summary: fakeSummary);
            command.SetCreatedBy(
                createdBy: fakeCreatedBy);
            return command;
        }
        private AddScriptItemCommand fakeAddScriptItemCommand(
            int iDMultimediaType)
        {
            var command = new AddScriptItemCommand(
                iDArticleScript: default(int),
                title: fakeTitle,
                code: fakeCode,
                iDMultimediaType: iDMultimediaType);
            command.SetSolicitedBy(
                solicitedBy: fakeSolicitedBy);
            return command;
        }
        private UpdateScriptCommand fakeUpdateScriptCommand(int iDArticleScript)
        {
            var updatedText = " updated";
            var command = new UpdateScriptCommand(
                iDScript: iDArticleScript,
                title: fakeTitle + updatedText,
                summary: fakeSummary + updatedText                
                );
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private UpdateScriptItemCommand fakeUpdateScriptItemCommand(int iDMultimediaType)
        {
            var updatedText = " updated";
            var command = new UpdateScriptItemCommand(
                iDArticleScript: default(int),
                iDArticleScriptItem: default(int),
                title: fakeTitle + updatedText,
                code: fakeCode + updatedText,
                iDMultimediaType: iDMultimediaType);
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;
        }
        private DeleteScriptCommand fakeDeleteScriptCommand()
        {
            var command = new DeleteScriptCommand(
                iDScript: default(int));
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }
        private DeleteScriptItemCommand fakeDeleteScriptItemCommand()
        {
            var command = new DeleteScriptItemCommand(
                iDArticleScript: default(int),
                iDArticleScriptItem: default(int));
            command.SetSolicitedBy(fakeSolicitedBy);
            return command;

        }

        public ArticleCommandHandlerTests()
        {
            _articleRepositoryMock = new Mock<IArticleRepository>();
            _articleScriptRepositoryMock = new Mock<IArticleScriptRepository>();

            _articlesQueriesMock = new Mock<IArticlesQueries>();
            _topicRepositoryMock = new Mock<ITopicRepository>();
            _articleCacheMock = new Mock<IArticleCache>();
            _articleScriptCacheMock = new Mock<IArticleScriptCache>();
            _articleScriptItemCacheMock = new Mock<IArticleScriptItemCache>();
            _articleMultimediaCacheMock = new Mock<IArticleMultimediaCache>();
            _constraintsRepositoryMock = new Mock<IConstraintRepository>();
            _imageServicesMock = new Mock<IImageServices>();
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger>();
        }

        [Fact]
        public async Task T001_Handler_De_Creacion_Retorna_False_Cuando_No_Agrega_Una_Article()
        {
            // Preparo el commando
            var command = fakeAddCommand();           
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object,
                topicRepository: _topicRepositoryMock.Object,
                articleScriptRepository: _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T002_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Article()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new EnableCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T003_Handler_De_Habilitacion_Retorna_False_Cuando_No_Exista_Una_Article()
        {
            // Preparo el commando
            var command = fakeEnableCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(null));
            // preparo el handler
            var handler = new EnableCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T004_Handler_De_En_Portada_Retorna_False_Cuando_No_Edita_Una_Article()
        {
            // Preparo el commando
            var command = fakeToHomeCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // Preparo la queries
            _articlesQueriesMock.Setup(service =>
                service.CountAsync(It.IsAny<int?>(),
                            It.IsAny<int?>(),
                            It.IsAny<int?>(),
                            It.IsAny<bool?>(),
                            It.IsAny<bool?>(),
                            It.IsAny<ArticlePlaceAtHome>(),
                            It.IsAny<string>())).Returns(Task.FromResult(0));
                
            // preparo el handler
            var handler = new ToHomeCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object,
                _articlesQueriesMock.Object
                );
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T005_Handler_De_En_Portada_Retorna_False_Cuando_No_Exista_Una_Article()
        {
            // Preparo el commando
            var command = fakeToHomeCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(null));
            // preparo el handler
            var handler = new ToHomeCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object,
                _articlesQueriesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T006_Handler_De_Cambio_De_Orden_Retorna_False_Cuando_No_Edita_Una_Article()
        {
            // Preparo el commando
            var command = fakeChangeHomeOrderCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeHomeOrderCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T007_Handler_De_De_Cambio_De_Orden_Retorna_False_Cuando_No_Exista_Una_Article()
        {
            // Preparo el commando
            var command = fakeChangeHomeOrderCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(null));
            // preparo el handler
            var handler = new ChangeHomeOrderCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T008_Handler_De_Habilitacion_Retorna_False_Cuando_No_Edita_Una_Article()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object,
                _topicRepositoryMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T009_Handler_De_Editar_Retorna_False_Cuando_No_Exista_Una_Article()
        {
            // Preparo el commando
            var command = fakeUpdateCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(null));
            // preparo el handler
            var handler = new UpdateCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object,
                _topicRepositoryMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }

        [Fact]
        public async Task T010_Handler_De_Borrar_Retorna_False_Cuando_No_Borra_Una_Article()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T011_Handler_De_Borrar_Retorna_False_Cuando_No_Exista_Una_Article()
        {
            // Preparo el commando
            var command = fakeDeleteCommand();
            // Preparo el repositorio
            _articleRepositoryMock.Setup(x =>
                x.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(null));
            // preparo el handler
            var handler = new DeleteCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T012_Handler_Cambia_Imagen_De_Portada_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(_logger.Object,
                _articleCacheMock.Object, 
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/articles/80/{0}", fakeFileName),
                article.HomeImage);
        }
        [Fact]
        public async Task T013_Handler_Quita_Imagen_De_Portada_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            article.ChangeImage(                
                imageType: ImageType.Home,
                uri: string.Format("/uploads/articles/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Home
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(_logger.Object,
                _articleCacheMock.Object,
                _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(article.HomeImage);
        }
        [Fact]
        public async Task T014_Handler_Cambia_Imagen_Horinzontal_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Horizontal
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(_logger.Object,
                _articleCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/articles/80/{0}", fakeFileName),
                article.HorizontalImage);
        }
        [Fact]
        public async Task T015_Handler_Quita_Imagen_Horizontal_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            article.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: string.Format("/uploads/articles/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Horizontal
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(_logger.Object,
                _articleCacheMock.Object,
                _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(article.HorizontalImage);
        }

        [Fact]
        public async Task T016_Handler_Cambia_Imagen_Vertical_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeChangeImageCommand(
                imageType: ImageType.Vertical
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new ChangeImageCommandHandler(_logger.Object,
                _articleCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(
                string.Format("/uploads/articles/80/{0}", fakeFileName),
                article.VerticalImage);
        }

        [Fact]
        public async Task T017_Handler_Quita_Imagen_Vertical_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            article.ChangeImage(
                imageType: ImageType.Vertical,
                uri: string.Format("/uploads/articles/{0}",
                    fakeFileName)
                );
            // Preparo el commando
            var command = fakeRemoveImageCommand(
                imageType: ImageType.Vertical
                );
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveImageCommandHandler(_logger.Object,
                _articleCacheMock.Object,
                _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Null(article.VerticalImage);
        }

        [Fact]
        public async Task T018_Handler_Agrega_Multimedio_Imagen_A_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(    
                sumario: null,
                fileName1: fakeFileName,
                fileName2: null,
                multimediaType: MultimediaType.Imagen
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(_logger.Object,
                _articleMultimediaCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(article.ArticleMultimedia);
            Assert.Equal(
                string.Format("/uploads/articles/80/{0}", fakeFileName),
                article.ArticleMultimedia.First().Path1);
        }

        [Fact]
        public async Task T019_Handler_Quita_Multimedio_Imagen_De_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            article.AddMultimedia(
                createdBy: fakeSolicitedBy,
                title: fakeTitle,
                summary: null,                        
                path1: string.Format("/uploads/articles/{0}",
                    fakeFileName),
                path2: null,
                multimediaType: MultimediaType.Imagen
                );
            // Preparo el commando
            var command = fakeRemoveMultimediaCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new RemoveMultimediaCommandHandler(_logger.Object,
                _articleMultimediaCacheMock.Object,
                _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Empty(article.ArticleMultimedia);
        }
        [Fact]
        public async Task T020_Handler_Agrega_Multimedio_Documento_A_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: null,
                fileName1: fakeDocumentFileName,
                fileName2: null,
                multimediaType: MultimediaType.Documento
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(_logger.Object,
                _articleMultimediaCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(article.ArticleMultimedia);
            Assert.Equal(
                string.Format("/uploads/articles/documents/{0}", fakeDocumentFileName),
                article.ArticleMultimedia.First().Path1);
        }

        [Fact]
        public async Task T021_Handler_Agrega_Multimedio_Widget_A_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: fakeWidgetCode,
                fileName1: null,
                fileName2: null,
                multimediaType: MultimediaType.Widget
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(_logger.Object,
                _articleMultimediaCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(article.ArticleMultimedia);
            Assert.Equal(
                fakeWidgetCode,
                article.ArticleMultimedia.First().Summary);
        }

        [Fact]
        public async Task T022_Handler_Cambia_La_Posicion_En_Home_de_Un_Article()
        {
            // preparo el articulo
            var article = fakeArticle();
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeURI);
            article.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeURI);
            // Preparo el commando
            var command = fakeChangePlaceAtHomeCommand(
                placeAtHome: ArticlePlaceAtHome.Slider);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(article));
            _articleRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new ChangePlaceAtHomeCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal((int)PlaceAtHome.Slider, article.IDPlaceAtHome);
        }
        [Fact]
        public async Task T023_Handler_Agrega_Multimedio_Link_A_Un_Article()
        {
            // Preparo el articulo
            var article = fakeArticle();
            // Preparo el commando
            var command = fakeAddMultimediaCommand(
                sumario: null,
                fileName1: fakeFileName,
                fileName2: fakeExternalURL,
                multimediaType: MultimediaType.Link
                );
            // Preparo el repositorio
            _imageServicesMock.Setup(s =>
                s.Salvar(It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<int>()))
                .Returns(true);
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<Article>(article));
            _articleRepositoryMock.Setup(r =>
               r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
               .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handlerd
            var handler = new AddMultimediaCommandHandler(_logger.Object,
                _articleMultimediaCacheMock.Object,
                _articleRepositoryMock.Object,
                _imageServicesMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(article.ArticleMultimedia);
            Assert.Equal(
                string.Format("/uploads/articles/80/{0}", fakeFileName),
                article.ArticleMultimedia.First().Path1);
            Assert.Equal(fakeExternalURL,
                article.ArticleMultimedia.First().Path2);
        }
        [Fact]
        public async Task T024_ChangeConstraintsHandler_Return_False_When_Not_Update()
        {
            // Preparo el commando
            var command = fakeChangeConstraintsCommand();
            command.SetSolicitedBy(fakeCreatedBy);
            // Preparo el repositorio
            _constraintsRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new ChangeConstraintsCommandHandler(
                _logger.Object,
                _articleCacheMock.Object, _constraintsRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T025_TranferToEditionCommandHandler_Change_Article_Edition()
        {
            // preparo el articulo
            var article = fakeArticle();
            var iDNewEdition =  EditionType.Especial_Content.Id;
            // Preparo el commando
            var command = fakeTransferToEditionCommand(
                iDEdition: iDNewEdition);
            // Preparo el repositorio
            _articleRepositoryMock.Setup(r =>
                r.GetAsync(It.IsAny<int>()))
                    .Returns(Task.FromResult(article));
            _articleRepositoryMock.Setup(r =>
                r.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult<SaveEntitiesResponse>(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new TransferToEditionCommandHandler(_logger.Object,
                _articleCacheMock.Object, _articleRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(iDNewEdition, article.IDEdition);
        }


        [Fact]
        public async Task T026_Handle_Add_Article_Script_Return_False_On_Error()
        {
            // Preparo el commando
            var command = fakeAddScriptCommand();
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new AddScriptCommandHandler(_logger.Object,
                _articleScriptCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T027_Handler_Enable_Article_Script()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            var command = fakeEnableScriptCommand(
                articleScript: articleScript);
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new EnableScriptCommandHandler(_logger.Object,
                _articleScriptCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(articleScript.Enabled);

        }
        [Fact]
        public async Task T028_Handler_Update_Article_Script()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            var command = fakeUpdateScriptCommand(
                iDArticleScript: articleScript.ID);
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new UpdateScriptCommandHandler(_logger.Object,
                _articleScriptCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            var updatedText = " updated";

            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeTitle + updatedText, articleScript.Title);
            Assert.Equal(fakeSummary + updatedText, articleScript.Summary);
        }
        [Fact]
        public async Task T029_Handler_Delete_Article_Script_Return_False_On_Error()
        {
            // Preparo el commando
            var command = fakeDeleteScriptCommand();
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(false)));
            // preparo el handler
            var handler = new DeleteScriptCommandHandler(_logger.Object,
                _articleScriptCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.False(result.success);
            Assert.NotEmpty(result.errors);
        }
        [Fact]
        public async Task T030_Handle_Add_Article_Script_Item()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            var command = fakeAddScriptItemCommand(
                MultimediaType.ScriptOnHeader.Id);
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new AddScriptItemCommandHandler(_logger.Object,
                _articleScriptItemCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.NotEmpty(articleScript.Items);
            Assert.Equal(fakeTitle, articleScript.Items.First().Title);
            Assert.Equal(fakeCode, articleScript.Items.First().Code);
            Assert.Equal(MultimediaType.ScriptOnHeader, articleScript.Items.First().MultimediaType);
        }
        [Fact]
        public async Task T031_Handler_Enable_Article_Script_Item()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                code: fakeCode,
                multimediaType: MultimediaType.ScriptOnBottom
                );
            var command = fakeEnableScriptItemCommand();
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new EnableScriptItemCommandHandler(_logger.Object,
                _articleScriptItemCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.True(articleScript.Items.First().Enabled);

        }
        [Fact]
        public async Task T032_Handler_Update_Article_Script_Item()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                code: fakeCode,
                multimediaType: MultimediaType.ScriptOnBottom
            );

            var command = fakeUpdateScriptItemCommand(
                iDMultimediaType: MultimediaType.ScriptOnHeader.Id);
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new UpdateScriptItemCommandHandler(_logger.Object,
                _articleScriptItemCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            var updatedText = " updated";

            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Equal(fakeTitle + updatedText, articleScript.Items.First().Title);
            Assert.Equal(fakeCode + updatedText, articleScript.Items.First().Code);
            Assert.Equal(MultimediaType.ScriptOnHeader, articleScript.Items.First().MultimediaType);
        }
        [Fact]
        public async Task T033_Handler_Delete_Article_Script()
        {
            // Preparo el commando
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                code: fakeCode,
                multimediaType: MultimediaType.ScriptOnBottom
            );
            var command = fakeDeleteScriptItemCommand();
            // Preparo el repositorio
            _articleScriptRepositoryMock.Setup(repository =>
                repository.GetAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(articleScript));
            _articleScriptRepositoryMock.Setup(repository =>
                repository.UnitOfWork.SaveEntitiesAsync(default(CancellationToken)))
                .Returns(Task.FromResult(fakeSaveEntitiesResponse(true)));
            // preparo el handler
            var handler = new DeleteScriptItemCommandHandler(_logger.Object,
                _articleScriptItemCacheMock.Object, _articleScriptRepositoryMock.Object);
            var cltToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cltToken);
            // pruebo que la respuesta contiene el success en falso y errores
            Assert.True(result.success);
            Assert.Null(result.errors);
            Assert.Empty(articleScript.Items);
        }
    }
}
