﻿using System;

namespace Pixelario.Logs.Application.QueryModels.LogEvents
{
    public class LogEventQueryModel
    {
        public int IDLogEvent { get; set; }
        public string Message { get; set; }
        public string MessageTemplate { get; set; }
        public string Level { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Exception { get; set; }
        public string Properties { get; set; }


    }
}
