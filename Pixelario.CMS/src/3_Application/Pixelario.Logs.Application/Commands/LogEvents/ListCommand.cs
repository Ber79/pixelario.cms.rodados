﻿using MediatR;
using Pixelario.Logs.Application.QueryModels.LogEvents;
using System;
using System.Collections.Generic;

namespace Pixelario.Logs.Application.Commands.LogEvents
{
    public class ListCommand : IRequest<List<LogEventQueryModel>>
    {
        private static readonly string[] ColumnOrders = { "le.Id", "le.Level" };
        private static readonly string[] OrderDirections = { "ASC", "DESC" };

        private int DefaultColumnOrder = 0;
        private int DefaultOrderDirection = 1;
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public string FilteredBy { get; private set; }
        public string Level { get; set; }
        public DateTime? TimeStampFrom { get; private set; }
        public DateTime? TimeStampTo { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo,
            int rowIndex,
            int rowCount,
            int? columnOrder,
            int? orderDirection)
        {
            this.FilteredBy = filteredBy;
            this.Level = level;
            this.TimeStampFrom = timeStampFrom;
            this.TimeStampTo = timeStampTo;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = columnOrder.HasValue ? ColumnOrders[columnOrder.Value]
                : ColumnOrders[this.DefaultColumnOrder];
            this.OrderDirection = orderDirection.HasValue ? OrderDirections[orderDirection.Value]
                : OrderDirections[this.DefaultOrderDirection];
        }
        public ListCommand(string level, int rowIndex, int rowCount)
        {
            this.FilteredBy = null;
            this.Level = level;
            this.TimeStampFrom = null;
            this.TimeStampTo = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = ColumnOrders[this.DefaultColumnOrder];
            this.OrderDirection = OrderDirections[this.DefaultOrderDirection];
        }
        public ListCommand(int rowIndex, int rowCount)
        {
            this.FilteredBy = null;
            this.Level = null;
            this.TimeStampFrom = null;
            this.TimeStampTo = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = ColumnOrders[this.DefaultColumnOrder];
            this.OrderDirection = OrderDirections[this.DefaultOrderDirection];
        }
        public ListCommand()
        {
            this.FilteredBy = null;
            this.Level = null;
            this.TimeStampFrom = null;
            this.TimeStampTo = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = ColumnOrders[this.DefaultColumnOrder];
            this.OrderDirection = OrderDirections[this.DefaultOrderDirection];
        }
    }
}
