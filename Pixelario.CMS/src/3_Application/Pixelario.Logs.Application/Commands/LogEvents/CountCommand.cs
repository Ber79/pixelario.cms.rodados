﻿using MediatR;
using System;

namespace Pixelario.Logs.Application.Commands.LogEvents
{
    public class CountCommand : IRequest<int>
    {
        public string FilteredBy { get; private set; }
        public string Level { get; set; }
        public DateTime? TimeStampFrom { get; private set; }
        public DateTime? TimeStampTo { get; private set; }
        public CountCommand(string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo)
        {
            this.FilteredBy = filteredBy;
            this.Level = level;
            this.TimeStampFrom = timeStampFrom;
            this.TimeStampTo = timeStampTo;
        }
        public CountCommand(string level)
        {
            this.FilteredBy = null;
            this.Level = level;
            this.TimeStampFrom = null;
            this.TimeStampTo = null;
        }
        public CountCommand()
        {
            this.FilteredBy = null;
            this.Level = null;
            this.TimeStampFrom = null;
            this.TimeStampTo = null;
        }
    }
}
