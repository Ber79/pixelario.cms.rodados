﻿using MediatR;
using Pixelario.Logs.Application.QueryModels.LogEvents;

namespace Pixelario.Logs.Application.Commands.LogEvents
{
    public class GetCommand : IRequest<LogEventQueryModel>
    {
        public int IDLogEvent { get; private set; }
        public GetCommand(int iDLogEvent)
        {
            IDLogEvent = iDLogEvent;
        }
    }
}
