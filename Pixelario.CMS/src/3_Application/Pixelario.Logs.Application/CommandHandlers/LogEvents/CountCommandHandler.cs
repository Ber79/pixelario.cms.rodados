﻿using MediatR;
using Pixelario.Logs.Application.Commands.LogEvents;
using Pixelario.Logs.Application.Queries.LogEvents;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.Logs.Application.CommandHandlers.LogEvents
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly ILogEventsQueries _logEventsQueries;
        public CountCommandHandler(
            ILogger logger,
            ILogEventsQueries logEventsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _logEventsQueries = logEventsQueries ?? throw new ArgumentNullException(nameof(logEventsQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            return await _logEventsQueries.CountAsync(
                filteredBy: command.FilteredBy,
                level: command.Level,
                timeStampFrom: command.TimeStampFrom,
                timeStampTo: command.TimeStampTo);
        }
    }
}