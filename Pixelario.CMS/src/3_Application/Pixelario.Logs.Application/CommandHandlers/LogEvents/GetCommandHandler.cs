﻿using MediatR;
using Pixelario.Logs.Application.Commands.LogEvents;
using Pixelario.Logs.Application.Queries.LogEvents;
using Pixelario.Logs.Application.QueryModels.LogEvents;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.Logs.Application.CommandHandlers.LogEvents
{
    public class GetCommandHandler : IRequestHandler<GetCommand, LogEventQueryModel>
    {
        private readonly ILogger _logger;
        private readonly ILogEventsQueries _logEventsQueries;
        public GetCommandHandler(
            ILogger logger,
            ILogEventsQueries logEventsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _logEventsQueries = logEventsQueries ?? throw new ArgumentNullException(nameof(logEventsQueries));
        }
        public async Task<LogEventQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            return await _logEventsQueries.GetAsync(
                iDLogEvent: command.IDLogEvent);
        }
    }
}