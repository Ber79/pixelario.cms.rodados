﻿using MediatR;
using Pixelario.Logs.Application.Commands.LogEvents;
using Pixelario.Logs.Application.Queries.LogEvents;
using Pixelario.Logs.Application.QueryModels.LogEvents;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.Logs.Application.CommandHandlers.LogEvents
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<LogEventQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly ILogEventsQueries _logEventsQueries;
        public ListCommandHandler(
            ILogger logger,
            ILogEventsQueries logEventsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _logEventsQueries = logEventsQueries ?? throw new ArgumentNullException(nameof(logEventsQueries));
        }
        public async Task<List<LogEventQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            return await _logEventsQueries.ListAsync(
                filteredBy: command.FilteredBy,
                level: command.Level,
                timeStampFrom: command.TimeStampFrom,
                timeStampTo: command.TimeStampTo,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
        }
    }
}