﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.Logs.Application.QueryModels.LogEvents;
using Serilog;

namespace Pixelario.Logs.Application.Queries.LogEvents
{
    public class LogEventsQueries : ILogEventsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public LogEventsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<LogEventQueryModel> GetAsync(int iDLogEvent)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT le.Id AS Id, le.Message AS Message, 
                        le.MessageTemplate AS MessageTemplate, 
                        le.Level AS Level, 
                        le.TimeStamp AS TimeStamp, le.Exception AS Exception, 
                        le.Properties AS Properties
                        FROM Logs AS le
                        WHERE le.Id = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDLogEvent });
                    return MapFullLogEvent(result);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private LogEventQueryModel MapFullLogEvent(dynamic result)
        {
            var logEvent = new LogEventQueryModel
            {
                IDLogEvent = result[0].Id,
                Message = result[0].Message,
                MessageTemplate = result[0].MessageTemplate,
                Level = result[0].Level,
                TimeStamp = result[0].TimeStamp,
                Exception = result[0].Exception,
                Properties = result[0].Properties
            };
            return logEvent;
        }

        public async Task<int> CountAsync(string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(filteredBy,
                    level,
                    timeStampFrom,
                    timeStampTo);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Logs AS le "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<LogEventQueryModel>> ListAsync(string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(filteredBy, level,
                    timeStampFrom, timeStampTo);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private Dictionary<string, string> SubLogEventBuilder(bool? includeEnableSubLogEvents,
            bool? includeHomePageSubLogEvents, bool? includeOnMenuSubLogEvents,
            string subLogEventColumnOrder,
            string subLogEventOrderDirection)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if ((includeEnableSubLogEvents.HasValue && includeEnableSubLogEvents.Value) ||
                (includeHomePageSubLogEvents.HasValue && includeHomePageSubLogEvents.Value) ||
                (includeOnMenuSubLogEvents.HasValue && includeOnMenuSubLogEvents.Value))
            {

                var subQueryWhereList = new List<string>();
                if (includeEnableSubLogEvents.HasValue && includeEnableSubLogEvents.Value)
                {
                    subQueryWhereList.Add("Properties = 1");
                }
                if (includeHomePageSubLogEvents.HasValue && includeHomePageSubLogEvents.Value)
                {
                    subQueryWhereList.Add("AtHome = 1");
                }
                if (includeOnMenuSubLogEvents.HasValue && includeOnMenuSubLogEvents.Value)
                {
                    subQueryWhereList.Add("OnMenu = 1");
                }
                var joinSubLogEventQuery = string.Format("SELECT IDSubLogEvent AS IDSubLogEvent, IDLogEvent as IDLogEvent, Level AS Level, {0} AS SubLogEvent_Order FROM SubLogEvents WHERE {1}",
                    !string.IsNullOrEmpty(subLogEventColumnOrder) ? subLogEventColumnOrder : "[Order]",
                    String.Join(" AND ", subQueryWhereList));
                var subLogEventsFields = ", st.IDSubLogEvent as IDSubLogEvent, st.Level as SubLogEventLevel";
                if (!string.IsNullOrEmpty(subLogEventColumnOrder) && !string.IsNullOrEmpty(subLogEventOrderDirection))
                {
                    subLogEventsFields += ", st.SubLogEvent_Order AS SubLogEvent_Order";
                    dictionary.Add("order",
                        ", SubLogEvent_Order " + subLogEventOrderDirection);
                }
                dictionary.Add("fields", subLogEventsFields);
                dictionary.Add("join", string.Format("LEFT JOIN ({0}) as st ON st.IDLogEvent = t.IDLogEvent ",
                    joinSubLogEventQuery));
            }
            else
            {
                dictionary.Add("join", "");
                dictionary.Add("fields", "");
                dictionary.Add("order", "");
            }
            return dictionary;
        }

        private static string WhereBuilder(string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo)
        {
            List<string> wheresParaQuery = new List<string>();
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("(le.Message Like '%{0}%' OR le.Exception Like '%{0}%')",
                    filteredBy));
            if (!string.IsNullOrEmpty(level))
                wheresParaQuery.Add(string.Format("le.Level Like '{0}%'",
                    level));
            if (timeStampFrom.HasValue && timeStampTo.HasValue)
            {                
                wheresParaQuery.Add(string.Format("le.TimeStamp >= '{0}' AND le.TimeStamp < '{1}'",
                    timeStampFrom.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                    timeStampTo.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")));               
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        private async Task<List<LogEventQueryModel>> QueryDb(
            string whereStatments, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT le.Id AS Id, le.Message AS Message, 
                    le.MessageTemplate AS MessageTemplate, 
                    le.Level AS Level, 
                    le.TimeStamp AS TimeStamp, le.Exception AS Exception, 
                    le.Properties AS Properties
                    FROM Logs AS le " +
                    whereStatments +
                    @" ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query, new
                {
                    rowIndex,
                    rowCount
                });
                return MapListToLogEventsList(result);
            }
        }

        private List<LogEventQueryModel> MapListToLogEventsList(dynamic result)
        {
            var logEventsList = new List<LogEventQueryModel>();
            var logEvent = new LogEventQueryModel()
            {
                IDLogEvent = -1
            };
            foreach (var item in result)
            {
                if (logEvent.IDLogEvent == -1)
                {
                    logEvent = new LogEventQueryModel()
                    {
                        IDLogEvent = item.Id,
                        Message = item.Message,
                        MessageTemplate = item.MessageTemplate,
                        Level = item.Level,
                        TimeStamp = item.TimeStamp,
                        Exception = item.Exception,
                        Properties = item.Properties
                    };
                }
                else if (logEvent.IDLogEvent != item.IDLogEvent)
                {
                    logEventsList.Add(logEvent);
                    logEvent = new LogEventQueryModel()
                    {
                        IDLogEvent = item.Id,
                        Message = item.Message,
                        MessageTemplate = item.MessageTemplate,
                        Level = item.Level,
                        TimeStamp = item.TimeStamp,
                        Exception = item.Exception,
                        Properties = item.Properties

                    };
                }
                
            }
            if (logEvent.IDLogEvent != -1 && !logEventsList.Contains(logEvent))
                logEventsList.Add(logEvent);
            return logEventsList;
        }
    }
}