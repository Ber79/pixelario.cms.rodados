﻿using Pixelario.Logs.Application.QueryModels.LogEvents;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.Logs.Application.Queries.LogEvents
{
    public interface ILogEventsQueries
    {
        /// <summary>
        /// Get a log event by his unique id
        /// </summary>
        /// <param name="iDLogEvent">unique id of a log event</param>
        /// <returns>View model of a log event</returns>
        Task<LogEventQueryModel> GetAsync(int iDLogEvent);
        /// <summary>
        ///  List of log events paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="filteredBy">text to filter on exception column</param>
        /// <param name="level">level to filteted</param>
        /// <param name="timeStampFrom">date from start to filter</param>
        /// <param name="timeStampTo">date to stop to filter</param>
        /// <param name="rowIndex"></param>
        /// <param name="rowCount"></param>
        /// <param name="columnOrder"></param>
        /// <param name="orderDirection"></param>
        /// <returns>Listo of log events model</returns>
        Task<List<LogEventQueryModel>> ListAsync(
            string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo,
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);

        /// <summary>
        /// Count the number of log events filtered by
        /// diferents options
        /// </summary>
        /// <param name="filteredBy">text to filter on exception column</param>
        /// <param name="level">level to filteted</param>
        /// <param name="timeStampFrom">date from start to filter</param>
        /// <param name="timeStampTo">date to stop to filter</param>
        /// <returns>Number with the total count</returns>
        Task<int> CountAsync(
            string filteredBy,
            string level,
            DateTime? timeStampFrom,
            DateTime? timeStampTo);
    }
}