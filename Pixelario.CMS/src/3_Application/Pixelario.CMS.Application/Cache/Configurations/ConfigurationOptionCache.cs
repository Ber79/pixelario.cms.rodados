﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Cache.Configurations
{
    public class ConfigurationOptionCache : IConfigurationOptionCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public ConfigurationOptionCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }
        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("configurationOptions"))
                    _cache.Remove(pair.Key);
                if (pair.Key.StartsWith("configurations"))
                    _cache.Remove(pair.Key);
            }
        }

        public int? GetCount(string keyAddition)
        {
            var cacheKey = "configurationOptions_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ConfigurationOptionQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "configurationOptions_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (ConfigurationOptionQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ConfigurationOptionQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "configurationOptions_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<ConfigurationOptionQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "configurationOptions_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

        public void SetItem(ConfigurationOptionQueryModel value, string keyAddition)
        {
            var cacheKey = "configurationOptions_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

        public void SetList(List<ConfigurationOptionQueryModel> value, string keyAddition)
        {
            var cacheKey = "configurationOptions_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
    }
}
