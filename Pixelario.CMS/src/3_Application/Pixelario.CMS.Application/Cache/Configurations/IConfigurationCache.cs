﻿using Pixelario.CMS.Application.QueryModels.Configurations;

namespace Pixelario.CMS.Application.Cache.Configurations
{
    public interface IConfigurationCache : ICache<ConfigurationQueryModel>
    {
        string GetState(string keyAddition);
        void SetState(string value, string keyAddition);
    }
}
