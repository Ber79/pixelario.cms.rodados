﻿using Pixelario.CMS.Application.QueryModels.Configurations;

namespace Pixelario.CMS.Application.Cache.Configurations
{
    public interface IConfigurationOptionCache : ICache<ConfigurationOptionQueryModel>
    {
    }
}
