﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Cache.Configurations
{
    public class ConfigurationCache : IConfigurationCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public ConfigurationCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }
        public int? GetCount(string keyAddition)
        {
            var cacheKey = "configurations_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("configurations"))
                    _cache.Remove(pair.Key);
            }
        }

        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "configurations_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

        public List<ConfigurationQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "configurations_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<ConfigurationQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetList(List<ConfigurationQueryModel> value, string keyAddition)
        {
            var cacheKey = "configurations_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

        public ConfigurationQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "configurations_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (ConfigurationQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void SetItem(ConfigurationQueryModel value, string keyAddition)
        {
            var cacheKey = "configurations_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

        public string GetState(string keyAddition)
        {
            var cacheKey = "configurations_state";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (string)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void SetState(string value, string keyAddition)
        {
            var cacheKey = "configurations_state";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
    }
}
