﻿using System.Collections.Generic;

namespace Pixelario.CMS.Application.Cache
{
    public interface ICache<T> where T : class
    {
        List<T> GetList(string keyAddition);
        void SetList(List<T> value, string keyAddition);
        int? GetCount(string keyAddition);
        void SetCount(int value, string keyAddition);
        T GetItem(string keyAddition);
        void SetItem(T value, string keyAddition);
        void Clear();
    }
}