﻿using Pixelario.CMS.Application.QueryModels.Advertisments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Cache.Advertisments
{
    public class AdvertismentCache : IAdvertismentCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public AdvertismentCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }

        public int? GetCount(string keyAddition)
        {
            var cacheKey = "advertisments_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "advertisments_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public List<AdvertismentQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "advertisments_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<AdvertismentQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetList(List<AdvertismentQueryModel> value, string keyAddition)
        {
            var cacheKey = "advertisments_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("advertisments"))
                    _cache.Remove(pair.Key);
            }
        }
        public AdvertismentQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "advertisments_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (AdvertismentQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetItem(AdvertismentQueryModel value, string keyAddition)
        {
            var cacheKey = "advertisments_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }

    }
}
