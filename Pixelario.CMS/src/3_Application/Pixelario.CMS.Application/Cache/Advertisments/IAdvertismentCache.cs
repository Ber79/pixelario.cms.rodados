﻿using Pixelario.CMS.Application.QueryModels.Advertisments;

namespace Pixelario.CMS.Application.Cache.Advertisments
{
    public interface IAdvertismentCache : ICache<AdvertismentQueryModel>
    {
    }
}
