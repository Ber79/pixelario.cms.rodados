﻿using Pixelario.CMS.Application.QueryModels.Topics;

namespace Pixelario.CMS.Application.Cache.Topics
{
    public interface ISubTopicCache : ICache<SubTopicQueryModel>
    {
    }
}
