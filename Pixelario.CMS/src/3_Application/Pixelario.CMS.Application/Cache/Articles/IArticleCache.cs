﻿using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Application.Cache.Articles
{
    public interface IArticleCache : ICache<ArticleQueryModel>
    {
    }
}
