﻿using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Application.Cache.Articles
{
    public interface IArticleScriptCache : ICache<ArticleScriptQueryModel>
    {
    }
}
