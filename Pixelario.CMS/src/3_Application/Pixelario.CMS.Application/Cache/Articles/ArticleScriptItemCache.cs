﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace Pixelario.CMS.Application.Cache.Articles
{
    public class ArticleScriptItemCache : IArticleScriptItemCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public ArticleScriptItemCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }

        public int? GetCount(string keyAddition)
        {
            var cacheKey = "articleScriptItems_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "articleScriptItems_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public List<ArticleScriptItemQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "articleScriptItems_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<ArticleScriptItemQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetList(List<ArticleScriptItemQueryModel> value, string keyAddition)
        {
            var cacheKey = "articleScriptItems_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("articleScriptItems"))
                    _cache.Remove(pair.Key);
                if (pair.Key.StartsWith("articles"))
                    _cache.Remove(pair.Key);
            }
        }
        public ArticleScriptItemQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "articleScriptItems_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (ArticleScriptItemQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetItem(ArticleScriptItemQueryModel value, string keyAddition)
        {
            var cacheKey = "articleScriptItems_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
    }
}
