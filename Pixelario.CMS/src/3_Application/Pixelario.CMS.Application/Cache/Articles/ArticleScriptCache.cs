﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Cache.Articles
{
    public class ArticleScriptCache : IArticleScriptCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public ArticleScriptCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }

        public int? GetCount(string keyAddition)
        {
            var cacheKey = "articleScripts_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "articleScripts_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public List<ArticleScriptQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "articleScripts_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<ArticleScriptQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetList(List<ArticleScriptQueryModel> value, string keyAddition)
        {
            var cacheKey = "articleScripts_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("articleScripts"))
                    _cache.Remove(pair.Key);
            }
        }
        public ArticleScriptQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "articleScripts_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (ArticleScriptQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetItem(ArticleScriptQueryModel value, string keyAddition)
        {
            var cacheKey = "articleScripts_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
    }
}
