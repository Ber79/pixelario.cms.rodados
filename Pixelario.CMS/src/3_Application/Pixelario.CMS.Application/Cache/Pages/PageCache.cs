﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Cache.Pages
{
    public class PageCache : IPageCache
    {
        private MemoryCache _cache;
        private readonly CacheItemPolicy _masterPolicy;
        public PageCache(MemoryCache cache)
        {
            _cache = cache;
            _masterPolicy = new CacheItemPolicy();
            _masterPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddYears(10));
            _masterPolicy.Priority = CacheItemPriority.NotRemovable;
        }

        public int? GetCount(string keyAddition)
        {
            var cacheKey = "pages_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (int)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetCount(int value, string keyAddition)
        {
            var cacheKey = "pages_count";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public List<PageQueryModel> GetList(string keyAddition)
        {
            var cacheKey = "pages_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (List<PageQueryModel>)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetList(List<PageQueryModel> value, string keyAddition)
        {
            var cacheKey = "pages_list";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
        public void Clear()
        {
            foreach (var pair in _cache.ToList())
            {
                if (pair.Key.StartsWith("pages"))
                    _cache.Remove(pair.Key);
                if (pair.Key.StartsWith("editions"))
                    _cache.Remove(pair.Key);
            }            
        }
        public PageQueryModel GetItem(string keyAddition)
        {
            var cacheKey = "pages_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            if (!_cache.Contains(cacheKey))
            {
                return null;
            }
            try
            {
                return (PageQueryModel)_cache.Get(cacheKey);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void SetItem(PageQueryModel value, string keyAddition)
        {
            var cacheKey = "pages_item";
            if (!string.IsNullOrEmpty(keyAddition))
                cacheKey += string.Format("_{0}",
                    keyAddition);
            _cache.Add(cacheKey, value, _masterPolicy);
        }
    }
}