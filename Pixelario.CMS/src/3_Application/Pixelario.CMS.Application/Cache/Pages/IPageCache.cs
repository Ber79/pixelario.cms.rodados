﻿using Pixelario.CMS.Application.QueryModels.Pages;

namespace Pixelario.CMS.Application.Cache.Pages
{
    public interface IPageCache : ICache<PageQueryModel>
    {
    }
}
