﻿using Pixelario.CMS.Application.QueryModels.Editions;

namespace Pixelario.CMS.Application.Cache
{
    public interface IEditionCache : ICache<EditionQueryModel>
    {
    }
}
