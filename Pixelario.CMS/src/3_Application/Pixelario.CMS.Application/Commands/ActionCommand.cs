﻿namespace Pixelario.CMS.Application.Commands
{
    public class ActionCommand
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}