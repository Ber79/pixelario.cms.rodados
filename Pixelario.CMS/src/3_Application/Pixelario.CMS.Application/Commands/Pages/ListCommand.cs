﻿using MediatR;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Pages
{
    public class ListCommand : IRequest<List<PageQueryModel>>
    {
        private string DefaultColumnOrder = "p.IDPage";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int? IDEdition { get; private set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu, int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            IDEdition = iDEdition;
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListCommand(int iDEdition, int rowIndex, int rowCount)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand(int rowIndex, int rowCount)
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDEdition.HasValue ? 0 :
                this.IDEdition.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (this.OnMenu == null ? 0 :
                this.OnMenu.Value ? 2: 1);
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }
    }
}
