﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Pages;

namespace Pixelario.CMS.Application.Commands.Pages
{
    public class GetMultimediaCommand : IRequest<PageMultimediaQueryModel>
    {
        public int IDMultimedia { get; private set; }
        public GetMultimediaCommand(int iDMultimedia)
        {
            IDMultimedia = iDMultimedia;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDMultimedia;
            return hash;
        }
    }
}
