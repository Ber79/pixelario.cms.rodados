﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Pages;

namespace Pixelario.CMS.Application.Commands.Pages
{
    public class GetCommand : IRequest<PageQueryModel>
    {
        public int IDPage { get; private set; }
        public GetCommand(int iDPage)
        {
            IDPage = iDPage;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDPage;
            return hash;
        }

    }
}
