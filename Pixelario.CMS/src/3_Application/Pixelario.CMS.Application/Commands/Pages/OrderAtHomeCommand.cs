﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class OrderAtHomeCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public int Order { get; set; }

        public OrderAtHomeCommand()
        {

        }
        public OrderAtHomeCommand(int iDPage,
            int order)
        {
            IDPage = iDPage;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}