﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class ChangeImageCommand : ActionCommand, 
        IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public ImageType ImageType { get; set; }
        public string FileName { get; set; }
        public string AppDomainAppPath { get; set; }
        public ChangeImageCommand()
        {

        }
        public ChangeImageCommand(
            int iDPage,
            ImageType imageType, 
            string appDomainAppPath, string fileName)
        {
            this.IDPage = iDPage;
            this.ImageType = imageType;
            this.FileName = fileName;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}