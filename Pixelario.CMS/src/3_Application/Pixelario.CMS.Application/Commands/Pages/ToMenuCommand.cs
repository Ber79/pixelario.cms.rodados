﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class ToMenuCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public int? IDEdition { get; set; }
        public ToMenuCommand()
        {

        }
        public ToMenuCommand(int iDPage)
        {
            IDPage = iDPage;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}