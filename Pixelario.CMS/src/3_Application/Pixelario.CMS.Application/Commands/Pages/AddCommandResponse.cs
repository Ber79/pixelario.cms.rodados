﻿using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class AddCommandResponse : CommandResponse
    {
        public int? IDPage { get; set; }
    }
}