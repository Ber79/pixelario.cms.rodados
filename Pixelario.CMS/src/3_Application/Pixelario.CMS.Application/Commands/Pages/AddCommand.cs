﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Pages
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public string Body { get; set; }
        public List<int> Editions { get; set; }
        public int? IDEdition { get; set; }
        public AddCommand()
        {

        }
        public AddCommand(string title,
            string summary, string body,
            string keywords,
            List<int> editions)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Body = body ?? throw new ArgumentNullException(nameof(body));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            Editions = editions ?? throw new ArgumentNullException(nameof(editions));
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}