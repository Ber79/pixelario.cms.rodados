﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class OrderAtMenuCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public int Order { get; set; }
        public int? IDEdition { get; set; }

        public OrderAtMenuCommand()
        {

        }
        public OrderAtMenuCommand(int iDPage,
            int order)
        {
            IDPage = iDPage;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}