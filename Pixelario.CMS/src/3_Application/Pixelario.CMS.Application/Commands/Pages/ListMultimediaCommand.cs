﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Domain.Commons;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Pages
{
    public class ListMultimediaCommand : IRequest<List<PageMultimediaQueryModel>>
    {
        private string DefaultColumnOrder = "pm.IDPage";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int IDPage { get; private set; }
        public MultimediaType MultimediaType { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListMultimediaCommand(int iDPage, MultimediaType multimediaType,
            string filteredBy,
            bool? enabled, bool? atHome,

            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            this.IDPage = iDPage;
            this.MultimediaType = multimediaType;
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListMultimediaCommand(int iDPage, MultimediaType multimediaType,
            int rowIndex, int rowCount)
        {
            this.IDPage = iDPage;
            this.MultimediaType = multimediaType;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListMultimediaCommand(int iDPage, MultimediaType multimediaType)
        {
            this.IDPage = iDPage;
            this.MultimediaType = multimediaType;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDPage;

            if (this.MultimediaType != null)
                hash = hash * 23 + this.MultimediaType.GetHashCode() + 1;
            else
                hash = hash * 23;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }
    }
}
