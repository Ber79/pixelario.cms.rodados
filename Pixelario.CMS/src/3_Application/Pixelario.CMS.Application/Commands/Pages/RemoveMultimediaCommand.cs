﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class RemoveMultimediaCommand : 
        ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public int IDMultimedia { get; set; }
        public int? IDEdition { get; set; }

        public RemoveMultimediaCommand()
        {

        }
        public RemoveMultimediaCommand(
            int iDPage,
            int iDMultimedia)
        {
            this.IDPage = iDPage;
            this.IDMultimedia = iDMultimedia;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}