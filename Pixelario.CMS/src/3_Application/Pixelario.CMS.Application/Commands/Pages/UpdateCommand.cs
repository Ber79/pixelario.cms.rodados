﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDPage { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Keywords { get; set; }
        public List<int> Editions { get; set; }
        public int? IDEdition { get; set; }
        public UpdateCommand()
        {

        }
        public UpdateCommand(int iDPage, string title,
            string summary, string body, string keywords,
            List<int> editions)
        {
            IDPage = iDPage;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Body = body ?? throw new ArgumentNullException(nameof(body));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            Editions = editions ?? throw new ArgumentNullException(nameof(editions));

        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}