﻿using MediatR;
namespace Pixelario.CMS.Application.Commands.Pages
{
    public class CountCommand : IRequest<int>
    {
        public int? IDEdition { get; private set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }

        public CountCommand(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu)
        {
            IDEdition = iDEdition;
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
        }
        public CountCommand(int? iDEdition)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
        }
        public CountCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
        }
    }
}
