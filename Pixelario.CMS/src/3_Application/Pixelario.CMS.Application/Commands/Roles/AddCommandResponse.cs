﻿using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class AddCommandResponse : CommandResponse
    {
        public int? ID { get; set; }
    }
}