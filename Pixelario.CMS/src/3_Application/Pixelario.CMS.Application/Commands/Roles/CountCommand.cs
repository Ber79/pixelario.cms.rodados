﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class CountCommand : IRequest<int>
    {
        public CountCommand()
        {
        }
    }
}
