﻿using MediatR;
using Pixelario.Identity.Domain;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class GetCommand : IRequest<ApplicationRole>
    {
        public int RoleId { get; private set; }
        public GetCommand(int roleId)
        {
            RoleId = roleId;
        }
    }
}
