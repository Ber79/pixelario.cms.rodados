﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class DeleteCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int RoleId { get; set; }
        public DeleteCommand()
        {

        }
        public DeleteCommand(int roleId)
        {
            RoleId = roleId;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }

    }
}
