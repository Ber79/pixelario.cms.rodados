﻿using MediatR;
using Pixelario.Identity.Infrastructure.Managers;
using System;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string RoleName { get; set; }
        public AddCommand()
        {

        }
        public AddCommand(string roleName)
        {
            RoleName = roleName ?? throw new ArgumentNullException(nameof(roleName));
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }       
    }
}            
    