﻿using MediatR;
using Pixelario.Identity.Domain;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class ListCommand : IRequest<List<ApplicationRole>>
    {
        private int DefaultRowIndex = 0;
        private int DefaultRowCount = 10;
        private string DefaultOrderBy = "Id";
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string OrderBy { get; private set; }
        public ListCommand(
            int rowIndex,
            int rowCount)
        {
            this.OrderBy = this.DefaultOrderBy;
            RowIndex = rowIndex;
            RowCount = rowCount;
        }
        public ListCommand()
        {
            this.OrderBy = this.DefaultOrderBy;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
        }
        public ListCommand(string orderBy)
        {
            this.OrderBy = orderBy;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
        }
    }
}
