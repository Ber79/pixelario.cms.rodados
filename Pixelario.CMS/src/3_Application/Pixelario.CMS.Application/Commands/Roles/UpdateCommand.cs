﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.Identity.Infrastructure.Managers;
using System;

namespace Pixelario.CMS.Application.Commands.Roles
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public UpdateCommand()
        {

        }
        public UpdateCommand(int roleId, string roleName)
        {
            RoleId = roleId;
            RoleName = roleName;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}            
    