﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class ChangeEditionTypeCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public int EditionType { get; set; }

        public ChangeEditionTypeCommand()
        {

        }
        public ChangeEditionTypeCommand(int iDEdition,
            int editionType)
        {
            IDEdition = iDEdition;
            EditionType = editionType;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}