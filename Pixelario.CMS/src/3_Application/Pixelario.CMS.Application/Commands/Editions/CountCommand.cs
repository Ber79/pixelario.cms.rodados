﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class CountCommand : IRequest<int>
    {
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public List<EditionType> EditionTypes { get; private set; }

        public CountCommand(string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes)
        {
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
            EditionTypes = editionTypes;
        }
        public CountCommand()
        {
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.EditionTypes = null;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());
            if (this.EditionTypes != null)
            {
                foreach (var item in this.EditionTypes)
                {
                    hash = hash * 23 + (item == null ? 0 :
                        item.GetHashCode());
                }
            }
            return hash;
        }
    }
}
