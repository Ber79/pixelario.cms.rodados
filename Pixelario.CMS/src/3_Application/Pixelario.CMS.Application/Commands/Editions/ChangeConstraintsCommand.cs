﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class ChangeConstraintsCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public List<int> PlansSelected { get; set; }
        public ChangeConstraintsCommand()
        {

        }
        public ChangeConstraintsCommand(int iDEdition,
            List<int> plansSelected)
        {
            IDEdition = iDEdition;
            PlansSelected = plansSelected;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}