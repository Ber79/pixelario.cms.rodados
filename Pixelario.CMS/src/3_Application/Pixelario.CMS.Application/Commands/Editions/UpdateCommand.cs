﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public UpdateCommand(int idEdition, string title,
            string summary, string keywords)
        {
            IDEdition = idEdition;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}