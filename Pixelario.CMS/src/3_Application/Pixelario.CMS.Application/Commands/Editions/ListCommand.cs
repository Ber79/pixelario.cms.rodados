﻿using MediatR;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class ListCommand : IRequest<List<EditionQueryModel>>
    {
        private string DefaultColumnOrder = "e.IDEdition";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public List<EditionType> EditionTypes { get; private set; }
        public PagesOnEditionQueryFilter PagesOnEditionFilter { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes,
            PagesOnEditionQueryFilter pagesOnEditionFilter,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
            EditionTypes = editionTypes;
            PagesOnEditionFilter = pagesOnEditionFilter;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListCommand()
        {
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.EditionTypes = null;
            this.PagesOnEditionFilter = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());
            if (this.EditionTypes != null)
            {
                foreach (var item in this.EditionTypes)
                {
                    hash = hash * 23 + (item == null ? 0 :
                        item.GetHashCode());
                }
            }
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (this.PagesOnEditionFilter == null ? 0 :
                   this.PagesOnEditionFilter.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }
    }
}
