﻿using MediatR;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class GetCommand : IRequest<EditionQueryModel>
    {
        public int IDEdition { get; private set; }
        public GetCommand(int iDEdition)
        {
            IDEdition = iDEdition;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDEdition;
            return hash;
        }

    }
}
