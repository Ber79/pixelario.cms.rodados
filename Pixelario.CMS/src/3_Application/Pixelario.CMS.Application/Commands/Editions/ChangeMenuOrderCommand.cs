﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class ChangeMenuOrderCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public int Order { get; set; }

        public ChangeMenuOrderCommand()
        {

        }
        public ChangeMenuOrderCommand(int idEdition,
            int order)
        {
            IDEdition = idEdition;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}