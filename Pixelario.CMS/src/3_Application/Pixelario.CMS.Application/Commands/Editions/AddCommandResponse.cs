﻿using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class AddCommandResponse : CommandResponse
    {
        public int? IDEdition { get; set; }
    }
}