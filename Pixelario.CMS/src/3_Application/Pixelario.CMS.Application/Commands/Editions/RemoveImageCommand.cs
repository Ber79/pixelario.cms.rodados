﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class RemoveImageCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public ImageType ImageType { get; set; }
        public RemoveImageCommand()
        {

        }
        public RemoveImageCommand(
            int iDEdition,
            ImageType imageType)
        {
            this.IDEdition = iDEdition;
            this.ImageType = imageType;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}