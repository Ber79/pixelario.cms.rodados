﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class ToMenuCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public ToMenuCommand()
        {

        }
        public ToMenuCommand(int idEdition)
        {
            IDEdition = idEdition;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}