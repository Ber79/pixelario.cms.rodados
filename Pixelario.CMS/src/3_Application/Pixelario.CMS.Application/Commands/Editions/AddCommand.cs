﻿using MediatR;
using System;

namespace Pixelario.CMS.Application.Commands.Editions
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public AddCommand(string title,
            string summary, string keywords)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}