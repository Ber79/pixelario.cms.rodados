﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Editions
{
    public class DeleteCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDEdition { get; set; }
        public DeleteCommand()
        {

        }
        public DeleteCommand(int idEdition)
        {
            IDEdition = idEdition;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}