﻿using MediatR;
using Pixelario.Identity.Domain;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class GetByNameCommand : IRequest<ApplicationUser>
    {
        public string UserName { get; private set; }
        public GetByNameCommand(string userName)
        {
            this.UserName = userName;
        }
    }
}
