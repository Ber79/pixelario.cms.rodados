﻿using MediatR;
using Pixelario.Identity.Domain;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class GetCommand : IRequest<ApplicationUser>
    {
        public int UserId { get; private set; }
        public GetCommand(int userId)
        {
            UserId = userId;
        }
    }
}
