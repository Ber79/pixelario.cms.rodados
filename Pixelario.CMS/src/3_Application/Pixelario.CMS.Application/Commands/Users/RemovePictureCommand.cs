﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Users
{
    public class RemovePictureCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get; set; }
        public RemovePictureCommand()
        {

        }
        public RemovePictureCommand(int userId)
        {
            UserId = userId;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}