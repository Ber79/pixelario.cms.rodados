﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Users
{
    public class ChangePasswordCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get;  set; }
        public string Password { get; set; }
        public ChangePasswordCommand()
        {

        }
        public ChangePasswordCommand(int userId,
            string password)
        {
            Password = password ?? throw new ArgumentNullException(nameof(password));
            SetUserId(userId);
        }
        public void SetUserId(int userId)
        {
            UserId = userId;

        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }

    }
}