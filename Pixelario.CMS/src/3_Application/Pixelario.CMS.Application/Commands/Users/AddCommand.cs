﻿using MediatR;
using Pixelario.Identity.Infrastructure.Managers;
using System;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public AddCommand()
        {

        }
        public AddCommand(string userName,
            string email, string password,
            string firstName, string lastName)
        {
            UserName = userName ?? throw new ArgumentNullException(nameof(userName));
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Password = password ?? throw new ArgumentNullException(nameof(password));
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }       
    }
}