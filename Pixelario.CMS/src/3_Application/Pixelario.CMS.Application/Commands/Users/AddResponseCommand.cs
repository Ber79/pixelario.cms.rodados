﻿using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Users
{
    public class AddCommandResponse : CommandResponse
    {
        public int? ID { get; set; }
    }
}