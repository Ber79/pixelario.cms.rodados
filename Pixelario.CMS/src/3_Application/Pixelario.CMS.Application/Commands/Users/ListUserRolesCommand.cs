﻿using MediatR;
using Pixelario.Identity.Domain;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class ListUserRolesCommand : IRequest<List<string>>
    {
        public int UserId { get; private set; }
        public ListUserRolesCommand(
            int userId)
        {
            this.UserId = userId;
        }
    }
}
