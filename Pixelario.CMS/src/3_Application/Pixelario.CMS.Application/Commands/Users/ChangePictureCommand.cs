﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Users
{
    public class ChangePictureCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get; set; }
        public string FileName { get; private set; }
        public string AppDomainAppPath { get; private set; }
        public ChangePictureCommand()
        {

        }
        public ChangePictureCommand(int userId,
            string appDomainAppPath, string fileName)
        {
            UserId = userId;
            SetAppDomainAppPath(appDomainAppPath, fileName);
        }
        public void SetAppDomainAppPath(string appDomainAppPath, string fileName)
        {
            AppDomainAppPath = appDomainAppPath ?? throw new ArgumentNullException(nameof(appDomainAppPath));
            FileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }

    }
}