﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.Identity.Infrastructure.Managers;
using System;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class SetRolesCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get; set; }
        public string[] Roles { get; set; }
        public SetRolesCommand()
        {

        }
        public SetRolesCommand(int userId,
            string[] roles)
        {
            UserId = userId;
            Roles = roles;
        }
        public void SetSolicitedBy(string solicitedBy)
        {            
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}