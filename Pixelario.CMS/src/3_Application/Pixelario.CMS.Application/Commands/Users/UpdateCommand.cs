﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UpdateCommand()
        {

        }
        public UpdateCommand(int userId,
            string firstName, string lastName) : this(firstName, lastName)
        {
            SetUserId(userId);
        }
        public UpdateCommand(
            string firstName, string lastName)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
        public void SetUserId(int userId)
        {
            this.UserId = userId;
        }
    }
}