﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class CountCommand : IRequest<int>
    {
        public CountCommand()
        {
        }
    }
}
