﻿using MediatR;
using Pixelario.Identity.Domain;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class ListCommand : IRequest<List<ApplicationUser>>
    {
        private int DefaultRowIndex = 0;
        private int DefaultRowCount = 10;
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public ListCommand(
            int rowIndex,
            int rowCount)
        {
            RowIndex = rowIndex;
            RowCount = rowCount;
        }
        public ListCommand()
        {
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
        }
    }
}
