﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Users
{
    public class DeleteCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int UserId { get; set; }
        public DeleteCommand()
        {

        }
        public DeleteCommand(int userId)
        {
            UserId = userId;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }

    }
}