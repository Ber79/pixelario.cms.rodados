﻿using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class AddCommandResponse : CommandResponse
    {
        public int? IDAdvertisment { get; set; }
    }
}