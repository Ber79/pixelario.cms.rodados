﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string URL { get;  set; }
        public AddCommand(string title,
            string code, string url)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Code = code;
            URL = url;
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}