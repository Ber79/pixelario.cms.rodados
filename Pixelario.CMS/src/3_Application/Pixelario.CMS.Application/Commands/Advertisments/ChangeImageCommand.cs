﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class ChangeImageCommand : ActionCommand, 
        IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        public ImageType ImageType { get; set; }
        public string FileName { get; set; }
        public string AppDomainAppPath { get; set; }
        public ChangeImageCommand()
        {

        }
        public ChangeImageCommand(
            int iDAdvertisment,
            ImageType imageType, 
            string appDomainAppPath, string fileName)
        {
            this.IDAdvertisment = iDAdvertisment;
            this.ImageType = imageType;
            this.FileName = fileName;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}