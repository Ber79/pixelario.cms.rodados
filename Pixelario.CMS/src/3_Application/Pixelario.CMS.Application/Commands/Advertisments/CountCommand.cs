﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class CountCommand : IRequest<int>
    {
        public int? PlaceAtWeb { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? Published { get; private set; }
        public CountCommand(int? placeAtWeb,
                string filteredBy,
                bool? enabled,
                bool? published)
        {
            this.PlaceAtWeb = placeAtWeb;
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
            this.Published = published;
        }
        public CountCommand()
        {
            this.PlaceAtWeb = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.Published = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.PlaceAtWeb.HasValue ? 0 :
                this.PlaceAtWeb.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.Published.HasValue ? 0 :
                this.Published.GetHashCode());
            return hash;
        }

    }
}
