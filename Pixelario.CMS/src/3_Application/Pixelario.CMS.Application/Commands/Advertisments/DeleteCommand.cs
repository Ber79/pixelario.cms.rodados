﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class DeleteCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        //public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public DeleteCommand()
        {

        }
        public DeleteCommand(int iDAdvertisment)
        {
            IDAdvertisment = iDAdvertisment;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}