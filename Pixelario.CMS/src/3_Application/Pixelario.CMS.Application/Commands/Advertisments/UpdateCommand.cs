﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string URL { get; set; }
        public string Source { get; set; }
        public UpdateCommand()
        {

        }
        public UpdateCommand(int iDAdvertisment,
            string title,
            string code, string url)
        {
            IDAdvertisment = iDAdvertisment;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Code = code;
            URL = url;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
        
    }
}