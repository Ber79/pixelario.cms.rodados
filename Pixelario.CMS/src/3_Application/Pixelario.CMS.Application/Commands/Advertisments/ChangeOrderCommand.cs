﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class ChangeOrderCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        public int Order { get; set; }
        //public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }
        public ChangeOrderCommand()
        {

        }
        public ChangeOrderCommand(int iDAdvertisment,
            int order)
        {
            IDAdvertisment = iDAdvertisment;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}