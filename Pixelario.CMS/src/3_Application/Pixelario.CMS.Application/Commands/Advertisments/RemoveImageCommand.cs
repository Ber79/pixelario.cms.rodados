﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class RemoveImageCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        public ImageType ImageType { get; set; }
        //public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public RemoveImageCommand()
        {

        }
        public RemoveImageCommand(
            int iDAdvertisment,
            ImageType imageType)
        {
            this.IDAdvertisment = iDAdvertisment;
            this.ImageType = imageType;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}