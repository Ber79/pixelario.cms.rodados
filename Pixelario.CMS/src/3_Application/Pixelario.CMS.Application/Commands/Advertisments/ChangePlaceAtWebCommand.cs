﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class ChangePlaceAtWebCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDAdvertisment { get; set; }
        public int PlaceAtWeb { get; set; }
        //public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public ChangePlaceAtWebCommand()
        {

        }
        public ChangePlaceAtWebCommand(int iDAdvertisment,
            int placeAtWeb)
        {
            IDAdvertisment = iDAdvertisment;
            PlaceAtWeb = placeAtWeb;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}