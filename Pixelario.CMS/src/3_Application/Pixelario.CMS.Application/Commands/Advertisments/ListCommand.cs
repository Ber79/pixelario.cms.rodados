﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Advertisments;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class ListCommand : IRequest<List<AdvertismentQueryModel>>
    {
        private string DefaultColumnOrder = "a.IDAdvertisment";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int? PlaceAtWeb { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? Published { get; private set; } 
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(int? placeAtWeb,
            string filteredBy,
            bool? enabled,
            bool? published,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            this.PlaceAtWeb = placeAtWeb;
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
            this.Published = published;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = columnOrder;
            this.OrderDirection = orderDirection;
        }
        public ListCommand(int iDEdition, int rowIndex, int rowCount)
        {
            this.PlaceAtWeb = iDEdition;
            this.FilteredBy = null;
            this.Enabled = null;
            this.Published = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand(int rowIndex, int rowCount)
        {
            this.PlaceAtWeb = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.Published = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand()
        {
            this.PlaceAtWeb = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.Published = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.PlaceAtWeb.HasValue ? 0 :
                this.PlaceAtWeb.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.Published.HasValue ? 0 :
                this.Published.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }

    }
}
