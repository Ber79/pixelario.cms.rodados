﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Advertisments;

namespace Pixelario.CMS.Application.Commands.Advertisments
{
    public class GetCommand : IRequest<AdvertismentQueryModel>
    {
        public int IDAdvertisment { get; private set; }
        public GetCommand(int iDAdvertisment)
        {
            IDAdvertisment = iDAdvertisment;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDAdvertisment;
            return hash;
        }
    }
}
