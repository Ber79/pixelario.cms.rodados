﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Configurations;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class GetConfigurationOptionCommand : IRequest<ConfigurationOptionQueryModel>
    {
        public int IDConfigurationOption { get; private set; }
        public GetConfigurationOptionCommand(int iDConfigurationOption)
        {
            IDConfigurationOption = iDConfigurationOption;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDConfigurationOption;
            return hash;
        }
    }
}
