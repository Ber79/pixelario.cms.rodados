﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class GetStateCommand : IRequest<string>
    {
        public string Key { get; private set; }
        public GetStateCommand(string key)
        {
            this.Key = key;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.Key) ? 0 :
                this.Key.GetHashCode());            
            return hash;
        }
    }
}
