﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class EnableConfigurationOptionCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public int IDConfigurationOption { get; set; }

        public EnableConfigurationOptionCommand(){}
        public EnableConfigurationOptionCommand(int iDConfiguration,
            int iDConfigurationOption)
        {
            IDConfiguration = iDConfiguration;
            IDConfigurationOption = iDConfigurationOption;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}