﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Configurations;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class GetCommand : IRequest<ConfigurationQueryModel>
    {
        public int IDConfiguration { get; private set; }
        public GetCommand(int iDConfiguration)
        {
            IDConfiguration = iDConfiguration;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDConfiguration;
            return hash;
        }
    }
}
