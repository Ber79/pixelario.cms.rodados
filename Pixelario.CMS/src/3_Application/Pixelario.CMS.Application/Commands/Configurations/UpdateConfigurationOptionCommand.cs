﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class UpdateConfigurationOptionCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public int IDConfigurationOption { get; set; }
        public string Value { get; set; }
        public UpdateConfigurationOptionCommand()
        {

        }
        public UpdateConfigurationOptionCommand(int iDConfiguration,
            int iDConfigurationOption, string value)
        {
            IDConfiguration = iDConfiguration;
            IDConfigurationOption = iDConfigurationOption;
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}