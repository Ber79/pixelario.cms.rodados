﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class BindRolesCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public string[] Roles { get; set; }
        public BindRolesCommand(int iDConfiguration, string[] roles)
        {
            IDConfiguration = iDConfiguration;
            Roles = roles;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}