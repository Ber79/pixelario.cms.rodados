﻿using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class SearchAndPagingOptionsToReturn
    {
        public string SearchText { get; set; }        
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public int? ConfigurationType { get; set; }
    }
}