﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public ConfigurationType ConfigurationType { get; set; }
        public UpdateCommand(int iDConfiguration, string title,
            string key,
            ConfigurationType configurationType)
        {
            IDConfiguration = iDConfiguration;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Key = key ?? throw new ArgumentNullException(nameof(key));
            ConfigurationType = configurationType;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}