﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class CountCommand : IRequest<int>
    {
        public string FilteredBy { get; private set; }
        public int? ConfigurationType { get; private set; }
        public string[] Keys { get; private set; }
        public CountCommand(
                string filteredBy,
                int? configurationType,
                string[] keys)
        {
            this.FilteredBy = filteredBy;
            this.Keys = keys;
            this.ConfigurationType = configurationType;
        }
        public CountCommand()
        {
            this.FilteredBy = null;
            this.Keys = null;
            this.ConfigurationType = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                 this.FilteredBy.GetHashCode());
            if (this.Keys != null)
            {
                foreach (var item in this.Keys)
                {
                    hash = hash * 23 + (item == null ? 0 :
                        item.GetHashCode());
                }
            }
            hash = hash * 23 + (!this.ConfigurationType.HasValue ? 0 :
                this.ConfigurationType.Value.GetHashCode());
            return hash;
        }

    }
}
