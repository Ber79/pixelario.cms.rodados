﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class SetStateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public string Key { get; set; }
        public string State { get; set; }
        public SetStateCommand(string key, string state)
        {
            Key = key;
            State = state;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}