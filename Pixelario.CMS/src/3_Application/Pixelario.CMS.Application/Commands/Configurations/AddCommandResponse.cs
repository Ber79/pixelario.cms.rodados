﻿using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class AddCommandResponse : CommandResponse
    {
        public int? IDConfiguration { get; set; }
    }
}