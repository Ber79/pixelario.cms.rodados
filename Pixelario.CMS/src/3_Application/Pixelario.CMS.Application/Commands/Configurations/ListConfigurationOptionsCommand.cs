﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class ListConfigurationOptionsCommand : IRequest<List<ConfigurationOptionQueryModel>>
    {
        private string DefaultColumnOrder = "co.IDConfigurationOption";
        private string DefaultOrderDirection = "DESC";
        public int IDConfiguration { get; private set; }
        public bool? Enabled { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListConfigurationOptionsCommand(int iDConfiguration,
            bool? enabled,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            IDConfiguration = iDConfiguration;
            Enabled = enabled;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListConfigurationOptionsCommand(int iDConfiguration, int rowIndex, int rowCount)
        {
            this.IDConfiguration = iDConfiguration;
            this.Enabled = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDConfiguration;
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.Value.GetHashCode() + 1);
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + this.ColumnOrder.GetHashCode();
            hash = hash * 23 + this.OrderDirection.GetHashCode();
            return hash;
        }

    }
}
