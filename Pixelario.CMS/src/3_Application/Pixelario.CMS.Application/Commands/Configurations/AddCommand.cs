﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using System;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public ConfigurationType ConfigurationType { get; set; }
        public AddCommand(string title,
            string key, 
            ConfigurationType configurationType)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Key = key ?? throw new ArgumentNullException(nameof(key));
            ConfigurationType = configurationType;
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}