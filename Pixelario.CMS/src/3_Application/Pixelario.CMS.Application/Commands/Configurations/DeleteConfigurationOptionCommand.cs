﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class DeleteConfigurationOptionCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public int IDConfigurationOption { get; set; }

        public DeleteConfigurationOptionCommand()
        {

        }
        public DeleteConfigurationOptionCommand(int iDConfiguration,
            int iDConfigurationOption)
        {
            this.IDConfiguration = iDConfiguration;
            this.IDConfigurationOption = iDConfigurationOption;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}