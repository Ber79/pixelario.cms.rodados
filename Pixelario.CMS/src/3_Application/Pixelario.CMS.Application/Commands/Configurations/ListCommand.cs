﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class ListCommand : IRequest<List<ConfigurationQueryModel>>
    {
        private static readonly string[] ColumnsOrder = { "c.IDConfiguration", "c.Title", "c.[Key]" };
        private static readonly string[] OrderDirections = { "ASC", "DESC" };
        private string DefaultColumnOrder = "c.IDConfiguration";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public string FilteredBy { get; private set; }
        public int? ConfigurationType { get; private set; }
        public string[] Keys { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string OrderBy { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(string filteredBy,
            int? configurationType,
            string[] keys,
            int rowIndex,
            int rowCount,
            int orderBy,
            int orderDirection)
        {
            this.FilteredBy = filteredBy;
            this.Keys = keys;
            this.ConfigurationType = configurationType;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.OrderBy = ColumnsOrder[orderBy];
            this.OrderDirection = OrderDirections[orderDirection];
        }
       
        public ListCommand(int rowIndex, int rowCount)
        {
            this.FilteredBy = null;
            this.Keys = null;
            this.ConfigurationType = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.OrderBy = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand()
        {
            this.FilteredBy = null;
            this.Keys = null;
            this.ConfigurationType = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.OrderBy = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            if (this.Keys != null)
            {
                foreach (var item in this.Keys)
                {
                    hash = hash * 23 + (item == null ? 0 :
                        item.GetHashCode());
                }
            }
            hash = hash * 23 + (!this.ConfigurationType.HasValue ? 0 :
                this.ConfigurationType.Value.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + this.OrderBy.GetHashCode();
            hash = hash * 23 + this.OrderDirection.GetHashCode();
            return hash;
        }

    }
}
