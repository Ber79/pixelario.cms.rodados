﻿using MediatR;
namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class CountConfigurationOptionsCommand : IRequest<int>
    {
        public int IDConfiguration { get; private set; }
        public bool? Enabled { get; private set; }

        public CountConfigurationOptionsCommand(int iDConfiguration,
            bool? enabled)
        {
            IDConfiguration = iDConfiguration;
            Enabled = enabled;
        }
        public CountConfigurationOptionsCommand(int iDConfiguration)
        {
            this.IDConfiguration = iDConfiguration;
            this.Enabled = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDConfiguration;
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.Value.GetHashCode() + 1);
            return hash;
        }
    }
}
