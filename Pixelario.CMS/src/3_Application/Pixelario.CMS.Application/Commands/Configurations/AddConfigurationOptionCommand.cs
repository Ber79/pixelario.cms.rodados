﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Configurations
{
    public class AddConfigurationOptionCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDConfiguration { get; set; }
        public string Value { get; set; }
        public AddConfigurationOptionCommand()
        {

        }
        public AddConfigurationOptionCommand(
            int iDConfiguration, string value)
        {
            IDConfiguration = iDConfiguration;
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}