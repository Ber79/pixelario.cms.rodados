﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Topics;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class GetCommand : IRequest<TopicQueryModel>
    {
        public int IDTopic { get; private set; }
        public GetCommand(int iDTopic)
        {
            IDTopic = iDTopic;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDTopic;
            return hash;
        }

    }
}
