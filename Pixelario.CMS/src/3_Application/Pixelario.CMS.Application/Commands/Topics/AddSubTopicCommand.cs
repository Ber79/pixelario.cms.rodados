﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class AddSubTopicCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }

        public AddSubTopicCommand(
            int iDTopic, string title,
            string summary, string keywords
            )
        {
            IDTopic = iDTopic;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}