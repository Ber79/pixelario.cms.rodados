﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class ChangeTopicHomeOrderCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int Order { get; set; }

        public ChangeTopicHomeOrderCommand(){}
        public ChangeTopicHomeOrderCommand(int iDTopic,
            int order)
        {
            IDTopic = iDTopic;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}