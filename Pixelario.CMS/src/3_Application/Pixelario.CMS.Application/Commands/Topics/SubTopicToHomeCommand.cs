﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class SubTopicToHomeCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int IDSubTopic { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }

        public SubTopicToHomeCommand()
        {

        }
        public SubTopicToHomeCommand(int iDTopic, 
            int iDSubTopic)
        {
            IDTopic = iDTopic;
            IDSubTopic = iDSubTopic;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}