﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class DeleteSubTopicCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int IDSubTopic { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }

        public DeleteSubTopicCommand()
        {

        }
        public DeleteSubTopicCommand(int iDTopic, int iDSubTopic)
        {
            this.IDTopic = iDTopic;
            this.IDSubTopic = iDSubTopic;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}