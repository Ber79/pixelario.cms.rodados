﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class ListCommand : IRequest<List<TopicQueryModel>>
    {
        private string DefaultColumnOrder = "t.IDTopic";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int? IDEdition { get; private set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public bool? IncludeEnableSubTopics { get; set; }
        public bool? IncludeHomePageSubTopics { get; set; }
        public bool? IncludeOnMenuSubTopics { get; set; }
        public string SubTopicColumnOrder { get; set; }
        public string SubTopicOrderDirection { get; set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            bool? includeEnableSubTopics,
            bool? includeHomePageSubTopics,
            bool? includeOnMenuSubTopics,
            string subTopicColumnOrder,
            string subTopicOrderDirection,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
            this.AtHome = atHome;
            this.OnMenu = onMenu;
            this.IncludeEnableSubTopics = includeEnableSubTopics;
            this.IncludeHomePageSubTopics = includeHomePageSubTopics;
            this.IncludeOnMenuSubTopics = includeOnMenuSubTopics;
            this.SubTopicColumnOrder = subTopicColumnOrder;
            this.SubTopicOrderDirection = subTopicOrderDirection;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = columnOrder;
            this.OrderDirection = orderDirection;
        }
        public ListCommand(int iDEdition, int rowIndex, int rowCount)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.IncludeEnableSubTopics = null;
            this.IncludeHomePageSubTopics = null;
            this.IncludeOnMenuSubTopics = null;
            this.SubTopicColumnOrder = null;
            this.SubTopicOrderDirection = null;

            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand(int rowIndex, int rowCount)
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.IncludeEnableSubTopics = null;
            this.IncludeHomePageSubTopics = null;
            this.IncludeOnMenuSubTopics = null;
            this.SubTopicColumnOrder = null;
            this.SubTopicOrderDirection = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.IncludeEnableSubTopics = null;
            this.IncludeHomePageSubTopics = null;
            this.IncludeOnMenuSubTopics = null;
            this.SubTopicColumnOrder = null;
            this.SubTopicOrderDirection = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDEdition.HasValue ? 0 :
                this.IDEdition.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());

            hash = hash * 23 + (!this.IncludeEnableSubTopics.HasValue ? 0 :
                !this.IncludeEnableSubTopics.Value ? 1 : 2);
            hash = hash * 23 + (!this.IncludeHomePageSubTopics.HasValue ? 0 :
                !this.IncludeHomePageSubTopics.Value ? 1 : 2);
            hash = hash * 23 + (!this.IncludeOnMenuSubTopics.HasValue ? 0 :
                !this.IncludeOnMenuSubTopics.Value ? 1 : 2);

            hash = hash * 23 + (string.IsNullOrEmpty(this.SubTopicColumnOrder) ? 0 :
                this.SubTopicColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.SubTopicOrderDirection) ? 0 :
                this.SubTopicOrderDirection.GetHashCode());

            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }

    }
}
