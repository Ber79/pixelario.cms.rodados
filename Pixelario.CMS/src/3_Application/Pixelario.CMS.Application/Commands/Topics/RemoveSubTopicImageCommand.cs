﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class RemoveSubTopicImageCommand : 
        ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int IDSubTopic { get; set; }
        public ImageType ImageType { get; set; }
        public RemoveSubTopicImageCommand()
        {

        }
        public RemoveSubTopicImageCommand(
            int iDTopic, int iDSubTopic,
            ImageType imageType)
        {
            this.IDTopic = iDTopic;
            this.IDSubTopic = iDSubTopic;
            this.ImageType = imageType;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}