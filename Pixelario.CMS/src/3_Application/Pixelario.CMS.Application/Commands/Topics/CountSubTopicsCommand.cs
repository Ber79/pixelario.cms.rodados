﻿using MediatR;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class CountSubTopicsCommand : IRequest<int>
    {
        public int? IDTopic { get; private set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }

        public CountSubTopicsCommand(int? iDTopic, string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu)
        {
            IDTopic = iDTopic;
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
        }
        public CountSubTopicsCommand(int? iDTopic)
        {
            this.IDTopic = iDTopic;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
        }
        public CountSubTopicsCommand()
        {
            this.IDTopic = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDTopic.HasValue ? 0 :
                this.IDTopic.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());
            return hash;
        }
    }
}
