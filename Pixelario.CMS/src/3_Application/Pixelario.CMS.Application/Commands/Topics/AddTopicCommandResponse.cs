﻿using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class AddTopicCommandResponse : CommandResponse
    {
        public int? IDTopic { get; set; }
    }
}