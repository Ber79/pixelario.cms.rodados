﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class ChangeSubTopicImageCommand : ActionCommand, 
        IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int IDSubTopic { get; set; }
        public ImageType ImageType { get; set; }
        public string FileName { get; set; }
        public string AppDomainAppPath { get; set; }
        public ChangeSubTopicImageCommand()
        {

        }
        public ChangeSubTopicImageCommand(
            int iDTopic, int iDSubTopic,
            ImageType imageType, 
            string appDomainAppPath, string fileName)
        {
            this.IDTopic = iDTopic;
            this.IDSubTopic = iDSubTopic;
            this.ImageType = imageType;
            this.FileName = fileName;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}