﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Topics;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class GetSubTopicCommand : IRequest<SubTopicQueryModel>
    {
        public int IDSubTopic { get; private set; }
        public GetSubTopicCommand(int iDSubTopic)
        {
            IDSubTopic = iDSubTopic;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDSubTopic;
            return hash;
        }
    }
}
