﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class ChangeSubTopicOrderCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public int IDSubTopic { get; set; }
        public int Order { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }

        public ChangeSubTopicOrderCommand()
        {

        }
        public ChangeSubTopicOrderCommand(
            int iDTopic, int iDSubTopic,
            int order)
        {
            IDTopic = iDTopic;
            IDSubTopic = iDSubTopic;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}