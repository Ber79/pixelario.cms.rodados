﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class ListSubTopicsCommand : IRequest<List<SubTopicQueryModel>>
    {
        private string DefaultColumnOrder = "s.IDSubTopic";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int? IDTopic { get; private set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListSubTopicsCommand(int? iDTopic, string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu, int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            IDTopic = iDTopic;
            FilteredBy = filteredBy;
            Enabled = enabled;
            AtHome = atHome;
            OnMenu = onMenu;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListSubTopicsCommand(int iDTopic, int rowIndex, int rowCount)
        {
            this.IDTopic = iDTopic;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListSubTopicsCommand(int rowIndex, int rowCount)
        {
            this.IDTopic = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListSubTopicsCommand()
        {
            this.IDTopic = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDTopic.HasValue ? 0 :
                this.IDTopic.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }
    }
}
