﻿using MediatR;
using System;
using System.Collections.Generic;


namespace Pixelario.CMS.Application.Commands.Topics
{
    public class AddTopicCommand : IRequest<AddTopicCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public List<int> Editions { get; set; }

        public AddTopicCommand(string title,
            string summary, string keywords,
            List<int> editions)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            Editions = editions ?? throw new ArgumentNullException(nameof(editions));
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}