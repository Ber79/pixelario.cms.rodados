﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class UpdateTopicCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Keywords { get; set; }
        public List<int> Editions { get; set; }

        public UpdateTopicCommand(int iDTopic, string title,
            string summary, string keywords,
            List<int> editions)
        {
            IDTopic = iDTopic;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            Editions = editions ?? throw new ArgumentNullException(nameof(editions));

        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}