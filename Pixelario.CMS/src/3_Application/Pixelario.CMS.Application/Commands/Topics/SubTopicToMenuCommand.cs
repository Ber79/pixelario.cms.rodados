﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class SubTopicToMenuCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }

        public int IDSubTopic { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }

        public SubTopicToMenuCommand()
        {

        }
        public SubTopicToMenuCommand(int iDSubTopic, int iDTopic)
        {
            IDSubTopic = iDSubTopic;
            IDTopic = iDTopic;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}