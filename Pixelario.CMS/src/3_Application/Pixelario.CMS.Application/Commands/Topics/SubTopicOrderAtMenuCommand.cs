﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class SubTopicOrderAtMenuCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDSubTopic { get; set; }
        public int IDTopic { get; set; }
        public int Order { get; set; }
        public int TopicPageNumber { get; set; }
        public int TopicPageSize { get; set; }

        public SubTopicOrderAtMenuCommand()
        {

        }
        public SubTopicOrderAtMenuCommand(int iDSubTopic, int iDTopic,
            int order)
        {
            IDSubTopic = iDSubTopic;
            IDTopic = iDTopic;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}