﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Topics
{
    public class CountCommand : IRequest<int>
    {
        public int? IDEdition { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public bool? OnMenu { get; private set; }
        public CountCommand(int? iDEdition,
                string filteredBy,
                bool? enabled,
                bool? atHome,
                bool? onMenu)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
            this.AtHome = atHome;
            this.OnMenu = onMenu;
        }
        public CountCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.AtHome = null;
            this.OnMenu = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDEdition.HasValue ? 0 :
                this.IDEdition.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (!this.OnMenu.HasValue ? 0 :
                this.OnMenu.GetHashCode());            
            return hash;
        }

    }
}
