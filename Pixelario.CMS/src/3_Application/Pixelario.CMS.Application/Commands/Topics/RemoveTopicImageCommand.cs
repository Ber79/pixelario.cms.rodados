﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Topics
{
    public class RemoveTopicImageCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDTopic { get; set; }
        public ImageType ImageType { get; set; }
        public RemoveTopicImageCommand()
        {

        }
        public RemoveTopicImageCommand(
            int iDTopic,
            ImageType imageType)
        {
            this.IDTopic = iDTopic;
            this.ImageType = imageType;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}