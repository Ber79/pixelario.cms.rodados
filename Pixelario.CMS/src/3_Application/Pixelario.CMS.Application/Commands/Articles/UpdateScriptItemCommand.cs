﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class UpdateScriptItemCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScriptItem { get; set; }
        public int IDArticleScript { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public int IDMultimediaType { get; set; }
        public UpdateScriptItemCommand()
        {

        }
        public UpdateScriptItemCommand(int iDArticleScript, 
            int iDArticleScriptItem, string title,
            string code, int iDMultimediaType)
        {
            IDArticleScript = iDArticleScript;
            IDArticleScriptItem = iDArticleScriptItem;
            IDMultimediaType = iDMultimediaType;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Code = code ?? throw new ArgumentNullException(nameof(code));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
    }
}