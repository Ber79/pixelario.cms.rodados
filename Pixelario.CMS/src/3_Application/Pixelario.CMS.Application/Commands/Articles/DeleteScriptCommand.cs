﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class DeleteScriptCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }
        public DeleteScriptCommand()
        {

        }
        public DeleteScriptCommand(int iDScript)
        {
            IDArticleScript = iDScript;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}