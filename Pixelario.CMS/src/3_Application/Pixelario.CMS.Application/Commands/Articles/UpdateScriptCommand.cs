﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class UpdateScriptCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public UpdateScriptCommand()
        {
        }
        public UpdateScriptCommand(int iDScript, string title,
            string summary)
        {
            this.IDArticleScript = iDScript;
            this.Title = title ?? throw new ArgumentNullException(nameof(title));
            this.Summary = summary ?? throw new ArgumentNullException(nameof(summary));
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
        
    }
}