﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class DeleteScriptItemCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }
        public int IDArticleScriptItem { get; set; }
        public DeleteScriptItemCommand()
        {

        }
        public DeleteScriptItemCommand(int iDArticleScript, int iDArticleScriptItem)
        {
            this.IDArticleScript = iDArticleScript;
            this.IDArticleScriptItem = iDArticleScriptItem;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}