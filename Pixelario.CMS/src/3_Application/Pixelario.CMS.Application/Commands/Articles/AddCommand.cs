﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class AddCommand : IRequest<AddCommandResponse>
    {
        public string CreatedBy { get; private set; }
        public int IDEdition { get;  set; }
        public DateTime? PublicationDate { get; set; }

        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get;  set; }
        public string Source { get; set; }
        public string Keywords { get; set; }
        public List<int> Topics { get; set; }
        public List<int> SubTopics { get; set; }
        public List<int> Scripts { get; set; }

        public AddCommand()
        {
            this.Topics = new List<int>();
            this.SubTopics = new List<int>();
            this.Scripts = new List<int>();
        }
        public AddCommand(int iDEdition, DateTime? publicationDate, string title,
            string summary, string body, string source,
            string keywords, 
            List<int> topics, List<int> subTopics, List<int> scripts)
        {
            this.IDEdition = iDEdition;
            this.PublicationDate = publicationDate;
            this.Title = title ?? throw new ArgumentNullException(nameof(title));
            this.Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            this.Body = body;
            this.Source = source;
            this.Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            this.Topics = topics;
            this.SubTopics = subTopics;
            this.Scripts = scripts;
        }
        public void SetCreatedBy(string createdBy)
        {
            this.CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
        }
    }
}