﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class RemoveMultimediaCommand : 
        ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public int IDMultimedia { get; set; }

        public RemoveMultimediaCommand()
        {

        }
        public RemoveMultimediaCommand(
            int iDArticle,
            int iDMultimedia)
        {
            this.IDArticle = iDArticle;
            this.IDMultimedia = iDMultimedia;            
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}