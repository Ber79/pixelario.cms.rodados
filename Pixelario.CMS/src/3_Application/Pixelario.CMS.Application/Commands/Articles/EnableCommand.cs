﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class EnableCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }

        public EnableCommand()
        {

        }
        public EnableCommand(int iDArticle)
        {
            IDArticle = iDArticle;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}