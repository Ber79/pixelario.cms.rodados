﻿using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ArticleResponseCommand : CommandResponse
    {
        public int? IDEdition { get; set; }
    }
}