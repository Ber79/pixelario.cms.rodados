﻿using Pixelario.CMS.SeedWork;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class AddCommandResponse : CommandResponse
    {
        public int? IDArticle { get; set; }
    }
}