﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class CountScriptCommand : IRequest<int>
    {
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public CountScriptCommand(
            string filteredBy,
            bool? enabled)
        {
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
        }
        public CountScriptCommand()
        {
            this.FilteredBy = null;
            this.Enabled = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            return hash;
        }
    }
}
