﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class GetScriptCommand : IRequest<ArticleScriptQueryModel>
    {
        public int IDArticleScript { get; private set; }
        public GetScriptCommand(int iDArticleScript)
        {
            IDArticleScript = iDArticleScript;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDArticleScript;
            return hash;
        }
    }
}
