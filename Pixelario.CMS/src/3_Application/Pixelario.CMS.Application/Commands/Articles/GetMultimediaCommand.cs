﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class GetMultimediaCommand : IRequest<ArticleMultimediaQueryModel>
    {
        public int IDMultimedia { get; private set; }
        public GetMultimediaCommand(int iDMultimedia)
        {
            IDMultimedia = iDMultimedia;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDMultimedia;
            return hash;
        }
    }
}
