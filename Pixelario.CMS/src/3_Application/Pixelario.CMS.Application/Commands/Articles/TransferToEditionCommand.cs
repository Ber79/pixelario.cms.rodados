﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class TransferToEditionCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }

        public int IDEdition { get; set; }

        public TransferToEditionCommand()
        {

        }
        public TransferToEditionCommand(int iDArticle,
            int iDEdition)
        {
            IDArticle = iDArticle;
            IDEdition = iDEdition;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}