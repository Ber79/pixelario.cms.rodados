﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ChangeConstraintsCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }

        public int IDArticle { get; set; }
        public List<int> PlansSelected { get; set; }
        public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public ChangeConstraintsCommand()
        {

        }
        public ChangeConstraintsCommand(int iDArticle, List<int> plansSelected)
        {
            this.IDArticle = iDArticle;
            this.PlansSelected = plansSelected;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}