﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class GetScriptItemCommand : IRequest<ArticleScriptItemQueryModel>
    {
        public int IDArticleScriptItem { get; private set; }
        public GetScriptItemCommand(int iDArticleScriptItem)
        {
            IDArticleScriptItem = iDArticleScriptItem;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDArticleScriptItem;
            return hash;
        }
    }
}
