﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class GetCommand : IRequest<ArticleQueryModel>
    {
        public int IDArticle { get; private set; }
        public GetCommand(int iDArticle)
        {
            IDArticle = iDArticle;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDArticle;
            return hash;
        }
    }
}
