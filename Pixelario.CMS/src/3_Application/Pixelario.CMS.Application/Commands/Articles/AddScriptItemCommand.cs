﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class AddScriptItemCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public int IDMultimediaType { get; set; }
        public AddScriptItemCommand()
        {

        }
        public AddScriptItemCommand(int iDArticleScript,
            string title, string code,
            int iDMultimediaType)
        {
            this.IDArticleScript = iDArticleScript;
            this.Title = title;
            this.Code = code;
            this.IDMultimediaType = iDMultimediaType;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}