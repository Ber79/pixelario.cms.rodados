﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ListScriptCommand : IRequest<List<ArticleScriptQueryModel>>
    {
        private string DefaultColumnOrder = "ars.IDArticleScript";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListScriptCommand(
            string filteredBy,
            bool? enabled,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = columnOrder;
            this.OrderDirection = orderDirection;
        }        
        public ListScriptCommand(int rowIndex, int rowCount)
        {
            this.FilteredBy = null;
            this.Enabled = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListScriptCommand()
        {
            this.FilteredBy = null;
            this.Enabled = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }

    }
}
