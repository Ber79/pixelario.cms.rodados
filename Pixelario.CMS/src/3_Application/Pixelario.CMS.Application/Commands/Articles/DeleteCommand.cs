﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class DeleteCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public DeleteCommand()
        {

        }
        public DeleteCommand(int iDArticle)
        {
            IDArticle = iDArticle;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}