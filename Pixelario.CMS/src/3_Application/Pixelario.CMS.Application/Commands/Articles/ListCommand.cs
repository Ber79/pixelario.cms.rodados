﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ListCommand : IRequest<List<ArticleQueryModel>>
    {
        private string DefaultColumnOrder = "a.IDArticle";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int? IDEdition { get; set; }
        public string FilteredBy { get; private set; }
        public int? IDTopic { get; set; }
        public int? IDSubTopic { get; set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public ArticlePlaceAtHome PlacesAtHome { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListCommand(int? iDEdition,
            string filteredBy,
            int? iDTopic,
            int? iDSubTopic,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = filteredBy;
            this.IDTopic = iDTopic;
            this.IDSubTopic = iDSubTopic;
            this.Enabled = enabled;
            this.AtHome = atHome;
            this.PlacesAtHome = placesAtHome;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = columnOrder;
            this.OrderDirection = orderDirection;
        }
        public ListCommand(int? iDEdition, int rowIndex, int rowCount)
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = null;
            this.IDTopic = null;
            this.IDSubTopic = null;
            this.Enabled = null;
            this.AtHome = null;
            this.PlacesAtHome = null;

            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand(int rowIndex, int rowCount)
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.IDTopic = null;
            this.IDSubTopic = null;
            this.Enabled = null;
            this.AtHome = null;
            this.PlacesAtHome = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.IDTopic = null;
            this.IDSubTopic = null;
            this.Enabled = null;
            this.AtHome = null;

            this.PlacesAtHome = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDEdition.HasValue ? 0 :
                this.IDEdition.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());

            hash = hash * 23 + (!this.IDTopic.HasValue ? 0 :
                this.IDTopic.Value);
            hash = hash * 23 + (!this.IDSubTopic.HasValue ? 0 :
                this.IDSubTopic.Value);

            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (this.PlacesAtHome == null ? 0 :
                this.PlacesAtHome.GetHashCode() + 1);
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }

    }
}
