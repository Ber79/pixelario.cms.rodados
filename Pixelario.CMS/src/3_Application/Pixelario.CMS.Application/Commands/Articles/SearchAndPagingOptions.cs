﻿using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class SearchAndPagingOptionsToReturn
    {
        public string SearchText { get; set; }
        public string PublicationDate { get; set; }
        public string Enable { get; set; }
        public string AtHome { get; set; }
        public string PlaceAtHome { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
    }
}