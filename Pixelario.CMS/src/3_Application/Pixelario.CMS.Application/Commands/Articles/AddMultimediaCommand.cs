﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class AddMultimediaCommand :
        ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string TypeName { get; set; }
        private MultimediaType _multimediaType;
        public MultimediaType MultimediaType
        {
            get
            {
                if (!string.IsNullOrEmpty(this.TypeName))
                {
                    return MultimediaType.FromName(this.TypeName);
                }
                else
                    return _multimediaType;
            }
            set
            {
                _multimediaType = value;
            }
        }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public string AppDomainAppPath { get; set; }
        public AddMultimediaCommand()
        {

        }
        public AddMultimediaCommand(
            int iDArticle,
            MultimediaType multimediaType,
            string title, string summary,
            string appDomainAppPath, string path1,
            string path2)
        {
            this.IDArticle = iDArticle;
            this.Title = title;
            this.Summary = summary;
            this.MultimediaType = multimediaType;
            this.Path1 = path1;
            this.Path2 = path2;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}