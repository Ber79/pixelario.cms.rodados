﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ChangePlaceAtHomeCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public int PlaceAtHome { get; set; }

        public ChangePlaceAtHomeCommand()
        {

        }
        public ChangePlaceAtHomeCommand(int iDArticle,
            int placeAtHome)
        {
            IDArticle = iDArticle;
            PlaceAtHome = placeAtHome;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}