﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class CountCommand : IRequest<int>
    {
        public int? IDEdition { get; set; }
        public string FilteredBy { get; private set; }
        public int? IDTopic { get; set; }
        public int? IDSubTopic { get; set; }
        public bool? Enabled { get; private set; }
        public bool? AtHome { get; private set; }
        public ArticlePlaceAtHome PlacesAtHome { get; private set; }
        public CountCommand(int? iDEdition, 
            string filteredBy,
            int? iDTopic,
            int? iDSubTopic,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome
            )
        {
            this.IDEdition = iDEdition;
            this.FilteredBy = filteredBy;
            this.IDTopic = iDTopic;
            this.IDSubTopic = iDSubTopic;
            this.Enabled = enabled;
            this.AtHome = atHome;
            this.PlacesAtHome = placesAtHome;
        }
        public CountCommand()
        {
            this.IDEdition = null;
            this.FilteredBy = null;
            this.IDTopic = null;
            this.IDSubTopic = null;
            this.Enabled = null;
            this.AtHome = null;
            this.PlacesAtHome = null;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.IDEdition.HasValue ? 0 :
                this.IDEdition.Value);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());

            hash = hash * 23 + (!this.IDTopic.HasValue ? 0 :
                this.IDTopic.Value);
            hash = hash * 23 + (!this.IDSubTopic.HasValue ? 0 :
                this.IDSubTopic.Value);

            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + (!this.AtHome.HasValue ? 0 :
                this.AtHome.GetHashCode());
            hash = hash * 23 + (this.PlacesAtHome == null ? 0 :
                this.PlacesAtHome.GetHashCode() + 1);
            return hash;
        }
    }
}
