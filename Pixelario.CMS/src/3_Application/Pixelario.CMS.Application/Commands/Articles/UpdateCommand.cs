﻿using MediatR;
using Pixelario.CMS.SeedWork;
using System;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class UpdateCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public DateTime? PublicationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Source { get; set; }
        public string Keywords { get; set; }
        public List<int> Topics { get; set; }
        public List<int> SubTopics { get; set; }
        public List<int> Scripts { get; set; }

        public UpdateCommand()
        {
            this.Topics = new List<int>();
            this.SubTopics = new List<int>();
            this.Scripts = new List<int>();

        }
        public UpdateCommand(int iDArticle, DateTime? publicationDate, string title,
            string summary, string body, string source, string keywords,
            List<int> topics, List<int> subTopics,
            List<int> scripts)
        {
            this.IDArticle = iDArticle;
            this.PublicationDate = publicationDate;
            this.Title = title ?? throw new ArgumentNullException(nameof(title));
            this.Summary = summary ?? throw new ArgumentNullException(nameof(summary));
            this.Body = body;
            this.Source = source;
            this.Keywords = keywords ?? throw new ArgumentNullException(nameof(keywords));
            this.Topics = topics;
            this.SubTopics = subTopics;
            this.Scripts = scripts;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            this.SolicitedBy = solicitedBy ?? throw new ArgumentNullException(nameof(solicitedBy));
        }
        
    }
}