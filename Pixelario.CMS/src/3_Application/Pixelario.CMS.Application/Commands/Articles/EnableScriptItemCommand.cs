﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class EnableScriptItemCommand : IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }
        public int IDArticleScriptItem { get; set; }
        public EnableScriptItemCommand(){}
        public EnableScriptItemCommand(int iDArticleScript,
            int iDArticleScriptItem)
        {
            this.IDArticleScript = iDArticleScript;
            this.IDArticleScriptItem = iDArticleScriptItem;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            this.SolicitedBy = solicitedBy;
        }
    }
}