﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class AddScriptCommand : IRequest<CommandResponse>
    {
        public string CreatedBy { get; private set; }
        public int IDArticle { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public AddScriptCommand()
        {

        }
        public AddScriptCommand(
            string title, string summary)
        {
            this.Title = title;
            this.Summary = summary;
        }
        public void SetCreatedBy(string createdBy)
        {
            CreatedBy = createdBy;
        }
    }
}