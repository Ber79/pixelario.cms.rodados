﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ChangeImageCommand : ActionCommand, IRequest<ArticleResponseCommand>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public ImageType ImageType { get; set; }
        public string FileName { get; set; }
        public string AppDomainAppPath { get; set; }
        public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public ChangeImageCommand()
        {

        }
        public ChangeImageCommand(
            int iDArticle,
            ImageType imageType, 
            string appDomainAppPath, string fileName)
        {
            this.IDArticle = iDArticle;
            this.ImageType = imageType;
            this.FileName = fileName;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}