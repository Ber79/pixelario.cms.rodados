﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ChangeHomeOrderCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public int Order { get; set; }
        public ChangeHomeOrderCommand()
        {

        }
        public ChangeHomeOrderCommand(int iDArticle,
            int order)
        {
            IDArticle = iDArticle;
            Order = order;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}