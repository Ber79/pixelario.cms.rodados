﻿using MediatR;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Domain.Commons;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class ListScriptItemsCommand : IRequest<List<ArticleScriptItemQueryModel>>
    {
        private string DefaultColumnOrder = "asi.IDArticleScriptItem";
        private string DefaultOrderDirection = "DESC";
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int IDArticleScript { get; private set; }
        public MultimediaType MultimediaType { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public string ColumnOrder { get; private set; }
        public string OrderDirection { get; private set; }
        public ListScriptItemsCommand(int iDArticleScript, MultimediaType multimediaType, 
            string filteredBy,
            bool? enabled, 
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection)
        {
            IDArticleScript = iDArticleScript;
            MultimediaType = multimediaType;
            FilteredBy = filteredBy;
            Enabled = enabled;
            RowIndex = rowIndex;
            RowCount = rowCount;
            ColumnOrder = columnOrder;
            OrderDirection = orderDirection;
        }
        public ListScriptItemsCommand(int iDArticleScript, int rowIndex, int rowCount)
        {
            this.IDArticleScript = iDArticleScript;
            this.MultimediaType = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }
        public ListScriptItemsCommand(int iDArticleScript)
        {
            this.IDArticleScript = iDArticleScript;
            this.MultimediaType = null;
            this.FilteredBy = null;
            this.Enabled = null;
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
            this.ColumnOrder = this.DefaultColumnOrder;
            this.OrderDirection = this.DefaultOrderDirection;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDArticleScript;
            hash = hash * 23 + (this.MultimediaType == null ? 0 :
                this.MultimediaType.Id);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            hash = hash * 23 + this.RowIndex;
            hash = hash * 23 + this.RowCount;
            hash = hash * 23 + (string.IsNullOrEmpty(this.ColumnOrder) ? 0 :
                this.ColumnOrder.GetHashCode());
            hash = hash * 23 + (string.IsNullOrEmpty(this.OrderDirection) ? 0 :
                this.OrderDirection.GetHashCode());
            return hash;
        }
    }
}
