﻿using MediatR;
using Pixelario.CMS.Domain.Commons;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class CropImageCommand : ActionCommand, IRequest<ArticleResponseCommand>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public string Action { get; set; }

        public ImageType ImageType
        {
            get
            {
                if (!string.IsNullOrEmpty(Action))
                {
                    switch(Action)
                    {
                        case "CropVerticalImage":
                            return ImageType.Vertical;
                        case "CropHorizontalImage":
                            return ImageType.Horizontal;
                        case "CropSlideImage":
                        default:
                            return ImageType.Slide;
                    }
                }
                return ImageType.Slide;
            }
        }
        public string XPoint { get; set; }
        public string YPoint { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string NaturalWidth { get; set; }
        public string FileName { get; set; }
        public string AppDomainAppPath { get; set; }
        public SearchAndPagingOptionsToReturn SearchAndPagingOptions { get; set; }

        public CropImageCommand()
        {

        }
        public CropImageCommand(
            int iDArticle,
            string action,
            string xPoint, string yPoint,
            string width, string height,
            string naturalWidth, string fileName,
            string appDomainAppPath)
        {
            this.IDArticle = iDArticle;
            this.Action = action;
            this.XPoint = xPoint;
            this.YPoint = yPoint;
            this.NaturalWidth = naturalWidth;
            this.FileName = fileName;
            this.AppDomainAppPath = appDomainAppPath;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}