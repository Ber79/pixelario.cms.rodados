﻿using MediatR;
using Pixelario.CMS.SeedWork;
namespace Pixelario.CMS.Application.Commands.Articles
{
    public class EnableScriptCommand : ActionCommand, IRequest<CommandResponse>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticleScript { get; set; }

        public EnableScriptCommand()
        {

        }
        public EnableScriptCommand(int iDArticleScript)
        {
            IDArticleScript = iDArticleScript;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}