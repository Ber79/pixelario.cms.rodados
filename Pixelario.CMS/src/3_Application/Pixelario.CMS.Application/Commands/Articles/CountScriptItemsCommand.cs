﻿using MediatR;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class CountScriptItemsCommand : IRequest<int>
    {
        public int IDArticleScript { get; private set; }
        public MultimediaType MultimediaType { get; set; }
        public string FilteredBy { get; private set; }
        public bool? Enabled { get; private set; }

        public CountScriptItemsCommand(int iDArticleScript, 
            MultimediaType multimediaType, string filteredBy,
            bool? enabled)
        {
            this.IDArticleScript = iDArticleScript;
            this.MultimediaType = multimediaType;
            this.FilteredBy = filteredBy;
            this.Enabled = enabled;
        }
        public CountScriptItemsCommand(int iDArticleScript)
        {
            this.IDArticleScript = iDArticleScript;
            this.MultimediaType = null;
            this.FilteredBy = null;
            this.Enabled = null;
        }
        
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.IDArticleScript;
            hash = hash * 23 + (this.MultimediaType == null ? 0 :
                this.MultimediaType.Id);
            hash = hash * 23 + (string.IsNullOrEmpty(this.FilteredBy) ? 0 :
                this.FilteredBy.GetHashCode());
            hash = hash * 23 + (!this.Enabled.HasValue ? 0 :
                this.Enabled.GetHashCode());
            return hash;
        }
    }
}
