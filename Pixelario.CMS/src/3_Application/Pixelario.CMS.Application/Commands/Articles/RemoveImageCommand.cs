﻿using MediatR;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.Commands.Articles
{
    public class RemoveImageCommand : ActionCommand, IRequest<ArticleResponseCommand>
    {
        public string SolicitedBy { get; private set; }
        public int IDArticle { get; set; }
        public ImageType ImageType { get; set; }

        public RemoveImageCommand()
        {

        }
        public RemoveImageCommand(
            int iDArticle,
            ImageType imageType)
        {
            this.IDArticle = iDArticle;
            this.ImageType = imageType;
        }
        public void SetSolicitedBy(string solicitedBy)
        {
            SolicitedBy = solicitedBy;
        }
    }
}