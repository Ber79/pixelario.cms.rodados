﻿using MediatR;

namespace Pixelario.CMS.Application.Commands.CacheRegisters
{
    public class CountCommand : IRequest<int>
    {
        public CountCommand()
        {
        }
    }
}
