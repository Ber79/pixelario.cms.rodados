﻿using MediatR;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.Commands.CacheRegisters
{
    public class ListCommand : IRequest<List<KeyValuePair<string, object>>>
    {
        private int DefaultRowCount = 10;
        private int DefaultRowIndex = 0;
        public int RowIndex { get; private set; }
        public int RowCount { get; private set; }
        public ListCommand( int rowIndex,
            int rowCount)
        {
            this.RowIndex = rowIndex;
            this.RowCount = rowCount;
        }
        public ListCommand()
        {
            this.RowIndex = this.DefaultRowIndex;
            this.RowCount = this.DefaultRowCount;
        }
    }
}
