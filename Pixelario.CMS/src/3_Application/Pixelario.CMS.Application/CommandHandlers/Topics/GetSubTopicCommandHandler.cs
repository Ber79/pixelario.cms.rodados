﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class GetSubTopicCommandHandler : IRequestHandler<GetSubTopicCommand, SubTopicQueryModel>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ISubTopicsQueries _subTopicsQueries;
        public GetSubTopicCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ISubTopicsQueries subTopicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _subTopicsQueries = subTopicsQueries ?? throw new ArgumentNullException(nameof(subTopicsQueries));
        }
        public async Task<SubTopicQueryModel> Handle(GetSubTopicCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _subTopicCache.GetItem(
              keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _subTopicsQueries.GetAsync(
                iDSubTopic: command.IDSubTopic);
            if (item != null)
            {
                _subTopicCache.SetItem(
                   value: item,
                    keyAddition: command.GetHashCode().ToString());

            }
            return item;
        }
    }
}