﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class ChangeSubTopicOrderCommandHandler : IRequestHandler<ChangeSubTopicOrderCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ITopicRepository _topicRepository;
        public ChangeSubTopicOrderCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task<CommandResponse> Handle(
            ChangeSubTopicOrderCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change home order value on subtopic with id {1}",
                    command.SolicitedBy,
                    command.IDSubTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    SubTopic subTema = null;
                    if (topic.HasSubTopic(
                        iDSubTopic: command.IDSubTopic,
                        subTopic: out subTema))
                    {
                        topic.ChangeSubTopicOrder(
                            iDSubTopic: command.IDSubTopic,
                            order: command.Order,
                            orderPlace: OrderPlace.AtHome);
                        _topicRepository.Update(topic);
                        var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Subtopic with id {0} has change home order value",
                                command.IDSubTopic);
                            response.success = true;
                            response.errors = null;
                            _subTopicCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Subtopic with id {0} not updated",
                                command.IDSubTopic);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Subtopic with id {0} not founded",
                            command.IDSubTopic);
                        response.success = false;
                        response.errors = new List<string>() { "No se pudo cambía el estado del sub tema." };
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el tema buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}