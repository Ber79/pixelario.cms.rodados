﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class ListSubTopicsCommandHandler : IRequestHandler<ListSubTopicsCommand, List<SubTopicQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ISubTopicsQueries _subTopicsQueries;
        public ListSubTopicsCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ISubTopicsQueries subTopicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _subTopicsQueries = subTopicsQueries ?? throw new ArgumentNullException(nameof(subTopicsQueries));
        }
        public async Task<List<SubTopicQueryModel>> Handle(ListSubTopicsCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _subTopicCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _subTopicsQueries.ListAsync(
                iDTopic: command.IDTopic,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _subTopicCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}