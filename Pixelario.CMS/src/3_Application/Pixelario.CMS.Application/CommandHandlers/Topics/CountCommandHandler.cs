﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicsQueries _topicsQueries;
        public CountCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicsQueries topicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicsQueries = topicsQueries ?? throw new ArgumentNullException(nameof(topicsQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _topicCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _topicsQueries.CountAsync(
                iDEdition: command.IDEdition,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu);
            _topicCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}