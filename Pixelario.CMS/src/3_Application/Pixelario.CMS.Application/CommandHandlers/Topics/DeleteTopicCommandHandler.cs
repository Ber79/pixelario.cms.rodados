﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class DeleteTopicCommandHandler : IRequestHandler<DeleteTopicCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicRepository _topicRepository;
        public DeleteTopicCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task<CommandResponse> Handle(DeleteTopicCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a topic with id {1}",
                    command.SolicitedBy,
                    command.IDTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    if (!topic.Enabled)
                    {
                        _topicRepository.Delete(topic);
                        var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Topic with id {0} has been deleted",
                                command.IDTopic);
                            response.success = true;
                            response.errors = null;
                            _topicCache.Clear();

                        }
                        else
                        {
                            _logger.Warning("Topic with id {0} has not been deleted",
                                command.IDTopic);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Topic with id {0} has not been deleted",
                            command.IDTopic);
                        response.errors = new List<string>() { "Debe deshabilitar el topic antes de borrarlo." };
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el topic buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}