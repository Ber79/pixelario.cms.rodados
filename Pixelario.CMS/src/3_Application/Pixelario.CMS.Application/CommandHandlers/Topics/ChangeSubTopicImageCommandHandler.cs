﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class ChangeSubTopicImageCommandHandler :
        IRequestHandler<ChangeSubTopicImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ITopicRepository _topicRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 400, 800 };
        private int DefaultSize = 80;
        public ChangeSubTopicImageCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ITopicRepository topicRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangeSubTopicImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change image type {1} on subtopic with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    SubTopic subTopic = null;
                    if (topic.HasSubTopic(
                        iDSubTopic: command.IDSubTopic,
                        subTopic: out subTopic))
                    {

                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\subtopics\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\subtopics\\raw\\{0}",
                                        command.FileName));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\subtopics\\{0}\\{1}",
                                        width.ToString(),
                                        command.FileName));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        topic.ChangeSubTopicImage(
                            iDSubTopic: command.IDSubTopic,
                            imageType: command.ImageType,
                            uri: string.Format(
                                "/uploads/subtopics/{0}/{1}",
                                    DefaultSize.ToString(),
                                    command.FileName));
                        _topicRepository.Update(topic);
                        var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Subtopic with id {0} has change his image",
                                command.IDSubTopic);
                            response.success = true;
                            response.errors = null;
                            _subTopicCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Subtopic with id {0} not updated",
                                command.IDSubTopic);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("SubTopic with id {0} not founded",
                            command.IDSubTopic);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el sub tema buscado." };
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el tema buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}