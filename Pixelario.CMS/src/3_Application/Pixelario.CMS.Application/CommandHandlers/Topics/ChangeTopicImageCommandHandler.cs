﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class ChangeTopicImageCommandHandler : IRequestHandler<ChangeTopicImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicRepository _topicRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 400, 800 };
        private int DefaultSize = 80;
        public ChangeTopicImageCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicRepository topicRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangeTopicImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change image type {1} on topic with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    foreach (var width in DefaultImagesSize)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\topics\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\topics\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\topics\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }
                    topic.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/topics/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName));
                    _topicRepository.Update(topic);
                    var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Topic with id {0} has change his image",
                            command.IDTopic);
                        response.success = true;
                        response.errors = null;
                        _topicCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Topic with id {0} not updated",
                            command.IDTopic);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el topic buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}