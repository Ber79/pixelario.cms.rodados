﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class AddTopicCommandHandler : IRequestHandler<AddTopicCommand, AddTopicCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicRepository _topicRepository;
        private readonly IEditionRepository _editionRepository;
        public AddTopicCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicRepository topicRepository,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<AddTopicCommandResponse> Handle(AddTopicCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddTopicCommandResponse();
            try
            {
                _logger.Information("{0} starting to add a topic titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var topic = new Topic(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    summary: command.Summary,
                    keywords: command.Keywords);
                if (command.Editions.Any())
                {
                    foreach (var id in command.Editions)
                    {
                        var edition = await _editionRepository.GetAsync(
                            iDEdition: id);
                        if (edition != null)
                        {
                            topic.BindEdition(
                                edition: edition);
                        }
                    }
                }
                _topicRepository.Add(topic);
                var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Topic with title \"{0}\" saved with id {1}",
                        command.Title, topic.ID);
                    response.IDTopic = topic.ID;
                    response.success = true;
                    response.errors = null;
                    _topicCache.Clear();
                }
                else
                {
                    _logger.Warning("Topic {0} not saved",
                               command.Title);
                    response.IDTopic = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDTopic = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}