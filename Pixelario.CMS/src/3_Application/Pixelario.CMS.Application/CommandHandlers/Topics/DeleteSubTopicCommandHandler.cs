﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class DeleteSubTopicCommandHandler : IRequestHandler<DeleteSubTopicCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ITopicRepository _topicRepository;
        public DeleteSubTopicCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task<CommandResponse> Handle(DeleteSubTopicCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a subtopic with id {1}",
                    command.SolicitedBy,
                    command.IDSubTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {

                    SubTopic subTopic = null;
                    if (topic.HasSubTopic(
                        iDSubTopic: command.IDSubTopic,
                        subTopic: out subTopic))
                    {
                        if (!subTopic.Enabled)
                        {
                            topic.DeleteSubTopic(subTopic);
                            _topicRepository.Delete(subTopic);
                            var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                            if (saved.Success)
                            {
                                _logger.Information("Subtopic with id {0} has been deleted",
                                    command.IDSubTopic);
                                response.success = true;
                                response.errors = null;
                                _subTopicCache.Clear();
                            }
                            else
                            {
                                _logger.Warning("Subtopic with id {0} has not been deleted",
                                    command.IDSubTopic);
                                response.success = false;
                                response.errors = saved.Errors as List<string>;
                            }
                        }
                        else
                        {
                            _logger.Warning("Subtopic with id {0} has not been deleted",
                                command.IDSubTopic);
                            response.success = false;
                            response.errors = new List<string>() { "Debe deshabilitar el sub topic antes de borrarlo." };
                        }
                    }
                    else
                    {
                        _logger.Warning("Subtopic with id {0} not founded",
                            command.IDSubTopic);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el sub topic que desea borrar." };
                    }

                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el topic buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}