﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class AddSubTopicCommandHandler : IRequestHandler<AddSubTopicCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ITopicRepository _topicRepository;
        public AddSubTopicCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task<CommandResponse> Handle(AddSubTopicCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add a subtopic titled \"{1}\"",
                    command.SolicitedBy,
                    command.Title);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    topic.AddSubTopic(
                        solicitedBy: command.SolicitedBy,
                        title: command.Title,
                        summary: command.Summary,
                        keywords: command.Keywords
                        );
                    _topicRepository.Update(topic);
                    var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Subtopic with title \"{0}\" saved on topic with id {1}",
                            command.Title, topic.ID);
                        response.success = true;
                        response.errors = null;
                        _subTopicCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Subtopic {0} not saved",
                               command.Title);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not found",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se pudo encontrar el tema." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}