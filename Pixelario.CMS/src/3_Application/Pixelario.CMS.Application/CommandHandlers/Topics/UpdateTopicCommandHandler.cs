﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class UpdateTopicCommandHandler : IRequestHandler<UpdateTopicCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicRepository _topicRepository;
        private readonly IEditionRepository _editionRepository;
        public UpdateTopicCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicRepository topicRepository,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<CommandResponse> Handle(UpdateTopicCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to update a topic with id {1}",
                    command.SolicitedBy,
                    command.IDTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    topic.Update(
                        title: command.Title,
                        summary: command.Summary,
                        keywords: command.Keywords);
                    if (command.Editions.Any())
                    {
                        var editionsToRemove = topic.Editions.Select(e => e.ID)
                            .Where(e => !command.Editions.Contains(e)).ToList();
                        foreach (var id in editionsToRemove)
                        {
                            topic.UnbindEdition(
                                edition: topic.Editions.First(
                                    e => e.ID == id)
                            );
                        }
                        var editionsToAdd = command.Editions.Where(
                            e => topic.Editions.Where(et => et.ID == e).Count() == 0)
                            .ToList();
                        foreach (var id in editionsToAdd)
                        {
                            var edition = await _editionRepository.GetAsync(
                                iDEdition: id);
                            if (edition != null)
                            {
                                topic.BindEdition(
                                    edition: edition);
                            }
                        }

                    }
                    else if (topic.Editions.Any())
                    {
                        foreach (var edition in topic.Editions)
                            topic.UnbindEdition(
                                    edition: edition
                                );
                    }
                    _topicRepository.Update(topic);
                    var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Topic with id {0} updated",
                            topic.ID);
                        response.success = true;
                        response.errors = null;
                        _topicCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Topic with id {0} not updated",
                                command.IDTopic); 
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el tema buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}