﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class GetCommandHandler : IRequestHandler<GetCommand, TopicQueryModel>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicsQueries _topicsQueries;
        public GetCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicsQueries topicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicsQueries = topicsQueries ?? throw new ArgumentNullException(nameof(topicsQueries));
        }
        public async Task<TopicQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _topicCache.GetItem(
               keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _topicsQueries.GetAsync(
                iDTopic: command.IDTopic);
            if (item != null)
            {
                _topicCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}