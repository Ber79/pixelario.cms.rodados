﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Topics;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Topics;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class OrderAtMenuCommandHandler : IRequestHandler<OrderAtMenuCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicRepository _topicRepository;
        public OrderAtMenuCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task<CommandResponse> Handle(OrderAtMenuCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change menu order value on topic with id {1}",
                    command.SolicitedBy,
                    command.IDTopic);
                var topic = await _topicRepository.GetAsync(
                    iDTopic: command.IDTopic);
                if (topic != null)
                {
                    topic.ChangeOrder(
                        order: command.Order,
                        orderPlace: OrderPlace.AtMenu);
                    _topicRepository.Update(topic);
                    var saved = await _topicRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Topic with id {0} has change menu order value",
                            command.IDTopic);
                        response.success = true;
                        response.errors = null;
                        _topicCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Topic with id {0} not updated",
                            command.IDTopic);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Topic with id {0} not founded",
                        command.IDTopic);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el tema buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}