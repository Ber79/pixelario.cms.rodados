﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class CountSubTopicsCommandHandler : IRequestHandler<CountSubTopicsCommand, int>
    {
        private readonly ILogger _logger;
        private readonly ISubTopicCache _subTopicCache;
        private readonly ISubTopicsQueries _subTopicsQueries;
        public CountSubTopicsCommandHandler(
            ILogger logger,
            ISubTopicCache subTopicCache,
            ISubTopicsQueries subTopicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subTopicCache = subTopicCache ?? throw new ArgumentNullException(nameof(subTopicCache));
            _subTopicsQueries = subTopicsQueries ?? throw new ArgumentNullException(nameof(subTopicsQueries));
        }
        public async Task<int> Handle(CountSubTopicsCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _subTopicCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _subTopicsQueries.CountAsync(
                iDTopic: command.IDTopic,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu);
            _subTopicCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;

        }
    }
}