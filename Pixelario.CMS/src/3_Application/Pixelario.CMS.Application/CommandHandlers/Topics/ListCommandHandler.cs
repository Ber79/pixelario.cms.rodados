﻿using MediatR;
using Pixelario.CMS.Application.Cache.Topics;
using Pixelario.CMS.Application.Commands.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Topics
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<TopicQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly ITopicCache _topicCache;
        private readonly ITopicsQueries _topicsQueries;
        public ListCommandHandler(
            ILogger logger,
            ITopicCache topicCache,
            ITopicsQueries topicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _topicCache = topicCache ?? throw new ArgumentNullException(nameof(topicCache));
            _topicsQueries = topicsQueries ?? throw new ArgumentNullException(nameof(topicsQueries));
        }
        public async Task<List<TopicQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _topicCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _topicsQueries.ListAsync(
                iDEdition: command.IDEdition,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu,
                includeEnableSubTopics: command.IncludeEnableSubTopics,
                includeHomePageSubTopics: command.IncludeHomePageSubTopics,
                includeOnMenuSubTopics: command.IncludeOnMenuSubTopics,
                subTopicColumnOrder: command.SubTopicColumnOrder,
                subTopicOrderDirection: command.SubTopicOrderDirection,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if(list !=null)
            {
                _topicCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}