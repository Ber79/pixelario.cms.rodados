﻿using MediatR;
using Pixelario.CMS.Application.Commands.CacheRegisters;
using Pixelario.CMS.Application.Queries.CacheRegisters;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.CacheRegisters
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<KeyValuePair<string, object>>>
    {
        private readonly ILogger _logger;
        private readonly ICacheRegisterQueries _cacheRegisterQueries;
        public ListCommandHandler(
            ILogger logger,
            ICacheRegisterQueries cacheRegisterQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cacheRegisterQueries = cacheRegisterQueries ?? throw new ArgumentNullException(nameof(cacheRegisterQueries));
        }
        public async Task<List<KeyValuePair<string, object>>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            return await Task.FromResult(_cacheRegisterQueries.List(
                rowIndex: command.RowIndex,
                rowCount: command.RowCount));
        }
    }
}