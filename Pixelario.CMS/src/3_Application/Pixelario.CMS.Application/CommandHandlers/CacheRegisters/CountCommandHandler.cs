﻿using MediatR;
using Pixelario.CMS.Application.Commands.CacheRegisters;
using Pixelario.CMS.Application.Queries.CacheRegisters;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.CacheRegisters
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly ICacheRegisterQueries _cacheRegisterQueries;
        public CountCommandHandler(
            ILogger logger,
            ICacheRegisterQueries cacheRegisterQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cacheRegisterQueries = cacheRegisterQueries ?? throw new ArgumentNullException(nameof(cacheRegisterQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            return await Task.FromResult(_cacheRegisterQueries.Count());
        }
    }
}