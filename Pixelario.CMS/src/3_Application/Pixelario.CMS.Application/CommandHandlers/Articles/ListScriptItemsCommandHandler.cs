﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ListScriptItemsCommandHandler : IRequestHandler<ListScriptItemsCommand, List<ArticleScriptItemQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptItemCache _articleScriptItemCache;
        private readonly IArticleScriptItemsQueries _articleScriptItemsQueries;
        public ListScriptItemsCommandHandler(
            ILogger logger,
            IArticleScriptItemCache articleScriptItemCache,
            IArticleScriptItemsQueries articleScriptItemsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptItemCache = articleScriptItemCache ?? throw new ArgumentNullException(nameof(articleScriptItemCache));
            _articleScriptItemsQueries = articleScriptItemsQueries ?? throw new ArgumentNullException(nameof(articleScriptItemsQueries));
        }
        public async Task<List<ArticleScriptItemQueryModel>> Handle(ListScriptItemsCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _articleScriptItemCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            int? iDMultimediaType = null;
            if (command.MultimediaType != null)
                iDMultimediaType = command.MultimediaType.Id;
            var list = await _articleScriptItemsQueries.ListAsync(
                iDArticleScript: command.IDArticleScript,
                iDMultimediaType: iDMultimediaType,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _articleScriptItemCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}