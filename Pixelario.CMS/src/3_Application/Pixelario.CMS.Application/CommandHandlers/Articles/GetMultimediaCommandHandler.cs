﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class GetMultimediaCommandHandler : IRequestHandler<GetMultimediaCommand, ArticleMultimediaQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IArticleMultimediaCache _articleMultimediaCache;
        private readonly IArticleMultimediasQueries _articleMultimediaQueries;
        public GetMultimediaCommandHandler(
            ILogger logger,
            IArticleMultimediaCache articleMultimediaCache,
            IArticleMultimediasQueries articleMultimediaQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleMultimediaCache = articleMultimediaCache ?? throw new ArgumentNullException(nameof(articleMultimediaCache));
            _articleMultimediaQueries = articleMultimediaQueries ?? throw new ArgumentNullException(nameof(articleMultimediaQueries));
        }
        public async Task<ArticleMultimediaQueryModel> Handle(GetMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _articleMultimediaCache.GetItem(
              keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _articleMultimediaQueries.GetAsync(
                iDMultimedia: command.IDMultimedia);
            if (item != null)
            {
                _articleMultimediaCache.SetItem(
                   value: item,
                   keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}