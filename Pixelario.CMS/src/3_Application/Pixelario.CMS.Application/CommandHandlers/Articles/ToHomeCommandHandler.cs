﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ToHomeCommandHandler : IRequestHandler<ToHomeCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        private readonly IArticlesQueries _articlesQueries;
        public ToHomeCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository,
            IArticlesQueries articlesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _articlesQueries = articlesQueries ?? throw new ArgumentNullException(nameof(articlesQueries));
        }
        public async Task<CommandResponse> Handle(ToHomeCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change ToHome value on article with id {1}",
                    command.SolicitedBy,
                    command.IDArticle);
                var article = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (article != null)
                {
                    if (!article.AtHome &&
                        article.Edition.EditionType == EditionType.Especial_Content.Id)
                    {
                        var totalArticlesAtHome = await _articlesQueries.CountAsync(
                            iDEdition: article.IDEdition,
                            iDTopic: null,
                            iDSubTopic: null,
                            enabled: true,
                            atHome: true,
                            placesAtHome: null,
                            filteredBy: null
                            );
                        if (totalArticlesAtHome >= 3)
                        {
                            throw new ArticleException("No puede tener mas de 3 árticulos en la portada.");
                        }
                    }
                    article.ChangeAtHome(
                        atHome: !article.AtHome);
                    _articleRepository.Update(article);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} has change ToHome value",
                            command.IDArticle);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}