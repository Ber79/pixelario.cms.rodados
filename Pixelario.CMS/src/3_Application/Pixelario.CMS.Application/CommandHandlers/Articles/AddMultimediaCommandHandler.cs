﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class AddMultimediaCommandHandler :
        IRequestHandler<AddMultimediaCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleMultimediaCache _articleMultimediaCache;
        private readonly IArticleRepository _articleRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 450, 800 };
        private int DefaultSize = 80;
        public AddMultimediaCommandHandler(
            ILogger logger,
            IArticleMultimediaCache articleMultimediaCache,
            IArticleRepository articleRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleMultimediaCache = articleMultimediaCache ?? throw new ArgumentNullException(nameof(articleMultimediaCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(AddMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add an article multimedia titled \"{1}\"",
                    command.SolicitedBy,
                    command.Title);
                var article = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (article != null)
                {
                    string path1 = null;
                    string path2 = null;
                    string summary = null;
                    if (command.MultimediaType == MultimediaType.Imagen)
                    {
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\articles\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/articles/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                    }
                    else if (command.MultimediaType == MultimediaType.Documento)
                    {
                        path1 = string.Format("/uploads/articles/documents/{0}",
                            command.Path1);
                        path2 = command.Path2;
                    }
                    else if (command.MultimediaType == MultimediaType.Widget)
                    {
                        summary = command.Summary;
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\articles\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/articles/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                    }
                    else if (command.MultimediaType == MultimediaType.Link)
                    {
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\articles\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\articles\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/articles/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                        path2 = command.Path2;
                    }
                    // Todo: Log
                    article.AddMultimedia(
                        createdBy: command.SolicitedBy,
                        title: command.Title,
                        summary: summary,
                        path1: path1,
                        path2: path2,
                        multimediaType: command.MultimediaType);
                    _articleRepository.Update(article);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article multimedia with title \"{0}\" saved on article with id {1}",
                            command.Title, article.ID);
                        response.success = true;
                        response.errors = null;
                        _articleMultimediaCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article multimedia {0} not saved",
                                command.Title);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not found",
                        command.IDArticle);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}