﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class TransferToEditionCommandHandler : IRequestHandler<TransferToEditionCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articuloRepository;
        public TransferToEditionCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articuloRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articuloRepository = articuloRepository ?? throw new ArgumentNullException(nameof(articuloRepository));
        }
        public async Task<CommandResponse> Handle(TransferToEditionCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to tranfer article with id {1} to edition with id {2}",
                    command.SolicitedBy,
                    command.IDArticle,
                    command.IDEdition);
                var article = await _articuloRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (article != null)
                {
                    article.TransferToEdition(
                        iDEdition: command.IDEdition);
                    _articuloRepository.Update(article);
                    var saved = await _articuloRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} has been transfer to edition with id {1}",
                            command.IDArticle, command.IDEdition);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}