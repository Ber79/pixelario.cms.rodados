﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class GetScriptCommandHandler : IRequestHandler<GetScriptCommand, ArticleScriptQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptsQueries _articleScriptsQueries;
        public GetScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptsQueries articleScriptsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptsQueries = articleScriptsQueries ?? throw new ArgumentNullException(nameof(articleScriptsQueries));
        }
        public async Task<ArticleScriptQueryModel> Handle(GetScriptCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _articleScriptCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _articleScriptsQueries.GetAsync(
                iDArticleScript: command.IDArticleScript);
            if (item != null)
            {
                _articleScriptCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}