﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class DeleteScriptItemCommandHandler : IRequestHandler<DeleteScriptItemCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptItemCache _articleScriptItemCache;
        private readonly IArticleScriptRepository _articleScriptRepository;
        public DeleteScriptItemCommandHandler(
            ILogger logger,
            IArticleScriptItemCache articleScriptItemCache,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptItemCache = articleScriptItemCache ?? throw new ArgumentNullException(nameof(articleScriptItemCache));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(DeleteScriptItemCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a article script item with id {1}",
                    command.SolicitedBy,
                    command.IDArticleScriptItem);
                var articleScript = await _articleScriptRepository.GetAsync(
                    iDArticleScript: command.IDArticleScript);
                if (articleScript != null)
                {
                    ArticleScriptItem articleScriptItem = null;
                    if (articleScript.HasItem(
                        iDArticleScriptItem: command.IDArticleScriptItem,
                        scriptItem: out articleScriptItem))
                    {
                        if (!articleScriptItem.Enabled)
                        {
                            articleScript.DeleteItem(articleScriptItem);
                            _articleScriptRepository.Delete(articleScriptItem);
                            var saved = await _articleScriptRepository.UnitOfWork.SaveEntitiesAsync();
                            if (saved.Success)
                            {
                                _logger.Information("Article script item with id {0} has been deleted",
                                    command.IDArticleScriptItem);
                                response.success = true;
                                response.errors = null;
                                _articleScriptItemCache.Clear();
                            }
                            else
                            {
                                _logger.Warning("Article script item with id {0} has not been deleted",
                                    command.IDArticleScriptItem);
                                response.success = false;
                                response.errors = saved.Errors as List<string>;
                            }
                        }
                        else
                        {
                            _logger.Warning("Article script item with id {0} has not been deleted",
                                command.IDArticleScriptItem);
                            response.success = false;
                            response.errors = new List<string>() { "Debe deshabilitar el ítem antes de borrarlo." };
                        }
                    }
                    else
                    {
                        _logger.Warning("Article script item with id {0} not founded",
                            command.IDArticleScriptItem);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el ítem que desea borrar." };
                    }

                }
                else
                {
                    _logger.Warning("Article script with id {0} not founded",
                        command.IDArticleScript);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el script de artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}