﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ListMultimediaCommandHandler : IRequestHandler<ListMultimediaCommand, List<ArticleMultimediaQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IArticleMultimediaCache _articleMultimediaCache;
        private readonly IArticleMultimediasQueries _articleMultimediaQueries;
        public ListMultimediaCommandHandler(
            ILogger logger,
            IArticleMultimediaCache articleMultimediaCache,
            IArticleMultimediasQueries articleMultimediaQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleMultimediaCache = articleMultimediaCache ?? throw new ArgumentNullException(nameof(articleMultimediaCache));
            _articleMultimediaQueries = articleMultimediaQueries ?? throw new ArgumentNullException(nameof(articleMultimediaQueries));
        }
        public async Task<List<ArticleMultimediaQueryModel>> Handle(ListMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _articleMultimediaCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var iDMultimediaTypes = command.MultimediaTypes.Where(mt=> mt != null).Select(mt => mt.Id).ToArray();
            var list = await _articleMultimediaQueries.ListAsync(
                iDArticle: command.IDArticle,
                iDMultimediaTypes: iDMultimediaTypes,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {

                _articleMultimediaCache.SetList(
                value: list,
                keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}