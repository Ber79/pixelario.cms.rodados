﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        private readonly ITopicRepository _topicRepository;
        private readonly IArticleScriptRepository _articleScriptRepository;
        public UpdateCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository,
            ITopicRepository topicRepository,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(UpdateCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to update an article with id {1}",
                    command.SolicitedBy,
                    command.IDArticle);
                var article = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (article != null)
                {
                    // TODO: Logs
                    article.Update(
                        publicationDate: command.PublicationDate,
                        title: command.Title,
                        summary: command.Summary,
                        body: command.Body,
                        source: command.Source,
                        keywords: command.Keywords);
                    if (command.Topics.Any())
                    {
                        var topicsToRemove = article.Topics.Select(t => t.ID)
                            .Where(t => !command.Topics.Contains(t)).ToList();
                        foreach (var id in topicsToRemove)
                        {
                            article.UnbindTopic(
                                topic: article.Topics.First(
                                    t => t.ID == id)
                            );
                        }
                        var topicsToAdd = command.Topics.Where(
                            t => article.Topics.Where(at => at.ID == t).Count() == 0)
                            .ToList();
                        foreach (var id in topicsToAdd)
                        {
                            var topic = await _topicRepository.GetAsync(
                                iDTopic: id);
                            if (topic != null)
                            {
                                article.BindTopic(
                                    topic: topic);
                            }
                        }
                    }
                    else if (article.Topics.Any())
                    {
                        var topicsToRemove = article.Topics.ToList();
                        foreach (var topic in topicsToRemove)
                            article.UnbindTopic(
                                    topic: topic);
                    }

                    if (command.SubTopics.Any())
                    {
                        var subTopicsToRemove = article.SubTopics.Select(s => s.ID)
                            .Where(s => !command.SubTopics.Contains(s)).ToList();
                        foreach (var id in subTopicsToRemove)
                        {
                            article.UnbindSubTopic(
                                subTopic: article.SubTopics.First(
                                    s => s.ID == id)
                            );
                        }
                        var subTopicsToAdd = command.SubTopics.Where(
                            s => article.SubTopics.Where(ast => ast.ID == s).Count() == 0)
                            .ToList();
                        foreach (var id in subTopicsToAdd)
                        {
                            var subTopic = await _topicRepository.GetSubTopicAsync(
                                iDSubTopic: id);
                            if (subTopic != null)
                            {
                                article.BindSubTopic(
                                    subTopic: subTopic);
                            }
                        }
                    }
                    else if (article.SubTopics.Any())
                    {
                        var subTopicsToRemove = article.SubTopics.ToList();
                        foreach (var subTopic in subTopicsToRemove)
                            article.UnbindSubTopic(
                                    subTopic: subTopic);
                    }

                    if (command.Scripts.Any())
                    {
                        var scriptsToRemove = article.Scripts.Select(ars => ars.ID)
                            .Where(ars => !command.Scripts.Contains(ars)).ToList();
                        foreach (var id in scriptsToRemove)
                        {
                            article.UnbindScript(
                                script: article.Scripts.First(
                                    ars => ars.ID == id)
                            );
                        }
                        var scriptsToAdd = command.Scripts.Where(
                            i => article.Scripts.Where(aas => aas.ID == i).Count() == 0)
                            .ToList();
                        foreach (var id in scriptsToAdd)
                        {
                            var script = await _articleScriptRepository.GetAsync(
                                iDArticleScript: id);
                            if (script != null)
                            {
                                article.BindScript(
                                    script: script);
                            }
                        }
                    }
                    else if (article.Scripts.Any())
                    {
                        var scriptsToRemove = article.Scripts.ToList();
                        foreach (var script in scriptsToRemove)
                            article.UnbindScript(
                                    script: script);
                    }




                    _articleRepository.Update(article);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} updated",
                            command.IDArticle);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el árticulo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}