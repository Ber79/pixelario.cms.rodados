﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class EnableScriptCommandHandler : IRequestHandler<EnableScriptCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptRepository _articleScriptRepository;
        public EnableScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(EnableScriptCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change enable value on article with id {1}",
                    command.SolicitedBy,
                    command.IDArticleScript);
                var articleScript = await _articleScriptRepository.GetAsync(
                    iDArticleScript: command.IDArticleScript);
                if (articleScript != null)
                {
                    articleScript.ChangeEnabled(
                        enabled: !articleScript.Enabled);
                    _articleScriptRepository.Update(articleScript);
                    var saved = await _articleScriptRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article Script with id {0} has change enable value",
                            command.IDArticleScript);
                        response.success = true;
                        response.errors = null;
                        _articleScriptCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article Script with id {0} not updated",
                            command.IDArticleScript);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article Script with id {0} not founded",
                        command.IDArticleScript);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el script de artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}