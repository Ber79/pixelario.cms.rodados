﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class RemoveImageCommandHandler : IRequestHandler<RemoveImageCommand,
        ArticleResponseCommand>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        public RemoveImageCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
        }
        public async Task<ArticleResponseCommand> Handle(RemoveImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new ArticleResponseCommand();
            try
            {
                _logger.Information("{0} starting to remove image type {1} on article with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDArticle);
                var article = await _articleRepository.GetAsync(
                idArticle: command.IDArticle);
                if (article != null)
                {
                    response.IDEdition = article.IDEdition;
                    article.ChangeImage(
                        imageType: command.ImageType,
                        uri: null);
                    _articleRepository.Update(article);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} has remove his image",
                            command.IDArticle);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.IDEdition = null;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}