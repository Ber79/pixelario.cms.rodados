﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class AddScriptItemCommandHandler : IRequestHandler<AddScriptItemCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptItemCache _articleScriptItemCache;
        private readonly IArticleScriptRepository _articleScriptRepository;
        public AddScriptItemCommandHandler(
            ILogger logger,
            IArticleScriptItemCache articleScriptItemCache,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptItemCache = articleScriptItemCache ?? throw new ArgumentNullException(nameof(articleScriptItemCache));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(AddScriptItemCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add a article script item titled \"{1}\"",
                    command.SolicitedBy,
                    command.Title);
                var articleScript = await _articleScriptRepository.GetAsync(
                    iDArticleScript: command.IDArticleScript);
                if (articleScript != null)
                {
                    articleScript.AddItem(
                        createdBy: command.SolicitedBy,
                        title: command.Title,
                        code: command.Code,
                        multimediaType: MultimediaType.From(
                            id: command.IDMultimediaType)
                        );
                    _articleScriptRepository.Update(articleScript);
                    var saved = await _articleScriptRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article script item with title \"{0}\" saved on article script with id {1}",
                            command.Title, articleScript.ID);
                        response.success = true;
                        response.errors = null;
                        _articleScriptItemCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article script item {0} not saved",
                               command.Title);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article script with id {0} not found",
                        command.IDArticleScript);
                    response.success = false;
                    response.errors = new List<string>() { "No se pudo encontrar el script de artículo." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}