﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        private readonly ITopicRepository _topicRepository;
        private readonly IArticleScriptRepository _articleScriptRepository;
        public AddCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository,
            ITopicRepository topicRepository,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add an article titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var article = new Article(
                    createdBy: command.CreatedBy,
                    iDEdition: command.IDEdition,
                    publicationDate: command.PublicationDate,
                    title: command.Title,
                    summary: command.Summary,
                    body: command.Body,
                    source: command.Source,
                    keywords: command.Keywords) ;
                if (command.Topics.Any())
                {
                    foreach (var iDTopic in command.Topics)
                    {
                        var topic = await _topicRepository.GetAsync(
                            iDTopic: iDTopic);
                        if (topic != null)
                        {
                            // TODO: Logs
                            article.BindTopic(                                
                                topic: topic);
                        }
                    }
                }
                if (command.SubTopics.Any())
                {
                    foreach (var iDSubTopic in command.SubTopics)
                    {
                        var subTopic = await _topicRepository.GetSubTopicAsync(
                            iDSubTopic: iDSubTopic);
                        if (subTopic != null)
                        {
                            article.BindSubTopic(                                
                                subTopic: subTopic);
                        }
                    }
                }
                if (command.Scripts.Any())
                {
                    foreach (var iDArticleScript in command.Scripts)
                    {
                        var script = await _articleScriptRepository.GetAsync(
                            iDArticleScript: iDArticleScript);
                        if (script != null)
                        {
                            article.BindScript(
                                script: script);
                        }
                    }
                }
                _articleRepository.Add(article);
                var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Article with title \"{0}\" saved with id {1}",
                        command.Title, article.ID);
                    response.IDArticle = article.ID;
                    response.success = true;
                    response.errors = null;
                    _articleCache.Clear();
                }
                else
                {
                    _logger.Warning("Article {0} not saved",
                        command.Title);
                    response.IDArticle = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDArticle = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}