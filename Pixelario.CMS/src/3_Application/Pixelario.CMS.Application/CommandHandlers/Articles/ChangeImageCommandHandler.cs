﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ChangeImageCommandHandler : IRequestHandler<ChangeImageCommand,
        ArticleResponseCommand>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImageSizes = { 80, 700, 500 };
        private int[] DefaultVerticalImageSizes = { 80, 425, 800 };
        private int DefaultSize = 80;
        public ChangeImageCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<ArticleResponseCommand> Handle(ChangeImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new ArticleResponseCommand();
            try
            {
                _logger.Information("{0} starting to change image type {1} on article with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDArticle);

                var articulo = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (articulo != null)
                {
                    response.IDEdition = articulo.IDEdition;
                    var imageSizes = DefaultImageSizes;
                    if (command.ImageType == ImageType.Vertical)
                        imageSizes = DefaultVerticalImageSizes;
                    foreach (var width in imageSizes)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\articles\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\articles\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\articles\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }
                    articulo.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/articles/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName));
                    _articleRepository.Update(articulo);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} has change his image",
                            command.IDArticle);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.IDEdition = null;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}