﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class UpdateScriptCommandHandler : IRequestHandler<UpdateScriptCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptRepository _articleScriptRepository;

        public UpdateScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(UpdateScriptCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to update an article script with id {1}",
                    command.SolicitedBy,
                    command.IDArticleScript);
                var articleScript = await _articleScriptRepository.GetAsync(
                    iDArticleScript: command.IDArticleScript);
                if (articleScript != null)
                {
                    // TODO: Logs
                    articleScript.Update(
                        title: command.Title,
                        summary: command.Summary);

                    _articleScriptRepository.Update(articleScript);
                    var saved = await _articleScriptRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article script with id {0} updated",
                            command.IDArticleScript);
                        response.success = true;
                        response.errors = null;
                        _articleScriptCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article script with id {0} not updated",
                            command.IDArticleScript);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article script with id {0} not founded",
                        command.IDArticleScript);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el script del artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}