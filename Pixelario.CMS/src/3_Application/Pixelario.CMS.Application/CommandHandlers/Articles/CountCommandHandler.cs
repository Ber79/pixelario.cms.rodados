﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticlesQueries _articlesQueries;
        public CountCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticlesQueries articlesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articlesQueries = articlesQueries ?? throw new ArgumentNullException(nameof(articlesQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _articleCache.GetCount(
               keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _articlesQueries.CountAsync(
                iDEdition: command.IDEdition,
                iDTopic: command.IDTopic,
                iDSubTopic: command.IDSubTopic,
                placesAtHome: command.PlacesAtHome,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome);
            _articleCache.SetCount(
               value: value,
               keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}