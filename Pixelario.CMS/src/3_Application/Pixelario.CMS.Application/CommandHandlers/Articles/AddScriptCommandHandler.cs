﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.SeedWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class AddScriptCommandHandler : IRequestHandler<AddScriptCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptRepository _articleScriptRepository;
        private readonly ITopicRepository _topicRepository;
        public AddScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptRepository articleScriptRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptRepository = articleScriptRepository ?? throw new ArgumentNullException(nameof(articleScriptRepository));
        }
        public async Task<CommandResponse> Handle(AddScriptCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add an article script titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var articleScript = new ArticleScript(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    summary: command.Summary);
                _articleScriptRepository.Add(articleScript);
                var saved = await _articleScriptRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("ArticleScript with title \"{0}\" saved with id {1}",
                        command.Title, articleScript.ID);
                    response.success = true;
                    response.errors = null;
                    _articleScriptCache.Clear();
                }
                else
                {
                    _logger.Warning("ArticleScript {0} not saved",
                        command.Title);
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}