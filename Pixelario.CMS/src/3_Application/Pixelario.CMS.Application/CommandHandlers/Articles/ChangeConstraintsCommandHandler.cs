﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.SeedWork;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ChangeConstraintsCommandHandler : IRequestHandler<ChangeConstraintsCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IConstraintRepository _constraintsRepository;
        public ChangeConstraintsCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IConstraintRepository constraintsRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _constraintsRepository = constraintsRepository ?? throw new ArgumentNullException(nameof(constraintsRepository));
        }
        public async Task<CommandResponse> Handle(ChangeConstraintsCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change constraints on article with id {1}",
                    command.SolicitedBy,
                    command.IDArticle);
                var constraints = await _constraintsRepository.ListByConstrainedAsync(
                    iDConstraintType: ConstraintType.OnArticle.Id,
                    iDConstrained: command.IDArticle);
                if (constraints != null && constraints.Count > 0)
                {
                    foreach (var constraint in constraints)
                    {
                        if (command.PlansSelected == null ||
                            !command.PlansSelected.Contains(constraint.IDPlan))
                        {
                            _constraintsRepository.Delete(constraint);
                        }
                    }
                }
                else
                {
                    if (command.PlansSelected != null)
                    {
                        foreach (var plan in command.PlansSelected)
                        {
                            if (constraints.Where(c => c.IDPlan == plan).Count() == 0)
                            {
                                _constraintsRepository.Add(new Constraint(
                                    createdBy: command.SolicitedBy,
                                    plan: Plan.From(plan),
                                    constraintType: ConstraintType.OnArticle,
                                    iDConstrained: command.IDArticle));
                            }
                        }
                    }
                }
                var saved = await _constraintsRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Article with id {0} has change enable value",
                        command.IDArticle);
                    response.success = true;
                    response.errors = null;
                    _articleCache.Clear();
                }
                else
                {
                    _logger.Warning("Article with id {0} not updated",
                        command.IDArticle);
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}