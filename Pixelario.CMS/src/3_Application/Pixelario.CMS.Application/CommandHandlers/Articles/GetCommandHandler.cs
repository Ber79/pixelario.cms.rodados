﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class GetCommandHandler : IRequestHandler<GetCommand, ArticleQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticlesQueries _articlesQueries;
        public GetCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticlesQueries articlesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articlesQueries = articlesQueries ?? throw new ArgumentNullException(nameof(articlesQueries));
        }
        public async Task<ArticleQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _articleCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _articlesQueries.GetAsync(
                iDArticle: command.IDArticle);
            if (item != null)
            {
                _articleCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}