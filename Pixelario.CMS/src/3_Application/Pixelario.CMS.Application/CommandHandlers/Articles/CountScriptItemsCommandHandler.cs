﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class CountScriptItemsCommandHandler : IRequestHandler<CountScriptItemsCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptItemCache _articleScriptItemCache;
        private readonly IArticleScriptItemsQueries _articleScriptItemQueries;
        public CountScriptItemsCommandHandler(
            ILogger logger,
            IArticleScriptItemCache articleScriptItemCache,
            IArticleScriptItemsQueries articleScriptItemQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptItemCache = articleScriptItemCache ?? throw new ArgumentNullException(nameof(articleScriptItemCache));
            _articleScriptItemQueries = articleScriptItemQueries ?? throw new ArgumentNullException(nameof(articleScriptItemQueries));
        }
        public async Task<int> Handle(CountScriptItemsCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _articleScriptItemCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            int? iDMultimediaType = null;
            if (command.MultimediaType != null)
                iDMultimediaType = command.MultimediaType.Id;
            var value = await _articleScriptItemQueries.CountAsync(
                iDArticleScript: command.IDArticleScript,
                iDMultimediaType: iDMultimediaType,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled);
            _articleScriptItemCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;

        }
    }
}