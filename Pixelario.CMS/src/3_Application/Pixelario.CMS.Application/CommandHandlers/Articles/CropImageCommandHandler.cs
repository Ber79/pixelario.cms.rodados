﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class CropImageCommandHandler : IRequestHandler<CropImageCommand,
        ArticleResponseCommand>
    {
        private readonly ILogger _logger;
        private readonly IArticleCache _articleCache;
        private readonly IArticleRepository _articleRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImageSizes = { 80, 700, 500 };
        private int[] DefaultVerticalImageSizes = { 80, 425, 800 };
        private int DefaultSize = 80;
        public CropImageCommandHandler(
            ILogger logger,
            IArticleCache articleCache,
            IArticleRepository articleRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleCache = articleCache ?? throw new ArgumentNullException(nameof(articleCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<ArticleResponseCommand> Handle(CropImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new ArticleResponseCommand();
            try
            {
                _logger.Information("{0} starting to change image type {1} on article with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDArticle);

                var articulo = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (articulo != null)
                {
                    var rawFile = Path.Combine(
                           command.AppDomainAppPath,
                           articulo.HomeImage.Replace("/80/", "/raw/").Replace("/", "\\")
                               .Substring(1));
                    var extention = rawFile.Substring(
                        startIndex: rawFile.LastIndexOf("."));
                    response.IDEdition = articulo.IDEdition;
                    var imageSizes = DefaultImageSizes;
                    if (command.ImageType == ImageType.Vertical)
                        imageSizes = DefaultVerticalImageSizes;
                    foreach (var widthSize in imageSizes)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\articles\\{0}", widthSize.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format("uploads\\articles\\{0}\\{1}{2}",
                                widthSize.ToString(), command.FileName,
                                extention));
                        int naturalWidth = 0;
                        if (!string.IsNullOrEmpty(command.NaturalWidth))
                        {
                            int index = command.NaturalWidth.IndexOf(".");
                            if (index > 0)
                            {
                                var intPortion = command.NaturalWidth.Substring(0,
                                    index);
                                int.TryParse(intPortion, out naturalWidth);
                            }
                            else
                            {
                                int.TryParse(command.NaturalWidth, out naturalWidth);
                            }
                        }

                        int xPoint = 0;
                        if (!string.IsNullOrEmpty(command.XPoint))
                        {
                            int index = command.XPoint.IndexOf(".");
                            if (index > 0)
                            {
                                var intPortion = command.XPoint.Substring(0,
                                    index);
                                int.TryParse(intPortion, out xPoint);
                            }
                            else
                            {
                                int.TryParse(command.XPoint, out xPoint);
                            }
                        }
                        int yPoint = 0;
                        if (!string.IsNullOrEmpty(command.YPoint))
                        {
                            int index = command.YPoint.IndexOf(".");
                            if (index > 0)
                            {
                                var intPortion = command.YPoint.Substring(0,
                                    index);
                                int.TryParse(intPortion, out yPoint);
                            }
                            else
                            {
                                int.TryParse(command.YPoint, out yPoint);
                            }
                        }
                        int width = 0;
                        if (!string.IsNullOrEmpty(command.Width))
                        {
                            int index = command.Width.IndexOf(".");
                            if (index > 0)
                            {
                                var intPortion = command.Width.Substring(0,
                                    index);
                                int.TryParse(intPortion, out width);
                            }
                            else
                            {
                                int.TryParse(command.Width, out width);
                            }
                        }
                        int height = 0;
                        if (!string.IsNullOrEmpty(command.Height))
                        {
                            int index = command.Height.IndexOf(".");
                            if (index > 0)
                            {
                                var intPortion = command.Height.Substring(0,
                                    index);
                                int.TryParse(intPortion, out height);
                            }
                            else
                            {
                                int.TryParse(command.Height, out height);
                            }
                        }

                        _imageServices.Salvar(rawFile,
                            newFile,
                            naturalWidth, widthSize,
                            xPoint, yPoint,
                            width, height);
                    }
                    articulo.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/articles/{0}/{1}{2}",
                                DefaultSize.ToString(),
                                command.FileName,
                                extention));
                    _articleRepository.Update(articulo);
                    var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Article with id {0} has change his image",
                            command.IDArticle);
                        response.success = true;
                        response.errors = null;
                        _articleCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Article with id {0} not updated",
                            command.IDArticle);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.IDEdition = null;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}