﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Articles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Articles;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class RemoveMultimediaCommandHandler : IRequestHandler<RemoveMultimediaCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IArticleMultimediaCache _articleMultimediaCache;
        private readonly IArticleRepository _articleRepository;
        public RemoveMultimediaCommandHandler(
            ILogger logger,
            IArticleMultimediaCache articleMultimediaCache,
            IArticleRepository articleRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleMultimediaCache = articleMultimediaCache ?? throw new ArgumentNullException(nameof(articleMultimediaCache));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
        }
        public async Task<CommandResponse> Handle(RemoveMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to remove multimedia on article with id {1}",
                    command.SolicitedBy,
                    command.IDArticle);
                var article = await _articleRepository.GetAsync(
                    idArticle: command.IDArticle);
                if (article != null)
                {
                    ArticleMultimedia multimedia = null;
                    if (article.ContainsMultimedia(
                        iDMultimedia: command.IDMultimedia,
                        multimedio: out multimedia))
                    {
                        article.RemoveMultimedia(
                            multimedio: multimedia);
                        _articleRepository.Delete(multimedia);
                        var saved = await _articleRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Article with id {0} has remove his multimedia",
                                command.IDArticle);
                            response.success = true;
                            response.errors = null;
                            _articleMultimediaCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Article with id {0} not updated",
                                command.IDArticle);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Article multimedia with id {0} not founded",
                            command.IDArticle);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el multimedio que desea borrar." };
                    }
                }
                else
                {
                    _logger.Warning("Article with id {0} not founded",
                        command.IDArticle);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el artículo buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}