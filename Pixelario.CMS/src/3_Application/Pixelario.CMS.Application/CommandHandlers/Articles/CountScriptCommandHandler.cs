﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class CountScriptCommandHandler : IRequestHandler<CountScriptCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptsQueries _articleScriptsQueries;
        public CountScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptsQueries articleScriptsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptsQueries = articleScriptsQueries ?? throw new ArgumentNullException(nameof(articleScriptsQueries));
        }
        public async Task<int> Handle(CountScriptCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _articleScriptCache.GetCount(
               keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _articleScriptsQueries.CountAsync(
                filteredBy: command.FilteredBy,
                enabled: command.Enabled);
            _articleScriptCache.SetCount(
               value: value,
               keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}