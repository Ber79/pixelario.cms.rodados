﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class GetScriptItemCommandHandler : IRequestHandler<GetScriptItemCommand, ArticleScriptItemQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptItemCache _articleScriptItemCache;
        private readonly IArticleScriptItemsQueries _articleScriptItemsQueries;
        public GetScriptItemCommandHandler(
            ILogger logger,
            IArticleScriptItemCache articleScriptItemCache,
            IArticleScriptItemsQueries articleScriptItemsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptItemCache = articleScriptItemCache ?? throw new ArgumentNullException(nameof(articleScriptItemCache));
            _articleScriptItemsQueries = articleScriptItemsQueries ?? throw new ArgumentNullException(nameof(articleScriptItemsQueries));
        }
        public async Task<ArticleScriptItemQueryModel> Handle(GetScriptItemCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _articleScriptItemCache.GetItem(
              keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _articleScriptItemsQueries.GetAsync(
                iDArticleScriptItem: command.IDArticleScriptItem);
            if (item != null)
            {
                _articleScriptItemCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}