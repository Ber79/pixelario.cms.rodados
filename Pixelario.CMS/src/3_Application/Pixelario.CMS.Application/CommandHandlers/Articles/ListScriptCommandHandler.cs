﻿using MediatR;
using Pixelario.CMS.Application.Cache.Articles;
using Pixelario.CMS.Application.Commands.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Articles
{
    public class ListScriptCommandHandler : IRequestHandler<ListScriptCommand, List<ArticleScriptQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IArticleScriptCache _articleScriptCache;
        private readonly IArticleScriptsQueries _articleScriptsQueries;
        public ListScriptCommandHandler(
            ILogger logger,
            IArticleScriptCache articleScriptCache,
            IArticleScriptsQueries articleScriptsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _articleScriptCache = articleScriptCache ?? throw new ArgumentNullException(nameof(articleScriptCache));
            _articleScriptsQueries = articleScriptsQueries ?? throw new ArgumentNullException(nameof(articleScriptsQueries));
        }
        public async Task<List<ArticleScriptQueryModel>> Handle(ListScriptCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _articleScriptCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _articleScriptsQueries.ListAsync(
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _articleScriptCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}