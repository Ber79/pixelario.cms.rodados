﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.Commands.Editions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.Queries.Editions;
using Pixelario.CMS.Application.Cache;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class GetCommandHandler : IRequestHandler<GetCommand, EditionQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionsQueries _editionsQueries;
        public GetCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionsQueries = editionsQueries ?? throw new ArgumentNullException(nameof(editionsQueries));
        }
        public async Task<EditionQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _editionCache.GetItem(
               keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _editionsQueries.GetAsync(
                iDEdition: command.IDEdition);
            if (item != null)
            {
                _editionCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}