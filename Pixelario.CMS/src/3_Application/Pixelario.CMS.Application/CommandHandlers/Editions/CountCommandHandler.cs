﻿using MediatR;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Application.Queries.Editions;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionsQueries _editionsQueries;

        public CountCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionsQueries = editionsQueries ?? throw new ArgumentNullException(nameof(editionsQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {

            var countFromCache = _editionCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _editionsQueries.CountAsync(
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu,
                editionTypes: command.EditionTypes);
            _editionCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}