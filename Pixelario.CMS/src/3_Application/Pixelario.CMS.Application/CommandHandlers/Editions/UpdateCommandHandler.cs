﻿using MediatR;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionRepository _editionRepository;
        public UpdateCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<CommandResponse> Handle(UpdateCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to update an edition with id {1}",
                    command.SolicitedBy,
                    command.IDEdition);
                var edition = await _editionRepository.GetAsync(
                    iDEdition: command.IDEdition);
                if (edition != null)
                {

                    edition.Update(
                        title: command.Title,
                        summary: command.Summary,
                        keywords: command.Keywords);
                    _editionRepository.Update(edition);
                    var saved = await _editionRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Edition with id {0} updated",
                            command.IDEdition);
                        response.success = true;
                        response.errors = null;
                        _editionCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Edition with id {0} not updated",
                            command.IDEdition);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Edition with id {0} not founded",
                        command.IDEdition);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la edición buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}