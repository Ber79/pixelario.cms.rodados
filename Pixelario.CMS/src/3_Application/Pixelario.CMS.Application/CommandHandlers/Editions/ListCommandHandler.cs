﻿using MediatR;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Application.Queries.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<EditionQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionsQueries _editionsQueries;
        public ListCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionsQueries = editionsQueries ?? throw new ArgumentNullException(nameof(editionsQueries));
        }
        public async Task<List<EditionQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _editionCache.GetList(
               keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _editionsQueries.ListAsync(
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu,
                editionTypes: command.EditionTypes,
                pagesOnEditionFilter: command.PagesOnEditionFilter,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _editionCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;

        }
    }
}