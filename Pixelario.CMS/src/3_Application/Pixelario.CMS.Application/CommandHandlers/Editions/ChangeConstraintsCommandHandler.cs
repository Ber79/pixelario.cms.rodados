﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Editions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System.Linq;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class ChangeConstraintsCommandHandler : IRequestHandler<ChangeConstraintsCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IConstraintRepository _constraintsRepository;
        public ChangeConstraintsCommandHandler(
            ILogger logger,
            IMediator mediator,
            IConstraintRepository constraintsRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _constraintsRepository = constraintsRepository ?? throw new ArgumentNullException(nameof(constraintsRepository));
        }
        public async Task<CommandResponse> Handle(ChangeConstraintsCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change constraints on edition with id {1}",
                    command.SolicitedBy,
                    command.IDEdition);
                var constraints = await _constraintsRepository.ListByConstrainedAsync(
                    iDConstraintType: ConstraintType.OnEdition.Id,
                    iDConstrained: command.IDEdition);
                if (constraints != null && constraints.Count > 0)
                {
                    foreach (var constraint in constraints)
                    {
                        if (command.PlansSelected == null || 
                            !command.PlansSelected.Contains(constraint.IDPlan))
                        {
                            _constraintsRepository.Delete(constraint);
                        }
                    }
                }
                else
                {
                    if (command.PlansSelected != null)
                    {
                        foreach (var plan in command.PlansSelected)
                        {
                            if (constraints.Where(c => c.IDPlan == plan).Count() == 0)
                            {
                                _constraintsRepository.Add(new Constraint(
                                    createdBy: command.SolicitedBy,
                                    plan: Plan.From(plan),
                                    constraintType: ConstraintType.OnEdition,
                                    iDConstrained: command.IDEdition));
                            }
                        }
                    }
                }
                var saved = await _constraintsRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Edition with id {0} has change enable value",
                        command.IDEdition);
                    response.success = true;
                    response.errors = null;
                }
                else
                {
                    _logger.Warning("Edition with id {0} not updated",
                        command.IDEdition);
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}