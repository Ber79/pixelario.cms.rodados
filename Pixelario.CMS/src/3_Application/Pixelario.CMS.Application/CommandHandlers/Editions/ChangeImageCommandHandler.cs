﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class ChangeImageCommandHandler : IRequestHandler<ChangeImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionRepository _editionRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 400, 800 };
        private int DefaultSize = 80;
        public ChangeImageCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionRepository editionRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangeImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change image type {1} on edition with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDEdition);
                var edition = await _editionRepository.GetAsync(
                    iDEdition: command.IDEdition);
                if (edition != null)
                {
                    foreach (var width in DefaultImagesSize)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\editions\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\editions\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\editions\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }
                    edition.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/editions/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName));
                    _editionRepository.Update(edition);
                    var saved = await _editionRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Edition with id {0} has change his image",
                            command.IDEdition);
                        response.success = true;
                        response.errors = null;
                        _editionCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Edition with id {0} not updated",
                            command.IDEdition);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }

                }
                else
                {
                    _logger.Warning("Edition with id {0} not founded",
                        command.IDEdition);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la edición buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}