﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Editions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class EnableCommandHandler : IRequestHandler<EnableCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionRepository _editionRepository;
        public EnableCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<CommandResponse> Handle(EnableCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change enable value on edition with id {1}",
                    command.SolicitedBy,
                    command.IDEdition);
                var edition = await _editionRepository.GetAsync(
                    iDEdition: command.IDEdition);
                if (edition != null)
                {
                    edition.ChangeEnabled(
                        enabled: !edition.Enabled);
                    _editionRepository.Update(edition);
                    var saved = await _editionRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Edition with id {0} has change enable value",
                            command.IDEdition);
                        response.success = true;
                        response.errors = null;
                        _editionCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Edition with id {0} not updated",
                            command.IDEdition);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Edition with id {0} not founded",
                        command.IDEdition);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la edición buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}