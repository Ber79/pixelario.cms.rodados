﻿using MediatR;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Commands.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Editions
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IEditionCache _editionCache;
        private readonly IEditionRepository _editionRepository;
        public AddCommandHandler(
            ILogger logger,
            IEditionCache editionCache,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _editionCache = editionCache ?? throw new ArgumentNullException(nameof(editionCache));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add an edition titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var edition = new Edition(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    summary: command.Summary,
                    keywords: command.Keywords);
                _editionRepository.Add(edition);
                var saved = await _editionRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Edition with title \"{0}\" saved with id {1}",
                        command.Title, edition.ID);
                    response.IDEdition = edition.ID;
                    response.success = true;
                    response.errors = null;
                    _editionCache.Clear();
                }
                else
                {
                    _logger.Warning("Edition {0} not saved",
                            command.Title);
                    response.IDEdition = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDEdition = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}