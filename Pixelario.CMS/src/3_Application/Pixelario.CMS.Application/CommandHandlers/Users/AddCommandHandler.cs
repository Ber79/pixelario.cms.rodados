﻿using MediatR;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;

        public AddCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add a user with username \"{1}\"",
                    command.CreatedBy,
                    command.UserName);
                var user = new ApplicationUser
                {
                    UserName = command.UserName,
                    Email = command.Email,
                    FirstName = command.FirstName,
                    LastName = command.LastName,
                    Picture = null
                };
                var saved = await _userManager.CreateAsync(
                    user: user,
                    password: command.Password);
                if (saved.Succeeded)
                {
                    _logger.Information("User with username \"{0}\" saved with id {1}",
                        command.UserName, user.Id);
                    response.ID = user.Id;
                    response.success = true;
                    response.errors = null;
                }
                else
                {
                    _logger.Warning("User with username {0} not saved",
                            command.UserName);
                    response.ID = null;
                    response.success = false;
                    response.errors = saved.Errors.ToList();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.ID = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}