﻿using MediatR;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;
        public CountCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            return await _userManager.Users.CountAsync();
        }
    }
}