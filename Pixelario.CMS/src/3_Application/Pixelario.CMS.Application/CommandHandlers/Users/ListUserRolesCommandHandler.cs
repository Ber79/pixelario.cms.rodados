﻿using MediatR;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class ListUserRolesCommandHandler : IRequestHandler<ListUserRolesCommand, List<string>>
    {
        private readonly ILogger _logger;
        private readonly ApplicationUserManager _userManager;
        public ListUserRolesCommandHandler(
            ILogger logger,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<List<string>> Handle(ListUserRolesCommand command,
            CancellationToken cancellationToken)
        {
            return await _userManager.GetRolesAsync(
                userId: command.UserId) as List<string>;
        }
    }
}