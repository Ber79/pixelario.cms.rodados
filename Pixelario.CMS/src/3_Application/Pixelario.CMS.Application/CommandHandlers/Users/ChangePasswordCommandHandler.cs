﻿using MediatR;
using Microsoft.AspNet.Identity;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class ChangePasswordCommandHandler :
        IRequestHandler<ChangePasswordCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;

        public ChangePasswordCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<CommandResponse> Handle(ChangePasswordCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change password on user with id {1}",
                    command.SolicitedBy, command.UserId);
                var user = await _userManager.FindByIdAsync(
                    userId: command.UserId);
                if (user != null)
                {
                    var resetToken = await _userManager.GeneratePasswordResetTokenAsync(
                        userId: command.UserId);
                    var saved = await _userManager.ResetPasswordAsync(
                        userId: command.UserId,
                        token: resetToken,
                        newPassword: command.Password
                        );

                    if (user.LockoutEnabled)
                    {
                        user.LockoutEnabled = false;
                        _userManager.Update(user);
                    }
                    if (saved.Succeeded)
                    {
                        _logger.Information("User with id {0} updated",
                            command.UserId); response.success = true;
                        response.errors = null;
                    }
                    else
                    {
                        _logger.Warning("User with id {0} not updated",
                            command.UserId);
                        response.success = false;
                        response.errors = saved.Errors.ToList();
                    }
                }
                else
                {
                    _logger.Warning("User with id {0} not found",
                        command.UserId);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el usuario buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message
    };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}