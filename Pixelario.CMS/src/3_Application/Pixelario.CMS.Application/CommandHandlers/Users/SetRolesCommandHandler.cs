﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class SetRolesCommandHandler : IRequestHandler<SetRolesCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;

        public SetRolesCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<CommandResponse> Handle(SetRolesCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to set roles \"{1}\" to user with id {2}",
                    command.SolicitedBy,
                    string.Join(",", command.Roles),
                    command.UserId);
                if (command.UserId == 1 && !command.Roles.Contains("Super Administrador"))
                {
                    // el usuar/io admin no puese perder el role de Super Administrador
                    response.success = false;
                    response.errors = new List<string>() { "El usuario admin no puese perder el role de Super Administrador." };
                }
                else
                {
                    var user = await _userManager.FindByIdAsync(
                        userId: command.UserId);
                    if (user != null)
                    {
                        var userRoles = await _userManager.GetRolesAsync(
                            userId: command.UserId);
                        var saved = await _userManager.AddToRolesAsync(
                            userId: command.UserId,
                            roles: command.Roles.Except(userRoles).ToArray<string>()
                            );
                        saved = await _userManager.RemoveFromRolesAsync(
                            userId: command.UserId,
                            roles: userRoles.Except(command.Roles).ToArray<string>()
                            );
                        if (saved.Succeeded)
                        {
                            _logger.Information("User with id {0} updated",
                                command.UserId);
                            response.success = true;
                            response.errors = null;
                        }
                        else
                        {
                            _logger.Warning("User with id {0} not updated",
                                    command.UserId);
                            response.success = false;
                            response.errors = saved.Errors.ToList();
                        }

                    }
                    else
                    {
                        _logger.Warning("User with id {0} not found",
                            command.UserId); response.success = false;
                        response.errors = new List<string>() { "No se encontró el usuario buscado." };
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}