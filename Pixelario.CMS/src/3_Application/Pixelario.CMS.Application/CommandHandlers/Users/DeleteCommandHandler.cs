﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;

namespace Pixelario.CMS.Web.Application.CommandHandlers.Users
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;

        public DeleteCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<CommandResponse> Handle(DeleteCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a user with id {1}",
                    command.SolicitedBy, command.UserId);
                var user = await _userManager.FindByIdAsync(
                    userId: command.UserId);
                var saved = await _userManager.DeleteAsync(
                    user: user);
                if (saved.Succeeded)
                {
                    _logger.Information("User with id {0} deleted",
                        command.UserId);
                    response.ID = user.Id;
                    response.success = true;
                    response.errors = null;
                }
                else
                {
                    _logger.Warning("User with id {0} not deleted",
                        command.UserId);
                    response.ID = null;
                    response.success = false;
                    response.errors = saved.Errors.ToList();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.ID = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}