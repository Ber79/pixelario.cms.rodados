﻿using MediatR;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class GetCommandHandler : IRequestHandler<GetCommand, ApplicationUser>
    {
        private readonly ILogger _logger;
        private readonly ApplicationUserManager _userManager;
        public GetCommandHandler(
            ILogger logger,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<ApplicationUser> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            return await _userManager.FindByIdAsync(
                userId: command.UserId);
        }
    }
}