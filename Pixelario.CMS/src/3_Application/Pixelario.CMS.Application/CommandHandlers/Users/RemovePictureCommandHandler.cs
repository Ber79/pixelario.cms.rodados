﻿using MediatR;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.CMS.Application.Services;
using Pixelario.CMS.SeedWork;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class RemovePictureCommandHandler :
        IRequestHandler<RemovePictureCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;

        public RemovePictureCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<CommandResponse> Handle(RemovePictureCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to remove picture on user with id {1}",
                    command.SolicitedBy, command.UserId);
                var user = await _userManager.FindByIdAsync(
                    userId: command.UserId);
                if (user != null)
                {
                    user.Picture = default(string);
                    var saved = await _userManager.UpdateAsync(
                        user: user);
                    if (saved.Succeeded)
                    {
                        _logger.Information("User with id {0} updated",
                            command.UserId); response.success = true;
                        response.errors = null;
                    }
                    else
                    {
                        _logger.Warning("User with id {0} not updated",
                            command.UserId);
                        response.success = false;
                        response.errors = saved.Errors.ToList();
                    }
                }
                else
                {
                    _logger.Warning("User with id {0} not found",
                        command.UserId);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el usuario buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() {
                    ex.Message
                };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}