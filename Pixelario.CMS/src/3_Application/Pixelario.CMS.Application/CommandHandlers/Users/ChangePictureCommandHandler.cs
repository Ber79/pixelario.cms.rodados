﻿using MediatR;
using Microsoft.AspNet.Identity;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Users;
using Pixelario.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;
using Pixelario.CMS.Application.Services;
using System.IO;

namespace Pixelario.CMS.Application.CommandHandlers.Users
{
    public class ChangePictureCommandHandler :
        IRequestHandler<ChangePictureCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationUserManager _userManager;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 400, 800 };
        private int DefaultSize = 80;

        public ChangePictureCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationUserManager userManager,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangePictureCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change picture on user with id {1}",
                    command.SolicitedBy, command.UserId);
                var user = await _userManager.FindByIdAsync(
                    userId: command.UserId);
                if (user != null)
                {
                    foreach (var width in DefaultImagesSize)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\users\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\users\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\users\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }

                    user.Picture = string.Format(
                            "/uploads/users/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName);
                    var saved = await _userManager.UpdateAsync(
                        user: user);
                    if (saved.Succeeded)
                    {
                        _logger.Information("User with id {0} updated",
                            command.UserId); response.success = true;
                        response.errors = null;
                    }
                    else
                    {
                        _logger.Warning("User with id {0} not updated",
                            command.UserId);
                        response.success = false;
                        response.errors = saved.Errors.ToList();
                    }
                }
                else
                {
                    _logger.Warning("User with id {0} not found",
                        command.UserId);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el usuario buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { 
                    ex.Message
                };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}