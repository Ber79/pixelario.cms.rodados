﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Advertisments;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class ChangePlaceAtWebCommandHandler : IRequestHandler<ChangePlaceAtWebCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentRepository _articuloRepository;
        public ChangePlaceAtWebCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentRepository articuloRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _articuloRepository = articuloRepository ?? throw new ArgumentNullException(nameof(articuloRepository));
        }
        public async Task<CommandResponse> Handle(ChangePlaceAtWebCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change place at web value on advertisment with id {1}",
                    command.SolicitedBy,
                    command.IDAdvertisment);
                var advertisment = await _articuloRepository.GetAsync(
                    idAdvertisment: command.IDAdvertisment);
                if (advertisment != null)
                {
                    advertisment.ChangePlaceAtWeb(
                        placeAtWeb: AdvertismentPlaceAtWeb.From(command.PlaceAtWeb));
                    _articuloRepository.Update(advertisment);
                    var saved = await _articuloRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Advertisment with id {0} has change his place AtWeb value to {1}",
                            command.IDAdvertisment, command.PlaceAtWeb);
                        response.success = true;
                        response.errors = null;
                        _advertismentCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Advertisment with id {0} not updated",
                            command.IDAdvertisment);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Advertisment with id {0} not founded",
                        command.IDAdvertisment);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el flyer buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}