﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Advertisments;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentRepository _advertismentRepository;
        public DeleteCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentRepository advertismentRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
        }
        public async Task<CommandResponse> Handle(DeleteCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete an advertisment with id {1}",
                    command.SolicitedBy,
                    command.IDAdvertisment);
                var advertisment = await _advertismentRepository.GetAsync(
                    idAdvertisment: command.IDAdvertisment);
                if (advertisment != null)
                {
                    if (!advertisment.Enabled)
                    {
                        _advertismentRepository.Delete(advertisment);
                        var saved = await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Advertisment with id {0} has been deleted",
                                command.IDAdvertisment);
                            response.success = true;
                            response.errors = null;
                            _advertismentCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Advertisment with id {0} has not been deleted",
                                command.IDAdvertisment);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Advertisment with id {0} has not been deleted",
                            command.IDAdvertisment);
                        response.success = false;
                        response.errors = new List<string>() { "Debe deshabilitar el flyer antes de borrarlo." };
                    }
                }
                else
                {
                    _logger.Warning("Advertisment with id {0} not founded",
                        command.IDAdvertisment);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el flyer buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}