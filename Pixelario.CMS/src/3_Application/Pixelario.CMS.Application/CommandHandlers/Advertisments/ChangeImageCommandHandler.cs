﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class ChangeImageCommandHandler : IRequestHandler<ChangeImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentRepository _advertismentRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImageSizes = { 80, 700, 500 };
        private int[] DefaultVerticalImageSizes = { 80, 425, 800 };
        private int DefaultSize = 80;
        public ChangeImageCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentRepository advertismentRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangeImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change image type {1} on advertisment with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDAdvertisment);

                var advertisment = await _advertismentRepository.GetAsync(
                    idAdvertisment: command.IDAdvertisment);
                if (advertisment != null)
                {
                    var imageSizes = DefaultImageSizes;
                    if (command.ImageType == ImageType.Vertical)
                        imageSizes = DefaultVerticalImageSizes;
                    foreach (var width in imageSizes)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\advertisments\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\advertisments\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\advertisments\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }
                    advertisment.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/advertisments/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName));
                    _advertismentRepository.Update(advertisment);
                    var saved = await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Advertisment with id {0} has change his image",
                            command.IDAdvertisment);
                        response.success = true;
                        response.errors = null;
                        _advertismentCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Advertisment with id {0} not updated",
                            command.IDAdvertisment);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Advertisment with id {0} not founded",
                        command.IDAdvertisment);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el flyer buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}