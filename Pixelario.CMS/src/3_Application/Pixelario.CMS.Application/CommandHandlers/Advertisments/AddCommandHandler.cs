﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentRepository _advertismentRepository;
        public AddCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentRepository advertismentRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add an advertisment titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var advertisment = new Advertisment(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    code: command.Code,
                    url: command.URL);
                _advertismentRepository.Add(advertisment);
                var saved = await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Advertisment with title \"{0}\" saved with id {1}",
                        command.Title, advertisment.ID);
                    response.IDAdvertisment = advertisment.ID;
                    response.success = true;
                    response.errors = null;
                    _advertismentCache.Clear();
                }
                else
                {
                    _logger.Warning("Advertisment {0} not saved",
                        command.Title);
                    response.IDAdvertisment = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDAdvertisment = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}