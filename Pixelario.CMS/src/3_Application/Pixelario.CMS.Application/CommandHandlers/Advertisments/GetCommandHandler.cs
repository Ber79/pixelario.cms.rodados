﻿using MediatR;
using Pixelario.CMS.Application.Cache.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.Queries.Advertisments;
using Pixelario.CMS.Application.QueryModels.Advertisments;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class GetCommandHandler : IRequestHandler<GetCommand, AdvertismentQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentsQueries _advertismentsQueries;
        public GetCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentsQueries advertismentsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentsQueries = advertismentsQueries ?? throw new ArgumentNullException(nameof(advertismentsQueries));
        }
        public async Task<AdvertismentQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _advertismentCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _advertismentsQueries.GetAsync(
                iDAdvertisment: command.IDAdvertisment);
            if (item != null)
            {
                _advertismentCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}