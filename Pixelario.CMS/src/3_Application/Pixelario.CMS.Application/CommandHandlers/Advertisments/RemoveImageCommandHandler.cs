﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Cache.Advertisments;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class RemoveImageCommandHandler : IRequestHandler<RemoveImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentRepository _advertismentRepository;
        public RemoveImageCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentRepository advertismentRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
        }
        public async Task<CommandResponse> Handle(RemoveImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to remove image type {1} on advertisment with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDAdvertisment);
                var advertisment = await _advertismentRepository.GetAsync(
                idAdvertisment: command.IDAdvertisment);
                if (advertisment != null)
                {
                    advertisment.ChangeImage(
                        imageType: command.ImageType,
                        uri: null);
                    _advertismentRepository.Update(advertisment);
                    var saved = await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Advertisment with id {0} has remove his image",
                            command.IDAdvertisment);
                        response.success = true;
                        response.errors = null;
                        _advertismentCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Advertisment with id {0} not updated",
                            command.IDAdvertisment);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;                    }
                }
                else
                {
                    _logger.Warning("Advertisment with id {0} not founded",
                        command.IDAdvertisment);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el flyer buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}