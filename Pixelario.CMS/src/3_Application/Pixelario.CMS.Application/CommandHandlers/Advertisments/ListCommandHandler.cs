﻿using MediatR;
using Pixelario.CMS.Application.Cache.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.Queries.Advertisments;
using Pixelario.CMS.Application.QueryModels.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<AdvertismentQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentsQueries _advertismentsQueries;
        public ListCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentsQueries advertismentsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentsQueries = advertismentsQueries ?? throw new ArgumentNullException(nameof(advertismentsQueries));
        }
        public async Task<List<AdvertismentQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _advertismentCache.GetList(
               keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }           
            AdvertismentPlaceAtWeb placeAtWeb = null;
            if (command.PlaceAtWeb.HasValue)
                placeAtWeb = AdvertismentPlaceAtWeb.FromValue<AdvertismentPlaceAtWeb>(command.PlaceAtWeb.Value);
            var list = await _advertismentsQueries.ListAsync(
                placeAtWeb: placeAtWeb,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                published: command.Published,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if(list != null)
            {
                _advertismentCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}