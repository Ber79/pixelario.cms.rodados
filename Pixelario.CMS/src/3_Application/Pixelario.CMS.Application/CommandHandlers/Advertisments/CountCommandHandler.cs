﻿using MediatR;
using Pixelario.CMS.Application.Cache.Advertisments;
using Pixelario.CMS.Application.Commands.Advertisments;
using Pixelario.CMS.Application.Queries.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Advertisments
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IAdvertismentCache _advertismentCache;
        private readonly IAdvertismentsQueries _advertismentsQueries;
        public CountCommandHandler(
            ILogger logger,
            IAdvertismentCache advertismentCache,
            IAdvertismentsQueries advertismentsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _advertismentCache = advertismentCache ?? throw new ArgumentNullException(nameof(advertismentCache));
            _advertismentsQueries = advertismentsQueries ?? throw new ArgumentNullException(nameof(advertismentsQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _advertismentCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            AdvertismentPlaceAtWeb placeAtWeb = null;
            if (command.PlaceAtWeb.HasValue)
                placeAtWeb = AdvertismentPlaceAtWeb.FromValue<AdvertismentPlaceAtWeb>(command.PlaceAtWeb.Value);
            var value = await _advertismentsQueries.CountAsync(
                placeAtWeb: placeAtWeb,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                published: command.Published);
            _advertismentCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}