﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class DeleteConfigurationOptionCommandHandler : IRequestHandler<DeleteConfigurationOptionCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationRepository _configurationRepository;
        public DeleteConfigurationOptionCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(DeleteConfigurationOptionCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a configuration option with id {1}",
                    command.SolicitedBy,
                    command.IDConfigurationOption);
                var configuration = await _configurationRepository.GetAsync(
                    iDConfiguration: command.IDConfiguration);
                if (configuration != null)
                {

                    ConfigurationOption configurationOption = null;
                    if (configuration.HasConfigurationOption(
                        iDConfigurationOption: command.IDConfigurationOption,
                        configurationOption: out configurationOption))
                    {
                        if (!configurationOption.Enabled)
                        {
                            configuration.DeleteConfigurationOption(configurationOption);
                            _configurationRepository.DeleteConfigurationOption(configurationOption);
                            var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                            if (saved.Success)
                            {
                                _logger.Information("Configuration option with id {0} has been deleted",
                                    command.IDConfigurationOption);
                                response.success = true;
                                response.errors = null;
                                _configurationOptionCache.Clear();
                            }
                            else
                            {
                                _logger.Warning("Configuration option with id {0} has not been deleted",
                                    command.IDConfigurationOption);
                                response.success = false;
                                response.errors = saved.Errors as List<string>;
                            }
                        }
                        else
                        {
                            _logger.Warning("Configuration option with id {0} has not been deleted",
                                command.IDConfigurationOption);
                            response.success = false;
                            response.errors = new List<string>() { "Debe deshabilitar el sub configuration antes de borrarlo." };
                        }
                    }
                    else
                    {
                        _logger.Warning("Configuration option with id {0} not founded",
                            command.IDConfigurationOption);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el sub configuration que desea borrar." };
                    }

                }
                else
                {
                    _logger.Warning("Configuration with id {0} not founded",
                        command.IDConfiguration);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el configuration buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}