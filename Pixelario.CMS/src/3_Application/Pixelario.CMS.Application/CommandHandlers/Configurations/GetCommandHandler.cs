﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class GetCommandHandler : IRequestHandler<GetCommand, ConfigurationQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationsQueries _configurationsQueries;
        public GetCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationsQueries configurationsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationsQueries = configurationsQueries ?? throw new ArgumentNullException(nameof(configurationsQueries));
        }
        public async Task<ConfigurationQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _configurationCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _configurationsQueries.GetAsync(
                iDConfiguration: command.IDConfiguration);
            if (item != null)
            {
                _configurationCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}