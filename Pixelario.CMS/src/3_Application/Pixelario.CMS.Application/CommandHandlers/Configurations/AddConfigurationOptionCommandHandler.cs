﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class AddConfigurationOptionCommandHandler : IRequestHandler<AddConfigurationOptionCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationRepository _configurationRepository;
        public AddConfigurationOptionCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(AddConfigurationOptionCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add a configuratioOption valued \"{1}\"",
                    command.SolicitedBy,
                    command.Value);
                var configuration = await _configurationRepository.GetAsync(
                    iDConfiguration: command.IDConfiguration);
                if (configuration != null)
                {
                    configuration.AddConfigurationOption(
                        solicitedBy: command.SolicitedBy,
                        value: command.Value);
                    _configurationRepository.Update(configuration);
                    var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Configuration option with value \"{0}\" saved on configuration with id {1}",
                            command.Value, configuration.ID);
                        response.success = true;
                        response.errors = null;
                        _configurationOptionCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Configuration {0} not saved",
                               command.Value);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Configuration with id {0} not found",
                        command.IDConfiguration);
                    response.success = false;
                    response.errors = new List<string>() { "No se pudo encontrar la configuración." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}