﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<ConfigurationQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationsQueries _configurationsQueries;
        public ListCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationsQueries configurationsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationsQueries = configurationsQueries ?? throw new ArgumentNullException(nameof(configurationsQueries));
        }
        public async Task<List<ConfigurationQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _configurationCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _configurationsQueries.ListAsync(
                filteredBy: command.FilteredBy,
                configurationType: command.ConfigurationType,
                keys: command.Keys,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.OrderBy,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _configurationCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;

        }
    }
}