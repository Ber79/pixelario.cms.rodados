﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class ListConfigurationOptionsCommandHandler : IRequestHandler<ListConfigurationOptionsCommand, List<ConfigurationOptionQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationOptionQueries _configurationOptionsQueries;
        public ListConfigurationOptionsCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationOptionQueries configurationOptionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationOptionsQueries = configurationOptionsQueries ?? throw new ArgumentNullException(nameof(configurationOptionsQueries));
        }
        public async Task<List<ConfigurationOptionQueryModel>> Handle(ListConfigurationOptionsCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _configurationOptionCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _configurationOptionsQueries.ListAsync(
                iDConfiguration: command.IDConfiguration,
                enabled: command.Enabled,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _configurationOptionCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}