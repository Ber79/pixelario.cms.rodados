﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class CountConfigurationOptionCommandHandler : IRequestHandler<CountConfigurationOptionsCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationOptionQueries _configurationOptionsQueries;
        public CountConfigurationOptionCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationOptionQueries configurationOptionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationOptionsQueries = configurationOptionsQueries ?? throw new ArgumentNullException(nameof(configurationOptionsQueries));
        }
        public async Task<int> Handle(CountConfigurationOptionsCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _configurationOptionCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _configurationOptionsQueries.CountAsync(
                iDConfiguration: command.IDConfiguration,
                enabled: command.Enabled);

            _configurationOptionCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;

        }
    }
}