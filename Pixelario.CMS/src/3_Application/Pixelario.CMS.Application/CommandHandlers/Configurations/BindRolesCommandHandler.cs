﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class BindRolesCommandHandler : IRequestHandler<BindRolesCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationRepository _configurationRepository;
        public BindRolesCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(BindRolesCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to set roles of a configuration with id {1}",
                    command.SolicitedBy,
                    command.IDConfiguration);
                var configuration = await _configurationRepository.GetAsync(
                    iDConfiguration: command.IDConfiguration);
                if (configuration != null)
                {
                    foreach(var role in configuration.GetBindedRoles())
                    {
                        configuration.UnbindToRole(
                            roleName: role);
                    }
                    foreach(var role in command.Roles)
                    {
                        configuration.BindToRole(
                            roleName: role);
                    }                    
                    _configurationRepository.Update(configuration);
                    var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Configuration with id {0} updated",
                            command.IDConfiguration);
                        response.success = true;
                        response.errors = null;
                        _configurationCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Configuration with id {0} not updated",
                            command.IDConfiguration);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Configuration with id {0} not founded",
                        command.IDConfiguration);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la configuración buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}