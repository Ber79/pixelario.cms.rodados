﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationRepository _configurationRepository;
        public DeleteCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(DeleteCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a configuration with id {1}",
                    command.SolicitedBy,
                    command.IDConfiguration);
                var configuration = await _configurationRepository.GetAsync(
                    iDConfiguration: command.IDConfiguration);
                if (configuration != null)
                {
                    _configurationRepository.Delete(configuration);
                    var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Configuration with id {0} has been deleted",
                            command.IDConfiguration);
                        response.success = true;
                        response.errors = null;
                        _configurationCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Configuration with id {0} has not been deleted",
                            command.IDConfiguration);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Configuration with id {0} not founded",
                        command.IDConfiguration); 
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la confiuración buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}