﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationRepository _configurationRepository;
        public AddCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add a configuration with key \"{1}\"",
                    command.CreatedBy,
                    command.Key);
                var configuration = new Configuration(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    key: command.Key,
                    configurationType: command.ConfigurationType);
                _configurationRepository.Add(configuration);
                var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Configuration with key \"{0}\" saved with id {1}",
                        command.Key, configuration.ID);
                    response.IDConfiguration = configuration.ID;
                    response.success = true;
                    response.errors = null;
                    _configurationCache.Clear();
                }
                else
                {
                    _logger.Warning("Configuration with key {0} not saved",
                            command.Key);
                    response.IDConfiguration = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDConfiguration = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}