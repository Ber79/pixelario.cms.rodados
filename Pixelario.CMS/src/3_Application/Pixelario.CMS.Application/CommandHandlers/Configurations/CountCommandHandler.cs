﻿using MediatR;
using Pixelario.CMS.Application.Cache;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationsQueries _configurationsQueries;
        public CountCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationsQueries configurationsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationsQueries = configurationsQueries ?? throw new ArgumentNullException(nameof(configurationsQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _configurationCache.GetCount(
                keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _configurationsQueries.CountAsync(
                filteredBy: command.FilteredBy,
                configurationType: command.ConfigurationType,
                keys: command.Keys);

            _configurationCache.SetCount(
                value: value,
                keyAddition: command.GetHashCode().ToString());
            return value;
        }
    }
}