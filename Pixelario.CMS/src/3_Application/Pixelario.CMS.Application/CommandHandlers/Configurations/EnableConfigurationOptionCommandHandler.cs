﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class EnableConfigurationOptionCommandHandler : IRequestHandler<EnableConfigurationOptionCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationRepository _configurationRepository;
        public EnableConfigurationOptionCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(EnableConfigurationOptionCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change enable value on subconfiguration with id {1}",
                    command.SolicitedBy,
                    command.IDConfigurationOption);
                var configuration = await _configurationRepository.GetAsync(
                    iDConfiguration: command.IDConfiguration);
                if (configuration != null)
                {
                    ConfigurationOption configurationOption = null;
                    if (configuration.HasConfigurationOption(
                        iDConfigurationOption: command.IDConfigurationOption,
                        configurationOption: out configurationOption))
                    {
                        configuration.ChangeConfigurationOptionEnabled(
                            iDConfigurationOption: command.IDConfigurationOption,
                            enabled: !configurationOption.Enabled);
                        _configurationRepository.Update(configuration);
                        var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Configuration option with id {0} has change enable value",
                                command.IDConfigurationOption);    
                            response.success = true;
                            response.errors = null;
                            _configurationOptionCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Configuration option with id {0} not updated",
                                command.IDConfigurationOption);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Configuration option with id {0} not founded",
                            command.IDConfigurationOption);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el sub tema buscado." };

                    }
                }
                else
                {
                    _logger.Warning("Configuration with id {0} not founded",
                           command.IDConfiguration);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el tema buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}