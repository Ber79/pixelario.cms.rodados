﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class GetConfigurationOptionCommandHandler : IRequestHandler<GetConfigurationOptionCommand, ConfigurationOptionQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationOptionCache _configurationOptionCache;
        private readonly IConfigurationOptionQueries _configurationOptionQueries;
        public GetConfigurationOptionCommandHandler(
            ILogger logger,
            IConfigurationOptionCache configurationOptionCache,
            IConfigurationOptionQueries configurationOptionQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationOptionCache = configurationOptionCache ?? throw new ArgumentNullException(nameof(configurationOptionCache));
            _configurationOptionQueries = configurationOptionQueries ?? throw new ArgumentNullException(nameof(configurationOptionQueries));
        }
        public async Task<ConfigurationOptionQueryModel> Handle(GetConfigurationOptionCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _configurationOptionCache.GetItem(
               keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _configurationOptionQueries.GetAsync(
                iDConfigurationOption: command.IDConfigurationOption);
            if (item != null)
            {
                _configurationOptionCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}