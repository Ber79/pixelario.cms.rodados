﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Configurations;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Serilog;
using Pixelario.CMS.Application.Cache.Configurations;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class SetStateCommandHandler : IRequestHandler<SetStateCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationRepository _configurationRepository;
        public SetStateCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationRepository configurationRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationRepository = configurationRepository ?? throw new ArgumentNullException(nameof(configurationRepository));
        }
        public async Task<CommandResponse> Handle(SetStateCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to set state of a configuration with key {1}",
                    command.SolicitedBy,
                    command.Key);
                var configuration = await _configurationRepository.GetAsync(
                    key: command.Key);
                if (configuration != null)
                {
                    configuration.SetState(
                        state: command.State);
                    _configurationRepository.Update(configuration);
                    var saved = await _configurationRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Configuration with key {0} updated",
                            command.Key);
                        response.success = true;
                        response.errors = null;
                        _configurationCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Configuration with key {0} not updated",
                            command.Key);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Configuration with key {0} not founded",
                        command.Key);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la configuración buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}