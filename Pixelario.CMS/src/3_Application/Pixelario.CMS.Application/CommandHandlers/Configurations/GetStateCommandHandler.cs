﻿using MediatR;
using Pixelario.CMS.Application.Cache.Configurations;
using Pixelario.CMS.Application.Commands.Configurations;
using Pixelario.CMS.Application.Queries.Configurations;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Configurations
{
    public class GetStateCommandHandler : IRequestHandler<GetStateCommand, string>
    {
        private readonly ILogger _logger;
        private readonly IConfigurationCache _configurationCache;
        private readonly IConfigurationsQueries _configurationsQueries;
        public GetStateCommandHandler(
            ILogger logger,
            IConfigurationCache configurationCache,
            IConfigurationsQueries configurationsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationCache = configurationCache ?? throw new ArgumentNullException(nameof(configurationCache));
            _configurationsQueries = configurationsQueries ?? throw new ArgumentNullException(nameof(configurationsQueries));
        }
        public async Task<string> Handle(GetStateCommand command,
            CancellationToken cancellationToken)
        {
            var stateFromCache = _configurationCache.GetState(
                keyAddition: command.GetHashCode().ToString());
            if (stateFromCache != null)
            {
                return stateFromCache;
            }
            var state = await _configurationsQueries.GetStateAsync(
                key: command.Key);
            if (state != null)
            {
                _configurationCache.SetState(
                    value: state,
                    keyAddition: command.GetHashCode().ToString());
            }
            return state;
        }
    }
}