﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class ListMultimediaCommandHandler : IRequestHandler<ListMultimediaCommand, List<PageMultimediaQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IPageMultimediaCache _pageMultimediaCache;
        private readonly IPageMultimediaQueries _pageMultimediaQueries;
        public ListMultimediaCommandHandler(
            ILogger logger,
            IPageMultimediaCache pageMultimediaCache,
            IPageMultimediaQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageMultimediaCache = pageMultimediaCache ?? throw new ArgumentNullException(nameof(pageMultimediaCache));
            _pageMultimediaQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task<List<PageMultimediaQueryModel>> Handle(ListMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _pageMultimediaCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            int? iDMultimediaType = null;
            if (command.MultimediaType != null)
                iDMultimediaType = command.MultimediaType.Id;
            var list = await _pageMultimediaQueries.ListAsync(
                iDPage: command.IDPage,
                iDMultimediaType: iDMultimediaType,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {

                _pageMultimediaCache.SetList(
                value: list,
                keyAddition: command.GetHashCode().ToString());
            }
            return list;

        }
    }
}