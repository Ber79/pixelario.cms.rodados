﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class ChangeImageCommandHandler : IRequestHandler<ChangeImageCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPageRepository _pageRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 400, 945 };
        private int DefaultSize = 80;
        public ChangeImageCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPageRepository pageRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(ChangeImageCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add an image type {1} on page with id {2}",
                    command.SolicitedBy, command.ImageType,
                    command.IDPage);
                var page = await _pageRepository.GetAsync(
                    iDPage: command.IDPage);
                if (page != null)
                {
                    foreach (var width in DefaultImagesSize)
                    {
                        var directory = Path.Combine(command.AppDomainAppPath,
                            string.Format("uploads\\pages\\{0}", width.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);
                        var rawFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\pages\\raw\\{0}",
                                    command.FileName));
                        var newFile = Path.Combine(
                            command.AppDomainAppPath,
                            string.Format(
                                "uploads\\pages\\{0}\\{1}",
                                    width.ToString(),
                                    command.FileName));
                        _imageServices.Salvar(rawFile,
                            newFile, width);
                    }
                    page.ChangeImage(
                        imageType: command.ImageType,
                        uri: string.Format(
                            "/uploads/pages/{0}/{1}",
                                DefaultSize.ToString(),
                                command.FileName));
                    _pageRepository.Update(page);
                    var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Page with id {0} has add his image",
                            command.IDPage);
                        response.success = true;
                        response.errors = null;
                        _pageCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Page with id {0} not updated",
                            command.IDPage);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Page with id {0} not founded",
                        command.IDPage);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la página buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}