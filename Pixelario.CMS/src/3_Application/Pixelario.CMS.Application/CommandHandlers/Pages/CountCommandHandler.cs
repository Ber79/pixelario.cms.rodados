﻿using MediatR;
using Pixelario.CMS.Application.Cache.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPagesQueries _pagesQueries;
        public CountCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPagesQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pagesQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            var countFromCache = _pageCache.GetCount(
               keyAddition: command.GetHashCode().ToString());
            if (countFromCache.HasValue)
            {
                return countFromCache.Value;
            }
            var value = await _pagesQueries.CountAsync(
                iDEdition: command.IDEdition,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu);
            _pageCache.SetCount(
               value: value,
               keyAddition: command.GetHashCode().ToString());
            return value;

        }
    }
}