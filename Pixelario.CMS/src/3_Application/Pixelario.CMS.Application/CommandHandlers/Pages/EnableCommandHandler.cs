﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class EnableCommandHandler : IRequestHandler<EnableCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPageRepository _pageRepository;
        public EnableCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPageRepository pageRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
        }
        public async Task<CommandResponse> Handle(EnableCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to change enable value on page with id {1}",
                    command.SolicitedBy,
                    command.IDPage);

                var page = await _pageRepository.GetAsync(
                    iDPage: command.IDPage);
                if (page != null)
                {
                    page.ChangeEnabled(
                        enabled: !page.Enabled);
                    _pageRepository.Update(page);
                    var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Page with id {0} has change enable value",
                            command.IDPage);
                        response.success = true;
                        response.errors = null;
                        _pageCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Page with id {0} not updated",
                            command.IDPage);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Page with id {0} not founded",
                        command.IDPage);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la página buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}