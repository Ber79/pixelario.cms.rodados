﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPageRepository _pageRepository;
        private readonly IEditionRepository _editionRepository;
        public UpdateCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPageRepository pageRepository,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<CommandResponse> Handle(UpdateCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to update a page with id {1}",
                    command.SolicitedBy,
                    command.IDPage);

                var page = await _pageRepository.GetAsync(
                    iDPage: command.IDPage);
                if (page != null)
                {
                    page.Update(
                        title: command.Title,
                        summary: command.Summary,
                        body: command.Body,
                        keywords: command.Keywords);
                    if (command.Editions.Any())
                    {
                        var editionsToRemove = page.Editions.Select(e => e.ID)
                            .Where(e => !command.Editions.Contains(e)).ToList();
                        foreach (var id in editionsToRemove)
                        {
                            page.UnbindEdition(
                                edition: page.Editions.First(
                                    e => e.ID == id)
                            );
                        }
                        var editionsToAdd = command.Editions.Where(
                            e => page.Editions.Where(et => et.ID == e).Count() == 0)
                            .ToList();
                        foreach (var id in editionsToAdd)
                        {
                            var edition = await _editionRepository.GetAsync(
                                iDEdition: id);
                            if (edition != null)
                            {
                                page.BindEdition(
                                    edition: edition);
                            }
                        }
                    }
                    else if (page.Editions.Any())
                    {
                        foreach (var edition in page.Editions)
                            page.UnbindEdition(
                                edition: edition);
                    }
                    _pageRepository.Update(page);
                    var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Page with id {0} updated",
                            command.IDPage);
                        response.success = true;
                        response.errors = null;
                        _pageCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Page with id {0} not updated",
                            command.IDPage);

                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Page with id {0} not founded",
                        command.IDPage);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la página buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}