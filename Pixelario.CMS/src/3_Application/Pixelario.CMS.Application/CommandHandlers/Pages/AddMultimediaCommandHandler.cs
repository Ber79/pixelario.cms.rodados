﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class AddMultimediaCommandHandler :
        IRequestHandler<AddMultimediaCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageMultimediaCache _pageMultimediaCache;
        private readonly IPageRepository _pageRepository;
        private readonly IImageServices _imageServices;
        private int[] DefaultImagesSize = { 80, 450, 800 };
        private int DefaultSize = 80;
        public AddMultimediaCommandHandler(
            ILogger logger,
            IPageMultimediaCache pageMultimediaCache,
            IPageRepository pageRepository,
            IImageServices imageServices)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageMultimediaCache = pageMultimediaCache ?? throw new ArgumentNullException(nameof(pageMultimediaCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _imageServices = imageServices ?? throw new ArgumentNullException(nameof(imageServices));
        }
        public async Task<CommandResponse> Handle(AddMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to add a multimedia type {1} on page with id {2}",
                    command.SolicitedBy, command.MultimediaType,
                    command.IDPage);

                var page = await _pageRepository.GetAsync(
                    iDPage: command.IDPage);
                if (page != null)
                {
                    string path1 = null;
                    string path2 = null;
                    string summary = null;
                    if (command.MultimediaType == MultimediaType.Imagen)
                    {
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\pages\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/pages/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                    }
                    else if (command.MultimediaType == MultimediaType.Documento)
                    {
                        path1 = string.Format("/uploads/pages/documents/{0}",
                            command.Path1);
                        path2 = command.Path2;
                    }
                    else if (command.MultimediaType == MultimediaType.Widget)
                    {
                        summary = command.Summary;
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\pages\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/pages/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                    }
                    else if (command.MultimediaType == MultimediaType.Link)
                    {
                        foreach (var width in DefaultImagesSize)
                        {
                            var directory = Path.Combine(command.AppDomainAppPath,
                                string.Format("uploads\\pages\\{0}", width.ToString()));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);
                            var rawFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\raw\\{0}",
                                        command.Path1));
                            var newFile = Path.Combine(
                                command.AppDomainAppPath,
                                string.Format(
                                    "uploads\\pages\\{0}\\{1}",
                                        width.ToString(),
                                        command.Path1));
                            _imageServices.Salvar(rawFile,
                                newFile, width);
                        }
                        path1 = string.Format(
                            "/uploads/pages/{0}/{1}",
                                DefaultSize.ToString(),
                                command.Path1);
                        path2 = command.Path2;
                    }
                    // Todo: Log
                    page.AddMultimedia(
                        createdBy: command.SolicitedBy,
                        title: command.Title,
                        summary: summary,
                        path1: path1,
                        path2: path2,
                        multimediaType: command.MultimediaType);
                    _pageRepository.Update(page);
                    var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                    if (saved.Success)
                    {
                        _logger.Information("Page with id {0} has add a multimedia",
                            page.ID);
                        response.success = true;
                        response.errors = null;
                        _pageMultimediaCache.Clear();
                    }
                    else
                    {
                        _logger.Warning("Page with id {0} not updated",
                            command.IDPage);
                        response.success = false;
                        response.errors = saved.Errors as List<string>;
                    }
                }
                else
                {
                    _logger.Warning("Page with id {0} not founded",
                        command.IDPage);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la página buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}