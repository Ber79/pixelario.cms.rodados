﻿using MediatR;
using Pixelario.CMS.Application.Cache.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using Pixelario.CMS.Application.QueryModels.Pages;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class GetMultimediaCommandHandler : IRequestHandler<GetMultimediaCommand, PageMultimediaQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IPageMultimediaCache _pageMultimediaCache;
        private readonly IPageMultimediaQueries _pageMultimediaQueries;
        public GetMultimediaCommandHandler(
            ILogger logger,
            IPageMultimediaCache pageMultimediaCache,
            IPageMultimediaQueries pageMultimediaQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageMultimediaCache = pageMultimediaCache ?? throw new ArgumentNullException(nameof(pageMultimediaCache));
            _pageMultimediaQueries = pageMultimediaQueries ?? throw new ArgumentNullException(nameof(pageMultimediaQueries));
        }
        public async Task<PageMultimediaQueryModel> Handle(GetMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _pageMultimediaCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _pageMultimediaQueries.GetAsync(
                iDMultimedia: command.IDMultimedia);
            if (item != null)
            {
                _pageMultimediaCache.SetItem(
                   value: item,
                   keyAddition: command.GetHashCode().ToString());
            }
            return item;

        }
    }
}