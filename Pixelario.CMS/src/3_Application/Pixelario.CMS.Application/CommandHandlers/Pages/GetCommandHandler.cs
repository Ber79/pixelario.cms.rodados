﻿using MediatR;
using Pixelario.CMS.Application.Cache.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using Pixelario.CMS.Application.QueryModels.Pages;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class GetCommandHandler : IRequestHandler<GetCommand, PageQueryModel>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPagesQueries _pagesQueries;
        public GetCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPagesQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pagesQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task<PageQueryModel> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            var itemFromCache = _pageCache.GetItem(
                keyAddition: command.GetHashCode().ToString());
            if (itemFromCache != null)
            {
                return itemFromCache;
            }
            var item = await _pagesQueries.GetAsync(
                iDPage: command.IDPage);
            if (item != null)
            {
                _pageCache.SetItem(
                    value: item,
                    keyAddition: command.GetHashCode().ToString());
            }
            return item;
        }
    }
}