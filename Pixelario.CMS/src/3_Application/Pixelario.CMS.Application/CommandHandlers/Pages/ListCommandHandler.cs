﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<PageQueryModel>>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPagesQueries _pagesQueries;
        public ListCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPagesQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pagesQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task<List<PageQueryModel>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            var listFromCache = _pageCache.GetList(
                keyAddition: command.GetHashCode().ToString());
            if (listFromCache != null)
            {
                return listFromCache;
            }
            var list = await _pagesQueries.ListAsync(
                iDEdition: command.IDEdition,
                filteredBy: command.FilteredBy,
                enabled: command.Enabled,
                atHome: command.AtHome,
                onMenu: command.OnMenu,
                rowIndex: command.RowIndex,
                rowCount: command.RowCount,
                columnOrder: command.ColumnOrder,
                orderDirection: command.OrderDirection);
            if (list != null)
            {
                _pageCache.SetList(
                    value: list,
                    keyAddition: command.GetHashCode().ToString());
            }
            return list;
        }
    }
}