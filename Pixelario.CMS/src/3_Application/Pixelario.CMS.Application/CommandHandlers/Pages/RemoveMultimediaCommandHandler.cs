﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class RemoveMultimediaCommandHandler : IRequestHandler<RemoveMultimediaCommand,
        CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageMultimediaCache _pageMultimediaCache;
        private readonly IPageRepository _pageRepository;
        public RemoveMultimediaCommandHandler(
            ILogger logger,
            IPageMultimediaCache pageMultimediaCache,
            IPageRepository pageRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageMultimediaCache = pageMultimediaCache ?? throw new ArgumentNullException(nameof(pageMultimediaCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
        }
        public async Task<CommandResponse> Handle(RemoveMultimediaCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to remove multimedia with id {1} on page with id {2}",
                    command.SolicitedBy, command.IDMultimedia,
                    command.IDPage);
                var page = await _pageRepository.GetAsync(
                    iDPage: command.IDPage);
                if (page != null)
                {
                    PageMultimedia multimedia = null;
                    if (page.ContainsMultimedia(
                        iDMultimedia: command.IDMultimedia,
                        multimedia: out multimedia))
                    {
                        page.RemoveMultimedia(
                            multimedia: multimedia);
                        _pageRepository.Delete(multimedia);
                        var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                        if (saved.Success)
                        {
                            _logger.Information("Page with id {0} has remove his multimedia",
                                command.IDPage);
                            response.success = true;
                            response.errors = null;
                            _pageMultimediaCache.Clear();
                        }
                        else
                        {
                            _logger.Warning("Page with id {0} not updated",
                                command.IDPage);
                            response.success = false;
                            response.errors = saved.Errors as List<string>;
                        }
                    }
                    else
                    {
                        _logger.Warning("Page with id {0} not founded",
                            command.IDPage);
                        response.success = false;
                        response.errors = new List<string>() { "No se encontró el multimedio que desea borrar." };
                    }
                }
                else
                {
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró la página buscada." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);
            }
            return response;
        }
    }
}