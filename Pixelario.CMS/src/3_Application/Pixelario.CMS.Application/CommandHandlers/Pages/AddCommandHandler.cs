﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Application.Commands.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Application.Cache.Pages;

namespace Pixelario.CMS.Application.CommandHandlers.Pages
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IPageCache _pageCache;
        private readonly IPageRepository _pageRepository;
        private readonly IEditionRepository _editionRepository;
        public AddCommandHandler(
            ILogger logger,
            IPageCache pageCache,
            IPageRepository pageRepository,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pageCache = pageCache ?? throw new ArgumentNullException(nameof(pageCache));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add a page titled \"{1}\"",
                    command.CreatedBy,
                    command.Title);
                var page = new Page(
                    createdBy: command.CreatedBy,
                    title: command.Title,
                    summary: command.Summary,
                    body: command.Body,
                    keywords: command.Keywords);
                if (command.Editions.Any())
                {
                    foreach (var id in command.Editions)
                    {
                        var edition = await _editionRepository.GetAsync(
                            iDEdition: id);
                        if (edition != null)
                        {
                            page.BindEdition(
                                edition: edition);
                        }
                    }
                }
                _pageRepository.Add(page);
                var saved = await _pageRepository.UnitOfWork.SaveEntitiesAsync();
                if (saved.Success)
                {
                    _logger.Information("Page with title \"{0}\" saved with id {1}",
                        command.Title, page.ID);
                    response.IDPage = page.ID;
                    response.success = true;
                    response.errors = null;
                    _pageCache.Clear();

                }
                else
                {
                    _logger.Warning("Page {0} not saved",
                        command.Title);
                    response.IDPage = null;
                    response.success = false;
                    response.errors = saved.Errors as List<string>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.IDPage = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}