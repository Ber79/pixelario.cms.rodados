﻿using MediatR;
using Pixelario.CMS.SeedWork;
using Pixelario.CMS.Application.Commands.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;

namespace Pixelario.CMS.Application.CommandHandlers.Roles
{
    public class DeleteCommandHandler :
        IRequestHandler<DeleteCommand, CommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationRoleManager _roleManager;

        public DeleteCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationRoleManager roleManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }
        public async Task<CommandResponse> Handle(
            DeleteCommand command,
            CancellationToken cancellationToken)
        {
            var response = new CommandResponse();
            try
            {
                _logger.Information("{0} starting to delete a role with id {1}",
                    command.SolicitedBy, command.RoleId);
                var role = await _roleManager.FindByIdAsync(command.RoleId);
                if (role != null)
                {
                    if (role.Name.ToLower() != "super administrador")
                    {
                        var saved = await _roleManager.DeleteAsync(
                            role: role);
                        if (saved.Succeeded)
                        {
                            _logger.Warning("Role with id {0} deleted",
                                command.RoleId);
                            response.success = true;
                            response.errors = null;
                        }
                        else
                        {
                            _logger.Warning("Role with id {0} not deleted",
                                command.RoleId);
                            response.success = false;
                            response.errors = saved.Errors.ToList();
                        }
                    }
                    else
                    {
                        _logger.Warning("Role with id {0} not deleted",
                            command.RoleId);
                        response.success = false;
                        response.errors =
                            new List<string>() { "El role super administrador no puede eliminarse." };
                    }
                }
                else
                {
                    _logger.Warning("Role with id {0} not found",
                        command.RoleId);
                    response.success = false;
                    response.errors = new List<string>() { "No se encontró el role buscado." };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}