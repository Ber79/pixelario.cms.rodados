﻿using MediatR;
using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Roles
{
    public class ListCommandHandler : IRequestHandler<ListCommand, List<ApplicationRole>>
    {
        private readonly ILogger _logger;
        private readonly ApplicationRoleManager _roleManager;
        public ListCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationRoleManager roleManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }
        public async Task<List<ApplicationRole>> Handle(ListCommand command,
            CancellationToken cancellationToken)
        {
            switch (command.OrderBy)
            {
                case "Name":
                    return await _roleManager.Roles
                            .OrderBy(x => x.Name)
                            .Skip(command.RowIndex)
                            .Take(command.RowCount)
                            .ToListAsync();
                case "Id":
                default:
                    return await _roleManager.Roles
                            .OrderByDescending(x => x.Id)
                            .Skip(command.RowIndex)
                            .Take(command.RowCount)
                            .ToListAsync();
            }
        }
    }
}