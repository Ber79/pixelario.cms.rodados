﻿using MediatR;
using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Domain;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Roles
{
    public class GetCommandHandler : IRequestHandler<GetCommand, ApplicationRole>
    {
        private readonly ILogger _logger;
        private readonly ApplicationRoleManager _roleManager;
        public GetCommandHandler(
            ILogger logger,
            ApplicationRoleManager roleManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }
        public async Task<ApplicationRole> Handle(GetCommand command,
            CancellationToken cancellationToken)
        {
            return await _roleManager.FindByIdAsync(
                roleId: command.RoleId);
        }
    }
}