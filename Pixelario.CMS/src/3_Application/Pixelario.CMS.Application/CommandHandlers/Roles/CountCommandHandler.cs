﻿using MediatR;
using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Infrastructure.Managers;
using Serilog;
using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.CommandHandlers.Roles
{
    public class CountCommandHandler : IRequestHandler<CountCommand, int>
    {
        private readonly ILogger _logger;
        private readonly ApplicationRoleManager _roleManager;
        public CountCommandHandler(
            ILogger logger,
            ApplicationRoleManager roleManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }
        public async Task<int> Handle(CountCommand command,
            CancellationToken cancellationToken)
        {
            return await _roleManager.Roles.CountAsync();
        }
    }
}