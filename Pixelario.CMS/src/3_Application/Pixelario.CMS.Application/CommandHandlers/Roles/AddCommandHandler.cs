﻿using MediatR;
using Pixelario.CMS.Application.Commands.Roles;
using Pixelario.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.Identity.Infrastructure.Managers;

namespace Pixelario.CMS.Application.CommandHandlers.Roles
{
    public class AddCommandHandler : IRequestHandler<AddCommand, AddCommandResponse>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ApplicationRoleManager _roleManager;

        public AddCommandHandler(
            ILogger logger,
            IMediator mediator,
            ApplicationRoleManager roleManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }
        public async Task<AddCommandResponse> Handle(AddCommand command,
            CancellationToken cancellationToken)
        {
            var response = new AddCommandResponse();
            try
            {
                _logger.Information("{0} starting to add a role with name {1}",
                    command.CreatedBy, command.RoleName);

                var role = new ApplicationRole
                {
                    Name = command.RoleName
                };
                var saved = await _roleManager.CreateAsync(
                    role: role);
                if (saved.Succeeded)
                {
                    _logger.Warning("Role with name {0} added with id {1}",
                        command.RoleName, role.Id);
                    response.ID = role.Id;
                    response.success = true;
                    response.errors = null;
                }
                else
                {
                    _logger.Warning("Role with name {0} not added",
                        command.RoleName);
                    response.ID = null;
                    response.success = false;
                    response.errors = saved.Errors.ToList();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                response.ID = null;
                response.success = false;
                response.errors = new List<string>() { ex.Message };
                if (ex.InnerException != null)
                    response.errors.Add(ex.InnerException.Message);

            }
            return response;
        }
    }
}