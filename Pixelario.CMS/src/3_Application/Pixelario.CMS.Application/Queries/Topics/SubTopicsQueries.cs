﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Topics
{
    public class SubTopicsQueries : ISubTopicsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public SubTopicsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<SubTopicQueryModel> GetAsync(int iDSubTopic)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT s.IDSubTopic AS IDSubTopic, s.CreatedBy AS CreatedBy, 
                    s.CreationDate AS CreationDate, s.IDTopic as IDTopic,
                    s.Title AS Title, 
                    s.Summary AS Summary, s.Keywords AS Keywords, 
                    s.Enabled AS Enabled, s.AtHome AS AtHome, 
                    s.[Order] AS [Order],  s.OnMenu AS OnMenu,
                    s.OrderAtMenu as OrderAtMenu,
                    s.HomeImage AS HomeImage                    
                    FROM SubTopics AS s                                        
                    WHERE s.IDSubTopic = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDSubTopic });
                    return MapFullSubTopic(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private SubTopicQueryModel MapFullSubTopic(dynamic result)
        {
            var subTopic = new SubTopicQueryModel
            {
                IDSubTopic = result[0].IDSubTopic,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                IDTopic = result[0].IDTopic,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Keywords = result[0].Keywords,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                OnMenu = result[0].OnMenu,
                OrderAtMenu = result[0].OrderAtMenu,
                HomeImage = result[0].HomeImage,
            };           
            return subTopic;
        }

        public async Task<int> CountAsync(int? iDTopic, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDTopic, filteredBy, enabled, atHome,
                        onMenu);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM SubTopics AS s "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<SubTopicQueryModel>> ListAsync(int? iDTopic,
            string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDTopic, filteredBy, enabled, atHome, onMenu);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private static string WhereBuilder(int? iDTopic, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu)
        {
            List<string> wheresParaQuery = new List<string>();
            if (iDTopic.HasValue)
                wheresParaQuery.Add(string.Format("s.IDTopic = {0}",
                    iDTopic.Value.ToString()));
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("s.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("s.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("s.Enabled = 1");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    wheresParaQuery.Add("s.AtHome = 1");
                }
                else
                {
                    wheresParaQuery.Add("s.AtHome = 1");
                }
            }
            if (onMenu.HasValue)
            {
                if (onMenu.Value)
                {
                    wheresParaQuery.Add("s.OnMenu = 1");
                }
                else
                {
                    wheresParaQuery.Add("s.OnMenu = 1");
                }
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        private async Task<List<SubTopicQueryModel>> QueryDb(string where,
            int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT s.IDSubTopic AS IDSubTopic, s.CreatedBy AS CreatedBy, 
                    s.CreationDate AS CreationDate, s.Title AS Title, 
                    s.Summary AS Summary, s.Keywords AS Keywords, 
                    s.Enabled AS Enabled, s.AtHome AS AtHome, 
                    s.[Order] AS [Order], s.OnMenu AS OnMenu,
                    s.OrderAtMenu as OrderAtMenu,
                    s.HomeImage AS HomeImage                    
                    FROM SubTopics AS s " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return MapListToTopicsList(result);
            }
        }
        private List<SubTopicQueryModel> MapListToTopicsList(dynamic result)
        {
            var subTopicsList = new List<SubTopicQueryModel>();
            var subTopic = new SubTopicQueryModel()
            {
                IDSubTopic = -1
            };
            foreach (var item in result)
            {
                if (subTopic.IDSubTopic == -1)
                {
                    subTopic = new SubTopicQueryModel()
                    {
                        IDSubTopic = item.IDSubTopic,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu,
                        HomeImage = item.HomeImage
                    };
                }
                else if (subTopic.IDTopic != item.IDTopic)
                {
                    subTopicsList.Add(subTopic);
                    subTopic = new SubTopicQueryModel()
                    {
                        IDSubTopic = item.IDSubTopic,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu,
                        HomeImage = item.HomeImage
                    };
                }               
            }
            if (subTopic.IDTopic != -1 && !subTopicsList.Contains(subTopic))
                subTopicsList.Add(subTopic);
            return subTopicsList;
        }

    }
}