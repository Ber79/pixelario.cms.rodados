﻿using Pixelario.CMS.Application.QueryModels.Topics;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Topics
{
    public interface ITopicsQueries
    {
        /// <summary>
        /// Get a topic by his unique id
        /// </summary>
        /// <param name="iDTopic">unique id of a topic</param>
        /// <returns>View model of a topic</returns>
        Task<TopicQueryModel> GetAsync(int iDTopic);
        /// <summary>
        /// List of topics paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDEdition">unique id of an edition</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="onMenu">on menu status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <param name="rowIndex">row to start listing</param>
        /// <param name="rowCount">number of row to return</param>
        /// <param name="columnOrder">order to list</param>
        /// <param name="orderDirection">direction of de list</param>
        /// <returns>List of topics on view model</returns>
        Task<List<TopicQueryModel>> ListAsync(            
            int? iDEdition, 
            string filteredBy, 
            bool? enabled, 
            bool? atHome,
            bool? onMenu,
            bool? includeEnableSubTopics,
            bool? includeHomePageSubTopics,
            bool? includeOnMenuSubTopics,
            string subTopicColumnOrder,
            string subTopicOrderDirection,
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);
        /// <summary>
        /// Count the number of topics filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDEdition">unique id of an edition</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="onMenu">on menu status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <returns>Number of topics</returns>
        Task<int> CountAsync(int? iDEdition, 
            string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu);
    }
}