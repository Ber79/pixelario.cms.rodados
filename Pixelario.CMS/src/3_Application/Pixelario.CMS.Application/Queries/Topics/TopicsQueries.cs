﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Application.QueryModels.Topics;
using Pixelario.CMS.Application.QueryModels.Editions;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Topics
{
    public class TopicsQueries : ITopicsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public TopicsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<TopicQueryModel> GetAsync(int iDTopic)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT t.IDTopic AS IDTopic, t.CreatedBy AS CreatedBy, 
                    t.CreationDate AS CreationDate, t.Title AS Title, 
                    t.Summary AS Summary, t.Keywords AS Keywords, 
                    t.Enabled AS Enabled, t.AtHome AS AtHome, 
                    t.[Order] AS [Order], t.OnMenu AS OnMenu,
                    t.OrderAtMenu as OrderAtMenu,
                    t.HomeImage AS HomeImage,
                    e.IDEdition as Editions_IDEdition, e.Title as Editions_Title
                    FROM Topics AS t
                    LEFT JOIN TopicEditions as te ON t.IDTopic = te.IDTopic
                    LEFT JOIN Editions as e ON te.IDEdition = e.IDEdition
                    WHERE t.IDTopic = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDTopic });
                    return MapFullTopic(result);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private TopicQueryModel MapFullTopic(dynamic result)
        {
            var topic = new TopicQueryModel
            {
                IDTopic = result[0].IDTopic,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Keywords = result[0].Keywords,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                HomeImage = result[0].HomeImage,
                Editions = new List<EditionQueryModel>()
            };
            foreach (dynamic item in result)
            {
                if (!(item.Editions_IDEdition is null))
                {
                    var edition = new EditionQueryModel
                    {
                        IDEdition = item.Editions_IDEdition,
                        Title = item.Editions_Title
                    };
                    topic.Editions.Add(edition);
                }
            }
            return topic;
        }

        public async Task<int> CountAsync(int? iDEdicion,
             string filteredBy, bool? enabled, bool? atHome,
             bool? onMenu)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDEdicion, filteredBy,
                        enabled, atHome, onMenu);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Topics AS t "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<TopicQueryModel>> ListAsync(int? iDEdicion,
            string filteredBy, bool? enabled, bool? atHome,
            bool? onMenu,
            bool? includeEnableSubTopics,
            bool? includeHomePageSubTopics,
            bool? includeOnMenuSubTopics,
            string subTopicColumnOrder,
            string subTopicOrderDirection,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDEdicion, filteredBy,
                    enabled, atHome, onMenu);
                var subTopicDictionary = SubTopicBuilder(
                    includeEnableSubTopics,
                    includeHomePageSubTopics,
                    includeOnMenuSubTopics,
                    subTopicColumnOrder,
                    subTopicOrderDirection);
                return await QueryDb(subTopicDictionary, where, rowIndex, rowCount,
                    columnOrder, orderDirection);

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private Dictionary<string, string> SubTopicBuilder(bool? includeEnableSubTopics,
            bool? includeHomePageSubTopics, bool? includeOnMenuSubTopics,
            string subTopicColumnOrder,
            string subTopicOrderDirection)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if ((includeEnableSubTopics.HasValue && includeEnableSubTopics.Value) ||
                (includeHomePageSubTopics.HasValue && includeHomePageSubTopics.Value) ||
                (includeOnMenuSubTopics.HasValue && includeOnMenuSubTopics.Value))
            {

                var subQueryWhereList = new List<string>();
                if (includeEnableSubTopics.HasValue && includeEnableSubTopics.Value)
                {
                    subQueryWhereList.Add("Enabled = 1");
                }
                if (includeHomePageSubTopics.HasValue && includeHomePageSubTopics.Value)
                {
                    subQueryWhereList.Add("AtHome = 1");
                }
                if (includeOnMenuSubTopics.HasValue && includeOnMenuSubTopics.Value)
                {
                    subQueryWhereList.Add("OnMenu = 1");
                }
                var joinSubTopicQuery = string.Format("SELECT IDSubTopic AS IDSubTopic, IDTopic as IDTopic, Title AS Title, {0} AS SubTopic_Order FROM SubTopics WHERE {1}",
                    !string.IsNullOrEmpty(subTopicColumnOrder) ? subTopicColumnOrder : "[Order]",
                    String.Join(" AND ", subQueryWhereList));
                var subTopicsFields = ", st.IDSubTopic as IDSubTopic, st.Title as SubTopicTitle";
                if (!string.IsNullOrEmpty(subTopicColumnOrder) && !string.IsNullOrEmpty(subTopicOrderDirection))
                {
                    subTopicsFields += ", st.SubTopic_Order AS SubTopic_Order";
                    dictionary.Add("order",
                        ", SubTopic_Order " + subTopicOrderDirection);
                }
                dictionary.Add("fields", subTopicsFields);
                dictionary.Add("join", string.Format("LEFT JOIN ({0}) as st ON st.IDTopic = t.IDTopic ",
                    joinSubTopicQuery));
            }
            else
            {
                dictionary.Add("join", "");
                dictionary.Add("fields", "");
                dictionary.Add("order", "");
            }
            return dictionary;
        }

        private static string WhereBuilder(int? iDEdicion, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu)
        {
            List<string> wheresParaQuery = new List<string>();
            if (iDEdicion.HasValue)
                wheresParaQuery.Add(string.Format("t.IDEdition = {0}",
                    iDEdicion.Value.ToString()));
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("t.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("t.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("t.Enabled = 1");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    wheresParaQuery.Add("t.AtHome = 1");
                }
                else
                {
                    wheresParaQuery.Add("t.AtHome = 1");
                }
            }
            if (onMenu.HasValue)
            {
                if (onMenu.Value)
                {
                    wheresParaQuery.Add("t.OnMenu = 1");
                }
                else
                {
                    wheresParaQuery.Add("t.OnMenu = 0");
                }
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        private async Task<List<TopicQueryModel>> QueryDb(Dictionary<string, string> subTopicDictionary,
            string where, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT t.IDTopic AS IDTopic, t.CreatedBy AS CreatedBy, 
                    t.CreationDate AS CreationDate, t.Title AS Title, 
                    t.Summary AS Summary, t.Keywords AS Keywords, 
                    t.Enabled AS Enabled, t.AtHome AS AtHome, 
                    t.[Order] AS [Order],  t.OnMenu AS OnMenu,
                    t.OrderAtMenu as OrderAtMenu,
                    t.HomeImage AS HomeImage " +
                    subTopicDictionary["fields"] +
                    " FROM (    SELECT * FROM Topics AS t "
                                + where + " ORDER BY " + columnOrder + " " + orderDirection +
                                @" OFFSET @rowIndex ROWS       
                                FETCH NEXT @rowCount ROWS ONLY
                    ) AS t " + subTopicDictionary["join"] +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    subTopicDictionary["order"];
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query, new
                {
                    rowIndex,
                    rowCount
                });
                return MapListToTopicsList(result);
            }
        }

        private List<TopicQueryModel> MapListToTopicsList(dynamic result)
        {
            var topicsList = new List<TopicQueryModel>();
            var topic = new TopicQueryModel()
            {
                IDTopic = -1
            };
            foreach (var item in result)
            {
                if (topic.IDTopic == -1)
                {
                    topic = new TopicQueryModel()
                    {
                        IDTopic = item.IDTopic,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        HomeImage = item.HomeImage,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu
                    };
                }
                else if (topic.IDTopic != item.IDTopic)
                {
                    topicsList.Add(topic);
                    topic = new TopicQueryModel()
                    {
                        IDTopic = item.IDTopic,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        HomeImage = item.HomeImage,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu

                    };
                }
                if (item.IDSubTopic != null)
                {
                    topic.SubTopics.Add(
                        new SubTopicQueryModel()
                        {
                            IDSubTopic = item.IDSubTopic,
                            Title = item.SubTopicTitle
                        });
                }
            }
            if (topic.IDTopic != -1 && !topicsList.Contains(topic))
                topicsList.Add(topic);
            return topicsList;
        }
    }
}