﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Editions
{
    public interface IEditionsQueries
    {
        Task<EditionQueryModel> GetAsync(int iDEdition);
        Task<List<EditionQueryModel>> ListAsync(            
            string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes,
            PagesOnEditionQueryFilter pagesOnEditionFilter,
            int rowIndex, int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes);
    }
}