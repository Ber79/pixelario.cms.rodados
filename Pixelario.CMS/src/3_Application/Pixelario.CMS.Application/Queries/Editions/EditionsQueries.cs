﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.QueryFilters.Editions;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Editions
{
    public class EditionsQueries : IEditionsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public EditionsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<EditionQueryModel> GetAsync(int iDEdition)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT e.IDEdition AS IDEdition, e.CreatedBy AS CreatedBy, 
                    e.CreationDate AS CreationDate, e.Title AS Title, 
                    e.Summary AS Summary, e.Keywords AS Keywords, 
                    e.Enabled AS Enabled, e.AtHome AS AtHome, 
                    e.[Order] AS [Order], e.EditionType as EditionType,
                    e.HomeImage AS HomeImage, e.OnMenu AS OnMenu,
	                e.OrderAtMenu AS OrderAtMenu,
                    t.IDTopic AS Topics_IDTopic, t.Title AS Topics_Title,
                    s.IDSubTopic AS SubTopics_IDSubTopic, s.Title AS SubTopics_Title
                    FROM Editions AS e
                    LEFT JOIN TopicEditions AS te ON e.IDEdition = te.IDEdition
                    LEFT JOIN (
                        SELECT IDTopic, Title
                        FROM Topics
                        WHERE Enabled = 1) AS t ON te.IDTopic = t.IDTopic
                    LEFT JOIN (
                        SELECT IDSubTopic, IDTopic, Title
                        FROM SubTopics
                        WHERE Enabled = 1) AS s ON s.IDTopic = t.IDTopic
                    WHERE e.IDEdition = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                        new { id = iDEdition });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapFullEdition(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }


        private EditionQueryModel MapFullEdition(dynamic result)
        {
            var edition = new EditionQueryModel
            {
                IDEdition = result[0].IDEdition,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Keywords = result[0].Keywords,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,                
                OnMenu = result[0].OnMenu,
                OrderAtMenu = result[0].OrderAtMenu,
                IDEditionType = result[0].EditionType,
                HomeImage = result[0].HomeImage,
                Topics = new List<TopicQueryModel>()
            };
            int actualIDTopic = 0;
            TopicQueryModel topic = null;
            foreach (dynamic item in result)
            {
                if (!(item.Topics_IDTopic is null))
                {
                    if (item.Topics_IDTopic != actualIDTopic)
                    {
                        if (actualIDTopic != 0)
                            edition.Topics.Add(topic);
                        actualIDTopic = item.Topics_IDTopic;
                        topic = new TopicQueryModel
                        {
                            IDTopic = item.Topics_IDTopic,
                            Title = item.Topics_Title,
                            SubTopics = new List<SubTopicQueryModel>()
                        };
                    }
                    if (!(item.SubTopics_IDSubTopic is null))
                    {
                        var subTopic = new SubTopicQueryModel
                        {
                            IDSubTopic = item.SubTopics_IDSubTopic,
                            Title = item.SubTopics_Title
                        };
                        topic.SubTopics.Add(subTopic);
                    }
                }
            }
            if (topic != null)
                edition.Topics.Add(topic);
            return edition;
        }


        public async Task<int> CountAsync(string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes)
        {
            _logger.Debug("Starting to query");
            try
            {
                List<string> whereStatments = new List<string>();
                if (!string.IsNullOrEmpty(filteredBy))
                    whereStatments.Add(string.Format("e.Title Like '{0}%'",
                        filteredBy));
                if (enabled.HasValue)
                {
                    if (enabled.Value)
                    {
                        whereStatments.Add("e.Enabled = 1");
                    }
                    else
                    {
                        whereStatments.Add("e.Enabled = 0");
                    }
                }
                if (atHome.HasValue)
                {
                    if (atHome.Value)
                    {
                        whereStatments.Add("e.AtHome = 1");
                    }
                    else
                    {
                        whereStatments.Add("e.AtHome = 0");
                    }
                }
                if (onMenu.HasValue)
                {
                    if (onMenu.Value)
                    {
                        whereStatments.Add("e.OnMenu = 1");
                    }
                    else
                    {
                        whereStatments.Add("e.OnMenu = 0");
                    }
                }
                if (editionTypes != null && editionTypes.Count > 0)
                {
                    var editionTypeWhere = new List<string>();
                    foreach (var editionType in editionTypes)
                    {
                        editionTypeWhere.Add($"e.EditionType = {editionType.Id}");
                    }
                    whereStatments.Add(string.Format("({0})",
                        string.Join(" OR ", editionTypeWhere)));
                }
                string where = "";
                if (whereStatments.Any())
                {
                    where = "WHERE " + String.Join(" AND ", whereStatments);
                }
                IEnumerable<int> result;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Editions AS e "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        public async Task<List<EditionQueryModel>> ListAsync(
            string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes,
            PagesOnEditionQueryFilter pagesOnEditionFilter,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string whereForEditions = WhereForEditionsBuilder(filteredBy: filteredBy,
                    enabled: enabled, atHome: atHome,
                    onMenu: onMenu,
                    editionTypes: editionTypes);
                string joinForPages = null;
                if (pagesOnEditionFilter != null)
                    joinForPages = pagesOnEditionFilter.GetJoin();
                string columnsForPages = null;
                if (pagesOnEditionFilter != null)
                    columnsForPages = pagesOnEditionFilter.GetColumns();
                return await QueryDb(whereForEditions,
                    columnsForPages, joinForPages,
                    rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private string WhereForEditionsBuilder(string filteredBy,
            bool? enabled, bool? atHome,
            bool? onMenu,
            List<EditionType> editionTypes)
        {
            List<string> whereStatments = new List<string>();
            if (!string.IsNullOrEmpty(filteredBy))
                whereStatments.Add(string.Format("e.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    whereStatments.Add("e.Enabled = 1");
                }
                else
                {
                    whereStatments.Add("e.Enabled = 0");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    whereStatments.Add("e.AtHome = 1");
                }
                else
                {
                    whereStatments.Add("e.AtHome = 0");
                }
            }
            if (onMenu.HasValue)
            {
                if (onMenu.Value)
                {
                    whereStatments.Add("e.OnMenu = 1");
                }
                else
                {
                    whereStatments.Add("e.OnMenu = 0");
                }
            }
            if (editionTypes != null && editionTypes.Count > 0)
            {
                var editionTypeWhere = new List<string>();
                foreach (var editionType in editionTypes)
                {
                    editionTypeWhere.Add($"e.EditionType = {editionType.Id}");
                }
                whereStatments.Add(string.Format("({0})",
                    string.Join(" OR ", editionTypeWhere)));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }

            return where;
        }

        private async Task<List<EditionQueryModel>> QueryDb(
            string whereForEditions, string columnsForPages,
            string joinForPages,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query =
                    @"SELECT e.IDEdition AS IDEdition, e.CreatedBy AS CreatedBy, 
                    e.CreationDate AS CreationDate, e.Title AS Title, 
                    e.Summary AS Summary, e.Keywords AS Keywords, 
                    e.Enabled AS Enabled, e.AtHome AS AtHome, 
                    e.[Order] AS [Order], e.EditionType as EditionType, e.OnMenu AS OnMenu,
	                e.OrderAtMenu AS OrderAtMenu,
                    e.HomeImage AS HomeImage" + columnsForPages +
                    @" FROM (
                        SELECT * FROM Editions AS e " + whereForEditions +
                        " ORDER BY " + columnOrder + " " + orderDirection +
                        @" OFFSET @rowIndex ROWS       
                        FETCH NEXT @rowCount ROWS ONLY
                    ) AS e " + joinForPages +
                    " ORDER BY " + columnOrder + " " + orderDirection;
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });


                if (result == null)
                    return new List<EditionQueryModel>();
                var editionList = new List<EditionQueryModel>();
                foreach(var register in result)
                {
                    if (!editionList.Any())
                        editionList.Add(new EditionQueryModel(
                            mappingMode: string.IsNullOrEmpty(columnsForPages) ?
                                EditionMappingMode.ListMapping : EditionMappingMode.ListWithPageMapping,
                            editionRegister: register));
                    else
                    {
                        var index = editionList.IndexOf(
                            editionList.Find(e => e.IDEdition == register.IDEdition));
                        if(index >= 0)
                        {
                            editionList[index].AddPage(
                                editionRegister: register);
                        }
                        else
                        {
                            editionList.Add(new EditionQueryModel(
                                mappingMode: string.IsNullOrEmpty(columnsForPages) ?
                                    EditionMappingMode.ListMapping : EditionMappingMode.ListWithPageMapping,
                                editionRegister: register));
                        }
                    }
                }
                return editionList;// MapDynamicsListToEditiosnList(result);
            }
        }

        private List<EditionQueryModel> MapDynamicsListToEditiosnList(dynamic result)
        {
            var editionsList = new List<EditionQueryModel>();
            var edition = new EditionQueryModel()
            {
                IDEdition = -1
            };
            foreach (var item in result)
            {
                if (edition.IDEdition == -1)
                {
                    edition = new EditionQueryModel()
                    {
                        IDEdition = item.IDEdition,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu,
                        IDEditionType = item.EditionType,
                        HomeImage = item.HomeImage
                    };
                }
                else if (edition.IDEdition != item.IDEdition)
                {
                    editionsList.Add(edition);
                    edition = new EditionQueryModel()
                    {
                        IDEdition = item.IDEdition,
                        CreatedBy = item.CreatedBy,
                        CreationDate = item.CreationDate,
                        Title = item.Title,
                        Summary = item.Summary,
                        Keywords = item.Keywords,
                        Enabled = item.Enabled,
                        AtHome = item.AtHome,
                        Order = item.Order,
                        OnMenu = item.OnMenu,
                        OrderAtMenu = item.OrderAtMenu,
                        IDEditionType = EditionType.From(item.EditionType),
                        HomeImage = item.HomeImage
                    };
                }
                if (item.IDPage != null)
                {
                    edition.Pages.Add(
                        new PageQueryModel()
                        {
                            IDPage = item.IDPage,
                            Title = item.PageTitle
                        });
                }
            }
            if (edition.IDEdition != -1 && !editionsList.Contains(edition))
                editionsList.Add(edition);
            return editionsList;
        }

    }
}