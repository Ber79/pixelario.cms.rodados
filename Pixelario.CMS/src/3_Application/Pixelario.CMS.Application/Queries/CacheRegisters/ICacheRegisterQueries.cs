﻿using System.Collections.Generic;

namespace Pixelario.CMS.Application.Queries.CacheRegisters
{
    public interface ICacheRegisterQueries
    {
        List<KeyValuePair<string, object>> List(
            int rowIndex, int rowCount);
        int Count();
    }
}