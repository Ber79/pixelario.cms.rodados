﻿using Pixelario.CMS.Application.Queries.CacheRegisters;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Pixelario.CMS.Application.Queries.CacheRegisters
{
    public class CacheRegisterQueries : ICacheRegisterQueries
    {
        private readonly ILogger _logger;
        private MemoryCache _cache;
        public CacheRegisterQueries(
            ILogger logger,
            MemoryCache cache)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }
        public int Count()
        {
            _logger.Debug("Starting to query");
            try
            {
                return _cache.Count();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public List<KeyValuePair<string, object>> List(
            int rowIndex, int rowCount)
        {
            _logger.Debug("Starting to query");
            try
            {
                var list = _cache.ToList();
                return list.Skip(rowIndex).Take(rowCount).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
    }
}