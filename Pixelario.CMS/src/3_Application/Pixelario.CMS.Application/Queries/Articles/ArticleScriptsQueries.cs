﻿using Dapper;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public class ArticleScriptsQueries : IArticleScriptsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;

        public ArticleScriptsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<int> CountAsync(string filteredBy, bool? enabled)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(filteredBy,
                    enabled);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"
                    SELECT  COUNT(*)
                    FROM    ArticleScripts AS ars "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        private string WhereBuilder(string filteredBy, bool? enabled)
        {
            List<string> wheresParaQuery = new List<string>();
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("ars.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("ars.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("ars.Enabled = 1");
                }
            }          
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        public async Task<ArticleScriptQueryModel> GetAsync(int iDArticleScript)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"
                    SELECT  ars.IDArticleScript AS IDArticleScript, 
                            ars.CreatedBy AS CreatedBy, ars.CreationDate AS CreationDate, 
                            ars.Title AS Title, ars.Summary AS Summary, 
                            ars.Enabled AS Enabled, ars.[Order] as [Order]
                    FROM ArticleScripts AS ars 
                    WHERE ars.IDArticleScript =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDArticleScript });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapSingle(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private ArticleScriptQueryModel MapSingle(dynamic result)
        {
            var multimedia = new ArticleScriptQueryModel
            {
                IDArticleScript = result[0].IDArticleScript,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Enabled = result[0].Enabled,
                Order = result[0].Order,
            };
            return multimedia;
        }

        public async Task<List<ArticleScriptQueryModel>> ListAsync(string filteredBy, bool? enabled, 
            int rowIndex, int rowCount, 
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(filteredBy, enabled);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private async Task<List<ArticleScriptQueryModel>> QueryDb(string where, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"
                SELECT  ars.IDArticleScript AS IDArticleScript, 
                        ars.CreatedBy AS CreatedBy, ars.CreationDate AS CreationDate, 
                        ars.Title AS Title, ars.Summary AS Summary, 
                        ars.Enabled AS Enabled,
                        ars.[Order] AS [Order]
                    FROM ArticleScripts AS ars " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return MapList(result.ToList());
            }
        }

        private List<ArticleScriptQueryModel> MapList(dynamic result)
        {
            var list = new List<ArticleScriptQueryModel>();
            foreach (var item in result)
            {
                list.Add(new ArticleScriptQueryModel
                {
                    IDArticleScript = item.IDArticleScript,
                    CreatedBy = item.CreatedBy,
                    CreationDate = item.CreationDate,
                    Title = item.Title,
                    Summary = item.Summary,
                    Enabled = item.Enabled,
                    Order = item.Order
                });
            }

            return list;
        }
    }
}
