﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Application.QueryModels.Articles;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public interface IArticlesQueries
    {
        /// <summary>
        /// Get an article by his unique id
        /// </summary>
        /// <param name="idArticle">unique id of an article</param>
        /// <returns>View model of an article</returns>
        Task<ArticleQueryModel> GetAsync(int iDArticle);
        /// <summary>
        /// List of articles paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="idEdicion">unique id of an edition</param>
        /// <param name="idTema">unique id of a topic</param>
        /// <param name="idSubtema">unique id of a subtopic</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <param name="rowIndex">row to start listing</param>
        /// <param name="rowCount">number of row to return</param>
        /// <param name="columnOrder">order to list</param>
        /// <param name="orderDirection">direction of de list</param>
        /// <returns>List of articles on view model</returns>
        Task<List<ArticleQueryModel>> ListAsync(            
            int? iDEdition, int? iDTopic,
            int? iDSubTopic, 
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome, string filteredBy, 
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);
        /// <summary>
        /// Count the number of articles filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDEdition">unique id of an edition</param>
        /// <param name="iDTopic">unique id of a topic</param>
        /// <param name="iDSubTopic">unique id of a subtopic</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <returns>Number of articles</returns>
        Task<int> CountAsync(int? iDEdition, int? iDTopic,
            int? iDSubTopic,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome, string filteredBy);
    }
}