﻿using Dapper;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public class ArticleScriptItemsQueries : IArticleScriptItemsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;

        public ArticleScriptItemsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<int> CountAsync(
            int iDArticleScript, int? iDMultimediaType,
            string filteredBy, bool? enabled)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(
                    iDArticleScript, iDMultimediaType,
                    filteredBy,
                    enabled);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"
                    SELECT  COUNT(*)
                    FROM    ArticleScriptItems AS asi "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        private string WhereBuilder(int iDArticleScript, int? iDMultimediaType, string filteredBy, bool? enabled)
        {
            List<string> wheresParaQuery = new List<string>();
            wheresParaQuery.Add(string.Format("asi.IDArticleScript = {0}",
                   iDArticleScript));
            if (iDMultimediaType.HasValue)
                wheresParaQuery.Add(string.Format("asi.IDMultimediaType = {0}",
                    iDMultimediaType.Value));
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("asi.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("asi.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("asi.Enabled = 1");
                }
            }          
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        public async Task<ArticleScriptItemQueryModel> GetAsync(int iDArticleScriptItem)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"
                    SELECT  asi.IDArticleScriptItem AS IDArticleScriptItem, 
                            asi.CreatedBy AS CreatedBy, asi.CreationDate AS CreationDate,
                            ars.IDArticleScript AS IDArticleScript,
                            ars.Title AS ArticleScriptTitle,
                            asi.IDMultimediaType as IDMultimediaType,
                            asi.Title AS Title, asi.Code AS Code, 
                            asi.Enabled AS Enabled
                    FROM ArticleScriptItems AS asi 
                    INNER JOIN ArticleScripts AS ars ON asi.IDArticleScript = ars.IDArticleScript
                    WHERE asi.IDArticleScriptItem =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDArticleScriptItem });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapSingle(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private ArticleScriptItemQueryModel MapSingle(dynamic result)
        {
            var multimedia = new ArticleScriptItemQueryModel
            {
                IDArticleScriptItem = result[0].IDArticleScriptItem,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                MultimediaType = MultimediaType.From(result[0].IDMultimediaType),
                Title = result[0].Title,
                Code = result[0].Code,
                Enabled = result[0].Enabled,
                ArticleScript = new ArticleScriptQueryModel()
                {
                    IDArticleScript = result[0].IDArticleScript,
                    Title = result[0].ArticleScriptTitle
                }
            };
            return multimedia;
        }

        public async Task<List<ArticleScriptItemQueryModel>> ListAsync(
            int iDArticleScript, int? iDMultimediaType,
            string filteredBy, bool? enabled, 
            int rowIndex, int rowCount, 
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(
                    iDArticleScript, iDMultimediaType,
                    filteredBy, enabled);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private async Task<List<ArticleScriptItemQueryModel>> QueryDb(string where, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"
                SELECT  asi.IDArticleScriptItem AS IDArticleScriptItem, 
                        asi.CreatedBy AS CreatedBy, asi.CreationDate AS CreationDate, 
                        asi.IDMultimediaType as IDMultimediaType,
                        asi.Title AS Title, asi.Code AS Code, 
                        asi.Enabled AS Enabled,
                        asi.[Order] AS [Order]
                    FROM ArticleScriptItems AS asi " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return MapList(result.ToList());
            }
        }

        private List<ArticleScriptItemQueryModel> MapList(dynamic result)
        {
            var list = new List<ArticleScriptItemQueryModel>();
            foreach (var item in result)
            {
                list.Add(new ArticleScriptItemQueryModel
                {
                    IDArticleScriptItem = item.IDArticleScriptItem,
                    CreatedBy = item.CreatedBy,
                    CreationDate = item.CreationDate,
                    MultimediaType = MultimediaType.From(result[0].IDMultimediaType),
                    Title = item.Title,
                    Code = item.Code,
                    Enabled = item.Enabled,
                    Order = item.Order
                });
            }

            return list;
        }
    }
}
