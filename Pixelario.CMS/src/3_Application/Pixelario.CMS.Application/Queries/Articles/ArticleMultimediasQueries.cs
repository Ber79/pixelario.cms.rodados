﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Application.QueryModels.Articles;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public class ArticleMultimediasQueries : IArticleMultimediasQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public ArticleMultimediasQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<ArticleMultimediaQueryModel> GetAsync(int iDMultimedia)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT m.IDMultimedia AS IDMultimedia, 
                    m.CreatedBy AS CreatedBy, m.CreationDate AS CreationDate, 
                    m.IDMultimediaType AS IDMultimediaType,
                    m.Title AS Title, m.Summary AS Summary, 
                    m.Path1 AS Path1, m.Path2 AS Path2, 
                    m.Enabled AS Enabled, m.AtHome AS AtHome, 
                    m.[Order] AS [Order], m.Cover AS Cover,
                    m.IDArticle AS IDArticle, a.Title as Article_Title,
                    FROM ArticleMultimedia AS m 
                    INNER JOIN Article AS a on a.IDArticle = m.IDArticle
                    WHERE m.IDMultimedia =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDMultimedia });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapDynamicToMultimedia(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private ArticleMultimediaQueryModel MapDynamicToMultimedia(dynamic result)
        {
            var multimedia = new ArticleMultimediaQueryModel
            {
                IDMultimedia = result[0].IDMultimedia,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                IDMultimediaType = result[0].IDMultimediaType,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Path1 = result[0].Path1,
                Path2 = result[0].Path2,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                AtCover = result[0].AtCover,
                Article = new ArticleQueryModel()
                {
                    IDArticle = result[0].IDArticle,
                    Title = result[0].Article_Title,
                }
            };
            return multimedia;
        }

        private List<ArticleMultimediaQueryModel> MapDynamicToMultimediaList(dynamic result)
        {
            var multimedia = new List<ArticleMultimediaQueryModel>();
            foreach (var item in result)
            {
                multimedia.Add(new ArticleMultimediaQueryModel
                {
                    IDMultimedia = item.IDMultimedia,
                    CreatedBy = item.CreatedBy,
                    CreationDate = item.CreationDate,
                    IDMultimediaType = item.IDMultimediaType,
                    Title = item.Title,
                    Summary = item.Summary,
                    Path1 = item.Path1,
                    Path2 = item.Path2,
                    Enabled = item.Enabled,
                    AtHome = item.AtHome,
                    Order = item.Order,
                    AtCover = item.AtCover,
                    Article = new ArticleQueryModel()
                    {
                        IDArticle = item.IDArticle,
                        Title = item.Article_Title,
                    }
                });
            }

            return multimedia;
        }

        public async Task<int> CountAsync(int? iDArticle, int[] iDMultimediaTypes,
            bool? enabled, bool? atHome, string filteredBy)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDArticle, iDMultimediaTypes,
                    enabled, atHome, filteredBy);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Articles AS a "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<ArticleMultimediaQueryModel>> ListAsync(
            int? iDArticle, int[] iDMultimediaTypes,
            bool? enabled, bool? atHome, string filteredBy,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDArticle, iDMultimediaTypes, enabled, atHome, filteredBy);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private static string WhereBuilder(int? iDArticle,
            int[] iDMultimediaTypes,
            bool? enabled, bool? atHome, string filteredBy)
        {
            List<string> wheresParaQuery = new List<string>();
            if (iDArticle.HasValue)
                wheresParaQuery.Add(string.Format("m.IDArticle = {0}",
                    iDArticle.Value.ToString()));
            if (iDMultimediaTypes.Length > 0)
            {
                List<string> orMultimediaQuery = new List<string>();
                foreach (var iDMultimediaType in iDMultimediaTypes)
                {
                    orMultimediaQuery.Add(string.Format("m.IDMultimediaType = {0}",
                        iDMultimediaType.ToString()));
                }
                if (orMultimediaQuery.Any())
                {
                    wheresParaQuery.Add( "(" + String.Join(" OR ", orMultimediaQuery) + ")");
                }
            }
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("m.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("m.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("m.Enabled = 1");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    wheresParaQuery.Add("m.AtHome = 1");
                }
                else
                {
                    wheresParaQuery.Add("m.AtHome = 1");
                }
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        private async Task<List<ArticleMultimediaQueryModel>> QueryDb(string where, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT m.IDMultimedia AS IDMultimedia, 
                    m.CreatedBy AS CreatedBy, m.CreationDate AS CreationDate, 
                    m.IDMultimediaType AS IDMultimediaType,
                    m.Title AS Title, m.Summary AS Summary, 
                    m.Path1 AS Path1, m.Path2 AS Path2, 
                    m.Enabled AS Enabled, m.AtHome AS AtHome, 
                    m.[Order] AS [Order], m.Cover AS AtCover,
                    m.IDArticle AS IDArticle, a.Title as Article_Title
                    FROM ArticleMultimedia AS m 
                    INNER JOIN Articles AS a on a.IDArticle = m.IDArticle " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return MapDynamicToMultimediaList(result.ToList());
            }
        }
    }
}