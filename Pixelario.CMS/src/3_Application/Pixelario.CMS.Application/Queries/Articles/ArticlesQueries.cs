﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.QueryModels.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Topics;
using Serilog;
using System.Collections;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public class ArticlesQueries : IArticlesQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public ArticlesQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<ArticleQueryModel> GetAsync(int iDArticle)
        {
            _logger.Debug("Starting to query");

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT a.IDArticle AS IDArticle, a.CreatedBy AS CreatedBy, 
                    a.CreationDate AS CreationDate, a.PublicationDate AS PublicationDate, 
                    a.Title AS Title, 
                    a.Summary AS Summary, a.Body AS Body, a.Source AS Source,
                    a.Keywords AS Keywords, 
                    a.Enabled AS Enabled, a.AtHome AS AtHome, 
                    a.[Order] AS [Order], a.OrderAtEdition AS OrderAtEdition,
                    a.IDPlaceAtHome AS IDPlaceAtHome,
                    a.HomeImage AS HomeImage, a.SlideImage AS SlideImage,
                    a.VerticalImage AS VerticalImage, a.HorizontalImage AS HorizontalImage,
                    a.IDEdition AS IDEdition, e.Title AS Edition_Title,
                    e.EditionType AS Edition_EditionType,
                    uts.IDTopic AS Topics_IDTopic, uts.TopicTitle AS Topics_Title,
                    uts.IDSubTopic AS SubTopics_IDSubTopic, uts.SubTopicTitle AS SubTopics_Title,
                    uts.IDArticleScript AS Scripts_IDArticleScript, uts.ScriptTitle AS Scripts_Title
                    FROM Articles AS a 
                    INNER JOIN Editions AS e on a.IDEdition = e.IDEdition
                    LEFT JOIN (
                        (SELECT at.IDArticle AS IDArticle, t.IDTopic AS IDTopic,
						NULL AS IDSubTopic, t.Title AS TopicTitle, NULL AS SubTopicTitle, 
						NULL AS IDArticleScript, NULL AS ScriptTitle
						FROM ArticleTopics AS at
						INNER JOIN Topics AS t ON at.IDTopic = t.IDTopic
						WHERE at.IDArticle = @id)
						UNION
						(SELECT ast.IDArticle AS IDArticle, NULL AS IDTopic, 
						ast.IDSubTopic AS IDSubTopic,
						NULL AS TopicTitle, s.Title AS SubTopicTitle, 
						NULL AS IDArticleScript, NULL AS ScriptTitle
						FROM ArticleSubTopics AS ast
						INNER JOIN SubTopics AS s ON ast.IDSubTopic = s.IDSubTopic
						WHERE ast.IDArticle = @id)
						UNION
						(SELECT aas.IDArticle AS IDArticle, NULL AS IDTopic, 
						NULL AS IDSubTopic,
						NULL AS TopicTitle, NULL AS SubTopicTitle, 
						aas.IDArticleScript AS IDArticleScript, ars.Title AS ScriptTitle
						FROM ArticleArticleScripts AS aas
						INNER JOIN ArticleScripts AS ars ON aas.IDArticleScript = ars.IDArticleScript
						WHERE aas.IDArticle = @id)
                    ) AS uts ON a.IDArticle = uts.IDArticle
                    WHERE a.IDArticle =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDArticle });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapFullArticle(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private ArticleQueryModel MapFullArticle(dynamic result)
        {
            var article = new ArticleQueryModel
            {
                IDArticle = result[0].IDArticle,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                PublicationDate = result[0].PublicationDate,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Body = result[0].Body,
                Source = result[0].Source,
                Keywords = result[0].Keywords,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                PlaceAtHome = ArticlePlaceAtHome.From(
                    result[0].IDPlaceAtHome),
                HomeImage = result[0].HomeImage,
                SlideImage = result[0].SlideImage,
                HorizontalImage = result[0].HorizontalImage,
                VerticalImage = result[0].VerticalImage,
                Edition = new EditionQueryModel()
                {
                    IDEdition = result[0].IDEdition,
                    Title = result[0].Edition_Title,
                    IDEditionType = result[0].Edition_EditionType
                },
                Topics = new List<TopicQueryModel>(),
                SubTopics = new List<SubTopicQueryModel>(),
                ArticleScripts = new List<ArticleScriptQueryModel>()
            };
            foreach (dynamic item in result)
            {
                if (!(item.Topics_IDTopic is null))
                {
                    var topic = new TopicQueryModel
                    {
                        IDTopic = item.Topics_IDTopic,
                        Title = item.Topics_Title
                    };
                    article.Topics.Add(topic);
                }
                if (!(item.SubTopics_IDSubTopic is null))
                {
                    var subTopic = new SubTopicQueryModel
                    {
                        IDSubTopic = item.SubTopics_IDSubTopic,
                        Title = item.SubTopics_Title
                    };
                    article.SubTopics.Add(subTopic);
                }
                if (!(item.Scripts_IDArticleScript is null))
                {
                    var script = new ArticleScriptQueryModel
                    {
                        IDArticleScript = item.Scripts_IDArticleScript,
                        Title = item.Scripts_Title
                    };
                    article.ArticleScripts.Add(script);
                }
            }
            return article;
        }
        public async Task<int> CountAsync(int? iDEdition, int? iDTopic, int? iDSubTopic,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome, string filteredBy)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchTextList = new List<string>();
                string innerJoinsOnArticles = innerJoinQueryBuilder(
                    iDTopic, iDSubTopic);
                string where = WhereOnlyOnArticlesBuilder(iDTopic, iDSubTopic,
                    iDEdition, enabled,
                    atHome, placesAtHome, filteredBy, out searchTextList);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Articles AS a " + innerJoinsOnArticles + " " + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query, new
                    {
                        searchText = searchTextList.Count > 0 ? searchTextList[0] : "",
                    });
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<ArticleQueryModel>> ListAsync(int? iDEdition,
            int? iDTopic, int? iDSubTopic,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome, string filteredBy,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchTextList = new List<string>();
                string innerJoinsOnArticles = innerJoinQueryBuilder(
                    iDTopic, iDSubTopic);
                string whereOnlyOnArticles = WhereOnlyOnArticlesBuilder(iDTopic, iDSubTopic,
                    iDEdition, enabled, atHome, placesAtHome, filteredBy, out searchTextList);
                string whereFullQuery = WhereFullQueryBuilder(
                    iDTopic, iDSubTopic);
                return await QueryDb(innerJoinsOnArticles, 
                    whereOnlyOnArticles, whereFullQuery,
                    rowIndex, rowCount, searchTextList,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private static string innerJoinQueryBuilder(int? iDTopic, int? iDSubTopic)
        {
            List<string> innerJoinStatments = new List<string>();
            if (iDTopic.HasValue)
            {                
                innerJoinStatments.Add("INNER JOIN ArticleTopics as at ON a.IDArticle = at.IDArticle");
            }
            if (iDSubTopic.HasValue)
            {
                innerJoinStatments.Add("INNER JOIN ArticleSubTopics as ast ON a.IDArticle = ast.IDArticle");
            }
            string innerJoin = "";
            if (innerJoinStatments.Any())
            {
                innerJoin = String.Join(" ", innerJoinStatments);
            }
            return innerJoin;
        }
        private static string WhereFullQueryBuilder(int? iDTopic, int? iDSubTopic)
        {
            List<string> whereStatments = new List<string>();
            if (iDTopic.HasValue && iDTopic.Value > 0)
            {
                whereStatments.Add(string.Format("at.IDTopic = {0}",
                    iDTopic.Value));
            }
            if (iDSubTopic.HasValue && iDSubTopic.Value > 0)
            {
                whereStatments.Add(string.Format("ast.IDSubTopic = {0}",
                    iDSubTopic.Value));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = " WHERE " + String.Join(" AND ", whereStatments);
            }

            return where;
        }

        private static string WhereOnlyOnArticlesBuilder(int? iDTopic, int? iDSubTopic,
            int? iDEdition,
            bool? enabled, bool? atHome,
            ArticlePlaceAtHome placesAtHome, string filteredBy, out List<string> searchTextList)
        {
            searchTextList = new List<string>();
            List<string> whereStatments = new List<string>();
            if (iDTopic.HasValue && iDTopic.Value > 0)
            {
                whereStatments.Add(string.Format("at.IDTopic = {0}",
                    iDTopic.Value));
            }
            if (iDSubTopic.HasValue && iDSubTopic.Value > 0)
            {
                whereStatments.Add(string.Format("ast.IDSubTopic = {0}",
                    iDSubTopic.Value));
            }

            if (iDEdition.HasValue)
                whereStatments.Add(string.Format("a.IDEdition = {0}",
                    iDEdition.Value.ToString()));

            if (!string.IsNullOrEmpty(filteredBy))
            {
                var filterKeys = filteredBy.Split(';');
                List<string> orFilterStatments = new List<string>();
                var indexOfKey = 0;
                foreach (var filterKey in filterKeys)
                {
                    var indexOfEqual = filterKey.IndexOf('=');
                    if (indexOfEqual > 0)
                    {
                        var filterAction = filterKey.Substring(0, indexOfEqual);
                        switch (filterAction)
                        {
                            case "keyword":
                                if (indexOfKey < 3)
                                {
                                    orFilterStatments.Add(string.Format("a.Keywords LIKE '%'+@keyword{0}+'%'",
                                        indexOfKey));
                                    searchTextList.Add(filterKey.Substring(indexOfEqual + 1));
                                    indexOfKey += 1;
                                }
                                break;
                            case "query":
                                orFilterStatments.Add(string.Format("(a.Title LIKE '%'+@searchText+'%' OR a.Summary LIKE '%'+@searchText+'%' OR a.Keywords  LIKE '%'+@searchText+'%')",
                                    filterKey.Substring(indexOfEqual + 1)));
                                searchTextList.Add(filterKey.Substring(indexOfEqual + 1));
                                break;
                            case "excludeArticle":
                                whereStatments.Add(string.Format("a.IDArticle <> {0}",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                            case "publishDateLessThan":
                                whereStatments.Add(string.Format("a.PublicationDate < '{0}'",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                            case "publishDateGreatherThan":
                                whereStatments.Add(string.Format("a.PublicationDate >= '{0}'",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                        }
                    }
                }
                string filterStatment = "";
                if (orFilterStatments.Any())
                {
                    filterStatment = "(" + String.Join(" OR ", orFilterStatments) + ")";
                    whereStatments.Add(filterStatment);
                }
            }
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    whereStatments.Add("a.Enabled = 1");
                }
                else
                {
                    whereStatments.Add("a.Enabled = 0");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    whereStatments.Add("a.AtHome = 1");
                }
                else
                {
                    whereStatments.Add("a.AtHome = 0");
                }
            }
            if (placesAtHome != null)
            {
                var placeAtHome = placesAtHome.Id;
                whereStatments.Add(string.Format("a.IDPlaceAtHome = {0}", placeAtHome.ToString()));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }
            return where;
        }

        private async Task<List<ArticleQueryModel>> QueryDb(
            string inneJoinsOnlyOnArticles,
            string whereOnlyOnArticles, string whereFullQuery,
            int rowIndex, int rowCount, List<string> searchTextList,
            string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query =
                    @"SELECT ao.RowNum,
                    ao.IDArticle AS IDArticle, ao.CreatedBy AS CreatedBy, 
                    ao.CreationDate AS CreationDate,
                    ao.PublicationDate AS PublicationDate, ao.Title AS Title, 
                    ao.Summary AS Summary, ao.Body AS Body, ao.Keywords AS Keywords, 
                    ao.Enabled AS Enabled, ao.AtHome AS AtHome, 
                    ao.[Order] AS [Order], ao.OrderAtEdition AS OrderAtEdition,
                    ao.IDPlaceAtHome AS PlaceAtHome,
                    ao.HomeImage AS HomeImage,  ao.SlideImage AS SlideImage,
                    ao.VerticalImage AS VerticalImage,
                    ao.HorizontalImage AS HorizontalImage,
                    uts.IDTopic AS Topics_IDTopic, uts.TopicTitle AS Topics_Title,
                    uts.IDSubTopic AS SubTopics_IDSubTopic, uts.SubTopicTitle AS SubTopics_Title
                    FROM (  SELECT ROW_NUMBER() OVER(
							ORDER BY (SELECT null)) AS RowNum, a.* FROM Articles AS a "
                            + inneJoinsOnlyOnArticles + " "
                            + whereOnlyOnArticles +
                            @" ORDER BY " + columnOrder + " " + orderDirection +
                            @" OFFSET @rowIndex ROWS       
                            FETCH NEXT @rowCount ROWS ONLY
                    ) AS ao 
                    LEFT JOIN (
                        (SELECT at.IDArticle AS IDArticle, t.IDTopic AS IDTopic,
						NULL AS IDSubTopic, t.Title AS TopicTitle, NULL AS SubTopicTitle
						FROM ArticleTopics AS at
						INNER JOIN Topics AS t ON at.IDTopic = t.IDTopic)
						UNION
                        (SELECT ast.IDArticle AS IDArticle, NULL AS IDTopic, 
						ast.IDSubTopic AS IDSubTopic,
						NULL AS TopicTitle, s.Title AS SubTopicTitle
						FROM ArticleSubTopics AS ast
						INNER JOIN SubTopics AS s ON ast.IDSubTopic = s.IDSubTopic
						)
					) AS uts ON ao.IDArticle = uts.IDArticle
                    ORDER BY ao.RowNum ASC";
                var sqlParameters = new
                {
                    rowIndex,
                    rowCount,
                    searchText = searchTextList.Count > 0 ? searchTextList[0] : "",
                    keyword0 = searchTextList.Count > 0 ? searchTextList[0] : "",
                    keyword1 = searchTextList.Count > 1 ? searchTextList[1] : "",
                    keyword2 = searchTextList.Count > 2 ? searchTextList[2] : ""
                };
                _logger.Debug($"Quering: {query} with parameters: rowIndex:{sqlParameters.rowIndex}, rowCount: {sqlParameters.rowCount}, searchText:{sqlParameters.searchText}, keyword0: {sqlParameters.keyword0}, keyword1: {sqlParameters.keyword1}, keyword2: {sqlParameters.keyword2}");
                var result = await connection.QueryAsync<dynamic>(query, sqlParameters);
                return MapShortArticle(result);
            }
        }

        private List<ArticleQueryModel> MapShortArticle(dynamic result)
        {
            if (result == null)
                return null;
            var articlesList = new List<ArticleQueryModel>();
            var article = (ArticleQueryModel)null;
            foreach (var register in result)
            {
                if (article == null || register.IDArticle != article.IDArticle)
                {
                    if (article != null)
                        articlesList.Add(article);
                    article = new ArticleQueryModel
                    {
                        IDArticle = register.IDArticle,
                        CreatedBy = register.CreatedBy,
                        CreationDate = register.CreationDate,
                        PublicationDate = register.PublicationDate,
                        Title = register.Title,
                        Summary = register.Summary,
                        Body = register.Body,
                        Keywords = register.Keywords,
                        Enabled = register.Enabled,
                        AtHome = register.AtHome,
                        Order = register.Order,
                        PlaceAtHome = ArticlePlaceAtHome.From(register.PlaceAtHome),
                        HomeImage = register.HomeImage,
                        SlideImage = register.SlideImage,
                        HorizontalImage = register.HorizontalImage,
                        VerticalImage = register.VerticalImage,
                        OrderAtEdition = register.OrderAtEdition,
                        Topics = new List<TopicQueryModel>(),
                        SubTopics = new List<SubTopicQueryModel>()
                    };
                }
                if (!(register.SubTopics_IDSubTopic is null) && article != null)
                {
                    var subTopic = new SubTopicQueryModel
                    {
                        IDSubTopic = register.SubTopics_IDSubTopic,
                        Title = register.SubTopics_Title
                    };
                    article.SubTopics.Add(subTopic);
                }
                if (!(register.Topics_IDTopic is null) && article != null)
                {
                    var topic = new TopicQueryModel
                    {
                        IDTopic = register.Topics_IDTopic,
                        Title = register.Topics_Title
                    };
                    article.Topics.Add(topic);
                }
            }
            if (article != null && articlesList.IndexOf(article) < 0)
                articlesList.Add(article);
            return articlesList;
        }
    }
}
