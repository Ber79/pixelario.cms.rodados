﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public interface IArticleScriptItemsQueries
    {
        Task<ArticleScriptItemQueryModel> GetAsync(int iDArticleScriptItem);
        Task<List<ArticleScriptItemQueryModel>> ListAsync(
            int iDArticleScript, int? iDMultimediaType,
            string filteredBy,
            bool? enabled,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(
            int iDArticleScript, int? iDMultimediaType,
            string filteredBy,
            bool? enabled);
    }
}