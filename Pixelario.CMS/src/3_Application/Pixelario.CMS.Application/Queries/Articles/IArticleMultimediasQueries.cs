﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public interface IArticleMultimediasQueries
    {
        /// <summary>
        /// Get a article's multimedia by his unique id
        /// </summary>
        /// <param name="iDMultimedia">unique id of a article's multimedia</param>
        /// <returns>View model of a article's multimedia</returns>
        Task<ArticleMultimediaQueryModel> GetAsync(int iDMultimedia);
        /// <summary>
        /// List of article's multimedia paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDArticle">unique id of an article</param>
        /// <param name="iDMultimediaType">unique id of a multimedia's type</param>        
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <param name="rowIndex">row to start listing</param>
        /// <param name="rowCount">number of row to return</param>
        /// <param name="columnOrder">order to list</param>
        /// <param name="orderDirection">direction of de list</param>
        /// <returns>List of articles on view model</returns>
        Task<List<ArticleMultimediaQueryModel>> ListAsync(            
            int? iDArticle, int[] iDMultimediaTypes,
            bool? enabled, bool? atHome,
            string filteredBy, 
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);
        /// <summary>
        /// Count the number of article's multimedias filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDArticle">unique id of an article</param>
        /// <param name="iDMultimediaType">unique id of a multimedia's type</param>        
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <returns>Number of articles</returns>
        Task<int> CountAsync(int? iDArticle, int[] iDMultimediaTypes,
            bool? enabled, bool? atHome,
            string filteredBy);
    }
}