﻿using Pixelario.CMS.Application.QueryModels.Articles;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Articles
{
    public interface IArticleScriptsQueries
    {
        Task<ArticleScriptQueryModel> GetAsync(int iDArticleScript);
        Task<List<ArticleScriptQueryModel>> ListAsync(
            string filteredBy,
            bool? enabled,
            int rowIndex,
            int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(
            string filteredBy,
            bool? enabled);
    }
}