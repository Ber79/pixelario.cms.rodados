﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Application.QueryModels.Advertisments;

using Serilog;
using System.Collections;

namespace Pixelario.CMS.Application.Queries.Advertisments
{
    public class AdvertismentsQueries : IAdvertismentsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public AdvertismentsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<AdvertismentQueryModel> GetAsync(int iDAdvertisment)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT a.IDAdvertisment AS IDAdvertisment, a.CreatedBy AS CreatedBy, 
                    a.CreationDate AS CreationDate, 
                    a.PublicationStartDate AS PublicationStartDate, 
                    a.PublicationEndDate AS PublicationEndDate, 
                    a.Title AS Title, 
                    a.Code AS Code, a.URL AS URL,
                    a.Enabled AS Enabled, a.Published AS Published, 
                    a.[Order] AS [Order],
                    a.IDPlaceAtWeb AS IDPlaceAtWeb,
                    a.HomeImage AS HomeImage, a.VerticalImage AS VerticalImage,
                    a.HorizontalImage AS HorizontalImage
                    FROM Advertisments AS a 
                    WHERE a.IDAdvertisment =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDAdvertisment });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapFullAdvertisment(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private AdvertismentQueryModel MapFullAdvertisment(dynamic result)
        {
            var advertisment = new AdvertismentQueryModel
            {
                IDAdvertisment = result[0].IDAdvertisment,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                PublicationStartDate = result[0].PublicationStartDate,
                PublicationEndDate = result[0].PublicationEndDate,
                Title = result[0].Title,
                Code = result[0].Code,
                URL = result[0].URL,
                Enabled = result[0].Enabled,
                Published = result[0].Published,
                Order = result[0].Order,
                PlaceAtWeb = AdvertismentPlaceAtWeb.From(
                    result[0].IDPlaceAtWeb),
                HomeImage = result[0].HomeImage,
                HorizontalImage = result[0].HorizontalImage,
                VerticalImage = result[0].VerticalImage
            };
            return advertisment;
        }
        public async Task<int> CountAsync(AdvertismentPlaceAtWeb placeAtWeb,
            bool? enabled, bool? published, string filteredBy)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchText = "";
                string where = AdvertismentsWhereStatmentBuilder(
                    placeAtWeb, enabled,
                    published, filteredBy, out searchText);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Advertisments AS a " + where;
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query, new
                    {
                        searchText
                    });
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<AdvertismentQueryModel>> ListAsync
            (AdvertismentPlaceAtWeb placeAtWeb,
            bool? enabled, bool? published, string filteredBy,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchText = "";
                string whereStatments = AdvertismentsWhereStatmentBuilder(
                    placeAtWeb,
                    enabled, published, 
                    filteredBy, out searchText);
                return await QueryDb(whereStatments, searchText,
                    rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private static string WhereFullQueryBuilder(int? iDTopic, int? iDSubTopic)
        {
            List<string> whereStatments = new List<string>();
            if (iDTopic.HasValue && iDTopic.Value > 0)
            {
                whereStatments.Add(string.Format("uts.IDTopic = {0}",
                    iDTopic.Value));
            }
            if (iDSubTopic.HasValue && iDSubTopic.Value > 0)
            {
                whereStatments.Add(string.Format("uts.IDSubTopic = {0}",
                    iDSubTopic.Value));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = " WHERE " + String.Join(" AND ", whereStatments);
            }

            return where;
        }

        private static string AdvertismentsWhereStatmentBuilder(
            AdvertismentPlaceAtWeb placeAtWeb,
            bool? enabled, bool? published, string filteredBy, 
            out string searchText)
        {
            searchText = "";
            List<string> whereStatments = new List<string>();            
            if (placeAtWeb != null)
                whereStatments.Add(string.Format("a.IDPlaceAtWeb = {0}",
                    placeAtWeb.Id.ToString()));

            if (!string.IsNullOrEmpty(filteredBy))
            {
                var filterKeys = filteredBy.Split(';');
                List<string> orFilterStatments = new List<string>();
                var indexOfKey = 0;
                foreach (var filterKey in filterKeys)
                {
                    var indexOfEqual = filterKey.IndexOf('=');
                    if (indexOfEqual > 0)
                    {
                        var filterAction = filterKey.Substring(0, indexOfEqual);
                        switch (filterAction)
                        {
                            case "query":
                                orFilterStatments.Add("a.Title LIKE '%'+@searchText+'%'");
                                searchText = filterKey.Substring(indexOfEqual + 1);
                                break;
                            case "excludeAdvertisment":
                                whereStatments.Add(string.Format("a.IDAdvertisment <> {0}",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                            case "publishEndDateLessThan":
                                whereStatments.Add(string.Format("a.PublicationEndDate < '{0}'",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                            case "publishStartDateGreatherThan":
                                whereStatments.Add(string.Format("a.PublicationStartDate >= '{0}'",
                                    filterKey.Substring(indexOfEqual + 1)));
                                break;
                        }
                    }
                }
                string filterStatment = "";
                if (orFilterStatments.Any())
                {
                    filterStatment = "(" + String.Join(" OR ", orFilterStatments) + ")";
                    whereStatments.Add(filterStatment);
                }
            }
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    whereStatments.Add("a.Enabled = 1");
                }
                else
                {
                    whereStatments.Add("a.Enabled = 0");
                }
            }
            if (published.HasValue)
            {
                if (published.Value)
                {
                    whereStatments.Add("a.Published = 1");
                }
                else
                {
                    whereStatments.Add("a.Published = 0");
                }
            }
            if (placeAtWeb != null)
            {
                var placePublished = placeAtWeb.Id;
                whereStatments.Add(string.Format("a.IDPlaceAtWeb = {0}", placePublished.ToString()));
            }
            string where = "";
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }
            return where;
        }

        private async Task<List<AdvertismentQueryModel>> QueryDb(
            string whereStatments, string searchText,
            int rowIndex, int rowCount, 
            string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query =
                    @"SELECT  a.IDAdvertisment AS IDAdvertisment, a.CreatedBy AS CreatedBy, 
                    a.CreationDate AS CreationDate, 
                    a.PublicationStartDate AS PublicationStartDate, 
                    a.PublicationEndDate AS PublicationEndDate, 
                    a.Title AS Title, 
                    a.Code AS Code, a.URL AS URL,
                    a.Enabled AS Enabled, a.Published AS Published, 
                    a.[Order] AS [Order],
                    a.IDPlaceAtWeb AS IDPlaceAtWeb,
                    a.HomeImage AS HomeImage, a.VerticalImage AS VerticalImage,
                    a.HorizontalImage AS HorizontalImage
                    FROM Advertisments AS a "
                    + whereStatments +
                    @" ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                var sqlParameters = new
                {
                    rowIndex,
                    rowCount,
                    searchText
                };
                _logger.Debug($"Quering: {query} with parameters: rowIndex:{sqlParameters.rowIndex}, rowCount: {sqlParameters.rowCount}, searchText:{sqlParameters.searchText}");
                var result = await connection.QueryAsync<dynamic>(query, sqlParameters);
                return MapShortAdvertisment(result);
            }
        }

        private List<AdvertismentQueryModel> MapShortAdvertisment(dynamic result)
        {
            if (result == null)
                return null;
            var advertismentsList = new List<AdvertismentQueryModel>();
            var advertisment = (AdvertismentQueryModel)null;
            foreach (var register in result)
            {
                if (advertisment == null || register.IDAdvertisment != advertisment.IDAdvertisment)
                {
                    if (advertisment != null)
                        advertismentsList.Add(advertisment);
                    advertisment = new AdvertismentQueryModel
                    {
                        IDAdvertisment = register.IDAdvertisment,
                        CreatedBy = register.CreatedBy,
                        CreationDate = register.CreationDate,
                        PublicationStartDate = register.PublicationStartDate,
                        PublicationEndDate = register.PublicationEndDate,
                        Title = register.Title,
                        Code = register.Code,
                        URL = register.URL,
                        Enabled = register.Enabled,
                        Published = register.Published,
                        Order = register.Order,
                        PlaceAtWeb = AdvertismentPlaceAtWeb.From(register.IDPlaceAtWeb),
                        HomeImage = register.HomeImage,
                        HorizontalImage = register.HorizontalImage,
                        VerticalImage = register.VerticalImage                        
                    };
                }
            }
            if (advertisment != null && advertismentsList.IndexOf(advertisment) < 0)
                advertismentsList.Add(advertisment);
            return advertismentsList;
        }
    }
}
