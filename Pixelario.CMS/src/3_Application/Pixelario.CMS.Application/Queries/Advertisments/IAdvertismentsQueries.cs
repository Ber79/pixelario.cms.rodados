﻿using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Application.QueryModels.Advertisments;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Advertisments
{
    public interface IAdvertismentsQueries
    {
        /// <summary>
        /// Get an advertisment by his unique id
        /// </summary>
        /// <param name="iDAdvertisment">unique id of an advertisment</param>
        /// <returns>Get an advertisment on query model</returns>
        Task<AdvertismentQueryModel> GetAsync(int iDAdvertisment);
        /// <summary>
        /// List of advertisments paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="placeAtWeb">unique id of a advertisment place at web</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="published">publish status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <param name="rowIndex">row to start listing</param>
        /// <param name="rowCount">number of row to return</param>
        /// <param name="columnOrder">order to list</param>
        /// <param name="orderDirection">direction of de list</param>
        /// <returns>List of advertisments on query model</returns>
        Task<List<AdvertismentQueryModel>> ListAsync(            
            AdvertismentPlaceAtWeb placeAtWeb, 
            bool? enabled, bool? published, string filteredBy, 
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);
        /// <summary>
        /// Count the number of advertisments filtered by
        /// diferents options
        /// </summary>
        /// <param name="placeAtWeb">unique id of a advertisment place at web</param>
        /// <param name="enabled">enable status</param>        
        /// <param name="published">publish status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <returns>Number of advertisments</returns>
        Task<int> CountAsync(AdvertismentPlaceAtWeb placeAtWeb, 
            bool? enabled, bool? published, string filteredBy);
    }
}