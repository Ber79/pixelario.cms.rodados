﻿using Pixelario.CMS.Application.QueryModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Pixelario.CMS.Application.Queries.Pages
{
    public interface IPageMultimediaQueries
    {
        /// <summary>
        /// Get a page's multimedia by his unique id
        /// </summary>
        /// <param name="iDMultimedia">unique id of a page's multimedia</param>
        /// <returns>View model of a page's multimedia</returns>
        Task<PageMultimediaQueryModel> GetAsync(int iDMultimedia);
        /// <summary>
        /// List of page's multimedia paged, ordered and filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDPage">unique id of an page</param>
        /// <param name="iDMultimediaType">unique id of a multimedia's type</param>        
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <param name="rowIndex">row to start listing</param>
        /// <param name="rowCount">number of row to return</param>
        /// <param name="columnOrder">order to list</param>
        /// <param name="orderDirection">direction of de list</param>
        /// <returns>List of pages on view model</returns>
        Task<List<PageMultimediaQueryModel>> ListAsync(            
            int? iDPage, int? iDMultimediaType,
            bool? enabled, bool? atHome,
            string filteredBy, 
            int rowIndex, 
            int rowCount,
            string columnOrder,
            string orderDirection);
        /// <summary>
        /// Count the number of page's multimedias filtered by
        /// diferents options
        /// </summary>
        /// <param name="iDPage">unique id of an page</param>
        /// <param name="iDMultimediaType">unique id of a multimedia's type</param>        
        /// <param name="enabled">enable status</param>        
        /// <param name="atHome">home status</param>
        /// <param name="filteredBy">text to filter</param>
        /// <returns>Number of pages</returns>
        Task<int> CountAsync(int? iDPage, int? iDMultimediaType,
            bool? enabled, bool? atHome,
            string filteredBy);
    }
}