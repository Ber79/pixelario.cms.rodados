﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Application.QueryModels.Editions;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Pages
{
    public class PagesQueries : IPagesQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public PagesQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<PageQueryModel> GetAsync(int iDPage)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT p.IDPage AS IDPage, p.CreatedBy AS CreatedBy, 
                    p.CreationDate AS CreationDate, p.Title AS Title, 
                    p.Summary AS Summary, p.Body AS Body, 
                    p.Keywords AS Keywords, 
                    p.Enabled AS Enabled, p.AtHome AS AtHome, 
                    p.[Order] AS [Order], p.OnMenu AS OnMenu, 
                    p.OrderAtMenu AS OrderAtMenu,
                    p.HomeImage AS HomeImage,
                    e.IDEdition AS IDEdition, e.Title AS EditionTitle
                    FROM Pages AS p
                    LEFT JOIN PageEditions AS pe ON p.IDPage = pe.IDPage
                    LEFT JOIN Editions AS e ON pe.IDEdition = e.IDEdition
                    WHERE p.IDPage = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query, new { id = iDPage });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapFullPage(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private PageQueryModel MapFullPage(dynamic result)
        {
            var tema = new PageQueryModel
            {
                IDPage = result[0].IDPage,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Body = result[0].Body,
                Keywords = result[0].Keywords,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                OnMenu = result[0].OnMenu,
                OrderAtMenu = result[0].OrderAtMenu,
                HomeImage = result[0].HomeImage,
                Editions = new List<EditionQueryModel>()
            };
            foreach (dynamic item in result)
            {
                if (!(item.IDEdition is null))
                {
                    var edition = new EditionQueryModel
                    {
                        IDEdition = item.IDEdition,
                        Title = item.EditionTitle
                    };
                    tema.Editions.Add(edition);
                }
            }
            return tema;
        }

        public async Task<int> CountAsync(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDEdition, filteredBy, enabled,
            atHome, onMenu);
                string joinEdition = JoinEditionsBuilder(iDEdition);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = $@"
                    SELECT COUNT(*)
                    FROM Pages AS p 
                    {joinEdition}
                    {where}";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        private string JoinEditionsBuilder(int? iDEdition)
        {
            string join = "";
            if (iDEdition.HasValue)
            {
                join = @"INNER JOIN PageEditions AS e
                        ON p.IDPage = e.IDPage";
            }
            return join;
        }

        public async Task<List<PageQueryModel>> ListAsync(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDEdition, filteredBy, enabled,
            atHome, onMenu);
                string joinEdition = JoinEditionsBuilder(iDEdition);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = $@"
                    SELECT p.IDPage AS IDPage, p.CreatedBy AS CreatedBy, 
                    p.CreationDate AS CreationDate, p.Title AS Title, 
                    p.Summary AS Summary, p.Body AS Body, 
                    p.Keywords AS Keywords, 
                    p.Enabled AS Enabled, p.AtHome AS AtHome, 
                    p.[Order] AS[Order], p.OnMenu AS OnMenu, 
                    p.OrderAtMenu AS OrderAtMenu,
                    p.HomeImage AS HomeImage
                    FROM Pages AS p
                    {joinEdition}
                    {where}
                    ORDER BY {columnOrder} {orderDirection}
                    OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<PageQueryModel>(query,
                            new
                            {
                                rowIndex,
                                rowCount
                            });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private static string WhereBuilder(int? iDEdition, string filteredBy,
            bool? enabled, bool? atHome, bool? onMenu)
        {
            List<string> wheresParaQuery = new List<string>();
            if (iDEdition.HasValue)
                wheresParaQuery.Add(string.Format("e.IDEdition = {0}",
                    iDEdition.Value.ToString()));
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("p.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("p.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("p.Enabled = 0");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    wheresParaQuery.Add("p.AtHome = 1");
                }
                else
                {
                    wheresParaQuery.Add("p.AtHome = 0");
                }
            }
            if (onMenu.HasValue)
            {
                if (onMenu.Value)
                {
                    wheresParaQuery.Add("p.OnMenu = 1");
                }
                else
                {
                    wheresParaQuery.Add("p.OnMenu = 0");
                }
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }
    }
}