﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Application.QueryModels.Pages;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Pages
{
    public class PageMultimediaQueries : IPageMultimediaQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public PageMultimediaQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<PageMultimediaQueryModel> GetAsync(int iDMultimedio)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT pm.IDMultimedia AS IDMultimedia, 
                    pm.CreatedBy AS CrIDMultimediaeatedBy, 
                    pm.CreationDate AS CreationDate, 
                    pm.IDMultimediaType AS IDMultimediaType,
                    pm.Title AS Title, pm.Summary AS Summary, 
                    pm.Path1 AS Path1, pm.Path2 AS Path2, 
                    pm.Enabled AS Enabled, pm.AtHome AS AtHome, 
                    pm.[Order] AS [Order], pm.Cover AS Cover,
                    pm.IDPage AS IDPage, p.Title as PageTitle
                    FROM PageMultimedia AS pm 
                    INNER JOIN Pages AS p on p.IDPage = pm.IDPage
                    WHERE pm.IDMultimedia =  @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                        new { id = iDMultimedio });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapDynamicToMultimedia(result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private PageMultimediaQueryModel MapDynamicToMultimedia(dynamic result)
        {
            var multimedia = new PageMultimediaQueryModel
            {
                IDMultimedia = result[0].IDMultimedia,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                IDMultimediaType = result[0].IDMultimediaType,
                Title = result[0].Title,
                Summary = result[0].Summary,
                Path1 = result[0].Path1,
                Path2 = result[0].Path2,
                Enabled = result[0].Enabled,
                AtHome = result[0].AtHome,
                Order = result[0].Order,
                Cover = result[0].Cover,
                Page = new PageQueryModel()
                {
                    IDPage = result[0].IDPage,
                    Title = result[0].PageTitle,
                }
            };
            return multimedia;
        }
        private List<PageMultimediaQueryModel> MapDynamicToMultimediaList(dynamic result)
        {
            var multimedia = new List<PageMultimediaQueryModel>();
            foreach (var item in result)
            {
                multimedia.Add(new PageMultimediaQueryModel
                {
                    IDMultimedia = item.IDMultimedia,
                    CreatedBy = item.CreatedBy,
                    CreationDate = item.CreationDate,
                    IDMultimediaType = item.IDMultimediaType,
                    Title = item.Title,
                    Summary = item.Summary,
                    Path1 = item.Path1,
                    Path2 = item.Path2,
                    Enabled = item.Enabled,
                    AtHome = item.AtHome,
                    Order = item.Order,
                    Cover = item.Cover,
                    Page = new PageQueryModel()
                    {
                        IDPage = item.IDPage,
                        Title = item.PageTitle,
                    }
                });
            }

            return multimedia;
        }

        public async Task<int> CountAsync(int? iDPage, int? iDMultimediaType,
            bool? enabled, bool? atHome, string filteredBy)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDPage, iDMultimediaType,
            enabled, atHome, filteredBy);
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM PageMultimedia AS pm "
                        + where;
                    var result = await connection.QueryAsync<int>(query);
                    _logger.Debug("Quering: {0}", query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }


        public async Task<List<PageMultimediaQueryModel>> ListAsync(
            int? iDPage, int? iDMultimediaType,
            bool? enabled, bool? atHome, string filteredBy,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                string where = WhereBuilder(iDPage, iDMultimediaType, enabled, atHome, filteredBy);
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private static string WhereBuilder(int? iDPage,
            int? iDMultimediaType,
            bool? enabled, bool? atHome, string filteredBy)
        {
            List<string> wheresParaQuery = new List<string>();
            if (iDPage.HasValue)
                wheresParaQuery.Add(string.Format("pm.IDPage = {0}",
                    iDPage.Value.ToString()));
            if (iDMultimediaType.HasValue)
                wheresParaQuery.Add(string.Format("pm.IDMultimediaType = {0}",
                    iDMultimediaType.Value.ToString()));
            if (!string.IsNullOrEmpty(filteredBy))
                wheresParaQuery.Add(string.Format("pm.Title Like '{0}%'",
                    filteredBy));
            if (enabled.HasValue)
            {
                if (enabled.Value)
                {
                    wheresParaQuery.Add("pm.Enabled = 1");
                }
                else
                {
                    wheresParaQuery.Add("pm.Enabled = 1");
                }
            }
            if (atHome.HasValue)
            {
                if (atHome.Value)
                {
                    wheresParaQuery.Add("pm.AtHome = 1");
                }
                else
                {
                    wheresParaQuery.Add("pm.AtHome = 1");
                }
            }
            string where = "";
            if (wheresParaQuery.Any())
            {
                where = "WHERE " + String.Join(" AND ", wheresParaQuery);
            }

            return where;
        }

        private async Task<List<PageMultimediaQueryModel>> QueryDb(string where, int rowIndex, int rowCount, string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT pm.IDMultimedia AS IDMultimedia, 
                    pm.CreatedBy AS CreatedBy, pm.CreationDate AS CreationDate, 
                    pm.IDMultimediaType AS IDMultimediaType,
                    pm.Title AS Title, pm.Summary AS Summary, 
                    pm.Path1 AS Path1, pm.Path2 AS Path2, 
                    pm.Enabled AS Enabled, pm.AtHome AS AtHome, 
                    pm.[Order] AS [Order], pm.Cover AS Cover,
                    pm.IDPage AS IDPage, p.Title as PageTitle
                    FROM PageMultimedia AS pm 
                    INNER JOIN Pages AS p on p.IDPage = pm.IDPage " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return MapDynamicToMultimediaList(result.ToList());
            }
        }
    }
}