﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Configurations
{
    public interface IConfigurationsQueries
    {
        Task<ConfigurationQueryModel> GetAsync(int iDConfiguration);
        Task<string> GetStateAsync(string key);
        Task<List<ConfigurationQueryModel>> ListAsync(            
            string filteredBy, string[] keys,
            int? configurationType,
            int rowIndex, int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(string filteredBy,
            int? configurationType, string[] keys);
    }
}