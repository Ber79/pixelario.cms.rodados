﻿using Pixelario.CMS.Application.QueryModels.Configurations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Configurations
{
    public interface IConfigurationOptionQueries
    {
        Task<ConfigurationOptionQueryModel> GetAsync(int iDConfigurationOption);
        Task<List<ConfigurationOptionQueryModel>> ListAsync(            
            int iDConfiguration, bool? enabled,
            int rowIndex, int rowCount,
            string columnOrder,
            string orderDirection);
        Task<int> CountAsync(int iDConfiguration, bool? enabled);
    }
}