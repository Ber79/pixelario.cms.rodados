﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;

namespace Pixelario.CMS.Application.Queries.Configurations
{
    public class ConfigurationsQueries : IConfigurationsQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public ConfigurationsQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<ConfigurationQueryModel> GetAsync(int iDConfiguration)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT c.IDConfiguration AS IDConfiguration, c.CreatedBy AS CreatedBy, 
                    c.CreationDate AS CreationDate, c.Title AS Title, 
                    c.[Key] AS [Key], c.State AS State, c.Type AS ConfigurationType,
                    c.Roles as Roles
                    FROM Configurations AS c
                    WHERE c.IDConfiguration = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                        new { id = iDConfiguration });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapDynamicToConfiguration(result);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        public async Task<string> GetStateAsync(string key)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT c.State AS State
                    FROM Configurations AS c
                    WHERE c.[key] = @key";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<string>(query,
                        new { key = key });
                    if (result.AsList().Count == 0)
                        return null;
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }

        private ConfigurationQueryModel MapDynamicToConfiguration(dynamic result)
        {
            var configuration = new ConfigurationQueryModel
            {
                IDConfiguration = result[0].IDConfiguration,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Title = result[0].Title,
                Key = result[0].Key,
                State = result[0].State,
                ConfigurationType = (ConfigurationType)result[0].ConfigurationType,
                Roles = result[0].Roles
            };
            return configuration;
        }


        public async Task<int> CountAsync(string filteredBy,
            int? configurationType, string[] keys)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchText = filteredBy;
                List<string> wheresParaQuery = new List<string>();
                if (!string.IsNullOrEmpty(filteredBy))
                    wheresParaQuery.Add("(c.Title LIKE '%'+ @searchText + '%' OR c.[Key] LIKE '%'+ @searchText + '%')");
                if (keys != null && keys.Length > 0)
                {
                    var wheresKeyList = new List<string>();
                    foreach (var key in keys)
                    {
                        wheresKeyList.Add(string.Format("c.[Key] Like '{0}%'",
                            key));
                    }
                    wheresParaQuery.Add(
                        "(" + String.Join(" OR ", wheresKeyList) + ")");
                }
                if (configurationType.HasValue)
                {
                    wheresParaQuery.Add(string.Format("c.Type = {0}",
                        configurationType.Value));
                }
                string where = "";
                if (wheresParaQuery.Any())
                {
                    where = "WHERE " + String.Join(" AND ", wheresParaQuery);
                }
                IEnumerable<int> result;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM Configurations AS c "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    result = await connection.QueryAsync<int>(query, new{
                        searchText
                    });
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        public async Task<List<ConfigurationQueryModel>> ListAsync(
            string filteredBy, string[] keys,
            int? configurationType,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                var searchText = filteredBy;
                List<string> wheresParaQuery = new List<string>();
                if (!string.IsNullOrEmpty(filteredBy))
                    wheresParaQuery.Add("(c.Title LIKE '%'+ @searchText + '%' OR c.[Key] LIKE '%'+ @searchText + '%')");
                if (keys != null && keys.Length > 0)
                {
                    var wheresKeyList = new List<string>();
                    foreach (var key in keys)
                    {
                        wheresKeyList.Add(string.Format("c.[Key] Like '{0}%'",
                            key));
                    }
                    wheresParaQuery.Add(
                        "(" + String.Join(" OR ", wheresKeyList) + ")");
                }
                if (configurationType.HasValue)
                {
                    wheresParaQuery.Add(string.Format("c.Type = {0}",
                        configurationType.Value));
                }
                string where = "";
                if (wheresParaQuery.Any())
                {
                    where = "WHERE " + String.Join(" AND ", wheresParaQuery);
                }
                return await QueryDb(where, rowIndex, rowCount,
                    searchText,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private async Task<List<ConfigurationQueryModel>> QueryDb(string where, 
            int rowIndex, int rowCount, 
            string searchText,
            string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"
                    SELECT	
                        c.IDConfiguration AS IDConfiguration, c.CreatedBy AS CreatedBy, 
		                c.CreationDate AS CreationDate, c.Title AS Title, 
		                c.[Key] AS [Key], c.State AS State, c.Type AS ConfigurationType,
		                c.Roles as Roles,
		                co.IDConfigurationOption AS IDConfigurationOption,
		                co.Value AS Value
                    FROM Configurations AS c
                    LEFT JOIN (
	                    SELECT	co.IDConfiguration AS IDConfiguration,
			                    co.IDConfigurationOption AS IDConfigurationOption,
			                    co.Value AS Value
	                    FROM ConfigurationOptions AS co
	                    WHERE co.Enabled = 1) AS co
                    ON c.IDConfiguration = co.IDConfiguration " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<dynamic>(query,
                        new
                        {
                            rowIndex,
                            rowCount,
                            searchText
                        });
                return MapListToTopicsList(result.ToList());
            }
        }

        private List<ConfigurationQueryModel> MapListToTopicsList(dynamic result)
        {
            if (result == null)
                return null;
            var configurationsList = new List<ConfigurationQueryModel>();
            var _lastIDConfiguration = -1;
            var configuration = new ConfigurationQueryModel();
            foreach (var item in result)
            {
                if (_lastIDConfiguration != item.IDConfiguration)
                {
                    if (_lastIDConfiguration != -1)
                        configurationsList.Add(configuration);
                    _lastIDConfiguration = item.IDConfiguration;
                    configuration = new ConfigurationQueryModel();
                    configuration.IDConfiguration = item.IDConfiguration;
                    configuration.CreatedBy = item.CreatedBy;
                    configuration.CreationDate = item.CreationDate;
                    configuration.Title = item.Title;
                    configuration.Key = item.Key;
                    configuration.State = item.State;
                    configuration.ConfigurationType = (ConfigurationType)item.ConfigurationType;
                    configuration.Roles = item.Roles;
                    if (item.IDConfigurationOption != null)
                    {
                        configuration.ConfigurationOptions.Add(
                            new ConfigurationOptionQueryModel()
                            {
                                IDConfigurationOption = item.IDConfigurationOption,
                                Value = item.Value
                            });
                    }
                }
                else
                {
                    if (item.IDConfigurationOption != null)
                    {
                        configuration.ConfigurationOptions.Add(
                            new ConfigurationOptionQueryModel()
                            {
                                IDConfigurationOption = item.IDConfigurationOption,
                                Value = item.Value
                            });
                    }
                }                
            }
            if (configuration.IDConfiguration != -1 && !configurationsList.Contains(configuration))
                configurationsList.Add(configuration);
            return configurationsList;
        }

    }
}