﻿using Dapper;
using Pixelario.CMS.Application.QueryModels.Configurations;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Pixelario.CMS.Application.Queries.Configurations
{
    public class ConfigurationOptionQueries : IConfigurationOptionQueries
    {
        private readonly ILogger _logger;
        private string _connectionString = string.Empty;
        public ConfigurationOptionQueries(
            ILogger logger,
            string connectionString)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _connectionString = !string.IsNullOrEmpty(connectionString)
                ? connectionString
                : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<ConfigurationOptionQueryModel> GetAsync(int iDConfigurationOption)
        {
            _logger.Debug("Starting to query");
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"
                    SELECT	co.IDConfigurationOption AS IDConfigurationOption,
		                    co.IDConfiguration AS IDConfiguration,
		                    c.Title AS ConfigurationTitle,
		                    co.CreatedBy AS CreatedBy, 
		                    co.CreationDate AS CreationDate, 
		                    co.Value AS Value, 
		                    co.Enabled AS Enabled, 
		                    co.[Order] AS [Order]
                    FROM ConfigurationOptions AS co
                    INNER JOIN Configurations AS c
	                    ON co.IDConfiguration = c.IDConfiguration
                    WHERE co.IDConfigurationOption = @id";
                    _logger.Debug("Quering: {0}", query);
                    var result = await connection.QueryAsync<dynamic>(query,
                        new { id = iDConfigurationOption });
                    if (result.AsList().Count == 0)
                        return null;
                    return MapDynamicToConfigurationOption(result);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private ConfigurationOptionQueryModel MapDynamicToConfigurationOption(dynamic result)
        {
            var configuration = new ConfigurationOptionQueryModel
            {
                IDConfigurationOption = result[0].IDConfigurationOption,
                CreatedBy = result[0].CreatedBy,
                CreationDate = result[0].CreationDate,
                Value = result[0].Value,
                Enabled = result[0].Enabled,
                Order = result[0].Order,
                Configuration = new ConfigurationQueryModel
                {
                    IDConfiguration = result[0].IDConfiguration,
                    Title = result[0].ConfigurationTitle
                }
            };
            return configuration;
        }


        public async Task<int> CountAsync(int iDConfiguration, 
            bool? enabled)
        {
            _logger.Debug("Starting to query");
            try
            {
                List<string> wheresParaQuery = new List<string>();
                wheresParaQuery.Add(string.Format("co.IDConfiguration = {0}",
                   iDConfiguration));
                string where = "";
                if (wheresParaQuery.Any())
                {
                    where = "WHERE " + String.Join(" AND ", wheresParaQuery);
                }
                IEnumerable<int> result;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    var query = @"SELECT COUNT(*)
                    FROM ConfigurationOptions AS co "
                        + where;
                    _logger.Debug("Quering: {0}", query);
                    result = await connection.QueryAsync<int>(query);
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return -1;
            }
        }

        public async Task<List<ConfigurationOptionQueryModel>> ListAsync(
            int iDConfiguration, bool? enabled,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            _logger.Debug("Starting to query");
            try
            {
                List<string> wheresParaQuery = new List<string>();
                wheresParaQuery.Add(string.Format("co.IDConfiguration = {0}",
                   iDConfiguration));
                string where = "";
                if (wheresParaQuery.Any())
                {
                    where = "WHERE " + String.Join(" AND ", wheresParaQuery);
                }
                return await QueryDb(where, rowIndex, rowCount,
                    columnOrder, orderDirection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception catched");
                return null;
            }
        }
        private async Task<List<ConfigurationOptionQueryModel>> QueryDb(string where,
            int rowIndex, int rowCount,
            string columnOrder, string orderDirection)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"SELECT co.IDConfigurationOption AS IDConfigurationOption,
                    co.IDConfiguration AS IDConfiguration,
                    co.CreatedBy AS CreatedBy, 
                    co.CreationDate AS CreationDate, 
                    co.Value AS Value, 
                    co.Enabled AS Enabled, 
                    co.[Order] AS [Order]
                    FROM ConfigurationOptions AS co " + where +
                    " ORDER BY " + columnOrder + " " + orderDirection +
                    @" OFFSET @rowIndex ROWS       
                    FETCH NEXT @rowCount ROWS ONLY";
                _logger.Debug("Quering: {0}", query);
                var result = await connection.QueryAsync<ConfigurationOptionQueryModel>(query,
                        new
                        {
                            rowIndex,
                            rowCount
                        });
                return result.ToList();
            }
        }
    }
}