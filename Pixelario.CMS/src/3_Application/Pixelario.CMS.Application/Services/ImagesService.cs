﻿using System;
using System.Collections.Generic;
using System.Web;

using System.IO;
using SD = System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;


namespace Pixelario.CMS.Application.Services
{
    public class ImageServices : IImageServices
    {
        public byte[] DrawImage(string file, int width)
        {
            try
            {
                //Instancio la imagen original
                using (SD.Image rawImage = SD.Image.FromFile(file))
                {
                    //Calculo el nuevo alto
                    int heigth = Convert.ToInt32(decimal.Floor((decimal)width * ((decimal)rawImage.Height / (decimal)rawImage.Width)));
                    //Instancio la nueva imagen 
                    using (SD.Bitmap nuevaImagen = new SD.Bitmap(width, heigth, rawImage.PixelFormat))
                    {
                        //Seteo la misma resolucion de la original.
                        nuevaImagen.SetResolution(rawImage.HorizontalResolution,
                            rawImage.VerticalResolution);
                        using (SD.Graphics Graphic = SD.Graphics.FromImage(nuevaImagen))
                        {
                            Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            var attributes = new ImageAttributes();
                            attributes.SetWrapMode(WrapMode.TileFlipXY);

                            Graphic.DrawImage(rawImage,
                                new SD.Rectangle(0, 0, width, heigth),
                                0, 0, rawImage.Width, rawImage.Height,
                                SD.GraphicsUnit.Pixel,
                                attributes);
                            MemoryStream ms = new MemoryStream();
                            nuevaImagen.Save(ms, rawImage.RawFormat);
                            return ms.GetBuffer();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw (Ex);
            }
        }
        public byte[] DrawImage(string file, int naturalWidth, int width,
            int xPoint, int yPoint, int wCrop, int hCrop)
        {
            try
            {
                //Instancio la imagen original
                using (SD.Image rawImage = SD.Image.FromFile(file))
                {
                    var rawWith = rawImage.Width;
                    float ratio = (float)1;// (float)naturalWidth / (float)rawWith;
                    //Calculo el nuevo alto
                    int heigth = 
                        Convert.ToInt32(decimal.Floor((decimal)width * ((decimal)hCrop / (decimal)wCrop)));
                    //Instancio la nueva imagen 
                    using (SD.Bitmap nuevaImagen = new SD.Bitmap(width, heigth, rawImage.PixelFormat))
                    {
                        //Seteo la misma resolucion de la original.
                        nuevaImagen.SetResolution(rawImage.HorizontalResolution,
                            rawImage.VerticalResolution);
                        using (SD.Graphics Graphic = SD.Graphics.FromImage(nuevaImagen))
                        {
                            Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                            Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            Graphic.DrawImage(rawImage,
                                new SD.Rectangle(0, 0, width, heigth),
                                xPoint * ratio, yPoint * ratio,
                                wCrop * ratio, hCrop * ratio,
                                SD.GraphicsUnit.Pixel);
                            MemoryStream ms = new MemoryStream();
                            nuevaImagen.Save(ms, rawImage.RawFormat);
                            return ms.GetBuffer();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw (Ex);
            }
        }

        public bool Salvar(string file, string newFile, int width)
        {
            try
            {
                byte[] byteStream = DrawImage(file, width);
                using (MemoryStream ms = new MemoryStream(byteStream, 0, byteStream.Length))
                {
                    ms.Write(byteStream, 0, byteStream.Length);
                    using (SD.Image imagenSteam = SD.Image.FromStream(ms, true))
                    {
                        imagenSteam.Save(newFile, imagenSteam.RawFormat);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return false;
            }
        }
        public bool Salvar(string file, string newFile,
            int naturalWidth, int width,
            int xPoint, int yPoint, int wCrop, int hCrop)
        {
            try
            {
                byte[] byteStream = DrawImage(file, naturalWidth, width,
                    xPoint, yPoint, wCrop, hCrop);
                using (MemoryStream ms = new MemoryStream(byteStream, 0, byteStream.Length))
                {
                    ms.Write(byteStream, 0, byteStream.Length);
                    using (SD.Image imagenSteam = SD.Image.FromStream(ms, true))
                    {
                        imagenSteam.Save(newFile, imagenSteam.RawFormat);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //return false;
            }
        }

    }
}
