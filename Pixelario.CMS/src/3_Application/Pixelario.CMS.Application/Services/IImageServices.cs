﻿
namespace Pixelario.CMS.Application.Services
{
    /// <summary>
    /// Interface de servicios para tratar imagenes
    /// </summary>
    public interface IImageServices
    {
        
        bool Salvar(string ArchivoGrafico, string ruta, int width);
        bool Salvar(string file, string newFile, int naturalWidth, int width,
            int xPoint, int yPoint, int wCrop, int hCrop);

    }
}
