﻿using System.Collections.Generic;
namespace Pixelario.CMS.Application.QueryModels.Configurations
{
    public partial class ConfigurationQueryModel
    {
        public string OnclickEliminar
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de borrar {0}?') == false) return false;", this.Title);
            }
        }
        public string Section
        {
            get
            {
                if (string.IsNullOrEmpty(this.Key))
                    return null;
                var segments = this.Key.Split('/');
                if (segments.Length > 1)
                {
                    return segments[1];
                }
                return null;
            }
        }
        public string Setting
        {
            get
            {
                var segments = this.Key.Split('/');
                if (segments.Length > 2)
                {
                    if (segments.Length > 3)                   
                        return segments[2] + "/" + segments[3];
                    else
                        return segments[2];
                }
                return null;
            }
        }
        public string Index
        {
            get
            {
                var segments = this.Key.Split('/');
                if (segments.Length > 3)
                {
                    return segments[3];
                }
                return null;
            }
        }
    }
}
