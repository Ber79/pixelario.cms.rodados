﻿using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.Commons;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.QueryModels.Configurations
{
    public partial class ConfigurationQueryModel
    {
        public int IDConfiguration { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public string State { get; set; }
        public string Roles { get; set; }
        public ConfigurationType ConfigurationType { get; internal set; }

        public string[] GetBindedRoles()
        {
            if (string.IsNullOrEmpty(this.Roles))
                return new string[] { };
            return this.Roles.Split(';');
        }
        public List<ConfigurationOptionQueryModel> ConfigurationOptions { get; set; }

        public ConfigurationQueryModel()
        {
            this.ConfigurationOptions = new List<ConfigurationOptionQueryModel>();
        }        
    }
}