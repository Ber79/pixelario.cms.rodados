﻿using System;
namespace Pixelario.CMS.Application.QueryModels.Configurations
{
    public partial class ConfigurationOptionQueryModel
    {
        public int IDConfigurationOption { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Value { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }
        public ConfigurationQueryModel Configuration { get; set; }

        public ConfigurationOptionQueryModel()
        {

        }
    }
}