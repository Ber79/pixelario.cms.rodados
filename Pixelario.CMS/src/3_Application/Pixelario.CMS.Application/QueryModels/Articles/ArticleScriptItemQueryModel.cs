﻿using Pixelario.CMS.Domain.Commons;
using System;

namespace Pixelario.CMS.Application.QueryModels.Articles
{
    public partial class ArticleScriptItemQueryModel
    {
        public int IDArticleScriptItem { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public MultimediaType MultimediaType { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }
        public ArticleScriptQueryModel ArticleScript { get; set; }
    }
}