﻿using System;

namespace Pixelario.CMS.Application.QueryModels.Articles
{
    public partial class ArticleScriptQueryModel
    {
        public int IDArticleScript { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public bool Enabled { get; set; }
        public int Order { get; set; }
    }
}