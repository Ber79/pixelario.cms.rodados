﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Application.QueryModels.Editions;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.QueryModels.Articles
{
    public partial class ArticleQueryModel
    {
        public int IDArticle { get; set; }
        public EditionQueryModel Edition { get; set; }
        public List<TopicQueryModel> Topics { get; set; }
        public List<SubTopicQueryModel> SubTopics { get; set; }
        public List<ArticleScriptQueryModel> ArticleScripts { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? PublicationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Source { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public int OrderAtEdition { get; set; }
        public ArticlePlaceAtHome PlaceAtHome { get; set; }
        public string HomeImage { get; set; }
        public string SlideImage { get; set; }
        public string VerticalImage { get; set; }
        public string HorizontalImage { get; set; }
        public string Keywords { get; set; }
        public List<ArticleMultimediaQueryModel> Multimedia { get; set; }

    }
}