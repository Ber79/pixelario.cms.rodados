﻿using System.Collections.Generic;

namespace Pixelario.CMS.Application.QueryModels.Articles

{
    public partial class ArticleMultimediaQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string ClaseDeEnabled
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string imageGrilla
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string altGrilla
        {
            get
            {
                return this.Enabled ? "Multimedio habilitado" : "Multimedio deshabilitado";
            }
        }
        public string valueGrilla
        {
            get
            {
                return this.Enabled ? "Habilitar el multimedio" : "Deshabilitar el multimedio";
            }
        }
        public string OnclickEliminar
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna.AtHome
        public string AtHomeButtonOnclic
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada {0}?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Multimedio en portada" : "Multimedio fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner multimedio en portada" : "Sacar multimedio de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Enabled
        public string OnclickHabilitar
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }

        public string ImgHabilitar
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string TitleHabilitar
        {
            get
            {
                return this.Enabled ? "Multimedio habilitado" : "Multimedio deshabilitado";
            }
        }
        public string ValueHabilitar
        {
            get
            {
                return this.Enabled ? "Habilitar multimedio" : "Deshabilitar multimedio";
            }
        }
        #endregion    

    }
}
