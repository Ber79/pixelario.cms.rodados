﻿using Pixelario.CMS.Application.Tools;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.QueryModels.Articles
{
    public partial class ArticleQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string EnabledButtonClass
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string EnabledButtonImage
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string EnabledButtonAltProperty
        {
            get
            {
                return this.Enabled ? "Artículo habilitado" : "Artículo Deshabilitado";
            }
        }
        public string EnabledButtonValue
        {
            get
            {
                return this.Enabled ? "Habilitar el artículo" : "Deshabilitar el artículo";
            }
        }
        public string DeleteButtonOnClic
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna AtHome
        public string AtHomeButtonOnclic
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada {0}?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Artículo en portada" : "Artículo fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner artículo en portada" : "Sacar artículo de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Enable
        public string EnabledButtonOnclic
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }
        
        public string EnabledButtonTitle
        {
            get
            {
                return this.Enabled ? "Artículo habilitad" : "Artículo Deshabilitado";
            }
        }        
        #endregion

        public string HomeImage450
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage) ? this.HomeImage.Replace("/Crudas/", "/450/") : this.HomeImage;
            }
        }
        public string GetVerticalImage(string width)
        {
            if (string.IsNullOrEmpty(this.VerticalImage))
                return null;
            return this.VerticalImage.Replace("/80/",
                string.Format("/{0}/", width));

        }
        public string GetHomeImage(string width)
        {
            if (string.IsNullOrEmpty(this.HomeImage)) return null;
            return this.HomeImage.Replace("/80/",
                string.Format("/{0}/", width));

        }

        public bool HasLongTitle
        {
            get
            {
                return this.Title.Length >= 40;
            }
        }


        public string Slug
        {
            get
            {
                return ToolsForURL.GenerateSlug(this.Title);
            }
        }
        public string URL
        {
            get
            {
                return string.Format("/articles/{0}/{1}",
                    this.IDArticle,
                    this.Slug);
            }
        }
    }
}
