﻿using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using System;

namespace Pixelario.CMS.Application.QueryModels.Advertisments
{
    public partial class AdvertismentQueryModel
    {
        public int IDAdvertisment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? PublicationStartDate { get; set; }
        public DateTime? PublicationEndDate { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string URL { get; set; }
        public bool Enabled { get; set; }
        public bool Published { get; set; }
        public int Order { get; set; }
        public AdvertismentPlaceAtWeb PlaceAtWeb { get; set; }
        public string HomeImage { get; set; }
        public string VerticalImage { get; set; }
        public string HorizontalImage { get; set; }
    }
}