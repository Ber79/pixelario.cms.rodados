﻿using Pixelario.CMS.Application.Tools;

namespace Pixelario.CMS.Application.QueryModels.Pages
{
    public partial class PageQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string EnabledButtonClass
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string EnableButtonImage
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string EnableButtonAlt
        {
            get
            {
                return this.Enabled ? "Página habilitada" : "Página deshabilitada";
            }
        }
        public string EnableButtonValue
        {
            get
            {
                return this.Enabled ? "Habilitar página" : "Deshabilitar página";
            }
        }
        public string EnableButtonTitle
        {
            get
            {
                return this.Enabled ? "Habilitar página" : "Deshabilitar página";
            }
        }
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna AtHome
        public string AtHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada {0}?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Página en portada" : "Página fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner página en portada" : "Sacar página de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Enabled
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }

        public string ImgHabilitar
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }

        #endregion

        #region Atributos de la columna On menu
        public string OnMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar del menú {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar al menú {0}?') == false) return false;", this.Title);
            }
        }
        public string OnMenuButtonImage
        {
            get
            {
                return this.OnMenu ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string OnMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Página en el menú" : "Página fuera del menú";
            }
        }
        public string OnMenuButtonValue
        {
            get
            {
                return this.OnMenu ? "Poner la página en el menú" : "Sacar la página del menú";
            }
        }
        #endregion



        public string Foto450
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage) ? this.HomeImage.Replace("/Crudas/", "/450/") : this.HomeImage;
            }
        }
        public string URL
        {
            get
            {
                if (string.IsNullOrEmpty(this.Title))
                    return "#";
                else
                    return string.Format("/pages/{0}/{1}",
                        this.IDPage,
                        ToolsForURL.GenerateSlug(this.Title));
            }
        }
    }
}
