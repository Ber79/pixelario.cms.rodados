﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.QueryModels.Pages
{
    public partial class PageQueryModel
    {
        public int IDPage { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }
        public string HomeImage { get; set; }        
        public string Keywords { get; set; }
        public List<EditionQueryModel> Editions { get; set; }
        public PageQueryModel()
        {

        }
        public PageQueryModel(int iDPage, string title)
        {
            this.IDPage = iDPage;
            this.Title = title;
        }
    }
}