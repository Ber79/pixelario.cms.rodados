﻿using System;

namespace Pixelario.CMS.Application.QueryModels.Pages
{
    public partial class PageMultimediaQueryModel
    {
        public int IDMultimedia { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int IDPage { get; set; }
        public int IDMultimediaType { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Path1 { get; set; }
        public string Path2 { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public bool Cover { get; set; }
        public PageQueryModel Page { get; set; }
    }
}