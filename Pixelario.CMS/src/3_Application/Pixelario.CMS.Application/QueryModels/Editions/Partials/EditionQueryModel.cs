﻿using Pixelario.CMS.Application.Tools;
namespace Pixelario.CMS.Application.QueryModels.Editions
{
    public partial class EditionQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string EnabledButtonClass
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string EnabledButtonImage
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string EnabledButtonAlt
        {
            get
            {
                return this.Enabled ? "Contenido habilitado" : "Contenido deshabilitado";
            }
        }
        public string EnabledButtonValue
        {
            get
            {
                return this.Enabled ? "Habilitar contenido" : "Deshabilitar contenido";
            }
        }
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna Portada
        public string AtHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada {0}?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTittle
        {
            get
            {
                return this.AtHome ? "Contenido en portada" : "Contenido fuera de portada";
            }
        }
        public string AtHomeButtonAlt
        {
            get
            {
                return this.AtHome ? "Contenido en portada" : "Contenido fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner contenido en portada" : "Sacar contenido de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Habilitado
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }

        public string EnabledButtonTitle
        {
            get
            {
                return this.Enabled ? "Contenido Habilitado" : "Contenido Deshabilitado";
            }
        }
     
        #endregion

        public string Foto450
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage) ? this.HomeImage.Replace("/Crudas/", "/450/") : this.HomeImage;
            }
        }

        public string Slug
        {
            get
            {
                return ToolsForURL.GenerateSlug(this.Title);
            }
        }

    }
}
