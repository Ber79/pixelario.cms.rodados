﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.QueryModels.Pages;
using Pixelario.CMS.Application.QueryModels.Topics;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Application.QueryModels.Editions
{
    public partial class EditionQueryModel
    {
        public int IDEdition { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }

        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }


        public int IDEditionType { get; set; }
        public EditionType EditionType
        {
            get
            {
                return EditionType.From(this.IDEditionType);
            }
        }
        public string HomeImage { get; set; }
        public string Keywords { get; set; }
        public List<TopicQueryModel> Topics { get; set; }
        public IList<PageQueryModel> Pages { get; set; }
        public EditionQueryModel()
        {
            this.Pages = new List<PageQueryModel>();
        }
        public EditionQueryModel(int iDEdition, string title)
        {
            this.IDEdition = iDEdition;
            this.Title = title;
        }
        public EditionQueryModel(EditionMappingMode mappingMode,
            dynamic editionRegister)
        {
            this.IDEdition = editionRegister.IDEdition;
            this.CreatedBy = editionRegister.CreatedBy;
            this.CreationDate = editionRegister.CreationDate;
            this.Title = editionRegister.Title;
            this.Summary = editionRegister.Summary;
            this.Keywords = editionRegister.Keywords;
            this.Enabled = editionRegister.Enabled;
            this.AtHome = editionRegister.AtHome;
            this.Order = editionRegister.Order;
            this.IDEditionType = editionRegister.EditionType;
            this.OnMenu = editionRegister.OnMenu;
            this.OrderAtMenu = editionRegister.OrderAtMenu;
            this.HomeImage = editionRegister.HomeImage;
            this.Pages = new List<PageQueryModel>();
            if (mappingMode == EditionMappingMode.ListWithPageMapping)
            {
                if (editionRegister.IDPage != null)
                {
                    this.Pages.Add(
                        new PageQueryModel(
                            iDPage: editionRegister.IDPage,
                            title: editionRegister.PageTitle
                        ));
                }
            }
        }
        public void AddPage(dynamic editionRegister)
        {
            if (editionRegister.IDPage != null)
            {
                this.Pages.Add(
                    new PageQueryModel(
                        iDPage: editionRegister.IDPage,
                        title: editionRegister.PageTitle
                    ));
            }
        }

    }
    public enum EditionMappingMode
    {
        GetMapping,
        ListMapping,
        ListWithPageMapping
    }
}