﻿using Pixelario.CMS.Application.QueryModels.Editions;
using System;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.QueryModels.Topics
{
    public partial class TopicQueryModel
    {
        public int IDTopic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }

        public string HomeImage { get; set; }        
        public string Keywords { get; set; }
        public IList<EditionQueryModel> Editions { get; set; }
        public List<SubTopicQueryModel> SubTopics { get; set; }
        public TopicQueryModel()
        {
            this.SubTopics = new List<SubTopicQueryModel>();
        }
    }
}