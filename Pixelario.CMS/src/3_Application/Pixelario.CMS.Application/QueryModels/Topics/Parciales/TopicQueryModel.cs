﻿using Pixelario.CMS.Application.Tools;
using System.Collections.Generic;
namespace Pixelario.CMS.Application.QueryModels.Topics
{
    public partial class TopicQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string EnableButtonClass
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string EnableButtonImage
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string EnableButtonAlt
        {
            get
            {
                return this.Enabled ? "Tema habilitado" : "Tema deshabilitado";
            }
        }
        public string EnableButtonValue
        {
            get
            {
                return this.Enabled ? "Habilitar tema" : "Deshabilitar tema";
            }
        }
        public string DeleteButtonOnClic
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna Portada
        public string AtHomeButtonClic
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar de la portada {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar a la portada {0}?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Tema en portada" : "Tema fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner tema en portada" : "Sacar tema de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Habilitado
        public string EnableButtonOnclic
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }



        #endregion

        #region Atributos de la columna On menu
        public string OnMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar del menú {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar al menú {0}?') == false) return false;", this.Title);
            }
        }
        public string OnMenuButtonImage
        {
            get
            {
                return this.OnMenu ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string OnMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Página en el menú" : "Página fuera del menú";
            }
        }
        public string OnMenuButtonValue
        {
            get
            {
                return this.OnMenu ? "Poner el tema en el menú" : "Sacar el tema del menú";
            }
        }
        #endregion

        public string Foto450
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage) ? this.HomeImage.Replace("/Crudas/", "/450/") : this.HomeImage;
            }
        }
        public string URL
        {
            get
            {
                return string.Format("/topics/{0}/{1}",
                    this.IDTopic,
                    ToolsForURL.GenerateSlug(this.Title));
            }
        }

    }
}
