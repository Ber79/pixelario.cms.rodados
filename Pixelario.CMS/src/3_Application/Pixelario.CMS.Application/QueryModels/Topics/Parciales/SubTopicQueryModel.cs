﻿using Pixelario.CMS.Application.Tools;
namespace Pixelario.CMS.Application.QueryModels.Topics
{
    public partial class SubTopicQueryModel
    {
        /// <summary>
        /// Clase que tendra el boton de habilitar.
        /// </summary>
        public string EnabledButtonClass
        {
            get
            {
                return this.Enabled ? "boton_grilla tick" : "boton_grilla cancel";
            }
        }
        public string EnabledButtonImage
        {
            get
            {
                return this.Enabled ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string EnabledButtonAlt
        {
            get
            {
                return this.Enabled ? "SubTema habilitado" : "SubTema deshabilitado";
            }
        }
        public string EnabledButtonValue
        {
            get
            {
                return this.Enabled ? "Habilitar subtema" : "Deshabilitar subtema";
            }
        }
        public string DeleteButtonOnClick
        {
            get
            {
                return string.Format("if (confirm('¿Está seguro de eliminar {0}?') == false) return false;", this.Title);
            }
        }
        #region Atributos de la columna AtHome
        public string AtHomeButtonOnClick
        {
            get
            {
                return this.AtHome ?
                    string.Format("if (confirm('¿Está seguro de sacar {0} de la pantalla de tema?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar {0} a la pantalla de tema?') == false) return false;", this.Title);
            }
        }
        public string AtHomeButtonImage
        {
            get
            {
                return this.AtHome ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string AtHomeButtonTitle
        {
            get
            {
                return this.AtHome ? "Contenido en portada" : "Contenido fuera de portada";
            }
        }
        public string AtHomeButtonValue
        {
            get
            {
                return this.AtHome ? "Poner contenido en portada" : "Sacar contenido de la portada";
            }
        }
        #endregion
        #region Atributos de la columna Enabled
        public string EnabledButtonOnClick
        {
            get
            {
                return this.Enabled ?
                    string.Format("if (confirm('¿Está seguro de deshabilitar {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de habilitar {0}?') == false) return false;", this.Title);
            }
        }
       
        public string EnabledButtonTitle
        {
            get
            {
                return this.Enabled ? "Contenido Enabled" : "Contenido Deshabilitado";
            }
        }
        #endregion
        #region Atributos de la columna On menu
        public string OnMenuButtonOnClick
        {
            get
            {
                return this.OnMenu ?
                    string.Format("if (confirm('¿Está seguro de sacar del menú {0}?') == false) return false;", this.Title) :
                    string.Format("if (confirm('¿Está seguro de agregar al menú {0}?') == false) return false;", this.Title);
            }
        }
        public string OnMenuButtonImage
        {
            get
            {
                return this.OnMenu ? "/img/icons/tick.png" : "/img/icons/cancel.png";
            }
        }
        public string OnMenuButtonTitle
        {
            get
            {
                return this.OnMenu ? "Subtema en el menú" : "Subtema fuera del menú";
            }
        }
        public string OnMenuButtonValue
        {
            get
            {
                return this.OnMenu ? "Poner el subtema en el menú" : "Sacar el subtema del menú";
            }
        }
        #endregion

        public string HomeImage450
        {
            get
            {
                return !string.IsNullOrEmpty(this.HomeImage) ? this.HomeImage.Replace("/raw/", "/450/") : this.HomeImage;
            }
        }
        public string URL
        {
            get
            {
                return string.Format("/subtopics/{0}/{1}",
                    this.IDSubTopic,
                    ToolsForURL.GenerateSlug(this.Title));
            }
        }

    }
}
