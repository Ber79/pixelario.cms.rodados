﻿using System;

namespace Pixelario.CMS.Application.QueryModels.Topics
{
    public partial class SubTopicQueryModel
    {
        public int IDSubTopic { get; set; }

        public int IDTopic { get; set; }
        public TopicQueryModel Topic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }        
        public string HomeImage { get; set; }        
        public string Keywords { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }

    }
}