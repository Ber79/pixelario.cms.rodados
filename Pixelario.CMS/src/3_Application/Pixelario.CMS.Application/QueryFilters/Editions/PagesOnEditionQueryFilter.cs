﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Application.QueryFilters.Editions
{
    public class PagesOnEditionQueryFilter
    {
        public bool? PagesAtHome { get; set; }
        public bool? PagesOnMenu { get; set; }

        public PagesOnEditionQueryFilter(bool? pagesAtHome,
            bool? pagesOnMenu)
        {
            PagesAtHome = pagesAtHome;
            PagesOnMenu = pagesOnMenu;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + (!this.PagesAtHome.HasValue ? 0 :
                this.PagesAtHome.GetHashCode());
            hash = hash * 23 + (!this.PagesOnMenu.HasValue ? 0 :
                this.PagesOnMenu.GetHashCode());
            return hash;
        }

        public string GetJoin()
        {
            string join = default(string);
            string where = default(string);

            List<string> whereStatments = new List<string>();
            if (PagesAtHome.HasValue)
            {
                if (PagesAtHome.Value)
                    whereStatments.Add("AtHome = 1");
                else
                    whereStatments.Add("AtHome = 0");
            }
            if (PagesOnMenu.HasValue)
            {
                if (PagesOnMenu.Value)
                    whereStatments.Add("OnMenu = 1");
                else
                    whereStatments.Add("OnMenu = 0");
            }
            if (whereStatments.Any())
            {
                where = "WHERE " + String.Join(" AND ", whereStatments);
            }
            join = @"LEFT JOIN (
                    SELECT pe.IDEdition as IDEdition, p.* FROM PageEditions AS pe
                    INNER JOIN Pages as p
                    ON pe.IDPage = p.IDPage " + where +
            ") AS p ON e.IDEdition = p.IDEdition";
            return join;
        }

        public string GetColumns()
        {
            return @", p.IDPage AS IDPage, p.Title AS PageTitle";
        }
    }
}