﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Pixelario.CMS.Domain.Events.Advertisments;

namespace Pixelario.CMS.Application.DomainEventHandlers.Advertisments
{
    public class OrderChangedDomainEventHandler : INotificationHandler<OrderChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IAdvertismentRepository _advertismentRepository;
        public OrderChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IAdvertismentRepository advertismentRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
        }
        public async Task Handle(OrderChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder advertisments on Domain Event.");
            _advertismentRepository.ReorderAdvertismentsAtPlace(domainEvent.Advertisment,
                domainEvent.LastOrder);
            await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}