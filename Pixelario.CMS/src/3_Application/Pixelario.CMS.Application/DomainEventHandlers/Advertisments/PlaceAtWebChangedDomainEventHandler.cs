﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Events.Advertisments;
using Pixelario.CMS.Application.Queries.Advertisments;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Advertisments
{
    public class PlaceAtWebChangedDomainEventHandler : INotificationHandler<PlaceAtWebChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IAdvertismentRepository _advertismentRepository;
        private readonly IAdvertismentsQueries _advertismentsQueries;

        public PlaceAtWebChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IAdvertismentRepository advertismentRepository,
            IAdvertismentsQueries advertismentsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _advertismentRepository = advertismentRepository ?? throw new ArgumentNullException(nameof(advertismentRepository));
            _advertismentsQueries = advertismentsQueries ?? throw new ArgumentNullException(nameof(advertismentsQueries));
        }
        public async Task Handle(PlaceAtWebChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.LastPlaceAtWeb == AdvertismentPlaceAtWeb.None.Id)
            {
                _logger.Information("Give order to advertisment with id {0} on Domain Event.",
                    domainEvent.Advertisment.ID);
                _advertismentRepository.OrderAdvertismentsAtPlace(
                    placeAtWeb: domainEvent.LastPlaceAtWeb);
                domainEvent.Advertisment.Order = await _advertismentsQueries.CountAsync(
                    placeAtWeb: AdvertismentPlaceAtWeb.From(domainEvent.Advertisment.IDPlaceAtWeb),
                    enabled: true,
                    published: true,
                    filteredBy: null);
                _advertismentRepository.Update(domainEvent.Advertisment);
            }
            else
            {
                _logger.Information("Order advertisments on Domain Event.");
                _advertismentRepository.OrderAdvertismentsAtPlace(
                    placeAtWeb: domainEvent.LastPlaceAtWeb);
                if (domainEvent.Advertisment.IDPlaceAtWeb !=
                    AdvertismentPlaceAtWeb.None.Id)
                {
                    domainEvent.Advertisment.Order = await _advertismentsQueries.CountAsync(
                        placeAtWeb: AdvertismentPlaceAtWeb.From(domainEvent.Advertisment.IDPlaceAtWeb),
                        enabled: true,
                        published: true,
                        filteredBy: null);
                    _advertismentRepository.Update(domainEvent.Advertisment);
                }
            }
            await _advertismentRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}