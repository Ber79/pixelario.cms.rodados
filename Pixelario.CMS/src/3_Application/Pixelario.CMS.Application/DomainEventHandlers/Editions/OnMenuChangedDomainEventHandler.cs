﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.Queries.Editions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.Events.Editions;
using System.Collections.Generic;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Editions
{
    public class OnMenuChangedDomainEventHandler : INotificationHandler<OnMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IEditionRepository _editionRepository;
        private readonly IEditionsQueries _editionsQueries;
        public OnMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IEditionRepository editionRepository,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
            _editionsQueries = editionsQueries ?? throw new ArgumentNullException(nameof(editionsQueries));
        }
        public async Task Handle(OnMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Edition.OnMenu)
            {
                _logger.Information("Give order on menu to edition with id {0} on Domain Event.",
                    domainEvent.Edition.ID);
                domainEvent.Edition.OrderAtMenu = await _editionsQueries.CountAsync(
                    filteredBy: null,
                    enabled: true,
                    atHome: null,
                    onMenu: true,
                    editionTypes: new List<EditionType>() {
                        EditionType.From(domainEvent.Edition.EditionType)
                    });
                _editionRepository.Update(domainEvent.Edition);
            }
            else
            {
                _logger.Information("Reorder on menu editions on Domain Event.");
                _editionRepository.OrderMenuOnTypes(
                    iDEditionException: domainEvent.Edition.ID);
            }
            await _editionRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}