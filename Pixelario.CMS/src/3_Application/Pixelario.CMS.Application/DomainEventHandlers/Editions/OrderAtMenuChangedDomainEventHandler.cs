﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Events.Editions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Editions
{
    public class OrderAtMenuChangedDomainEventHandler : INotificationHandler<OrderAtMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IEditionRepository _editionRepository;
        public OrderAtMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task Handle(OrderAtMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder editions at menu on Domain Event.");
            _editionRepository.ReOrderMenuOnType(domainEvent.Edition, 
                domainEvent.LastOrder);
            await _editionRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}