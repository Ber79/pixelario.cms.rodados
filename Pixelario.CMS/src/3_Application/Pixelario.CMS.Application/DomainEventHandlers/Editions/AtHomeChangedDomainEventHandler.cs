﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Application.Queries.Editions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.Events.Editions;
using System.Collections.Generic;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Editions
{
    public class AtHomeChangedDomainEventHandler : INotificationHandler<AtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IEditionRepository _editionRepository;
        private readonly IEditionsQueries _editionsQueries;
        public AtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IEditionRepository editionRepository,
            IEditionsQueries editionsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
            _editionsQueries = editionsQueries ?? throw new ArgumentNullException(nameof(editionsQueries));
        }
        public async Task Handle(AtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Edition.AtHome)
            {
                _logger.Information("Give order to edition with id {0} on Domain Event.",
                    domainEvent.Edition.ID);
                domainEvent.Edition.Order = await _editionsQueries.CountAsync(
                    filteredBy: null,
                    enabled: true,
                    atHome: true,
                    onMenu: null,
                    editionTypes: new List<EditionType>() {
                        EditionType.From(domainEvent.Edition.EditionType)
                    });
                _editionRepository.Update(domainEvent.Edition);
            }
            else
            {
                _logger.Information("Reorder editions on Domain Event.");
                _editionRepository.OrderOnTypes(
                    iDEditionException: domainEvent.Edition.ID);
            }
            await _editionRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}