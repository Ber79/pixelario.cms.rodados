﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Events.Editions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Editions
{
    public class OrderChangedDomainEventHandler : INotificationHandler<OrderChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IEditionRepository _editionRepository;
        public OrderChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IEditionRepository editionRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _editionRepository = editionRepository ?? throw new ArgumentNullException(nameof(editionRepository));
        }
        public async Task Handle(OrderChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder editions on Domain Event.");
            _editionRepository.ReorderOnType(domainEvent.Edition, 
                domainEvent.LastOrder);
            await _editionRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}