﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.Events.Topics;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class SubTopicOnMenuChangedDomainEventHandler : INotificationHandler<SubTopicOnMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        private readonly ISubTopicsQueries _subTopicsQueries;
        public SubTopicOnMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository,
            ISubTopicsQueries subTopicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _subTopicsQueries = subTopicsQueries ?? throw new ArgumentNullException(nameof(subTopicsQueries));
        }
        public async Task Handle(SubTopicOnMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            var topic = await _topicRepository.GetAsync(
                iDTopic: domainEvent.SubTopic.IDTopic);
            if (domainEvent.SubTopic.OnMenu)
            {
                foreach (var subTopic in topic.SubTopics)
                {
                    if (subTopic.ID == domainEvent.SubTopic.ID)
                    {
                        _logger.Information("Give order on menu to subtopic with id {0} on Domain Event.",
                            domainEvent.SubTopic.ID);
                        subTopic.OrderAtMenu = await _subTopicsQueries.CountAsync(
                                iDTopic: domainEvent.SubTopic.IDTopic,
                                filteredBy: null,
                                enabled: true,
                                atHome: null,
                                onMenu: true);
                    }
                }
                _topicRepository.Update(topic);
            }
            else
            {
                _logger.Information("Reorder subtopic of topic with id {0} at menu on Domain Event.",
                    topic.ID);
                await _topicRepository.OrderSubTopicsAtMenuAsync(iDTopic: topic.ID);
            }
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}