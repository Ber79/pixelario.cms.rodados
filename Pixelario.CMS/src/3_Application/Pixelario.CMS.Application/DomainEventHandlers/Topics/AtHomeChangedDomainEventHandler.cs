﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.Events.Topics;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class AtHomeChangedDomainEventHandler : INotificationHandler<AtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        private readonly ITopicsQueries _topicsQueries;
        public AtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository,
            ITopicsQueries topicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _topicsQueries = topicsQueries ?? throw new ArgumentNullException(nameof(topicsQueries));
        }
        public async Task Handle(AtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Topic.AtHome)
            {
                _logger.Information("Give order to topic with id {0} on Domain Event.",
                    domainEvent.Topic.ID);
                domainEvent.Topic.Order = await _topicsQueries
                    .CountAsync(
                    iDEdition: null,
                    filteredBy: null,
                    enabled: true,
                    atHome: true,
                    onMenu: null);
                _topicRepository.Update(domainEvent.Topic);
            }
            else
            {
                _logger.Information("Reorder topics on Domain Event.");
                _topicRepository.OrderTopicsAtHome();
            }
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}