﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Events.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class OrderAtMenuChangedDomainEventHandler : INotificationHandler<OrderAtMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        public OrderAtMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task Handle(OrderAtMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder topics at menu on Domain Event.");
            _topicRepository.ReorderTopicsAtMenu(domainEvent.Topic, domainEvent.LastOrder);
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}