﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Events.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class OrderAtHomeChangedDomainEventHandler : INotificationHandler<OrderAtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        public OrderAtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task Handle(OrderAtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder topics at home on Domain Event.");
            _topicRepository.ReorderTopicsAtHome(domainEvent.Topic, domainEvent.LastOrder);
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}