﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.Events.Topics;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class SubTopicAtHomeChangedDomainEventHandler : INotificationHandler<SubTopicAtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        private readonly ISubTopicsQueries _subTopicsQueries;
        public SubTopicAtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository,
            ISubTopicsQueries subTopicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _subTopicsQueries = subTopicsQueries ?? throw new ArgumentNullException(nameof(subTopicsQueries));
        }
        public async Task Handle(SubTopicAtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            var topic = await _topicRepository.GetAsync(
                iDTopic: domainEvent.SubTopic.IDTopic);
            if (domainEvent.SubTopic.AtHome)
            {
                foreach (var subTopic in topic.SubTopics)
                {
                    if (subTopic.ID == domainEvent.SubTopic.ID)
                    {
                        _logger.Information("Give order to subtopic with id {0} on Domain Event.",
                            domainEvent.SubTopic.ID);
                        subTopic.Order = await _subTopicsQueries.CountAsync(
                                iDTopic: domainEvent.SubTopic.IDTopic,
                                filteredBy: null,
                                enabled: true,
                                atHome: true,
                                onMenu: null);
                    }
                }
                _topicRepository.Update(topic);
            }
            else
            {
                _logger.Information("Reorder subtopics on Domain Event.");
                await _topicRepository.OrderSubTopicsAtHomeAsync(iDTopic: topic.ID);
            }
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();

        }
    }
}