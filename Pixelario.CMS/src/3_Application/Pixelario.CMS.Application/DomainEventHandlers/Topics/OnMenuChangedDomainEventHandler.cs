﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Events.Topics;
using Pixelario.CMS.Application.Queries.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class OnMenuChangedDomainEventHandler : INotificationHandler<OnMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        private readonly ITopicsQueries _topicsQueries;
        public OnMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository,
            ITopicsQueries topicsQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _topicsQueries = topicsQueries ?? throw new ArgumentNullException(nameof(topicsQueries));
        }
        public async Task Handle(OnMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Topic.OnMenu)
            {
                _logger.Information("Give order on menu to topic with id {0} on Domain Event.",
                    domainEvent.Topic.ID);       
                domainEvent.Topic.OrderAtMenu = await _topicsQueries
                    .CountAsync(
                        iDEdition: null,
                        filteredBy: null,
                        enabled: true,
                        atHome: null, 
                        onMenu: true);
                _topicRepository.Update(domainEvent.Topic);
            }
            else
            {
                _logger.Information("Reorder topic at menu on Domain Event.");
                _topicRepository.OrderTopicsAtMenu();
            }
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}