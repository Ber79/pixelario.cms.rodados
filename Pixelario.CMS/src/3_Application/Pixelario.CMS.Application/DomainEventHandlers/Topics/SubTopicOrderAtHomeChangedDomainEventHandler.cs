﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Events.Topics;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Topics
{
    public class SubTopicOrderAtHomeChangedDomainEventHandler : INotificationHandler<SubTopicOrderAtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly ITopicRepository _topicRepository;
        public SubTopicOrderAtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            ITopicRepository topicRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
        }
        public async Task Handle(SubTopicOrderAtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder subtopics at home on Domain Event.");
            await _topicRepository.ReorderSubTopicsAtHome(
                subtopic: domainEvent.SubTopic,
                lastOrder: domainEvent.LastOrder);
            await _topicRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}