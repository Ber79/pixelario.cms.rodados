﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Events.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Articles
{
    public class OrderChangedDomainEventHandler : INotificationHandler<OrderChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IArticleRepository _articleRepository;
        public OrderChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IArticleRepository articleRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
        }
        public async Task Handle(OrderChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder articles on Domain Event.");
            if (domainEvent.Article.Edition.EditionType ==
                EditionType.Especial_Content.Id)
            {
                _articleRepository.ReorderArticlesAtHomeByEdition(domainEvent.Article,
                    domainEvent.LastOrder);
            }
            else
            {
                _articleRepository.ReorderArticlesAtPlace(domainEvent.Article,
                    domainEvent.LastOrder);
            }
            await _articleRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}