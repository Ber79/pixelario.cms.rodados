﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Events.Articles;
using Pixelario.CMS.Application.Queries.Articles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Articles
{
    public class PlaceAtHomeChangedDomainEventHandler : INotificationHandler<PlaceAtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IArticleRepository _articleRepository;
        private readonly IArticlesQueries _articlesQueries;

        public PlaceAtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IArticleRepository articleRepository,
            IArticlesQueries articlesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
            _articlesQueries = articlesQueries ?? throw new ArgumentNullException(nameof(articlesQueries));
        }
        public async Task Handle(PlaceAtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.LastPlaceAtHome == ArticlePlaceAtHome.None.Id)
            {
                _logger.Information("Give order to article with id {0} on Domain Event.",
                    domainEvent.Article.ID);
                if (domainEvent.Article.Edition.EditionType == EditionType.Especial_Content.Id)
                {
                    domainEvent.Article.Order = await _articlesQueries.CountAsync(
                        iDEdition: domainEvent.Article.IDEdition,
                        iDTopic: null,
                        iDSubTopic: null,
                        enabled: true,
                        atHome: true,
                        placesAtHome: null,
                        filteredBy: null);
                }
                else
                {
                    _articleRepository.OrderArticlesAtPlace(
                        placeAtHome: domainEvent.LastPlaceAtHome);
                    domainEvent.Article.Order = await _articlesQueries.CountAsync(
                        iDEdition: null,
                        iDTopic: null,
                        iDSubTopic: null,
                        enabled: true,
                        atHome: true,
                        placesAtHome: ArticlePlaceAtHome.From(domainEvent.Article.IDPlaceAtHome),
                        filteredBy: null);
                }
                _articleRepository.Update(domainEvent.Article);
            }
            else if (domainEvent.Article.IDPlaceAtHome !=
                ArticlePlaceAtHome.None.Id)
            {
                _logger.Information("Reorder articles on Domain Event.");
                if (domainEvent.Article.Edition.EditionType == EditionType.Especial_Content.Id)
                {
                    _articleRepository.OrderArticlesAtHomeByEdition(
                        iDEdition: domainEvent.Article.IDEdition);
                }
                else
                {
                    _articleRepository.OrderArticlesAtPlace(
                        placeAtHome: domainEvent.LastPlaceAtHome);
                    _articleRepository.ReorderArticlesAtPlace(
                        article: domainEvent.Article,
                        LastOrder: int.MaxValue);
                }
            }
            else
            {
                if (domainEvent.Article.Edition.EditionType ==
                    EditionType.Especial_Content.Id)
                {
                    _articleRepository.OrderArticlesAtHomeByEdition(
                        iDEdition: domainEvent.Article.IDEdition);
                }
                else
                {
                    _articleRepository.OrderArticlesAtPlace(
                        placeAtHome: domainEvent.LastPlaceAtHome);
                }
            }
            await _articleRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}