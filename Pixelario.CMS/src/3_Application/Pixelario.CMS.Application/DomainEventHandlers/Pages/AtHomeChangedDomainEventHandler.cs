﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Events.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Pages
{
    public class AtHomeChangedDomainEventHandler : INotificationHandler<AtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IPageRepository _pageRepository;
        private readonly IPagesQueries _pagesQueries;
        public AtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IPageRepository pageRepository,
            IPagesQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _pagesQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task Handle(AtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Page.AtHome)
            {
                _logger.Information("Give order at home to page with id {0} on Domain Event.",
                    domainEvent.Page.ID);
                domainEvent.Page.Order = await _pagesQueries
                    .CountAsync(
                        iDEdition: null,
                        filteredBy: null,
                        enabled: true,
                        atHome: true,
                        onMenu: null);
                _pageRepository.Update(domainEvent.Page);
            }
            else
            {
                _logger.Information("Reorder at home pages on Domain Event.");
                _pageRepository.OrderPagesAtHome();
            }
            await _pageRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}