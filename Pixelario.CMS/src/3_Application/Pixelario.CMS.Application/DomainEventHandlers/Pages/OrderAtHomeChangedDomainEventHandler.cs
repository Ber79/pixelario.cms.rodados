﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Events.Pages;
using System;
using System.Threading;
using System.Threading.Tasks;
using Serilog;


namespace Pixelario.CMS.Application.DomainEventHandlers.Pages
{
    public class OrderAtHomeChangedDomainEventHandler : INotificationHandler<OrderAtHomeChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IPageRepository _pageRepository;
        public OrderAtHomeChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IPageRepository pageRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
        }
        public async Task Handle(OrderAtHomeChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            _logger.Information("Reorder pages at home on Domain Event.");
            _pageRepository.ReorderPagesAtHome(domainEvent.Page, domainEvent.LastOrder);
            await _pageRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}