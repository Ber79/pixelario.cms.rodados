﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Events.Pages;
using Pixelario.CMS.Application.Queries.Pages;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Pixelario.CMS.Application.DomainEventHandlers.Pages
{
    public class OnMenuChangedDomainEventHandler : INotificationHandler<OnMenuChangedDomainEvent>
    {
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IPageRepository _pageRepository;
        private readonly IPagesQueries _pagesQueries;
        public OnMenuChangedDomainEventHandler(
            ILogger logger,
            IMediator mediator,
            IPageRepository pageRepository,
            IPagesQueries pagesQueries)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _pageRepository = pageRepository ?? throw new ArgumentNullException(nameof(pageRepository));
            _pagesQueries = pagesQueries ?? throw new ArgumentNullException(nameof(pagesQueries));
        }
        public async Task Handle(OnMenuChangedDomainEvent domainEvent,
            CancellationToken cancellationToken)
        {
            if (domainEvent.Page.OnMenu)
            {
                _logger.Information("Give order on menu to page with id {0} on Domain Event.",
                    domainEvent.Page.ID);
                int? iDEdition = null;
                if (domainEvent.Page.Editions.Any())
                    iDEdition = domainEvent.Page.Editions.First().ID;
                domainEvent.Page.OrderAtMenu = await _pagesQueries
                    .CountAsync(
                        iDEdition: iDEdition,
                        filteredBy: null,
                        enabled: true,
                        atHome: null, onMenu: true);
                _pageRepository.Update(domainEvent.Page);
            }
            else
            {
                if (domainEvent.Page.Editions.Any())
                {
                    _logger.Information("Reorder pages at menu on Domain Event.");
                    _pageRepository.OrderPagesAtMenu(
                            edition: domainEvent.Page.Editions.First());
                }
            }
            await _pageRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}