﻿using Pixelario.CMS.Seedwork;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Accounts
{
    public class Account : Entity, IAggregateRoot
    {
        public int UserId { get; set; }
        public int IDPlan { get;  set; }
        
        public DateTime CreationDate { get; private set; }

        protected Account()
        {
            this.CreationDate = DateTime.Now;
        }
        public Account(int userId, Plan plan) : this()
        {
            this.UserId = userId;
            this.IDPlan = plan.Id;
        }
    }
}
