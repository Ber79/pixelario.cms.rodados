﻿using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Accounts
{
    public interface IAccountRepository : IRepository<Account>
    {
        Account Add(Account account);
        void Update(Account account);
        void Delete(Account account);
        Task<Account> GetAsync(int iDAccount);
    }
}
