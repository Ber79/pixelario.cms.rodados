﻿using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Plans
{
    public class Constraint : Entity, IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public int IDConstraintType { get; set; }
        public int IDPlan { get; set; }
        public int IDConstrained { get; set; }
        protected Constraint()
        {
            this.CreationDate = DateTime.Now;
        }
        public Constraint(string createdBy, Plan plan, 
            ConstraintType constraintType,
            int iDConstrained) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (plan == null)
            {
                throw new ArgumentException("message", nameof(plan));
            }
            if (constraintType == null)
            {
                throw new ArgumentException("message", nameof(constraintType));
            }
            if (iDConstrained <= 0)
            {
                throw new ArgumentException("message", nameof(iDConstrained));
            }
            this.CreatedBy = createdBy;
            this.IDPlan = plan.Id;
            this.IDConstraintType = constraintType.Id;
            this.IDConstrained = iDConstrained;
        }
    }
}
