﻿using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Plans
{
    public class Plan : Enumeration
    {
        public static Plan None =
           new Plan(
               id: 0,
               name: nameof(None).ToLowerInvariant(),
               title: "None",
               enabled: false);
        public static Plan Free =
            new Plan(
                id: 1,
                name: nameof(Free).ToLowerInvariant(),
                title: "Free",
                enabled: true);
        
        public string Title { get; private set; }
        public bool Enabled { get; private set; }
        public override string ToString()
        {
            return this.Id.ToString();
        }
        protected Plan()
        {

        }
        public Plan(int id, string name,
            string title, bool enabled)
            : base(id, name)
        {
            this.Title = title;
            this.Enabled = enabled;
        }
        private static IEnumerable<Plan> _list =
                new[] { None, Free };

        public static List<Plan> List()
        {
            return _list.Where(et => et.Enabled).ToList();
        }
        public static Plan FromName(string name)
        {
            var plan = _list
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (plan == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles planes son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return plan;
        }
        public static Plan From(int id)
        {
            var plan = _list
                .SingleOrDefault(s => s.Id == id);
            if (plan == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles planes son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return plan;
        }
    }
}
