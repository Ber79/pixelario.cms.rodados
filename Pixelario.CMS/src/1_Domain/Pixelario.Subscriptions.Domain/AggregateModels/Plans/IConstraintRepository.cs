﻿using Pixelario.CMS.Seedwork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Plans
{
    public interface IConstraintRepository : IRepository<Constraint>
    {
        Constraint Add(Constraint account);
        void Update(Constraint account);
        void Delete(Constraint account);
        Task<List<Constraint>> ListByConstrainedAsync(int iDConstraintType,
            int iDConstrained);
    }
}
