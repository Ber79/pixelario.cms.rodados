﻿using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.Subscriptions.Domain.AggregateModels.Plans
{
    public class ConstraintType : Enumeration
    {
        public static ConstraintType OnEdition =
           new ConstraintType(
               id: 1,
               name: nameof(OnEdition).ToLowerInvariant(),
               title: "OnEdition",
               enabled: true);
        public static ConstraintType OnArticle =
            new ConstraintType(
                id: 2,
                name: nameof(OnArticle).ToLowerInvariant(),
                title: "OnArticle",
                enabled: true);
        
        public string Title { get; private set; }
        public bool Enabled { get; private set; }
        public override string ToString()
        {
            return this.Id.ToString();
        }
        protected ConstraintType()
        {

        }
        public ConstraintType(int id, string name,
            string title, bool enabled)
            : base(id, name)
        {
            this.Title = title;
            this.Enabled = enabled;
        }
        private static IEnumerable<ConstraintType> _list =
                new[] { OnEdition, OnArticle };

        public static List<ConstraintType> List()
        {
            return _list.Where(et => et.Enabled).ToList();
        }
        public static ConstraintType FromName(string name)
        {
            var plan = _list
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (plan == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Las posibles restricciones son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return plan;
        }
        public static ConstraintType From(int id)
        {
            var plan = _list
                .SingleOrDefault(s => s.Id == id);
            if (plan == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Las posibles restricciones son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return plan;
        }
    }
}
