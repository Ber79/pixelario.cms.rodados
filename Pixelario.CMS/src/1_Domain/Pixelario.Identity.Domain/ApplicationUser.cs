﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Pixelario.Identity.Domain
{
    public class ApplicationUser : IdentityUser<int,
        ApplicationUserLogin, ApplicationUserRole, 
        ApplicationUserClaim>, IUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(
                this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("Picture", this.Picture ?? ""));
            userIdentity.AddClaim(new Claim("FullName", string.Format("{0} {1}",
                this.FirstName, this.LastName)));
            return userIdentity;
        }
    }
}
