﻿namespace Pixelario.CMS.Seedwork
{
    /// <summary>
    /// Autor: Ing. Bernardo Raskovsky
    /// Función: Interfaz para repositorios del modelo agregado
    /// </summary>
    public interface IRepository<T> 
        where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}