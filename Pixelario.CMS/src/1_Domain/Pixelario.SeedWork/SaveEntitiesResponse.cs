﻿using System.Collections.Generic;

namespace Pixelario.CMS.Seedwork
{
    public class SaveEntitiesResponse
    {
        public bool Success { get; set; }
        public IList<string> Errors { get; set; }

    }
}