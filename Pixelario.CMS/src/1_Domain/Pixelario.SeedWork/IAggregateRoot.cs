﻿namespace Pixelario.CMS.Seedwork
{
    /// <summary>
    /// Autor: Ing. Bernardo Raskovsky
    /// Función: Interfaz para marcar la entidad raiz de 
    /// un modelo agregado
    /// </summary>
    public interface IAggregateRoot{}
}
