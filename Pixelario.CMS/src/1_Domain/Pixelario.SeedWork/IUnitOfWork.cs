﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Pixelario.CMS.Seedwork
{
    /// <summary>
    /// Autor: Ing. Bernardo Raskovsky
    /// Función: Interfaz para unidades de trabajo del
    /// modelo repositorio
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SaveEntitiesResponse> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}