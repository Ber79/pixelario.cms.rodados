﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pixelario.CMS.SeedWork
{
    public class CommandResponse
    {
        public bool success { get; set; }
        public List<string> errors { get; set; }
    }
}
