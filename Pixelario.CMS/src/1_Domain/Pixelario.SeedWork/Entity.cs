﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Seedwork
{
    /// <summary>
    /// Autor: Ing. Bernardo Raskovsky
    /// Función:Clase base para Entidades
    /// </summary>
    public abstract class Entity
    {
        int? _requestedHashCode;
        int _iD;
        public virtual int ID
        {
            get
            {
                return _iD;
            }
            protected set
            {
                _iD = value;
            }
        }
        

        private List<INotification> _beforeCommitDomainEvents;
        public IReadOnlyCollection<INotification> BeforeCommitDomainEvents => _beforeCommitDomainEvents?.AsReadOnly();
        private List<INotification> _afterCommitDomainEvents;
        public IReadOnlyCollection<INotification> AfterCommitDomainEvents => _afterCommitDomainEvents?.AsReadOnly();

        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Agrega un evento de dominio a la coleccion de eventos
        /// que se disparan antes del commit
        /// </summary>
        /// <param name="eventItem">Evento de dominio que se agrega</param>
        public void AddBeforeCommitDomainEvent(INotification eventItem)
        {
            _beforeCommitDomainEvents = _beforeCommitDomainEvents ?? new List<INotification>();
            _beforeCommitDomainEvents.Add(eventItem);
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Agrega un evento de dominio a la coleccion de eventos
        /// que se disparan despues del commit
        /// </summary>
        /// <param name="eventItem">Evento de dominio que se agrega</param>
        public void AddAfterCommitDomainEvent(INotification eventItem)
        {
            _afterCommitDomainEvents = _afterCommitDomainEvents ?? new List<INotification>();
            _afterCommitDomainEvents.Add(eventItem);
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Elimina un evento de dominio a la coleccionde eventos
        /// que se disparan antes del commit
        /// </summary>
        /// <param name="eventItem">Evento de dominio que se agrega</param>
        public void RemoveBeforeCommitDomainEvent(INotification eventItem)
        {
            _beforeCommitDomainEvents?.Remove(eventItem);
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Elimina un evento de dominio a la coleccionde eventos
        /// que se disparan despues del commit
        /// </summary>
        /// <param name="eventItem">Evento de dominio que se agrega</param>
        public void RemoveAfterCommitDomainEvent(INotification eventItem)
        {
            _afterCommitDomainEvents?.Remove(eventItem);
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Elimina todos los eventos de dominio de la coleccion de eventos
        /// que se disparan antes del commit
        /// </summary>
        public void ClearBeforeDomainEvents()
        {
            _beforeCommitDomainEvents?.Clear();
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Elimina todos los eventos de dominio de la coleccion de eventos
        /// que se disparan despues del commit
        /// </summary>
        public void ClearAfterDomainEvents()
        {
            _afterCommitDomainEvents?.Clear();
        }
        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Obtiene un valor que indica si la Entidad es transitoria.
        /// </summary>
        /// <returns>Verdadero si es transitoria</returns>
        public bool IsTransient()
        {
            return this.ID == default(Int32);
        }

        /// <summary>
        /// Autor:Ing. Bernardo Raskovsky
        /// Función: Genera y almacena un valor de Hash. Se utiliza para chequear
        /// si dos entidades son distintas.
        /// https://blogs.msdn.microsoft.com/ericlippert/2011/02/28/guidelines-and-rules-for-gethashcode/
        /// </summary>
        /// <returns>Numero Hash</returns>
        public override int GetHashCode()
        {
            if(!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.ID.GetHashCode() ^ 31;
                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();
        }

        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Operador para controlar que un objeto es igual a la entidad
        /// </summary>
        /// <param name="obj">Objeto de comparación</param>
        /// <returns>Verdadero si es igual</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Entity))
                return false;
            if (Object.ReferenceEquals(this, obj))
                return true;
            if (this.GetType() != obj.GetType())
                return false;
            Entity item = (Entity)obj;
            if (item.IsTransient() || this.IsTransient())
                return false;
            else
                return item.ID == this.ID;
        }
        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Operador para controlar que dos objeto de tipo Entidad son igual
        /// </summary>
        /// <param name="left">Objeto izquierdo a comparar</param>
        /// <param name="right">Objeto derecho a comparar</param>
        /// <returns>Verdadero si son iguales</returns>
        public static bool operator ==(Entity left, Entity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null)) ? true : false;
            else
                return left.Equals(right);
        }
        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Operador para controlar que dos objeto de tipo Entidad son distintos
        /// </summary>
        /// <param name="left">Objeto izquierdo a comparar</param>
        /// <param name="right">Objeto derecho a comparar</param>
        /// <returns>Verdadero si son distintos</returns>
        public static bool operator !=(Entity left, Entity right)
        {
            return !(left == right);
        }
    }
}
