﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Commons;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class PageAgreggateTests
    {
        private string fakeCreatedBy = "usuarioAdministrador",
            fakeSolicitadoPor = "usuarioAdministrador";
        private string fakeTitle = "Title de la página";
        private string fakeSummary = "Summary de la página";
        private string fakeBody = "Body de la página";
        private string fakeKeyword = "Keywords de la página";
        private int fakeOrder = 1;
        private string fakeImageUri = "/uploads/imagenes/fakeimage.jpg";
       
        private Page fakePage()
        {
            return new Page(createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  body: fakeBody,
                  keywords: fakeKeyword);
        }
        [Fact]
        public void T001_Crear_Correctamente_Una_Pagina()
        {
            var page = new Page(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  body: fakeBody,
                  keywords: fakeKeyword);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(page);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, page.Title);
            Assert.Equal(fakeSummary, page.Summary);
            Assert.Equal(fakeBody, page.Body);
            Assert.Empty(page.PageMultimedia);
            // Pruebo el estado inicial del Page
            Assert.Equal(DateTime.Now.Date, 
                page.CreationDate.Date);
            Assert.False(page.Enabled);
        }
        [Fact]
        public void T002_Habilitar_Desahabilitar_Correctamente_Una_Pagina()
        {
            var page = fakePage();
            // Habilito el page
            page.ChangeEnabled(
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(page.Enabled);
            // Deshabilito el page
            page.ChangeEnabled(
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(page.Enabled);
        }
        [Fact]
        public void T003_Cambiar_Order_De_Portada_Correctamente_En_Una_Pagina()
        {
            var page = fakePage();
            // cambio el page
            page.ChangeOrder(
                orderPlace: OrderPlace.AtHome,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, page.Order);
            // pruebo que tenga eventos de dominio           
            Assert.Single(page.AfterCommitDomainEvents);
            // cambio de nuevo el orden de la página
            page.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrder + 1);
            // Pruebo que no cambia el orden en otras ubicaciones
            Assert.Equal(fakeOrder, page.Order);
            // pruebo que tenga eventos de dominio           
            Assert.Single(page.AfterCommitDomainEvents);
        }

        [Fact]
        public void T004_Cambiar_Imagen_De_Portada_Correctamente_En_Una_Pagina()
        {
            var page = fakePage();
            // cambio la imagen de la página
            page.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, page.HomeImage);
        }        
        [Fact]
        public void T005_Cambiar_Estado_En_Portada_Correctamente_Una_Pagina()
        {
            var page = fakePage();
            // Habilito el page
            page.ChangeAtHome(
                atHome: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(page.AtHome);
            // Deshabilito el page
            page.ChangeAtHome(
                atHome: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(page.AtHome);
        }
        [Fact]
        public void T006_Crear_Correctamente_Una_PaginaMultimedia()
        {
            var multimedio = new PageMultimedia(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  path1: fakeImageUri,
                  path2: fakeImageUri,
                  iDMultimediaType: MultimediaType.Imagen.Id);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(multimedio);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, multimedio.Title);
            Assert.Equal(fakeSummary, multimedio.Summary);
            Assert.Equal(fakeImageUri, multimedio.Path1);
            Assert.Equal(fakeImageUri, multimedio.Path2);
            // Pruebo el estado inicial del Page
            Assert.Equal(DateTime.Now.Date, multimedio.CreationDate.Date);
            Assert.True(multimedio.Enabled);
            Assert.False(multimedio.AtHome);
            Assert.Equal(0, multimedio.Order);
        }
        [Fact]
        public void T007_Agregar_Correctamente_Un_Multimedio_A_Una_Pagina()
        {
            var page = fakePage();
            page.AddMultimedia(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  path1: fakeImageUri,
                  path2: fakeImageUri,
                  multimediaType: MultimediaType.Imagen
                  );
            Assert.Single(page.PageMultimedia);
        }
        [Fact]
        public void T008_Cambiar_Estado_En_Menu_Correctamente_Una_Pagina()
        {
            var page = fakePage();
            // Habilito el page
            page.ChangeOnMenu(
                onMenu: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(page.OnMenu);
            // Deshabilito el page
            page.ChangeOnMenu(
                onMenu: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(page.OnMenu);
        }
        [Fact]
        public void T009_Cambiar_Order_Del_Menu_Correctamente_En_Una_Pagina()
        {
            var page = fakePage();
            // cambio el page
            page.ChangeOrder(
                orderPlace: OrderPlace.AtMenu,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, page.OrderAtMenu);
            // pruebo que tenga eventos de dominio           
            Assert.Single(page.AfterCommitDomainEvents);
            // cambio de nuevo el orden de la página
            page.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrder + 1);
            // Pruebo que no cambia el orden en otras ubicaciones
            Assert.Equal(fakeOrder, page.OrderAtMenu);
            // pruebo que tenga eventos de dominio           
            Assert.Single(page.AfterCommitDomainEvents);
        }

    }
}
