﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Configurations;
using Pixelario.CMS.Domain.Commons;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class ConfigurationAgreggateTests
    {
        private string fakeCreatedBy = "test",
            fakeSolicitedBy = "test";
        private string fakeTitle = "Fake title";
        private string fakeKey = "Fake-Key";
        private string fakeNewTitle = "Fake title";
        private string fakeNewKey = "Fake-Key";
        private string fakeState = "fake state";
        private string fakeRoles = "fakeRole1; fake Role 2";
        private Configuration fakeConfiguration()
        {
            return new Configuration(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText);
        }
        [Fact]
        public void T001_Crear_Correctamente_Una_Configuracion()
        {
            var configuration = new Configuration(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  key: fakeKey,
                  configurationType: ConfigurationType.LineText);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(configuration);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, configuration.Title);
            Assert.Equal(fakeKey, configuration.Key);
            // Pruebo el estado inicial del Configuration
            Assert.Equal(DateTime.Now.Date,
                configuration.CreationDate.Date);
        }

        [Fact]
        public void T002_Editar_Correctamente_Una_Configuration()
        {
            var configuration = new Configuration(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText);
            // Edito la configuración
            configuration.Update(
                solicitedBy: fakeSolicitedBy,
                title: fakeNewTitle,
                key: fakeNewKey,
                configurationType: ConfigurationType.AreaText);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeNewTitle, configuration.Title);
            Assert.Equal(fakeNewKey, configuration.Key);
            Assert.Equal(ConfigurationType.AreaText, configuration.ConfigurationType);
        }

        [Fact]
        public void T003_Establecer_Correctamente_El_Estado_De_Una_Configuration()
        {
            var configuration = new Configuration(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText);
            // Edito la configuración
            configuration.SetState(
                state: fakeState);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeState, configuration.State);
        }

        [Fact]
        public void T004_Bind_And_Unbind_Roles_To_Configuration()
        {
            var configuration = new Configuration(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                key: fakeKey,
                configurationType: ConfigurationType.LineText);
            // Bind role to Configuration
            foreach (var fakeRole in fakeRoles.Split(';'))
            {
                configuration.BindToRole(fakeRoles);
            }
            foreach (var fakeRole in fakeRoles.Split(';'))
            {
                Assert.Contains(fakeRole, configuration.GetBindedRoles());
            }
            configuration.UnbindToRole(fakeRoles.Split(';')[0]);
            Assert.DoesNotContain(fakeRoles.Split(';')[0], configuration.GetBindedRoles());
        }
    }
}