﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Exceptions;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class AdvertismentAgreggateTests
    {
        private string fakeCreatedBy = "usuarioAdministrador",
            fakeSolicitedBy = "usuarioAdministrador";
        private string fakeTitle = "Title del articulo";
        private string fakeCode = "Code del articulo";
        private string fakeUrl = "Url del articulo";
        private string fakeKeyword = "Keywords del articulo";
        private int fakeOrder = 1;
        private string fakeImageUri = "/uploads/imagenes/fakeimage.jpg";
        private DateTime? fakePublicationDate = DateTime.Now.AddDays(1);
        private string fakeSource = "Fake advertisment source";
        private Advertisment fakeAdvertisment()
        {
            var advertisment = new Advertisment(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                code: fakeCode,
                url: fakeUrl);
            return advertisment;
        }
        [Fact]
        public void T001_Add_Advertisment_Correctly()
        {
            var advertisment = fakeAdvertisment();
            // Pruebo que no sea null la cuenta
            Assert.NotNull(advertisment);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Null(advertisment.PublicationStartDate);
            Assert.Null(advertisment.PublicationEndDate);
            Assert.Equal(fakeTitle, advertisment.Title);
            Assert.Equal(fakeCode, advertisment.Code);
            Assert.Equal(fakeUrl, advertisment.URL);
            Assert.Equal(AdvertismentPlaceAtWeb.None.Id, advertisment.IDPlaceAtWeb);
            // Pruebo el estado inicial del Advertisment
            Assert.Equal(DateTime.Now.Date, advertisment.CreationDate.Date);
            Assert.False(advertisment.Enabled);
        }
        [Fact]
        public void T002_Enabling_Advertisment()
        {
            var advertisment = fakeAdvertisment();
            // Enabling advertisment
            advertisment.ChangeEnabled(
                enabled: true);
            // Testing
            Assert.True(advertisment.Enabled);
            // Desabling  advertisment
            advertisment.ChangeEnabled(
                enabled: false);
            // Testing
            Assert.False(advertisment.Enabled);

        }
        [Fact]
        public void T003_Changing_Home_Image_On_Adertisment()
        {
            var advertisment = fakeAdvertisment();
            // cambio la imagen del articulo
            advertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, advertisment.HomeImage);
        }
        [Fact]
        public void T004_Changing_Vertical_Image_On_Adertisment()
        {
            var advertisment = fakeAdvertisment();
            // cambio la imagen del articulo
            advertisment.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, advertisment.VerticalImage);
        }
        [Fact]
        public void T005_Changing_Horizontal_Image_On_Adertisment()
        {
            var advertisment = fakeAdvertisment();
            // cambio la imagen del articulo
            advertisment.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, advertisment.HorizontalImage);
        }
        [Fact]
        public void T006_Change_Advertisment_Published_Value()
        {
            var advertisment = fakeAdvertisment();
            advertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            // Habilito el articulo
            advertisment.ChangeEnabled(
                enabled: true);
            advertisment.ChangePublish(
                publish: true);
            // Testing published
            Assert.True(advertisment.Published);
            // Unpublishing
            advertisment.ChangePublish(
                publish: false);
            // Testing unpublished
            Assert.False(advertisment.Published);
        }
        [Fact]
        public void T007_Change_Published_On_Advertisment_Without_Image_Throw_Exception()
        {
            var advertisment = fakeAdvertisment();
            Action action = () =>
            {
                // cambio el articulo
                advertisment.ChangeEnabled(enabled: true);
                advertisment.ChangePublish(publish: true);
            };
            Assert.Throws<AdvertismentException>(action);
        }
        [Fact]
        public void T008_Change_Advertisment_Place_At_Web()
        {
            var advertisment = fakeAdvertisment();
            advertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            advertisment.ChangeEnabled(
                enabled: true);
            advertisment.ChangePublish(
                publish: true);
            // Change Advertisment Place
            advertisment.ChangePlaceAtWeb(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(AdvertismentPlaceAtWeb.Aside.Id,
                advertisment.IDPlaceAtWeb);
        }
        [Fact]
        public void T009_Change_Advertisment_Order_Correctly()
        {
            var advertisment = fakeAdvertisment();
            advertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            advertisment.ChangeEnabled(
                enabled: true);
            advertisment.ChangePublish(
                publish: true);
            advertisment.ChangePlaceAtWeb(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            // Change Order
            advertisment.ChangeOrder(                
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, advertisment.Order);
        }
        [Fact]
        public void T010_Remove_Image_On_Published_Advertisment_Throw_Exception()
        {
            var advertisment = fakeAdvertisment();
            advertisment.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            advertisment.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            advertisment.ChangeEnabled(
                enabled: true);
            advertisment.ChangePublish(
                publish: true);
            // Change Advertisment Place
            advertisment.ChangePlaceAtWeb(
                placeAtWeb: AdvertismentPlaceAtWeb.Aside);
            Action action = () =>
            {
                advertisment.ChangeImage(
                    imageType: ImageType.Home,
                    uri: string.Empty);
            };
            Assert.Throws<AdvertismentException>(action);
        }
    }
}
