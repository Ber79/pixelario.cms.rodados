﻿using System;
using System.Linq;
using Moq;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Exceptions;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class ArticleAgreggateTests
    {
        private string fakeCreatedBy = "usuarioAdministrador",
            fakeSolicitedBy = "usuarioAdministrador";
        private string fakeTitle = "Title del articulo";
        private string fakeSummary = "Summary del articulo";
        private string fakeBody = "Body del articulo";
        private string fakeKeyword = "Keywords del articulo";
        private int fakeOrder = 1;
        private string fakeImageUri = "/uploads/imagenes/fakeimage.jpg";
        private DateTime? fakePublicationDate = DateTime.Now.AddDays(1);
        private string fakeSource = "Fake article source";
        private string fakeCode = "<iframe href=\"http://www.google.com\"></iframe>";
        private Edition fakeEdition(EditionType editionType)
        {
            var edition = new Edition(createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            edition.ChangeEditionType(editionType);
            return edition;
        }
        private Edition fakeNewEdition(EditionType editionType)
        {
            var editionMoq = new Mock<Edition>(fakeCreatedBy, fakeTitle,
                fakeSummary, fakeKeyword);
            editionMoq.SetupGet(p => p.ID).Returns(2);
            var edition = editionMoq.Object;
            edition.ChangeEditionType(editionType);
            return edition;
        }

        private Article fakeArticle(Edition edition)
        {
            var article = new Article(
                createdBy: fakeCreatedBy,
                publicationDate: fakePublicationDate,
                title: fakeTitle,
                summary: fakeSummary,
                body: fakeBody,
                source: fakeSource,
                iDEdition: edition.ID,
                keywords: fakeKeyword);
            article.Edition = edition;
            return article;
        }
        private Topic fakeTopic()
        {
            return new Topic(
                createdBy: fakeCreatedBy,
                title: fakeTitle,
                summary: fakeSummary,
                keywords: fakeKeyword);
        }
        private SubTopic fakeSubTopic()
        {
            return new SubTopic(
                   createdBy: fakeCreatedBy,
                   title: fakeTitle,
                   summary: fakeSummary,
                   keywords: fakeKeyword);
        }
        private ArticleScript fakeArticleScript()
        {
            return new ArticleScript(
                   createdBy: fakeCreatedBy,
                   title: fakeTitle,
                   summary: fakeSummary);
        }
       

        [Fact]
        public void T001_Crear_Correctamente_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // Pruebo que no sea null la cuenta
            Assert.NotNull(article);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakePublicationDate, article.PublicationDate);
            Assert.Equal(fakeTitle, article.Title);
            Assert.Equal(fakeSummary, article.Summary);
            Assert.Equal(fakeBody, article.Body);
            Assert.Equal(fakeSource, article.Source);
            Assert.Equal((int)PlaceAtHome.None, article.IDPlaceAtHome);
            Assert.Empty(article.ArticleMultimedia);
            Assert.Empty(article.Topics);
            // Pruebo el estado inicial del Article
            Assert.Equal(DateTime.Now.Date, article.CreationDate.Date);
            Assert.False(article.Enabled);
        }
        [Fact]
        public void T002_Habilitar_Desahabilitar_Correctamente_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // Habilito el articulo
            article.ChangeEnabled(
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(article.Enabled);
            //Assert.NotEmpty(article.AfterCommitDomainEvents);
            // Deshabilito el articulo
            article.ChangeEnabled(
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(article.Enabled);
            //Assert.Equal(2, article.AfterCommitDomainEvents.Count);

        }
        [Fact]
        public void T003_Cambiar_Orden_De_Portada_Correctamente_En_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // cambio el articulo
            article.ChangeOrder(
                orderPlace: OrderPlace.AtHome,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, article.Order);
            // pruebo que tenga eventos de dominio           
            //Assert.Single(article.AfterCommitDomainEvents);
            // cambio de nuevo el orden del articulo
            article.ChangeOrder(
                orderPlace: OrderPlace.AtMenu,
                order: fakeOrder + 1);
            // Pruebo que no cambia el orden en otras ubicaciones
            Assert.Equal(fakeOrder, article.Order);
            // pruebo que tenga eventos de dominio           
            //Assert.Single(article.AfterCommitDomainEvents);
        }

        [Fact]
        public void T004_Cambiar_Imagen_De_Portada_Correctamente_En_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // cambio la imagen del articulo
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, article.HomeImage);
        }
        [Fact]
        public void T005_Cambiar_Imagen_Vertical_Correctamente_En_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // cambio la imagen del articulo
            article.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, article.VerticalImage);
        }
        [Fact]
        public void T006_Cambiar_Imagen_Horizontal_Correctamente_En_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // cambio la imagen del articulo
            article.ChangeImage(
                imageType: ImageType.Horizontal,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, article.HorizontalImage);
        }
        [Fact]
        public void T007_Cambiar_Estado_En_Portada_Correctamente_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // Habilito el articulo
            article.ChangeAtHome(
                atHome: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(article.AtHome);
            //Assert.Single(article.AfterCommitDomainEvents);
            // Deshabilito el articulo
            article.ChangeAtHome(
                atHome: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(article.AtHome);
            //Assert.Equal(2, article.AfterCommitDomainEvents.Count);

        }
        [Fact]
        public void T008_Cambiar_Orden_De_Edition_Correctamente_En_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            // cambio el articulo
            article.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, article.OrderAtEdition);
            // pruebo que tenga eventos de dominio           
            //Assert.Single(article.AfterCommitDomainEvents);
            // cambio de nuevo el orden del articulo
            article.ChangeOrder(
                orderPlace: OrderPlace.AtMenu,
                order: fakeOrder + 1);
            // Pruebo que no cambia el orden en otras ubicaciones
            Assert.Equal(fakeOrder, article.OrderAtEdition);
            // pruebo que tenga eventos de dominio           
            //Assert.Single(article.AfterCommitDomainEvents);
        }
        [Fact]
        public void T009_Cambiar_Portada_En_Un_Articulo_De_Edicion_Especial_Sin_Imagenes_De_Portada_Da_Error()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.Especial_Content));
            Action action = () =>
            {
                // cambio el articulo
                article.ChangeAtHome(atHome: true);
            };
            Assert.Throws<ArticleException>(action);
        }
        [Fact]
        public void T010_Cambiar_Portada_En_Un_Articulo_De_Edicion_Especial_Correctamente()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.Especial_Content));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            article.ChangeAtHome(atHome: true);
            Assert.Equal(ArticlePlaceAtHome.Boxes.Id, 
                article.IDPlaceAtHome);
        }
        [Fact]
        public void T011_Cambiar_Lugar_En_Portada_A_Columns_En_Un_Articulo_Con_Imagene_De_Portda_Da_OK()
        {
            // creo el articulo y le doy una imagen de portada
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // cambio el articulo
            article.ChangePlaceAtHome(
                placeAtHome: ArticlePlaceAtHome.Columns);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(ArticlePlaceAtHome.Columns.Id, 
                article.IDPlaceAtHome);
        }
        [Fact]
        public void T012_Cambiar_Lugar_En_Portada_A_Slider_En_Un_Articulo_Sin_Imagen_Vertical_Da_Error()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            Action action = () =>
            {
                // cambio el articulo
                article.ChangePlaceAtHome(
                    placeAtHome: ArticlePlaceAtHome.Slider);
            };
            Assert.Throws<ArticleException>(action);
        }
        [Fact]
        public void T013_Cambiar_Lugar_En_Portada_A_Boxes_En_Un_Articulo_Con_Imagenes_De_Portda_Y_Vertical_Da_OK()
        {
            // creo el articulo y le doy una imagen de portada
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            article.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            // cambio el articulo
            article.ChangePlaceAtHome(
                placeAtHome: ArticlePlaceAtHome.Slider);
            // Pruebo que tenga el nuevo lugar
            Assert.Equal((int)PlaceAtHome.Slider, article.IDPlaceAtHome);
        }
        [Fact]
        public void T014_Quitar_Imagen_De_Portada_En_Un_Articulo_En_Portada_Da_Error()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            article.ChangeAtHome(
               atHome: true);
            // cambio el articulo
            article.ChangePlaceAtHome(
                placeAtHome: ArticlePlaceAtHome.Boxes);
            Action action = () =>
            {
                article.ChangeImage(
                    imageType: ImageType.Home,
                    uri: string.Empty);
            };
            Assert.Throws<ArticleException>(action);
        }
        [Fact]
        public void T015_Quitar_Imagen_Vertical_En_Un_Articulo_En_Slider_Da_Error()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            article.ChangeImage(
                imageType: ImageType.Vertical,
                uri: fakeImageUri);
            article.ChangeAtHome(
               atHome: true);
            // cambio el articulo
            article.ChangePlaceAtHome(
                placeAtHome: ArticlePlaceAtHome.Slider);
            Action action = () =>
            {
                article.ChangeImage(
                    imageType: ImageType.Vertical,
                    uri: string.Empty);
            };
            Assert.Throws<ArticleException>(action);
        }
        [Fact]
        public void T016_Crear_Correctamente_Un_Multimedio()
        {
            var multimedia = new ArticleMultimedia(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  path1: fakeImageUri,
                  path2: fakeImageUri,
                  iDMultimediaType: MultimediaType.Imagen.Id);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(multimedia);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, multimedia.Title);
            Assert.Equal(fakeSummary, multimedia.Summary);
            Assert.Equal(fakeImageUri, multimedia.Path1);
            Assert.Equal(fakeImageUri, multimedia.Path2);
            // Pruebo el estado inicial del Article
            Assert.Equal(DateTime.Now.Date,
                multimedia.CreationDate.Date);
            Assert.True(multimedia.Enabled);
            Assert.False(multimedia.AtHome);
            Assert.Equal(0, multimedia.Order);
        }
        [Fact]
        public void T017_Agregar_Correctamente_Un_Multimedio_A_Un_Articulo()
        {
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.AddMultimedia(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  path1: fakeImageUri,
                  path2: fakeImageUri,
                  multimediaType: MultimediaType.FromName("Imagen")
                  );
            Assert.Single(article.ArticleMultimedia);
        }

        [Fact]
        public void T018_Vincular_Y_Desvincular_Correctamente_Un_Tema_A_Un_Articulo()
        {
            var topic = fakeTopic();
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.BindTopic(
                  topic: topic);
            Assert.Single(article.Topics);
            article.UnbindTopic(
                topic: topic);
            Assert.Empty(article.Topics);
        }
        [Fact]
        public void T019_Vincular_Y_Desvincular_Correctamente_Un_SubTopic_A_Un_Articulo()
        {
            var subTopic = fakeSubTopic();
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.BindSubTopic(
                  subTopic: subTopic);
            Assert.Single(article.SubTopics);
            article.UnbindSubTopic(
                subTopic: subTopic);
            Assert.Empty(article.SubTopics);

        }
        [Fact]
        public void T020_Tranfer_Article_Properly()
        {
            var edition = fakeEdition(
                editionType: EditionType.News);
            var iDNewEdition = EditionType.Especial_Content.Id;
            var article = fakeArticle(
                edition: edition);
            Assert.Equal(default(int), article.IDEdition);
            article.TransferToEdition(
                  iDEdition: iDNewEdition);
            Assert.Equal(EditionType.Especial_Content.Id, article.IDEdition);            
        }
        [Fact]
        public void T021_Add_Article_Script()
        {
            var articleScript = fakeArticleScript();
            // Pruebo que no sea null la cuenta
            Assert.NotNull(articleScript);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, articleScript.Title);
            Assert.Equal(fakeSummary, articleScript.Summary);
            Assert.Empty(articleScript.Items);
            // Pruebo el estado inicial del Article
            Assert.Equal(DateTime.Now.Date, articleScript.CreationDate.Date);
            Assert.False(articleScript.Enabled);
        }
        [Fact]
        public void T022_Add_Article_Script_Item()
        {
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  code: fakeCode,
                  multimediaType: MultimediaType.ScriptOnHeader
                  );
            Assert.Single(articleScript.Items);
            Assert.Equal(fakeCreatedBy, articleScript.Items.First().CreatedBy);
            Assert.Equal(fakeTitle, articleScript.Items.First().Title);
            Assert.Equal(fakeCode, articleScript.Items.First().Code);
            Assert.Equal(MultimediaType.ScriptOnHeader.Id, articleScript.Items.First().IDMultimediaType);

        }
        [Fact]
        public void T023_Enable_Article_Script()
        {
            var articleScript = fakeArticleScript();
            // Habilito el articulo
            articleScript.ChangeEnabled(
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(articleScript.Enabled);
            // Deshabilito el articulo
            articleScript.ChangeEnabled(
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(articleScript.Enabled);

        }
        [Fact]
        public void T023_Enable_Article_Script_Item()
        {
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  code: fakeCode,
                  multimediaType: MultimediaType.ScriptOnHeader
                  );
            // Habilito el articulo
            articleScript.ChangeItemEnabled(
                iDArticleScriptItem: articleScript.Items.First().ID,
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(articleScript.Items.First().Enabled);
            // Deshabilito el articulo
            articleScript.ChangeItemEnabled(
                iDArticleScriptItem: articleScript.Items.First().ID,
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(articleScript.Items.First().Enabled);

        }
        [Fact]
        public void T024_Update_Article_Script()
        {
            var articleScript = fakeArticleScript();
            // Habilito el articulo
            articleScript.Update(
                title: fakeTitle + " editado",
                summary: fakeSummary + " editado");
            // Test update
            Assert.Equal(fakeTitle + " editado", articleScript.Title);
            Assert.Equal(fakeSummary + " editado", articleScript.Summary);
        }
        [Fact]
        public void T025_Update_Article_Script_Item()
        {
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  code: fakeCode,
                  multimediaType: MultimediaType.ScriptOnHeader
                  );
            // Habilito el articulo
            articleScript.UpdateItem(
                iDArticleScriptItem: articleScript.Items.First().ID,
                title: fakeTitle + " editado",
                code: fakeCode + " editado",
                multimediaType: MultimediaType.ScriptOnBottom);
            // Test update
            Assert.Equal(fakeTitle + " editado", articleScript.Items.First().Title);            
            Assert.Equal(fakeCode + " editado", articleScript.Items.First().Code);
            Assert.Equal(MultimediaType.ScriptOnBottom.Id, articleScript.Items.First().IDMultimediaType);

        }
        [Fact]
        public void T026_Delete_Article_Script_Item()
        {
            var articleScript = fakeArticleScript();
            articleScript.AddItem(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  code: fakeCode,
                  multimediaType: MultimediaType.ScriptOnHeader
                  );
            // Habilito el articulo
            articleScript.DeleteItem(
                item: articleScript.Items.First()
                );
            // Test update
            Assert.Empty(articleScript.Items);
        }
        [Fact]
        public void T027_Bind_And_Unbind_Script_To_Article()
        {
            var script = fakeArticleScript();
            var article = fakeArticle(
                edition: fakeEdition(EditionType.News));
            article.BindScript(
                  script: script);
            Assert.Single(article.Scripts);
            article.UnbindScript(
                script: script);
            Assert.Empty(article.Scripts);
        }

    }
}
