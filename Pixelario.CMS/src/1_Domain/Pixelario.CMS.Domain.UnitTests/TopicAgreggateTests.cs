﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class TopicAgreggateTests
    {
        private string fakeCreatedBy = "usuarioAdministrador",
            fakeSolicitedBy = "usuarioAdministrador";
        private string fakeTitle = "Title del tema";
        private string fakeSummary = "Summary del tema";
        private string fakeKeyword = "Keywords del tema";
        private int fakeOrder = 1;
        private string fakeImageUri = "/uploads/imagenes/fakeimage.jpg";
        private Topic fakeTopic()
        {
            return new Topic(createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
        }
        private SubTopic fakeSubTopic()
        {
            return new SubTopic(
                   createdBy: fakeCreatedBy,
                   title: fakeTitle,
                   summary: fakeSummary,
                   keywords: fakeKeyword);
        }
        private Edition fakeEdition()
        {
            return new Edition(createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
        }
        [Fact]
        public void T001_Crear_Correctamente_Un_Topic()
        {
            var topic = new Topic(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(topic);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, topic.Title);
            Assert.Equal(fakeSummary, topic.Summary);
            Assert.Equal(fakeKeyword, topic.Keywords);
            // Pruebo el estado inicial del Topic
            Assert.Equal(DateTime.Now.Date, topic.CreationDate.Date);
            Assert.False(topic.Enabled);
            Assert.Empty(topic.SubTopics);
            Assert.Empty(topic.Editions);
        }
        [Fact]
        public void T002_Habilitar_Desahabilitar_Correctamente_Un_Topic()
        {
            var topic = fakeTopic();
            // Habilito el tema
            topic.ChangeEnabled(
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(topic.Enabled);
            // Deshabilito el tema
            topic.ChangeEnabled(
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(topic.Enabled);
        }
        [Fact]
        public void T003_Cambiar_Order_De_AtHome_Correctamente_En_Un_Topic()
        {
            var topic = fakeTopic();
            // cambio el orden del tema en el menu
            topic.ChangeOrder(
                orderPlace: OrderPlace.AtHome,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, topic.Order);
            // pruebo que tenga eventos de dominio           
            Assert.Single(topic.AfterCommitDomainEvents);
            // cambio de nuevo el orden del tema en otro lugar
            topic.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrder + 1);
            Assert.Equal(fakeOrder, topic.Order);
            // pruebo que no tenga nuevos eventos de dominio           
            Assert.Single(topic.AfterCommitDomainEvents);
        }

        [Fact]
        public void T004_Cambiar_Imagen_De_AtHome_Correctamente_En_Un_Topic()
        {
            var topic = fakeTopic();
            // cambio la imagen del tema
            topic.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, topic.HomeImage);
        }
        [Fact]
        public void T005_Cambiar_Esto_En_AtHome_Correctamente_Un_Topic()
        {
            var topic = fakeTopic();
            // Habilito el tema
            topic.ChangeAtHome(
                atHome: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(topic.AtHome);
            // Deshabilito el tema
            topic.ChangeAtHome(
                atHome: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(topic.AtHome);
        }
        [Fact]
        public void T006_Crear_Correctamente_Un_SubTopic()
        {
            var subtopic = new SubTopic(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            // Pruebo que no sea null el sub tema
            Assert.NotNull(subtopic);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, subtopic.Title);
            Assert.Equal(fakeSummary, subtopic.Summary);
            // Pruebo el estado inicial del subtopic
            Assert.Equal(DateTime.Now.Date, subtopic.CreationDate.Date);
            Assert.False(subtopic.Enabled);
            Assert.False(subtopic.AtHome);
            Assert.Equal(0, subtopic.Order);
            //Assert.Empty(subtopic.SubTopics);
        }
        [Fact]
        public void T007_Agregar_Y_Borrar_Correctamente_Un_Subtopic_A_Un_Topic()
        {
            var topic = fakeTopic();
            topic.AddSubTopic(
                  solicitedBy: fakeSolicitedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            Assert.Single(topic.SubTopics);
            int index = 0;
            SubTopic subTopicToDelete = null;
            foreach (var subTopic in topic.SubTopics)
            {
                if (index == 0)
                {
                    subTopicToDelete = subTopic;
                }
                index++;
            }
            topic.DeleteSubTopic(
                subTopic: subTopicToDelete);
            Assert.Empty(topic.SubTopics);
        }
        //[Fact]
        //public void T008_Agregar_Correctamente_Un_Subtopic_A_Un_SubTopic()
        //{
        //    var subTopic = fakeSubTopic();
        //    subTopic.AgregarSubTopic(
        //          createdBy: fakeCreatedBy,
        //          title: fakeTitle,
        //          summary: fakeSummary,
        //          keywords: fakeKeyword);
        //    Assert.Single(subTopic.SubTopics);
        //}
        [Fact]
        public void T009_Cambiar_Imagen_De_AtHome_Correctamente_En_Un_SubTopic()
        {
            var subTopic = fakeSubTopic();
            // cambio la imagen del tema
            subTopic.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, subTopic.HomeImage);
        }
        [Fact]
        public void T010_Agregar_Y_Borrar_Correctamente_Una_Edition_A_Un_Topic()
        {
            var topic = fakeTopic();
            var edition = fakeEdition();
            topic.BindEdition(
                edition: edition);
            Assert.Single(topic.Editions);
            topic.UnbindEdition(
                edition: edition);
            Assert.Empty(topic.Editions);
        }
        [Fact]
        public void T011_Cambiar_Estado_En_Menu_Correctamente_Un_Tema()
        {
            var topic = fakeTopic();
            // Habilito el tema
            topic.ChangeOnMenu(
                onMenu: true);
            // Pruebo habilitado en true
            Assert.True(topic.OnMenu);
            // Deshabilito el tema
            topic.ChangeOnMenu(
                onMenu: false);
            // Pruebo no sea habilitado en true
            Assert.False(topic.OnMenu);
        }
        [Fact]
        public void T012_Cambiar_Order_Del_Menu_Correctamente_En_Un_Tema()
        {
            var topic = fakeTopic();
            // cambio el tema
            topic.ChangeOrder(
                orderPlace: OrderPlace.AtMenu,
                order: fakeOrder);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrder, topic.OrderAtMenu);
            // pruebo que tenga eventos de dominio           
            Assert.Single(topic.AfterCommitDomainEvents);
            // cambio de nuevo el orden de la página
            topic.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrder + 1);
            // Pruebo que no cambia el orden en otras ubicaciones
            Assert.Equal(fakeOrder, topic.OrderAtMenu);
            // pruebo que tenga eventos de dominio           
            Assert.Single(topic.AfterCommitDomainEvents);
        }

    }
}
