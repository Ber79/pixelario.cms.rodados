﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Xunit;

namespace Pixelario.CMS.Domain.UnitTest
{
    public class EditionAgreggateTests
    {
        
        private string fakeCreatedBy = "usuarioAdministrador",
            fakeSolicitadoPor = "usuarioAdministrador";
        private string fakeTitle = "Title de la edición";
        private string fakeSummary = "Summary de la edición";
        private string fakeKeyword = "Keywords de la edición";
        private int fakeOrden = 1;
        private string fakeImageUri = "/uploads/imagenes/fakeimage.jpg";
        private Edition fakeEdition()
        {
            return new Edition(createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
        }
        [Fact]
        public void T001_Crear_Correctamente_Un_Edition()
        {
            var edition = new Edition(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            // Pruebo que no sea null la cuenta
            Assert.NotNull(edition);
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(fakeTitle, edition.Title);
            Assert.Equal(fakeSummary, edition.Summary);
            // Pruebo el estado inicial de la edición
            Assert.Equal(DateTime.Now.Date, edition.CreationDate.Date);
            Assert.Equal(EditionType.None.Id, edition.EditionType);

            Assert.False(edition.Enabled);
            Assert.False(edition.OnMenu);
            Assert.False(edition.AtHome);

            Assert.Equal(0, edition.Order);
            Assert.Equal(0, edition.OrderAtMenu);
        }
        [Fact]
        public void T002_Habilitar_Desahabilitar_Correctamente_Un_Edition()
        {
            var edition = fakeEdition();
            // Habilito la edición
            edition.ChangeEnabled(
                enabled: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(edition.Enabled);
            // Habilitar no crea eventos de dominio           
            Assert.Null(edition.AfterCommitDomainEvents);
            // Deshabilito la edición
            edition.ChangeEnabled(
                enabled: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(edition.Enabled);
            // pruebo que tiene 1 evento de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
        }
        [Fact]
        public void T003_Cambiar_Orden_De_Portada_Correctamente_En_Un_Edition()
        {
            var edition = fakeEdition();
            // cambio el orden de la edición en el menu
            edition.ChangeOrder(
                orderPlace: OrderPlace.AtHome,
                order: fakeOrden);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeOrden, edition.Order);
            // pruebo que tenga eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
            // cambio de nuevo el orden de la edición en otro lugar
            edition.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrden + 1);
            Assert.Equal(fakeOrden, edition.Order);
            // pruebo que no tenga nuevos eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
        }
        [Fact]
        public void T004_Cambiar_Imagen_De_Portada_Correctamente_En_Un_Edition()
        {
            var edition = fakeEdition();
            // cambio la imagen de la edición
            edition.ChangeImage(
                imageType: ImageType.Home,
                uri: fakeImageUri);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(fakeImageUri, edition.HomeImage);
        }
        [Fact]
        public void T005_Cambiar_Estado_En_Portada_Correctamente_Un_Edition()
        {
            var edition = fakeEdition();
            // Habilito la edición
            edition.ChangeAtHome(
                atHome: true);
            // Pruebo que no sea Enabled sea true
            Assert.True(edition.AtHome);
            // pruebo que no tenga nuevos eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);

            // Deshabilito la edición
            edition.ChangeAtHome(
                atHome: false);
            // Pruebo que no sea Enabled sea true
            Assert.False(edition.AtHome);
            // pruebo que no tenga dos eventos de dominio           
            Assert.Equal(2, edition.AfterCommitDomainEvents.Count);

        }
        [Fact]
        public void T006_Editar_Correctamente_Un_Edition()
        {
            var edition = new Edition(
                  createdBy: fakeCreatedBy,
                  title: fakeTitle,
                  summary: fakeSummary,
                  keywords: fakeKeyword);
            // Edito la edicion
            edition.Update(
                title: string.Format("{0} - editado",
                    fakeTitle),
                summary: string.Format("{0} - editado", 
                    fakeSummary),
                keywords: string.Format("{0} - editado", 
                    fakeKeyword));
            // Pruebo que la información esta en las propiedad correctamente
            Assert.Equal(string.Format("{0} - editado",
                    fakeTitle), edition.Title);
            Assert.Equal(string.Format("{0} - editado",
                    fakeSummary), edition.Summary);
            Assert.Equal(string.Format("{0} - editado",
                    fakeKeyword), edition.Keywords);
        }
        [Fact]
        public void T007_Cambiar_Tipo_De_Edicion_Correctamente_En_Un_Edition()
        {
            var edition = fakeEdition();
            // cambio el orden de la edición en el menu
            edition.ChangeEditionType(
                editionType: EditionType.News);
            // Pruebo que tenga el nuevo orden
            Assert.Equal(EditionType.News.Id, edition.EditionType);           
            // pruebo que tenga nuevos eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
        }
        [Fact]
        public void T008_Change_Edition_OnMenu_Property()
        {
            var edition = fakeEdition();
            // Change OnMenu to true
            edition.ChangeOnMenu(
                onMenu: true);
            // Test OnMenu is true
            Assert.True(edition.OnMenu);
            // pruebo que no tenga nuevos eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);

            // Change OnMenu to false
            edition.ChangeOnMenu(
                onMenu: false);
            // Test OnMenu is false
            Assert.False(edition.OnMenu);
            // pruebo que no tenga dos eventos de dominio           
            Assert.Equal(2, edition.AfterCommitDomainEvents.Count);

        }
        [Fact]
        public void T009_Change_Edition_OnMenu_Order_Property()
        {
            var edition = fakeEdition();
            // Change edition menu order
            edition.ChangeOrder(
                orderPlace: OrderPlace.AtMenu,
                order: fakeOrden);
            // Test edition menu order
            Assert.Equal(fakeOrden, edition.OrderAtMenu);
            // pruebo que tenga eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
            // Change edition menu order again
            edition.ChangeOrder(
                orderPlace: OrderPlace.AtEdition,
                order: fakeOrden + 1);
            Assert.Equal(fakeOrden, edition.OrderAtMenu);
            // pruebo que no tenga nuevos eventos de dominio           
            Assert.Single(edition.AfterCommitDomainEvents);
        }

    }
}
