﻿using Pixelario.Subscriptions.Domain.AggregateModels.Accounts;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using System;
using Xunit;

namespace Pixelario.Subscriptions.UnitTests
{
    public class AccountAgreggateTests
    {
        private readonly int fakeUserId = 1;
        private readonly Plan freePlan = Plan.Free;
        [Fact]
        public void T001_Create_Correctly_An_Account()
        {
            var account = new Account(
                  userId: fakeUserId,
                  plan: freePlan);
            // Account is not null
            Assert.NotNull(account);
            // Checking properties
            Assert.Equal(fakeUserId, account.UserId);
            Assert.Equal(freePlan.Id, account.IDPlan);
        }
    }
}
