﻿using System;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.Subscriptions.Domain.AggregateModels.Plans;
using Xunit;

namespace Pixelario.Subscriptions.UnitTests
{
    public class PlanAgreggateTests
    {
        private string fakeCreatedBy = "DomainTestProyect";
        private string fakeEditionTitle = "Título de la edición";
        private string fakeEditionSummary = "Sumario de la edición";
        private string fakeEditionKeyword = "Keywords de la edición";
        private int fakeID = 1;
        private readonly Plan freePlan = Plan.Free;
        private readonly ConstraintType onEdition = ConstraintType.OnEdition;
       
        [Fact]
        public void T001_Create_Correctly_A_Constraint()
        {
            var constraint = new Constraint(
                createdBy: fakeCreatedBy,
                plan: freePlan,
                constraintType: onEdition,
                iDConstrained: fakeID);
            // Plan is not null
            Assert.NotNull(constraint);
            // Checking properties
            Assert.Equal(fakeCreatedBy, constraint.CreatedBy);
            Assert.Equal(freePlan.Id, constraint.IDPlan);
            Assert.Equal(onEdition.Id, constraint.IDConstraintType);
            Assert.Equal(fakeID, constraint.IDConstrained);
        }
    }
}
