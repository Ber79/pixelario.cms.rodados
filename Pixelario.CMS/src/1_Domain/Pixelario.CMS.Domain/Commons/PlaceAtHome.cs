﻿namespace Pixelario.CMS.Domain.Commons
{
    public enum PlaceAtHome
    {
        None=0,
        Slider,
        Columns,
        Boxes
    }
}
