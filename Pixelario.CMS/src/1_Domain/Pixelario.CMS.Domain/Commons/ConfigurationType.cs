﻿namespace Pixelario.CMS.Domain.Commons
{
    public enum ConfigurationType
    {
        LineText = 1,
        AreaText = 2,
        BoolOption = 3,
        Image = 4,
        File=5,
        Complementary = 6,
        MaskedText = 7,
        EnumerationValue = 8,
        EnumerationClass = 9
    }
}
