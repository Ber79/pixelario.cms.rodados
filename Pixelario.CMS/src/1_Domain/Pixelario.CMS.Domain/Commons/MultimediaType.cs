﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pixelario.CMS.Seedwork;

namespace Pixelario.CMS.Domain.Commons
{
    public class MultimediaType : Enumeration
    {
        public static MultimediaType Imagen =
            new MultimediaType(
                id: 1,
                name: nameof(Imagen).ToLowerInvariant(),
                title: "Imagen",
                extensions: new string[] { ".jpg", ".png", ".jpeg" });
        public static MultimediaType Widget =
            new MultimediaType(
                id: 2,
                name: nameof(Widget).ToLowerInvariant(),
                title: "Widget",
                extensions: new string[] { });
        public static MultimediaType Documento =
            new MultimediaType(
                id: 3,
                name: nameof(Documento).ToLowerInvariant(),
                title: "Documento",
                extensions: new string[] {".doc", ".docx", ".ppt", ".xls", ".xlsx", ".pdf" }
                );
        public static MultimediaType Link =
            new MultimediaType(
                id: 4,
                name: nameof(Link).ToLowerInvariant(),
                title: "Link",
                extensions: new string[] { ".jpg", ".png", ".jpeg" });
        public static MultimediaType ScriptOnHeader =
            new MultimediaType(
                id: 5,
                name: nameof(Link).ToLowerInvariant(),
                title: "ScriptOnHeader",
                extensions: new string[] { });
        public static MultimediaType ScriptOnBottom =
            new MultimediaType(
                id: 6,
                name: nameof(Link).ToLowerInvariant(),
                title: "ScriptOnBottom",
                extensions: new string[] { });
        public static MultimediaType ScriptOnArticleBottom =
            new MultimediaType(
                id: 7,
                name: nameof(Link).ToLowerInvariant(),
                title: "ScriptOnArticleBottom",
                extensions: new string[] { });
        public string Title { get; private set; }
        public string[] Extensions { get; private set; }

        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Lista estatica de todos los tipos de registros
        /// </summary>
        public static IEnumerable<MultimediaType> List() =>
            new[] { Imagen, Widget, Documento, Link, ScriptOnHeader, 
                ScriptOnBottom, ScriptOnArticleBottom };


        protected MultimediaType()
        {

        }
        public MultimediaType(int id,string name, string title, 
            string[] extensions) 
            : base(id, name) 
        {
            this.Title = title;
            this.Extensions = extensions;
        }




        public static MultimediaType FromName(string name)
        {
            var metodo = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (metodo == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles tipos de multimedios son: {String.Join(",", List().Select(s => s.Name))}");
            }
            return metodo;
        }
        public static MultimediaType From(int id)
        {
            var metodo = List()
                .SingleOrDefault(s => s.Id == id);
            if (metodo == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles tipos de multimedios son: {String.Join(",", List().Select(s => s.Name))}");
            }
            return metodo;
        }
    }
}