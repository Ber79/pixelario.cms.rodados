﻿namespace Pixelario.CMS.Domain.Commons
{
    public enum ImageType
    {
        Home,
        Vertical,
        Horizontal,
        Slide
    }
}
