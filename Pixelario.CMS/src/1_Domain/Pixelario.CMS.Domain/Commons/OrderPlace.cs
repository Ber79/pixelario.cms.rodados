﻿namespace Pixelario.CMS.Domain.Commons
{
    public enum OrderPlace
    {
        AtHome,
        AtEdition,
        AtMenu
    }
}
