﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public interface IArticleRepository : IRepository<Article>
    {
        Article Add(Article articulo);
        void Update(Article articulo);
        void Delete(Article articulo);
        void Delete(ArticleMultimedia multimedia);
        Task<Article> GetAsync(int idArticle);
        void ReorderArticlesAtPlace(Article article, int LastOrder);
        void OrderArticlesAtPlace(int placeAtHome);
        void ReorderArticlesAtHomeByEdition(Article article, 
            int LastOrder);
        void OrderArticlesAtHomeByEdition(int iDEdition);
    }
}
