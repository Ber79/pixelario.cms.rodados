﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Seedwork;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public class ArticlePlaceAtHome : Enumeration
    {
        public static ArticlePlaceAtHome None =
            new ArticlePlaceAtHome(
                id: 0,
                name: nameof(None).ToLowerInvariant(),
                title: "None",
                editionType: EditionType.None);
        public static ArticlePlaceAtHome Slider =
            new ArticlePlaceAtHome(
                id: 1,
                name: nameof(Slider).ToLowerInvariant(),
                title: "Slider",
                editionType: EditionType.News);
        public static ArticlePlaceAtHome Columns =
            new ArticlePlaceAtHome(
                id: 2,
                name: nameof(Columns).ToLowerInvariant(),
                title: "Columns",
                editionType: EditionType.News);
        public static ArticlePlaceAtHome Boxes =
            new ArticlePlaceAtHome(
                id: 3,
                name: nameof(Boxes).ToLowerInvariant(),
                title: "Boxes",
                editionType: EditionType.Especial_Content);
        public static ArticlePlaceAtHome Aside =
            new ArticlePlaceAtHome(
                id: 4,
                name: nameof(Aside).ToLowerInvariant(),
                title: "Aside",
                editionType: EditionType.Flyer);
        public static ArticlePlaceAtHome Footer_Left =
            new ArticlePlaceAtHome(
                id: 5,
                name: nameof(Footer_Left).ToLowerInvariant(),
                title: "Footer_Left",
                editionType: EditionType.Flyer);
        public static ArticlePlaceAtHome Footer_Right =
            new ArticlePlaceAtHome(
                id: 6,
                name: nameof(Footer_Right).ToLowerInvariant(),
                title: "Footer_Right",
                editionType: EditionType.Flyer);
        public string Title { get; private set; }
        public EditionType EditionType { get; private set; }

        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Lista estatica de todos los tipos de registros
        /// </summary>
        private static IEnumerable<ArticlePlaceAtHome> _list =
            new[] { None, Slider, Columns, Boxes, Aside,
                Footer_Left, Footer_Right};

        public static List<ArticlePlaceAtHome> List(EditionType editionType)
        {
            return
                _list.Where(aph => aph.EditionType == editionType).ToList();
        }

        protected ArticlePlaceAtHome()
        {

        }
        public ArticlePlaceAtHome(int id, string name, string title,
            EditionType editionType)
            : base(id, name)
        {
            this.Title = title;
            this.EditionType = editionType;
        }




        public static ArticlePlaceAtHome FromName(string name)
        {
            var articlePlaceAtHome = _list
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (articlePlaceAtHome == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles lugares son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return articlePlaceAtHome;
        }
        public static ArticlePlaceAtHome From(int id)
        {
            var articlePlaceAtHome = _list
                .SingleOrDefault(s => s.Id == id);
            if (articlePlaceAtHome == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles lugares son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return articlePlaceAtHome;
        }
    }
}