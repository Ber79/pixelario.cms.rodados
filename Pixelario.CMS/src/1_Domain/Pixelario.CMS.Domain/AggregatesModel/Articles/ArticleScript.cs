﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public class ArticleScript : Entity, IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public bool Enabled { get; private set; }
        public int Order { get; private set; }
        
        public ICollection<ArticleScriptItem> Items { get; set; }
        public ICollection<Article> Articles { get; set; }
        protected ArticleScript()
        {
            CreationDate = DateTime.Now;
            Items = new HashSet<ArticleScriptItem>();
            Articles = new HashSet<Article>();
            Enabled = false;
            Order = 0;
        }
        public ArticleScript(string createdBy, string title,
            string summary) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            CreatedBy = createdBy;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary;
        }

        public void AddItem(string createdBy, 
            string title, string code,
            MultimediaType multimediaType)
        {
            this.Items.Add(
                new ArticleScriptItem(
                       createdBy: createdBy,
                       title: title,
                       code: code,
                       multimediaType: multimediaType));
        }

        public void ChangeEnabled(bool enabled)
        {
            if (this.Enabled != enabled)
                this.Enabled = enabled;
        }

        public void ChangeItemEnabled(int iDArticleScriptItem, bool enabled)
        {
            foreach (var item in this.Items)
            {
                if (item.ID == iDArticleScriptItem)
                {
                    item.ChangeEnabled(
                        enabled: enabled);
                }
            }
        }

        public void Update(string title, string summary)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            this.Title = title;
            this.Summary = summary;
        }

        public void UpdateItem(int iDArticleScriptItem, string title, string code,
            MultimediaType multimediaType)
        {
            foreach (var item in this.Items)
            {
                if (item.ID == iDArticleScriptItem)
                {
                    item.Update(
                        title: title,
                        code: code,
                        multimediaType: multimediaType);
                }
            }
        }

        public void DeleteItem(ArticleScriptItem item)
        {
            this.Items.Remove(item);
        }
        public bool HasItem(int iDArticleScriptItem,
            out ArticleScriptItem scriptItem)
        {
            scriptItem = null;
            if (this.Items == null)
                return false;
            foreach (var item in this.Items)
            {
                if (item.ID == iDArticleScriptItem)
                {
                    scriptItem = item;
                    return true;
                }
            }
            return false;
        }
    }
}
