﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Articles;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public class Article : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime? PublicationDate { get; private set; }

        public int IDEdition { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public string Body { get; private set; }
        public string Source { get; private set; }
        public bool Enabled { get; private set; }
        public bool AtHome { get; private set; }
        public int Order { get; set; }
        public int OrderAtEdition { get; private set; }
        public int IDPlaceAtHome { get; private set; }
        public string HomeImage { get; private set; }
        public string SlideImage { get; private set; }
        public string VerticalImage { get; private set; }
        public string HorizontalImage { get; private set; }
        public string Keywords { get; private set; }

        public ICollection<ArticleMultimedia> ArticleMultimedia { get; set; }
        public ICollection<Topic> Topics { get; set; }
        public ICollection<SubTopic> SubTopics { get; set; }
        public Edition Edition { get; set; }
        public ICollection<ArticleScript> Scripts { get; set; }

        protected Article()
        {
            CreationDate = DateTime.Now;
            ArticleMultimedia = new HashSet<ArticleMultimedia>();
            Topics = new HashSet<Topic>();
            SubTopics = new HashSet<SubTopic>();
            this.Scripts = new HashSet<ArticleScript>();
            Order = 0;
            OrderAtEdition = 0;
            IDPlaceAtHome = ArticlePlaceAtHome.None.Id;
            AtHome = false;
            Enabled = false;
            AtHome = false;
        }
        public Article(string createdBy,
            int iDEdition,
            DateTime? publicationDate,
            string title,
            string summary, string body,
            string source,
            string keywords) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            if (string.IsNullOrEmpty(body))
            {
                body = string.Empty;
            }
            if (iDEdition < 0)
            {
                throw new ArgumentException("message", nameof(iDEdition));
            }
            IDEdition = iDEdition;
            CreatedBy = createdBy;
            PublicationDate = publicationDate;
            Title = title;
            Summary = summary;
            Body = body;
            Source = source;
            Keywords = keywords;
        }

        public void Update(DateTime? publicationDate,
            string title, string summary,
            string body, string source,
            string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            if (string.IsNullOrEmpty(body))
            {
                body = string.Empty;
            }
            PublicationDate = publicationDate;
            Title = title;
            Summary = summary;
            Body = body;
            Source = source;
            Keywords = keywords;
        }



        public void ChangeEnabled(
            bool enabled)
        {
            var lastPlaceAtHome = this.IDPlaceAtHome;
            if (this.Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    this.AtHome = false;
                    this.IDPlaceAtHome = ArticlePlaceAtHome.None.Id;
                    this.Order = 0;
                    // Domain event
                    AddAfterCommitDomainEvent(new PlaceAtHomeChangedDomainEvent(
                        article: this,
                        lastPlaceAtHome: lastPlaceAtHome));
                }
            }
        }
        public void ChangeAtHome(bool atHome)
        {
            var lastPlaceAtHome = this.IDPlaceAtHome;
            if (AtHome != atHome)
            {
                if (Edition.EditionType == EditionType.Especial_Content.Id)
                {
                    if (string.IsNullOrEmpty(this.HomeImage) && atHome)
                    {
                        throw new ArticleException("No puede ubicar un articulo en portada sin una imagen de portada.");
                    }
                    if(atHome)
                    {
                        this.IDPlaceAtHome = ArticlePlaceAtHome.Boxes.Id;
                    }
                }
                AtHome = atHome;
                if (!atHome)
                {
                    this.IDPlaceAtHome = ArticlePlaceAtHome.None.Id;
                    this.Order = 0;
                }
                // Domain event
                AddAfterCommitDomainEvent(new PlaceAtHomeChangedDomainEvent(
                    article: this,
                    lastPlaceAtHome: lastPlaceAtHome));
            }
        }

        public void ChangePlaceAtHome(ArticlePlaceAtHome placeAtHome)
        {
            if ((placeAtHome == ArticlePlaceAtHome.Columns ||
                    placeAtHome == ArticlePlaceAtHome.Slider) &&
                    string.IsNullOrEmpty(this.HomeImage))
            {
                throw new ArticleException("No puede ubicar un articulo en portada sin una imagen de portada.");
            }
            if (placeAtHome == ArticlePlaceAtHome.Slider &&
                    string.IsNullOrEmpty(this.VerticalImage))
            {
                throw new ArticleException("No puede ubicar un articulo en el slider sin una imagen vertical.");
            }
            var lastPlaceAtHome = this.IDPlaceAtHome;
            if (this.IDPlaceAtHome != placeAtHome.Id)
            {
                this.IDPlaceAtHome = placeAtHome.Id;
                if (placeAtHome == ArticlePlaceAtHome.None)
                {
                    this.Order = 0;
                }
                // Domain event
                AddAfterCommitDomainEvent(new PlaceAtHomeChangedDomainEvent(
                    article: this,
                    lastPlaceAtHome: lastPlaceAtHome));
            }
        }
        public void ChangeOrder(
            int order,
            OrderPlace orderPlace)
        {
            if (order < 0)
                throw new ArticleException("El order del articulo es menor que cero");
            int lastOrder = this.Order;
            if (Order != order &&
                orderPlace == OrderPlace.AtHome)
            {
                Order = order;
            }
            else if (OrderAtEdition != order &&
                orderPlace == OrderPlace.AtEdition)
            {
                OrderAtEdition = order;
            }
            AddAfterCommitDomainEvent(new OrderChangedDomainEvent(
                 article: this,
                 orderPlace: orderPlace,
                 lastOrder: lastOrder));
        }

        public void ChangeImage(ImageType imageType,
            string uri)
        {
            if (imageType == ImageType.Home)
            {
                if (string.IsNullOrEmpty(uri) && this.AtHome &&
                    (this.IDPlaceAtHome == ArticlePlaceAtHome.Boxes.Id ||
                        this.IDPlaceAtHome == ArticlePlaceAtHome.Slider.Id))
                    throw new ArticleException("No puede quitar la imagen de portada si el articulo está en la portada.");
                HomeImage = uri;
            }
            else if (imageType == ImageType.Slide)
            {
                if (string.IsNullOrEmpty(uri) && this.AtHome &&
                    this.IDPlaceAtHome == ArticlePlaceAtHome.Slider.Id)
                    throw new ArticleException("No puede quitar la imagen de slide si el articulo está en la portada.");
                SlideImage = uri;
            }
            else if (imageType == ImageType.Vertical)
            {
                if (string.IsNullOrEmpty(uri) && this.AtHome &&
                    this.IDPlaceAtHome == ArticlePlaceAtHome.Slider.Id)
                    throw new ArticleException("No puede quitar la imagen vertical si el articulo está en la portada.");
                VerticalImage = uri;
            }
            else if (imageType == ImageType.Horizontal)
                HorizontalImage = uri;
        }


        public void AddMultimedia(string createdBy,
            string title, string summary,
            string path1, string path2,
            MultimediaType multimediaType)
        {
            ArticleMultimedia.Add(new ArticleMultimedia(
                  createdBy: createdBy,
                  title: title,
                  summary: summary,
                  path1: path1,
                  path2: path2,
                  iDMultimediaType: multimediaType.Id));
        }

        public void BindTopic(
            Topic topic)
        {
            Topics.Add(topic);
        }
        public void UnbindTopic(
            Topic topic)
        {
            Topics.Remove(topic);
        }
        public void BindSubTopic(
            SubTopic subTopic)
        {
            SubTopics.Add(subTopic);
        }
        public void UnbindSubTopic(
            SubTopic subTopic)
        {
            SubTopics.Remove(subTopic);
        }

        public bool ContainsMultimedia(int iDMultimedia,
            out ArticleMultimedia multimedio)
        {
            multimedio = null;
            if (this.ArticleMultimedia == null)
                return false;
            foreach (var item in this.ArticleMultimedia)
            {
                if (item.ID == iDMultimedia)
                {
                    multimedio = item;
                    return true;
                }
            }
            return false;
        }

        public void RemoveMultimedia(ArticleMultimedia multimedio)
        {
            this.ArticleMultimedia.Remove(multimedio);
        }

        public void TransferToEdition(int iDEdition)
        {
            if (iDEdition <= 0)
                throw new ArgumentNullException(nameof(iDEdition));
            if (IDEdition == iDEdition)
                throw new ArticleException("El árticulo ya se encuentra en esta edición");
            if (this.IDEdition != iDEdition)
            {
                this.IDEdition = iDEdition;
            }
        }
        public bool ScriptIsBind(ArticleScript script)
        {
            return this.Scripts
                .Where(s => s.ID == script.ID)
                .Count() > 0;
        }
        public void BindScript(ArticleScript script)
        {
            if (!this.ScriptIsBind(script))
                this.Scripts.Add(script);
        }

        public void UnbindScript(ArticleScript script)
        {
            if (this.ScriptIsBind(script))
                this.Scripts.Remove(script);
        }
    }
}
