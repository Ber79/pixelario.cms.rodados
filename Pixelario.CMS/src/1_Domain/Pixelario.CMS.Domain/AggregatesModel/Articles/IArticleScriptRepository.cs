﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public interface IArticleScriptRepository : IRepository<ArticleScript>
    {
        ArticleScript Add(ArticleScript articleScript);
        void Update(ArticleScript articleScript);
        void Delete(ArticleScript articleScript);
        void Delete(ArticleScriptItem articleScriptItem);
        Task<ArticleScript> GetAsync(int iDArticleScript);
    }
}
