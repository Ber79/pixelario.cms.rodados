﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.CMS.Domain.AggregatesModel.Articles
{
    public class ArticleScriptItem : Entity
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }

        public string Title { get; private set; }
        public string Code { get; private set; }
        public bool Enabled { get; private set; }
        public int Order { get; private set; }
        public int IDArticleScript { get; private set; }
        public ArticleScript ArticleScript { get; set; }
        public int IDMultimediaType { get; private set; }
        public MultimediaType MultimediaType
        {
            get
            {
                return MultimediaType.FromValue<MultimediaType>(IDMultimediaType);
            }
        }
        protected ArticleScriptItem()
        {
            CreationDate = DateTime.Now;
            Enabled = false;
            Order = 0;
        }
        public ArticleScriptItem(string createdBy, string title,
            string code, MultimediaType multimediaType) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(code))
            {
                throw new ArgumentException("message", nameof(code));
            }
            this.CreatedBy = createdBy;
            this.Title = title;
            this.Code = code;
            this.IDMultimediaType = multimediaType.Id;
        }

        public void ChangeEnabled(bool enabled)
        {
            if (this.Enabled != enabled)
                this.Enabled = enabled;
        }

        public void Update(string title, string code,
            MultimediaType multimediaType)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(code))
            {
                throw new ArgumentException("message", nameof(code));
            }
            this.Title = title;
            this.Code = code;
            this.IDMultimediaType = multimediaType.Id;
        }
    }
}
