﻿using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Configurations
{
     public interface IConfigurationRepository : IRepository<Configuration>
    {
        Configuration Add(Configuration configuration);
        void Update(Configuration configuration);
        void Delete(Configuration configuration);        
        Task<Configuration> GetAsync(int iDConfiguration);
        Task<Configuration> GetAsync(string key);
        Task<ConfigurationOption> GetConfigurationOptionAsync(int iDConfigurationOption);
        void DeleteConfigurationOption(ConfigurationOption configurationOption);
    }
}
