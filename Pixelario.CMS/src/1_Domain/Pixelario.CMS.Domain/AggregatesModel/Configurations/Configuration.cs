﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixelario.CMS.Domain.AggregatesModel.Configurations
{
    public class Configuration : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Title { get; private set; }
        public string Key { get; private set; }
        public string State { get; private set; }
        public string Roles { get; private set; }
        public ConfigurationType ConfigurationType { get; set; }

        public ICollection<ConfigurationOption> ConfigurationOptions { get; set; }


        protected Configuration()
        {
            CreationDate = DateTime.Now;
            State = default(string);
            this.ConfigurationOptions = new HashSet<ConfigurationOption>();
        }
        public Configuration(string createdBy, string title,
            string key,
            ConfigurationType configurationType) : this()
        {
            CreatedBy = createdBy ?? throw new ArgumentNullException(nameof(createdBy));
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Key = key ?? throw new ArgumentNullException(nameof(key));
            ConfigurationType = configurationType;
        }
        public void Update(string solicitedBy, string title,
            string key,
            ConfigurationType configurationType)
        {
            if (solicitedBy == null)
            {
                throw new ArgumentNullException(nameof(solicitedBy));
            }

            if (title == null)
            {
                throw new ArgumentNullException(nameof(title));
            }

            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            Title = title;
            Key = key;
            ConfigurationType = configurationType;
        }
        public void SetState(string state)
        {
            State = state;
        }
        public string[] GetBindedRoles()
        {
            if (string.IsNullOrEmpty(this.Roles))
                return new string[]{ };
            return this.Roles.Split(';');
        }
        public void BindToRole(string roleName)
        {
            if (string.IsNullOrEmpty(this.Roles))
            {
                this.Roles = roleName;
            }
            else
            {
                if (!this.Roles.Contains(roleName))
                {
                    this.Roles += $";{roleName}";
                }
            }
        }
        public void UnbindToRole(string roleName)
        {
            if (!string.IsNullOrEmpty(Roles))
            {                
                var _actualRoles = this.GetBindedRoles();
                var _rolesUpdated = new List<string>();
                for (int i=0; i< _actualRoles.Length; i++)
                {
                    if(_actualRoles[i] != roleName)
                    {
                        _rolesUpdated.Add(_actualRoles[i]);
                    }
                }
                this.Roles = string.Join(";", _rolesUpdated);
            }            
        }


        public void AddConfigurationOption(string solicitedBy, string value)
        {
            ConfigurationOptions.Add(new ConfigurationOption(
                  createdBy: solicitedBy,
                  value: value));
        }

        public void UpdateConfigurationOption(int iDConfigurationOption,
            string value)
        {
            foreach (var configurationOption in this.ConfigurationOptions)
            {
                if (configurationOption.ID == iDConfigurationOption)
                {
                    configurationOption.Update(
                        value: value);
                }
            }
        }
        public bool HasConfigurationOption(int iDConfigurationOption,
            out ConfigurationOption configurationOption)
        {
            configurationOption = null;
            if (this.ConfigurationOptions == null)
                return false;
            foreach (var item in this.ConfigurationOptions)
            {
                if (item.ID == iDConfigurationOption)
                {
                    configurationOption = item;
                    return true;
                }
            }
            return false;
        }
        public void ChangeConfigurationOptionEnabled(int iDConfigurationOption, bool enabled)
        {
            foreach (var configurationOption in this.ConfigurationOptions)
            {
                if (configurationOption.ID == iDConfigurationOption)
                {
                    configurationOption.ChangeEnabled(
                        enabled: enabled);
                }
            }
        }

        public void DeleteConfigurationOption(ConfigurationOption configurationOption)
        {
            ConfigurationOptions.Remove(configurationOption);
        }

    }
}
