﻿using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.CMS.Domain.AggregatesModel.Configurations
{
    public class ConfigurationOption : Entity
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Value { get; private set; }
        public int Order { get; set; }
        public bool Enabled { get; private set; }
        public int IDConfiguration { get; set; }
        public Configuration Configuration { get; set; }

        private ConfigurationOption()
        {
            CreationDate = DateTime.Now;
            Order = 0;
            Enabled = false;

        }
        public  ConfigurationOption(string createdBy, string value) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("message", nameof(value));
            }
            this.CreatedBy = createdBy;
            this.Value = value;
        }
        public void Update(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("message", nameof(value));
            }
            this.Value = value;
        }
        public void ChangeEnabled(
            bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    this.Order = 0;                  
                }
            }
        }

    }
}
