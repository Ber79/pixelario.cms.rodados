﻿using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Editions
{
    public interface IEditionRepository : IRepository<Edition>
    {
        Edition Add(Edition edition);
        void Update(Edition edition);
        void Delete(Edition edition);
        Task<Edition> GetAsync(int iDEdition);
        void ReorderOnType(Edition edition, int LastOrder);
        void ReOrderMenuOnType(Edition edition, int lastOrder);
        void OrderOnType(int IDEditionType);
        void OrderMenuOnTypes(int? iDEditionException);
        void OrderOnTypes(int? iDEditionException);
    }
}
