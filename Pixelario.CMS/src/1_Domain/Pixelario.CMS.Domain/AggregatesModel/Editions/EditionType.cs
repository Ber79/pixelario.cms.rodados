﻿using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pixelario.CMS.Domain.AggregatesModel.Editions
{
    public class EditionType : Enumeration
    {
        public static EditionType None =
            new EditionType(
                id: 0,
                name: nameof(None).ToLowerInvariant(),
                title: "None",
                enabled: true);
        public static EditionType News =
            new EditionType(
                id: 1,
                name: nameof(News).ToLowerInvariant(),
                title: "News",
                enabled: true);
        public static EditionType Flyer =
            new EditionType(
                id: 2,
                name: nameof(Flyer).ToLowerInvariant(),
                title: "Flyer",
                enabled: false);
        public static EditionType Especial_Content =
            new EditionType(
                id: 3,
                name: nameof(Especial_Content).ToLowerInvariant(),
                title: "Especial_Content",
                enabled: true);
        public static EditionType Information =
            new EditionType(
                id: 4,
                name: nameof(Information).ToLowerInvariant(),
                title: "Information",
                enabled: true);
        public string Title { get; private set; }
        public bool Enabled { get; private set; }

        protected EditionType()
        {

        }
        public EditionType(int id, string name,
            string title, bool enabled)
            : base(id, name)
        {
            this.Title = title;
            this.Enabled = enabled;
        }
        private static IEnumerable<EditionType> _list =
                new[] { None, News, Flyer, Especial_Content,
                Information};

        public static List<EditionType> List()
        {
            return _list.Where(et => et.Enabled).ToList();
        }
        public static EditionType FromName(string name)
        {
            var editionsType = _list
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (editionsType == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles tipos de ediciones son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return editionsType;
        }
        public static EditionType From(int id)
        {
            var editionsType = _list
                .SingleOrDefault(s => s.Id == id);
            if (editionsType == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles tipos de ediciones son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return editionsType;
        }
    }
}
