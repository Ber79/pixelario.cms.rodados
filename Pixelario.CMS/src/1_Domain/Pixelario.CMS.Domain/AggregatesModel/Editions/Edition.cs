﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Editions;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Domain.AggregatesModel.Editions
{
    public class Edition : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public bool Enabled { get; private set; }
        public bool AtHome { get; private set; }
        public bool OnMenu { get; private set; }
        public int Order { get; set; }
        public int OrderAtMenu { get; set; }
        public int EditionType { get; private set; }
        public string HomeImage { get; private set; }
        public string Keywords { get; private set; }

        public ICollection<Topic> Topics { get; set; }
        public ICollection<Page> Pages { get; set; }
        public ICollection<Article> Articles { get; set; }

        protected Edition()
        {
            Topics = new HashSet<Topic>();
            Pages = new HashSet<Page>();
            CreationDate = DateTime.Now;
            Order = 0;
            OrderAtMenu = 0;
            AtHome = false;
            OnMenu = false;
            Enabled = false;
            EditionType = 0;
        }
        public Edition(string createdBy, string title,
            string summary, string keywords) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            CreatedBy = createdBy;
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }

        public void Update(
            string title,
            string summary, string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }

        public void ChangeEnabled(
            bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    var lastOrder = this.Order;
                    this.AtHome = false;
                    this.Order = 0;
                    // Domain event
                    AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                        edition: this));
                }
            }
        }
        public void ChangeAtHome(
            bool atHome)
        {
            if (AtHome != atHome)
            {
                AtHome = atHome;                
                AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                    edition: this));
            }
        }

        public void ChangeOrder(
            int order,
            OrderPlace orderPlace)
        {
            if (order < 0)
                throw new EditionException("El orden de la edición es menor que cero");
            var lastOrder = 0;
            if (Order != order && 
                orderPlace == OrderPlace.AtHome)
            {
                lastOrder = Order;
                Order = order;
                AddAfterCommitDomainEvent(new OrderChangedDomainEvent(
                    edition: this,
                    lastOrder: lastOrder));
            }
            else if (OrderAtMenu != order && 
                orderPlace == OrderPlace.AtMenu)
            {
                lastOrder = OrderAtMenu;
                OrderAtMenu = order;
                AddAfterCommitDomainEvent(new OrderAtMenuChangedDomainEvent(
                    edition: this,
                    lastOrder: lastOrder));

            }
        }
        public void ChangeOnMenu(
            bool onMenu)
        {
            if (OnMenu != onMenu)
            {
                OnMenu = onMenu;
                AddAfterCommitDomainEvent(new OnMenuChangedDomainEvent(
                    edition: this));
            }
        }

        public void ChangeEditionType(
            EditionType editionType)
        {
            if (this.EditionType != editionType.Id)
            {
                this.EditionType = editionType.Id;
                //Todo: change on Database EditionType for IDEditionType
                if(this.EditionType ==
                    Pixelario.CMS.Domain.AggregatesModel.Editions.EditionType.None.Id)
                {
                    Order = 0;
                }
                AddAfterCommitDomainEvent(new EditionTypeChangedDomainEvent(
                    edition:this));
            }
        }
        public void ChangeImage(
            ImageType imageType, string uri)
        {
            if (imageType == ImageType.Home)
                HomeImage = uri;
            else
            {
                throw new EditionException("Tipo de imagen de edición no disponible.");
            }
        }

    }
}
