﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Pages
{
    public interface IPageRepository : IRepository<Page>
    {
        Page Add(Page page);
        void Update(Page page);
        void Delete(Page page);
        void Delete(PageMultimedia multimedia);
        Task<Page> GetAsync(int iDPage);
        void ReorderPagesAtHome(Page page, int LastOrder);
        void ReorderPagesAtMenu(Page page, int LastOrder);
        void OrderPagesAtMenu(Edition edition);
        void OrderPagesAtHome();

    }
}
