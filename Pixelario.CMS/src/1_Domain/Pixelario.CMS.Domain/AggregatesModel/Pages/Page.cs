﻿using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Pages;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Domain.AggregatesModel.Pages
{
    public class Page : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public bool Enabled { get; set; }
        public bool AtHome { get; set; }
        public int Order { get; set; }
        public bool OnMenu { get; set; }
        public int OrderAtMenu { get; set; }
        public string HomeImage { get; set; }
        public string Keywords { get; set; }

        public ICollection<PageMultimedia> PageMultimedia { get; set; }
        public ICollection<Edition> Editions { get; set; }

        protected Page()
        {
            CreationDate = DateTime.Now;
            PageMultimedia = new HashSet<PageMultimedia>();
            Order = 0;
            AtHome = false;
            Enabled = false;
            Editions = new HashSet<Edition>();

        }
        public Page(string createdBy, string title,
            string summary, string body, string keywords) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            CreatedBy = createdBy;
            Title = title;
            Summary = summary;
            Body = body;
            Keywords = keywords;
        }
        public void Update(
            string title,
            string summary, string body,
            string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            if (string.IsNullOrEmpty(body))
            {
                throw new ArgumentException("message", nameof(body));
            }
            Title = title;
            Summary = summary;
            Body = body;
            Keywords = keywords;
        }

        public void ChangeEnabled(
            bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    this.AtHome = false;
                    this.OnMenu = false;
                    this.Order = 0;
                    this.OrderAtMenu = 0;
                    // Domain event
                    AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                        page: this));
                    AddAfterCommitDomainEvent(new OnMenuChangedDomainEvent(
                        page: this));
                }
            }
        }

        public void ChangeOrder(
            int order,
            OrderPlace orderPlace)
        {
            if (order < 0)
                throw new PageException("El order del articulo es menor que cero");
            var lastOrder = 0;

            if (orderPlace == OrderPlace.AtHome)
            {
                if (Order != order)
                {
                    lastOrder = Order;
                    Order = order;
                    AddAfterCommitDomainEvent(new OrderAtHomeChangedDomainEvent(
                        page: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }
            else if (orderPlace == OrderPlace.AtMenu)
            {
                if (OrderAtMenu != order)
                {
                    lastOrder = OrderAtMenu;
                    OrderAtMenu = order;
                    AddAfterCommitDomainEvent(new OrderAtMenuChangedDomainEvent(
                        page: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }
        }

        public void ChangeImage(
            ImageType imageType, string uri)
        {
            if (imageType == ImageType.Home)
                HomeImage = uri;
            else
            {
                throw new EditionException("Tipo de imagen de página no disponible.");
            }
        }

        public void ChangeAtHome(bool atHome)
        {
            if (AtHome != atHome)
            {
                AtHome = atHome;
                if (!atHome)
                {
                    this.Order = 0;
                }
                AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                    page: this));
            }
        }
        public void BindEdition(
            Edition edition)
        {
            Editions.Add(edition);
        }
        public void UnbindEdition(
            Edition edition)
        {
            Editions.Remove(edition);
        }
        public bool ContainsMultimedia(int iDMultimedia,
            out PageMultimedia multimedia)
        {
            multimedia = null;
            if (this.PageMultimedia == null)
                return false;
            foreach (var item in this.PageMultimedia)
            {
                if (item.ID == iDMultimedia)
                {
                    multimedia = item;
                    return true;
                }
            }
            return false;
        }


        public void AddMultimedia(string createdBy,
            string title, string summary,
            string path1, string path2,
            MultimediaType multimediaType)
        {
            PageMultimedia.Add(new PageMultimedia(
                  createdBy: createdBy,
                  title: title,
                  summary: summary,
                  path1: path1,
                  path2: path2,
                  iDMultimediaType: multimediaType.Id));
        }
        public void RemoveMultimedia(PageMultimedia multimedia)
        {
            this.PageMultimedia.Remove(multimedia);
        }
        public void ChangeOnMenu(bool onMenu)
        {
            if (OnMenu != onMenu)
            {
                OnMenu = onMenu;
                if (!onMenu)
                {
                    this.OrderAtMenu = 0;
                }
                AddAfterCommitDomainEvent(new OnMenuChangedDomainEvent(
                    page: this));
            }
        }
    }
}
