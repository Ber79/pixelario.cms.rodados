﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.CMS.Domain.AggregatesModel.Pages
{
    public class PageMultimedia : Entity
    {
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }

        public string Path1 { get; private set; }
        public string Path2 { get; private set; }
        public bool Enabled { get; private set; }
        public bool AtHome { get; private set; }
        public int Order { get; private set; }
        public bool Cover { get; private set; }
        public int IDMultimediaType { get; private set; }
        public MultimediaType MultimediaType
        {
            get
            {
                return MultimediaType.FromValue<MultimediaType>(IDMultimediaType);
            }
        }
        public int IDPage { get; private set; }
        public Page Page { get; set; }

        protected PageMultimedia()
        {
            CreationDate = DateTime.Now;
            Enabled = true;
            AtHome = false;
            Cover = false;
            Order = 0;
        }
        public PageMultimedia(string createdBy, string title,
            string summary, string path1,
            string path2, int iDMultimediaType) : this()
        {
            CreatedBy = createdBy;
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Summary = summary;
            Path1 = path1;
            Path2 = path2;
            IDMultimediaType = iDMultimediaType;
        }
    }
}
