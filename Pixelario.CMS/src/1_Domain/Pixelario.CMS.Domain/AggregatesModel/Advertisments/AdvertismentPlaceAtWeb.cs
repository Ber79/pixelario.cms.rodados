﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pixelario.CMS.Seedwork;

namespace Pixelario.CMS.Domain.AggregatesModel.Advertisments
{
    public class AdvertismentPlaceAtWeb : Enumeration
    {
        public static AdvertismentPlaceAtWeb None =
            new AdvertismentPlaceAtWeb(
                id: 0,
                name: nameof(None).ToLowerInvariant(),
                title: "None",
                enabled: true);
        public static AdvertismentPlaceAtWeb Columns =
            new AdvertismentPlaceAtWeb(
                id: 1,
                name: nameof(Columns).ToLowerInvariant(),
                title: "Columns",
                enabled: false);
        public static AdvertismentPlaceAtWeb TopNews =
            new AdvertismentPlaceAtWeb(
                id: 2,
                name: nameof(TopNews).ToLowerInvariant(),
                title: "TopNews",
                enabled: false);
        public static AdvertismentPlaceAtWeb Aside =
            new AdvertismentPlaceAtWeb(
                id: 3,
                name: nameof(Aside).ToLowerInvariant(),
                title: "Aside",
                enabled: true);
        public static AdvertismentPlaceAtWeb Footer_Left =
            new AdvertismentPlaceAtWeb(
                id: 4,
                name: nameof(Footer_Left).ToLowerInvariant(),
                title: "Footer_Left",
                enabled: true);
        public static AdvertismentPlaceAtWeb Footer_Right =
            new AdvertismentPlaceAtWeb(
                id: 5,
                name: nameof(Footer_Right).ToLowerInvariant(),
                title: "Footer_Right",
                enabled: true);
        public static AdvertismentPlaceAtWeb AsideTop =
            new AdvertismentPlaceAtWeb(
                id: 6,
                name: nameof(AsideTop).ToLowerInvariant(),
                title: "AsideTop",
                enabled: true);
        public static AdvertismentPlaceAtWeb Script =
            new AdvertismentPlaceAtWeb(
                id: 7,
                name: nameof(Script).ToLowerInvariant(),
                title: "Script",
                enabled: true);

        public static AdvertismentPlaceAtWeb ArticleBottom =
            new AdvertismentPlaceAtWeb(
                id: 8,
                name: nameof(Script).ToLowerInvariant(),
                title: "ArticleBottom",
                enabled: true);

        public string Title { get; private set; }
        public bool Enabled { get; private set; }

        /// <summary>
        /// Autor: Ing. Bernardo Raskovsky
        /// Función: Lista estatica de todos los tipos de registros
        /// </summary>
        private static IEnumerable<AdvertismentPlaceAtWeb> _list =
            new[] { None, Columns, TopNews, AsideTop, Aside,
                Footer_Left, Footer_Right, Script, ArticleBottom};

        public static List<AdvertismentPlaceAtWeb> List()
        {
            return _list.Where(apw=>apw.Enabled).ToList();
        }

        protected AdvertismentPlaceAtWeb()
        {

        }
        public AdvertismentPlaceAtWeb(int id, string name, 
            string title, bool enabled)
            : base(id, name)
        {
            this.Title = title;
            this.Enabled = enabled;
        }
                     
        public static AdvertismentPlaceAtWeb FromName(string name)
        {
            var articlePlaceAtWeb = _list
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (articlePlaceAtWeb == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles lugares son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return articlePlaceAtWeb;
        }
        public static AdvertismentPlaceAtWeb From(int id)
        {
            var articlePlaceAtWeb = _list
                .SingleOrDefault(s => s.Id == id);
            if (articlePlaceAtWeb == null)
            {
                //TODO: Adding to common exception
                throw new Exception($"Los posibles lugares son: {String.Join(",", _list.Select(s => s.Name))}");
            }
            return articlePlaceAtWeb;
        }
    }
}