﻿using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Advertisments;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.CMS.Domain.AggregatesModel.Advertisments
{
    public class Advertisment : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime? PublicationStartDate { get; private set; }
        public DateTime? PublicationEndDate { get; private set; }
        public string Title { get; private set; }
        public string Code { get; private set; }
        public string URL { get; private set; }
        public bool Published { get; private set; }
        public bool Enabled { get; private set; }
        public int Order { get; set; }
        public int IDPlaceAtWeb { get; private set; }
        public string HomeImage { get; private set; }
        public string VerticalImage { get; private set; }
        public string HorizontalImage { get; private set; }
        protected Advertisment()
        {
            CreationDate = DateTime.Now;
            Order = 0;
            IDPlaceAtWeb = AdvertismentPlaceAtWeb.None.Id;
            Enabled = false;
            Published = false;
        }
        public Advertisment(string createdBy,
            string title,
            string code, string url) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            CreatedBy = createdBy;
            Title = title;
            Code = code;
            URL = url;
        }

        public void Update(
            string title, string code,
            string url)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            Title = title;
            Code = code;
            URL = url;
        }



        public void ChangeEnabled(
            bool enabled)
        {
            Enabled = enabled;
        }
        public void ChangePublish(bool publish)
        {
            //if (publish && string.IsNullOrEmpty(this.HomeImage) &&
            //    string.IsNullOrEmpty(this.VerticalImage) &&
            //    string.IsNullOrEmpty(this.HorizontalImage))
            //{
            //    throw new AdvertismentException("No puede publicar un flyer sin imagenes.");
            //}
            if (!Published && !PublicationStartDate.HasValue)
            {
                PublicationStartDate = DateTime.Now;
            }
            else if (Published)
            {
                PublicationEndDate = DateTime.Now;
            }
            Published = publish;
        }

        public void ChangePlaceAtWeb(AdvertismentPlaceAtWeb placeAtWeb)
        {
            if (placeAtWeb == AdvertismentPlaceAtWeb.Script &&
                string.IsNullOrEmpty(this.Code))
            {
                throw new AdvertismentException("No puede publicar el flyer sin código.");
            }
            var lastPlaceAtWeb = this.IDPlaceAtWeb;
            this.IDPlaceAtWeb = placeAtWeb.Id;
            if (placeAtWeb == AdvertismentPlaceAtWeb.None)
                this.Order = 0;
            // Domain event
            AddAfterCommitDomainEvent(new PlaceAtWebChangedDomainEvent(
                advertisment: this,
                lastPlaceAtWeb: lastPlaceAtWeb));

        }
        public void ChangeOrder(
            int order)
        {
            if (order < 0)
                throw new AdvertismentException("El orden del flyer es menor que cero");
            int lastOrder = this.Order;
            Order = order;
            AddAfterCommitDomainEvent(new OrderChangedDomainEvent(
                advertisment: this,
                lastOrder: lastOrder));

        }

        public void ChangeImage(ImageType imageType,
            string uri)
        {
            if (string.IsNullOrEmpty(uri) && this.Published)
                throw new AdvertismentException("No puede quitar la imagen si el flyer está en la portada.");
            if (imageType == ImageType.Home)
            {
                HomeImage = uri;
            }
            else if (imageType == ImageType.Vertical)
            {
                VerticalImage = uri;
            }
            else if (imageType == ImageType.Horizontal)
                HorizontalImage = uri;
        }
    }
}
