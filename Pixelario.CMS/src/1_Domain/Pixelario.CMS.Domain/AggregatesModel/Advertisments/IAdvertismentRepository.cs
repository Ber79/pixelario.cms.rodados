﻿using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Advertisments
{
    public interface IAdvertismentRepository : IRepository<Advertisment>
    {
        Advertisment Add(Advertisment advertisment);
        void Update(Advertisment advertisment);
        void Delete(Advertisment advertisment);
        Task<Advertisment> GetAsync(int idAdvertisment);
        void ReorderAdvertismentsAtPlace(Advertisment advertisment, int LastOrder);
        void OrderAdvertismentsAtPlace(int placeAtWeb);
    }
}
