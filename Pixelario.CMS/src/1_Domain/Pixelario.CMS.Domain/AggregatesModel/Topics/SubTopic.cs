﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Topics;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Domain.AggregatesModel.Topics
{
    public class SubTopic : Entity
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public bool Enabled { get; private set; }
        public bool AtHome { get; private set; }
        public int Order { get; set; }
        public bool OnMenu { get; private set; }
        public int OrderAtMenu { get; set; }

        public string HomeImage { get; private set; }
        public string Keywords { get; private set; }

        public int IDTopic { get; set; }
        public Topic Topic { get; set; }
        public ICollection<Article> Articles { get; set; }

        protected SubTopic()
        {
            CreationDate = DateTime.Now;
            Order = 0;
            AtHome = false;
            Enabled = false;
            AtHome = false;
            Articles = new HashSet<Article>();
        }
        public SubTopic(string createdBy, string title,
            string summary, string keywords) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            CreatedBy = createdBy;
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }

        public void ChangeEnabled(
            bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    this.AtHome = false;
                    this.Order = 0;
                    this.OnMenu = false;
                    this.OrderAtMenu = 0;
                    // Domain event
                    AddAfterCommitDomainEvent(new SubTopicAtHomeChangedDomainEvent(
                        subTopic: this));
                    AddAfterCommitDomainEvent(new SubTopicOnMenuChangedDomainEvent(
                        subTopic: this));

                }
            }
        }

        internal void Update(
            string title, string summary, string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            this.Title = title;
            this.Summary = summary;
            this.Keywords = keywords;
        }

        internal void ChangeOrder(
            int order, OrderPlace orderPlace)
        {
            if (order < 0)
                throw new TopicException("El orden del sub tema es menor que cero");
            var lastOrder = 0;
            if (orderPlace == OrderPlace.AtMenu)
            {
                if (this.OrderAtMenu != order)
                {
                    lastOrder = this.OrderAtMenu;
                    this.OrderAtMenu = order;
                    AddAfterCommitDomainEvent(new SubTopicOrderAtMenuChangedDomainEvent(
                        subTopic: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }
            else if (orderPlace == OrderPlace.AtHome)
            {
                if (Order != order)
                {
                    lastOrder = Order;
                    Order = order;
                    AddAfterCommitDomainEvent(new SubTopicOrderAtHomeChangedDomainEvent(
                        subTopic: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }
        }

        public void ChangeAtHome(
            bool atHome)
        {
            if (AtHome != atHome)
            {
                AtHome = atHome;
                if (!atHome)
                {
                    this.Order = 0;
                }
                AddAfterCommitDomainEvent(new SubTopicAtHomeChangedDomainEvent(
                    subTopic: this));
            }
        }

        public void ChangeOnMenu(bool onMenu)
        {
            if (OnMenu != onMenu)
            {
                OnMenu = onMenu;
                if (!onMenu)
                {
                    this.OrderAtMenu = 0;
                }
                AddAfterCommitDomainEvent(new SubTopicOnMenuChangedDomainEvent(
                    subTopic: this));
            }
        }

        public void ChangeImage(
            ImageType imageType, string uri)
        {    
            if (imageType == ImageType.Home)
                HomeImage = uri;
            else
                throw new TopicException("Tipo de imagen de sub tema no disponible.");
        }
    }
}
