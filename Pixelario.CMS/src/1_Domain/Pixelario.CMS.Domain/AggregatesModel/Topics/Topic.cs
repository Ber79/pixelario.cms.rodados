﻿using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.AggregatesModel.Editions;
using Pixelario.CMS.Domain.Commons;
using Pixelario.CMS.Domain.Events.Topics;
using Pixelario.CMS.Domain.Exceptions;
using Pixelario.CMS.Seedwork;
using System;
using System.Collections.Generic;

namespace Pixelario.CMS.Domain.AggregatesModel.Topics
{
    public class Topic : Entity,
        IAggregateRoot
    {
        public string CreatedBy { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public bool Enabled { get; private set; }
        public bool AtHome { get; private set; }
        public int Order { get; set; }
        public bool OnMenu { get; private set; }
        public int OrderAtMenu { get; set; }

        public string HomeImage { get; private set; }
        public string Keywords { get; private set; }

        public ICollection<SubTopic> SubTopics { get; set; }
        public ICollection<Edition> Editions { get; set; }
        public ICollection<Article> Articles { get; set; }


        protected Topic()
        {
            CreationDate = DateTime.Now;
            Order = 0;
            AtHome = false;
            Enabled = false;
            SubTopics = new HashSet<SubTopic>();
            Editions = new HashSet<Edition>();
            Articles = new HashSet<Article>();
        }
        public Topic(string createdBy, string title,
            string summary, string keywords) : this()
        {
            if (string.IsNullOrEmpty(createdBy))
            {
                throw new ArgumentException("message", nameof(createdBy));
            }
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            CreatedBy = createdBy;
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }

        public void UpdateSubTopic(int iDSubTopic, 
            string title, string summary, string keywords)
        {
            foreach (var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.Update(                        
                        title: title,
                        summary: summary, 
                        keywords: keywords);
                }
            }

        }

        public void ChangeSubTopicOrder(
            int iDSubTopic, int order, 
            OrderPlace orderPlace)
        {
            foreach (var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.ChangeOrder(
                        order: order,
                        orderPlace: orderPlace);
                }
            }
        }

        public void ChangeSubTopicAtHome(
            int iDSubTopic, bool atHome)
        {
            foreach (var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.ChangeAtHome(
                        atHome: atHome);
                }
            }
        }
        public void ChangeSubTopicOnMenu(
           int iDSubTopic, bool onMenu)
        {
            foreach (var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.ChangeOnMenu(
                        onMenu: onMenu);
                }
            }
        }

        public void ChangeSubTopicEnabled(int iDSubTopic, bool enabled)
        {
            foreach(var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.ChangeEnabled(
                        enabled: enabled);
                }
            }
        }

        public bool HasSubTopic(int iDSubTopic,
            out SubTopic subTopic)
        {
            subTopic = null;
            if (this.SubTopics == null)
                return false;
            foreach(var item in this.SubTopics)
            {
                if (item.ID == iDSubTopic)
                {
                    subTopic = item;
                    return true;
                }                    
            }
            return false;
        }

        public void Update(
            string title,  
            string summary, string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            Title = title;
            Summary = summary;
            Keywords = keywords;
        }



        public void ChangeEnabled(
            bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                if (!enabled)
                {
                    this.AtHome = false;
                    this.Order = 0;
                    this.OnMenu = false;
                    this.OrderAtMenu = 0;
                    // Domain event
                    AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                        topic: this));
                    AddAfterCommitDomainEvent(new OnMenuChangedDomainEvent(
                        topic: this));
                }
            }
        }

        public void ChangeOrder(
            int order,
            OrderPlace orderPlace)
        {
            if (order < 0)
                throw new TopicException("El orden del tema es menor que cero");
            var lastOrder = 0;
            if (orderPlace == OrderPlace.AtHome)
            {
                if (Order != order)
                {
                    lastOrder = Order;
                    Order = order;
                    AddAfterCommitDomainEvent(new OrderAtHomeChangedDomainEvent(
                        topic: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }
            else if (orderPlace == OrderPlace.AtMenu)
            {
                if (OrderAtMenu != order)
                {
                    lastOrder = OrderAtMenu;
                    OrderAtMenu = order;
                    AddAfterCommitDomainEvent(new OrderAtMenuChangedDomainEvent(
                        topic: this,
                        orderPlace: orderPlace,
                        lastOrder: lastOrder));
                }
            }            
        }

        public void ChangeImage(
            ImageType imageType, string uri)
        {
            if (imageType == ImageType.Home)
                HomeImage = uri;            
            else
                throw new TopicException("Tipo de imagen de tema no disponible.");
        }
        public void ChangeSubTopicImage(
            int iDSubTopic, ImageType imageType, string uri)
        {
            foreach (var subTopic in this.SubTopics)
            {
                if (subTopic.ID == iDSubTopic)
                {
                    subTopic.ChangeImage(
                        imageType: imageType,
                        uri: uri);
                }
            }
        }
        public void ChangeAtHome(bool atHome)
        {
            if (AtHome != atHome)
            {
                AtHome = atHome;
                if (!atHome)
                {
                    this.Order = 0;
                }
                AddAfterCommitDomainEvent(new AtHomeChangedDomainEvent(
                    topic: this));
            }
        }

        public void BindEdition( 
            Edition edition)
        {
            Editions.Add(edition);
        }
        public void UnbindEdition(
            Edition edition)
        {
            Editions.Remove(edition);
        }

        public void AddSubTopic(string solicitedBy, string title, 
            string summary, string keywords)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("message", nameof(title));
            }
            if (string.IsNullOrEmpty(summary))
            {
                throw new ArgumentException("message", nameof(summary));
            }
            SubTopics.Add(new SubTopic(
                  createdBy: solicitedBy,
                  title: title,
                  summary: summary,
                  keywords: keywords));
        }
        public void DeleteSubTopic(SubTopic subTopic)
        {
            SubTopics.Remove(subTopic);
        }
        public void ChangeOnMenu(bool onMenu)
        {
            if (OnMenu != onMenu)
            {
                OnMenu = onMenu;
                if (!onMenu)
                {
                    this.OrderAtMenu = 0;
                }
                AddAfterCommitDomainEvent(new OnMenuChangedDomainEvent(
                    topic: this));
            }
        }

    }
}
