﻿using Pixelario.CMS.Seedwork;
using System.Threading.Tasks;

namespace Pixelario.CMS.Domain.AggregatesModel.Topics
{
    public interface ITopicRepository : IRepository<Topic>
    {
        Topic Add(Topic tema);
        void Update(Topic tema);
        void Delete(Topic tema);
        void Delete(SubTopic subTopic);
        Task<Topic> GetAsync(int iDTopic);
        Task<SubTopic> GetSubTopicAsync(int iDSubTopic);
        void ReorderTopicsAtHome(Topic topic, int LastOrder);
        void ReorderTopicsAtMenu(Topic topic, int LastOrder);
        void OrderTopicsAtMenu();
        void OrderTopicsAtHome();
        /// <summary>
        /// Correct the order on the menu of sub topics of a specific topic
        /// </summary>
        /// <param name="iDTopic">topic unique id</param>
        Task OrderSubTopicsAtMenuAsync(int iDTopic);
        /// <summary>
        /// Correct the order on home of sub topics of a specific topic
        /// </summary>
        /// <param name="iDTopic">topic unique id</param>
        Task OrderSubTopicsAtHomeAsync(int iDTopic);
        /// <summary>
        /// Reorder topic's subtopics at home after changing an order
        /// </summary>
        /// <param name="subtopic">subtopic that changed order</param>
        /// <param name="lastOrder">last order of the subtopic</param>        
        Task ReorderSubTopicsAtHome(SubTopic subtopic, int lastOrder);
        /// <summary>
        /// Reorder topic's subtopics at menu after changing an order
        /// </summary>
        /// <param name="subtopic">subtopic that changed order</param>
        /// <param name="lastOrder">last order of the subtopic</param>        
        Task ReorderSubTopicsAtMenu(SubTopic subtopic, int lastOrder);


    }
}
