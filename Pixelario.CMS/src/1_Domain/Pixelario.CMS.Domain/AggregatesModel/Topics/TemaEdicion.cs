﻿using Pixelario.CMS.Domain.AggregatesModel.Ediciones;
using Pixelario.CMS.Seedwork;
using System;

namespace Pixelario.CMS.Domain.AggregatesModel.Temas
{
    public class TemaEdicion : Entity,
        IAggregateRoot
    {
        public int IDEdicion { get; private set; }        

        protected TemaEdicion()
        {
        }
        public TemaEdicion(
            int iDEdicion) : this()
        {                   
            if (iDEdicion < 0)
            {
                throw new ArgumentException("message", nameof(iDEdicion));
            }
            IDEdicion = iDEdicion;
        }
    }
}
