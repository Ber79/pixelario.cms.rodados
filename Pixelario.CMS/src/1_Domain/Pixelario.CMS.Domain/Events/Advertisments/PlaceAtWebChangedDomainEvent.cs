﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;

namespace Pixelario.CMS.Domain.Events.Advertisments
{
    public class PlaceAtWebChangedDomainEvent : INotification
    {
        public Advertisment Advertisment;
        public int LastPlaceAtWeb { get; set; }
        public PlaceAtWebChangedDomainEvent(Advertisment advertisment, 
            int lastPlaceAtWeb)
        {
            this.Advertisment = advertisment;
            this.LastPlaceAtWeb = lastPlaceAtWeb;
        }
    }
}