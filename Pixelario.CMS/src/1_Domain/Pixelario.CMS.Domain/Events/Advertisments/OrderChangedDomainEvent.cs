﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Advertisments;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Domain.Events.Advertisments
{
    public class OrderChangedDomainEvent : INotification
    {
        public Advertisment Advertisment;
        public int LastOrder { get; set; }
        public OrderChangedDomainEvent(
            Advertisment advertisment, int lastOrder)
        {
            this.Advertisment = advertisment;
            this.LastOrder = lastOrder;
        }
    }
}