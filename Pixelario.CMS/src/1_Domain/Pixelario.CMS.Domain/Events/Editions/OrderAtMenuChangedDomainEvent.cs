﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;

namespace Pixelario.CMS.Domain.Events.Editions
{
    public class OrderAtMenuChangedDomainEvent : INotification
    {
        public Edition Edition;
        public int LastOrder { get; set; }
        public OrderAtMenuChangedDomainEvent(
            Edition edition, int lastOrder)
        {
            this.Edition = edition;
            this.LastOrder = lastOrder;
        }
    }
}