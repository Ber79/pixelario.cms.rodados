﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;

namespace Pixelario.CMS.Domain.Events.Editions
{
    public class AtHomeChangedDomainEvent : INotification
    {
        public Edition Edition { get; set; }
        public AtHomeChangedDomainEvent(Edition edition)
        {
            this.Edition = edition;
        }
    }
}