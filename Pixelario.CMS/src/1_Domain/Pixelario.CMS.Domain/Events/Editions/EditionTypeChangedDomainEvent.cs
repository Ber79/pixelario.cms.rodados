﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;

namespace Pixelario.CMS.Domain.Events.Editions
{
    public class EditionTypeChangedDomainEvent : INotification
    {
        public Edition Edition { get; }
        public EditionTypeChangedDomainEvent(Edition edition)
        {
            Edition = edition;
        }

    }
}