﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Editions;

namespace Pixelario.CMS.Domain.Events.Editions
{
    public class OnMenuChangedDomainEvent : INotification
    {
        public Edition Edition { get; set; }
        public OnMenuChangedDomainEvent(Edition edition)
        {
            this.Edition = edition;
        }
    }
}