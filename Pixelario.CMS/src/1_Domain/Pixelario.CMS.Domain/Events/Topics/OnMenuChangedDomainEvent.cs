﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
namespace Pixelario.CMS.Domain.Events.Topics
{
    public class OnMenuChangedDomainEvent : INotification
    {
        public Topic Topic { get; set; }
        public OnMenuChangedDomainEvent(Topic topic)
        {
            this.Topic = topic;
        }
    }
}