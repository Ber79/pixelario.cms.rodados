﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
namespace Pixelario.CMS.Domain.Events.Topics
{
    public class SubTopicAtHomeChangedDomainEvent : INotification
    {
        public SubTopic SubTopic { get; set; }
        public SubTopicAtHomeChangedDomainEvent(SubTopic subTopic)
        {
            this.SubTopic = subTopic;
        }
    }
}