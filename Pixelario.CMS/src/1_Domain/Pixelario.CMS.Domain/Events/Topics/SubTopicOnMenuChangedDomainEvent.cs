﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
namespace Pixelario.CMS.Domain.Events.Topics
{
    public class SubTopicOnMenuChangedDomainEvent : INotification
    {
        public SubTopic SubTopic { get; set; }
        public SubTopicOnMenuChangedDomainEvent(SubTopic subTopic)
        {
            this.SubTopic = subTopic;
        }
    }
}