﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
namespace Pixelario.CMS.Domain.Events.Topics
{
    public class AtHomeChangedDomainEvent : INotification
    {
        public Topic Topic { get; set; }
        public AtHomeChangedDomainEvent(Topic topic)
        {
            this.Topic = topic;
        }
    }
}