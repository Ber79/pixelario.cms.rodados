﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Domain.Events.Topics
{
    public class OrderAtMenuChangedDomainEvent : INotification
    {
        public Topic Topic;
        public OrderPlace OrderPlace { get; set; }
        public int LastOrder { get; set; }
        public OrderAtMenuChangedDomainEvent(
            Topic topic, OrderPlace orderPlace, int lastOrder)
        {
            this.Topic = topic;
            this.OrderPlace = orderPlace;
            this.LastOrder = lastOrder;
        }
    }
}