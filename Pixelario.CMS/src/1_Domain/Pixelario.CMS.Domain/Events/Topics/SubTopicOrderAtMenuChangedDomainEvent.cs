﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Topics;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Domain.Events.Topics
{
    public class SubTopicOrderAtMenuChangedDomainEvent : INotification
    {
        public SubTopic SubTopic;
        public OrderPlace OrderPlace { get; set; }
        public int LastOrder { get; set; }
        public SubTopicOrderAtMenuChangedDomainEvent(
            SubTopic subTopic, OrderPlace orderPlace, 
            int lastOrder)
        {
            this.SubTopic = subTopic;
            this.OrderPlace = orderPlace;
            this.LastOrder = lastOrder;
        }
    }
}