﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Domain.Events.Pages
{
    public class OrderAtMenuChangedDomainEvent : INotification
    {
        public Page Page;
        public OrderPlace OrderPlace { get; set; }
        public int LastOrder { get; set; }
        public OrderAtMenuChangedDomainEvent(
            Page page, OrderPlace orderPlace, int lastOrder)
        {
            this.Page = page;
            this.OrderPlace = orderPlace;
            this.LastOrder = lastOrder;
        }
    }
}