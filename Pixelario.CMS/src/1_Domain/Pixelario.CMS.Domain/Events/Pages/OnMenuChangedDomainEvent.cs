﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
namespace Pixelario.CMS.Domain.Events.Pages
{
    public class OnMenuChangedDomainEvent : INotification
    {
        public Page Page { get; set; }
        public OnMenuChangedDomainEvent(Page page)
        {
            this.Page = page;
        }
    }
}