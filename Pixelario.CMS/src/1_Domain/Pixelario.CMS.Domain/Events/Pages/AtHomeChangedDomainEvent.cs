﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Pages;
namespace Pixelario.CMS.Domain.Events.Pages
{
    public class AtHomeChangedDomainEvent : INotification
    {
        public Page Page { get; set; }
        public AtHomeChangedDomainEvent(Page page)
        {
            this.Page = page;
        }
    }
}