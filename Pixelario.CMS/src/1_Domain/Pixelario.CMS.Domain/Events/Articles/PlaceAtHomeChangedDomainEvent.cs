﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;

namespace Pixelario.CMS.Domain.Events.Articles
{
    public class PlaceAtHomeChangedDomainEvent : INotification
    {
        public Article Article;
        public int LastPlaceAtHome { get; set; }

        public PlaceAtHomeChangedDomainEvent(Article article, 
            int lastPlaceAtHome)
        {
            this.Article = article;
            this.LastPlaceAtHome = lastPlaceAtHome;
        }
    }
}