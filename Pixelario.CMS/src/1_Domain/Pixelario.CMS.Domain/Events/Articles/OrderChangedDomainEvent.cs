﻿using MediatR;
using Pixelario.CMS.Domain.AggregatesModel.Articles;
using Pixelario.CMS.Domain.Commons;

namespace Pixelario.CMS.Domain.Events.Articles
{
    public class OrderChangedDomainEvent : INotification
    {
        public Article Article;
        public OrderPlace OrderPlace { get; set; }
        public int LastOrder { get; set; }
        public OrderChangedDomainEvent(
            Article article, OrderPlace orderPlace, int lastOrder)
        {
            this.Article = article;
            this.OrderPlace = orderPlace;
            this.LastOrder = lastOrder;
        }
    }
}