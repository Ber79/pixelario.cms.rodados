﻿using System;
using System.Runtime.Serialization;

namespace Pixelario.CMS.Domain.Exceptions
{
    [Serializable]
    public class ArticleException : Exception
    {
        public ArticleException()
        {
        }

        public ArticleException(string message) : base(message)
        {
        }

        public ArticleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ArticleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}