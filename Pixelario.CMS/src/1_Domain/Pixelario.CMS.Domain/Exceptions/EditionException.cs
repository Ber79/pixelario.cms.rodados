﻿using System;
using System.Runtime.Serialization;

namespace Pixelario.CMS.Domain.Exceptions
{
    [Serializable]
    public class EditionException : Exception
    {
        public EditionException()
        {
        }

        public EditionException(string message) : base(message)
        {
        }

        public EditionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EditionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}