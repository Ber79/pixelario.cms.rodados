﻿using System;
using System.Runtime.Serialization;
namespace Pixelario.CMS.Domain.Exceptions
{
    [Serializable]
    public class AdvertismentException : Exception
    {
        public AdvertismentException()
        {
        }

        public AdvertismentException(string message) : base(message)
        {
        }

        public AdvertismentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AdvertismentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}