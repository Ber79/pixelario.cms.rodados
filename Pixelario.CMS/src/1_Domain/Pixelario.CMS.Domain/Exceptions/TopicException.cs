﻿using System;
using System.Runtime.Serialization;

namespace Pixelario.CMS.Domain.Exceptions
{
    [Serializable]
    internal class TopicException : Exception
    {
        public TopicException()
        {
        }

        public TopicException(string message) : base(message)
        {
        }

        public TopicException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TopicException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}