# Pixelario.CMS

C�digo fuente para la plataforma editorial de [Pixelario(http://www.pixelario.com.ar?ref=bitbucket)]

### Initialize App Setting command
#### Parameters:
- appName: Nombre de la app.
- appDomain: Dominio de la app.
- mode: Modo del archivo que va escribirse. Default "debug".
- $enableSubscriptions: Habilita las subscripciones en la app. Default false.
#### Ejemplo:
Sobre la raiz del proyecto
>. & ".\Initialize-AppSettings.ps1" -appName "pixelario" -appDomain "pixelario.com.ar" -mode "release" -$enableSubscriptions true

### Initialize ConnectionStrings command
#### Parameters:
- cmsSource: Ubicacion del server con la DB del CMS. Default "localhost".
- cmsCatalog: Nombre de la DB del CMS. Default "CCET1".
- cmsUserId: Nombre del usuario de la DB del CMS. Default "sa".
- cmsUserPassword: Password del usuario de la DB del CMS.
- identitySource: Ubicacion del server con la DB del Identity. Default "localhost".
- identityCatalog: Nombre de la DB del Identity. Default "ApplicationIdentity".
- identityUserId: Nombre del usuario de la DB del Identity. Default "sa".
- identityUserPassword: Password del usuario de la DB del Identity.
- sbcSource: Ubicacion del server con la DB del M�dulo de Subscripci�n. Default "localhost".
- sbcCatalog: Nombre de la DB del M�dulo de Subscripci�n. Default "Subscriptions".
- sbcUserId: Nombre del usuario de la DB del M�dulo de Subscripci�n. Default "sa".
- sbcUserPassword: Password del usuario de la DB del M�dulo de Subscripci�n.
- mode: Modo del archivo que va escribirse. Default "debug".

#### Ejemplo:
Sobre la raiz del proyecto
>. & ".\Initialize-ConnectionStrings.ps1" -cmsCatalog "pixelario" -mode "release"
